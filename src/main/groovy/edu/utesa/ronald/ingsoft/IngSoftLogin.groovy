package edu.utesa.ronald.ingsoft

import com.vaadin.annotations.Push
import com.vaadin.annotations.Theme
import com.vaadin.annotations.Widgetset
import com.vaadin.server.Page
import com.vaadin.server.VaadinRequest
import com.vaadin.spring.annotation.SpringUI
import com.vaadin.ui.HorizontalLayout
import com.vaadin.ui.UI
import edu.utesa.ronald.ingsoft.models.interfaces.CallbackUser
import edu.utesa.ronald.ingsoft.ui.Login
import edu.utesa.ronald.ingsoft.utils.Languages
import edu.utesa.ronald.ingsoft.utils.Themes
import edu.utesa.ronald.ingsoft.utils.configurations.VaadinConfiguration
import groovy.transform.CompileStatic

/**
 * login
 *
 *  Created by ronald on 2/25/17.
 */
@SuppressWarnings("GroovyUnusedDeclaration")
@Push
@Theme(Themes.ING_SOFT)
@Widgetset("AppWidgetSet")
@SpringUI(path = VaadinConfiguration.LOGIN)
@CompileStatic
class IngSoftLogin extends UI {

    private HorizontalLayout mainLayout
    private Login login

    private CallbackUser callbackUser

    @Override
    protected void init(VaadinRequest vaadinRequest) {
        Languages.setDefault(Languages.instance.SPANISH)
        mainLayout = new HorizontalLayout()
        mainLayout.setSizeFull()
        mainLayout.setSpacing(true)
        setContent(mainLayout)
        callbackUser = new CallbackUser() {
            @Override
            void onDialogResult() {
                if (login) {
                    login.close()
                }
                Page.getCurrent().setLocation(VaadinConfiguration.APP)
            }
        }

        login = new Login(callbackUser)
        addWindow(login)
    }
}
