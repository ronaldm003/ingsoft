package edu.utesa.ronald.ingsoft

import com.vaadin.annotations.Push
import com.vaadin.annotations.Theme
import com.vaadin.annotations.Widgetset
import com.vaadin.server.VaadinRequest
import com.vaadin.spring.annotation.SpringUI
import com.vaadin.ui.UI
import edu.utesa.ronald.ingsoft.ui.SignUp
import edu.utesa.ronald.ingsoft.utils.Themes
import edu.utesa.ronald.ingsoft.utils.configurations.VaadinConfiguration
import groovy.transform.CompileStatic

/**
 * sign up
 *  Created by ronald on 2/25/17.
 */
@SuppressWarnings("GroovyUnusedDeclaration")
@Push
@Theme(Themes.ING_SOFT)
@Widgetset("AppWidgetSet")
@SpringUI(path = VaadinConfiguration.SIGN_UP)
@CompileStatic
class IngSoftSignUp extends UI {

    private SignUp signUp

    @Override
    protected void init(VaadinRequest vaadinRequest) {
        signUp = new SignUp()
        addWindow(signUp)
    }
}
