package edu.utesa.ronald.ingsoft

import edu.utesa.ronald.ingsoft.utils.BootStrap
import edu.utesa.ronald.ingsoft.utils.Constants
import edu.utesa.ronald.ingsoft.utils.Languages
import groovy.transform.CompileStatic
import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication

/**
 * inicio del app
 *  Created by ronald on 2/25/17.
 */
@CompileStatic
@SpringBootApplication
class IngSoftSpringInit {
    private static BootStrap bootStrap

    static void main(String[] args) throws Exception {
        startSpring(args)
    }

    static void startSpring(String[] args) {
        Constants.instance.setApplicationContext(SpringApplication.run(IngSoftSpringInit.class, args))
        Constants.instance.autoWiredClass(this)
        bootStrap = new BootStrap()
        bootStrap.init()
    }

    static void stop() {
    }
}
