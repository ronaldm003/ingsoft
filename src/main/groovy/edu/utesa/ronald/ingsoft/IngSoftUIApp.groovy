package edu.utesa.ronald.ingsoft

import com.vaadin.annotations.Push
import com.vaadin.annotations.Theme
import com.vaadin.annotations.Widgetset
import com.vaadin.server.Page
import com.vaadin.server.VaadinRequest
import com.vaadin.server.VaadinSession
import com.vaadin.spring.annotation.SpringUI
import com.vaadin.ui.HorizontalLayout
import com.vaadin.ui.UI
import edu.utesa.ronald.ingsoft.models.LoginManager
import edu.utesa.ronald.ingsoft.ui.IngSoft
import edu.utesa.ronald.ingsoft.utils.Constants
import edu.utesa.ronald.ingsoft.utils.Themes
import edu.utesa.ronald.ingsoft.utils.configurations.VaadinConfiguration
import groovy.transform.CompileStatic

/**
 *  Created by ronald on 2/25/17.
 */
@SuppressWarnings("GroovyUnusedDeclaration")
@Push
@Theme(Themes.ING_SOFT)
@Widgetset("AppWidgetSet")
@SpringUI(path = VaadinConfiguration.APP)
@CompileStatic
class IngSoftUIApp extends UI {
    private HorizontalLayout mainLayout
    private IngSoft profileSoft
    private LoginManager loginManager

    @Override
    protected void init(VaadinRequest vaadinRequest) {
        mainLayout = new HorizontalLayout()
        mainLayout.setSizeFull()
        mainLayout.setSpacing(true)
        setContent(mainLayout)
        loginManager = (LoginManager) VaadinSession.current.session.getAttribute(Constants.USER_LOGIN)
        if (loginManager) {
            profileSoft = new IngSoft()
            setContent(profileSoft)
        } else {
            try {
                Page.getCurrent().setLocation(VaadinConfiguration.LOGIN)
                VaadinSession.getCurrent().getSession().invalidate()
            } catch (Exception ignore) {
            }
        }
    }
}
