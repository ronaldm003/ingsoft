package edu.utesa.ronald.ingsoft.components

import com.vaadin.ui.Calendar
import com.vaadin.ui.components.calendar.CalendarComponentEvents
import com.vaadin.ui.components.calendar.handler.BasicDateClickHandler
import com.vaadin.ui.components.calendar.handler.BasicWeekClickHandler
import edu.utesa.ronald.ingsoft.models.enums.CalendarMode
import edu.utesa.ronald.ingsoft.models.interfaces.CallbackCalendar
import edu.utesa.ronald.ingsoft.utils.Languages
import groovy.transform.CompileStatic

import java.text.DateFormatSymbols

/**
 *  Created by ronald on 7/26/16.
 */
@CompileStatic
final class CalendarManager {
    private GregorianCalendar calendar
    private Calendar calendarComponent
    private CallbackCalendar callbackCalendar

    private Date currentMonthsFirstDate
    private CalendarMode calendarMode

    CalendarManager(Calendar calendarComponent, CallbackCalendar callbackCalendar) {
        this.calendarComponent = calendarComponent
        this.callbackCalendar = callbackCalendar
        configureCalendar()
    }

    private void configureCalendar() {
        calendar = new GregorianCalendar(Languages.instance.getDefault())
        calendar.setTime(getToday())

        calendarComponent.setLocale(Languages.instance.getDefault())
        calendarComponent.setImmediate(true)
        calendarComponent.getInternalCalendar().setTime(today)
        calendarComponent.setStartDate(calendarComponent.getStartDate())
        calendarComponent.setEndDate(calendarComponent.getEndDate())

        calendar.add(GregorianCalendar.DAY_OF_MONTH, -rollAmount())
        currentMonthsFirstDate = calendar.getTime()

        resetTime(false)
        switchToMonthView()

        calendarComponent.setHandler(new BasicWeekClickHandler() {
            @Override
            void weekClick(CalendarComponentEvents.WeekClick event) {
                super.weekClick(event)
                calendarMode = CalendarMode.WEEK
                GregorianCalendar cal = new GregorianCalendar()
                cal.setWeekDate(event.getYear(), event.getWeek(), java.util.Calendar.SUNDAY)
                Date start = cal.getTime()
                cal.setWeekDate(event.getYear(), event.getWeek(), java.util.Calendar.SATURDAY)
                callbackCalendar.findEvents(start, cal.getTime())
            }
        })

        calendarComponent.setHandler(new BasicDateClickHandler() {
            @Override
            void dateClick(CalendarComponentEvents.DateClickEvent event) {
                super.dateClick(event)
                calendarMode = CalendarMode.DAY
                callbackCalendar.findEvents(event.getDate(), event.getDate())
            }
        })

        switchToWeekView()
        refresh()
    }

    void switchToMonthView() {
        calendarMode = CalendarMode.MONTH
        initDateCalendar()
        calendar.setTime(getToday())
        refresh()
    }

    void switchToWeekView() {
        refresh()
        CalendarComponentEvents.WeekClickHandler handler = (CalendarComponentEvents.WeekClickHandler) calendarComponent
                .getHandler(CalendarComponentEvents.WeekClick.EVENT_ID)
        handler.weekClick(new CalendarComponentEvents.WeekClick(calendarComponent, calendar
                .get(GregorianCalendar.WEEK_OF_YEAR), calendar
                .get(GregorianCalendar.YEAR)))
    }

    void switchToDayView() {
        BasicDateClickHandler handler = (BasicDateClickHandler) calendarComponent
                .getHandler(CalendarComponentEvents.DateClickEvent.EVENT_ID)
        handler.dateClick(new CalendarComponentEvents.DateClickEvent(calendarComponent, calendar.getTime()))
    }

    void hideWeekends(boolean weekendsHidden) {
        if (weekendsHidden) {
            int firstToShow = (GregorianCalendar.MONDAY - calendar.getFirstDayOfWeek()) % 7
            calendarComponent.setFirstVisibleDayOfWeek(firstToShow + 1)
            calendarComponent.setLastVisibleDayOfWeek(firstToShow + 5)
        } else {
            calendarComponent.setFirstVisibleDayOfWeek(1)
            calendarComponent.setLastVisibleDayOfWeek(7)
        }
    }

    void next() {
        switch (calendarMode) {
            case CalendarMode.MONTH:
                nextMonth()
                break
            case CalendarMode.WEEK:
                nextWeek()
                break
            case CalendarMode.DAY:
                nextDay()
                break
        }
    }

    void previous() {
        switch (calendarMode) {
            case CalendarMode.MONTH:
                previousMonth()
                break
            case CalendarMode.WEEK:
                previousWeek()
                break
            case CalendarMode.DAY:
                previousDay()
                break
        }
    }

    private void nextMonth() {
        rollMonth(1)
    }

    private void previousMonth() {
        rollMonth(-1)
    }

    private void nextWeek() {
        rollWeek(1)
    }

    private void previousWeek() {
        rollWeek(-1)
    }

    private void nextDay() {
        rollDate(1)
    }

    private void previousDay() {
        rollDate(-1)
    }

    private void rollMonth(int direction) {
        calendar.setTime(currentMonthsFirstDate)
        calendar.add(GregorianCalendar.MONTH, direction)
        resetTime(false)
        currentMonthsFirstDate = calendar.getTime()
        calendarComponent.setStartDate(currentMonthsFirstDate)

        calendar.add(GregorianCalendar.MONTH, 1)
        calendar.add(GregorianCalendar.DATE, -1)
        resetCalendarTime(true)
    }

    private void rollWeek(int direction) {
        calendar.add(GregorianCalendar.WEEK_OF_YEAR, direction)
        calendar.set(GregorianCalendar.DAY_OF_WEEK, calendar.getFirstDayOfWeek())
        resetCalendarTime(false)
        resetTime(true)
        calendar.add(GregorianCalendar.DATE, 6)
        calendarComponent.setEndDate(calendar.getTime())
    }

    private void rollDate(int direction) {
        calendar.add(GregorianCalendar.DATE, direction)
        resetCalendarTime(false)
        resetCalendarTime(true)
    }

    private void resetTime(boolean max) {
        if (max) {
            calendar.set(GregorianCalendar.HOUR_OF_DAY, calendar.getMaximum(GregorianCalendar.HOUR_OF_DAY))
            calendar.set(GregorianCalendar.MINUTE, calendar.getMaximum(GregorianCalendar.MINUTE))
            calendar.set(GregorianCalendar.SECOND, calendar.getMaximum(GregorianCalendar.SECOND))
            calendar.set(GregorianCalendar.MILLISECOND, calendar.getMaximum(GregorianCalendar.MILLISECOND))
        } else {
            calendar.set(GregorianCalendar.HOUR_OF_DAY, 0)
            calendar.set(GregorianCalendar.MINUTE, 0)
            calendar.set(GregorianCalendar.SECOND, 0)
            calendar.set(GregorianCalendar.MILLISECOND, 0)
        }
    }

    private void initDateCalendar() {
        calendar.add(GregorianCalendar.DAY_OF_MONTH, -rollAmount())
        calendarComponent.setStartDate(calendar.getTime())
        calendar.add(GregorianCalendar.MONTH, 1)
        calendar.add(GregorianCalendar.DATE, -1)
        calendarComponent.setEndDate(calendar.getTime())
    }

    private void resetCalendarTime(boolean resetEndTime) {
        resetTime(resetEndTime)
        if (resetEndTime) {
            calendarComponent.setEndDate(calendar.getTime())
        } else {
            calendarComponent.setStartDate(calendar.getTime())
        }
    }

    private int rollAmount() {
        return calendar.get(GregorianCalendar.DAY_OF_MONTH) - 1
    }

    private static Date getToday() {
        return new Date()
    }

    String getMothAndYear() {
        DateFormatSymbols s = new DateFormatSymbols(Languages.instance.getDefault())
        String month = s.getShortMonths()[calendar.get(GregorianCalendar.MONTH)]
        return month + " " + calendar.get(GregorianCalendar.YEAR)
    }

    Date getEndOfDay(Date date) {
        java.util.Calendar c = (java.util.Calendar) calendar.clone()
        c.setTime(date)
        c.set(java.util.Calendar.MILLISECOND, c.getActualMaximum(java.util.Calendar.MILLISECOND))
        c.set(java.util.Calendar.SECOND, c.getActualMaximum(java.util.Calendar.SECOND))
        c.set(java.util.Calendar.MINUTE, c.getActualMaximum(java.util.Calendar.MINUTE))
        c.set(java.util.Calendar.HOUR, c.getActualMaximum(java.util.Calendar.HOUR))
        c.set(java.util.Calendar.HOUR_OF_DAY, c.getActualMaximum(java.util.Calendar.HOUR_OF_DAY))
        return c.getTime()
    }

    CalendarMode getCalendarMode() {
        return calendarMode
    }

    void refresh() {
        callbackCalendar.findEvents(calendarComponent.getStartDate(), calendarComponent.getEndDate())
    }
}
