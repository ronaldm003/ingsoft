package edu.utesa.ronald.ingsoft.components

import com.vaadin.ui.Grid
import com.vaadin.ui.themes.ValoTheme
import edu.utesa.ronald.ingsoft.utils.Languages
import edu.utesa.ronald.ingsoft.utils.container.BaseContainer
import edu.utesa.ronald.ingsoft.utils.container.BaseQueryFactory
import groovy.transform.CompileStatic

/**
 *  Created by ronald on 7/08/17.
 */
@CompileStatic
final class GridManager {

    private BaseContainer container
    private Grid grid

    private boolean activeCountFooter = false
    private Grid.FooterRow frCount

    GridManager(Grid grid, BaseContainer container) {
        this.grid = grid
        this.container = container
        configureGrid()
    }

    private void configureGrid() {
        grid.addStyleName(ValoTheme.TABLE_COMPACT)
        grid.removeAllColumns()
        grid.setContainerDataSource(container)
        grid.removeColumn(BaseQueryFactory.OBJ)
        for (Grid.Column column : grid.getColumns()) {
            grid.getColumn(column.getPropertyId().toString()).setHeaderCaption(Languages.instance.getText("grid.column." + column.getPropertyId().toString()))
        }
    }

    final void select(Object itemId) {
        grid.select(itemId)
    }

    final void clearSelect() {
        grid.deselectAll()
        grid.select(null)
    }

    final boolean haveSelection() {
        if (isMultiSelect()) {
            return grid.getSelectedRows().size() > 0
        }
        return grid.getSelectedRow()
    }

    final boolean isMultiSelect() {
        if (grid.getSelectionModel() instanceof Grid.SelectionModel.Multi) {
            return true
        }
        return false
    }

    final void updateTable() {
        grid.clearSortOrder()
        clearSelect()
        if (activeCountFooter) {
            countFooter()
        }
    }

    final void setActiveCountFooter(boolean activeCountFooter) {
        this.activeCountFooter = activeCountFooter
    }

    final void countFooter() {
        if (!frCount) {
            frCount = grid.appendFooterRow()
        }
        int size = container.sizeQuery()
        for (Object pid : grid.getContainerDataSource().getContainerPropertyIds() as Collection) {
            Grid.FooterCell cell = frCount.getCell(pid)
            if (cell) {
                cell.setText(Languages.instance.getText("table.configuration.row.count") + " " + size)
            }
        }
    }

    final Grid getGrid() {
        return grid
    }

    final void filtered(boolean filter) {
        container.setFiltered(filter)
    }
}
