package edu.utesa.ronald.ingsoft.components

import com.vaadin.server.Resource
import com.vaadin.ui.Component
import com.vaadin.ui.TabSheet
import com.vaadin.ui.themes.ValoTheme
import groovy.transform.CompileStatic

/**
 *  Created by ronald on 7/26/17.
 */
@SuppressWarnings(["ChangeToOperator", "ChangeToOperator"])
@CompileStatic
final class TabSheetManager {

    private TabSheet tabSheet

    TabSheetManager(TabSheet tabSheet) {
        this.tabSheet = tabSheet
        this.tabSheet.addStyleName(ValoTheme.TABSHEET_COMPACT_TABBAR)
    }

    /**
     * Adiciona los tabs normales y se cierran.
     *
     * @param name
     * @param component
     * @param icon
     */
    void addTabPanel(String name, Component component, Resource icon) {
        addTabPanel(name, component, icon, true)
    }

    /**
     * Adiciona tabs sin iconos y no se cierran
     * @param name
     * @param component
     */
    void addTabPanel(String name, Component component) {
        addTabPanel(name, component, null, false)
    }

    void removeTabs(List<TabSheet.Tab> tabs) {
        for (TabSheet.Tab tab : tabs) {
            tabSheet.removeTab(tab)
        }
    }

    void removeTab(String name) {
        List<TabSheet.Tab> tabs = new ArrayList<>()
        for (int i = 0; i < tabSheet.getComponentCount(); i++) {
            if (tabSheet.getTab(i).caption.equals(name)) {
                tabs.add(tabSheet.getTab(i))
                return
            }
        }
        removeTabs(tabs)
    }

    void addTabPanel(String name, Component component, Resource icon, boolean closable) {
        for (int i = 0; i < tabSheet.getComponentCount(); i++) {
            if (tabSheet.getTab(i).caption.equals(name)) {
                tabSheet.setSelectedTab(i)
                return
            }
        }
        TabSheet.Tab tab = tabSheet.addTab(component, name, icon)
        tab.setClosable(closable)
        selectTab(tab)
    }

    int countTabs() {
        return tabSheet.getComponentCount()
    }

    TabSheet.Tab getTab(int position) {
        return tabSheet.getTab(position)
    }

    /**
     * Retorno false para mantener rapida la contesta.
     *
     * @param position
     * @return
     */
    boolean selectTab(int position) {
        tabSheet.setSelectedTab(position)
        return false
    }

    /**
     * Retorno false para mantener rapida la contesta.
     *
     * @param position
     * @return
     */
    boolean selectTab(TabSheet.Tab tab) {
        tabSheet.setSelectedTab(tab)
        return false
    }

    String selectedTabName() {
        return tabSheet().getTab(tabSheet.getSelectedTab()).caption
    }

    final TabSheet tabSheet() {
        return tabSheet
    }
}
