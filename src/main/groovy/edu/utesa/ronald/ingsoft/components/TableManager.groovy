package edu.utesa.ronald.ingsoft.components

import com.vaadin.ui.Table
import com.vaadin.ui.themes.ValoTheme
import edu.utesa.ronald.ingsoft.models.constants.Styles
import edu.utesa.ronald.ingsoft.utils.container.BaseContainer
import groovy.transform.CompileStatic

/**
 *  Created by ronald on 7/08/17.
 */
@CompileStatic
final class TableManager {

    private BaseContainer container
    private Table table

    private boolean activeCountFooter = false

    TableManager(Table table, BaseContainer container) {
        this.table = table
        this.container = container
        configureGrid()
    }

    private void configureGrid() {
        table.addStyleName(ValoTheme.TABLE_SMALL)
        table.addStyleName(ValoTheme.TABLE_COMPACT)
        table.setEditable(false)
        table.setImmediate(true)
        table.setFooterVisible(false)
        table.setContainerDataSource(container)
    }

    final void select(Object itemId) {
        table.select(itemId)
    }

    final void clearSelect() {
        table.select(null)
    }

    final void updateTable() {
        container.refresh()
        table.refreshRowCache()
        table.unselect(table.getValue())
    }

    final void setActiveCountFooter(boolean activeCountFooter) {
        this.activeCountFooter = activeCountFooter
    }

    void removeColumnsDefault() {
        table.setColumnCollapsingAllowed(true)
        table.setColumnCollapsed("OBJ", true)
        table.addStyleName(Styles.TABLE_HIDE_COLLAPSE)
    }

    final Table getTable() {
        return table
    }

    final void filtered(boolean filter) {
        container.setFiltered(filter)
    }
}
