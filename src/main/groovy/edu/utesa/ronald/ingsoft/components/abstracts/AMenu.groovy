package edu.utesa.ronald.ingsoft.components.abstracts

import com.kbdunn.vaadin.addons.fontawesome.FontAwesome
import com.vaadin.server.VaadinSession
import com.vaadin.ui.Button
import com.vaadin.ui.Component
import com.vaadin.ui.VerticalLayout
import com.vaadin.ui.Window
import com.vaadin.ui.themes.ValoTheme
import edu.utesa.ronald.ingsoft.models.LoginManager
import edu.utesa.ronald.ingsoft.models.constants.Styles
import edu.utesa.ronald.ingsoft.models.interfaces.OnClick
import edu.utesa.ronald.ingsoft.models.interfaces.SearchMenu
import edu.utesa.ronald.ingsoft.ui.components.ContentPanel
import edu.utesa.ronald.ingsoft.utils.Constants
import edu.utesa.ronald.ingsoft.utils.Languages
import groovy.transform.CompileStatic

/**
 *  Created by ronald on 03/17/17.
 */
@SuppressWarnings("ChangeToOperator")
@CompileStatic
abstract class AMenu extends VerticalLayout implements SearchMenu {

    private static final String DESCRIPTION = ".description"
    private static final String KEY = ".key"
    protected LoginManager loginManager = null
    ContentPanel contentPanel

    protected AMenu(ContentPanel contentPanel) {
        this.contentPanel = contentPanel
        loginManager = (LoginManager) VaadinSession.current.session.getAttribute(Constants.USER_LOGIN)
        buildLayout()
        security()
    }

    private static boolean visibleButton(Button button, boolean showMenu) {
        button.setVisible(showMenu)
        button.setEnabled(showMenu)
        return showMenu
    }

    private boolean visibleMenu(boolean showMenu) {
        setVisible(showMenu)
        setEnabled(showMenu)
        return showMenu
    }

    AMenu subMenu() {
        addStyleName(Styles.SUB_MENU)
        return this
    }

    protected abstract void buildLayout()

    protected abstract void security()

    protected abstract String captionContants(String caption)

    private boolean findByButton(Button button, List<String> tokens) {
        boolean result = searchByCaption(button.getCaption(), tokens) ||
                searchByDescription(captionContants(button.getCaption()), tokens) ||
                searchByKey(captionContants(button.getCaption()), tokens)
        return visibleButton(button, result)
    }

    private static boolean findBySubMenu(AMenu aMenu, String search) {
        return aMenu.onSearch(search)
    }

    @Override
    boolean onSearch(String search) {
        List<String> tokens = tokens(search)
        Iterator<Component> itr = this.iterator()
        boolean showMenu = false
        while (itr.hasNext()) {
            Component component = itr.next()
            if (component instanceof Button) {
                if (findByButton((Button) component, tokens)) {
                    showMenu = true
                }
            } else if (component instanceof AMenu) {
                if (findBySubMenu((AMenu) component, search)) {
                    showMenu = true
                }
            }
        }
        return visibleMenu(showMenu)
    }


    @Override
    void clearSearch() {
        Iterator<Component> itr = this.iterator()
        while (itr.hasNext()) {
            Component component = itr.next()
            if (component instanceof Button) {
                visibleButton((Button) component, true)
            } else if (component instanceof AMenu) {
                ((AMenu) component).clearSearch()
            }
        }
        visibleMenu(true)
    }

    protected Button createButton(final String name, FontAwesome fontAwesome, final OnClick onClick) {
        Button button = new Button(name)
        button.setSizeFull()
        button.setIcon(fontAwesome)
        button.addStyleName(Styles.MENU_BUTTON)
        button.addStyleName(ValoTheme.BUTTON_TINY)
        button.addClickListener(new Button.ClickListener() {
            @Override
            void buttonClick(Button.ClickEvent event) {
                Component component = onClick.onClick()
                if (component) {
                    if (component instanceof Window) {
                        getUI().addWindow(component)
                    } else {
                        contentPanel.addTabPanel(name, component, fontAwesome)
                    }
                }
            }
        })
        return button
    }

    private static boolean searchBy(String caption, List<String> values) {
        for (String val : values) {
            if (caption.contains(val)) {
                return true
            }
        }
        return false
    }

    protected static boolean searchByCaption(String caption, List<String> values) {
        return searchBy(caption.toLowerCase(), values)
    }

    protected static boolean searchByDescription(String constant, List<String> values) {
        return searchBy(Languages.instance.getText(constant + DESCRIPTION).toLowerCase(), values)
    }

    protected static boolean searchByKey(String constant, List<String> values) {
        return searchBy(Languages.instance.getText(constant + KEY).toLowerCase(), values)
    }

    protected static List<String> tokens(String values) {
        List<String> tokens = values.toLowerCase().split(" ").toList()
        return tokens
    }

    boolean haveMenuItems() {
        return getComponentCount() > 0
    }
}
