package edu.utesa.ronald.ingsoft.components.abstracts

import com.vaadin.server.VaadinSession
import com.vaadin.ui.Component
import com.vaadin.ui.Grid
import com.vaadin.ui.Panel
import com.vaadin.ui.themes.ValoTheme
import edu.utesa.ronald.ingsoft.components.GridManager
import edu.utesa.ronald.ingsoft.models.LoginManager
import edu.utesa.ronald.ingsoft.utils.Constants
import edu.utesa.ronald.ingsoft.utils.container.BaseContainer
import groovy.transform.CompileStatic

/**
 *  Created by ronald on 6/27/17.
 */
@CompileStatic
abstract class APanel extends Panel {

    public LoginManager loginManager = null
    GridManager gridManager

    protected void confLayout(Component component) {
        setContent(component)
        addStyleName(ValoTheme.PANEL_BORDERLESS)
        loginManager = (LoginManager) VaadinSession.current.session.getAttribute(Constants.USER_LOGIN)
        setSizeFull()
        confLanguage()
        buildLayout()
        security()
    }

    protected abstract void confLanguage()

    protected abstract void buildLayout()

    protected abstract void security()

    protected void manageGrid(Grid grid, BaseContainer container) {
        gridManager = new GridManager(grid, container)
    }

    void updateTable() {
        if (gridManager) {
            gridManager.updateTable()
        }
    }

    void setActiveCountFooter(boolean activeCountFooter) {
        if (gridManager) {
            gridManager.setActiveCountFooter(activeCountFooter)
        }
    }

    void filtered(boolean filter) {
        if (gridManager) {
            gridManager.filtered(filter)
        }
    }

    protected static void applyPermission(Component component, boolean canUse) {
        component.setVisible(canUse)
        component.setEnabled(canUse)
    }
}
