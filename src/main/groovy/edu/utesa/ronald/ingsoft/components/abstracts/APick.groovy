package edu.utesa.ronald.ingsoft.components.abstracts

import com.vaadin.ui.Component
import groovy.transform.CompileStatic

/**
 *  Created by ronald on 7/20/17.
 */
@CompileStatic
abstract class APick extends AWindow {

    protected static String PICK = ".pick"

    protected boolean multiSelect

    protected APick(boolean multiSelect) {
        this.multiSelect = multiSelect
    }

    @Override
    protected void confLayout(Component component) {
        super.confLayout(component)
        pickerLanguage()
        preparePicker()
        buildPicker()
    }

    protected abstract void pickerLanguage()

    protected abstract void preparePicker()

    protected abstract void buildPicker()
}
