package edu.utesa.ronald.ingsoft.components.abstracts

import com.vaadin.event.SelectionEvent
import com.vaadin.server.VaadinSession
import com.vaadin.ui.Component
import com.vaadin.ui.Grid
import com.vaadin.ui.Panel
import com.vaadin.ui.Window
import edu.utesa.ronald.ingsoft.components.GridManager
import edu.utesa.ronald.ingsoft.models.LoginManager
import edu.utesa.ronald.ingsoft.utils.Constants
import edu.utesa.ronald.ingsoft.utils.container.BaseContainer
import edu.utesa.ronald.ingsoft.utils.permissions.PermissionType
import groovy.transform.CompileStatic

/**
 *  Created by ronald on 6/27/17.
 */
@CompileStatic
abstract class ATab<T extends Window, P> extends Panel {

    public LoginManager loginManager = null
    P itemSelect
    public GridManager gridManager

    protected void confLayout(Component component) {
        setContent(component)
        loginManager = (LoginManager) VaadinSession.current.session.getAttribute(Constants.USER_LOGIN)
        setSizeFull()
        confLanguage()
        buildLayout()
        if (!loginManager.havePermission(PermissionType.ROOT)) {
            security()
        }
    }

    protected abstract void confLanguage()

    protected abstract void buildLayout()

    abstract void refreshItem()

    abstract T onNew()

    abstract T onEdit()

    abstract T onView()

    abstract Class topClass()

    protected void select(P p) {
        itemSelect = p
    }

    void unselect() {
        itemSelect = null
        if (gridManager) {
            gridManager.clearSelect()
        }
    }

    abstract boolean deleteDomain()

    protected abstract void security()

    protected abstract void enableMenu()

    protected abstract void disableMenu()

    protected abstract void manageGrid(BaseContainer container)

    protected void manageGrid(Grid grid, BaseContainer container) {
        gridManager = new GridManager(grid, container)
        gridManager.getGrid().addSelectionListener(new SelectionEvent.SelectionListener() {
            @Override
            void select(SelectionEvent event) {
                if (gridManager.haveSelection()) {
                    select((P) gridManager.grid.getSelectedRow())
                    enableMenu()
                } else {
                    unselect()
                    disableMenu()
                }
            }
        })
    }

    void setActiveCountFooter(boolean activeCountFooter) {
        gridManager.setActiveCountFooter(activeCountFooter)
    }
}
