package edu.utesa.ronald.ingsoft.components.abstracts

import com.vaadin.server.VaadinSession
import com.vaadin.ui.Component
import com.vaadin.ui.Grid
import com.vaadin.ui.Window
import edu.utesa.ronald.ingsoft.components.GridManager
import edu.utesa.ronald.ingsoft.models.LoginManager
import edu.utesa.ronald.ingsoft.utils.Constants
import edu.utesa.ronald.ingsoft.utils.container.BaseContainer
import groovy.transform.CompileStatic

/**
 *  Created by ronald on 6/25/17.
 */
@CompileStatic
abstract class AWindow extends Window {

    LoginManager loginManager = null
    public GridManager gridManager

    protected void confLayout(Component component) {
        setContent(component)
        loginManager = (LoginManager) VaadinSession.current.session.getAttribute(Constants.USER_LOGIN)
        prepare()
        confLanguage()
        buildLayout()
        security()
    }

    protected abstract void prepare()

    protected abstract void confLanguage()

    protected abstract void buildLayout()

    protected abstract void security()

    protected void manageGrid(Grid grid, BaseContainer container) {
        gridManager = new GridManager(grid, container)
    }

    void updateTable() {
        gridManager.updateTable()
    }

    void setActiveCountFooter(boolean activeCountFooter) {
        gridManager.setActiveCountFooter(activeCountFooter)
    }
}
