package edu.utesa.ronald.ingsoft.components.abstracts

import com.vaadin.server.Sizeable
import com.vaadin.ui.GridLayout
import com.vaadin.ui.TabSheet
import com.vaadin.ui.Window
import edu.utesa.ronald.ingsoft.models.LoginManager
import edu.utesa.ronald.ingsoft.ui.components.forms.FormEventAction
import groovy.transform.CompileStatic

/**
 *  Created by ronald on 07/25/2016.
 */
@CompileStatic
abstract class AWindowEventForm extends Window {

    protected static String NEW_CAPTION = ".new"
    protected static String EDIT_CAPTION = ".edit"
    protected static String VIEW_CAPTION = ".view"
    protected static String FORM = ".form."

    protected GridLayout mainLayout

    protected TabSheet tabSheet = new TabSheet()
    protected FormEventAction formEventAction = new FormEventAction()

    public Boolean edit = null
    public LoginManager loginManager = null

    protected void initLayout(int columns, int rows) {
        center()
        mainLayout = new GridLayout(columns, rows)
        mainLayout.setWidth(100, Sizeable.Unit.PERCENTAGE)
        mainLayout.setSpacing(true)
        buildLayout()
    }

    protected void showLayout() {
        security()
        setContent(mainLayout)
    }

    protected abstract void prepareTabStart()

    protected abstract void prepareTab(String captionLanguage)

    protected abstract void onCreate()

    protected abstract void onEditView()

    protected abstract void buildLayout()

    protected abstract void restore()

    protected abstract void visualize()

    protected abstract void security()

    protected abstract boolean getElementsValid()

}
