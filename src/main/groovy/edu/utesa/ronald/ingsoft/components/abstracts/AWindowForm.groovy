package edu.utesa.ronald.ingsoft.components.abstracts

import com.vaadin.server.VaadinSession
import com.vaadin.ui.Component
import edu.utesa.ronald.ingsoft.models.LoginManager
import edu.utesa.ronald.ingsoft.utils.Constants
import groovy.transform.CompileStatic
import sun.reflect.generics.reflectiveObjects.NotImplementedException

/**
 *  Created by ronald on 6/25/17.
 */
@CompileStatic
abstract class AWindowForm<T> extends AWindow {

    protected static String NEW_CAPTION = ".new"
    protected static String EDIT_CAPTION = ".edit"
    protected static String VIEW_CAPTION = ".view"
    protected static String FORM = ".form."

    protected abstract void prepareTabStart()

    protected abstract void prepareTab(String captionLanguage)

    protected abstract void onCreate()

    protected abstract void onEditView()

    abstract T onSave()

    /**
     * Caso para tabs sin utilidad en el save.
     * Solo son informativos
     */
    protected abstract void onViewTabs()

    protected abstract void buildView()

    @Override
    protected void confLayout(Component component) {
        throw new NotImplementedException()
    }

    protected void confLayout(String captionLanguage) {
        loginManager = (LoginManager) VaadinSession.current.session.getAttribute(Constants.USER_LOGIN)
        confLanguage()
        prepareTab(captionLanguage)
        buildLayout()
        prepareTabStart()
        security()
    }
}
