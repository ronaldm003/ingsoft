package edu.utesa.ronald.ingsoft.components.abstracts

import com.vaadin.server.StreamResource
import groovy.transform.CompileStatic

/**
 *  Created by ronald on 1/23/2015.
 */
@CompileStatic
abstract class AWindowReport extends AWindow {

    protected static StreamResource getStreamResource(ByteArrayOutputStream baos, String nombre) {
        StreamResource.StreamSource source = new StreamResource.StreamSource() {
            InputStream getStream() {
                ByteArrayOutputStream stream = baos
                InputStream input = new ByteArrayInputStream(stream.toByteArray())
                return input
            }
        }
        StreamResource resource = new StreamResource(source, nombre + System.currentTimeMillis())
        return resource
    }
}
