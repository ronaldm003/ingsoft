package edu.utesa.ronald.ingsoft.components.abstracts

import com.vaadin.ui.Component
import edu.utesa.ronald.ingsoft.models.interfaces.WindowStartFocus
import groovy.transform.CompileStatic

/**
 *  Created by ronald on 7/18/17.
 */
@CompileStatic
abstract class AWindowTab<ITEM> extends APanel implements WindowStartFocus {

    ITEM itemSave
    protected Boolean edit = null

    protected AWindowTab(ITEM item, Boolean edit) {
        itemSave = item
        this.edit = edit
    }

    @Override
    protected void confLayout(Component component) {
        super.confLayout(component)
        if (edit != null) {
            restore()
            if (!edit) {
                visualize()
            }
        }
    }

    @Override
    protected void buildLayout() {
    }

    @Override
    protected void security() {
    }

    protected abstract void restore()

    protected abstract void visualize()

    protected abstract boolean getElementsValid()

    protected abstract void collectData()

    protected abstract ITEM getValid()
}
