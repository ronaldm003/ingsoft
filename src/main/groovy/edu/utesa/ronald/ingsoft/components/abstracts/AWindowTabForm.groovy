package edu.utesa.ronald.ingsoft.components.abstracts

import com.vaadin.server.Sizeable
import com.vaadin.ui.GridLayout
import com.vaadin.ui.Panel
import edu.utesa.ronald.ingsoft.models.LoginManager
import edu.utesa.ronald.ingsoft.ui.components.forms.FormTabAction
import groovy.transform.CompileStatic

/**
 *  Created by ronald on 7/8/16.
 */
@CompileStatic
abstract class AWindowTabForm extends Panel {

    protected GridLayout mainLayout
    public FormTabAction formTabAction

    protected LoginManager loginManager = null
    protected Boolean edit = null

    protected void initLayout(int columns, int rows) {
        mainLayout = new GridLayout(columns, rows)
        mainLayout.setWidth(100, Sizeable.Unit.PERCENTAGE)
        mainLayout.setSpacing(true)
        buildLayout()
    }

    protected void showLayout() {
        security()
        setContent(mainLayout)
    }

    void enableFormMenuAction() {
        formTabAction.btnDelete().setEnabled(true)
    }

    void disableFormMenuAction() {
        formTabAction.btnDelete().setEnabled(false)
    }

    void visualizeFormMenuAction() {
//        formTabAction.getCbItems().setEnabled(false)
        formTabAction.btnNew().setEnabled(false)
        formTabAction.btnDelete().setEnabled(false)
        formTabAction.btnNew().setEnabled(false)
        formTabAction.btnEdit().setEnabled(false)
    }

    protected abstract void confLayout()

    protected abstract void buildLayout()

    protected abstract void restore()

    protected abstract void visualize()

    protected abstract void security()

    protected abstract boolean getElementsValid()

    protected abstract void collectData()

}
