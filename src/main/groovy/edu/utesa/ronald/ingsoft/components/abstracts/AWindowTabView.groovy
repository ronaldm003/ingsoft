package edu.utesa.ronald.ingsoft.components.abstracts

import com.kbdunn.vaadin.addons.fontawesome.FontAwesome
import com.vaadin.addon.contextmenu.GridContextMenu
import com.vaadin.addon.contextmenu.Menu
import com.vaadin.addon.contextmenu.MenuItem
import com.vaadin.event.SelectionEvent
import com.vaadin.ui.Grid
import com.vaadin.ui.Window
import edu.utesa.ronald.ingsoft.components.GridManager
import edu.utesa.ronald.ingsoft.ui.components.TabBase
import edu.utesa.ronald.ingsoft.utils.Languages
import edu.utesa.ronald.ingsoft.utils.container.BaseContainer
import groovy.transform.CompileStatic

/**
 *  Created by ronald on 7/18/17.
 */
@CompileStatic
abstract class AWindowTabView<T, VIEW extends Window> extends AWindowTab<T> {

    TabBase tabBase = new TabBase()

    protected T select
    protected GridContextMenu gridContextMenu

    AWindowTabView() {
        super(null, null)
        gridContextMenu = new GridContextMenu(tabBase.getGrid())
        tabBase.offMenu()
        confLayout(tabBase)
        configureGridMenu()
    }

    protected void windowView(VIEW window) {
        if (window) {
            getUI().addWindow(window)
        }
    }

    protected abstract VIEW onView()

    private void configureGridMenu() {
        gridContextMenu.addGridHeaderContextMenuListener(new GridContextMenu.GridContextMenuOpenListener() {
            @Override
            void onContextMenuOpen(GridContextMenu.GridContextMenuOpenListener.GridContextMenuOpenEvent event) {
                gridContextMenu.removeItems()
                addGridHeaderMenu()
            }
        })
        gridContextMenu.addGridBodyContextMenuListener(new GridContextMenu.GridContextMenuOpenListener() {
            @Override
            void onContextMenuOpen(GridContextMenu.GridContextMenuOpenListener.GridContextMenuOpenEvent event) {
                gridContextMenu.removeItems()
                if (event.itemId) {
                    gridManager.select(event.itemId)
                }
                addGridBodyMenu()
            }
        })
        gridContextMenu.addGridFooterContextMenuListener(new GridContextMenu.GridContextMenuOpenListener() {
            @Override
            void onContextMenuOpen(GridContextMenu.GridContextMenuOpenListener.GridContextMenuOpenEvent event) {
                gridContextMenu.removeItems()
                addGridFooterMenu()
            }
        })
    }

    protected final void baseViewMenu() {
        gridContextMenu.addItem(Languages.instance.getText("window.action.view"), FontAwesome.BELL,
                new Menu.Command() {
                    @Override
                    void menuSelected(MenuItem selectedItem) {
                        if (gridManager.haveSelection()) {
                            windowView(onView())
                        }
                    }
                })
    }

    protected final void baseMenu() {
        if (gridManager.haveSelection()) {
            baseViewMenu()
        }
    }

    protected void addGridHeaderMenu() {
        baseMenu()
    }

    protected void addGridBodyMenu() {
        baseMenu()
    }

    protected void addGridFooterMenu() {
        baseMenu()
    }

    protected void manageGrid(BaseContainer container) {
        manageGrid(tabBase.grid, container)
    }

    @Override
    protected void manageGrid(Grid grid, BaseContainer container) {
        gridManager = new GridManager(grid, container)
        gridManager.getGrid().addSelectionListener(new SelectionEvent.SelectionListener() {
            @Override
            void select(SelectionEvent event) {
                select = (T) gridManager.grid.getSelectedRow()
            }
        })
    }

    @Override
    T getValid() {
        return null
    }

    @Override
    protected void collectData() {
    }

    @Override
    protected boolean getElementsValid() {
        return false
    }

    @Override
    protected void visualize() {
    }

    @Override
    protected void restore() {
    }

    @Override
    protected void security() {
    }

    @Override
    void startFocus() {
    }

    @Override
    protected void confLanguage() {
    }
}
