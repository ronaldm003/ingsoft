package edu.utesa.ronald.ingsoft.domains.admin

import edu.utesa.ronald.ingsoft.utils.BaseDB
import grails.persistence.Entity

/**
 *
 *  Created by ronald on 07/26/17.
 */
@SuppressWarnings(["GroovyUnusedDeclaration", "ChangeToOperator"])
@Entity
class Cycle extends BaseDB {

    String code
    String description
    Date startDate
    Date endDate

    static hasMany = [listSubjects: Subject]

    static constraints = {
        description nullable: true
    }

    static mapping = {
        table "adm_cycle"

        listSubjects lazy: false

        description sqlType: "text"
    }

    @Override
    boolean equals(Object o) {
        if (!o) {
            return false
        } else if (this.is(o)) {
            return true
        } else if (!this.getClass().equals(o.getClass())) {
            return false
        }
        return this.id == ((Cycle) o).id
    }

    @Override
    int hashCode() {
        if (id) {
            return id.hashCode()
        }
        return super.hashCode()
    }

    @Override
    String toString() {
        return code
    }
}
