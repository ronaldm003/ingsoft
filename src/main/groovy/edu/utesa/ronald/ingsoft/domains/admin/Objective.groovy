package edu.utesa.ronald.ingsoft.domains.admin

import edu.utesa.ronald.ingsoft.domains.control.Article
import edu.utesa.ronald.ingsoft.utils.BaseDB
import grails.persistence.Entity

import java.sql.Time

/**
 * Los objetivos son los procedimientos que debe de implementar el estudiante y los mismo tienen limites,
 * una lista de materiales para el objetivo en especifico y
 *  Created by ronald on 07/26/17.
 */
@SuppressWarnings(["GroovyUnusedDeclaration", "ChangeToOperator"])
@Entity
class Objective extends BaseDB {

    String description
    Date limitDate
    Long procedureQuantity = 0 // unlimited
    Time averageTime = new Time(0, 45, 0) // eso es en minutos

    static hasMany = [listArticle: Article]

    static constraints = {
        limitDate nullable: true
        averageTime nullable: true
    }

    static mapping = {
        table "adm_objectives"

        description sqlType: "text"

        listArticle lazy: false
    }

    @Override
    boolean equals(Object o) {
        if (!o) {
            return false
        } else if (this.is(o)) {
            return true
        } else if (!this.getClass().equals(o.getClass())) {
            return false
        }
        return this.id == ((Objective) o).id
    }

    @Override
    int hashCode() {
        if (id) {
            return id.hashCode()
        }
        return super.hashCode()
    }

}
