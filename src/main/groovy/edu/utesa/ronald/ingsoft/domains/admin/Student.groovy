package edu.utesa.ronald.ingsoft.domains.admin

import edu.utesa.ronald.ingsoft.domains.security.Person
import edu.utesa.ronald.ingsoft.utils.BaseDB
import grails.persistence.Entity

/**
 *
 *  Created by ronald on 07/26/17.
 */
@SuppressWarnings(["GroovyUnusedDeclaration", "ChangeToOperator"])
@Entity
class Student extends BaseDB {

    Person person
    String enrollment  // matricula

    static hasMany = [listSubjects: Subject]

    static constraints = {
    }

    static mapping = {
        table "adm_student"

        listSubjects lazy: false
    }

    @Override
    boolean equals(Object o) {
        if (!o) {
            return false
        } else if (this.is(o)) {
            return true
        } else if (!this.getClass().equals(o.getClass())) {
            return false
        }
        return this.id == ((Student) o).id
    }

    @Override
    int hashCode() {
        if (id) {
            return id.hashCode()
        }
        return super.hashCode()
    }

    @Override
    String toString() {
        return enrollment + ":" + person?.names()
    }

}
