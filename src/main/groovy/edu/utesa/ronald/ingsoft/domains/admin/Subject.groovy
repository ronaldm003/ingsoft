package edu.utesa.ronald.ingsoft.domains.admin

import edu.utesa.ronald.ingsoft.utils.BaseDB
import grails.persistence.Entity

/**
 *
 *  Created by ronald on 07/26/17.
 */
@SuppressWarnings(["GroovyUnusedDeclaration", "ChangeToOperator"])
@Entity
class Subject extends BaseDB {

    String code
    String description
    Long credits = 0
    Long teoricHours = 0
    Long practicalHours = 0

    static hasMany = [listObjectives: Objective, listSubjectSchedule: SubjectSchedule]

    static constraints = {
        description nullable: true
    }

    static mapping = {
        table "adm_subject"

        description sqlType: "text"

        listObjectives lazy: false
        listSubjectSchedule lazy: false
    }

    @Override
    boolean equals(Object o) {
        if (!o) {
            return false
        } else if (this.is(o)) {
            return true
        } else if (!this.getClass().equals(o.getClass())) {
            return false
        }
        return this.id == ((Subject) o).id
    }

    @Override
    int hashCode() {
        if (id) {
            return id.hashCode()
        }
        return super.hashCode()
    }

    @Override
    String toString() {
        return code + ":" + description
    }
}
