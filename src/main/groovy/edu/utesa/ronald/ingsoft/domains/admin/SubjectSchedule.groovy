package edu.utesa.ronald.ingsoft.domains.admin

import edu.utesa.ronald.ingsoft.utils.BaseDB
import edu.utesa.ronald.ingsoft.utils.Languages
import grails.persistence.Entity

import java.sql.Time

/**
 *
 *  Created by ronald on 07/26/17.
 */
@SuppressWarnings(["GroovyUnusedDeclaration", "ChangeToOperator"])
@Entity
class SubjectSchedule extends BaseDB {

    // 1-7 (1 = sunday , .., 7 = saturday)
    int day

    Time startTime
    Time endTime

    static constraints = {
    }

    static mapping = {
        table "adm_subject_sche"

    }

    @Override
    boolean equals(Object o) {
        if (!o) {
            return false
        } else if (this.is(o)) {
            return true
        } else if (!this.getClass().equals(o.getClass())) {
            return false
        }
        return this.id == ((SubjectSchedule) o).id
    }

    @Override
    int hashCode() {
        if (id) {
            return id.hashCode()
        }
        return super.hashCode()
    }

    @Override
    String toString() {
        return Languages.instance.dayOfWeek(day) + "<:>" + startTime + "<:>" + endTime

    }
}
