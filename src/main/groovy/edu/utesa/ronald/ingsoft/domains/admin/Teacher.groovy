package edu.utesa.ronald.ingsoft.domains.admin

import edu.utesa.ronald.ingsoft.domains.security.Person
import edu.utesa.ronald.ingsoft.utils.BaseDB
import grails.persistence.Entity

/**
 *
 *  Created by ronald on 07/26/17.
 */
@SuppressWarnings(["GroovyUnusedDeclaration", "ChangeToOperator"])
@Entity
class Teacher extends BaseDB {

    String code
    Person person

    static hasMany = [listSubjects: Subject]

    static constraints = {
    }

    static mapping = {
        table "adm_teacher"

        listSubjects lazy: false
    }

    @Override
    boolean equals(Object o) {
        if (!o) {
            return false
        } else if (this.is(o)) {
            return true
        } else if (!this.getClass().equals(o.getClass())) {
            return false
        }
        return this.id == ((Teacher) o).id
    }

    @Override
    int hashCode() {
        if (id) {
            return id.hashCode()
        }
        return super.hashCode()
    }

    @Override
    String toString() {
        return code + ":" + person.names()
    }
}
