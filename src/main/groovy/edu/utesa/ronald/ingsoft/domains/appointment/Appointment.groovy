package edu.utesa.ronald.ingsoft.domains.appointment

import edu.utesa.ronald.ingsoft.domains.admin.Objective
import edu.utesa.ronald.ingsoft.domains.admin.Student
import edu.utesa.ronald.ingsoft.domains.admin.Teacher
import edu.utesa.ronald.ingsoft.domains.control.Patient
import edu.utesa.ronald.ingsoft.utils.BaseDB
import grails.persistence.Entity

/**
 *  Created by ronald on 7/28/16.
 */
@SuppressWarnings(["GroovyUnusedDeclaration", "ChangeToOperator"])
@Entity
class Appointment extends BaseDB {

    Patient patient
    Student student
    Teacher teacher

    Date start
    Date end

    String name
    String description

    Objective objective

    AppointmentType appointmentType

    static constraints = {
        appointmentType nullable: true
        name nullable: true
        description nullable: true
    }

    static mapping = {
        table "med_apo"

    }

    @Override
    boolean equals(Object o) {
        if (!o) {
            return false
        } else if (this.is(o)) {
            return true
        } else if (!this.getClass().equals(o.getClass())) {
            return false
        }
        return this.id == ((Appointment) o).id
    }

    @Override
    int hashCode() {
        if (id) {
            return id.hashCode()
        }
        return super.hashCode()
    }

    @Override
    String toString() {
        return name ?: "" + " : " + description ?: ""
    }

}
