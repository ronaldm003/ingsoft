package edu.utesa.ronald.ingsoft.domains.control

import edu.utesa.ronald.ingsoft.utils.BaseDB
import grails.persistence.Entity

/**
 *
 *  Created by ronald on 07/26/17.
 */
@SuppressWarnings(["GroovyUnusedDeclaration", "ChangeToOperator"])
@Entity
class Article extends BaseDB {

    Long code
    String description
    Long quantity
    Long punceo
    BigDecimal cost

    static constraints = {
    }

    static mapping = {
        table "con_articles"
    }

    @Override
    boolean equals(Object o) {
        if (!o) {
            return false
        } else if (this.is(o)) {
            return true
        } else if (!this.getClass().equals(o.getClass())) {
            return false
        }
        return this.id == ((Article) o).id
    }

    @Override
    int hashCode() {
        if (id) {
            return id.hashCode()
        }
        return super.hashCode()
    }

}
