package edu.utesa.ronald.ingsoft.domains.control

import edu.utesa.ronald.ingsoft.domains.admin.Student
import edu.utesa.ronald.ingsoft.domains.admin.Teacher
import edu.utesa.ronald.ingsoft.utils.BaseDB
import grails.persistence.Entity

/**
 * Esto aqui es la parte de la receta
 *  Created by ronald on 07/26/17.
 */
@SuppressWarnings(["GroovyUnusedDeclaration", "ChangeToOperator"])
@Entity
class Prescription extends BaseDB {

    Student student
    Patient patient
    Teacher teacher
    Date date
    String indications

    static constraints = {
        teacher nullable: true
        indications nullable: true
    }

    static mapping = {
        table "con_prescript"

        indications sqlType: "text"
    }

    @Override
    boolean equals(Object o) {
        if (!o) {
            return false
        } else if (this.is(o)) {
            return true
        } else if (!this.getClass().equals(o.getClass())) {
            return false
        }
        return this.id == ((Prescription) o).id
    }

    @Override
    int hashCode() {
        if (id) {
            return id.hashCode()
        }
        return super.hashCode()
    }

}
