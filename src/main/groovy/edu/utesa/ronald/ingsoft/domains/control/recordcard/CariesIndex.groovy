package edu.utesa.ronald.ingsoft.domains.control.recordcard

import edu.utesa.ronald.ingsoft.utils.BaseDB
import grails.persistence.Entity

/**
 *  Created by ronald on 8/8/16.
 */
@SuppressWarnings(["GroovyUnusedDeclaration", "ChangeToOperator"])
@Entity
class CariesIndex extends BaseDB {

    String decayed
    String lost
    String sealed
    String teethPresent
    String level
    String groupQuantity
    Date date

    static constraints = {
        decayed nullable: true
        lost nullable: true
        sealed nullable: true
        teethPresent nullable: true
        level nullable: true
        groupQuantity nullable: true
        date nullable: true
    }

    static mapping = {
        table "car_ind"

        decayed sqlType: 'text'
        lost sqlType: 'text'
        sealed sqlType: 'text'
        teethPresent sqlType: 'text'
        level sqlType: 'text'
        groupQuantity sqlType: 'text'

    }

    @Override
    boolean equals(Object o) {
        if (!o) {
            return false
        } else if (this.is(o)) {
            return true
        } else if (!this.getClass().equals(o.getClass())) {
            return false
        }
        return this.id == ((CariesIndex) o).id
    }

    @Override
    int hashCode() {
        if (id) {
            return id.hashCode()
        }
        return super.hashCode()
    }

}
