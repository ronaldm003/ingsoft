package edu.utesa.ronald.ingsoft.domains.control.recordcard

import edu.utesa.ronald.ingsoft.utils.BaseDB
import grails.persistence.Entity

/**
 *  Created by ronald on 8/8/16.
 */
@SuppressWarnings(["GroovyUnusedDeclaration", "ChangeToOperator"])
@Entity
class ExtraBuccal extends BaseDB implements Comparable<ExtraBuccal> {

    Long code = 0
    String question

    static constraints = {
    }

    static mapping = {
        table "cont_rec_extbuc"

        question sqlType: "Text"
    }

    @Override
    int compareTo(ExtraBuccal obj) {
        if (this.code == obj.code) {
            return 0
        } else if (this.code > obj.code) {
            return 1
        }
        return -1
    }

    @Override
    boolean equals(Object o) {
        if (!o) {
            return false
        } else if (this.is(o)) {
            return true
        } else if (!this.getClass().equals(o.getClass())) {
            return false
        }
        return this.id == ((ExtraBuccal) o).id
    }

    @Override
    int hashCode() {
        if (id) {
            return id.hashCode()
        }
        return super.hashCode()
    }

}
