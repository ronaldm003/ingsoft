package edu.utesa.ronald.ingsoft.domains.control.recordcard

import edu.utesa.ronald.ingsoft.utils.BaseDB
import grails.persistence.Entity

/**
 *  Created by ronald on 8/8/16.
 */
@SuppressWarnings(["GroovyUnusedDeclaration", "ChangeToOperator"])
@Entity
class IntraBuccal extends BaseDB implements Comparable<IntraBuccal> {

    Long code = 0
    String question

    static constraints = {
    }

    static mapping = {
        table "cont_rec_intbuc"

        question sqlType: "Text"
    }

    @Override
    int compareTo(IntraBuccal obj) {
        if (this.code == obj.code) {
            return 0
        } else if (this.code > obj.code) {
            return 1
        }
        return -1
    }

    @Override
    boolean equals(Object o) {
        if (!o) {
            return false
        } else if (this.is(o)) {
            return true
        } else if (!this.getClass().equals(o.getClass())) {
            return false
        }
        return this.id == ((IntraBuccal) o).id
    }

    @Override
    int hashCode() {
        if (id) {
            return id.hashCode()
        }
        return super.hashCode()
    }

}
