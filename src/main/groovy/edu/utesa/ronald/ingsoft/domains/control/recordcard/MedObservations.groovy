package edu.utesa.ronald.ingsoft.domains.control.recordcard

import edu.utesa.ronald.ingsoft.utils.BaseDB
import grails.persistence.Entity

/**
 *
 *  Created by ronald on 07/26/17.
 */
@SuppressWarnings(["GroovyUnusedDeclaration", "ChangeToOperator"])
@Entity
class MedObservations extends BaseDB {

    String observations
    String actualMedicaments
    String medicamentName
    String medicamentFor
    String whoIndicateIt

    static constraints = {
        observations nullable: true
        actualMedicaments nullable: true
        medicamentName nullable: true
        medicamentFor nullable: true
        whoIndicateIt nullable: true
    }

    static mapping = {
        table "con_med_obser"

        observations sqlType: "text"
        actualMedicaments sqlType: "text"
        medicamentName sqlType: "text"
        medicamentFor sqlType: "text"
        whoIndicateIt sqlType: "text"
    }

    @Override
    boolean equals(Object o) {
        if (!o) {
            return false
        } else if (this.is(o)) {
            return true
        } else if (!this.getClass().equals(o.getClass())) {
            return false
        }
        return this.id == ((MedObservations) o).id
    }

    @Override
    int hashCode() {
        if (id) {
            return id.hashCode()
        }
        return super.hashCode()
    }

    @Override
    String toString() {
        return description
    }

}
