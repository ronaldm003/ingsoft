package edu.utesa.ronald.ingsoft.domains.control.recordcard

import edu.utesa.ronald.ingsoft.models.enums.recordcard.ArcType
import edu.utesa.ronald.ingsoft.models.enums.recordcard.FacialProfile
import edu.utesa.ronald.ingsoft.utils.BaseDB
import grails.persistence.Entity

/**
 *  Created by ronald on 8/8/16.
 */
@SuppressWarnings(["GroovyUnusedDeclaration", "ChangeToOperator"])
@Entity
class Occlusion extends BaseDB {

    FacialProfile facialProfile// Recto , convexo,Concavo
    String molarRelation
    String canineRelation
    String eruptionSecuence
    String crossBite
    String supernumeraries
    String congenitalAbsences
    String primateSpaces
    String middleLine
    String mandibularDeviation
    String openBite
    String overBite
    String openJet
    ArcType arcType// triangular , oval , cuadrado
    String maxilarSup // Adecuada, inadecuada
    String maxilarInf // Adecuada, inadecuada

    static constraints = {
        facialProfile nullable: true
        molarRelation nullable: true
        canineRelation nullable: true
        eruptionSecuence nullable: true
        crossBite nullable: true
        supernumeraries nullable: true
        congenitalAbsences nullable: true
        primateSpaces nullable: true
        middleLine nullable: true
        mandibularDeviation nullable: true
        openBite nullable: true
        overBite nullable: true
        openJet nullable: true
        arcType nullable: true
        maxilarSup nullable: true
        maxilarInf nullable: true
    }

    static mapping = {
        table "cont_rec_occlu"

        molarRelation sqlType: 'text'
        canineRelation sqlType: 'text'
        eruptionSecuence sqlType: 'text'
        crossBite sqlType: 'text'
        supernumeraries sqlType: 'text'
        congenitalAbsences sqlType: 'text'
        primateSpaces sqlType: 'text'
        middleLine sqlType: 'text'
        mandibularDeviation sqlType: 'text'
        openBite sqlType: 'text'
        overBite sqlType: 'text'
        openJet sqlType: 'text'
        arcType sqlType: 'text'
        maxilarSup sqlType: 'text'
        maxilarInf sqlType: 'text'

    }

    @Override
    boolean equals(Object o) {
        if (!o) {
            return false
        } else if (this.is(o)) {
            return true
        } else if (!this.getClass().equals(o.getClass())) {
            return false
        }
        return this.id == ((Occlusion) o).id
    }

    @Override
    int hashCode() {
        if (id) {
            return id.hashCode()
        }
        return super.hashCode()
    }

}
