package edu.utesa.ronald.ingsoft.domains.control.recordcard

import edu.utesa.ronald.ingsoft.utils.BaseDB
import grails.persistence.Entity

/**
 *  Created by ronald on 8/8/16.
 */
@SuppressWarnings(["GroovyUnusedDeclaration", "ChangeToOperator"])
@Entity
class OralHygiene extends BaseDB {

    String brushed
    String floss
    String mouthwashes
    String sanitizeTongue

    static hasMany = [listPatientOralIllness: PatientOralIllness]

    static constraints = {
        brushed nullable: true
        floss nullable: true
        mouthwashes nullable: true
        sanitizeTongue nullable: true

    }

    static mapping = {
        table "cont_rec_oral_hy"

        brushed sqlType: 'text'
        floss sqlType: 'text'
        mouthwashes sqlType: 'text'
        sanitizeTongue sqlType: 'text'

        listPatientOralIllness lazy: true

    }

    @Override
    boolean equals(Object o) {
        if (!o) {
            return false
        } else if (this.is(o)) {
            return true
        } else if (!this.getClass().equals(o.getClass())) {
            return false
        }
        return this.id == ((OralHygiene) o).id
    }

    @Override
    int hashCode() {
        if (id) {
            return id.hashCode()
        }
        return super.hashCode()
    }

}
