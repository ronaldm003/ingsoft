package edu.utesa.ronald.ingsoft.domains.control.recordcard

import edu.utesa.ronald.ingsoft.utils.BaseDB
import grails.persistence.Entity

/**
 *  Created by ronald on 8/8/16.
 */
@SuppressWarnings(["GroovyUnusedDeclaration", "ChangeToOperator"])
@Entity
class OralHygieneIndex extends BaseDB {

    Long code
    String tooth
    String vestib16
    String vestib11
    String vestib26
    String ling36
    String vestib31
    String ling46
    String total
    String indexNumber
    String hygeneLevel
    Date date

    static constraints = {
        code nullable: true
        tooth nullable: true
        vestib16 nullable: true
        vestib11 nullable: true
        vestib26 nullable: true
        ling36 nullable: true
        vestib31 nullable: true
        ling46 nullable: true
        total nullable: true
        indexNumber nullable: true
        hygeneLevel nullable: true
        date nullable: true

    }

    static mapping = {
        table "con_or_hy_ind"

        tooth sqlType: 'text'
        vestib16 sqlType: 'text'
        vestib11 sqlType: 'text'
        vestib26 sqlType: 'text'
        ling36 sqlType: 'text'
        vestib31 sqlType: 'text'
        ling46 sqlType: 'text'
        total sqlType: 'text'
        indexNumber sqlType: 'text'
        hygeneLevel sqlType: 'text'

    }

    @Override
    boolean equals(Object o) {
        if (!o) {
            return false
        } else if (this.is(o)) {
            return true
        } else if (!this.getClass().equals(o.getClass())) {
            return false
        }
        return this.id == ((OralHygieneIndex) o).id
    }

    @Override
    int hashCode() {
        if (id) {
            return id.hashCode()
        }
        return super.hashCode()
    }

}
