package edu.utesa.ronald.ingsoft.domains.control.recordcard

import edu.utesa.ronald.ingsoft.utils.BaseDB
import grails.persistence.Entity

/**
 *  Created by ronald on 7/4/16.
 */
@SuppressWarnings(["GroovyUnusedDeclaration", "ChangeToOperator"])
@Entity
class PatientEvaluationAnswer extends BaseDB implements Comparable<PatientEvaluationAnswer> {

    EvaluationQuestion evaluationQuestion
    String answer

    static constraints = {
        answer nullable: true
    }

    static mapping = {
        table "con_rec_eva_ans"

        evaluationQuestion cascade: "all"
        answer sqlType: "Text"
    }

    @Override
    int compareTo(PatientEvaluationAnswer obj) {
        if (this.id == obj.id) {
            return 0
        } else if (this.id < obj.id) {
            return 1
        }
        return -1
    }

    @Override
    boolean equals(Object o) {
        if (!o) {
            return false
        } else if (this.is(o)) {
            return true
        } else if (!this.getClass().equals(o.getClass())) {
            return false
        }
        return this.id == ((PatientEvaluationAnswer) o).id
    }

    @Override
    int hashCode() {
        if (id) {
            return id.hashCode()
        }
        return super.hashCode()
    }

}
