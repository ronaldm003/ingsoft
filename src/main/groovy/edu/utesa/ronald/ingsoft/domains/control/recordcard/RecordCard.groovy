package edu.utesa.ronald.ingsoft.domains.control.recordcard

import edu.utesa.ronald.ingsoft.domains.admin.Cycle
import edu.utesa.ronald.ingsoft.domains.admin.Student
import edu.utesa.ronald.ingsoft.domains.admin.Teacher
import edu.utesa.ronald.ingsoft.domains.control.Patient
import edu.utesa.ronald.ingsoft.domains.utils.Occupation
import edu.utesa.ronald.ingsoft.models.enums.Behavior
import edu.utesa.ronald.ingsoft.models.enums.Complexion
import edu.utesa.ronald.ingsoft.utils.BaseDB
import grails.persistence.Entity

/**
 *
 *  Esto es para el llenado de las fichas
 *  Created by ronald on 07/26/17.
 */
@SuppressWarnings(["GroovyUnusedDeclaration", "ChangeToOperator"])
@Entity
class RecordCard extends BaseDB {

    Cycle cycle
    Long recordId

    Date ingressDate

    Teacher teacher
    Student student
    Patient patient

    Occlusion occlusion

    String fatherName
    Occupation fatherOccupation
    String motherName
    Occupation motherOccupation

    String medicalAlerts
    String parentsLiveTogether
    String brothers
    String pediatrician
    String school
    Behavior behavior // Cooperador,Agresivo,Temeroso,COnfiado
    Complexion complexion // Normal,Delgado,Sobrepeso,obeso
    String height
    String weight
    String icm
    OralHygiene oralHygiene

    static hasMany = [listPatientIllness          : PatientIllness,
                      listPatientEvaluationAnswer : PatientEvaluationAnswer,
                      listPatientExtraBuccalAnswer: PatientExtraBuccalAnswer,
                      listPatientIntraBuccalAnswer: PatientIntraBuccalAnswer,
                      listToothGroup              : ToothGroup,
                      listMedObservations         : MedObservations,
                      listPatientHabits           : PatientHabits,
                      listOralHygieneIndex        : OralHygieneIndex,
                      listCariesIndex             : CariesIndex]

    static constraints = {
        student nullable: true
        teacher nullable: true
        recordId nullable: true
        occlusion nullable: true
        fatherName nullable: true
        fatherOccupation nullable: true
        motherName nullable: true
        motherOccupation nullable: true
        medicalAlerts nullable: true
        parentsLiveTogether nullable: true
        brothers nullable: true
        pediatrician nullable: true
        school nullable: true
        behavior nullable: true
        complexion nullable: true
        height nullable: true
        weight nullable: true
        icm nullable: true
        oralHygiene nullable: true
    }

    static mapping = {
        table "rec_card"

        listPatientIllness lazy: false
        listPatientEvaluationAnswer lazy: false
        listPatientExtraBuccalAnswer lazy: false
        listPatientIntraBuccalAnswer lazy: false
        listToothGroup lazy: false
        listMedObservations lazy: false
        listPatientHabits lazy: false
        listOralHygieneIndex lazy: false
        listCariesIndex lazy: false

    }

    @Override
    boolean equals(Object o) {
        if (!o) {
            return false
        } else if (this.is(o)) {
            return true
        } else if (!this.getClass().equals(o.getClass())) {
            return false
        }
        return this.id == ((RecordCard) o).id
    }

    @Override
    int hashCode() {
        if (id) {
            return id.hashCode()
        }
        return super.hashCode()
    }

}
