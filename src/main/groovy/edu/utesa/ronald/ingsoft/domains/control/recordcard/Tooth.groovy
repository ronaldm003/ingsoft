package edu.utesa.ronald.ingsoft.domains.control.recordcard

import edu.utesa.ronald.ingsoft.utils.BaseDB
import grails.persistence.Entity

/**
 *
 *  Esto es para el llenado de las fichas
 *  Created by ronald on 07/26/17.
 */
@SuppressWarnings(["GroovyUnusedDeclaration", "ChangeToOperator"])
@Entity
class Tooth extends BaseDB {

    String number
    String cOclusal
    String cDistal
    String cVestibular
    String cMesial
    String cPalatinaSup
    String cLingualInf
    boolean child = false

    static constraints = {
        cOclusal nullable: true
        cDistal nullable: true
        cVestibular nullable: true
        cMesial nullable: true
        cPalatinaSup nullable: true
        cLingualInf nullable: true

    }

    static mapping = {
        table "con_rec_tooth"

        number sqlType: 'text'
        cOclusal sqlType: 'text'
        cDistal sqlType: 'text'
        cVestibular sqlType: 'text'
        cMesial sqlType: 'text'
        cPalatinaSup sqlType: 'text'
        cLingualInf sqlType: 'text'

    }

    @Override
    boolean equals(Object o) {
        if (!o) {
            return false
        } else if (this.is(o)) {
            return true
        } else if (!this.getClass().equals(o.getClass())) {
            return false
        }
        return this.id == ((Tooth) o).id
    }

    @Override
    int hashCode() {
        if (id) {
            return id.hashCode()
        }
        return super.hashCode()
    }

}
