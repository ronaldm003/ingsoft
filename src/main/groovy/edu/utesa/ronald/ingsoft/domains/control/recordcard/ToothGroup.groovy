package edu.utesa.ronald.ingsoft.domains.control.recordcard

import edu.utesa.ronald.ingsoft.utils.BaseDB
import grails.persistence.Entity

/**
 *  Created by ronald on 7/8/16.
 */
@SuppressWarnings(["GroovyUnusedDeclaration", "ChangeToOperator"])
@Entity
class ToothGroup extends BaseDB {

    String name
    String description

    boolean admin = false

    static hasMany = [listTooths: Tooth]

    static constraints = {
        description nullable: true
    }

    static mapping = {
        table "tooth_grp"

        listTooths lazy: false
    }

    @Override
    boolean equals(Object o) {
        if (!o) {
            return false
        } else if (this.is(o)) {
            return true
        } else if (!this.getClass().equals(o.getClass())) {
            return false
        }
        return this.id == ((ToothGroup) o).id
    }

    @Override
    int hashCode() {
        if (id) {
            return id.hashCode()
        }
        return super.hashCode()
    }

}
