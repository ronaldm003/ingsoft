package edu.utesa.ronald.ingsoft.domains.location

import edu.utesa.ronald.ingsoft.domains.control.Equipment
import edu.utesa.ronald.ingsoft.domains.control.Inventory
import edu.utesa.ronald.ingsoft.utils.BaseDB
import grails.gorm.annotation.Entity

/**
 *  Aqui se manejan 2 turnos diarios.
 *  Created by ronald on 3/6/17.
 */
@SuppressWarnings(["GroovyUnusedDeclaration", "ChangeToOperator"])
@Entity
class Clinic extends BaseDB {

    Long code
    String description
    Location location
    Inventory inventory

    static hasMany = [listEquipments: Equipment]

    static constraints = {
        code unique: true
        description nullable: true
        location nullable: true
        inventory nullable: true
    }

    static mapping = {
        table "loc_clinic"

        description sqlType: "text"

        listEquipments lazy: false
    }

    @Override
    boolean equals(Object o) {
        if (!o) {
            return false
        } else if (this.is(o)) {
            return true
        } else if (!this.getClass().equals(o.getClass())) {
            return false
        }
        return this.id == ((Clinic) o).id
    }

    @Override
    int hashCode() {
        if (id) {
            return id.hashCode()
        }
        return super.hashCode()
    }

    @Override
    String toString() {
        return description
    }

}
