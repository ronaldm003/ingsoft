package edu.utesa.ronald.ingsoft.domains.location

import edu.utesa.ronald.ingsoft.utils.BaseDB
import grails.gorm.annotation.Entity

/**
 *  Created by ronald on 3/6/17.
 */
@SuppressWarnings(["GroovyUnusedDeclaration", "ChangeToOperator"])
@Entity
class Location extends BaseDB {

    Long code
    String description
    Long longitude
    Long latitude

    static constraints = {
        code nullable: true
        description nullable: true
        longitude nullable: true
        latitude nullable: true
    }

    static mapping = {
        table "loc_locations"

        description sqlType: "text"
    }

    @Override
    boolean equals(Object o) {
        if (!o) {
            return false
        } else if (this.is(o)) {
            return true
        } else if (!this.getClass().equals(o.getClass())) {
            return false
        }
        return this.id == ((Location) o).id
    }

    @Override
    int hashCode() {
        if (id) {
            return id.hashCode()
        }
        return super.hashCode()
    }

    @Override
    String toString() {
        return description
    }

}
