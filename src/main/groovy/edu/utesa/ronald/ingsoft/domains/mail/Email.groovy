package edu.utesa.ronald.ingsoft.domains.mail

import edu.utesa.ronald.ingsoft.utils.BaseDB
import grails.persistence.Entity

/**
 * Usando el API puedo rastrear los correos enviados.
 *
 *  Created by ronald on 07/26/17.
 */
@SuppressWarnings(["GroovyUnusedDeclaration", "ChangeToOperator"])
@Entity
class Email extends BaseDB {

    String emailID
    String message

    static constraints = {
    }

    static mapping = {
        table "mail_emails"
    }

    @Override
    boolean equals(Object o) {
        if (!o) {
            return false
        } else if (this.is(o)) {
            return true
        } else if (!this.getClass().equals(o.getClass())) {
            return false
        }
        return this.id == ((Email) o).id
    }

    @Override
    int hashCode() {
        if (id) {
            return id.hashCode()
        }
        return super.hashCode()
    }

}
