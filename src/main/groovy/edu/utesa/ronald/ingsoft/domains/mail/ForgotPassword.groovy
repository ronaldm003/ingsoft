package edu.utesa.ronald.ingsoft.domains.mail

import edu.utesa.ronald.ingsoft.utils.BaseDB
import grails.persistence.Entity

/**
 * Tabla para los ID unicos generados para recuperar los password.
 *
 * Elimino según se usen.
 *
 *  Created by ronald on 07/28/17.
 */
@SuppressWarnings(["GroovyUnusedDeclaration", "ChangeToOperator"])
@Entity
class ForgotPassword extends BaseDB {

    String keyLost
    String email
    Date requestDate

    static constraints = {
        keyLost unique: true
    }

    static mapping = {
        table "mail_forgot_pass"

        keyLost column: 'key_lost', index: 'forgot_passwords_key_lost_email_idx'
        email column: 'email', index: 'forgot_passwords_key_lost_email_idx'
        enabled column: 'enabled', index: 'forgot_passwords_enabled_idx'
    }

    @Override
    boolean equals(Object o) {
        if (!o) {
            return false
        } else if (this.is(o)) {
            return true
        } else if (!this.getClass().equals(o.getClass())) {
            return false
        }
        return this.id == ((ForgotPassword) o).id
    }

    @Override
    int hashCode() {
        if (id) {
            return id.hashCode()
        }
        return super.hashCode()
    }

}
