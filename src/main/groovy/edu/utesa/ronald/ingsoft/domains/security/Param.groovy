package edu.utesa.ronald.ingsoft.domains.security

import edu.utesa.ronald.ingsoft.models.params.ParamValueType
import edu.utesa.ronald.ingsoft.utils.BaseDB
import grails.gorm.annotation.Entity

/**
 *  Created by ronald on 7/4/17.
 */
@SuppressWarnings(["GroovyUnusedDeclaration", "ChangeToOperator"])
@Entity
class Param extends BaseDB {

    Integer code
    ParamValueType paramValueType

    String name
    String description
    String value

    Boolean changeRoot = false

    static constraints = {
        description nullable: true
        value maxSize: 10000
    }

    static mapping = {
        table "sec_parameters"

        description sqlType: "text"
    }

    @Override
    boolean equals(Object o) {
        if (!o) {
            return false
        } else if (this.is(o)) {
            return true
        } else if (!this.getClass().equals(o.getClass())) {
            return false
        }
        return this.id == ((Param) o).id
    }

    @Override
    int hashCode() {
        if (id) {
            return id.hashCode()
        }
        return super.hashCode()
    }


}
