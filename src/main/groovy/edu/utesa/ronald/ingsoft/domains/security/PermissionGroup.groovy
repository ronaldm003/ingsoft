package edu.utesa.ronald.ingsoft.domains.security

import edu.utesa.ronald.ingsoft.utils.BaseDB
import grails.persistence.Entity

/**
 *  Created by ronald on 7/8/16.
 */
@SuppressWarnings(["GroovyUnusedDeclaration", "ChangeToOperator"])
@Entity
class PermissionGroup extends BaseDB {

    String name
    String description

    boolean admin = false

    static hasMany = [listPermissions: Permission]

    static constraints = {
        description nullable: true
    }

    static mapping = {
        table "sec_perm_group"

        listPermissions lazy: false
    }

    Boolean havePermission(Integer code) {
        for (Permission p : listPermissions) {
            if (p.getCode() == code) {
                return true
            }
        }
        return false
    }

    @Override
    boolean equals(Object o) {
        if (!o) {
            return false
        } else if (this.is(o)) {
            return true
        } else if (!this.getClass().equals(o.getClass())) {
            return false
        }
        return this.id == ((PermissionGroup) o).id
    }

    @Override
    int hashCode() {
        if (id) {
            return id.hashCode()
        }
        return super.hashCode()
    }

}
