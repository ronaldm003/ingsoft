package edu.utesa.ronald.ingsoft.domains.security

import edu.utesa.ronald.ingsoft.domains.control.recordcard.EducationLevel
import edu.utesa.ronald.ingsoft.domains.location.Location
import edu.utesa.ronald.ingsoft.domains.utils.Occupation
import edu.utesa.ronald.ingsoft.models.enums.Gender
import edu.utesa.ronald.ingsoft.models.enums.Nationality
import edu.utesa.ronald.ingsoft.models.enums.WarrantType
import edu.utesa.ronald.ingsoft.utils.BaseDB
import grails.gorm.annotation.Entity

/**
 *  Created by ronald on 7/4/17.
 */
@SuppressWarnings(["GroovyUnusedDeclaration", "ChangeToOperator"])
@Entity
class Person extends BaseDB {

    Long code

    String name
    String lastName
    Date birthDate
    Date ingressDate
    String email
    Location location

    String phone
    String cellphone
    String warrant

    EducationLevel educationLevel
    Nationality nationality = Nationality.DOMINICAN
    WarrantType warrantType = WarrantType.ID
    Gender gender = Gender.MAN
    Occupation occupation

    static constraints = {
        code unique: true

        lastName nullable: true
        email nullable: true
        phone nullable: true
        cellphone nullable: true
        birthDate nullable: true
        ingressDate nullable: true
        educationLevel nullable: true

        occupation nullable: true
        location nullable: true
        gender nullable: true
        warrantType nullable: true
        warrant nullable: true
        nationality nullable: true
    }

    static mapping = {
        table "sec_person"

        warrant column: 'warrant', index: 'person_warrant_idx'
        phone column: 'phone', index: 'person_phone_idx'
        email column: 'email', index: 'person_email_idx'
    }

    @Override
    boolean equals(Object o) {
        if (!o) {
            return false
        } else if (this.is(o)) {
            return true
        } else if (!this.getClass().equals(o.getClass())) {
            return false
        }
        return this.id == ((Person) o).id
    }

    @Override
    int hashCode() {
        if (id) {
            return id.hashCode()
        }
        return super.hashCode()
    }

    @Override
    String toString() {
        return name ?: "" + " - " + lastName ?: "" + " - " + warrant ?: ""
    }

    String identification() {
        return name ?: "" + " - " + warrant ?: ""
    }

    String names() {
        return name ?: "" + " " + lastName ?: ""
    }

}
