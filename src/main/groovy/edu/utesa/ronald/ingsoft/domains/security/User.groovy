package edu.utesa.ronald.ingsoft.domains.security

import edu.utesa.ronald.ingsoft.models.enums.UserType
import edu.utesa.ronald.ingsoft.utils.BaseDB
import grails.gorm.annotation.Entity

/**
 *  Created by ronald on 7/4/17.
 */
@SuppressWarnings(["GroovyUnusedDeclaration", "ChangeToOperator"])
@Entity
class User extends BaseDB {

    boolean admin = false // Dice si es el dueño de la cuenta.

    Person person

    String username
    String password // Esto es sha256

    UserType userType = UserType.TEACHER

    static hasMany = [listPermission: Permission]

    static constraints = {
        username unique: true, nullable: false

    }

    static mapping = {
        table "sec_users"

        username column: 'username', index: 'user_user_pass_idx'
        password column: 'password', index: 'user_user_pass_idx'

    }

    @Override
    boolean equals(Object o) {
        if (!o) {
            return false
        } else if (this.is(o)) {
            return true
        } else if (!this.getClass().equals(o.getClass())) {
            return false
        }
        return this.id == ((User) o).id
    }

    @Override
    int hashCode() {
        if (id) {
            return id.hashCode()
        }
        return super.hashCode()
    }

    Boolean havePermission(Integer code) {
        return this.listPermission.contains(new Permission(code: code))
    }

    @Override
    String toString() {
        return username
    }

}
