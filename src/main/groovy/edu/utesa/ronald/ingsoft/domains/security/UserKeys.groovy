package edu.utesa.ronald.ingsoft.domains.security

import edu.utesa.ronald.ingsoft.utils.BaseDB
import grails.gorm.annotation.Entity

/**
 *  Created by ronald on 3/6/17.
 */
@SuppressWarnings(["GroovyUnusedDeclaration", "ChangeToOperator"])
@Entity
class UserKeys extends BaseDB {

    User user
    String authorizationKey //UUID
    User secUser
    Date date
    boolean estatus = false

    static constraints = {
        user nullable: true
        authorizationKey nullable: true
        secUser nullable: true
        date nullable: true
    }

    static mapping = {
        table "sec_user_keys"
    }

    @Override
    boolean equals(Object o) {
        if (!o) {
            return false
        } else if (this.is(o)) {
            return true
        } else if (!this.getClass().equals(o.getClass())) {
            return false
        }
        return this.id == ((UserKeys) o).id
    }

    @Override
    int hashCode() {
        if (id) {
            return id.hashCode()
        }
        return super.hashCode()
    }
}

