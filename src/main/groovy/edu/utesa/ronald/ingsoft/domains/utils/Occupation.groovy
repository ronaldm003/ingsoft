package edu.utesa.ronald.ingsoft.domains.utils

import edu.utesa.ronald.ingsoft.utils.BaseDB
import grails.gorm.annotation.Entity

/**
 *  Created by ronald on 7/4/17.
 */
@SuppressWarnings(["GroovyUnusedDeclaration", "ChangeToOperator"])
@Entity
class Occupation extends BaseDB {

    String description

    static constraints = {
        description nullable: true
    }

    static mapping = {
        table "util_occupation"

    }

    @Override
    boolean equals(Object o) {
        if (!o) {
            return false
        } else if (this.is(o)) {
            return true
        } else if (!this.getClass().equals(o.getClass())) {
            return false
        }
        return this.id == ((Occupation) o).id
    }

    @Override
    int hashCode() {
        if (id) {
            return id.hashCode()
        }
        return super.hashCode()
    }

    @Override
    String toString() {
        return description
    }

}
