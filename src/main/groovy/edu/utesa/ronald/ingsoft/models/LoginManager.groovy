package edu.utesa.ronald.ingsoft.models

import edu.utesa.ronald.ingsoft.domains.security.Permission
import edu.utesa.ronald.ingsoft.domains.security.User
import edu.utesa.ronald.ingsoft.services.security.UserService
import edu.utesa.ronald.ingsoft.utils.Constants
import groovy.transform.CompileStatic
import org.springframework.beans.factory.annotation.Autowired

/**
 *  Created by ronald on 1/16/2015.
 */
@CompileStatic
class LoginManager {

    @Autowired
    private UserService userService

    private User user

    LoginManager() {
        user = null
        Constants.instance.autoWiredClass(this)
    }

    User getUser() {
        return user
    }

    void setUser(User user) {
        this.user = user
    }

    Long getID() {
        return user.id
    }

//    Long getIdUser() {
//        return user.code
//    }

    boolean isUser() {
        if (user) {
            return true
        }
        return false
    }

    String getPassword() {
        return user.password
    }

    String getUsername() {
        return user.username
    }

//    String getPhone() {
//        return user.phone
//    }
//
//    Date getBirthday() {
//        return user.birthDate
//    }
//
//    String getEmail() {
//        return user.emails
//    }

    boolean getAdmin() {
        return user.admin
    }

//    String getName() {
//        return user.name
//    }
//
//    String getCellphone() {
//        return user.cellphone
//    }
//
//
//    Date getBirthDate() {
//        return user.birthDate
//    }
//
//    Date getIngressDate() {
//        return user.ingressDate
//    }
//
//    Gender getGender() {
//        return user.gender
//    }
//
//    Location getLocation() {
//        return user.location
//    }

    String getRegistryName() {
        if (user) {
            return user.getId() + "-" + user.username
        }
        return ""
    }

    void refresh(User user) {
        this.user = user
    }

    List<Permission> getPermissions() {
        return user.listPermission.toList()
    }

    boolean havePermission(Integer code) {
        return userService.refresh(user.id).havePermission(code)
    }
}
