package edu.utesa.ronald.ingsoft.models.containers

import edu.utesa.ronald.ingsoft.models.factories.DocumentsLocationFactory
import edu.utesa.ronald.ingsoft.ui.dashboard.components.charts.models.DocumentsLocation
import edu.utesa.ronald.ingsoft.utils.container.BaseContainer
import edu.utesa.ronald.ingsoft.utils.container.LazyQuery
import groovy.transform.CompileStatic

/**
 *  Created by ronald on 3/07/17.
 */
@CompileStatic
class DocumentsLocationContainer extends BaseContainer {

    DocumentsLocationContainer(LazyQuery<DocumentsLocation> lazyQuery) {
        super(new DocumentsLocationFactory(lazyQuery))
        addColumns()
    }

    @Override
    void addColumns() {
        addStringColumn("location")
        addStringColumn("documentsAmount")
    }
}
