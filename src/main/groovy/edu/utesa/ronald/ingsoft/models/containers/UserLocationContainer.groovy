package edu.utesa.ronald.ingsoft.models.containers

import edu.utesa.ronald.ingsoft.models.factories.UserLocationFactory
import edu.utesa.ronald.ingsoft.ui.dashboard.components.charts.models.UserLocation
import edu.utesa.ronald.ingsoft.utils.container.BaseContainer
import edu.utesa.ronald.ingsoft.utils.container.LazyQuery
import groovy.transform.CompileStatic

/**
 *  Created by ronald on 3/07/17.
 */
@CompileStatic
class UserLocationContainer extends BaseContainer {

    UserLocationContainer(LazyQuery<UserLocation> lazyQuery) {
        super(new UserLocationFactory(lazyQuery))
        addColumns()
    }

    @Override
    void addColumns() {
        addStringColumn("location")
        addStringColumn("userAmount")
    }
}
