package edu.utesa.ronald.ingsoft.models.containers.admin

import edu.utesa.ronald.ingsoft.domains.admin.Cycle
import edu.utesa.ronald.ingsoft.models.factories.admin.CycleFactory
import edu.utesa.ronald.ingsoft.utils.container.BaseContainer
import edu.utesa.ronald.ingsoft.utils.container.LazyQuery
import groovy.transform.CompileStatic

/**
 *  Created by ronald on 3/07/17.
 */
@CompileStatic
class CycleContainer extends BaseContainer {

    CycleContainer(LazyQuery<Cycle> lazyQuery) {
        super(new CycleFactory(lazyQuery))
        addColumns()
    }

    @Override
    void addColumns() {
        addStringColumn("code")
        addStringColumn("description")
    }
}
