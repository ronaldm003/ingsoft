package edu.utesa.ronald.ingsoft.models.containers.admin

import edu.utesa.ronald.ingsoft.domains.admin.Objective
import edu.utesa.ronald.ingsoft.models.factories.admin.ObjectiveFactory
import edu.utesa.ronald.ingsoft.utils.container.BaseContainer
import edu.utesa.ronald.ingsoft.utils.container.LazyQuery
import groovy.transform.CompileStatic

/**
 *  Created by ronald on 3/07/17.
 */
@CompileStatic
class ObjectiveContainer extends BaseContainer {

    ObjectiveContainer(LazyQuery<Objective> lazyQuery) {
        super(new ObjectiveFactory(lazyQuery))
        addColumns()
    }

    @Override
    void addColumns() {
        addStringColumn("description")
        addDateColumn("limitDate")
        addStringColumn("procedureQuantity")
    }
}
