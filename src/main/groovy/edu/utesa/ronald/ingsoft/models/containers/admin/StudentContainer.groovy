package edu.utesa.ronald.ingsoft.models.containers.admin

import edu.utesa.ronald.ingsoft.domains.admin.Student
import edu.utesa.ronald.ingsoft.models.factories.admin.StudentFactory
import edu.utesa.ronald.ingsoft.utils.container.BaseContainer
import edu.utesa.ronald.ingsoft.utils.container.LazyQuery
import groovy.transform.CompileStatic

/**
 *  Created by ronald on 3/07/17.
 */
@CompileStatic
class StudentContainer extends BaseContainer {

    StudentContainer(LazyQuery<Student> lazyQuery) {
        super(new StudentFactory(lazyQuery))
        addColumns()
    }

    @Override
    void addColumns() {
        addStringColumn("name")
        addStringColumn("lastName")
        addDateColumn("birthDate")
        addDateColumn("ingressDate")
        addStringColumn("email")
        addStringColumn("phone")
        addStringColumn("warrant")
        addStringColumn("enrollment")
    }
}
