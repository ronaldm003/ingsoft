package edu.utesa.ronald.ingsoft.models.containers.admin

import edu.utesa.ronald.ingsoft.domains.admin.Subject
import edu.utesa.ronald.ingsoft.models.factories.admin.SubjectFactory
import edu.utesa.ronald.ingsoft.utils.container.BaseContainer
import edu.utesa.ronald.ingsoft.utils.container.LazyQuery
import groovy.transform.CompileStatic

/**
 *  Created by ronald on 3/07/17.
 */
@CompileStatic
class SubjectContainer extends BaseContainer {

    SubjectContainer(LazyQuery<Subject> lazyQuery) {
        super(new SubjectFactory(lazyQuery))
        addColumns()
    }

    @Override
    void addColumns() {
        addStringColumn("code")
        addStringColumn("description")
        addStringColumn("credits")
        addStringColumn("teoricHours")
        addStringColumn("practicalHours")
    }
}
