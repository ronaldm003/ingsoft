package edu.utesa.ronald.ingsoft.models.containers.admin

import edu.utesa.ronald.ingsoft.domains.admin.SubjectSchedule
import edu.utesa.ronald.ingsoft.models.factories.admin.SubjectScheduleFactory
import edu.utesa.ronald.ingsoft.utils.container.BaseContainer
import edu.utesa.ronald.ingsoft.utils.container.LazyQuery
import groovy.transform.CompileStatic

/**
 *  Created by ronald on 3/07/17.
 */
@CompileStatic
class SubjectScheduleContainer extends BaseContainer {

    SubjectScheduleContainer(LazyQuery<SubjectSchedule> lazyQuery) {
        super(new SubjectScheduleFactory(lazyQuery))
        addColumns()
    }

    @Override
    void addColumns() {
        addStringColumn("day")
        addStringColumn("startTime")
        addStringColumn("endTime")
    }
}
