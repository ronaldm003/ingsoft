package edu.utesa.ronald.ingsoft.models.containers.admin

import edu.utesa.ronald.ingsoft.domains.admin.Teacher
import edu.utesa.ronald.ingsoft.models.factories.admin.TeacherFactory
import edu.utesa.ronald.ingsoft.utils.container.BaseContainer
import edu.utesa.ronald.ingsoft.utils.container.LazyQuery
import groovy.transform.CompileStatic

/**
 *  Created by ronald on 3/07/17.
 */
@CompileStatic
class TeacherContainer extends BaseContainer {

    TeacherContainer(LazyQuery<Teacher> lazyQuery) {
        super(new TeacherFactory(lazyQuery))
        addColumns()
    }

    @Override
    void addColumns() {
        addStringColumn("code")
        addStringColumn("name")
        addStringColumn("lastName")
        addDateColumn("birthDate")
        addDateColumn("ingressDate")
        addStringColumn("email")
        addStringColumn("phone")
        addStringColumn("warrant")
    }
}
