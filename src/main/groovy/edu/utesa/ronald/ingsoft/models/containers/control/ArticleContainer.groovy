package edu.utesa.ronald.ingsoft.models.containers.control

import edu.utesa.ronald.ingsoft.domains.control.Article
import edu.utesa.ronald.ingsoft.models.factories.control.ArticleFactory
import edu.utesa.ronald.ingsoft.utils.container.BaseContainer
import edu.utesa.ronald.ingsoft.utils.container.LazyQuery
import groovy.transform.CompileStatic

/**
 *  Created by ronald on 3/07/17.
 */
@CompileStatic
class ArticleContainer extends BaseContainer {

    ArticleContainer(LazyQuery<Article> lazyQuery) {
        super(new ArticleFactory(lazyQuery))
        addColumns()
    }

    @Override
    void addColumns() {
        addStringColumn("code")
        addStringColumn("description")
        addStringColumn("quantity")
        addStringColumn("punceo")
        addStringColumn("cost")
    }
}
