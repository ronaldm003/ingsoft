package edu.utesa.ronald.ingsoft.models.containers.control

import edu.utesa.ronald.ingsoft.domains.control.Equipment
import edu.utesa.ronald.ingsoft.models.factories.control.EquipmentFactory
import edu.utesa.ronald.ingsoft.utils.container.BaseContainer
import edu.utesa.ronald.ingsoft.utils.container.LazyQuery
import groovy.transform.CompileStatic

/**
 *  Created by ronald on 3/07/17.
 */
@CompileStatic
class EquipmentContainer extends BaseContainer {

    EquipmentContainer(LazyQuery<Equipment> lazyQuery) {
        super(new EquipmentFactory(lazyQuery))
        addColumns()
    }

    @Override
    void addColumns() {
        addStringColumn("name")
        addStringColumn("description")
    }
}
