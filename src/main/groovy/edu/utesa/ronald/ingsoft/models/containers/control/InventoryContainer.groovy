package edu.utesa.ronald.ingsoft.models.containers.control

import edu.utesa.ronald.ingsoft.domains.control.Inventory
import edu.utesa.ronald.ingsoft.models.factories.control.InventoryFactory
import edu.utesa.ronald.ingsoft.utils.container.BaseContainer
import edu.utesa.ronald.ingsoft.utils.container.LazyQuery
import groovy.transform.CompileStatic

/**
 *  Created by ronald on 3/07/17.
 */
@CompileStatic
class InventoryContainer extends BaseContainer {

    InventoryContainer(LazyQuery<Inventory> lazyQuery) {
        super(new InventoryFactory(lazyQuery))
        addColumns()
    }

    @Override
    void addColumns() {
        addStringColumn("description")
    }
}
