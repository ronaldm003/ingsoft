package edu.utesa.ronald.ingsoft.models.containers.control

import edu.utesa.ronald.ingsoft.domains.control.Prescription
import edu.utesa.ronald.ingsoft.models.factories.control.PrescriptionFactory
import edu.utesa.ronald.ingsoft.utils.container.BaseContainer
import edu.utesa.ronald.ingsoft.utils.container.LazyQuery
import groovy.transform.CompileStatic

/**
 *  Created by ronald on 3/07/17.
 */
@CompileStatic
class PrescriptionContainer extends BaseContainer {

    PrescriptionContainer(LazyQuery<Prescription> lazyQuery) {
        super(new PrescriptionFactory(lazyQuery))
        addColumns()
    }

    @Override
    void addColumns() {
        addStringColumn("code")
        addStringColumn("description")
    }
}
