package edu.utesa.ronald.ingsoft.models.containers.control.recordcard

import edu.utesa.ronald.ingsoft.domains.control.recordcard.CariesIndex
import edu.utesa.ronald.ingsoft.models.factories.control.recordcard.CariesIndexFactory
import edu.utesa.ronald.ingsoft.utils.container.BaseContainer
import edu.utesa.ronald.ingsoft.utils.container.LazyQuery
import groovy.transform.CompileStatic

/**
 *  Created by ronald on 3/07/17.
 */
@CompileStatic
class CariesIndexContainer extends BaseContainer {

    CariesIndexContainer(LazyQuery<CariesIndex> lazyQuery) {
        super(new CariesIndexFactory(lazyQuery))
        addColumns()
    }

    @Override
    void addColumns() {
        addStringColumn("decayed")
        addStringColumn("lost")
        addStringColumn("sealed")
        addStringColumn("teethPresent")
        addStringColumn("level")
        addStringColumn("groupQuantity")
        addStringColumn("date")
    }
}
