package edu.utesa.ronald.ingsoft.models.containers.control.recordcard

import edu.utesa.ronald.ingsoft.domains.control.recordcard.EducationLevel
import edu.utesa.ronald.ingsoft.models.factories.control.recordcard.EducationLevelFactory
import edu.utesa.ronald.ingsoft.utils.container.BaseContainer
import edu.utesa.ronald.ingsoft.utils.container.LazyQuery
import groovy.transform.CompileStatic

/**
 *  Created by ronald on 3/07/17.
 */
@CompileStatic
class EducationLevelContainer extends BaseContainer {

    EducationLevelContainer(LazyQuery<EducationLevel> lazyQuery) {
        super(new EducationLevelFactory(lazyQuery))
        addColumns()
    }

    @Override
    void addColumns() {
        addStringColumn("description")
    }
}
