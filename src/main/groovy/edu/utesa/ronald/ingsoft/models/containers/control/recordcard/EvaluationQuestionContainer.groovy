package edu.utesa.ronald.ingsoft.models.containers.control.recordcard

import edu.utesa.ronald.ingsoft.domains.control.recordcard.EvaluationQuestion
import edu.utesa.ronald.ingsoft.models.factories.control.recordcard.EvaluationQuestionFactory
import edu.utesa.ronald.ingsoft.utils.container.BaseContainer
import edu.utesa.ronald.ingsoft.utils.container.LazyQuery
import groovy.transform.CompileStatic

/**
 *  Created by ronald on 3/07/17.
 */
@CompileStatic
class EvaluationQuestionContainer extends BaseContainer {

    EvaluationQuestionContainer(LazyQuery<EvaluationQuestion> lazyQuery) {
        super(new EvaluationQuestionFactory(lazyQuery))
        addColumns()
    }

    @Override
    void addColumns() {
        addStringColumn("code")
        addStringColumn("question")
    }
}
