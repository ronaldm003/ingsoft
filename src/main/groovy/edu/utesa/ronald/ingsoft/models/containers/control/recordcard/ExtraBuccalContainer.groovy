package edu.utesa.ronald.ingsoft.models.containers.control.recordcard

import edu.utesa.ronald.ingsoft.domains.control.recordcard.ExtraBuccal
import edu.utesa.ronald.ingsoft.models.factories.control.recordcard.ExtraBuccalFactory
import edu.utesa.ronald.ingsoft.utils.container.BaseContainer
import edu.utesa.ronald.ingsoft.utils.container.LazyQuery
import groovy.transform.CompileStatic

/**
 *  Created by ronald on 3/07/17.
 */
@CompileStatic
class ExtraBuccalContainer extends BaseContainer {

    ExtraBuccalContainer(LazyQuery<ExtraBuccal> lazyQuery) {
        super(new ExtraBuccalFactory(lazyQuery))
        addColumns()
    }

    @Override
    void addColumns() {
        addStringColumn("code")
        addStringColumn("question")
    }
}
