package edu.utesa.ronald.ingsoft.models.containers.control.recordcard

import edu.utesa.ronald.ingsoft.domains.control.recordcard.IntraBuccal
import edu.utesa.ronald.ingsoft.models.factories.control.recordcard.IntraBuccalFactory
import edu.utesa.ronald.ingsoft.utils.container.BaseContainer
import edu.utesa.ronald.ingsoft.utils.container.LazyQuery
import groovy.transform.CompileStatic

/**
 *  Created by ronald on 3/07/17.
 */
@CompileStatic
class IntraBuccalContainer extends BaseContainer {

    IntraBuccalContainer(LazyQuery<IntraBuccal> lazyQuery) {
        super(new IntraBuccalFactory(lazyQuery))
        addColumns()
    }

    @Override
    void addColumns() {
        addStringColumn("code")
        addStringColumn("question")
    }
}
