package edu.utesa.ronald.ingsoft.models.containers.control.recordcard

import edu.utesa.ronald.ingsoft.domains.control.recordcard.MedObservations
import edu.utesa.ronald.ingsoft.models.factories.control.recordcard.MedObservationsFactory
import edu.utesa.ronald.ingsoft.utils.container.BaseContainer
import edu.utesa.ronald.ingsoft.utils.container.LazyQuery
import groovy.transform.CompileStatic

/**
 *  Created by ronald on 3/07/17.
 */
@CompileStatic
class MedObservationsContainer extends BaseContainer {

    MedObservationsContainer(LazyQuery<MedObservations> lazyQuery) {
        super(new MedObservationsFactory(lazyQuery))
        addColumns()
    }

    @Override
    void addColumns() {
        addStringColumn("observations")
        addStringColumn("actualMedicaments")
        addStringColumn("medicamentName")
        addStringColumn("medicamentFor")
        addStringColumn("whoIndicateIt")
    }
}
