package edu.utesa.ronald.ingsoft.models.containers.control.recordcard

import edu.utesa.ronald.ingsoft.domains.control.recordcard.Occlusion
import edu.utesa.ronald.ingsoft.models.factories.control.recordcard.OcclusionFactory
import edu.utesa.ronald.ingsoft.utils.container.BaseContainer
import edu.utesa.ronald.ingsoft.utils.container.LazyQuery
import groovy.transform.CompileStatic

/**
 *  Created by ronald on 3/07/17.
 */
@CompileStatic
class OcclusionContainer extends BaseContainer {

    OcclusionContainer(LazyQuery<Occlusion> lazyQuery) {
        super(new OcclusionFactory(lazyQuery))
        addColumns()
    }

    @Override
    void addColumns() {
        addStringColumn("facialProfile")
        addStringColumn("molarRelation")
        addStringColumn("canineRelation")
        addStringColumn("eruptionSecuence")
        addStringColumn("crossBite")
        addStringColumn("supernumeraries")
        addStringColumn("congenitalAbsences")
        addStringColumn("primateSpaces")
        addStringColumn("middleLine")
        addStringColumn("mandibularDeviation")
        addStringColumn("openBite")
        addStringColumn("overBite")
        addStringColumn("openJet")
        addStringColumn("arcType")
        addStringColumn("maxilarSup")
        addStringColumn("maxilarInf")
    }
}
