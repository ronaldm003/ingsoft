package edu.utesa.ronald.ingsoft.models.containers.control.recordcard

import edu.utesa.ronald.ingsoft.domains.control.recordcard.OralHygiene
import edu.utesa.ronald.ingsoft.models.factories.control.recordcard.OralHygieneFactory
import edu.utesa.ronald.ingsoft.utils.container.BaseContainer
import edu.utesa.ronald.ingsoft.utils.container.LazyQuery
import groovy.transform.CompileStatic

/**
 *  Created by ronald on 3/07/17.
 */
@CompileStatic
class OralHygieneContainer extends BaseContainer {

    OralHygieneContainer(LazyQuery<OralHygiene> lazyQuery, boolean readOnly) {
        super(new OralHygieneFactory(lazyQuery, readOnly))
        addColumns()
    }

    @Override
    void addColumns() {
        addStringColumn("brushed")
        addStringColumn("floss")
        addStringColumn("mouthwashes")
        addStringColumn("sanitizeTongue")
    }
}
