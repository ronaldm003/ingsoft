package edu.utesa.ronald.ingsoft.models.containers.control.recordcard

import edu.utesa.ronald.ingsoft.domains.control.recordcard.OralHygieneIndex
import edu.utesa.ronald.ingsoft.models.factories.control.recordcard.OralHygieneIndexFactory
import edu.utesa.ronald.ingsoft.utils.container.BaseContainer
import edu.utesa.ronald.ingsoft.utils.container.LazyQuery
import groovy.transform.CompileStatic

/**
 *  Created by ronald on 3/07/17.
 */
@CompileStatic
class OralHygieneIndexContainer extends BaseContainer {

    OralHygieneIndexContainer(LazyQuery<OralHygieneIndex> lazyQuery) {
        super(new OralHygieneIndexFactory(lazyQuery))
        addColumns()
    }

    @Override
    void addColumns() {
        addStringColumn("code")
        addStringColumn("tooth")
        addStringColumn("vestib16")
        addStringColumn("vestib11")
        addStringColumn("vestib26")
        addStringColumn("ling36")
        addStringColumn("vestib31")
        addStringColumn("ling46")
        addStringColumn("total")
        addStringColumn("indexNumber")
        addStringColumn("hygeneLevel")
        addStringColumn("date")
    }
}
