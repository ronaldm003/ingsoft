package edu.utesa.ronald.ingsoft.models.containers.control.recordcard

import edu.utesa.ronald.ingsoft.domains.control.recordcard.OralIllness
import edu.utesa.ronald.ingsoft.models.factories.control.recordcard.OralIllnessFactory
import edu.utesa.ronald.ingsoft.utils.container.BaseContainer
import edu.utesa.ronald.ingsoft.utils.container.LazyQuery
import groovy.transform.CompileStatic

/**
 *  Created by ronald on 3/07/17.
 */
@CompileStatic
class OralIllnessContainer extends BaseContainer {

    OralIllnessContainer(LazyQuery<OralIllness> lazyQuery) {
        super(new OralIllnessFactory(lazyQuery))
        addColumns()
    }

    @Override
    void addColumns() {
        addStringColumn("code")
        addStringColumn("description")
    }
}
