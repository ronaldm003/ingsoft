package edu.utesa.ronald.ingsoft.models.containers.control.recordcard

import com.vaadin.ui.TextArea
import edu.utesa.ronald.ingsoft.domains.control.recordcard.PatientEvaluationAnswer
import edu.utesa.ronald.ingsoft.models.factories.control.recordcard.PatientEvaluationAnswerFactory
import edu.utesa.ronald.ingsoft.utils.container.BaseContainer
import edu.utesa.ronald.ingsoft.utils.container.LazyQuery
import groovy.transform.CompileStatic

/**
 *  Created by ronald on 3/07/17.
 */
@CompileStatic
class PatientEvaluationAnswerContainer extends BaseContainer {

    PatientEvaluationAnswerContainer(LazyQuery<PatientEvaluationAnswer> lazyQuery, boolean readOnly) {
        super(new PatientEvaluationAnswerFactory(lazyQuery, readOnly))
        addColumns()
    }

    @Override
    void addColumns() {
        addContainerProperty("evaluationQuestion", String.class, "", false, false)
        addContainerProperty("answer", TextArea.class, new TextArea(), true, false)
    }
}
