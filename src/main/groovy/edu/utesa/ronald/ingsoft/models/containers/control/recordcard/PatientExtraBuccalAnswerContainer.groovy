package edu.utesa.ronald.ingsoft.models.containers.control.recordcard

import com.vaadin.ui.TextArea
import edu.utesa.ronald.ingsoft.domains.control.recordcard.PatientExtraBuccalAnswer
import edu.utesa.ronald.ingsoft.models.factories.control.recordcard.PatientExtraBuccalAnswerFactory
import edu.utesa.ronald.ingsoft.utils.container.BaseContainer
import edu.utesa.ronald.ingsoft.utils.container.LazyQuery
import groovy.transform.CompileStatic

/**
 *  Created by ronald on 3/07/17.
 */
@CompileStatic
class PatientExtraBuccalAnswerContainer extends BaseContainer {

    PatientExtraBuccalAnswerContainer(LazyQuery<PatientExtraBuccalAnswer> lazyQuery, boolean readOnly) {
        super(new PatientExtraBuccalAnswerFactory(lazyQuery, readOnly))
        addColumns()
    }

    @Override
    void addColumns() {
        addContainerProperty("extraBuccal", String.class, "", false, false)
        addContainerProperty("answer", TextArea.class, new TextArea(), true, false)
    }
}
