package edu.utesa.ronald.ingsoft.models.containers.control.recordcard

import com.vaadin.ui.CheckBox
import edu.utesa.ronald.ingsoft.domains.control.recordcard.PatientHabits
import edu.utesa.ronald.ingsoft.models.factories.control.recordcard.PatientHabitsFactory
import edu.utesa.ronald.ingsoft.utils.container.BaseContainer
import edu.utesa.ronald.ingsoft.utils.container.LazyQuery
import groovy.transform.CompileStatic

/**
 *  Created by ronald on 3/07/17.
 */
@CompileStatic
class PatientHabitsContainer extends BaseContainer {

    PatientHabitsContainer(LazyQuery<PatientHabits> lazyQuery, boolean readOnly) {
        super(new PatientHabitsFactory(lazyQuery, readOnly))
        addColumns()
    }

    @Override
    void addColumns() {
        addContainerProperty("habits", String.class, "", false, false)
        addContainerProperty("state", CheckBox.class, new CheckBox(), true, false)
    }
}
