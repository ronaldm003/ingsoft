package edu.utesa.ronald.ingsoft.models.containers.control.recordcard

import com.vaadin.ui.TextArea
import edu.utesa.ronald.ingsoft.domains.control.recordcard.PatientIntraBuccalAnswer
import edu.utesa.ronald.ingsoft.models.factories.control.recordcard.PatientIntraBuccalAnswerFactory
import edu.utesa.ronald.ingsoft.utils.container.BaseContainer
import edu.utesa.ronald.ingsoft.utils.container.LazyQuery
import groovy.transform.CompileStatic

/**
 *  Created by ronald on 3/07/17.
 */
@CompileStatic
class PatientIntraBuccalAnswerContainer extends BaseContainer {

    PatientIntraBuccalAnswerContainer(LazyQuery<PatientIntraBuccalAnswer> lazyQuery, boolean readOnly) {
        super(new PatientIntraBuccalAnswerFactory(lazyQuery, readOnly))
        addColumns()
    }

    @Override
    void addColumns() {
        addContainerProperty("intraBuccal", String.class, "", false, false)
        addContainerProperty("answer", TextArea.class, new TextArea(), true, false)
    }
}
