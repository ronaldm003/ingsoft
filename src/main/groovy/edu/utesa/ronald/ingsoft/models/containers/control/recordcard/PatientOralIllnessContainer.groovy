package edu.utesa.ronald.ingsoft.models.containers.control.recordcard

import com.vaadin.ui.CheckBox
import edu.utesa.ronald.ingsoft.domains.control.recordcard.PatientOralIllness
import edu.utesa.ronald.ingsoft.models.factories.control.recordcard.PatientOralIllnessFactory
import edu.utesa.ronald.ingsoft.utils.container.BaseContainer
import edu.utesa.ronald.ingsoft.utils.container.LazyQuery
import groovy.transform.CompileStatic

/**
 *  Created by ronald on 3/07/17.
 */
@CompileStatic
class PatientOralIllnessContainer extends BaseContainer {

    PatientOralIllnessContainer(LazyQuery<PatientOralIllness> lazyQuery, boolean readOnly) {
        super(new PatientOralIllnessFactory(lazyQuery, readOnly))
        addColumns()
    }

    @Override
    void addColumns() {
        addContainerProperty("oralIllness", String.class, "", false, false)
        addContainerProperty("state", CheckBox.class, new CheckBox(), true, false)
    }
}
