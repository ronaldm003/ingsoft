package edu.utesa.ronald.ingsoft.models.containers.control.recordcard

import edu.utesa.ronald.ingsoft.domains.control.recordcard.RecordCard
import edu.utesa.ronald.ingsoft.models.factories.control.recordcard.RecordCardFactory
import edu.utesa.ronald.ingsoft.utils.container.BaseContainer
import edu.utesa.ronald.ingsoft.utils.container.LazyQuery
import groovy.transform.CompileStatic

/**
 *  Created by ronald on 3/07/17.
 */
@CompileStatic
class RecordCardContainer extends BaseContainer {

    RecordCardContainer(LazyQuery<RecordCard> lazyQuery) {
        super(new RecordCardFactory(lazyQuery))
        addColumns()
    }

    @Override
    void addColumns() {
        addStringColumn("cycle")
        addStringColumn("recordId")
        addDateColumn("ingressDate")
        addStringColumn("student")
        addStringColumn("patient")
        addStringColumn("teacher")
    }
}
