package edu.utesa.ronald.ingsoft.models.containers.control.recordcard

import edu.utesa.ronald.ingsoft.domains.control.recordcard.Tooth
import edu.utesa.ronald.ingsoft.models.factories.control.recordcard.ToothFactory
import edu.utesa.ronald.ingsoft.utils.container.BaseContainer
import edu.utesa.ronald.ingsoft.utils.container.LazyQuery
import groovy.transform.CompileStatic

/**
 *  Created by ronald on 3/07/17.
 */
@CompileStatic
class ToothContainer extends BaseContainer {

    ToothContainer(LazyQuery<Tooth> lazyQuery) {
        super(new ToothFactory(lazyQuery))
        addColumns()
    }

    @Override
    void addColumns() {
        addStringColumn("number")
        addStringColumn("cOclusal")
        addStringColumn("cDistal")
        addStringColumn("cVestibular")
        addStringColumn("cMesial")
        addStringColumn("cPalatinaSup")
        addStringColumn("cLingualInf")
        addBooleanColumn("child")
    }
}
