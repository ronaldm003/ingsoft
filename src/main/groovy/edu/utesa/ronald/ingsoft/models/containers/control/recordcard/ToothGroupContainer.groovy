package edu.utesa.ronald.ingsoft.models.containers.control.recordcard

import edu.utesa.ronald.ingsoft.domains.control.recordcard.ToothGroup
import edu.utesa.ronald.ingsoft.models.factories.control.recordcard.ToothGroupFactory
import edu.utesa.ronald.ingsoft.utils.container.BaseContainer
import edu.utesa.ronald.ingsoft.utils.container.LazyQuery
import groovy.transform.CompileStatic

/**
 *  Created by ronald on 3/07/17.
 */
@CompileStatic
class ToothGroupContainer extends BaseContainer {

    ToothGroupContainer(LazyQuery<ToothGroup> lazyQuery) {
        super(new ToothGroupFactory(lazyQuery))
        addColumns()
    }

    @Override
    void addColumns() {
        addStringColumn("name")
        addStringColumn("description")
        addBooleanColumn("admin")
    }
}
