package edu.utesa.ronald.ingsoft.models.containers.dental

import edu.utesa.ronald.ingsoft.domains.control.recordcard.RecordCard
import edu.utesa.ronald.ingsoft.models.factories.dental.ConsultsFactory
import edu.utesa.ronald.ingsoft.utils.container.BaseContainer
import edu.utesa.ronald.ingsoft.utils.container.LazyQuery
import groovy.transform.CompileStatic

/**
 *  Created by ronald on 3/07/17.
 */
@CompileStatic
class ConsultsContainer extends BaseContainer {

    ConsultsContainer(LazyQuery<RecordCard> lazyQuery) {
        super(new ConsultsFactory(lazyQuery))
        addColumns()
    }

    @Override
    void addColumns() {
        addStringColumn("cycle")
        addStringColumn("recordId")
        addDateColumn("ingressDate")
        addStringColumn("student")
        addStringColumn("patient")
        addStringColumn("teacher")
    }
}
