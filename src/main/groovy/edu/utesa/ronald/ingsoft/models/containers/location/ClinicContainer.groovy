package edu.utesa.ronald.ingsoft.models.containers.location

import edu.utesa.ronald.ingsoft.domains.location.Clinic
import edu.utesa.ronald.ingsoft.models.factories.location.ClinicFactory
import edu.utesa.ronald.ingsoft.utils.container.BaseContainer
import edu.utesa.ronald.ingsoft.utils.container.LazyQuery
import groovy.transform.CompileStatic

/**
 *  Created by ronald on 3/07/17.
 */
@CompileStatic
class ClinicContainer extends BaseContainer {

    ClinicContainer(LazyQuery<Clinic> lazyQuery) {
        super(new ClinicFactory(lazyQuery))
        addColumns()
    }

    @Override
    void addColumns() {
        addStringColumn("code")
        addStringColumn("description")
        addStringColumn("location")
        addStringColumn("inventory")
    }
}
