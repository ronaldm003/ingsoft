package edu.utesa.ronald.ingsoft.models.containers.location

import edu.utesa.ronald.ingsoft.domains.location.Location
import edu.utesa.ronald.ingsoft.models.factories.location.LocationFactory
import edu.utesa.ronald.ingsoft.utils.container.BaseContainer
import edu.utesa.ronald.ingsoft.utils.container.LazyQuery
import groovy.transform.CompileStatic

/**
 *  Created by ronald on 3/07/17.
 */
@CompileStatic
class LocationContainer extends BaseContainer {

    LocationContainer(LazyQuery<Location> lazyQuery) {
        super(new LocationFactory(lazyQuery))
        addColumns()
    }

    @Override
    void addColumns() {
        addStringColumn("code")
        addStringColumn("description")
    }
}
