package edu.utesa.ronald.ingsoft.models.containers.security

import edu.utesa.ronald.ingsoft.domains.security.Param
import edu.utesa.ronald.ingsoft.models.factories.security.ParamFactory
import edu.utesa.ronald.ingsoft.utils.container.BaseContainer
import edu.utesa.ronald.ingsoft.utils.container.LazyQuery
import groovy.transform.CompileStatic

/**
 *  Created by ronald on 4/11/18.
 */
@CompileStatic
class ParamContainer extends BaseContainer {

    ParamContainer(LazyQuery<Param> lazyQuery) {
        super(new ParamFactory(lazyQuery))

        addColumns()
    }

    @Override
    void addColumns() {
        addStringColumn("paramValueType")
        addStringColumn("name")
        addStringColumn("description")
        addStringColumn("value")
    }
}
