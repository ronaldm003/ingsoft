package edu.utesa.ronald.ingsoft.models.containers.security

import edu.utesa.ronald.ingsoft.domains.security.Permission
import edu.utesa.ronald.ingsoft.models.factories.security.PermissionFactory
import edu.utesa.ronald.ingsoft.utils.container.BaseContainer
import edu.utesa.ronald.ingsoft.utils.container.LazyQuery
import groovy.transform.CompileStatic

/**
 *  Created by ronald on 4/11/18.
 */
@CompileStatic
class PermissionContainer extends BaseContainer {

    PermissionContainer(LazyQuery<Permission> lazyQuery) {
        super(new PermissionFactory(lazyQuery))
        addColumns()
    }

    @Override
    void addColumns() {
        addIntegerColumn("code")
        addStringColumn("name")
        addStringColumn("description")
    }
}