package edu.utesa.ronald.ingsoft.models.containers.security

import edu.utesa.ronald.ingsoft.domains.security.PermissionGroup
import edu.utesa.ronald.ingsoft.models.factories.security.PermissionGroupFactory
import edu.utesa.ronald.ingsoft.utils.container.BaseContainer
import edu.utesa.ronald.ingsoft.utils.container.LazyQuery
import groovy.transform.CompileStatic

/**
 *  Created by ronald on 4/11/18.
 */
@CompileStatic
class PermissionGroupContainer extends BaseContainer {

    PermissionGroupContainer(LazyQuery<PermissionGroup> lazyQuery) {
        super(new PermissionGroupFactory(lazyQuery))

        addColumns()
    }

    @Override
    void addColumns() {
        addStringColumn("name")
        addStringColumn("description")
        addBooleanColumn("enabled")
    }
}
