package edu.utesa.ronald.ingsoft.models.containers.security

import edu.utesa.ronald.ingsoft.domains.security.Permission
import edu.utesa.ronald.ingsoft.models.factories.security.PermissionFactory
import edu.utesa.ronald.ingsoft.utils.container.BaseContainer
import edu.utesa.ronald.ingsoft.utils.container.LazyQuery
import groovy.transform.CompileStatic

/**
 *  Created by Ing. Aluis Marte on 2/4/2015.
 */
@CompileStatic
class PermissionsUserContainer extends BaseContainer {

    PermissionsUserContainer(LazyQuery<Permission> lazyQuery) {
        super(new PermissionFactory(lazyQuery))

        addColumns()
    }

    @Override
    void addColumns() {
        addStringColumn("name")
        addStringColumn("description")
        addBooleanColumn("enabled")
    }
}