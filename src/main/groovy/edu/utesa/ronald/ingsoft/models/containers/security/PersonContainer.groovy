package edu.utesa.ronald.ingsoft.models.containers.security

import edu.utesa.ronald.ingsoft.domains.security.Person
import edu.utesa.ronald.ingsoft.models.factories.security.PersonFactory
import edu.utesa.ronald.ingsoft.utils.container.BaseContainer
import edu.utesa.ronald.ingsoft.utils.container.LazyQuery
import groovy.transform.CompileStatic

/**
 *  Created by ronald on 4/11/18.
 */
@CompileStatic
class PersonContainer extends BaseContainer {

    PersonContainer(LazyQuery<Person> lazyQuery) {
        super(new PersonFactory(lazyQuery))
        addColumns()
    }

    @Override
    void addColumns() {
        addStringColumn("name")
        addStringColumn("location")
        addStringColumn("gender")
        addStringColumn("phone")
        addStringColumn("email")
        addBooleanColumn("admin")
    }
}
