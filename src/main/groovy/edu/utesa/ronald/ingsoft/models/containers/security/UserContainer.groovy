package edu.utesa.ronald.ingsoft.models.containers.security

import edu.utesa.ronald.ingsoft.domains.security.User
import edu.utesa.ronald.ingsoft.models.factories.security.UserFactory
import edu.utesa.ronald.ingsoft.utils.container.BaseContainer
import edu.utesa.ronald.ingsoft.utils.container.LazyQuery
import groovy.transform.CompileStatic

/**
 *  Created by ronald on 3/07/17.
 */
@CompileStatic
class UserContainer extends BaseContainer {

    UserContainer(LazyQuery<User> lazyQuery) {
        super(new UserFactory(lazyQuery))
        addColumns()
    }

    @Override
    void addColumns() {
        addStringColumn("name")
        addStringColumn("username")
        addStringColumn("location")
        addStringColumn("gender")
        addStringColumn("phone")
        addStringColumn("email")
        addBooleanColumn("admin")
    }
}
