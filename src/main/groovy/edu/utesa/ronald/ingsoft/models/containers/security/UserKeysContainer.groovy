package edu.utesa.ronald.ingsoft.models.containers.security

import edu.utesa.ronald.ingsoft.domains.security.UserKeys
import edu.utesa.ronald.ingsoft.models.factories.security.UserKeysFactory
import edu.utesa.ronald.ingsoft.utils.container.BaseContainer
import edu.utesa.ronald.ingsoft.utils.container.LazyQuery
import groovy.transform.CompileStatic

/**
 *  Created by ronald on 3/07/17.
 */
@CompileStatic
class UserKeysContainer extends BaseContainer {

    UserKeysContainer(LazyQuery<UserKeys> lazyQuery) {
        super(new UserKeysFactory(lazyQuery))
        addColumns()
    }

    @Override
    void addColumns() {
        addStringColumn("user")
        addStringColumn("authorizationKey")
        addStringColumn("role")
        addStringColumn("secUser")
        addStringColumn("date")
        addBooleanColumn("estatus")
    }
}
