package edu.utesa.ronald.ingsoft.models.containers.utils

import edu.utesa.ronald.ingsoft.domains.utils.Occupation
import edu.utesa.ronald.ingsoft.models.factories.utils.OccupationFactory
import edu.utesa.ronald.ingsoft.utils.container.BaseContainer
import edu.utesa.ronald.ingsoft.utils.container.LazyQuery
import groovy.transform.CompileStatic

/**
 *  Created by ronald on 3/07/17.
 */
@CompileStatic
class OccupationContainer extends BaseContainer {

    OccupationContainer(LazyQuery<Occupation> lazyQuery) {
        super(new OccupationFactory(lazyQuery))
        addColumns()
    }

    @Override
    void addColumns() {
        addStringColumn("description")
    }
}
