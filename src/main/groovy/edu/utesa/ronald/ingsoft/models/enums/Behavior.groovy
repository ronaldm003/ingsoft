package edu.utesa.ronald.ingsoft.models.enums

import edu.utesa.ronald.ingsoft.utils.Languages

/**
 * Modo de vista del calendario.
 * <p>
 * Created by ronald on 7/18/16.
 */
public enum Behavior {
    COOPERATOR, AGGRESSIVE, FEARFUL, CONFIDENT

    @Override
    public String toString() {
        switch (this) {
            case COOPERATOR:
                return Languages.instance.getText("enum.behavior.cooperator")
            case AGGRESSIVE:
                return Languages.instance.getText("enum.behavior.aggressive")
            case FEARFUL:
                return Languages.instance.getText("enum.behavior.fearful")
            case CONFIDENT:
                return Languages.instance.getText("enum.behavior.confident")
        }
        return ""
    }

}