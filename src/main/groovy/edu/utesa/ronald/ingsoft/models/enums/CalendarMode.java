package edu.utesa.ronald.ingsoft.models.enums;

/**
 * Modo de vista del calendario.
 * <p>
 * Created by ronald on 7/18/16.
 */
public enum CalendarMode {
    MONTH, WEEK, DAY
}