package edu.utesa.ronald.ingsoft.models.enums

import com.vaadin.ui.ComboBox
import edu.utesa.ronald.ingsoft.utils.Languages

/**
 *  Created by ronald on 3/20/17.
 */
enum CivilEstateEnum {
    SINGLE, MARRIED, DIVORCED, WIDOWED

    @Override
    String toString() {
        switch (valueOf(name())) {
            case SINGLE:
                return Languages.instance.getText("enum.civil.estate.single")
            case MARRIED:
                return Languages.instance.getText("enum.civil.estate.married")
            case DIVORCED:
                return Languages.instance.getText("enum.civil.estate.divorced")
            case WIDOWED:
                return Languages.instance.getText("enum.civil.estate.widowed")
        }
        return ""
    }

}
