package edu.utesa.ronald.ingsoft.models.enums

import edu.utesa.ronald.ingsoft.utils.Languages

/**
 * Modo de vista del calendario.
 * <p>
 * Created by ronald on 7/18/16.
 */
public enum Complexion {
    NORMAL, SLIM, OVERWEIGHT, OBESE

    @Override
    public String toString() {
        switch (this) {
            case NORMAL:
                return Languages.instance.getText("enum.complexion.normal")
            case SLIM:
                return Languages.instance.getText("enum.complexion.slim")
            case OVERWEIGHT:
                return Languages.instance.getText("enum.complexion.overweight")
            case OBESE:
                return Languages.instance.getText("enum.complexion.obese")
        }
        return ""
    }
}