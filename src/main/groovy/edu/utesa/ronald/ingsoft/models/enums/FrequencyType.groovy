package edu.utesa.ronald.ingsoft.models.enums

import edu.utesa.ronald.ingsoft.utils.Languages

/**
 * Created by ronald on 9/6/16.
 */
enum FrequencyType {
    ANNUAL, MONTHLY, BIWEEKLY, WEEKLY, DAILY

    @Override
    String toString() {
        switch (this) {
            case ANNUAL:
                return Languages.instance.getText("enum.frequency.type.Years")
            case MONTHLY:
                return Languages.instance.getText("enum.frequency.type.Months")
            case BIWEEKLY:
                return Languages.instance.getText("enum.frequency.type.Fortnights")
            case WEEKLY:
                return Languages.instance.getText("enum.frequency.type.Weeks")
            case DAILY:
                return Languages.instance.getText("enum.frequency.type.Days")
        }
        return ""
    }
}
