package edu.utesa.ronald.ingsoft.models.enums

import com.vaadin.ui.ComboBox
import edu.utesa.ronald.ingsoft.utils.Languages

/**
 *  Created by ronald on 3/20/17.
 */
enum Gender {
    MAN, WOMAN, OTHERS

    @Override
    String toString() {
        switch (valueOf(name())) {
            case MAN:
                return Languages.instance.getText("enum.gender.man")
            case WOMAN:
                return Languages.instance.getText("enum.gender.woman")
            case OTHERS:
                return Languages.instance.getText("enum.gender.other")
        }
        return ""
    }

}
