package edu.utesa.ronald.ingsoft.models.enums

import edu.utesa.ronald.ingsoft.utils.Languages

/**
 *  Created by ronald on 3/20/17.
 */
enum Nationality {
    DOMINICAN, HAITIAN, AMERICAN, COLOMBIAN, VENEZUELAN, OTHER

    @Override
    String toString() {
        switch (valueOf(name())) {
            case DOMINICAN:
                return Languages.instance.getText("enum.nationality.dominican")
            case HAITIAN:
                return Languages.instance.getText("enum.nationality.haitian")
            case AMERICAN:
                return Languages.instance.getText("enum.nationality.american")
            case COLOMBIAN:
                return Languages.instance.getText("enum.nationality.colombian")
            case VENEZUELAN:
                return Languages.instance.getText("enum.nationality.venezuelan")
            case OTHER:
                return Languages.instance.getText("enum.nationality.other")
        }
        return ""
    }
}
