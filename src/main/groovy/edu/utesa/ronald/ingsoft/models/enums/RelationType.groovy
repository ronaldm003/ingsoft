package edu.utesa.ronald.ingsoft.models.enums

import edu.utesa.ronald.ingsoft.utils.Languages

/**
 *  Created by ronald on 3/20/17.
 */
enum RelationType {
    ACQUAINTANCE, ADOPTED, AUNT, BOYFRIEND, BROTHER, BROTHER_IN_LAW, COUSIN, DAD, DAUGHTER, DAUGHTER_IN_LAW,
    FATHER, FATHER_IN_LAW, FIRSTBORN, GIRLFRIEND, GODFATHER, GODMOTHER, GRANDAD, GRANDCHILDREN, GRANDDAUGHTER,
    GRANDFATHER, GRANDMA, GRANDMOTHER, GRANDSON, GREAT_GRANDFATHER, GREAT_GRANDMOTHER, HUSBAND,
    MOM, MOTHER, MOTHER_IN_LAW, NEPHEW, NIECE, SIBLING, SISTER, SISTER_IN_LAW, SON, SON_IN_LAW, STEPDAUGHTER,
    STEPFATHER, STEPMOTHER, STEPSON, UNCLE, WIFE

    @Override
    String toString() {
        switch (valueOf(name())) {
            case ADOPTED:
                return Languages.instance.getText("enum.relation.type.adopted")
            case AUNT:
                return Languages.instance.getText("enum.relation.type.aunt")
            case BOYFRIEND:
                return Languages.instance.getText("enum.relation.type.boyfriend")
            case BROTHER:
                return Languages.instance.getText("enum.relation.type.brother")
            case BROTHER_IN_LAW:
                return Languages.instance.getText("enum.relation.type.brother_in_law")
            case COUSIN:
                return Languages.instance.getText("enum.relation.type.cousin")
            case DAD:
                return Languages.instance.getText("enum.relation.type.dad")
            case DAUGHTER:
                return Languages.instance.getText("enum.relation.type.daughter")
            case DAUGHTER_IN_LAW:
                return Languages.instance.getText("enum.relation.type.daughter_in_law")
            case FATHER:
                return Languages.instance.getText("enum.relation.type.father")
            case FATHER_IN_LAW:
                return Languages.instance.getText("enum.relation.type.father_in_law")
            case FIRSTBORN:
                return Languages.instance.getText("enum.relation.type.firstborn")
            case GIRLFRIEND:
                return Languages.instance.getText("enum.relation.type.girlfriend")
            case GODFATHER:
                return Languages.instance.getText("enum.relation.type.godfather")
            case GODMOTHER:
                return Languages.instance.getText("enum.relation.type.godmother")
            case GRANDAD:
                return Languages.instance.getText("enum.relation.type.grandad")
            case GRANDCHILDREN:
                return Languages.instance.getText("enum.relation.type.grandchildren")
            case GRANDDAUGHTER:
                return Languages.instance.getText("enum.relation.type.granddaughter")
            case GRANDFATHER:
                return Languages.instance.getText("enum.relation.type.grandfather")
            case GRANDMA:
                return Languages.instance.getText("enum.relation.type.grandma")
            case GRANDMOTHER:
                return Languages.instance.getText("enum.relation.type.grandmother")
            case GRANDSON:
                return Languages.instance.getText("enum.relation.type.grandson")
            case GREAT_GRANDFATHER:
                return Languages.instance.getText("enum.relation.type.great_grandfather")
            case GREAT_GRANDMOTHER:
                return Languages.instance.getText("enum.relation.type.great_grandmother")
            case HUSBAND:
                return Languages.instance.getText("enum.relation.type.husband")
            case MOM:
                return Languages.instance.getText("enum.relation.type.mom")
            case MOTHER:
                return Languages.instance.getText("enum.relation.type.mother")
            case MOTHER_IN_LAW:
                return Languages.instance.getText("enum.relation.type.mother_in_law")
            case NEPHEW:
                return Languages.instance.getText("enum.relation.type.nephew")
            case NIECE:
                return Languages.instance.getText("enum.relation.type.niece")
            case SIBLING:
                return Languages.instance.getText("enum.relation.type.sibling")
            case SISTER:
                return Languages.instance.getText("enum.relation.type.sister")
            case SISTER_IN_LAW:
                return Languages.instance.getText("enum.relation.type.sister_in_law")
            case SON:
                return Languages.instance.getText("enum.relation.type.son")
            case SON_IN_LAW:
                return Languages.instance.getText("enum.relation.type.son_in_law")
            case STEPDAUGHTER:
                return Languages.instance.getText("enum.relation.type.stepdaughter")
            case STEPFATHER:
                return Languages.instance.getText("enum.relation.type.stepfather")
            case STEPMOTHER:
                return Languages.instance.getText("enum.relation.type.stepmother")
            case STEPSON:
                return Languages.instance.getText("enum.relation.type.stepson")
            case UNCLE:
                return Languages.instance.getText("enum.relation.type.uncle")
            case WIFE:
                return Languages.instance.getText("enum.relation.type.wife")
        }
        return ""
    }

}
