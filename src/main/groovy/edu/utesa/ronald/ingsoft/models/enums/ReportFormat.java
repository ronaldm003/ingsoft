package edu.utesa.ronald.ingsoft.models.enums;

/**
 * Formatos de reportes.
 * Created by Ing. Aluis Marte on 9/20/2016.
 */
public enum ReportFormat {
    PDF, XLS, HTML, XML, CSV, RTF, TEXT, ODT, ODS, DOCX, XLSX, PPTX;

    public static String getCaption(ReportFormat reportFormat) {
        switch (reportFormat) {
            case PDF:
                return "PDF";
            case XLS:
                return "XLS";
            case HTML:
                return "HTML";
            case XML:
                return "XML";
            case CSV:
                return "CSV";
            case RTF:
                return "RTF";
            case TEXT:
                return "TEXT";
            case ODT:
                return "ODT";
            case ODS:
                return "ODS";
            case DOCX:
                return "DOCX";
            case XLSX:
                return "XLSX";
            case PPTX:
                return "PPTX";
            default:
                return "";
        }
    }

    public static String getExtensionFormat(ReportFormat reportFormat) {
        switch (reportFormat) {
            case PDF:
                return ".pdf";
            case XLS:
                return ".xls";
            case HTML:
                return ".html";
            case XML:
                return ".xml";
            case CSV:
                return ".csv";
            case RTF:
                return ".rtf";
            case TEXT:
                return ".txt";
            case ODT:
                return ".odt";
            case ODS:
                return ".ods";
            case DOCX:
                return ".docx";
            case XLSX:
                return ".xlsx";
            case PPTX:
                return ".pptx";
            default:
                return "";
        }
    }

    public static String getMimeType(ReportFormat reportFormat) {
        switch (reportFormat) {
            case PDF:
                return "application/pdf";
            case XLS:
                return "application/vnd.ms-excel";
            case HTML:
                return "text/html";
            case XML:
                return "application/xml";
            case CSV:
                return "application/csv";
            case RTF:
                return "application/rtf";
            case TEXT:
                return "text/plain";
            case ODT:
                return "application/vnd.oasis.opendocument.text";
            case ODS:
                return "application/vnd.oasis.opendocument.spreadsheet";
            case DOCX:
                return "application/vnd.openxmlformats-officedocument.wordprocessingml.document";
            case XLSX:
                return "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
            case PPTX:
                return "application/vnd.openxmlformats-officedocument.presentationml.presentation";
            default:
                return "";
        }
    }
}