package edu.utesa.ronald.ingsoft.models.enums

import edu.utesa.ronald.ingsoft.utils.Languages
import groovy.transform.CompileStatic

/**
 * Modelo para tipo de documento
 *
 * Created by ronald on 4/09/18.
 */
@CompileStatic
enum UserType {
    TEACHER, PATIENT, STUDENT, ADMIN, ROOT

    @Override
    String toString() {
        switch (this) {
            case TEACHER:
                return Languages.instance.getText("enum.user.type.teacher")
            case PATIENT:
                return Languages.instance.getText("enum.user.type.patient")
            case STUDENT:
                return Languages.instance.getText("enum.user.type.student")
            case ADMIN:
                return Languages.instance.getText("enum.user.type.admin")
            case ROOT:
                return Languages.instance.getText("enum.user.type.root")
        }
        return ""
    }

}
