package edu.utesa.ronald.ingsoft.models.enums

import edu.utesa.ronald.ingsoft.utils.Languages
import groovy.transform.CompileStatic

/**
 * Modelo para tipo de documento
 *
 * Created by ronald on 4/09/18.
 */
@CompileStatic
enum WarrantType {
    NONE, ID, PASSPORT

    @Override
    String toString() {
        switch (this) {
            case NONE:
                return Languages.instance.getText("enum.warranttype.none")
            case ID:
                return Languages.instance.getText("enum.warranttype.id")
            case PASSPORT:
                return Languages.instance.getText("enum.warranttype.passport")
        }
        return ""
    }

}
