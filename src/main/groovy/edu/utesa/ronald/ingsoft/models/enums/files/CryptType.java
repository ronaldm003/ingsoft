package edu.utesa.ronald.ingsoft.models.enums.files;

public enum CryptType {

    AES;

    public final String getCrypt() {
        switch (valueOf(name())) {
            case AES:
                return "AES";
        }
        return "";
    }
}
