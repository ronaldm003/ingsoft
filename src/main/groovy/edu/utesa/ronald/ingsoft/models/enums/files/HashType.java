package edu.utesa.ronald.ingsoft.models.enums.files;

/**
 * Tipo de hash
 * Created by ronald on 7/4/17.
 */
public enum HashType {

    MD5, SHA1, SHA256;

    public static String getHashAlgorithm(HashType hashType) {
        switch (hashType) {
            case MD5:
                return "MD5";
            case SHA1:
                return "SHA-1";
            case SHA256:
                return "SHA-256";
        }
        return "";
    }
}
