package edu.utesa.ronald.ingsoft.models.enums.params

import edu.utesa.ronald.ingsoft.models.interfaces.ParamGetValue
import edu.utesa.ronald.ingsoft.utils.Languages
import groovy.transform.CompileStatic

/**
 * Estado para boolean
 *  Created by ronald on 7/4/17.
 */
@CompileStatic
enum BooleanValue implements ParamGetValue<BooleanValue> {
    TRUE, FALSE

    @Override
    String toString() {
        switch (valueOf(name())) {
            case TRUE:
                return Languages.instance.getText("enum.booelan.true")
            case FALSE:
                return Languages.instance.getText("enum.booelan.false")
        }
        return ""
    }

    @Override
    final String getValue() {
        return this.toString()
    }

    static BooleanValue getValue(String value) {
        switch (value) {
            case Languages.instance.getText("enum.booelan.true"):
                return TRUE
            case Languages.instance.getText("enum.booelan.false"):
                return FALSE
        }
        return null
    }
}
