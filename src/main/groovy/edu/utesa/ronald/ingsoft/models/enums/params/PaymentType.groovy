package edu.utesa.ronald.ingsoft.models.enums.params

import edu.utesa.ronald.ingsoft.models.interfaces.ParamGetValue
import edu.utesa.ronald.ingsoft.utils.Languages
import groovy.transform.CompileStatic

/**
 *  Created by ronald on 9/6/17.
 */
@CompileStatic
enum PaymentType implements ParamGetValue<PaymentType> {
    DEPOSIT, PAYPAL, MASTERCARD, VISA

    @Override
    String toString() {
        switch (this) {
            case DEPOSIT:
                return Languages.instance.getText("enum.payment.type.deposit")
            case PAYPAL:
                return Languages.instance.getText("enum.payment.type.paypal")
            case MASTERCARD:
                return Languages.instance.getText("enum.payment.type.mastercard")
            case VISA:
                return Languages.instance.getText("enum.payment.type.visa")
        }
        return ""
    }

    @Override
    String getValue() {
        return this.toString()
    }

    static PaymentType getValue(String value) {
        switch (value) {
            case Languages.instance.getText("enum.payment.type.deposit"):
                return DEPOSIT
            case Languages.instance.getText("enum.payment.type.paypal"):
                return PAYPAL
            case Languages.instance.getText("enum.payment.type.mastercard"):
                return MASTERCARD
            case Languages.instance.getText("enum.payment.type.visa"):
                return VISA
        }
        return null
    }
}
