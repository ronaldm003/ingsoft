package edu.utesa.ronald.ingsoft.models.enums.params

import edu.utesa.ronald.ingsoft.models.interfaces.ParamGetValue
import groovy.transform.CompileStatic

/**
 * Este caso tiene el modelo viejo, porque no cambia con el idioma.
 *  Created by ronald on03/07/17.
 */
@CompileStatic
enum PaypalMode implements ParamGetValue<PaypalMode> {
    SANDBOX, LIVE

    private static final String sSandbox = "sandbox";
    private static final String sLive = "live";

    @Override
    String toString() {
        switch (this) {
            case SANDBOX:
                return sSandbox
            case LIVE:
                return sLive
        }
        return ""
    }

    @Override
    final String getValue() {
        return this.toString()
    }

    static PaypalMode getValue(String value) {
        switch (value) {
            case sSandbox:
                return SANDBOX;
            case sLive:
                return LIVE;
        }
        return null;
    }
}
