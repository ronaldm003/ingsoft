package edu.utesa.ronald.ingsoft.models.enums.recordcard

import edu.utesa.ronald.ingsoft.utils.Languages

/**
 *  Created by ronald on 3/20/17.
 */
enum ArcType {
    TRIANGULAR, OVAL, SQUARE

    @Override
    String toString() {
        switch (valueOf(name())) {
            case TRIANGULAR:
                return Languages.instance.getText("enum.arc.type.triangular")
            case OVAL:
                return Languages.instance.getText("enum.arc.type.oval")
            case SQUARE:
                return Languages.instance.getText("enum.arc.type.square")
        }
        return ""
    }

}
