package edu.utesa.ronald.ingsoft.models.enums.recordcard

import edu.utesa.ronald.ingsoft.utils.Languages

/**
 *  Created by ronald on 3/20/17.
 */
enum FacialProfile {
    STRAIGHT, CONVEX, CONCAVE

    @Override
    String toString() {
        switch (valueOf(name())) {
            case STRAIGHT:
                return Languages.instance.getText("enum.facial.profile.straight")
            case CONVEX:
                return Languages.instance.getText("enum.facial.profile.convex")
            case CONCAVE:
                return Languages.instance.getText("enum.facial.profile.concave")
        }
        return ""
    }

}
