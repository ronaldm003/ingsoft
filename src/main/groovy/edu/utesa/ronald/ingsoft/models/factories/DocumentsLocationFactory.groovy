package edu.utesa.ronald.ingsoft.models.factories

import com.vaadin.data.Item
import edu.utesa.ronald.ingsoft.models.queries.DocumentsLocationQuery
import edu.utesa.ronald.ingsoft.ui.dashboard.components.charts.models.DocumentsLocation
import edu.utesa.ronald.ingsoft.utils.container.BaseQueryFactory
import edu.utesa.ronald.ingsoft.utils.container.LazyQuery
import groovy.transform.CompileStatic
import org.vaadin.addons.lazyquerycontainer.Query
import org.vaadin.addons.lazyquerycontainer.QueryDefinition

/**
 *  Created by ronald on 3/07/17.
 */
@CompileStatic
class DocumentsLocationFactory extends BaseQueryFactory<DocumentsLocation> {

    DocumentsLocationFactory(LazyQuery<DocumentsLocation> lazyQuery) {
        super(lazyQuery)
    }

    @Override
    Item constructItem() {
        return constructItem(new DocumentsLocation())
    }

    @Override
    Object createProperty(Object propertyID, DocumentsLocation dataObject) {
        switch (propertyID.toString()) {
            case OBJ:
                return dataObject
            case "location":
                return dataObject.location.toString()
            case "documentsAmount":
                return dataObject.documentsAmount.toString()
        }
        return getDefaultProperty(propertyID)
    }

    @Override
    Query constructQuery(QueryDefinition queryDefinition) {
        this.queryDefinition = queryDefinition
        return new DocumentsLocationQuery(this)
    }
}
