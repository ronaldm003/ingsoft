package edu.utesa.ronald.ingsoft.models.factories

import com.vaadin.data.Item
import edu.utesa.ronald.ingsoft.models.queries.UserLocationQuery
import edu.utesa.ronald.ingsoft.ui.dashboard.components.charts.models.UserLocation
import edu.utesa.ronald.ingsoft.utils.container.BaseQueryFactory
import edu.utesa.ronald.ingsoft.utils.container.LazyQuery
import groovy.transform.CompileStatic
import org.vaadin.addons.lazyquerycontainer.Query
import org.vaadin.addons.lazyquerycontainer.QueryDefinition

/**
 *  Created by ronald on 3/07/17.
 */
@CompileStatic
class UserLocationFactory extends BaseQueryFactory<UserLocation> {

    UserLocationFactory(LazyQuery<UserLocation> lazyQuery) {
        super(lazyQuery)
    }

    @Override
    Item constructItem() {
        return constructItem(new UserLocation())
    }

    @Override
    Object createProperty(Object propertyID, UserLocation dataObject) {
        switch (propertyID.toString()) {
            case OBJ:
                return dataObject
            case "location":
                return dataObject.location.toString()
            case "userAmount":
                return dataObject.userAmount.toString()
        }
        return getDefaultProperty(propertyID)
    }

    @Override
    Query constructQuery(QueryDefinition queryDefinition) {
        this.queryDefinition = queryDefinition
        return new UserLocationQuery(this)
    }
}
