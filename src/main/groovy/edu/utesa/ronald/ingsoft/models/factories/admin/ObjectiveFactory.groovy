package edu.utesa.ronald.ingsoft.models.factories.admin

import com.vaadin.data.Item
import edu.utesa.ronald.ingsoft.domains.admin.Objective
import edu.utesa.ronald.ingsoft.models.queries.admin.ObjectiveQuery
import edu.utesa.ronald.ingsoft.utils.container.BaseQueryFactory
import edu.utesa.ronald.ingsoft.utils.container.LazyQuery
import groovy.transform.CompileStatic
import org.vaadin.addons.lazyquerycontainer.Query
import org.vaadin.addons.lazyquerycontainer.QueryDefinition

/**
 *  Created by victor on 08/10/15.
 */
@CompileStatic
class ObjectiveFactory extends BaseQueryFactory<Objective> {

    ObjectiveFactory(LazyQuery<Objective> lazyQuery) {
        super(lazyQuery)
    }

    @Override
    Item constructItem() {
        return constructItem(new Objective())
    }

    @Override
    Object createProperty(Object propertyID, Objective dataObject) {
        switch (propertyID.toString()) {
            case OBJ:
                return dataObject
            case ID:
                return dataObject.getId()
            case "description":
                return dataObject.description
            case "limitDate":
                return dataObject.limitDate
            case "procedureQuantity":
                return dataObject.procedureQuantity
            case "enabled":
                return dataObject.enabled
        }
        return getDefaultProperty(propertyID)
    }

    @Override
    Query constructQuery(QueryDefinition queryDefinition) {
        this.queryDefinition = queryDefinition
        return new ObjectiveQuery(this)
    }
}
