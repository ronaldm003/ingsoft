package edu.utesa.ronald.ingsoft.models.factories.admin

import com.vaadin.data.Item
import edu.utesa.ronald.ingsoft.domains.admin.Student
import edu.utesa.ronald.ingsoft.models.queries.admin.StudentQuery
import edu.utesa.ronald.ingsoft.services.admin.StudentService
import edu.utesa.ronald.ingsoft.utils.Constants
import edu.utesa.ronald.ingsoft.utils.container.BaseQueryFactory
import edu.utesa.ronald.ingsoft.utils.container.LazyQuery
import groovy.transform.CompileStatic
import org.springframework.beans.factory.annotation.Autowired
import org.vaadin.addons.lazyquerycontainer.Query
import org.vaadin.addons.lazyquerycontainer.QueryDefinition

/**
 *  Created by victor on 08/10/15.
 */
@CompileStatic
class StudentFactory extends BaseQueryFactory<Student> {

    @Autowired
    private StudentService studentService

    StudentFactory(LazyQuery<Student> lazyQuery) {
        super(lazyQuery)
        Constants.instance.autoWiredClass(this)
    }

    @Override
    Item constructItem() {
        return constructItem(new Student())
    }

    @Override
    Object createProperty(Object propertyID, Student dataObject) {
        switch (propertyID.toString()) {
            case OBJ:
                return dataObject
            case ID:
                return dataObject.getId()
            case "name":
                return dataObject.person.name
            case "lastName":
                return dataObject.person.lastName
            case "birthDate":
                return dataObject.person.birthDate
            case "ingressDate":
                return dataObject.person.ingressDate
            case "email":
                return dataObject.person.email
            case "phone":
                return dataObject.person.phone
            case "warrant":
                return dataObject.person.warrant
            case "enrollment":
                return dataObject.enrollment
            case "enabled":
                return dataObject.enabled
        }
        return getDefaultProperty(propertyID)
    }

    @Override
    Query constructQuery(QueryDefinition queryDefinition) {
        this.queryDefinition = queryDefinition
        return new StudentQuery(this)
    }
}
