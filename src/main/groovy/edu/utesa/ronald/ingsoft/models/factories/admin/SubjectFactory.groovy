package edu.utesa.ronald.ingsoft.models.factories.admin

import com.vaadin.data.Item
import edu.utesa.ronald.ingsoft.domains.admin.Subject
import edu.utesa.ronald.ingsoft.models.queries.admin.SubjectQuery
import edu.utesa.ronald.ingsoft.utils.container.BaseQueryFactory
import edu.utesa.ronald.ingsoft.utils.container.LazyQuery
import groovy.transform.CompileStatic
import org.vaadin.addons.lazyquerycontainer.Query
import org.vaadin.addons.lazyquerycontainer.QueryDefinition

/**
 *  Created by victor on 08/10/15.
 */
@CompileStatic
class SubjectFactory extends BaseQueryFactory<Subject> {

    SubjectFactory(LazyQuery<Subject> lazyQuery) {
        super(lazyQuery)
    }

    @Override
    Item constructItem() {
        return constructItem(new Subject())
    }

    @Override
    Object createProperty(Object propertyID, Subject dataObject) {
        switch (propertyID.toString()) {
            case OBJ:
                return dataObject
            case ID:
                return dataObject.getId()
            case "code":
                return dataObject.code
            case "description":
                return dataObject.description
            case "credits":
                return dataObject.credits
            case "teoricHours":
                return dataObject.teoricHours
            case "practicalHours":
                return dataObject.practicalHours
            case "enabled":
                return dataObject.enabled
        }
        return getDefaultProperty(propertyID)
    }

    @Override
    Query constructQuery(QueryDefinition queryDefinition) {
        this.queryDefinition = queryDefinition
        return new SubjectQuery(this)
    }
}
