package edu.utesa.ronald.ingsoft.models.factories.admin

import com.vaadin.data.Item
import edu.utesa.ronald.ingsoft.domains.admin.SubjectSchedule
import edu.utesa.ronald.ingsoft.models.queries.admin.SubjectScheduleQuery
import edu.utesa.ronald.ingsoft.utils.Languages
import edu.utesa.ronald.ingsoft.utils.container.BaseQueryFactory
import edu.utesa.ronald.ingsoft.utils.container.LazyQuery
import groovy.transform.CompileStatic
import org.vaadin.addons.lazyquerycontainer.Query
import org.vaadin.addons.lazyquerycontainer.QueryDefinition

/**
 *  Created by victor on 08/10/15.
 */
@CompileStatic
class SubjectScheduleFactory extends BaseQueryFactory<SubjectSchedule> {

    SubjectScheduleFactory(LazyQuery<SubjectSchedule> lazyQuery) {
        super(lazyQuery)
    }

    @Override
    Item constructItem() {
        return constructItem(new SubjectSchedule())
    }

    @Override
    Object createProperty(Object propertyID, SubjectSchedule dataObject) {
        switch (propertyID.toString()) {
            case OBJ:
                return dataObject
            case ID:
                return dataObject.getId()
            case "day":
                return Languages.instance.dayOfWeek(dataObject.day)
            case "startTime":
                return dataObject.startTime
            case "endTime":
                return dataObject.endTime
            case "enabled":
                return dataObject.enabled
        }
        return getDefaultProperty(propertyID)
    }

    @Override
    Query constructQuery(QueryDefinition queryDefinition) {
        this.queryDefinition = queryDefinition
        return new SubjectScheduleQuery(this)
    }
}
