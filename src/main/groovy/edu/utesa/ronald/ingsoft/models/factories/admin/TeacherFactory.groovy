package edu.utesa.ronald.ingsoft.models.factories.admin

import com.vaadin.data.Item
import edu.utesa.ronald.ingsoft.domains.admin.Teacher
import edu.utesa.ronald.ingsoft.models.queries.admin.TeacherQuery
import edu.utesa.ronald.ingsoft.services.admin.TeacherService
import edu.utesa.ronald.ingsoft.utils.Constants
import edu.utesa.ronald.ingsoft.utils.container.BaseQueryFactory
import edu.utesa.ronald.ingsoft.utils.container.LazyQuery
import groovy.transform.CompileStatic
import org.springframework.beans.factory.annotation.Autowired
import org.vaadin.addons.lazyquerycontainer.Query
import org.vaadin.addons.lazyquerycontainer.QueryDefinition

/**
 *  Created by ronald on 04/12/18.
 */
@CompileStatic
class TeacherFactory extends BaseQueryFactory<Teacher> {

    @Autowired
    private TeacherService teacherService

    TeacherFactory(LazyQuery<Teacher> lazyQuery) {
        super(lazyQuery)
        Constants.instance.autoWiredClass(this)
    }

    @Override
    Item constructItem() {
        return constructItem(new Teacher())
    }

    @Override
    Object createProperty(Object propertyID, Teacher dataObject) {
        switch (propertyID.toString()) {
            case OBJ:
                return dataObject
            case ID:
                return dataObject.getId()
            case "code":
                return dataObject.code
            case "name":
                return dataObject.person.name
            case "lastName":
                return dataObject.person.lastName
            case "birthDate":
                return dataObject.person.birthDate
            case "ingressDate":
                return dataObject.person.ingressDate
            case "email":
                return dataObject.person.email
            case "phone":
                return dataObject.person.phone
            case "warrant":
                return dataObject.person.warrant
        }
        return getDefaultProperty(propertyID)
    }

    @Override
    Query constructQuery(QueryDefinition queryDefinition) {
        this.queryDefinition = queryDefinition
        return new TeacherQuery(this)
    }
}
