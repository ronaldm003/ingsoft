package edu.utesa.ronald.ingsoft.models.factories.control

import com.vaadin.data.Item
import edu.utesa.ronald.ingsoft.domains.control.Article
import edu.utesa.ronald.ingsoft.models.queries.control.ArticleQuery
import edu.utesa.ronald.ingsoft.utils.container.BaseQueryFactory
import edu.utesa.ronald.ingsoft.utils.container.LazyQuery
import groovy.transform.CompileStatic
import org.vaadin.addons.lazyquerycontainer.Query
import org.vaadin.addons.lazyquerycontainer.QueryDefinition

/**
 *  Created by victor on 08/10/15.
 */
@CompileStatic
class ArticleFactory extends BaseQueryFactory<Article> {

    ArticleFactory(LazyQuery<Article> lazyQuery) {
        super(lazyQuery)
    }

    @Override
    Item constructItem() {
        return constructItem(new Article())
    }

    @Override
    Object createProperty(Object propertyID, Article dataObject) {
        switch (propertyID.toString()) {
            case OBJ:
                return dataObject
            case ID:
                return dataObject.getId()
            case "code":
                return dataObject.code
            case "description":
                return dataObject.description
            case "quantity":
                return dataObject.quantity
            case "punceo":
                return dataObject.punceo
            case "cost":
                return dataObject.cost
            case "enabled":
                return dataObject.enabled
        }
        return getDefaultProperty(propertyID)
    }

    @Override
    Query constructQuery(QueryDefinition queryDefinition) {
        this.queryDefinition = queryDefinition
        return new ArticleQuery(this)
    }
}
