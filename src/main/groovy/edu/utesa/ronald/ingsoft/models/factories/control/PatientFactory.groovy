package edu.utesa.ronald.ingsoft.models.factories.control

import com.vaadin.data.Item
import edu.utesa.ronald.ingsoft.domains.control.Patient
import edu.utesa.ronald.ingsoft.models.queries.control.PatientQuery
import edu.utesa.ronald.ingsoft.services.control.PatientService
import edu.utesa.ronald.ingsoft.utils.Constants
import edu.utesa.ronald.ingsoft.utils.container.BaseQueryFactory
import edu.utesa.ronald.ingsoft.utils.container.LazyQuery
import groovy.transform.CompileStatic
import org.springframework.beans.factory.annotation.Autowired
import org.vaadin.addons.lazyquerycontainer.Query
import org.vaadin.addons.lazyquerycontainer.QueryDefinition

/**
 *  Created by victor on 08/10/15.
 */
@CompileStatic
class PatientFactory extends BaseQueryFactory<Patient> {

    @Autowired
    private PatientService patientService

    PatientFactory(LazyQuery<Patient> lazyQuery) {
        super(lazyQuery)
        Constants.instance.autoWiredClass(this)
    }

    @Override
    Item constructItem() {
        return constructItem(new Patient())
    }

    @Override
    Object createProperty(Object propertyID, Patient dataObject) {
        switch (propertyID.toString()) {
            case OBJ:
                return dataObject
            case ID:
                return dataObject.getId()
            case "code":
                return dataObject.code
            case "person":
                return patientService.refresh(dataObject.id, { it.person.enabled }).person.names()
            case "enabled":
                return dataObject.enabled
        }
        return getDefaultProperty(propertyID)
    }

    @Override
    Query constructQuery(QueryDefinition queryDefinition) {
        this.queryDefinition = queryDefinition
        return new PatientQuery(this)
    }
}
