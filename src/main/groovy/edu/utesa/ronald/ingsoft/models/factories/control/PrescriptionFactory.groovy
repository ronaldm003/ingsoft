package edu.utesa.ronald.ingsoft.models.factories.control

import com.vaadin.data.Item
import edu.utesa.ronald.ingsoft.domains.control.Prescription
import edu.utesa.ronald.ingsoft.models.queries.control.PrescriptionQuery
import edu.utesa.ronald.ingsoft.utils.container.BaseQueryFactory
import edu.utesa.ronald.ingsoft.utils.container.LazyQuery
import groovy.transform.CompileStatic
import org.vaadin.addons.lazyquerycontainer.Query
import org.vaadin.addons.lazyquerycontainer.QueryDefinition

/**
 *  Created by victor on 08/10/15.
 */
@CompileStatic
class PrescriptionFactory extends BaseQueryFactory<Prescription> {

    PrescriptionFactory(LazyQuery<Prescription> lazyQuery) {
        super(lazyQuery)
    }

    @Override
    Item constructItem() {
        return constructItem(new Prescription())
    }

    @Override
    Object createProperty(Object propertyID, Prescription dataObject) {
        switch (propertyID.toString()) {
            case OBJ:
                return dataObject
            case ID:
                return dataObject.getId()
            case "enabled":
                return dataObject.enabled
        }
        return getDefaultProperty(propertyID)
    }

    @Override
    Query constructQuery(QueryDefinition queryDefinition) {
        this.queryDefinition = queryDefinition
        return new PrescriptionQuery(this)
    }
}
