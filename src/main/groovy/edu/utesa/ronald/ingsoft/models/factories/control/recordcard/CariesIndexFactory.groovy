package edu.utesa.ronald.ingsoft.models.factories.control.recordcard

import com.vaadin.data.Item
import edu.utesa.ronald.ingsoft.domains.control.recordcard.CariesIndex
import edu.utesa.ronald.ingsoft.models.queries.control.recordcard.CariesIndexQuery
import edu.utesa.ronald.ingsoft.utils.container.BaseQueryFactory
import edu.utesa.ronald.ingsoft.utils.container.LazyQuery
import groovy.transform.CompileStatic
import org.vaadin.addons.lazyquerycontainer.Query
import org.vaadin.addons.lazyquerycontainer.QueryDefinition

/**
 *  Created by victor on 08/10/15.
 */
@CompileStatic
class CariesIndexFactory extends BaseQueryFactory<CariesIndex> {

    CariesIndexFactory(LazyQuery<CariesIndex> lazyQuery) {
        super(lazyQuery)
    }

    @Override
    Item constructItem() {
        return constructItem(new CariesIndex())
    }

    @Override
    Object createProperty(Object propertyID, CariesIndex dataObject) {
        switch (propertyID.toString()) {
            case OBJ:
                return dataObject
            case ID:
                return dataObject.getId()
            case "decayed":
                return dataObject.decayed
            case "lost":
                return dataObject.lost
            case "sealed":
                return dataObject.sealed
            case "teethPresent":
                return dataObject.teethPresent
            case "level":
                return dataObject.level
            case "groupQuantity":
                return dataObject.groupQuantity
            case "date":
                return dataObject.date
            case "enabled":
                return dataObject.enabled
        }
        return getDefaultProperty(propertyID)
    }

    @Override
    Query constructQuery(QueryDefinition queryDefinition) {
        this.queryDefinition = queryDefinition
        return new CariesIndexQuery(this)
    }

}