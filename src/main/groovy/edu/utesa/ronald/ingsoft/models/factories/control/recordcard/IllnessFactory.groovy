package edu.utesa.ronald.ingsoft.models.factories.control.recordcard

import com.vaadin.data.Item
import edu.utesa.ronald.ingsoft.domains.control.recordcard.Illness
import edu.utesa.ronald.ingsoft.models.queries.control.recordcard.IllnessQuery
import edu.utesa.ronald.ingsoft.utils.container.BaseQueryFactory
import edu.utesa.ronald.ingsoft.utils.container.LazyQuery
import groovy.transform.CompileStatic
import org.vaadin.addons.lazyquerycontainer.Query
import org.vaadin.addons.lazyquerycontainer.QueryDefinition

/**
 *  Created by victor on 08/10/15.
 */
@CompileStatic
class IllnessFactory extends BaseQueryFactory<Illness> {

    IllnessFactory(LazyQuery<Illness> lazyQuery) {
        super(lazyQuery)
    }

    @Override
    Item constructItem() {
        return constructItem(new Illness())
    }

    @Override
    Object createProperty(Object propertyID, Illness dataObject) {
        switch (propertyID.toString()) {
            case OBJ:
                return dataObject
            case ID:
                return dataObject.getId()
            case "code":
                return dataObject.code.toString()
            case "description":
                return dataObject.description
            case "enabled":
                return dataObject.enabled
        }
        return getDefaultProperty(propertyID)
    }

    @Override
    Query constructQuery(QueryDefinition queryDefinition) {
        this.queryDefinition = queryDefinition
        return new IllnessQuery(this)
    }

}