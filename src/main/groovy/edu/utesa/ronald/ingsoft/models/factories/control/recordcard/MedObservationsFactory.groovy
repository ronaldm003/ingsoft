package edu.utesa.ronald.ingsoft.models.factories.control.recordcard

import com.vaadin.data.Item
import edu.utesa.ronald.ingsoft.domains.control.recordcard.MedObservations
import edu.utesa.ronald.ingsoft.models.queries.control.recordcard.MedObservationsQuery
import edu.utesa.ronald.ingsoft.utils.container.BaseQueryFactory
import edu.utesa.ronald.ingsoft.utils.container.LazyQuery
import groovy.transform.CompileStatic
import org.vaadin.addons.lazyquerycontainer.Query
import org.vaadin.addons.lazyquerycontainer.QueryDefinition

/**
 *  Created by victor on 08/10/15.
 */
@CompileStatic
class MedObservationsFactory extends BaseQueryFactory<MedObservations> {

    MedObservationsFactory(LazyQuery<MedObservations> lazyQuery) {
        super(lazyQuery)
    }

    @Override
    Item constructItem() {
        return constructItem(new MedObservations())
    }

    @Override
    Object createProperty(Object propertyID, MedObservations dataObject) {
        switch (propertyID.toString()) {
            case OBJ:
                return dataObject
            case ID:
                return dataObject.getId()
            case "observations":
                return dataObject.observations
            case "actualMedicaments":
                return dataObject.actualMedicaments
            case "medicamentName":
                return dataObject.medicamentName
            case "medicamentFor":
                return dataObject.medicamentFor
            case "whoIndicateIt":
                return dataObject.whoIndicateIt
            case "enabled":
                return dataObject.enabled
        }
        return getDefaultProperty(propertyID)
    }

    @Override
    Query constructQuery(QueryDefinition queryDefinition) {
        this.queryDefinition = queryDefinition
        return new MedObservationsQuery(this)
    }

}