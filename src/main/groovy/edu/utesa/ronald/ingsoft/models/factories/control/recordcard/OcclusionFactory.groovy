package edu.utesa.ronald.ingsoft.models.factories.control.recordcard

import com.vaadin.data.Item
import edu.utesa.ronald.ingsoft.domains.control.recordcard.Occlusion
import edu.utesa.ronald.ingsoft.models.queries.control.recordcard.OcclusionQuery
import edu.utesa.ronald.ingsoft.utils.container.BaseQueryFactory
import edu.utesa.ronald.ingsoft.utils.container.LazyQuery
import groovy.transform.CompileStatic
import org.vaadin.addons.lazyquerycontainer.Query
import org.vaadin.addons.lazyquerycontainer.QueryDefinition

/**
 *  Created by victor on 08/10/15.
 */
@CompileStatic
class OcclusionFactory extends BaseQueryFactory<Occlusion> {

    OcclusionFactory(LazyQuery<Occlusion> lazyQuery) {
        super(lazyQuery)
    }

    @Override
    Item constructItem() {
        return constructItem(new Occlusion())
    }

    @Override
    Object createProperty(Object propertyID, Occlusion dataObject) {
        switch (propertyID.toString()) {
            case OBJ:
                return dataObject
            case ID:
                return dataObject.getId()
            case "facialProfile":
                return dataObject.facialProfile.toString()
            case "molarRelation":
                return dataObject.molarRelation
            case "canineRelation":
                return dataObject.canineRelation
            case "eruptionSecuence":
                return dataObject.eruptionSecuence
            case "crossBite":
                return dataObject.crossBite
            case "supernumeraries":
                return dataObject.supernumeraries
            case "congenitalAbsences":
                return dataObject.congenitalAbsences
            case "primateSpaces":
                return dataObject.primateSpaces
            case "middleLine":
                return dataObject.middleLine
            case "mandibularDeviation":
                return dataObject.mandibularDeviation
            case "openBite":
                return dataObject.openBite
            case "overBite":
                return dataObject.overBite
            case "openJet":
                return dataObject.openJet
            case "arcType":
                return dataObject.arcType.toString()
            case "maxilarSup":
                return dataObject.maxilarSup
            case "maxilarInf":
                return dataObject.maxilarInf
            case "enabled":
                return dataObject.enabled
        }
        return getDefaultProperty(propertyID)
    }

    @Override
    Query constructQuery(QueryDefinition queryDefinition) {
        this.queryDefinition = queryDefinition
        return new OcclusionQuery(this)
    }

}