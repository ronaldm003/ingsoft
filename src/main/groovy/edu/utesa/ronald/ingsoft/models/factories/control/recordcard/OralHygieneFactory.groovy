package edu.utesa.ronald.ingsoft.models.factories.control.recordcard

import com.vaadin.data.Item
import edu.utesa.ronald.ingsoft.domains.control.recordcard.OralHygiene
import edu.utesa.ronald.ingsoft.models.queries.control.recordcard.OralHygieneQuery
import edu.utesa.ronald.ingsoft.utils.container.BaseQueryFactory
import edu.utesa.ronald.ingsoft.utils.container.LazyQuery
import groovy.transform.CompileStatic
import org.vaadin.addons.lazyquerycontainer.Query
import org.vaadin.addons.lazyquerycontainer.QueryDefinition

/**
 *  Created by victor on 08/10/15.
 */
@CompileStatic
class OralHygieneFactory extends BaseQueryFactory<OralHygiene> {

    private boolean readOnly = false

    OralHygieneFactory(LazyQuery<OralHygiene> lazyQuery, boolean readOnly) {
        super(lazyQuery)
        this.readOnly = readOnly
    }

    @Override
    Item constructItem() {
        return constructItem(new OralHygiene())
    }

    @Override
    Object createProperty(Object propertyID, OralHygiene dataObject) {
        switch (propertyID.toString()) {
            case OBJ:
                return dataObject
            case ID:
                return dataObject.getId()
            case "brushed":
                return dataObject.brushed
            case "floss":
                return dataObject.floss
            case "mouthwashes":
                return dataObject.mouthwashes
            case "sanitizeTongue":
                return dataObject.sanitizeTongue
            case "enabled":
                return dataObject.enabled
        }
        return getDefaultProperty(propertyID)
    }

    @Override
    Query constructQuery(QueryDefinition queryDefinition) {
        this.queryDefinition = queryDefinition
        return new OralHygieneQuery(this)
    }
}