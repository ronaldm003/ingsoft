package edu.utesa.ronald.ingsoft.models.factories.control.recordcard

import com.vaadin.data.Item
import edu.utesa.ronald.ingsoft.domains.control.recordcard.OralHygieneIndex
import edu.utesa.ronald.ingsoft.models.queries.control.recordcard.OralHygieneIndexQuery
import edu.utesa.ronald.ingsoft.utils.container.BaseQueryFactory
import edu.utesa.ronald.ingsoft.utils.container.LazyQuery
import groovy.transform.CompileStatic
import org.vaadin.addons.lazyquerycontainer.Query
import org.vaadin.addons.lazyquerycontainer.QueryDefinition

/**
 *  Created by victor on 08/10/15.
 */
@CompileStatic
class OralHygieneIndexFactory extends BaseQueryFactory<OralHygieneIndex> {

    OralHygieneIndexFactory(LazyQuery<OralHygieneIndex> lazyQuery) {
        super(lazyQuery)
    }

    @Override
    Item constructItem() {
        return constructItem(new OralHygieneIndex())
    }

    @Override
    Object createProperty(Object propertyID, OralHygieneIndex dataObject) {
        switch (propertyID.toString()) {
            case OBJ:
                return dataObject
            case ID:
                return dataObject.getId()
            case "code":
                return dataObject.code.toString()
            case "tooth":
                return dataObject.tooth
            case "vestib16":
                return dataObject.vestib16
            case "vestib11":
                return dataObject.vestib11
            case "vestib26":
                return dataObject.vestib26
            case "ling36":
                return dataObject.ling36
            case "vestib31":
                return dataObject.vestib31
            case "ling46":
                return dataObject.ling46
            case "total":
                return dataObject.total
            case "indexNumber":
                return dataObject.indexNumber
            case "hygeneLevel":
                return dataObject.hygeneLevel
            case "date":
                return dataObject.date
            case "enabled":
                return dataObject.enabled
        }
        return getDefaultProperty(propertyID)
    }

    @Override
    Query constructQuery(QueryDefinition queryDefinition) {
        this.queryDefinition = queryDefinition
        return new OralHygieneIndexQuery(this)
    }

}