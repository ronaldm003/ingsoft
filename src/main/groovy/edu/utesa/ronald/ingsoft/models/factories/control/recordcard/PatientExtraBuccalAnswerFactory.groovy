package edu.utesa.ronald.ingsoft.models.factories.control.recordcard

import com.vaadin.data.Item
import com.vaadin.event.FieldEvents
import com.vaadin.ui.TextArea
import com.vaadin.ui.themes.ValoTheme
import edu.utesa.ronald.ingsoft.domains.control.recordcard.PatientExtraBuccalAnswer
import edu.utesa.ronald.ingsoft.models.queries.control.recordcard.PatientExtraBuccalAnswerQuery
import edu.utesa.ronald.ingsoft.utils.container.BaseQueryFactory
import edu.utesa.ronald.ingsoft.utils.container.LazyQuery
import groovy.transform.CompileStatic
import org.vaadin.addons.lazyquerycontainer.Query
import org.vaadin.addons.lazyquerycontainer.QueryDefinition

/**
 *  Created by victor on 08/10/15.
 */
@CompileStatic
class PatientExtraBuccalAnswerFactory extends BaseQueryFactory<PatientExtraBuccalAnswer> {

    private boolean readOnly = false

    PatientExtraBuccalAnswerFactory(LazyQuery<PatientExtraBuccalAnswer> lazyQuery, boolean readOnly) {
        super(lazyQuery)
        this.readOnly = readOnly
    }

    @Override
    Item constructItem() {
        return constructItem(new PatientExtraBuccalAnswer())
    }

    @Override
    Object createProperty(Object propertyID, PatientExtraBuccalAnswer dataObject) {
        switch (propertyID.toString()) {
            case OBJ:
                return dataObject
            case ID:
                return dataObject.getId()
            case "extraBuccal":
                return dataObject.extraBuccal.question
            case "answer":
                return createTextArea(dataObject)
            case "enabled":
                return dataObject.enabled
        }
        return getDefaultProperty(propertyID)
    }

    @Override
    Query constructQuery(QueryDefinition queryDefinition) {
        this.queryDefinition = queryDefinition
        return new PatientExtraBuccalAnswerQuery(this)
    }

    private TextArea createTextArea(final PatientExtraBuccalAnswer dataObject) {
        final TextArea textArea = new TextArea("")
        textArea.setRows(2)
        textArea.addStyleName(ValoTheme.TEXTAREA_TINY)
        textArea.setSizeFull()
        textArea.setValue(dataObject.answer ?: "")
        textArea.setReadOnly(readOnly)
        textArea.addTextChangeListener(new FieldEvents.TextChangeListener() {
            @Override
            void textChange(FieldEvents.TextChangeEvent event) {
                dataObject.answer = event.text
            }
        })
        return textArea
    }
}
