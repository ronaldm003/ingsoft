package edu.utesa.ronald.ingsoft.models.factories.control.recordcard

import com.vaadin.data.Item
import com.vaadin.data.Property
import com.vaadin.ui.CheckBox
import edu.utesa.ronald.ingsoft.domains.control.recordcard.PatientIllness
import edu.utesa.ronald.ingsoft.models.queries.control.recordcard.PatientIllnessQuery
import edu.utesa.ronald.ingsoft.utils.container.BaseQueryFactory
import edu.utesa.ronald.ingsoft.utils.container.LazyQuery
import groovy.transform.CompileStatic
import org.vaadin.addons.lazyquerycontainer.Query
import org.vaadin.addons.lazyquerycontainer.QueryDefinition

/**
 *  Created by victor on 08/10/15.
 */
@CompileStatic
class PatientIllnessFactory extends BaseQueryFactory<PatientIllness> {

    private boolean readOnly = false

    PatientIllnessFactory(LazyQuery<PatientIllness> lazyQuery, boolean readOnly) {
        super(lazyQuery)
        this.readOnly = readOnly
    }

    @Override
    Item constructItem() {
        return constructItem(new PatientIllness())
    }

    @Override
    Object createProperty(Object propertyID, PatientIllness dataObject) {
        switch (propertyID.toString()) {
            case OBJ:
                return dataObject
            case ID:
                return dataObject.getId()
            case "illness":
                return dataObject.illness.toString()
            case "state":
                return createCheckBox(dataObject)
            case "enabled":
                return dataObject.enabled
        }
        return getDefaultProperty(propertyID)
    }

    @Override
    Query constructQuery(QueryDefinition queryDefinition) {
        this.queryDefinition = queryDefinition
        return new PatientIllnessQuery(this)
    }

    private CheckBox createCheckBox(PatientIllness dataObject) {
        final CheckBox checkBox = new CheckBox("", dataObject.getState())
        checkBox.setReadOnly(readOnly)
        checkBox.addValueChangeListener(new Property.ValueChangeListener() {
            @Override
            void valueChange(Property.ValueChangeEvent event) {
                dataObject.state = checkBox.getValue()
            }
        })
        return checkBox
    }
}