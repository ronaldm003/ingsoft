package edu.utesa.ronald.ingsoft.models.factories.control.recordcard

import com.vaadin.data.Item
import edu.utesa.ronald.ingsoft.domains.admin.Student
import edu.utesa.ronald.ingsoft.domains.admin.Teacher
import edu.utesa.ronald.ingsoft.domains.control.Patient
import edu.utesa.ronald.ingsoft.domains.control.recordcard.RecordCard
import edu.utesa.ronald.ingsoft.models.queries.control.recordcard.RecordCardQuery
import edu.utesa.ronald.ingsoft.services.control.recordcard.RecordCardService
import edu.utesa.ronald.ingsoft.utils.Constants
import edu.utesa.ronald.ingsoft.utils.container.BaseQueryFactory
import edu.utesa.ronald.ingsoft.utils.container.LazyQuery
import groovy.transform.CompileStatic
import org.springframework.beans.factory.annotation.Autowired
import org.vaadin.addons.lazyquerycontainer.Query
import org.vaadin.addons.lazyquerycontainer.QueryDefinition

@CompileStatic
class RecordCardFactory extends BaseQueryFactory<RecordCard> {

    @Autowired
    RecordCardService recordCardService

    RecordCardFactory(LazyQuery<RecordCard> lazyQuery) {
        super(lazyQuery)
        Constants.instance.autoWiredClass(this)
    }

    @Override
    Item constructItem() {
        return constructItem(new RecordCard())
    }

    @Override
    Object createProperty(Object propertyID, RecordCard dataObject) {
        switch (propertyID.toString()) {
            case OBJ:
                return dataObject
            case ID:
                return dataObject.getId()
            case "cycle":
                try {
                    if (!dataObject.cycle) {
                        return ""
                    }
                    return dataObject.cycle.toString() ?: ""
                } catch (Exception ignored) {
                    return dataObject.cycle.toString() ?: ""
                }
            case "recordId":
                return dataObject.recordId ?: ""
            case "ingressDate":
                return dataObject.ingressDate ?: ""
            case "student":
                try {
                    if (!dataObject.student) {
                        return ""
                    }
                    Student student = recordCardService.refresh(dataObject.id, {
                        it.student.enabled; it.student.person.enabled; it.student.listSubjects.size()
                    })?.student
                    return student ? student.toString() : ""
                } catch (Exception ex) {
                    ex.printStackTrace()
                    Student student = recordCardService.refresh(dataObject.id, {
                        it.student.enabled; it.student.person.enabled; it.student.listSubjects.size()
                    })?.student
                    return student ? student.toString() : ""
                }
            case "patient":
                try {
                    if (!dataObject.patient) {
                        return ""
                    }
                    Patient patient = recordCardService.refresh(dataObject.id, {
                        it.patient.enabled; it.patient.person.enabled
                    })?.patient
                    return patient ? patient.toString() : ""
                } catch (Exception ignored) {
                    Patient patient = recordCardService.refresh(dataObject.id, {
                        it.patient.enabled; it.patient.person.enabled
                    })?.patient
                    return patient ? patient.toString() : ""
                }
            case "teacher":
                try {
                    if (!dataObject.teacher) {
                        return ""
                    }
                    Teacher teacher = recordCardService.refresh(dataObject.id, {
                        it.teacher.enabled; it.teacher.person.enabled
                    })?.teacher
                    return teacher ? teacher.toString() : ""
                } catch (Exception ignored) {
                    Teacher teacher = recordCardService.refresh(dataObject.id, {
                        it.teacher.enabled; it.teacher.person.enabled
                    })?.teacher
                    return teacher ? teacher.toString() : ""
                }
            case "enabled":
                return dataObject.enabled
        }
        return getDefaultProperty(propertyID)
    }

    @Override
    Query constructQuery(QueryDefinition queryDefinition) {
        this.queryDefinition = queryDefinition
        return new RecordCardQuery(this)
    }
}
