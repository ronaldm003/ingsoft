package edu.utesa.ronald.ingsoft.models.factories.control.recordcard

import com.vaadin.data.Item
import edu.utesa.ronald.ingsoft.domains.control.recordcard.Tooth
import edu.utesa.ronald.ingsoft.models.queries.control.recordcard.ToothQuery
import edu.utesa.ronald.ingsoft.utils.container.BaseQueryFactory
import edu.utesa.ronald.ingsoft.utils.container.LazyQuery
import groovy.transform.CompileStatic
import org.vaadin.addons.lazyquerycontainer.Query
import org.vaadin.addons.lazyquerycontainer.QueryDefinition

/**
 *  Created by victor on 08/10/15.
 */
@CompileStatic
class ToothFactory extends BaseQueryFactory<Tooth> {

    ToothFactory(LazyQuery<Tooth> lazyQuery) {
        super(lazyQuery)
    }

    @Override
    Item constructItem() {
        return constructItem(new Tooth())
    }

    @Override
    Object createProperty(Object propertyID, Tooth dataObject) {
        switch (propertyID.toString()) {
            case OBJ:
                return dataObject
            case ID:
                return dataObject.getId()
            case "number":
                return dataObject.number
            case "cOclusal":
                return dataObject.cOclusal
            case "cDistal":
                return dataObject.cDistal
            case "cVestibular":
                return dataObject.cVestibular
            case "cMesial":
                return dataObject.cMesial
            case "cPalatinaSup":
                return dataObject.cPalatinaSup
            case "cLingualInf":
                return dataObject.cLingualInf
            case "child":
                return dataObject.child
            case "enabled":
                return dataObject.enabled
        }
        return getDefaultProperty(propertyID)
    }

    @Override
    Query constructQuery(QueryDefinition queryDefinition) {
        this.queryDefinition = queryDefinition
        return new ToothQuery(this)
    }


}