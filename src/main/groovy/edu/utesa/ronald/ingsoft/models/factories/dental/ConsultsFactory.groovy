package edu.utesa.ronald.ingsoft.models.factories.dental

import com.vaadin.data.Item
import edu.utesa.ronald.ingsoft.domains.control.recordcard.RecordCard
import edu.utesa.ronald.ingsoft.models.queries.dental.ConsultsQuery
import edu.utesa.ronald.ingsoft.services.control.recordcard.RecordCardService
import edu.utesa.ronald.ingsoft.utils.Constants
import edu.utesa.ronald.ingsoft.utils.container.BaseQueryFactory
import edu.utesa.ronald.ingsoft.utils.container.LazyQuery
import groovy.transform.CompileStatic
import org.springframework.beans.factory.annotation.Autowired
import org.vaadin.addons.lazyquerycontainer.Query
import org.vaadin.addons.lazyquerycontainer.QueryDefinition

/**
 *  Created by ronald on 3/07/17.
 */
@CompileStatic
class ConsultsFactory extends BaseQueryFactory<RecordCard> {
    @Autowired
    private RecordCardService recordCardService

    ConsultsFactory(LazyQuery<RecordCard> lazyQuery) {
        super(lazyQuery)
        Constants.instance.autoWiredClass(this)
    }

    @Override
    Item constructItem() {
        return constructItem(new RecordCard())
    }

    @Override
    Object createProperty(Object propertyID, RecordCard dataObject) {
        switch (propertyID.toString()) {
            case OBJ:
                return dataObject
            case "cycle":
                try {
                    if (!dataObject.cycle) {
                        return ""
                    }
                    return dataObject.cycle?.toString()
                } catch (Exception ignored) {
                    return dataObject.cycle?.toString()
                }
            case "recordId":
                return dataObject.recordId
            case "ingressDate":
                return dataObject.ingressDate
            case "student":
                try {
                    if (!dataObject.student) {
                        return ""
                    }
                    return dataObject.student?.toString()
                } catch (Exception ignored) {
                    return dataObject.student?.toString()
                }
            case "patient":
                try {
                    if (!dataObject.patient) {
                        return ""
                    }
                    return dataObject.patient?.toString()
                } catch (Exception ignored) {
                    return dataObject.patient?.toString()
                }
            case "teacher":
                try {
                    if (!dataObject.teacher) {
                        return ""
                    }
                    return dataObject.teacher?.toString()
                } catch (Exception ignored) {
                    return dataObject.teacher?.toString()
                }
        }
        return getDefaultProperty(propertyID)
    }

    @Override
    Query constructQuery(QueryDefinition queryDefinition) {
        this.queryDefinition = queryDefinition
        return new ConsultsQuery(this)
    }
}
