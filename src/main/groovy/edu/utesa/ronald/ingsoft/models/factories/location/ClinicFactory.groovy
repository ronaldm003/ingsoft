package edu.utesa.ronald.ingsoft.models.factories.location

import com.vaadin.data.Item
import edu.utesa.ronald.ingsoft.domains.location.Clinic
import edu.utesa.ronald.ingsoft.models.queries.location.ClinicQuery
import edu.utesa.ronald.ingsoft.utils.container.BaseQueryFactory
import edu.utesa.ronald.ingsoft.utils.container.LazyQuery
import groovy.transform.CompileStatic
import org.vaadin.addons.lazyquerycontainer.Query
import org.vaadin.addons.lazyquerycontainer.QueryDefinition

/**
 *  Created by ronald on 3/07/17.
 */
@CompileStatic
class ClinicFactory extends BaseQueryFactory<Clinic> {

    ClinicFactory(LazyQuery<Clinic> lazyQuery) {
        super(lazyQuery)
    }

    @Override
    Item constructItem() {
        return constructItem(new Clinic())
    }

    @Override
    Object createProperty(Object propertyID, Clinic dataObject) {
        switch (propertyID.toString()) {
            case OBJ:
                return dataObject
            case "code":
                return dataObject.code
            case "description":
                return dataObject.description
            case "location":
                return dataObject.location ? dataObject.location.toString() : ""
            case "inventory":
                return dataObject.inventory ? dataObject.inventory.toString() : ""
            case "enabled":
                return dataObject.enabled
        }
        return getDefaultProperty(propertyID)
    }

    @Override
    Query constructQuery(QueryDefinition queryDefinition) {
        this.queryDefinition = queryDefinition
        return new ClinicQuery(this)
    }
}
