package edu.utesa.ronald.ingsoft.models.factories.location

import com.vaadin.data.Item
import edu.utesa.ronald.ingsoft.domains.location.Location
import edu.utesa.ronald.ingsoft.models.queries.location.LocationQuery
import edu.utesa.ronald.ingsoft.utils.Constants
import edu.utesa.ronald.ingsoft.utils.container.BaseQueryFactory
import edu.utesa.ronald.ingsoft.utils.container.LazyQuery
import groovy.transform.CompileStatic
import org.vaadin.addons.lazyquerycontainer.Query
import org.vaadin.addons.lazyquerycontainer.QueryDefinition

/**
 *  Created by ronald on 3/07/17.
 */
@CompileStatic
class LocationFactory extends BaseQueryFactory<Location> {

    LocationFactory(LazyQuery<Location> lazyQuery) {
        super(lazyQuery)
    }

    @Override
    Item constructItem() {
        return constructItem(new Location())
    }

    @Override
    Object createProperty(Object propertyID, Location dataObject) {
        switch (propertyID.toString()) {
            case OBJ:
                return dataObject
            case "code":
                return dataObject.code
            case "description":
                return dataObject.description
            case "enabled":
                return dataObject.enabled
        }
        return getDefaultProperty(propertyID)
    }

    @Override
    Query constructQuery(QueryDefinition queryDefinition) {
        this.queryDefinition = queryDefinition
        return new LocationQuery(this)
    }
}
