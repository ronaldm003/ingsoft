package edu.utesa.ronald.ingsoft.models.factories.security

import com.vaadin.data.Item
import edu.utesa.ronald.ingsoft.domains.security.Param
import edu.utesa.ronald.ingsoft.models.queries.security.ParamQuery
import edu.utesa.ronald.ingsoft.utils.container.BaseQueryFactory
import edu.utesa.ronald.ingsoft.utils.container.LazyQuery
import groovy.transform.CompileStatic
import org.vaadin.addons.lazyquerycontainer.Query
import org.vaadin.addons.lazyquerycontainer.QueryDefinition

/**
 *  Created by ronald on 4/11/18.
 */
@CompileStatic
class ParamFactory extends BaseQueryFactory<Param> {

    ParamFactory(LazyQuery<Param> lazyQuery) {
        super(lazyQuery)
    }

    @Override
    Item constructItem() {
        return constructItem(new Param())
    }

    @Override
    Object createProperty(Object propertyID, Param dataObject) {
        switch (propertyID.toString()) {
            case OBJ:
                return dataObject
            case ID:
                return dataObject.getId()
            case "enabled":
                return dataObject.enabled
            case "code":
                return dataObject.code
            case "paramValueType":
                return dataObject.paramValueType.toString()
            case "name":
                return dataObject.name
            case "description":
                return dataObject.description
            case "value":
                return dataObject.value
        }
        return getDefaultProperty(propertyID)
    }

    @Override
    Query constructQuery(QueryDefinition queryDefinition) {
        this.queryDefinition = queryDefinition
        return new ParamQuery(this)
    }
}
