package edu.utesa.ronald.ingsoft.models.factories.security

import com.vaadin.data.Item
import edu.utesa.ronald.ingsoft.domains.security.Permission
import edu.utesa.ronald.ingsoft.models.queries.security.PermissionQuery
import edu.utesa.ronald.ingsoft.utils.container.BaseQueryFactory
import edu.utesa.ronald.ingsoft.utils.container.LazyQuery
import groovy.transform.CompileStatic
import org.vaadin.addons.lazyquerycontainer.Query
import org.vaadin.addons.lazyquerycontainer.QueryDefinition

/**
 *  Created by ronald on 4/11/18.
 */
@CompileStatic
class PermissionFactory extends BaseQueryFactory<Permission> {

    PermissionFactory(LazyQuery<Permission> lazyQuery) {
        super(lazyQuery)
    }

    @Override
    Item constructItem() {
        return constructItem(new Permission())
    }

    @Override
    Object createProperty(Object propertyID, Permission dataObject) {
        switch (propertyID.toString()) {
            case OBJ:
                return dataObject
            case ID:
                return dataObject.getId()
            case "enabled":
                return dataObject.enabled
            case "code":
                return dataObject.code
            case "name":
                return dataObject.name
            case "description":
                return dataObject.description
        }
        return getDefaultProperty(propertyID)
    }

    @Override
    Query constructQuery(QueryDefinition queryDefinition) {
        this.queryDefinition = queryDefinition
        return new PermissionQuery(this)
    }
}
