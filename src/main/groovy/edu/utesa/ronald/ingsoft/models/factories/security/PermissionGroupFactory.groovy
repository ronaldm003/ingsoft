package edu.utesa.ronald.ingsoft.models.factories.security

import com.vaadin.data.Item
import edu.utesa.ronald.ingsoft.domains.security.PermissionGroup
import edu.utesa.ronald.ingsoft.models.queries.security.PermissionGroupQuery
import edu.utesa.ronald.ingsoft.utils.container.BaseQueryFactory
import edu.utesa.ronald.ingsoft.utils.container.LazyQuery
import groovy.transform.CompileStatic
import org.vaadin.addons.lazyquerycontainer.Query
import org.vaadin.addons.lazyquerycontainer.QueryDefinition

/**
 *  Created by ronald on 4/11/18.
 */
@CompileStatic
class PermissionGroupFactory extends BaseQueryFactory<PermissionGroup> {

    PermissionGroupFactory(LazyQuery<PermissionGroup> lazyQuery) {
        super(lazyQuery)
    }

    @Override
    Item constructItem() {
        return constructItem(new PermissionGroup())
    }

    @Override
    Object createProperty(Object propertyID, PermissionGroup dataObject) {
        switch (propertyID.toString()) {
            case OBJ:
                return dataObject
            case ID:
                return dataObject.getId()
            case "enabled":
                return dataObject.enabled
            case "name":
                return dataObject.name
            case "description":
                return dataObject.description
            case "listPermissions":
                return dataObject.listPermissions.size()
        }
        return getDefaultProperty(propertyID)
    }

    @Override
    Query constructQuery(QueryDefinition queryDefinition) {
        this.queryDefinition = queryDefinition
        return new PermissionGroupQuery(this)
    }
}
