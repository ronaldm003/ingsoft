package edu.utesa.ronald.ingsoft.models.factories.security

import com.vaadin.data.Item
import edu.utesa.ronald.ingsoft.domains.security.Person
import edu.utesa.ronald.ingsoft.models.queries.security.PersonQuery
import edu.utesa.ronald.ingsoft.services.security.PersonService
import edu.utesa.ronald.ingsoft.utils.Constants
import edu.utesa.ronald.ingsoft.utils.container.BaseQueryFactory
import edu.utesa.ronald.ingsoft.utils.container.LazyQuery
import groovy.transform.CompileStatic
import org.springframework.beans.factory.annotation.Autowired
import org.vaadin.addons.lazyquerycontainer.Query
import org.vaadin.addons.lazyquerycontainer.QueryDefinition

/**
 *  Created by ronald on 3/07/17.
 */
@CompileStatic
class PersonFactory extends BaseQueryFactory<Person> {

    @Autowired
    private PersonService userService

    PersonFactory(LazyQuery<Person> lazyQuery) {
        super(lazyQuery)
        Constants.instance.autoWiredClass(this)
    }

    @Override
    Item constructItem() {
        return constructItem(new Person())
    }

    @Override
    Object createProperty(Object propertyID, Person dataObject) {
        switch (propertyID.toString()) {
            case OBJ:
                return dataObject
            case "name":
                return dataObject.name
            case "location":
                try {
                    if (!dataObject.location) {
                        return ""
                    }
                    return dataObject.location.toString()
                } catch (Exception ignored) {
                    return userService.refresh(dataObject.id).location.toString()
                }
            case "gender":
                return dataObject.gender
            case "cellphone":
                return dataObject.cellphone
            case "phone":
                return dataObject.phone
            case "email":
                return dataObject.email
        }
        return getDefaultProperty(propertyID)
    }

    @Override
    Query constructQuery(QueryDefinition queryDefinition) {
        this.queryDefinition = queryDefinition
        return new PersonQuery(this)
    }
}
