package edu.utesa.ronald.ingsoft.models.factories.security

import com.vaadin.data.Item
import edu.utesa.ronald.ingsoft.domains.security.User
import edu.utesa.ronald.ingsoft.models.queries.security.UserQuery
import edu.utesa.ronald.ingsoft.services.security.UserService
import edu.utesa.ronald.ingsoft.utils.Constants
import edu.utesa.ronald.ingsoft.utils.container.BaseQueryFactory
import edu.utesa.ronald.ingsoft.utils.container.LazyQuery
import groovy.transform.CompileStatic
import org.springframework.beans.factory.annotation.Autowired
import org.vaadin.addons.lazyquerycontainer.Query
import org.vaadin.addons.lazyquerycontainer.QueryDefinition

/**
 *  Created by ronald on 3/07/17.
 */
@CompileStatic
class UserFactory extends BaseQueryFactory<User> {

    @Autowired
    private UserService userService

    UserFactory(LazyQuery<User> lazyQuery) {
        super(lazyQuery)
        Constants.instance.autoWiredClass(this)
    }

    @Override
    Item constructItem() {
        return constructItem(new User())
    }

    @Override
    Object createProperty(Object propertyID, User dataObject) {
        switch (propertyID.toString()) {
            case OBJ:
                return dataObject
            case "name":
                return dataObject.person ? dataObject.person.name : ""
            case "username":
                return dataObject.username ?: ""
            case "location":
                try {
                    if (!dataObject.person.location) {
                        return ""
                    }
                    return dataObject.person.location.toString() ?: ""
                } catch (Exception ignored) {
                    return userService.refresh(dataObject.id).person.location.toString() ?: ""
                }
            case "gender":
                return dataObject.person ? dataObject.person.gender : ""
            case "cellphone":
                return dataObject.person ? dataObject.person.cellphone : ""
            case "phone":
                return dataObject.person ? dataObject.person.phone : ""
            case "email":
                return dataObject.person ? dataObject.person.email : ""
            case "userType":
                return dataObject.userType
            case "admin":
                return dataObject.admin
        }
        return getDefaultProperty(propertyID)
    }

    @Override
    Query constructQuery(QueryDefinition queryDefinition) {
        this.queryDefinition = queryDefinition
        return new UserQuery(this)
    }
}
