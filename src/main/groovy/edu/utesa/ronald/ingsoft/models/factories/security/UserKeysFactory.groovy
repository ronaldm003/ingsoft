package edu.utesa.ronald.ingsoft.models.factories.security

import com.vaadin.data.Item
import edu.utesa.ronald.ingsoft.domains.security.UserKeys
import edu.utesa.ronald.ingsoft.models.queries.security.UserKeysQuery
import edu.utesa.ronald.ingsoft.utils.container.BaseQueryFactory
import edu.utesa.ronald.ingsoft.utils.container.LazyQuery
import groovy.transform.CompileStatic
import org.vaadin.addons.lazyquerycontainer.Query
import org.vaadin.addons.lazyquerycontainer.QueryDefinition

/**
 *  Created by ronald on 3/07/17.
 */
@CompileStatic
class UserKeysFactory extends BaseQueryFactory<UserKeys> {

    UserKeysFactory(LazyQuery<UserKeys> lazyQuery) {
        super(lazyQuery)
    }

    @Override
    Item constructItem() {
        return constructItem(new UserKeys())
    }

    @Override
    Object createProperty(Object propertyID, UserKeys dataObject) {
        switch (propertyID.toString()) {
            case OBJ:
                return dataObject
            case "user":
                return dataObject.user
            case "authorizationKey":
                return dataObject.authorizationKey
            case "secUser":
                return dataObject.secUser
            case "date":
                return dataObject.date
            case "estatus":
                return dataObject.estatus
            case "enabled":
                return dataObject.enabled
        }
        return getDefaultProperty(propertyID)
    }

    @Override
    Query constructQuery(QueryDefinition queryDefinition) {
        this.queryDefinition = queryDefinition
        return new UserKeysQuery(this)
    }
}
