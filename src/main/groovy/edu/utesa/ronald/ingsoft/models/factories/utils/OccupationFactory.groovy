package edu.utesa.ronald.ingsoft.models.factories.utils

import com.vaadin.data.Item
import edu.utesa.ronald.ingsoft.domains.utils.Occupation
import edu.utesa.ronald.ingsoft.models.queries.utils.OccupationQuery
import edu.utesa.ronald.ingsoft.utils.container.BaseQueryFactory
import edu.utesa.ronald.ingsoft.utils.container.LazyQuery
import groovy.transform.CompileStatic
import org.vaadin.addons.lazyquerycontainer.Query
import org.vaadin.addons.lazyquerycontainer.QueryDefinition

/**
 *  Created by ronald on 3/07/17.
 */
@CompileStatic
class OccupationFactory extends BaseQueryFactory<Occupation> {

    OccupationFactory(LazyQuery<Occupation> lazyQuery) {
        super(lazyQuery)
    }

    @Override
    Item constructItem() {
        return constructItem(new Occupation())
    }

    @Override
    Object createProperty(Object propertyID, Occupation dataObject) {
        switch (propertyID.toString()) {
            case OBJ:
                return dataObject
            case "description":
                return dataObject.description
            case "enabled":
                return dataObject.enabled
        }
        return getDefaultProperty(propertyID)
    }

    @Override
    Query constructQuery(QueryDefinition queryDefinition) {
        this.queryDefinition = queryDefinition
        return new OccupationQuery(this)
    }
}
