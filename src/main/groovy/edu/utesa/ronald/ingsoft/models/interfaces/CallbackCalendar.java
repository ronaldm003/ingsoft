package edu.utesa.ronald.ingsoft.models.interfaces;

import java.util.Date;

/**
 * Created by ronald on 7/26/16.
 */
public interface CallbackCalendar {
    void findEvents(Date start, Date end);
}
