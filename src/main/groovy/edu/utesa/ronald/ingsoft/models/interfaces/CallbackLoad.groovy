package edu.utesa.ronald.ingsoft.models.interfaces

/**
 *  Created by ronald on 3/7/17.
 */
interface CallbackLoad {
    void complete();
}
