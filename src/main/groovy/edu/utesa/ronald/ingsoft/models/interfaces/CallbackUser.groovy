package edu.utesa.ronald.ingsoft.models.interfaces

/**
 *  Created by ronald on 2/26/17.
 */
interface CallbackUser {
    void onDialogResult();
}
