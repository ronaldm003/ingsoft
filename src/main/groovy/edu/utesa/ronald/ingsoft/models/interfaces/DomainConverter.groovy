package edu.utesa.ronald.ingsoft.models.interfaces

interface DomainConverter<DOMAIN> {
    DOMAIN toDomain()
}