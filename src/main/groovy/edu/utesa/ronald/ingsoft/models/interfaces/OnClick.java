package edu.utesa.ronald.ingsoft.models.interfaces;

import com.vaadin.ui.Component;

/**
 * Created by ronald on 4/20/17.
 */
public interface OnClick {

    Component onClick();
}
