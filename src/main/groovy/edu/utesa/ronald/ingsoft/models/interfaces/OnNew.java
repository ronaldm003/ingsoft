package edu.utesa.ronald.ingsoft.models.interfaces;

/**
 * En caso de un new en una ventana.
 * <p>
 * Created by ronald on 9/08/17.
 */
public interface OnNew<T> {

    void result(T t);
}
