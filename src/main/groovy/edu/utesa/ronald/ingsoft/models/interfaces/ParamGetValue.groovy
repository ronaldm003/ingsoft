package edu.utesa.ronald.ingsoft.models.interfaces

import groovy.transform.CompileStatic

/**
 *  Created by ronald on 2/26/17.
 */
@CompileStatic
interface ParamGetValue<T> {

    String getValue()
}
