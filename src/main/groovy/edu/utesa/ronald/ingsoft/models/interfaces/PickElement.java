package edu.utesa.ronald.ingsoft.models.interfaces;

import java.util.List;

/**
 * Created by ronald on 7/21/17.
 */
public interface PickElement<T> {

    void onResult(List<T> elements);
}
