package edu.utesa.ronald.ingsoft.models.interfaces

import com.vaadin.ui.Component
import groovy.transform.CompileStatic

/**
 *  Created by ronald on03/18/17.
 */
@CompileStatic
interface PrintElement {

    Component print()
}
