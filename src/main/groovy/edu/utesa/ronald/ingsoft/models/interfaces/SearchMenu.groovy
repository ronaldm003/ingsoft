package edu.utesa.ronald.ingsoft.models.interfaces

import groovy.transform.CompileStatic

/**
 * Busqueda por caption.
 * Busqueda por descripción.
 * Busqueda por valores.
 *
 * Created by ronald on 12/4/18.
 */
@CompileStatic
interface SearchMenu {

    boolean onSearch(String search)

    void clearSearch()
}