package edu.utesa.ronald.ingsoft.models.interfaces;

public interface WindowStartFocus {

    void startFocus();
}
