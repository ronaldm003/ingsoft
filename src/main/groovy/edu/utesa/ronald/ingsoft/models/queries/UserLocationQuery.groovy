package edu.utesa.ronald.ingsoft.models.queries

import edu.utesa.ronald.ingsoft.ui.dashboard.components.charts.models.UserLocation
import edu.utesa.ronald.ingsoft.utils.container.BaseQuery
import edu.utesa.ronald.ingsoft.utils.container.BaseQueryFactory
import groovy.transform.CompileStatic

/**
 *  Created by ronald on 3/07/17.
 */
@CompileStatic
class UserLocationQuery extends BaseQuery<UserLocation> {

    UserLocationQuery(BaseQueryFactory<UserLocation> baseQueryFactory) {
        super(baseQueryFactory)
    }
}
