package edu.utesa.ronald.ingsoft.models.queries.admin

import edu.utesa.ronald.ingsoft.domains.admin.Cycle
import edu.utesa.ronald.ingsoft.utils.container.BaseQuery
import edu.utesa.ronald.ingsoft.utils.container.BaseQueryFactory
import groovy.transform.CompileStatic

/**
 *  Created by ronald on 3/07/17.
 */
@CompileStatic
class CycleQuery extends BaseQuery<Cycle> {

    CycleQuery(BaseQueryFactory<Cycle> baseQueryFactory) {
        super(baseQueryFactory)
    }
}
