package edu.utesa.ronald.ingsoft.models.queries.admin

import edu.utesa.ronald.ingsoft.domains.admin.Objective
import edu.utesa.ronald.ingsoft.utils.container.BaseQuery
import edu.utesa.ronald.ingsoft.utils.container.BaseQueryFactory
import groovy.transform.CompileStatic

/**
 *  Created by ronald on 3/07/17.
 */
@CompileStatic
class ObjectiveQuery extends BaseQuery<Objective> {

    ObjectiveQuery(BaseQueryFactory<Objective> baseQueryFactory) {
        super(baseQueryFactory)
    }
}
