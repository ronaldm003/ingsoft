package edu.utesa.ronald.ingsoft.models.queries.admin

import edu.utesa.ronald.ingsoft.domains.admin.Student
import edu.utesa.ronald.ingsoft.utils.container.BaseQuery
import edu.utesa.ronald.ingsoft.utils.container.BaseQueryFactory
import groovy.transform.CompileStatic

/**
 *  Created by ronald on 3/07/17.
 */
@CompileStatic
class StudentQuery extends BaseQuery<Student> {

    StudentQuery(BaseQueryFactory<Student> baseQueryFactory) {
        super(baseQueryFactory)
    }
}
