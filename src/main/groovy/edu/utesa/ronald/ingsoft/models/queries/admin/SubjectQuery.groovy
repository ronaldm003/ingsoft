package edu.utesa.ronald.ingsoft.models.queries.admin

import edu.utesa.ronald.ingsoft.domains.admin.Subject
import edu.utesa.ronald.ingsoft.utils.container.BaseQuery
import edu.utesa.ronald.ingsoft.utils.container.BaseQueryFactory
import groovy.transform.CompileStatic

/**
 *  Created by ronald on 3/07/17.
 */
@CompileStatic
class SubjectQuery extends BaseQuery<Subject> {

    SubjectQuery(BaseQueryFactory<Subject> baseQueryFactory) {
        super(baseQueryFactory)
    }
}
