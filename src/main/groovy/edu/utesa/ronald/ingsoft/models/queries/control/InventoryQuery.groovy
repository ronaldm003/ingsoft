package edu.utesa.ronald.ingsoft.models.queries.control

import edu.utesa.ronald.ingsoft.domains.control.Inventory
import edu.utesa.ronald.ingsoft.utils.container.BaseQuery
import edu.utesa.ronald.ingsoft.utils.container.BaseQueryFactory
import groovy.transform.CompileStatic

/**
 *  Created by ronald on 3/07/17.
 */
@CompileStatic
class InventoryQuery extends BaseQuery<Inventory> {

    InventoryQuery(BaseQueryFactory<Inventory> baseQueryFactory) {
        super(baseQueryFactory)
    }
}
