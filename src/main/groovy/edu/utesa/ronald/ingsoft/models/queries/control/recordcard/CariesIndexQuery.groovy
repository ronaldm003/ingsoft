package edu.utesa.ronald.ingsoft.models.queries.control.recordcard

import edu.utesa.ronald.ingsoft.domains.control.recordcard.CariesIndex
import edu.utesa.ronald.ingsoft.utils.container.BaseQuery
import edu.utesa.ronald.ingsoft.utils.container.BaseQueryFactory
import groovy.transform.CompileStatic

/**
 *  Created by ronald on 3/07/17.
 */
@CompileStatic
class CariesIndexQuery extends BaseQuery<CariesIndex> {

    CariesIndexQuery(BaseQueryFactory<CariesIndex> baseQueryFactory) {
        super(baseQueryFactory)
    }
}
