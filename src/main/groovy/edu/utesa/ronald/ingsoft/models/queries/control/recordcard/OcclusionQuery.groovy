package edu.utesa.ronald.ingsoft.models.queries.control.recordcard

import edu.utesa.ronald.ingsoft.domains.control.recordcard.Occlusion
import edu.utesa.ronald.ingsoft.utils.container.BaseQuery
import edu.utesa.ronald.ingsoft.utils.container.BaseQueryFactory
import groovy.transform.CompileStatic

/**
 *  Created by ronald on 3/07/17.
 */
@CompileStatic
class OcclusionQuery extends BaseQuery<Occlusion> {

    OcclusionQuery(BaseQueryFactory<Occlusion> baseQueryFactory) {
        super(baseQueryFactory)
    }
}
