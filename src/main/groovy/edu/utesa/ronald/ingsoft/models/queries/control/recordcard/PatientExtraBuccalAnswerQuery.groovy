package edu.utesa.ronald.ingsoft.models.queries.control.recordcard

import edu.utesa.ronald.ingsoft.domains.control.recordcard.PatientExtraBuccalAnswer
import edu.utesa.ronald.ingsoft.utils.container.BaseQuery
import edu.utesa.ronald.ingsoft.utils.container.BaseQueryFactory
import groovy.transform.CompileStatic

/**
 *  Created by ronald on 3/07/17.
 */
@CompileStatic
class PatientExtraBuccalAnswerQuery extends BaseQuery<PatientExtraBuccalAnswer> {

    PatientExtraBuccalAnswerQuery(BaseQueryFactory<PatientExtraBuccalAnswer> baseQueryFactory) {
        super(baseQueryFactory)
    }
}
