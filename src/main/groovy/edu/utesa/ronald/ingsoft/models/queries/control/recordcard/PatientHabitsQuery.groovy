package edu.utesa.ronald.ingsoft.models.queries.control.recordcard

import edu.utesa.ronald.ingsoft.domains.control.recordcard.PatientHabits
import edu.utesa.ronald.ingsoft.utils.container.BaseQuery
import edu.utesa.ronald.ingsoft.utils.container.BaseQueryFactory
import groovy.transform.CompileStatic

/**
 *  Created by ronald on 3/07/17.
 */
@CompileStatic
class PatientHabitsQuery extends BaseQuery<PatientHabits> {

    PatientHabitsQuery(BaseQueryFactory<PatientHabits> baseQueryFactory) {
        super(baseQueryFactory)
    }
}
