package edu.utesa.ronald.ingsoft.models.queries.control.recordcard

import edu.utesa.ronald.ingsoft.domains.control.recordcard.Tooth
import edu.utesa.ronald.ingsoft.utils.container.BaseQuery
import edu.utesa.ronald.ingsoft.utils.container.BaseQueryFactory
import groovy.transform.CompileStatic

/**
 *  Created by ronald on 3/07/17.
 */
@CompileStatic
class ToothQuery extends BaseQuery<Tooth> {

    ToothQuery(BaseQueryFactory<Tooth> baseQueryFactory) {
        super(baseQueryFactory)
    }
}
