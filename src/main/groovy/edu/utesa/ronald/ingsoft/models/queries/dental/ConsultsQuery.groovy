package edu.utesa.ronald.ingsoft.models.queries.dental

import edu.utesa.ronald.ingsoft.domains.control.recordcard.RecordCard
import edu.utesa.ronald.ingsoft.utils.container.BaseQuery
import edu.utesa.ronald.ingsoft.utils.container.BaseQueryFactory
import groovy.transform.CompileStatic

/**
 *  Created by ronald on 3/07/17.
 */
@CompileStatic
class ConsultsQuery extends BaseQuery<RecordCard> {

    ConsultsQuery(BaseQueryFactory<RecordCard> baseQueryFactory) {
        super(baseQueryFactory)
    }
}
