package edu.utesa.ronald.ingsoft.models.queries.location

import edu.utesa.ronald.ingsoft.domains.location.Clinic
import edu.utesa.ronald.ingsoft.utils.container.BaseQuery
import edu.utesa.ronald.ingsoft.utils.container.BaseQueryFactory
import groovy.transform.CompileStatic

/**
 *  Created by ronald on 3/07/17.
 */
@CompileStatic
class ClinicQuery extends BaseQuery<Clinic> {

    ClinicQuery(BaseQueryFactory<Clinic> baseQueryFactory) {
        super(baseQueryFactory)
    }
}
