package edu.utesa.ronald.ingsoft.models.queries.location

import edu.utesa.ronald.ingsoft.domains.location.Location
import edu.utesa.ronald.ingsoft.utils.container.BaseQuery
import edu.utesa.ronald.ingsoft.utils.container.BaseQueryFactory
import groovy.transform.CompileStatic

/**
 *  Created by ronald on 3/07/17.
 */
@CompileStatic
class LocationQuery extends BaseQuery<Location> {

    LocationQuery(BaseQueryFactory<Location> baseQueryFactory) {
        super(baseQueryFactory)
    }
}
