package edu.utesa.ronald.ingsoft.models.queries.security

import edu.utesa.ronald.ingsoft.domains.security.PermissionGroup
import edu.utesa.ronald.ingsoft.utils.container.BaseQuery
import edu.utesa.ronald.ingsoft.utils.container.BaseQueryFactory
import groovy.transform.CompileStatic

/**
 *  Created by ronald on 4/11/18.
 */
@CompileStatic
class PermissionGroupQuery extends BaseQuery<PermissionGroup> {

    PermissionGroupQuery(BaseQueryFactory baseQueryFactory) {
        super(baseQueryFactory)
    }
}
