package edu.utesa.ronald.ingsoft.models.queries.security

import edu.utesa.ronald.ingsoft.domains.security.Permission
import edu.utesa.ronald.ingsoft.utils.container.BaseQuery
import edu.utesa.ronald.ingsoft.utils.container.BaseQueryFactory
import groovy.transform.CompileStatic

/**
 *  Created by ronald on 4/11/18.
 */
@CompileStatic
class PermissionQuery extends BaseQuery<Permission> {

    PermissionQuery(BaseQueryFactory baseQueryFactory) {
        super(baseQueryFactory)
    }
}
