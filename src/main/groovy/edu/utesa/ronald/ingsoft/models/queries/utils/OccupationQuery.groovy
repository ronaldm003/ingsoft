package edu.utesa.ronald.ingsoft.models.queries.utils

import edu.utesa.ronald.ingsoft.domains.utils.Occupation
import edu.utesa.ronald.ingsoft.utils.container.BaseQuery
import edu.utesa.ronald.ingsoft.utils.container.BaseQueryFactory
import groovy.transform.CompileStatic

/**
 *  Created by ronald on 3/07/17.
 */
@CompileStatic
class OccupationQuery extends BaseQuery<Occupation> {

    OccupationQuery(BaseQueryFactory<Occupation> baseQueryFactory) {
        super(baseQueryFactory)
    }
}
