package edu.utesa.ronald.ingsoft.models.suggestions

import com.vaadin.server.VaadinSession
import edu.utesa.ronald.ingsoft.domains.control.Article
import edu.utesa.ronald.ingsoft.models.LoginManager
import edu.utesa.ronald.ingsoft.services.control.ArticleService
import edu.utesa.ronald.ingsoft.utils.Constants
import eu.maxschuster.vaadin.autocompletetextfield.AutocompleteQuery
import eu.maxschuster.vaadin.autocompletetextfield.AutocompleteSuggestion
import eu.maxschuster.vaadin.autocompletetextfield.AutocompleteSuggestionProvider
import groovy.transform.CompileStatic
import org.springframework.beans.factory.annotation.Autowired

/**
 *  Created by ronald on 8/5/17.
 */
@CompileStatic
class ArticleSuggestions implements AutocompleteSuggestionProvider {

    @Autowired
    private ArticleService articleService

    private LoginManager loginManager

    ArticleSuggestions() {
        Constants.instance.autoWiredClass(this)
        loginManager = (LoginManager) VaadinSession.current.session.getAttribute(Constants.USER_LOGIN)
    }

    @Override
    Collection<AutocompleteSuggestion> querySuggestions(AutocompleteQuery query) {
        List<AutocompleteSuggestion> suggestions = new ArrayList<>()
        String term = query.getTerm()
        int limit = query.getLimit()
        for (Article article : articleService.list(term, limit)) {
            suggestions.add(new AutocompleteSuggestion(article.description, article.id.toString()))
        }
        return suggestions
    }
}
