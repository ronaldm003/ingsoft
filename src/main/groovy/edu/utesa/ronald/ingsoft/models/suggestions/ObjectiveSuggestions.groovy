package edu.utesa.ronald.ingsoft.models.suggestions

import com.vaadin.server.VaadinSession
import edu.utesa.ronald.ingsoft.domains.admin.Objective
import edu.utesa.ronald.ingsoft.models.LoginManager
import edu.utesa.ronald.ingsoft.services.admin.ObjectiveService
import edu.utesa.ronald.ingsoft.utils.Constants
import eu.maxschuster.vaadin.autocompletetextfield.AutocompleteQuery
import eu.maxschuster.vaadin.autocompletetextfield.AutocompleteSuggestion
import eu.maxschuster.vaadin.autocompletetextfield.AutocompleteSuggestionProvider
import groovy.transform.CompileStatic
import org.springframework.beans.factory.annotation.Autowired

/**
 *  Created by ronald on 8/5/17.
 */
@CompileStatic
class ObjectiveSuggestions implements AutocompleteSuggestionProvider {

    @Autowired
    private ObjectiveService objectiveService

    private LoginManager loginManager

    ObjectiveSuggestions() {
        Constants.instance.autoWiredClass(this)
        loginManager = (LoginManager) VaadinSession.current.session.getAttribute(Constants.USER_LOGIN)
    }

    @Override
    Collection<AutocompleteSuggestion> querySuggestions(AutocompleteQuery query) {
        List<AutocompleteSuggestion> suggestions = new ArrayList<>()
        String term = query.getTerm()
        int limit = query.getLimit()
        for (Objective objective : objectiveService.list(term, limit)) {
            suggestions.add(new AutocompleteSuggestion(objective.description, objective.id.toString()))
        }
        return suggestions
    }
}
