package edu.utesa.ronald.ingsoft.models.suggestions

import com.vaadin.server.VaadinSession
import edu.utesa.ronald.ingsoft.domains.control.Patient
import edu.utesa.ronald.ingsoft.models.LoginManager
import edu.utesa.ronald.ingsoft.services.control.PatientService
import edu.utesa.ronald.ingsoft.utils.Constants
import eu.maxschuster.vaadin.autocompletetextfield.AutocompleteQuery
import eu.maxschuster.vaadin.autocompletetextfield.AutocompleteSuggestion
import eu.maxschuster.vaadin.autocompletetextfield.AutocompleteSuggestionProvider
import groovy.transform.CompileStatic
import org.springframework.beans.factory.annotation.Autowired

/**
 *  Created by ronald on 8/5/17.
 */
@CompileStatic
class PatientSuggestions implements AutocompleteSuggestionProvider {

    @Autowired
    private PatientService patientService

    private LoginManager loginManager

    PatientSuggestions() {
        Constants.instance.autoWiredClass(this)
        loginManager = (LoginManager) VaadinSession.current.session.getAttribute(Constants.USER_LOGIN)
    }

    @Override
    Collection<AutocompleteSuggestion> querySuggestions(AutocompleteQuery query) {
        List<AutocompleteSuggestion> suggestions = new ArrayList<>()
        String term = query.getTerm()
        int limit = query.getLimit()
        for (Patient patient : patientService.list(term, limit)) {
            suggestions.add(new AutocompleteSuggestion(patient.person.name, patient.id.toString()))
        }
        return suggestions
    }
}
