package edu.utesa.ronald.ingsoft.models.suggestions

import com.vaadin.server.VaadinSession
import edu.utesa.ronald.ingsoft.domains.security.User
import edu.utesa.ronald.ingsoft.models.LoginManager
import edu.utesa.ronald.ingsoft.services.security.UserService
import edu.utesa.ronald.ingsoft.utils.Constants
import eu.maxschuster.vaadin.autocompletetextfield.AutocompleteQuery
import eu.maxschuster.vaadin.autocompletetextfield.AutocompleteSuggestion
import eu.maxschuster.vaadin.autocompletetextfield.AutocompleteSuggestionProvider
import groovy.transform.CompileStatic
import org.springframework.beans.factory.annotation.Autowired

/**
 *  Created by ronald on 8/5/17.
 */
@CompileStatic
class UserSuggestions implements AutocompleteSuggestionProvider {

    @Autowired
    private UserService entityService

    private LoginManager loginManager

    UserSuggestions() {
        Constants.instance.autoWiredClass(this)
        loginManager = (LoginManager) VaadinSession.current.session.getAttribute(Constants.USER_LOGIN)
    }

    @Override
    Collection<AutocompleteSuggestion> querySuggestions(AutocompleteQuery query) {
        List<AutocompleteSuggestion> suggestions = new ArrayList<>()
        String term = query.getTerm()
        int limit = query.getLimit()
        for (User user : entityService.list(term, limit)) {
            suggestions.add(new AutocompleteSuggestion(user.toString()))
        }
        return suggestions
    }
}
