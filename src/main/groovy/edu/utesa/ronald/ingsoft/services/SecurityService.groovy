package edu.utesa.ronald.ingsoft.services

import edu.utesa.ronald.ingsoft.domains.security.User
import edu.utesa.ronald.ingsoft.models.LoginManager
import edu.utesa.ronald.ingsoft.utils.TouchableDomain
import edu.utesa.ronald.ingsoft.utils.security.HashUtils
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional

import java.util.function.Consumer

/**
 *  Created by ronald on 7/4/17.
 */
@SuppressWarnings("GrMethodMayBeStatic")
@Service("securityService")
@Transactional(rollbackFor = Exception.class)
class SecurityService {

    LoginManager authenticate(String username, String password) {
        password = HashUtils.encodeSHA256(password)
        LoginManager loginManager = new LoginManager()
        User user = authenticateUser(username, password, {
            it.person.enabled;
            it.listPermission.size()
        })
        if (user) {
            loginManager.setUser(user)
            return loginManager
        }
        return null
    }

    private static User authenticateUser(String username, String password, Consumer<User> touch = null) {
        User user = User.findByUsernameAndPasswordAndEnabled(username, password, true)
        TouchableDomain.applyTouch(user, touch)
        return user
    }
}
