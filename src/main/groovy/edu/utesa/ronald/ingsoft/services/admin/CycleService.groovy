package edu.utesa.ronald.ingsoft.services.admin

import edu.utesa.ronald.ingsoft.domains.admin.Cycle
import edu.utesa.ronald.ingsoft.models.LoginManager
import edu.utesa.ronald.ingsoft.utils.Constants
import edu.utesa.ronald.ingsoft.utils.TouchableDomain
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional

import java.util.function.Consumer

/**
 *  Created by Ronald on 16/11/17.
 */
@SuppressWarnings("GrMethodMayBeStatic")
@Service("cycleService")
@Transactional(rollbackFor = Exception.class)
class CycleService {

    private Cycle createBase(Cycle cycle, String userName) {
        if (cycle != null && !cycle.hasErrors()) {
            if (!Cycle.exists(cycle.getId())) {
                cycle.dateCreated = new Date()
                cycle.createdBy = userName
            }
            cycle.lastUpdated = new Date()
            cycle.modifyBy = userName
            return cycle.save(flush: true, failOnError: true)
        }
        return null
    }

    Cycle bootStrap(Cycle cycle) {
        return createBase(cycle, Constants.ROOT)
    }

    Cycle create(Cycle cycle, LoginManager loginManager) {
        return createBase(cycle, loginManager.getUsername())
    }

    List<Cycle> list(boolean enabled, int start, int size, Consumer<Cycle> touch = null) {
        List<Cycle> cycleList = Cycle.withCriteria {
            firstResult(start)
            maxResults(size)
            order("id", "desc")
            eq "enabled", enabled
        }.findAll()
        TouchableDomain.applyTouch(cycleList, touch)
        return cycleList
    }

    List<Cycle> list(boolean enabled, Consumer<Cycle> touch = null) {
        List<Cycle> cycleList = Cycle.withCriteria {
            order("id", "desc")
            eq "enabled", enabled
        }.findAll()
        TouchableDomain.applyTouch(cycleList, touch)
        return cycleList
    }

    List<Cycle> list(String description, int size, Consumer<Cycle> touch = null) {
        List<Cycle> cycleList = Cycle.withCriteria {
            if (size > 0) {
                maxResults(size)
            }
            order("id", "desc")
            like "description", "%" + description + "%"
            eq "enabled", true
        }.findAll()
        TouchableDomain.applyTouch(cycleList, touch)
        return cycleList
    }

    List<Cycle> list(boolean enabled, boolean admin, Consumer<Cycle> touch = null) {
        List<Cycle> cycleList = Cycle.withCriteria {
            order("id", "desc")
            eq "enabled", enabled
        }.findAll()
        TouchableDomain.applyTouch(cycleList, touch)
        return cycleList
    }

    int count(boolean enabled) {
        return Cycle.countByEnabled(enabled)
    }

    Cycle refresh(Long id, Consumer<Cycle> touch = null) {
        Cycle cycle = Cycle.findById(id)
        TouchableDomain.applyTouch(cycle, touch)
        return cycle
    }

    Cycle byId(Long id, Consumer<Cycle> touch = null) {
        Cycle cycle = Cycle.findById(id)
        TouchableDomain.applyTouch(cycle, touch)
        return cycle
    }

    Cycle byCode(String code, Consumer<Cycle> touch = null) {
        Cycle cycle = Cycle.findByCode(code)
        TouchableDomain.applyTouch(cycle, touch)
        return cycle
    }

    Cycle delete(Cycle cycle, LoginManager loginManager) {
        if (!cycle) {
            return null
        }
        cycle = Cycle.findById(cycle.id)
        cycle.enabled = !cycle.enabled
        return create(cycle, loginManager)
    }

    String findMaxCode() {
        Cycle cycle = Cycle.withCriteria {
            order("id", "desc")
        }.find() as Cycle
        if (!cycle) {
            return ""
        }
        return cycle.code.toString()
    }

    Cycle findLatestCycle(Consumer<Cycle> touch = null) {
        Cycle cycle = Cycle.withCriteria {
            order("id", "desc")
        }.find() as Cycle
        if (!cycle) {
            return null
        }
        TouchableDomain.applyTouch(cycle, touch)
        return cycle
    }

    Cycle byDescription(String description, Consumer<Cycle> touch = null) {
        Cycle cycle = Cycle.findByDescriptionAndEnabled(description, true)
        TouchableDomain.applyTouch(cycle, touch)
        return cycle
    }

}
