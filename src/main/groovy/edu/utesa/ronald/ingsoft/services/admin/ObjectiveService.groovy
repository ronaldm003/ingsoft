package edu.utesa.ronald.ingsoft.services.admin

import edu.utesa.ronald.ingsoft.domains.admin.Objective
import edu.utesa.ronald.ingsoft.models.LoginManager
import edu.utesa.ronald.ingsoft.utils.Constants
import edu.utesa.ronald.ingsoft.utils.TouchableDomain
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional

import java.util.function.Consumer

/**
 *  Created by Ronald on 16/11/17.
 */
@SuppressWarnings("GrMethodMayBeStatic")
@Service("objectiveService")
@Transactional(rollbackFor = Exception.class)
class ObjectiveService {

    private Objective createBase(Objective objective, String userName) {
        if (objective != null && !objective.hasErrors()) {
            if (!Objective.exists(objective.getId())) {
                objective.dateCreated = new Date()
                objective.createdBy = userName
            }
            objective.lastUpdated = new Date()
            objective.modifyBy = userName
            return objective.save(flush: true, failOnError: true)
        }
        return null
    }

    Objective bootStrap(Objective objective) {
        return createBase(objective, Constants.ROOT)
    }

    Objective create(Objective objective, LoginManager loginManager) {
        return createBase(objective, loginManager.getUsername())
    }

    List<Objective> list(boolean enabled, int start, int size, Consumer<Objective> touch = null) {
        List<Objective> objectiveList = Objective.withCriteria {
            firstResult(start)
            maxResults(size)
            order("id", "desc")
            eq "enabled", enabled
        }.findAll()
        TouchableDomain.applyTouch(objectiveList, touch)
        return objectiveList
    }

    List<Objective> list(boolean enabled, Consumer<Objective> touch = null) {
        List<Objective> objectiveList = Objective.withCriteria {
            order("id", "desc")
            eq "enabled", enabled
        }.findAll()
        TouchableDomain.applyTouch(objectiveList, touch)
        return objectiveList
    }

    List<Objective> list(String description, int size, Consumer<Objective> touch = null) {
        List<Objective> objectiveList = Objective.withCriteria {
            if (size > 0) {
                maxResults(size)
            }
            order("id", "desc")
            like "description", "%" + description + "%"
            eq "enabled", true
        }.findAll()
        TouchableDomain.applyTouch(objectiveList, touch)
        return objectiveList
    }

    List<Objective> list(boolean enabled, boolean admin) {
        return Objective.withCriteria {
            order("id", "desc")
            eq "enabled", enabled
        }.findAll()
    }

    int count(boolean enabled) {
        return Objective.countByEnabled(enabled)
    }

    Objective refresh(Long id) {
        Objective objective = Objective.findById(id)
        objective.listArticle.size()
        return objective
    }

    Objective byId(Long id) {
        return Objective.findById(id)
    }

    Objective byCode(Long code) {
        return Objective.findByCode(code)
    }

    Objective delete(Objective objective, LoginManager loginManager) {
        if (!objective) {
            return null
        }
        objective = Objective.findById(objective.id)
        objective.enabled = !objective.enabled
        return create(objective, loginManager)
    }

    Long findMaxCode() {
        Objective objective = Objective.withCriteria {
            order("id", "desc")
        }.find() as Objective
        if (!objective) {
            return 0
        }
        return objective.code
    }

    Objective byDescription(String description, Consumer<Objective> touch = null) {
        Objective objective = Objective.findByDescriptionAndEnabled(description, true)
        TouchableDomain.applyTouch(objective, touch)
        return objective
    }

}
