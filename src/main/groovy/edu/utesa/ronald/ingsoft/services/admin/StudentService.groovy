package edu.utesa.ronald.ingsoft.services.admin

import edu.utesa.ronald.ingsoft.domains.admin.Student
import edu.utesa.ronald.ingsoft.models.LoginManager
import edu.utesa.ronald.ingsoft.utils.Constants
import edu.utesa.ronald.ingsoft.utils.TouchableDomain
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional

import java.util.function.Consumer

/**
 *  Created by Ronald on 16/11/17.
 */
@SuppressWarnings("GrMethodMayBeStatic")
@Service("studentService")
@Transactional(rollbackFor = Exception.class)
class StudentService {

    private Student createBase(Student student, String userName) {
        if (student != null && !student.hasErrors()) {
            if (!Student.exists(student.getId())) {
                student.dateCreated = new Date()
                student.createdBy = userName
            }
            student.lastUpdated = new Date()
            student.modifyBy = userName
            return student.save(flush: true, failOnError: true)
        }
        return null
    }

    Student bootStrap(Student student) {
        return createBase(student, Constants.ROOT)
    }

    Student create(Student student, LoginManager loginManager) {
        return createBase(student, loginManager.getUsername())
    }

    List<Student> list(boolean enabled, int start, int size, Consumer<Student> touch = null) {
        List<Student> studentList = Student.withCriteria {
            firstResult(start)
            maxResults(size)
            order("id", "desc")
            eq "enabled", enabled
        }.findAll()
        TouchableDomain.applyTouch(studentList, touch)
        return studentList
    }

    List<Student> list(boolean enabled, Consumer<Student> touch = null) {
        List<Student> studentList = Student.withCriteria {
            order("id", "desc")
            eq "enabled", enabled
        }.findAll()
        TouchableDomain.applyTouch(studentList, touch)
        return studentList
    }

    List<Student> list(String enrollment, int size, Consumer<Student> touch = null) {
        List<Student> studentList = Student.withCriteria {
            if (size > 0) {
                maxResults(size)
            }
            order("id", "desc")
            like "enrollment", "%" + enrollment + "%"
            eq "enabled", true
        }.findAll()
        TouchableDomain.applyTouch(studentList, touch)
        return studentList
    }

    List<Student> list(boolean enabled, boolean admin, Consumer<Student> touch = null) {
        List<Student> studentList = Student.withCriteria {
            order("id", "desc")
            eq "enabled", enabled
        }.findAll()
        TouchableDomain.applyTouch(studentList, touch)
        return studentList
    }

    int count(boolean enabled) {
        return Student.countByEnabled(enabled)
    }

    Student refresh(Long id, Consumer<Student> touch = null) {
        Student student = Student.findById(id)
        TouchableDomain.applyTouch(student, touch)
        return student
    }

    Student byId(Long id, Consumer<Student> touch = null) {
        Student student = Student.findById(id)
        TouchableDomain.applyTouch(student, touch)
        return student
    }

    Student byCode(Long code, Consumer<Student> touch = null) {
        Student student = Student.findByCode(code)
        TouchableDomain.applyTouch(student, touch)
        return student
    }

    Student delete(Student student, LoginManager loginManager) {
        if (!student) {
            return null
        }
        student = Student.findById(student.id)
        student.enabled = !student.enabled
        return create(student, loginManager)
    }

    Student byEnrollment(String enrollment, Consumer<Student> touch = null) {
        Student student = Student.findByEnrollmentAndEnabled(enrollment, true)
        TouchableDomain.applyTouch(student, touch)
        return student
    }

}
