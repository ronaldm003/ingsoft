package edu.utesa.ronald.ingsoft.services.admin

import edu.utesa.ronald.ingsoft.domains.admin.SubjectSchedule
import edu.utesa.ronald.ingsoft.models.LoginManager
import edu.utesa.ronald.ingsoft.utils.Constants
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional

import java.sql.Time

/**
 *  Created by Ronald on 16/11/17.
 */
@SuppressWarnings("GrMethodMayBeStatic")
@Service("subjectScheduleService")
@Transactional(rollbackFor = Exception.class)
class SubjectScheduleService {

    private SubjectSchedule createBase(SubjectSchedule subjectSchedule, String userName) {
        if (subjectSchedule != null && !subjectSchedule.hasErrors()) {
            if (!SubjectSchedule.exists(subjectSchedule.getId())) {
                subjectSchedule.dateCreated = new Date()
                subjectSchedule.createdBy = userName
            }
            subjectSchedule.lastUpdated = new Date()
            subjectSchedule.modifyBy = userName
            return subjectSchedule.save(flush: true, failOnError: true)
        }
        return null
    }

    SubjectSchedule bootStrap(SubjectSchedule subjectSchedule) {
        return createBase(subjectSchedule, Constants.ROOT)
    }

    SubjectSchedule create(SubjectSchedule subjectSchedule, LoginManager loginManager) {
        return createBase(subjectSchedule, loginManager.getUsername())
    }

    List<SubjectSchedule> list(boolean enabled, int start, int size) {
        return SubjectSchedule.withCriteria {
            firstResult(start)
            maxResults(size)
            order("id", "desc")
            eq "enabled", enabled
        }.findAll()
    }

    List<SubjectSchedule> list(boolean enabled) {
        return SubjectSchedule.withCriteria {
            order("id", "desc")
            eq "enabled", enabled
        }.findAll()
    }

    List<SubjectSchedule> list(String description, int size) {
        return SubjectSchedule.withCriteria {
            if (size > 0) {
                maxResults(size)
            }
            order("id", "desc")
            like "description", "%" + description + "%"
            eq "enabled", true
        }.findAll()
    }

    List<SubjectSchedule> list(boolean enabled, boolean admin) {
        return SubjectSchedule.withCriteria {
            order("id", "desc")
            eq "enabled", enabled
        }.findAll()
    }

    int count(boolean enabled) {
        return SubjectSchedule.countByEnabled(enabled)
    }

    SubjectSchedule refresh(Long id) {
        SubjectSchedule subjectSchedule = SubjectSchedule.findById(id)
        return subjectSchedule
    }

    SubjectSchedule byId(Long id) {
        return SubjectSchedule.findById(id)
    }

    SubjectSchedule delete(SubjectSchedule subjectSchedule, LoginManager loginManager) {
        if (!subjectSchedule) {
            return null
        }
        subjectSchedule = SubjectSchedule.findById(subjectSchedule.id)
        subjectSchedule.enabled = !subjectSchedule.enabled
        return create(subjectSchedule, loginManager)
    }

    SubjectSchedule byDayAndStartTimeAndEndTime(int day, Time startTime, Time endTime) {
        return SubjectSchedule.findByDayAndStartTimeAndEndTimeAndEnabled(day, startTime, endTime, true)
    }

}
