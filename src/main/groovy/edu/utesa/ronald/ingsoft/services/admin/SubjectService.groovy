package edu.utesa.ronald.ingsoft.services.admin

import edu.utesa.ronald.ingsoft.domains.admin.Subject
import edu.utesa.ronald.ingsoft.models.LoginManager
import edu.utesa.ronald.ingsoft.utils.Constants
import edu.utesa.ronald.ingsoft.utils.TouchableDomain
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional

import java.util.function.Consumer

/**
 *  Created by Ronald on 16/11/17.
 */
@SuppressWarnings("GrMethodMayBeStatic")
@Service("subjectService")
@Transactional(rollbackFor = Exception.class)
class SubjectService {

    private Subject createBase(Subject subject, String userName) {
        if (subject != null && !subject.hasErrors()) {
            if (!Subject.exists(subject.getId())) {
                subject.dateCreated = new Date()
                subject.createdBy = userName
            }
            subject.lastUpdated = new Date()
            subject.modifyBy = userName
            return subject.save(flush: true, failOnError: true)
        }
        return null
    }

    Subject bootStrap(Subject subject) {
        return createBase(subject, Constants.ROOT)
    }

    Subject create(Subject subject, LoginManager loginManager) {
        return createBase(subject, loginManager.getUsername())
    }

    List<Subject> list(boolean enabled, int start, int size, Consumer<Subject> touch = null) {
        List<Subject> subjectList = Subject.withCriteria {
            firstResult(start)
            maxResults(size)
            order("id", "desc")
            eq "enabled", enabled
        }.findAll()
        TouchableDomain.applyTouch(subjectList, touch)
        return subjectList
    }

    List<Subject> list(boolean enabled, Consumer<Subject> touch = null) {
        List<Subject> subjectList = Subject.withCriteria {
            order("id", "desc")
            eq "enabled", enabled
        }.findAll()
        TouchableDomain.applyTouch(subjectList, touch)
        return subjectList
    }

    List<Subject> list(String description, int size, Consumer<Subject> touch = null) {
        List<Subject> subjectList = Subject.withCriteria {
            if (size > 0) {
                maxResults(size)
            }
            order("id", "desc")
            like "description", "%" + description + "%"
            eq "enabled", true
        }.findAll()
        TouchableDomain.applyTouch(subjectList, touch)
        return subjectList
    }

    List<Subject> list(boolean enabled, boolean admin, Consumer<Subject> touch = null) {
        List<Subject> subjectList = Subject.withCriteria {
            order("id", "desc")
            eq "enabled", enabled
        }.findAll()
        TouchableDomain.applyTouch(subjectList, touch)
        return subjectList
    }

    int count(boolean enabled) {
        return Subject.countByEnabled(enabled)
    }

    Subject refresh(Long id, Consumer<Subject> touch = null) {
        Subject subject = Subject.findById(id)
        TouchableDomain.applyTouch(subject, touch)
        return subject
    }

    Subject byId(Long id, Consumer<Subject> touch = null) {
        Subject subject = Subject.findById(id)
        TouchableDomain.applyTouch(subject, touch)
        return subject
    }

    Subject byCode(String code, Consumer<Subject> touch = null) {
        Subject subject = Subject.findByCodeAndEnabled(code, true)
        TouchableDomain.applyTouch(subject, touch)
        return subject
    }

    Subject delete(Subject subject, LoginManager loginManager) {
        if (!subject) {
            return null
        }
        subject = Subject.findById(subject.id)
        subject.enabled = !subject.enabled
        return create(subject, loginManager)
    }

    Long findMaxCode() {
        Subject subject = Subject.withCriteria {
            order("id", "desc")
        }.find() as Subject
        if (!subject) {
            return 0
        }
        return subject.code.toLong()
    }

    Subject byDescription(String description, Consumer<Subject> touch = null) {
        Subject subject = Subject.findByDescriptionAndEnabled(description, true)
        TouchableDomain.applyTouch(subject, touch)
        return subject
    }

}
