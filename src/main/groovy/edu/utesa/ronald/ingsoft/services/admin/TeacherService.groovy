package edu.utesa.ronald.ingsoft.services.admin

import edu.utesa.ronald.ingsoft.domains.admin.Teacher
import edu.utesa.ronald.ingsoft.models.LoginManager
import edu.utesa.ronald.ingsoft.utils.Constants
import edu.utesa.ronald.ingsoft.utils.TouchableDomain
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional

import java.util.function.Consumer

/**
 *  Created by Ronald on 16/11/17.
 */
@SuppressWarnings("GrMethodMayBeStatic")
@Service("teacherService")
@Transactional(rollbackFor = Exception.class)
class TeacherService {

    private Teacher createBase(Teacher teacher, String userName) {
        if (teacher != null && !teacher.hasErrors()) {
            if (!Teacher.exists(teacher.getId())) {
                teacher.dateCreated = new Date()
                teacher.createdBy = userName
            }
            teacher.lastUpdated = new Date()
            teacher.modifyBy = userName
            return teacher.save(flush: true, failOnError: true)
        }
        return null
    }

    Teacher bootStrap(Teacher teacher) {
        return createBase(teacher, Constants.ROOT)
    }

    Teacher create(Teacher teacher, LoginManager loginManager) {
        return createBase(teacher, loginManager.getUsername())
    }

    List<Teacher> list(boolean enabled, int start, int size, Consumer<Teacher> touch = null) {
        List<Teacher> teacherList = Teacher.withCriteria {
            firstResult(start)
            maxResults(size)
            order("id", "desc")
            eq "enabled", enabled
        }.findAll()
        TouchableDomain.applyTouch(teacherList, touch)
        return teacherList
    }

    List<Teacher> list(boolean enabled, Consumer<Teacher> touch = null) {
        List<Teacher> teacherList = Teacher.withCriteria {
            order("id", "desc")
            eq "enabled", enabled
        }.findAll()
        TouchableDomain.applyTouch(teacherList, touch)
        return teacherList
    }

    List<Teacher> list(String code, int size, Consumer<Teacher> touch = null) {
        List<Teacher> teacherList = Teacher.withCriteria {
            if (size > 0) {
                maxResults(size)
            }
            order("id", "desc")
            like "code", "%" + code + "%"
            eq "enabled", true
        }.findAll()
        TouchableDomain.applyTouch(teacherList, touch)
        return teacherList
    }

    List<Teacher> list(boolean enabled, boolean admin, Consumer<Teacher> touch = null) {
        List<Teacher> teacherList = Teacher.withCriteria {
            order("id", "desc")
            eq "enabled", enabled
        }.findAll()
        TouchableDomain.applyTouch(teacherList, touch)
        return teacherList
    }

    int count(boolean enabled) {
        return Teacher.countByEnabled(enabled)
    }

    Teacher refresh(Long id, Consumer<Teacher> touch = null) {
        Teacher teacher = Teacher.findById(id)
        TouchableDomain.applyTouch(teacher, touch)
        return teacher
    }

    Teacher byId(Long id, Consumer<Teacher> touch = null) {
        Teacher teacher = Teacher.findById(id)
        TouchableDomain.applyTouch(teacher, touch)
        return teacher
    }

    Teacher delete(Teacher teacher, LoginManager loginManager) {
        if (!teacher) {
            return null
        }
        teacher = Teacher.findById(teacher.id)
        teacher.enabled = !teacher.enabled
        return create(teacher, loginManager)
    }

    String findMaxCode(Consumer<Teacher> touch = null) {
        Teacher teacher = Teacher.withCriteria {
            order("id", "desc")
        }.find() as Teacher
        if (!teacher) {
            return ""
        }
        TouchableDomain.applyTouch(teacher, touch)
        return teacher.code
    }

    Teacher byCode(String code, Consumer<Teacher> touch = null) {
        Teacher teacher = Teacher.findByCodeAndEnabled(code, true)
        TouchableDomain.applyTouch(teacher, touch)
        return teacher
    }

}
