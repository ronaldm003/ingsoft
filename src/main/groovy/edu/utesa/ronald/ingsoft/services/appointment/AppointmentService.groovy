package edu.utesa.ronald.ingsoft.services.appointment

import edu.utesa.ronald.ingsoft.domains.admin.Cycle
import edu.utesa.ronald.ingsoft.domains.admin.Student
import edu.utesa.ronald.ingsoft.domains.appointment.Appointment
import edu.utesa.ronald.ingsoft.models.LoginManager
import edu.utesa.ronald.ingsoft.utils.Constants
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional

/**
 *  Created by ronald on 7/29/16.
 */
@SuppressWarnings("GrMethodMayBeStatic")
@Service("appointmentService")
@Transactional(rollbackFor = Exception.class)
class AppointmentService {

    private Appointment createBase(Appointment appointment, String userName) {
        if (appointment != null && !appointment.hasErrors()) {
            if (!Appointment.exists(appointment.getId())) {
                appointment.dateCreated = new Date()
                appointment.createdBy = userName
            }
            appointment.lastUpdated = new Date()
            appointment.modifyBy = userName
            return appointment.save(flush: true, failOnError: true)
        }
        return null
    }

    Appointment create(Appointment appointment, LoginManager loginManager) {
        if (appointment != null && !appointment.hasErrors()) {
            if (Appointment.exists(appointment.id)) {
                appointment.lastUpdated = new Date()
                appointment.modifyBy = loginManager.username
                return appointment.merge(flush: true, failOnError: true)
            } else {
                appointment.createdBy = loginManager.username
                appointment.modifyBy = loginManager.username
                return appointment.save(flush: true, failOnError: true)
            }
        }
        return null
    }

    boolean delete(Appointment appointment, LoginManager loginManager) {
        if (appointment != null && !appointment.hasErrors()) {
            appointment.enabled = false
            appointment.lastUpdated = new Date()
            appointment.modifyBy = loginManager.username
            return appointment.merge(flush: true, failOnError: true)
        }
        return false
    }

    List<Appointment> list(int start, int size) {
        return Appointment.withCriteria {
            firstResult(start)
            maxResults(size)
            order("id", "desc")
            eq "enabled", true
        }.findAll()
    }

    List<Appointment> list(boolean enab) {
        return Appointment.withCriteria {
            order("id", "desc")
            eq "enabled", enab
        }.findAll()
    }

    Appointment findByName(String name) {
        return Appointment.findByName(name)
    }

    List<Appointment> listByCycleAndStudent(Cycle cycle, Student student) {
        return Appointment.withCriteria {
            order("id", "desc")
            eq "enabled", true
            if (student) {
                eq "student", student
            }
            if (cycle) {
                ge "start", Constants.instance.parseGreaterEqualDateQuery(cycle.startDate)
                lte "end", Constants.instance.parseLessThanEqualDateQuery(cycle.endDate)
            }
        }.findAll()
    }

    List<Appointment> listByStudent(Student student, Date start, Date end) {
        return Appointment.withCriteria {
            order("id", "desc")
            eq "enabled", true
            if (student) {
                eq "student", student
            }
            ge "start", Constants.instance.parseGreaterEqualDateQuery(start)
            lte "end", Constants.instance.parseLessThanEqualDateQuery(end)

        }.findAll()
    }

    int count() {
        return Appointment.countByEnabled(true)
    }

    List<Appointment> list(Date start, Date end) {
        return Appointment.withCriteria {
            order("id", "desc")
            eq "enabled", true
            ge "start", Constants.instance.parseGreaterEqualDateQuery(start)
            lte "start", Constants.instance.parseLessThanEqualDateQuery(end)
            or {
                lte "end", Constants.instance.parseLessThanEqualDateQuery(end)
            }
        }.findAll()
    }

    Appointment refresh(Long id) {
        Appointment appointment = Appointment.findById(id)
        if (appointment.patient) {
            appointment.patient.enabled
        }
        if (appointment.student) {
            appointment.student.enabled
        }
        if (appointment.teacher) {
            appointment.teacher.enabled
        }
        if (appointment.appointmentType) {
            appointment.appointmentType.enabled
        }
        if (appointment.objective) {
            appointment.objective.enabled
        }
        return appointment
    }
}
