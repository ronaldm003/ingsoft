package edu.utesa.ronald.ingsoft.services.appointment

import edu.utesa.ronald.ingsoft.domains.appointment.AppointmentType
import edu.utesa.ronald.ingsoft.models.LoginManager
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional

/**
 *  Created by ronald on 7/29/16.
 */
@SuppressWarnings("GrMethodMayBeStatic")
@Service("appointmentTypeService")
@Transactional(rollbackFor = Exception.class)
class AppointmentTypeService {

    private AppointmentType createBase(AppointmentType appointmentType, String userName) {
        if (appointmentType != null && !appointmentType.hasErrors()) {
            if (!AppointmentType.exists(appointmentType.getId())) {
                appointmentType.dateCreated = new Date()
                appointmentType.createdBy = userName
            }
            appointmentType.lastUpdated = new Date()
            appointmentType.modifyBy = userName
            return appointmentType.save(flush: true, failOnError: true)
        }
        return null
    }

    AppointmentType create(AppointmentType appointmentType, LoginManager loginManager) {
        if (appointmentType != null && !appointmentType.hasErrors()) {
            if (AppointmentType.exists(appointmentType.id)) {
                appointmentType.lastUpdated = new Date()
                appointmentType.modifyBy = loginManager.username
                return appointmentType.merge(flush: true, failOnError: true)
            } else {
                appointmentType.createdBy = loginManager.username
                appointmentType.modifyBy = loginManager.username
                return appointmentType.save(flush: true, failOnError: true)
            }
        }
        return null
    }

    List<AppointmentType> list(int start, int size) {
        return AppointmentType.withCriteria {
            firstResult(start)
            maxResults(size)
            order("id", "desc")
            eq "enabled", true
        }.findAll()
    }

    List<AppointmentType> list(boolean enab, int start, int size) {
        return AppointmentType.withCriteria {
            firstResult(start)
            maxResults(size)
            order("id", "desc")
            eq "enabled", enab
        }.findAll()
    }

    List<AppointmentType> list(boolean enab) {
        return AppointmentType.withCriteria {
            order("id")
            eq "enabled", enab
        }.findAll()
    }

    AppointmentType findById(Long id) {
        return AppointmentType.findById(id)
    }

    AppointmentType findByName(String name) {
        return AppointmentType.findByName(name)
    }

    int count(boolean enab) {
        return AppointmentType.countByEnabled(enab)
    }

    int count() {
        return AppointmentType.countByEnabled(true)
    }
}
