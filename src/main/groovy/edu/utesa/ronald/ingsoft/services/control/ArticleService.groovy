package edu.utesa.ronald.ingsoft.services.control

import edu.utesa.ronald.ingsoft.domains.control.Article
import edu.utesa.ronald.ingsoft.models.LoginManager
import edu.utesa.ronald.ingsoft.utils.Constants
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional

/**
 *  Created by Ronald on 16/11/17.
 */
@SuppressWarnings("GrMethodMayBeStatic")
@Service("articleService")
@Transactional(rollbackFor = Exception.class)
class ArticleService {

    private Article createBase(Article article, String userName) {
        if (article != null && !article.hasErrors()) {
            if (!Article.exists(article.getId())) {
                article.dateCreated = new Date()
                article.createdBy = userName
            }
            article.lastUpdated = new Date()
            article.modifyBy = userName
            return article.save(flush: true, failOnError: true)
        }
        return null
    }

    Article bootStrap(Article article) {
        return createBase(article, Constants.ROOT)
    }

    Article create(Article article, LoginManager loginManager) {
        return createBase(article, loginManager.getUsername())
    }

    List<Article> list(boolean enabled, int start, int size) {
        return Article.withCriteria {
            firstResult(start)
            maxResults(size)
            order("id", "desc")
            eq "enabled", enabled
        }.findAll()
    }

    List<Article> list(boolean enabled) {
        return Article.withCriteria {
            order("id", "desc")
            eq "enabled", enabled
        }.findAll()
    }

    List<Article> list(String description, int size) {
        return Article.withCriteria {
            if (size > 0) {
                maxResults(size)
            }
            order("id", "desc")
            like "description", "%" + description + "%"
            eq "enabled", true
        }.findAll()
    }

    List<Article> list(boolean enabled, boolean admin) {
        return Article.withCriteria {
            order("id", "desc")
            eq "enabled", enabled
        }.findAll()
    }

    int count(boolean enabled) {
        return Article.countByEnabled(enabled)
    }

    Article refresh(Long id) {
        Article article = Article.findById(id)
        return article
    }

    Article byId(Long id) {
        return Article.findById(id)
    }

    Article byCode(Long code) {
        return Article.findByCode(code)
    }

    Article delete(Article article, LoginManager loginManager) {
        if (!article) {
            return null
        }
        article = Article.findById(article.id)
        article.enabled = !article.enabled
        return create(article, loginManager)
    }

    Long findMaxCode() {
        Article article = Article.withCriteria {
            order("id", "desc")
        }.find() as Article
        if (!article) {
            return 0
        }
        return article.code
    }

    Article byDescription(String description) {
        return Article.findByDescriptionAndEnabled(description, true)
    }

}
