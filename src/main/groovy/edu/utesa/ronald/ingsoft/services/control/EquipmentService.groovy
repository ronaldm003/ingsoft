package edu.utesa.ronald.ingsoft.services.control

import edu.utesa.ronald.ingsoft.domains.control.Equipment
import edu.utesa.ronald.ingsoft.models.LoginManager
import edu.utesa.ronald.ingsoft.utils.Constants
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional

/**
 *  Created by Ronald on 16/11/17.
 */
@SuppressWarnings("GrMethodMayBeStatic")
@Service("equipmentService")
@Transactional(rollbackFor = Exception.class)
class EquipmentService {

    private Equipment createBase(Equipment equipment, String userName) {
        if (equipment != null && !equipment.hasErrors()) {
            if (!Equipment.exists(equipment.getId())) {
                equipment.dateCreated = new Date()
                equipment.createdBy = userName
            }
            equipment.lastUpdated = new Date()
            equipment.modifyBy = userName
            return equipment.save(flush: true, failOnError: true)
        }
        return null
    }

    Equipment bootStrap(Equipment equipment) {
        return createBase(equipment, Constants.ROOT)
    }

    Equipment create(Equipment equipment, LoginManager loginManager) {
        return createBase(equipment, loginManager.getUsername())
    }

    List<Equipment> list(boolean enabled, int start, int size) {
        return Equipment.withCriteria {
            firstResult(start)
            maxResults(size)
            order("id", "desc")
            eq "enabled", enabled
        }.findAll()
    }

    List<Equipment> list(boolean enabled) {
        return Equipment.withCriteria {
            order("id", "desc")
            eq "enabled", enabled
        }.findAll()
    }

    List<Equipment> list(String description, int size) {
        return Equipment.withCriteria {
            if (size > 0) {
                maxResults(size)
            }
            order("id", "desc")
            like "description", "%" + description + "%"
            eq "enabled", true
        }.findAll()
    }

    List<Equipment> list(boolean enabled, boolean admin) {
        return Equipment.withCriteria {
            order("id", "desc")
            eq "enabled", enabled
        }.findAll()
    }

    int count(boolean enabled) {
        return Equipment.countByEnabled(enabled)
    }

    Equipment refresh(Long id) {
        Equipment equipment = Equipment.findById(id)
        return equipment
    }

    Equipment byId(Long id) {
        return Equipment.findById(id)
    }

    Equipment byCode(Long code) {
        return Equipment.findByCode(code)
    }

    Equipment delete(Equipment equipment, LoginManager loginManager) {
        if (!equipment) {
            return null
        }
        equipment = Equipment.findById(equipment.id)
        equipment.enabled = !equipment.enabled
        return create(equipment, loginManager)
    }

    Long findMaxCode() {
        Equipment equipment = Equipment.withCriteria {
            order("id", "desc")
        }.find() as Equipment
        if (!equipment) {
            return 0
        }
        return equipment.code
    }

    Equipment byName(String description) {
        return Equipment.findByDescriptionAndEnabled(description, true)
    }

}
