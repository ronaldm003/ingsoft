package edu.utesa.ronald.ingsoft.services.control

import edu.utesa.ronald.ingsoft.domains.control.Inventory
import edu.utesa.ronald.ingsoft.models.LoginManager
import edu.utesa.ronald.ingsoft.utils.Constants
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional

/**
 *  Created by Ronald on 16/11/17.
 */
@SuppressWarnings("GrMethodMayBeStatic")
@Service("inventoryService")
@Transactional(rollbackFor = Exception.class)
class InventoryService {

    private Inventory createBase(Inventory inventory, String userName) {
        if (inventory != null && !inventory.hasErrors()) {
            if (!Inventory.exists(inventory.getId())) {
                inventory.dateCreated = new Date()
                inventory.createdBy = userName
            }
            inventory.lastUpdated = new Date()
            inventory.modifyBy = userName
            return inventory.save(flush: true, failOnError: true)
        }
        return null
    }

    Inventory bootStrap(Inventory inventory) {
        return createBase(inventory, Constants.ROOT)
    }

    Inventory create(Inventory inventory, LoginManager loginManager) {
        return createBase(inventory, loginManager.getUsername())
    }

    List<Inventory> list(boolean enabled, int start, int size) {
        return Inventory.withCriteria {
            firstResult(start)
            maxResults(size)
            order("id", "desc")
            eq "enabled", enabled
        }.findAll()
    }

    List<Inventory> list(boolean enabled) {
        return Inventory.withCriteria {
            order("id", "desc")
            eq "enabled", enabled
        }.findAll()
    }

    List<Inventory> list(String description, int size) {
        return Inventory.withCriteria {
            if (size > 0) {
                maxResults(size)
            }
            order("id", "desc")
            like "description", "%" + description + "%"
            eq "enabled", true
        }.findAll()
    }

    List<Inventory> list(boolean enabled, boolean admin) {
        return Inventory.withCriteria {
            order("id", "desc")
            eq "enabled", enabled
        }.findAll()
    }

    int count(boolean enabled) {
        return Inventory.countByEnabled(enabled)
    }

    Inventory refresh(Long id) {
        Inventory inventory = Inventory.findById(id)
        inventory.listArticles.size()
        return inventory
    }

    Inventory byId(Long id) {
        return Inventory.findById(id)
    }

    Inventory byCode(Long code) {
        return Inventory.findByCode(code)
    }

    Inventory delete(Inventory inventory, LoginManager loginManager) {
        if (!inventory) {
            return null
        }
        inventory = Inventory.findById(inventory.id)
        inventory.enabled = !inventory.enabled
        return create(inventory, loginManager)
    }

    Long findMaxCode() {
        Inventory inventory = Inventory.withCriteria {
            order("id", "desc")
        }.find() as Inventory
        if (!inventory) {
            return 0
        }
        return inventory.code
    }

    Inventory byName(String description) {
        return Inventory.findByDescriptionAndEnabled(description, true)
    }

}
