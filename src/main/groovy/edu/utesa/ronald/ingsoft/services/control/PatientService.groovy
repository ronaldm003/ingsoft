package edu.utesa.ronald.ingsoft.services.control

import edu.utesa.ronald.ingsoft.domains.control.Patient
import edu.utesa.ronald.ingsoft.domains.security.Person
import edu.utesa.ronald.ingsoft.models.LoginManager
import edu.utesa.ronald.ingsoft.utils.Constants
import edu.utesa.ronald.ingsoft.utils.TouchableDomain
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional

import java.util.function.Consumer

/**
 *  Created by Ronald on 16/11/17.
 */
@SuppressWarnings("GrMethodMayBeStatic")
@Service("patientService")
@Transactional(rollbackFor = Exception.class)
class PatientService {

    private Patient createBase(Patient patient, String userName) {
        if (patient != null && !patient.hasErrors()) {
            if (!Patient.exists(patient.getId())) {
                patient.dateCreated = new Date()
                patient.createdBy = userName
            }
            patient.lastUpdated = new Date()
            patient.modifyBy = userName
            return patient.save(flush: true, failOnError: true)
        }
        return null
    }

    Patient bootStrap(Patient patient) {
        return createBase(patient, Constants.ROOT)
    }

    Patient create(Patient patient, LoginManager loginManager) {
        return createBase(patient, loginManager.getUsername())
    }

    List<Patient> list(boolean enabled, int start, int size, Consumer<Patient> touch = null) {
        List<Patient> patientList = Patient.withCriteria {
            firstResult(start)
            maxResults(size)
            order("id", "desc")
            eq "enabled", enabled
        }.findAll()
        TouchableDomain.applyTouch(patientList, touch)
        return patientList
    }

    List<Patient> list(boolean enabled, Consumer<Patient> touch = null) {
        List<Patient> patientList = Patient.withCriteria {
            order("id", "desc")
            eq "enabled", enabled
        }.findAll()
        TouchableDomain.applyTouch(patientList, touch)
        return patientList
    }

    List<Patient> list(String name, int size, Consumer<Patient> touch = null) {
        List<Patient> patientList = Patient.withCriteria {
            if (size > 0) {
                maxResults(size)
            }
            order("id", "desc")
            person {
                like "name", "%" + name + "%"
            }
            eq "enabled", true
        }.findAll()
        TouchableDomain.applyTouch(patientList, touch)
        return patientList
    }

    List<Patient> list(boolean enabled, boolean admin, Consumer<Patient> touch = null) {
        List<Patient> patientList = Patient.withCriteria {
            order("id", "desc")
            eq "enabled", enabled
        }.findAll()
        TouchableDomain.applyTouch(patientList, touch)
        return patientList
    }

    int count(boolean enabled) {
        return Patient.countByEnabled(enabled)
    }

    Patient refresh(Long id, Consumer<Patient> touch = null) {
        Patient patient = Patient.findById(id)
        TouchableDomain.applyTouch(patient, touch)
        return patient
    }

    Patient byId(Long id, Consumer<Patient> touch = null) {
        Patient patient = Patient.findById(id)
        TouchableDomain.applyTouch(patient, touch)
        return patient
    }

    Patient byCode(Long code, Consumer<Patient> touch = null) {
        Patient patient = Patient.findByCodeAndEnabled(code, true)
        TouchableDomain.applyTouch(patient, touch)
        return patient
    }

    Patient byName(String name, Consumer<Patient> touch = null) {
        Patient patient = Patient.withCriteria {
            order("id", "desc")
            person {
                eq "name", name
            }
            eq "enabled", true
        } as Patient
        TouchableDomain.applyTouch(patient, touch)
        return patient
    }

    Patient delete(Patient patient, LoginManager loginManager) {
        if (!patient) {
            return null
        }
        patient = Patient.findById(patient.id)
        patient.enabled = !patient.enabled
        return create(patient, loginManager)
    }

    Long findMaxCode() {
        Patient patient = Patient.withCriteria {
            order("id", "desc")
        }.find() as Patient
        if (!patient) {
            return 0
        }
        return patient.code
    }

    Patient byWarrant(String warrant, Consumer<Patient> touch = null) {
        Patient patient = Patient.withCriteria {
            person {
                eq "warrant", warrant
            }
            eq "enabled", true
        } as Patient
        TouchableDomain.applyTouch(patient, touch)
        return patient
    }

    Patient byPerson(Person person, Consumer<Patient> touch = null) {
        Patient patient = Patient.findByPersonAndEnabled(person, true)
        TouchableDomain.applyTouch(patient, touch)
        return patient
    }

}
