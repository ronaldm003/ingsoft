package edu.utesa.ronald.ingsoft.services.control

import edu.utesa.ronald.ingsoft.domains.control.Prescription
import edu.utesa.ronald.ingsoft.models.LoginManager
import edu.utesa.ronald.ingsoft.utils.Constants
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional

/**
 *  Created by Ronald on 16/11/17.
 */
@SuppressWarnings("GrMethodMayBeStatic")
@Service("prescriptionService")
@Transactional(rollbackFor = Exception.class)
class PrescriptionService {

    private Prescription createBase(Prescription prescription, String userName) {
        if (prescription != null && !prescription.hasErrors()) {
            if (!Prescription.exists(prescription.getId())) {
                prescription.dateCreated = new Date()
                prescription.createdBy = userName
            }
            prescription.lastUpdated = new Date()
            prescription.modifyBy = userName
            return prescription.save(flush: true, failOnError: true)
        }
        return null
    }

    Prescription bootStrap(Prescription prescription) {
        return createBase(prescription, Constants.ROOT)
    }

    Prescription create(Prescription prescription, LoginManager loginManager) {
        return createBase(prescription, loginManager.getUsername())
    }

    List<Prescription> list(boolean enabled, int start, int size) {
        return Prescription.withCriteria {
            firstResult(start)
            maxResults(size)
            order("id", "desc")
            eq "enabled", enabled
        }.findAll()
    }

    List<Prescription> list(boolean enabled) {
        return Prescription.withCriteria {
            order("id", "desc")
            eq "enabled", enabled
        }.findAll()
    }

    List<Prescription> list(String description, int size) {
        return Prescription.withCriteria {
            if (size > 0) {
                maxResults(size)
            }
            order("id", "desc")
            like "description", "%" + description + "%"
            eq "enabled", true
        }.findAll()
    }

    List<Prescription> list(boolean enabled, boolean admin) {
        return Prescription.withCriteria {
            order("id", "desc")
            eq "enabled", enabled
        }.findAll()
    }

    int count(boolean enabled) {
        return Prescription.countByEnabled(enabled)
    }

    Prescription refresh(Long id) {
        Prescription prescription = Prescription.findById(id)
        return prescription
    }

    Prescription byId(Long id) {
        return Prescription.findById(id)
    }

    Prescription byCode(Long code) {
        return Prescription.findByCode(code)
    }

    Prescription delete(Prescription prescription, LoginManager loginManager) {
        if (!prescription) {
            return null
        }
        prescription = Prescription.findById(prescription.id)
        prescription.enabled = !prescription.enabled
        return create(prescription, loginManager)
    }

    Long findMaxCode() {
        Prescription prescription = Prescription.withCriteria {
            order("id", "desc")
        }.find() as Prescription
        if (!prescription) {
            return 0
        }
        return prescription.code
    }

    Prescription byName(String description) {
        return Prescription.findByDescriptionAndEnabled(description, true)
    }

}
