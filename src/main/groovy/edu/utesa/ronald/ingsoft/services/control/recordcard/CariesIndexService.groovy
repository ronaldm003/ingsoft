package edu.utesa.ronald.ingsoft.services.control.recordcard

import edu.utesa.ronald.ingsoft.domains.control.recordcard.CariesIndex
import edu.utesa.ronald.ingsoft.models.LoginManager
import edu.utesa.ronald.ingsoft.utils.Constants
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional

/**
 *  Created by Ronald on 16/11/17.
 */
@SuppressWarnings("GrMethodMayBeStatic")
@Service("cariesIndexService")
@Transactional(rollbackFor = Exception.class)
class CariesIndexService {

    private CariesIndex createBase(CariesIndex cariesIndex, String userName) {
        if (cariesIndex != null && !cariesIndex.hasErrors()) {
            if (!CariesIndex.exists(cariesIndex.getId())) {
                cariesIndex.dateCreated = new Date()
                cariesIndex.createdBy = userName
            }
            cariesIndex.lastUpdated = new Date()
            cariesIndex.modifyBy = userName
            return cariesIndex.save(flush: true, failOnError: true)
        }
        return null
    }

    CariesIndex bootStrap(CariesIndex cariesIndex) {
        return createBase(cariesIndex, Constants.ROOT)
    }

    CariesIndex create(CariesIndex cariesIndex, LoginManager loginManager) {
        return createBase(cariesIndex, loginManager.getUsername())
    }

    List<CariesIndex> list(boolean enabled, int start, int size) {
        return CariesIndex.withCriteria {
            firstResult(start)
            maxResults(size)
            order("id", "desc")
            eq "enabled", enabled
        }.findAll()
    }

    List<CariesIndex> list(boolean enabled) {
        return CariesIndex.withCriteria {
            order("id", "desc")
            eq "enabled", enabled
        }.findAll()
    }

    List<CariesIndex> list(String description, int size) {
        return CariesIndex.withCriteria {
            if (size > 0) {
                maxResults(size)
            }
            order("id", "desc")
            like "description", "%" + description + "%"
            eq "enabled", true
        }.findAll()
    }

    List<CariesIndex> list(boolean enabled, boolean admin) {
        return CariesIndex.withCriteria {
            order("id", "desc")
            eq "enabled", enabled
        }.findAll()
    }

    int count(boolean enabled) {
        return CariesIndex.countByEnabled(enabled)
    }

    CariesIndex refresh(Long id) {
        CariesIndex cariesIndex = CariesIndex.findById(id)
        return cariesIndex
    }

    CariesIndex byId(Long id) {
        return CariesIndex.findById(id)
    }

    CariesIndex byCode(Long code) {
        return CariesIndex.findByCode(code)
    }

    CariesIndex delete(CariesIndex cariesIndex, LoginManager loginManager) {
        if (!cariesIndex) {
            return null
        }
        cariesIndex = CariesIndex.findById(cariesIndex.id)
        cariesIndex.enabled = !cariesIndex.enabled
        return create(cariesIndex, loginManager)
    }

    Long findMaxCode() {
        CariesIndex cariesIndex = CariesIndex.withCriteria {
            order("id", "desc")
        }.find() as CariesIndex
        if (!cariesIndex) {
            return 0
        }
        return cariesIndex.code.toLong()
    }

    CariesIndex byDescription(String description) {
        return CariesIndex.findByDescriptionAndEnabled(description, true)
    }

}
