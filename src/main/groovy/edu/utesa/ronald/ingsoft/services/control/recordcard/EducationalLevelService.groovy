package edu.utesa.ronald.ingsoft.services.control.recordcard

import edu.utesa.ronald.ingsoft.domains.control.recordcard.EducationLevel
import edu.utesa.ronald.ingsoft.models.LoginManager
import edu.utesa.ronald.ingsoft.utils.Constants
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional

/**
 *  Created by Ronald on 16/11/17.
 */
@SuppressWarnings("GrMethodMayBeStatic")
@Service("educationalLevelService")
@Transactional(rollbackFor = Exception.class)
class EducationalLevelService {

    private EducationLevel createBase(EducationLevel educationLevel, String userName) {
        if (educationLevel != null && !educationLevel.hasErrors()) {
            if (!EducationLevel.exists(educationLevel.getId())) {
                educationLevel.dateCreated = new Date()
                educationLevel.createdBy = userName
            }
            educationLevel.lastUpdated = new Date()
            educationLevel.modifyBy = userName
            return educationLevel.save(flush: true, failOnError: true)
        }
        return null
    }

    EducationLevel bootStrap(EducationLevel educationLevel) {
        return createBase(educationLevel, Constants.ROOT)
    }

    EducationLevel create(EducationLevel educationLevel, LoginManager loginManager) {
        return createBase(educationLevel, loginManager.getUsername())
    }

    List<EducationLevel> list(boolean enabled, int start, int size) {
        return EducationLevel.withCriteria {
            firstResult(start)
            maxResults(size)
            order("id", "desc")
            eq "enabled", enabled
        }.findAll()
    }

    List<EducationLevel> list(boolean enabled) {
        return EducationLevel.withCriteria {
            order("id", "desc")
            eq "enabled", enabled
        }.findAll()
    }

    List<EducationLevel> list(String description, int size) {
        return EducationLevel.withCriteria {
            if (size > 0) {
                maxResults(size)
            }
            order("id", "desc")
            like "description", "%" + description + "%"
            eq "enabled", true
        }.findAll()
    }

    List<EducationLevel> list(boolean enabled, boolean admin) {
        return EducationLevel.withCriteria {
            order("id", "desc")
            eq "enabled", enabled
        }.findAll()
    }

    int count(boolean enabled) {
        return EducationLevel.countByEnabled(enabled)
    }

    EducationLevel refresh(Long id) {
        EducationLevel educationLevel = EducationLevel.findById(id)
        return educationLevel
    }

    EducationLevel byId(Long id) {
        return EducationLevel.findById(id)
    }

    EducationLevel byCode(Long code) {
        return EducationLevel.findByCode(code)
    }

    EducationLevel delete(EducationLevel educationLevel, LoginManager loginManager) {
        if (!educationLevel) {
            return null
        }
        educationLevel = EducationLevel.findById(educationLevel.id)
        educationLevel.enabled = !educationLevel.enabled
        return create(educationLevel, loginManager)
    }

    Long findMaxCode() {
        EducationLevel educationLevel = EducationLevel.withCriteria {
            order("id", "desc")
        }.find() as EducationLevel
        if (!educationLevel) {
            return 0
        }
        return educationLevel.code.toLong()
    }

    EducationLevel byDescription(String description) {
        return EducationLevel.findByDescriptionAndEnabled(description, true)
    }

}
