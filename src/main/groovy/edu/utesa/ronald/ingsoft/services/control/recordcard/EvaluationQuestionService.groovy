package edu.utesa.ronald.ingsoft.services.control.recordcard

import edu.utesa.ronald.ingsoft.domains.control.recordcard.EvaluationQuestion
import edu.utesa.ronald.ingsoft.models.LoginManager
import edu.utesa.ronald.ingsoft.utils.Constants
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional

/**
 *  Created by Ronald on 16/11/17.
 */
@SuppressWarnings("GrMethodMayBeStatic")
@Service("evaluationQuestionService")
@Transactional(rollbackFor = Exception.class)
class EvaluationQuestionService {

    private EvaluationQuestion createBase(EvaluationQuestion evaluationQuestion, String userName) {
        if (evaluationQuestion != null && !evaluationQuestion.hasErrors()) {
            if (!EvaluationQuestion.exists(evaluationQuestion.getId())) {
                evaluationQuestion.dateCreated = new Date()
                evaluationQuestion.createdBy = userName
            }
            evaluationQuestion.lastUpdated = new Date()
            evaluationQuestion.modifyBy = userName
            return evaluationQuestion.save(flush: true, failOnError: true)
        }
        return null
    }

    EvaluationQuestion bootStrap(EvaluationQuestion evaluationQuestion) {
        return createBase(evaluationQuestion, Constants.ROOT)
    }

    EvaluationQuestion create(EvaluationQuestion evaluationQuestion, LoginManager loginManager) {
        return createBase(evaluationQuestion, loginManager.getUsername())
    }

    List<EvaluationQuestion> list(boolean enabled, int start, int size) {
        return EvaluationQuestion.withCriteria {
            firstResult(start)
            maxResults(size)
            order("id", "desc")
            eq "enabled", enabled
        }.findAll()
    }

    List<EvaluationQuestion> list(boolean enabled) {
        return EvaluationQuestion.withCriteria {
            order("id", "desc")
            eq "enabled", enabled
        }.findAll()
    }

    List<EvaluationQuestion> list(String description, int size) {
        return EvaluationQuestion.withCriteria {
            if (size > 0) {
                maxResults(size)
            }
            order("id", "desc")
            like "description", "%" + description + "%"
            eq "enabled", true
        }.findAll()
    }

    List<EvaluationQuestion> list(boolean enabled, boolean admin) {
        return EvaluationQuestion.withCriteria {
            order("id", "desc")
            eq "enabled", enabled
        }.findAll()
    }

    int count(boolean enabled) {
        return EvaluationQuestion.countByEnabled(enabled)
    }

    EvaluationQuestion refresh(Long id) {
        EvaluationQuestion evaluationQuestion = EvaluationQuestion.findById(id)
        return evaluationQuestion
    }

    EvaluationQuestion byId(Long id) {
        return EvaluationQuestion.findById(id)
    }

    EvaluationQuestion byCode(Long code) {
        return EvaluationQuestion.findByCode(code)
    }

    EvaluationQuestion delete(EvaluationQuestion evaluationQuestion, LoginManager loginManager) {
        if (!evaluationQuestion) {
            return null
        }
        evaluationQuestion = EvaluationQuestion.findById(evaluationQuestion.id)
        evaluationQuestion.enabled = !evaluationQuestion.enabled
        return create(evaluationQuestion, loginManager)
    }

    Long findMaxCode() {
        EvaluationQuestion evaluationQuestion = EvaluationQuestion.withCriteria {
            order("id", "desc")
        }.find() as EvaluationQuestion
        if (!evaluationQuestion) {
            return 0
        }
        return evaluationQuestion.code.toLong()
    }

    EvaluationQuestion byQuestion(String question) {
        return EvaluationQuestion.findByQuestionAndEnabled(question, true)
    }

}
