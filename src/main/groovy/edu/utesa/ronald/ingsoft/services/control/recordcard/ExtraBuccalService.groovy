package edu.utesa.ronald.ingsoft.services.control.recordcard

import edu.utesa.ronald.ingsoft.domains.control.recordcard.ExtraBuccal
import edu.utesa.ronald.ingsoft.models.LoginManager
import edu.utesa.ronald.ingsoft.utils.Constants
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional

/**
 *  Created by Ronald on 16/11/17.
 */
@SuppressWarnings("GrMethodMayBeStatic")
@Service("extraBuccalService")
@Transactional(rollbackFor = Exception.class)
class ExtraBuccalService {

    private ExtraBuccal createBase(ExtraBuccal extraBuccal, String userName) {
        if (extraBuccal != null && !extraBuccal.hasErrors()) {
            if (!ExtraBuccal.exists(extraBuccal.getId())) {
                extraBuccal.dateCreated = new Date()
                extraBuccal.createdBy = userName
            }
            extraBuccal.lastUpdated = new Date()
            extraBuccal.modifyBy = userName
            return extraBuccal.save(flush: true, failOnError: true)
        }
        return null
    }

    ExtraBuccal bootStrap(ExtraBuccal extraBuccal) {
        return createBase(extraBuccal, Constants.ROOT)
    }

    ExtraBuccal create(ExtraBuccal extraBuccal, LoginManager loginManager) {
        return createBase(extraBuccal, loginManager.getUsername())
    }

    List<ExtraBuccal> list(boolean enabled, int start, int size) {
        return ExtraBuccal.withCriteria {
            firstResult(start)
            maxResults(size)
            order("id", "desc")
            eq "enabled", enabled
        }.findAll()
    }

    List<ExtraBuccal> list(boolean enabled) {
        return ExtraBuccal.withCriteria {
            order("id", "desc")
            eq "enabled", enabled
        }.findAll()
    }

    List<ExtraBuccal> list(String description, int size) {
        return ExtraBuccal.withCriteria {
            if (size > 0) {
                maxResults(size)
            }
            order("id", "desc")
            like "description", "%" + description + "%"
            eq "enabled", true
        }.findAll()
    }

    List<ExtraBuccal> list(boolean enabled, boolean admin) {
        return ExtraBuccal.withCriteria {
            order("id", "desc")
            eq "enabled", enabled
        }.findAll()
    }

    int count(boolean enabled) {
        return ExtraBuccal.countByEnabled(enabled)
    }

    ExtraBuccal refresh(Long id) {
        ExtraBuccal extraBuccal = ExtraBuccal.findById(id)
        return extraBuccal
    }

    ExtraBuccal byId(Long id) {
        return ExtraBuccal.findById(id)
    }

    ExtraBuccal byCode(Long code) {
        return ExtraBuccal.findByCode(code)
    }

    ExtraBuccal delete(ExtraBuccal extraBuccal, LoginManager loginManager) {
        if (!extraBuccal) {
            return null
        }
        extraBuccal = ExtraBuccal.findById(extraBuccal.id)
        extraBuccal.enabled = !extraBuccal.enabled
        return create(extraBuccal, loginManager)
    }


    Long findMaxCode() {
        ExtraBuccal intraBuccal = ExtraBuccal.withCriteria {
            order("id", "desc")
        }.find() as ExtraBuccal
        if (!intraBuccal) {
            return 0
        }
        return intraBuccal.code
    }

    ExtraBuccal byQuestion(String question) {
        return ExtraBuccal.findByQuestionAndEnabled(question, true)
    }

}
