package edu.utesa.ronald.ingsoft.services.control.recordcard

import edu.utesa.ronald.ingsoft.domains.control.recordcard.Habits
import edu.utesa.ronald.ingsoft.models.LoginManager
import edu.utesa.ronald.ingsoft.utils.Constants
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional

/**
 *  Created by Ronald on 16/11/17.
 */
@SuppressWarnings("GrMethodMayBeStatic")
@Service("habitsService")
@Transactional(rollbackFor = Exception.class)
class HabitsService {

    private Habits createBase(Habits habits, String userName) {
        if (habits != null && !habits.hasErrors()) {
            if (!Habits.exists(habits.getId())) {
                habits.dateCreated = new Date()
                habits.createdBy = userName
            }
            habits.lastUpdated = new Date()
            habits.modifyBy = userName
            return habits.save(flush: true, failOnError: true)
        }
        return null
    }

    Habits bootStrap(Habits habits) {
        return createBase(habits, Constants.ROOT)
    }

    Habits create(Habits habits, LoginManager loginManager) {
        return createBase(habits, loginManager.getUsername())
    }

    List<Habits> list(boolean enabled, int start, int size) {
        return Habits.withCriteria {
            firstResult(start)
            maxResults(size)
            order("id", "desc")
            eq "enabled", enabled
        }.findAll()
    }

    List<Habits> list(boolean enabled) {
        return Habits.withCriteria {
            order("id", "desc")
            eq "enabled", enabled
        }.findAll()
    }

    List<Habits> list(String description, int size) {
        return Habits.withCriteria {
            if (size > 0) {
                maxResults(size)
            }
            order("id", "desc")
            like "description", "%" + description + "%"
            eq "enabled", true
        }.findAll()
    }

    List<Habits> list(boolean enabled, boolean admin) {
        return Habits.withCriteria {
            order("id", "desc")
            eq "enabled", enabled
        }.findAll()
    }

    int count(boolean enabled) {
        return Habits.countByEnabled(enabled)
    }

    Habits refresh(Long id) {
        Habits habits = Habits.findById(id)
        return habits
    }

    Habits byId(Long id) {
        return Habits.findById(id)
    }

    Habits byCode(Long code) {
        return Habits.findByCode(code)
    }

    Habits delete(Habits habits, LoginManager loginManager) {
        if (!habits) {
            return null
        }
        habits = Habits.findById(habits.id)
        habits.enabled = !habits.enabled
        return create(habits, loginManager)
    }

    Long findMaxCode() {
        Habits habits = Habits.withCriteria {
            order("id", "desc")
        }.find() as Habits
        if (!habits) {
            return 0
        }
        return habits.code.toLong()
    }

    Habits byDescription(String description) {
        return Habits.findByDescriptionAndEnabled(description, true)
    }

}
