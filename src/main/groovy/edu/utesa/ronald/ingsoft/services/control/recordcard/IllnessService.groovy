package edu.utesa.ronald.ingsoft.services.control.recordcard

import edu.utesa.ronald.ingsoft.domains.control.recordcard.Illness
import edu.utesa.ronald.ingsoft.models.LoginManager
import edu.utesa.ronald.ingsoft.utils.Constants
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional

/**
 *  Created by Ronald on 16/11/17.
 */
@SuppressWarnings("GrMethodMayBeStatic")
@Service("illnessService")
@Transactional(rollbackFor = Exception.class)
class IllnessService {

    private Illness createBase(Illness illness, String userName) {
        if (illness != null && !illness.hasErrors()) {
            if (!Illness.exists(illness.getId())) {
                illness.dateCreated = new Date()
                illness.createdBy = userName
            }
            illness.lastUpdated = new Date()
            illness.modifyBy = userName
            return illness.save(flush: true, failOnError: true)
        }
        return null
    }

    Illness bootStrap(Illness illness) {
        return createBase(illness, Constants.ROOT)
    }

    Illness create(Illness illness, LoginManager loginManager) {
        return createBase(illness, loginManager.getUsername())
    }

    List<Illness> list(boolean enabled, int start, int size) {
        return Illness.withCriteria {
            firstResult(start)
            maxResults(size)
            order("id", "desc")
            eq "enabled", enabled
        }.findAll()
    }

    List<Illness> list(boolean enabled) {
        return Illness.withCriteria {
            order("id", "desc")
            eq "enabled", enabled
        }.findAll()
    }

    List<Illness> list(String description, int size) {
        return Illness.withCriteria {
            if (size > 0) {
                maxResults(size)
            }
            order("id", "desc")
            like "description", "%" + description + "%"
            eq "enabled", true
        }.findAll()
    }

    List<Illness> list(boolean enabled, boolean admin) {
        return Illness.withCriteria {
            order("id", "desc")
            eq "enabled", enabled
        }.findAll()
    }

    int count(boolean enabled) {
        return Illness.countByEnabled(enabled)
    }

    Illness refresh(Long id) {
        Illness illness = Illness.findById(id)
        return illness
    }

    Illness byId(Long id) {
        return Illness.findById(id)
    }

    Illness byCode(Long code) {
        return Illness.findByCode(code)
    }

    Illness delete(Illness illness, LoginManager loginManager) {
        if (!illness) {
            return null
        }
        illness = Illness.findById(illness.id)
        illness.enabled = !illness.enabled
        return create(illness, loginManager)
    }

    Long findMaxCode() {
        Illness illness = Illness.withCriteria {
            order("id", "desc")
        }.find() as Illness
        if (!illness) {
            return 0
        }
        return illness.code.toLong()
    }

    Illness byDescription(String description) {
        return Illness.findByDescriptionAndEnabled(description, true)
    }

}
