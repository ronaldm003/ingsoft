package edu.utesa.ronald.ingsoft.services.control.recordcard

import edu.utesa.ronald.ingsoft.domains.control.recordcard.IntraBuccal
import edu.utesa.ronald.ingsoft.models.LoginManager
import edu.utesa.ronald.ingsoft.utils.Constants
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional

/**
 *  Created by Ronald on 16/11/17.
 */
@SuppressWarnings("GrMethodMayBeStatic")
@Service("intraBuccalService")
@Transactional(rollbackFor = Exception.class)
class IntraBuccalService {

    private IntraBuccal createBase(IntraBuccal intraBuccal, String userName) {
        if (intraBuccal != null && !intraBuccal.hasErrors()) {
            if (!IntraBuccal.exists(intraBuccal.getId())) {
                intraBuccal.dateCreated = new Date()
                intraBuccal.createdBy = userName
            }
            intraBuccal.lastUpdated = new Date()
            intraBuccal.modifyBy = userName
            return intraBuccal.save(flush: true, failOnError: true)
        }
        return null
    }

    IntraBuccal bootStrap(IntraBuccal intraBuccal) {
        return createBase(intraBuccal, Constants.ROOT)
    }

    IntraBuccal create(IntraBuccal intraBuccal, LoginManager loginManager) {
        return createBase(intraBuccal, loginManager.getUsername())
    }

    List<IntraBuccal> list(boolean enabled, int start, int size) {
        return IntraBuccal.withCriteria {
            firstResult(start)
            maxResults(size)
            order("id", "desc")
            eq "enabled", enabled
        }.findAll()
    }

    List<IntraBuccal> list(boolean enabled) {
        return IntraBuccal.withCriteria {
            order("id", "desc")
            eq "enabled", enabled
        }.findAll()
    }

    List<IntraBuccal> list(String description, int size) {
        return IntraBuccal.withCriteria {
            if (size > 0) {
                maxResults(size)
            }
            order("id", "desc")
            like "description", "%" + description + "%"
            eq "enabled", true
        }.findAll()
    }

    List<IntraBuccal> list(boolean enabled, boolean admin) {
        return IntraBuccal.withCriteria {
            order("id", "desc")
            eq "enabled", enabled
        }.findAll()
    }

    int count(boolean enabled) {
        return IntraBuccal.countByEnabled(enabled)
    }

    IntraBuccal refresh(Long id) {
        IntraBuccal intraBuccal = IntraBuccal.findById(id)
        return intraBuccal
    }

    IntraBuccal byId(Long id) {
        return IntraBuccal.findById(id)
    }

    IntraBuccal byCode(Long code) {
        return IntraBuccal.findByCode(code)
    }

    IntraBuccal delete(IntraBuccal intraBuccal, LoginManager loginManager) {
        if (!intraBuccal) {
            return null
        }
        intraBuccal = IntraBuccal.findById(intraBuccal.id)
        intraBuccal.enabled = !intraBuccal.enabled
        return create(intraBuccal, loginManager)
    }

    Long findMaxCode() {
        IntraBuccal intraBuccal = IntraBuccal.withCriteria {
            order("id", "desc")
        }.find() as IntraBuccal
        if (!intraBuccal) {
            return 0
        }
        return intraBuccal.code
    }

    IntraBuccal byQuestion(String question) {
        return IntraBuccal.findByQuestionAndEnabled(question, true)
    }

}
