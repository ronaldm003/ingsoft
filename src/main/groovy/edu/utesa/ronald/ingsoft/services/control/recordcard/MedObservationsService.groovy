package edu.utesa.ronald.ingsoft.services.control.recordcard

import edu.utesa.ronald.ingsoft.domains.control.recordcard.MedObservations
import edu.utesa.ronald.ingsoft.models.LoginManager
import edu.utesa.ronald.ingsoft.utils.Constants
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional

/**
 *  Created by Ronald on 16/11/17.
 */
@SuppressWarnings("GrMethodMayBeStatic")
@Service("medObservationsService")
@Transactional(rollbackFor = Exception.class)
class MedObservationsService {

    private MedObservations createBase(MedObservations medObservations, String userName) {
        if (medObservations != null && !medObservations.hasErrors()) {
            if (!MedObservations.exists(medObservations.getId())) {
                medObservations.dateCreated = new Date()
                medObservations.createdBy = userName
            }
            medObservations.lastUpdated = new Date()
            medObservations.modifyBy = userName
            return medObservations.save(flush: true, failOnError: true)
        }
        return null
    }

    MedObservations bootStrap(MedObservations medObservations) {
        return createBase(medObservations, Constants.ROOT)
    }

    MedObservations create(MedObservations medObservations, LoginManager loginManager) {
        return createBase(medObservations, loginManager.getUsername())
    }

    List<MedObservations> list(boolean enabled, int start, int size) {
        return MedObservations.withCriteria {
            firstResult(start)
            maxResults(size)
            order("id", "desc")
            eq "enabled", enabled
        }.findAll()
    }

    List<MedObservations> list(boolean enabled) {
        return MedObservations.withCriteria {
            order("id", "desc")
            eq "enabled", enabled
        }.findAll()
    }

    List<MedObservations> list(String description, int size) {
        return MedObservations.withCriteria {
            if (size > 0) {
                maxResults(size)
            }
            order("id", "desc")
            like "description", "%" + description + "%"
            eq "enabled", true
        }.findAll()
    }

    List<MedObservations> list(boolean enabled, boolean admin) {
        return MedObservations.withCriteria {
            order("id", "desc")
            eq "enabled", enabled
        }.findAll()
    }

    int count(boolean enabled) {
        return MedObservations.countByEnabled(enabled)
    }

    MedObservations refresh(Long id) {
        MedObservations medObservations = MedObservations.findById(id)
        return medObservations
    }

    MedObservations byId(Long id) {
        return MedObservations.findById(id)
    }

    MedObservations byCode(Long code) {
        return MedObservations.findByCode(code)
    }

    MedObservations delete(MedObservations medObservations, LoginManager loginManager) {
        if (!medObservations) {
            return null
        }
        medObservations = MedObservations.findById(medObservations.id)
        medObservations.enabled = !medObservations.enabled
        return create(medObservations, loginManager)
    }

    Long findMaxCode() {
        MedObservations medObservations = MedObservations.withCriteria {
            order("id", "desc")
        }.find() as MedObservations
        if (!medObservations) {
            return 0
        }
        return medObservations.number.toLong()
    }

    MedObservations byNumber(String number) {
        return MedObservations.findByNumberAndEnabled(description, true)
    }

}
