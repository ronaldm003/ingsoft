package edu.utesa.ronald.ingsoft.services.control.recordcard

import edu.utesa.ronald.ingsoft.domains.control.recordcard.Occlusion
import edu.utesa.ronald.ingsoft.models.LoginManager
import edu.utesa.ronald.ingsoft.utils.Constants
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional

/**
 *  Created by Ronald on 16/11/17.
 */
@SuppressWarnings("GrMethodMayBeStatic")
@Service("occlusionService")
@Transactional(rollbackFor = Exception.class)
class OcclusionService {

    private Occlusion createBase(Occlusion occlusion, String userName) {
        if (occlusion != null && !occlusion.hasErrors()) {
            if (!Occlusion.exists(occlusion.getId())) {
                occlusion.dateCreated = new Date()
                occlusion.createdBy = userName
            }
            occlusion.lastUpdated = new Date()
            occlusion.modifyBy = userName
            return occlusion.save(flush: true, failOnError: true)
        }
        return null
    }

    Occlusion bootStrap(Occlusion occlusion) {
        return createBase(occlusion, Constants.ROOT)
    }

    Occlusion create(Occlusion occlusion, LoginManager loginManager) {
        return createBase(occlusion, loginManager.getUsername())
    }

    List<Occlusion> list(boolean enabled, int start, int size) {
        return Occlusion.withCriteria {
            firstResult(start)
            maxResults(size)
            order("id", "desc")
            eq "enabled", enabled
        }.findAll()
    }

    List<Occlusion> list(boolean enabled) {
        return Occlusion.withCriteria {
            order("id", "desc")
            eq "enabled", enabled
        }.findAll()
    }

    List<Occlusion> list(String description, int size) {
        return Occlusion.withCriteria {
            if (size > 0) {
                maxResults(size)
            }
            order("id", "desc")
            like "description", "%" + description + "%"
            eq "enabled", true
        }.findAll()
    }

    List<Occlusion> list(boolean enabled, boolean admin) {
        return Occlusion.withCriteria {
            order("id", "desc")
            eq "enabled", enabled
        }.findAll()
    }

    int count(boolean enabled) {
        return Occlusion.countByEnabled(enabled)
    }

    Occlusion refresh(Long id) {
        Occlusion occlusion = Occlusion.findById(id)
        return occlusion
    }

    Occlusion byId(Long id) {
        return Occlusion.findById(id)
    }

    Occlusion byCode(Long code) {
        return Occlusion.findByCode(code)
    }

    Occlusion delete(Occlusion occlusion, LoginManager loginManager) {
        if (!occlusion) {
            return null
        }
        occlusion = Occlusion.findById(occlusion.id)
        occlusion.enabled = !occlusion.enabled
        return create(occlusion, loginManager)
    }

    Long findMaxCode() {
        Occlusion occlusion = Occlusion.withCriteria {
            order("id", "desc")
        }.find() as Occlusion
        if (!occlusion) {
            return 0
        }
        return occlusion.number.toLong()
    }

    Occlusion byNumber(String number) {
        return Occlusion.findByNumberAndEnabled(description, true)
    }

}
