package edu.utesa.ronald.ingsoft.services.control.recordcard

import edu.utesa.ronald.ingsoft.domains.control.recordcard.OralHygieneIndex
import edu.utesa.ronald.ingsoft.models.LoginManager
import edu.utesa.ronald.ingsoft.utils.Constants
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional

/**
 *  Created by Ronald on 16/11/17.
 */
@SuppressWarnings("GrMethodMayBeStatic")
@Service("oralHygieneIndexService")
@Transactional(rollbackFor = Exception.class)
class OralHygieneIndexService {

    private OralHygieneIndex createBase(OralHygieneIndex oralHygieneIndex, String userName) {
        if (oralHygieneIndex != null && !oralHygieneIndex.hasErrors()) {
            if (!OralHygieneIndex.exists(oralHygieneIndex.getId())) {
                oralHygieneIndex.dateCreated = new Date()
                oralHygieneIndex.createdBy = userName
            }
            oralHygieneIndex.lastUpdated = new Date()
            oralHygieneIndex.modifyBy = userName
            return oralHygieneIndex.save(flush: true, failOnError: true)
        }
        return null
    }

    OralHygieneIndex bootStrap(OralHygieneIndex oralHygieneIndex) {
        return createBase(oralHygieneIndex, Constants.ROOT)
    }

    OralHygieneIndex create(OralHygieneIndex oralHygieneIndex, LoginManager loginManager) {
        return createBase(oralHygieneIndex, loginManager.getUsername())
    }

    List<OralHygieneIndex> list(boolean enabled, int start, int size) {
        return OralHygieneIndex.withCriteria {
            firstResult(start)
            maxResults(size)
            order("id", "desc")
            eq "enabled", enabled
        }.findAll()
    }

    List<OralHygieneIndex> list(boolean enabled) {
        return OralHygieneIndex.withCriteria {
            order("id", "desc")
            eq "enabled", enabled
        }.findAll()
    }

    List<OralHygieneIndex> list(String description, int size) {
        return OralHygieneIndex.withCriteria {
            if (size > 0) {
                maxResults(size)
            }
            order("id", "desc")
            like "description", "%" + description + "%"
            eq "enabled", true
        }.findAll()
    }

    List<OralHygieneIndex> list(boolean enabled, boolean admin) {
        return OralHygieneIndex.withCriteria {
            order("id", "desc")
            eq "enabled", enabled
        }.findAll()
    }

    int count(boolean enabled) {
        return OralHygieneIndex.countByEnabled(enabled)
    }

    OralHygieneIndex refresh(Long id) {
        OralHygieneIndex oralHygieneIndex = OralHygieneIndex.findById(id)
        return oralHygieneIndex
    }

    OralHygieneIndex byId(Long id) {
        return OralHygieneIndex.findById(id)
    }

    OralHygieneIndex byCode(Long code) {
        return OralHygieneIndex.findByCode(code)
    }

    OralHygieneIndex delete(OralHygieneIndex oralHygieneIndex, LoginManager loginManager) {
        if (!oralHygieneIndex) {
            return null
        }
        oralHygieneIndex = OralHygieneIndex.findById(oralHygieneIndex.id)
        oralHygieneIndex.enabled = !oralHygieneIndex.enabled
        return create(oralHygieneIndex, loginManager)
    }

    Long findMaxCode() {
        OralHygieneIndex oralHygieneIndex = OralHygieneIndex.withCriteria {
            order("id", "desc")
        }.find() as OralHygieneIndex
        if (!oralHygieneIndex) {
            return 0
        }
        return oralHygieneIndex.number.toLong()
    }

    OralHygieneIndex byNumber(String number) {
        return OralHygieneIndex.findByNumberAndEnabled(description, true)
    }

}
