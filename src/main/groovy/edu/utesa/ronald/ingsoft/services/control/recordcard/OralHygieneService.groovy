package edu.utesa.ronald.ingsoft.services.control.recordcard

import edu.utesa.ronald.ingsoft.domains.control.recordcard.OralHygiene
import edu.utesa.ronald.ingsoft.models.LoginManager
import edu.utesa.ronald.ingsoft.utils.Constants
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional

/**
 *  Created by Ronald on 16/11/17.
 */
@SuppressWarnings("GrMethodMayBeStatic")
@Service("oralHygieneService")
@Transactional(rollbackFor = Exception.class)
class OralHygieneService {

    private OralHygiene createBase(OralHygiene oralHygiene, String userName) {
        if (oralHygiene != null && !oralHygiene.hasErrors()) {
            if (!OralHygiene.exists(oralHygiene.getId())) {
                oralHygiene.dateCreated = new Date()
                oralHygiene.createdBy = userName
            }
            oralHygiene.lastUpdated = new Date()
            oralHygiene.modifyBy = userName
            return oralHygiene.save(flush: true, failOnError: true)
        }
        return null
    }

    OralHygiene bootStrap(OralHygiene oralHygiene) {
        return createBase(oralHygiene, Constants.ROOT)
    }

    OralHygiene create(OralHygiene oralHygiene, LoginManager loginManager) {
        return createBase(oralHygiene, loginManager.getUsername())
    }

    List<OralHygiene> list(boolean enabled, int start, int size) {
        return OralHygiene.withCriteria {
            firstResult(start)
            maxResults(size)
            order("id", "desc")
            eq "enabled", enabled
        }.findAll()
    }

    List<OralHygiene> list(boolean enabled) {
        return OralHygiene.withCriteria {
            order("id", "desc")
            eq "enabled", enabled
        }.findAll()
    }

    List<OralHygiene> list(String description, int size) {
        return OralHygiene.withCriteria {
            if (size > 0) {
                maxResults(size)
            }
            order("id", "desc")
            like "description", "%" + description + "%"
            eq "enabled", true
        }.findAll()
    }

    List<OralHygiene> list(boolean enabled, boolean admin) {
        return OralHygiene.withCriteria {
            order("id", "desc")
            eq "enabled", enabled
        }.findAll()
    }

    int count(boolean enabled) {
        return OralHygiene.countByEnabled(enabled)
    }

    OralHygiene refresh(Long id) {
        OralHygiene oralHygiene = OralHygiene.findById(id)
        return oralHygiene
    }

    OralHygiene byId(Long id) {
        return OralHygiene.findById(id)
    }

    OralHygiene byCode(Long code) {
        return OralHygiene.findByCode(code)
    }

    OralHygiene delete(OralHygiene oralHygiene, LoginManager loginManager) {
        if (!oralHygiene) {
            return null
        }
        oralHygiene = OralHygiene.findById(oralHygiene.id)
        oralHygiene.enabled = !oralHygiene.enabled
        return create(oralHygiene, loginManager)
    }

    Long findMaxCode() {
        OralHygiene oralHygiene = OralHygiene.withCriteria {
            order("id", "desc")
        }.find() as OralHygiene
        if (!oralHygiene) {
            return 0
        }
        return oralHygiene.code.toLong()
    }

    OralHygiene byDescription(String description) {
        return OralHygiene.findByDescriptionAndEnabled(description, true)
    }

}
