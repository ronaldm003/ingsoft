package edu.utesa.ronald.ingsoft.services.control.recordcard

import edu.utesa.ronald.ingsoft.domains.control.recordcard.OralIllness
import edu.utesa.ronald.ingsoft.models.LoginManager
import edu.utesa.ronald.ingsoft.utils.Constants
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional

/**
 *  Created by Ronald on 16/11/17.
 */
@SuppressWarnings("GrMethodMayBeStatic")
@Service("oralIllnessService")
@Transactional(rollbackFor = Exception.class)
class OralIllnessService {

    private OralIllness createBase(OralIllness oralIllness, String userName) {
        if (oralIllness != null && !oralIllness.hasErrors()) {
            if (!OralIllness.exists(oralIllness.getId())) {
                oralIllness.dateCreated = new Date()
                oralIllness.createdBy = userName
            }
            oralIllness.lastUpdated = new Date()
            oralIllness.modifyBy = userName
            return oralIllness.save(flush: true, failOnError: true)
        }
        return null
    }

    OralIllness bootStrap(OralIllness oralIllness) {
        return createBase(oralIllness, Constants.ROOT)
    }

    OralIllness create(OralIllness oralIllness, LoginManager loginManager) {
        return createBase(oralIllness, loginManager.getUsername())
    }

    List<OralIllness> list(boolean enabled, int start, int size) {
        return OralIllness.withCriteria {
            firstResult(start)
            maxResults(size)
            order("id", "desc")
            eq "enabled", enabled
        }.findAll()
    }

    List<OralIllness> list(boolean enabled) {
        return OralIllness.withCriteria {
            order("id", "desc")
            eq "enabled", enabled
        }.findAll()
    }

    List<OralIllness> list(String description, int size) {
        return OralIllness.withCriteria {
            if (size > 0) {
                maxResults(size)
            }
            order("id", "desc")
            like "description", "%" + description + "%"
            eq "enabled", true
        }.findAll()
    }

    List<OralIllness> list(boolean enabled, boolean admin) {
        return OralIllness.withCriteria {
            order("id", "desc")
            eq "enabled", enabled
        }.findAll()
    }

    int count(boolean enabled) {
        return OralIllness.countByEnabled(enabled)
    }

    OralIllness refresh(Long id) {
        OralIllness oralIllness = OralIllness.findById(id)
        return oralIllness
    }

    OralIllness byId(Long id) {
        return OralIllness.findById(id)
    }

    OralIllness byCode(Long code) {
        return OralIllness.findByCode(code)
    }

    OralIllness delete(OralIllness oralIllness, LoginManager loginManager) {
        if (!oralIllness) {
            return null
        }
        oralIllness = OralIllness.findById(oralIllness.id)
        oralIllness.enabled = !oralIllness.enabled
        return create(oralIllness, loginManager)
    }

    Long findMaxCode() {
        OralIllness oralIllness = OralIllness.withCriteria {
            order("id", "desc")
        }.find() as OralIllness
        if (!oralIllness) {
            return 0
        }
        return oralIllness.code.toLong()
    }

    OralIllness byDescription(String description) {
        return OralIllness.findByDescriptionAndEnabled(description, true)
    }

}
