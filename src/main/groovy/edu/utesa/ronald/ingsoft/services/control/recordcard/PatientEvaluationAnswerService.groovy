package edu.utesa.ronald.ingsoft.services.control.recordcard

import edu.utesa.ronald.ingsoft.domains.control.recordcard.PatientEvaluationAnswer
import edu.utesa.ronald.ingsoft.models.LoginManager
import edu.utesa.ronald.ingsoft.utils.Constants
import edu.utesa.ronald.ingsoft.utils.TouchableDomain
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional

import java.util.function.Consumer

/**
 *  Created by Ronald on 16/11/17.
 */
@SuppressWarnings("GrMethodMayBeStatic")
@Service("patientEvaluationAnswerService")
@Transactional(rollbackFor = Exception.class)
class PatientEvaluationAnswerService {

    private PatientEvaluationAnswer createBase(PatientEvaluationAnswer patientEvaluationAnswer, String userName) {
        if (patientEvaluationAnswer != null && !patientEvaluationAnswer.hasErrors()) {
            if (!PatientEvaluationAnswer.exists(patientEvaluationAnswer.getId())) {
                patientEvaluationAnswer.dateCreated = new Date()
                patientEvaluationAnswer.createdBy = userName
            }
            patientEvaluationAnswer.lastUpdated = new Date()
            patientEvaluationAnswer.modifyBy = userName
            return patientEvaluationAnswer.save(flush: true, failOnError: true)
        }
        return null
    }

    PatientEvaluationAnswer bootStrap(PatientEvaluationAnswer patientEvaluationAnswer) {
        return createBase(patientEvaluationAnswer, Constants.ROOT)
    }

    PatientEvaluationAnswer create(PatientEvaluationAnswer patientEvaluationAnswer, LoginManager loginManager) {
        return createBase(patientEvaluationAnswer, loginManager.getUsername())
    }

    List<PatientEvaluationAnswer> list(boolean enabled, int start, int size) {
        return PatientEvaluationAnswer.withCriteria {
            firstResult(start)
            maxResults(size)
            order("id", "desc")
            eq "enabled", enabled
        }.findAll()
    }

    List<PatientEvaluationAnswer> list(boolean enabled) {
        return PatientEvaluationAnswer.withCriteria {
            order("id", "desc")
            eq "enabled", enabled
        }.findAll()
    }

    List<PatientEvaluationAnswer> list(String description, int size) {
        return PatientEvaluationAnswer.withCriteria {
            if (size > 0) {
                maxResults(size)
            }
            order("id", "desc")
            like "description", "%" + description + "%"
            eq "enabled", true
        }.findAll()
    }

    List<PatientEvaluationAnswer> list(boolean enabled, boolean admin) {
        return PatientEvaluationAnswer.withCriteria {
            order("id", "desc")
            eq "enabled", enabled
        }.findAll()
    }

    int count(boolean enabled) {
        return PatientEvaluationAnswer.countByEnabled(enabled)
    }

    PatientEvaluationAnswer refresh(Long id, Consumer<PatientEvaluationAnswer> touch = null) {
        PatientEvaluationAnswer patientEvaluationAnswer = PatientEvaluationAnswer.findById(id)
        TouchableDomain.applyTouch(patientEvaluationAnswer, touch)
        return patientEvaluationAnswer
    }

    PatientEvaluationAnswer byId(Long id) {
        return PatientEvaluationAnswer.findById(id)
    }

    PatientEvaluationAnswer byCode(Long code) {
        return PatientEvaluationAnswer.findByCode(code)
    }

    PatientEvaluationAnswer delete(PatientEvaluationAnswer patientEvaluationAnswer, LoginManager loginManager) {
        if (!patientEvaluationAnswer) {
            return null
        }
        patientEvaluationAnswer = PatientEvaluationAnswer.findById(patientEvaluationAnswer.id)
        patientEvaluationAnswer.enabled = !patientEvaluationAnswer.enabled
        return create(patientEvaluationAnswer, loginManager)
    }

    Long findMaxCode() {
        PatientEvaluationAnswer patientEvaluationAnswer = PatientEvaluationAnswer.withCriteria {
            order("id", "desc")
        }.find() as PatientEvaluationAnswer
        if (!patientEvaluationAnswer) {
            return 0
        }
        return patientEvaluationAnswer.number.toLong()
    }

}
