package edu.utesa.ronald.ingsoft.services.control.recordcard

import edu.utesa.ronald.ingsoft.domains.control.recordcard.PatientExtraBuccalAnswer
import edu.utesa.ronald.ingsoft.models.LoginManager
import edu.utesa.ronald.ingsoft.utils.Constants
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional

/**
 *  Created by Ronald on 16/11/17.
 */
@SuppressWarnings("GrMethodMayBeStatic")
@Service("patientExtraBuccalAnswerService")
@Transactional(rollbackFor = Exception.class)
class PatientExtraBuccalAnswerService {

    private PatientExtraBuccalAnswer createBase(PatientExtraBuccalAnswer patientExtraBuccalAnswer, String userName) {
        if (patientExtraBuccalAnswer != null && !patientExtraBuccalAnswer.hasErrors()) {
            if (!PatientExtraBuccalAnswer.exists(patientExtraBuccalAnswer.getId())) {
                patientExtraBuccalAnswer.dateCreated = new Date()
                patientExtraBuccalAnswer.createdBy = userName
            }
            patientExtraBuccalAnswer.lastUpdated = new Date()
            patientExtraBuccalAnswer.modifyBy = userName
            return patientExtraBuccalAnswer.save(flush: true, failOnError: true)
        }
        return null
    }

    PatientExtraBuccalAnswer bootStrap(PatientExtraBuccalAnswer patientExtraBuccalAnswer) {
        return createBase(patientExtraBuccalAnswer, Constants.ROOT)
    }

    PatientExtraBuccalAnswer create(PatientExtraBuccalAnswer patientExtraBuccalAnswer, LoginManager loginManager) {
        return createBase(patientExtraBuccalAnswer, loginManager.getUsername())
    }

    List<PatientExtraBuccalAnswer> list(boolean enabled, int start, int size) {
        return PatientExtraBuccalAnswer.withCriteria {
            firstResult(start)
            maxResults(size)
            order("id", "desc")
            eq "enabled", enabled
        }.findAll()
    }

    List<PatientExtraBuccalAnswer> list(boolean enabled) {
        return PatientExtraBuccalAnswer.withCriteria {
            order("id", "desc")
            eq "enabled", enabled
        }.findAll()
    }

    List<PatientExtraBuccalAnswer> list(String description, int size) {
        return PatientExtraBuccalAnswer.withCriteria {
            if (size > 0) {
                maxResults(size)
            }
            order("id", "desc")
            like "description", "%" + description + "%"
            eq "enabled", true
        }.findAll()
    }

    List<PatientExtraBuccalAnswer> list(boolean enabled, boolean admin) {
        return PatientExtraBuccalAnswer.withCriteria {
            order("id", "desc")
            eq "enabled", enabled
        }.findAll()
    }

    int count(boolean enabled) {
        return PatientExtraBuccalAnswer.countByEnabled(enabled)
    }

    PatientExtraBuccalAnswer refresh(Long id) {
        PatientExtraBuccalAnswer patientExtraBuccalAnswer = PatientExtraBuccalAnswer.findById(id)
        return patientExtraBuccalAnswer
    }

    PatientExtraBuccalAnswer byId(Long id) {
        return PatientExtraBuccalAnswer.findById(id)
    }

    PatientExtraBuccalAnswer byCode(Long code) {
        return PatientExtraBuccalAnswer.findByCode(code)
    }

    PatientExtraBuccalAnswer delete(PatientExtraBuccalAnswer patientExtraBuccalAnswer, LoginManager loginManager) {
        if (!patientExtraBuccalAnswer) {
            return null
        }
        patientExtraBuccalAnswer = PatientExtraBuccalAnswer.findById(patientExtraBuccalAnswer.id)
        patientExtraBuccalAnswer.enabled = !patientExtraBuccalAnswer.enabled
        return create(patientExtraBuccalAnswer, loginManager)
    }

    Long findMaxCode() {
        PatientExtraBuccalAnswer patientExtraBuccalAnswer = PatientExtraBuccalAnswer.withCriteria {
            order("id", "desc")
        }.find() as PatientExtraBuccalAnswer
        if (!patientExtraBuccalAnswer) {
            return 0
        }
        return patientExtraBuccalAnswer.number.toLong()
    }

}
