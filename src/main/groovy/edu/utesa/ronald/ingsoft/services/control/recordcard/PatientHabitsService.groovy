package edu.utesa.ronald.ingsoft.services.control.recordcard

import edu.utesa.ronald.ingsoft.domains.control.recordcard.PatientHabits
import edu.utesa.ronald.ingsoft.models.LoginManager
import edu.utesa.ronald.ingsoft.utils.Constants
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional

/**
 *  Created by Ronald on 16/11/17.
 */
@SuppressWarnings("GrMethodMayBeStatic")
@Service("patientHabitsService")
@Transactional(rollbackFor = Exception.class)
class PatientHabitsService {

    private PatientHabits createBase(PatientHabits patientHabits, String userName) {
        if (patientHabits != null && !patientHabits.hasErrors()) {
            if (!PatientHabits.exists(patientHabits.getId())) {
                patientHabits.dateCreated = new Date()
                patientHabits.createdBy = userName
            }
            patientHabits.lastUpdated = new Date()
            patientHabits.modifyBy = userName
            return patientHabits.save(flush: true, failOnError: true)
        }
        return null
    }

    PatientHabits bootStrap(PatientHabits patientHabits) {
        return createBase(patientHabits, Constants.ROOT)
    }

    PatientHabits create(PatientHabits patientHabits, LoginManager loginManager) {
        return createBase(patientHabits, loginManager.getUsername())
    }

    List<PatientHabits> list(boolean enabled, int start, int size) {
        return PatientHabits.withCriteria {
            firstResult(start)
            maxResults(size)
            order("id", "desc")
            eq "enabled", enabled
        }.findAll()
    }

    List<PatientHabits> list(boolean enabled) {
        return PatientHabits.withCriteria {
            order("id", "desc")
            eq "enabled", enabled
        }.findAll()
    }

    List<PatientHabits> list(String description, int size) {
        return PatientHabits.withCriteria {
            if (size > 0) {
                maxResults(size)
            }
            order("id", "desc")
            like "description", "%" + description + "%"
            eq "enabled", true
        }.findAll()
    }

    List<PatientHabits> list(boolean enabled, boolean admin) {
        return PatientHabits.withCriteria {
            order("id", "desc")
            eq "enabled", enabled
        }.findAll()
    }

    int count(boolean enabled) {
        return PatientHabits.countByEnabled(enabled)
    }

    PatientHabits refresh(Long id) {
        PatientHabits patientHabits = PatientHabits.findById(id)
        return patientHabits
    }

    PatientHabits byId(Long id) {
        return PatientHabits.findById(id)
    }

    PatientHabits byCode(Long code) {
        return PatientHabits.findByCode(code)
    }

    PatientHabits delete(PatientHabits patientHabits, LoginManager loginManager) {
        if (!patientHabits) {
            return null
        }
        patientHabits = PatientHabits.findById(patientHabits.id)
        patientHabits.enabled = !patientHabits.enabled
        return create(patientHabits, loginManager)
    }

    Long findMaxCode() {
        PatientHabits patientHabits = PatientHabits.withCriteria {
            order("id", "desc")
        }.find() as PatientHabits
        if (!patientHabits) {
            return 0
        }
        return patientHabits.number.toLong()
    }

}
