package edu.utesa.ronald.ingsoft.services.control.recordcard

import edu.utesa.ronald.ingsoft.domains.control.recordcard.PatientIllness
import edu.utesa.ronald.ingsoft.models.LoginManager
import edu.utesa.ronald.ingsoft.utils.Constants
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional

/**
 *  Created by Ronald on 16/11/17.
 */
@SuppressWarnings("GrMethodMayBeStatic")
@Service("patientIllnessService")
@Transactional(rollbackFor = Exception.class)
class PatientIllnessService {

    private PatientIllness createBase(PatientIllness patientIllness, String userName) {
        if (patientIllness != null && !patientIllness.hasErrors()) {
            if (!PatientIllness.exists(patientIllness.getId())) {
                patientIllness.dateCreated = new Date()
                patientIllness.createdBy = userName
            }
            patientIllness.lastUpdated = new Date()
            patientIllness.modifyBy = userName
            return patientIllness.save(flush: true, failOnError: true)
        }
        return null
    }

    PatientIllness bootStrap(PatientIllness patientIllness) {
        return createBase(patientIllness, Constants.ROOT)
    }

    PatientIllness create(PatientIllness patientIllness, LoginManager loginManager) {
        return createBase(patientIllness, loginManager.getUsername())
    }

    List<PatientIllness> list(boolean enabled, int start, int size) {
        return PatientIllness.withCriteria {
            firstResult(start)
            maxResults(size)
            order("id", "desc")
            eq "enabled", enabled
        }.findAll()
    }

    List<PatientIllness> list(boolean enabled) {
        return PatientIllness.withCriteria {
            order("id", "desc")
            eq "enabled", enabled
        }.findAll()
    }

    List<PatientIllness> list(String description, int size) {
        return PatientIllness.withCriteria {
            if (size > 0) {
                maxResults(size)
            }
            order("id", "desc")
            like "description", "%" + description + "%"
            eq "enabled", true
        }.findAll()
    }

    List<PatientIllness> list(boolean enabled, boolean admin) {
        return PatientIllness.withCriteria {
            order("id", "desc")
            eq "enabled", enabled
        }.findAll()
    }

    int count(boolean enabled) {
        return PatientIllness.countByEnabled(enabled)
    }

    PatientIllness refresh(Long id) {
        PatientIllness patientIllness = PatientIllness.findById(id)
        return patientIllness
    }

    PatientIllness byId(Long id) {
        return PatientIllness.findById(id)
    }

    PatientIllness byCode(Long code) {
        return PatientIllness.findByCode(code)
    }

    PatientIllness delete(PatientIllness patientIllness, LoginManager loginManager) {
        if (!patientIllness) {
            return null
        }
        patientIllness = PatientIllness.findById(patientIllness.id)
        patientIllness.enabled = !patientIllness.enabled
        return create(patientIllness, loginManager)
    }

    Long findMaxCode() {
        PatientIllness patientIllness = PatientIllness.withCriteria {
            order("id", "desc")
        }.find() as PatientIllness
        if (!patientIllness) {
            return 0
        }
        return patientIllness.number.toLong()
    }

}
