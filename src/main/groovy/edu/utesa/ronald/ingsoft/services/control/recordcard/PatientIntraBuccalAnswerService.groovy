package edu.utesa.ronald.ingsoft.services.control.recordcard

import edu.utesa.ronald.ingsoft.domains.control.recordcard.PatientIntraBuccalAnswer
import edu.utesa.ronald.ingsoft.models.LoginManager
import edu.utesa.ronald.ingsoft.utils.Constants
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional

/**
 *  Created by Ronald on 16/11/17.
 */
@SuppressWarnings("GrMethodMayBeStatic")
@Service("patientIntraBuccalAnswerService")
@Transactional(rollbackFor = Exception.class)
class PatientIntraBuccalAnswerService {

    private PatientIntraBuccalAnswer createBase(PatientIntraBuccalAnswer patientIntraBuccalAnswer, String userName) {
        if (patientIntraBuccalAnswer != null && !patientIntraBuccalAnswer.hasErrors()) {
            if (!PatientIntraBuccalAnswer.exists(patientIntraBuccalAnswer.getId())) {
                patientIntraBuccalAnswer.dateCreated = new Date()
                patientIntraBuccalAnswer.createdBy = userName
            }
            patientIntraBuccalAnswer.lastUpdated = new Date()
            patientIntraBuccalAnswer.modifyBy = userName
            return patientIntraBuccalAnswer.save(flush: true, failOnError: true)
        }
        return null
    }

    PatientIntraBuccalAnswer bootStrap(PatientIntraBuccalAnswer patientIntraBuccalAnswer) {
        return createBase(patientIntraBuccalAnswer, Constants.ROOT)
    }

    PatientIntraBuccalAnswer create(PatientIntraBuccalAnswer patientIntraBuccalAnswer, LoginManager loginManager) {
        return createBase(patientIntraBuccalAnswer, loginManager.getUsername())
    }

    List<PatientIntraBuccalAnswer> list(boolean enabled, int start, int size) {
        return PatientIntraBuccalAnswer.withCriteria {
            firstResult(start)
            maxResults(size)
            order("id", "desc")
            eq "enabled", enabled
        }.findAll()
    }

    List<PatientIntraBuccalAnswer> list(boolean enabled) {
        return PatientIntraBuccalAnswer.withCriteria {
            order("id", "desc")
            eq "enabled", enabled
        }.findAll()
    }

    List<PatientIntraBuccalAnswer> list(String description, int size) {
        return PatientIntraBuccalAnswer.withCriteria {
            if (size > 0) {
                maxResults(size)
            }
            order("id", "desc")
            like "description", "%" + description + "%"
            eq "enabled", true
        }.findAll()
    }

    List<PatientIntraBuccalAnswer> list(boolean enabled, boolean admin) {
        return PatientIntraBuccalAnswer.withCriteria {
            order("id", "desc")
            eq "enabled", enabled
        }.findAll()
    }

    int count(boolean enabled) {
        return PatientIntraBuccalAnswer.countByEnabled(enabled)
    }

    PatientIntraBuccalAnswer refresh(Long id) {
        PatientIntraBuccalAnswer patientIntraBuccalAnswer = PatientIntraBuccalAnswer.findById(id)
        return patientIntraBuccalAnswer
    }

    PatientIntraBuccalAnswer byId(Long id) {
        return PatientIntraBuccalAnswer.findById(id)
    }

    PatientIntraBuccalAnswer byCode(Long code) {
        return PatientIntraBuccalAnswer.findByCode(code)
    }

    PatientIntraBuccalAnswer delete(PatientIntraBuccalAnswer patientIntraBuccalAnswer, LoginManager loginManager) {
        if (!patientIntraBuccalAnswer) {
            return null
        }
        patientIntraBuccalAnswer = PatientIntraBuccalAnswer.findById(patientIntraBuccalAnswer.id)
        patientIntraBuccalAnswer.enabled = !patientIntraBuccalAnswer.enabled
        return create(patientIntraBuccalAnswer, loginManager)
    }

    Long findMaxCode() {
        PatientIntraBuccalAnswer patientIntraBuccalAnswer = PatientIntraBuccalAnswer.withCriteria {
            order("id", "desc")
        }.find() as PatientIntraBuccalAnswer
        if (!patientIntraBuccalAnswer) {
            return 0
        }
        return patientIntraBuccalAnswer.number.toLong()
    }

}
