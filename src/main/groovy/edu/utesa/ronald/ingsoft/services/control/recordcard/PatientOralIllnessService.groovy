package edu.utesa.ronald.ingsoft.services.control.recordcard

import edu.utesa.ronald.ingsoft.domains.control.recordcard.PatientOralIllness
import edu.utesa.ronald.ingsoft.models.LoginManager
import edu.utesa.ronald.ingsoft.utils.Constants
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional

/**
 *  Created by Ronald on 16/11/17.
 */
@SuppressWarnings("GrMethodMayBeStatic")
@Service("patientOralIllnessService")
@Transactional(rollbackFor = Exception.class)
class PatientOralIllnessService {

    private PatientOralIllness createBase(PatientOralIllness patientOralIllness, String userName) {
        if (patientOralIllness != null && !patientOralIllness.hasErrors()) {
            if (!PatientOralIllness.exists(patientOralIllness.getId())) {
                patientOralIllness.dateCreated = new Date()
                patientOralIllness.createdBy = userName
            }
            patientOralIllness.lastUpdated = new Date()
            patientOralIllness.modifyBy = userName
            return patientOralIllness.save(flush: true, failOnError: true)
        }
        return null
    }

    PatientOralIllness bootStrap(PatientOralIllness patientOralIllness) {
        return createBase(patientOralIllness, Constants.ROOT)
    }

    PatientOralIllness create(PatientOralIllness patientOralIllness, LoginManager loginManager) {
        return createBase(patientOralIllness, loginManager.getUsername())
    }

    List<PatientOralIllness> list(boolean enabled, int start, int size) {
        return PatientOralIllness.withCriteria {
            firstResult(start)
            maxResults(size)
            order("id", "desc")
            eq "enabled", enabled
        }.findAll()
    }

    List<PatientOralIllness> list(boolean enabled) {
        return PatientOralIllness.withCriteria {
            order("id", "desc")
            eq "enabled", enabled
        }.findAll()
    }

    List<PatientOralIllness> list(String description, int size) {
        return PatientOralIllness.withCriteria {
            if (size > 0) {
                maxResults(size)
            }
            order("id", "desc")
            like "description", "%" + description + "%"
            eq "enabled", true
        }.findAll()
    }

    List<PatientOralIllness> list(boolean enabled, boolean admin) {
        return PatientOralIllness.withCriteria {
            order("id", "desc")
            eq "enabled", enabled
        }.findAll()
    }

    int count(boolean enabled) {
        return PatientOralIllness.countByEnabled(enabled)
    }

    PatientOralIllness refresh(Long id) {
        PatientOralIllness patientOralIllness = PatientOralIllness.findById(id)
        return patientOralIllness
    }

    PatientOralIllness byId(Long id) {
        return PatientOralIllness.findById(id)
    }

    PatientOralIllness byCode(Long code) {
        return PatientOralIllness.findByCode(code)
    }

    PatientOralIllness delete(PatientOralIllness patientOralIllness, LoginManager loginManager) {
        if (!patientOralIllness) {
            return null
        }
        patientOralIllness = PatientOralIllness.findById(patientOralIllness.id)
        patientOralIllness.enabled = !patientOralIllness.enabled
        return create(patientOralIllness, loginManager)
    }

    Long findMaxCode() {
        PatientOralIllness patientOralIllness = PatientOralIllness.withCriteria {
            order("id", "desc")
        }.find() as PatientOralIllness
        if (!patientOralIllness) {
            return 0
        }
        return patientOralIllness.number.toLong()
    }

}
