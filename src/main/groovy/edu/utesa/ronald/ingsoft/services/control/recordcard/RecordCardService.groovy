package edu.utesa.ronald.ingsoft.services.control.recordcard

import edu.utesa.ronald.ingsoft.domains.admin.Cycle
import edu.utesa.ronald.ingsoft.domains.admin.Student
import edu.utesa.ronald.ingsoft.domains.admin.Teacher
import edu.utesa.ronald.ingsoft.domains.control.Patient
import edu.utesa.ronald.ingsoft.domains.control.recordcard.RecordCard
import edu.utesa.ronald.ingsoft.models.LoginManager
import edu.utesa.ronald.ingsoft.utils.Constants
import edu.utesa.ronald.ingsoft.utils.TouchableDomain
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional

import java.util.function.Consumer

/**
 *  Created by Ronald on 16/11/17.
 */
@SuppressWarnings("GrMethodMayBeStatic")
@Service("recordCardService")
@Transactional(rollbackFor = Exception.class)
class RecordCardService {

    private RecordCard createBase(RecordCard recordCard, String userName) {
        if (recordCard != null && !recordCard.hasErrors()) {
            if (!RecordCard.exists(recordCard.getId())) {
                recordCard.dateCreated = new Date()
                recordCard.createdBy = userName
            }
            recordCard.lastUpdated = new Date()
            recordCard.modifyBy = userName
            return recordCard.save(flush: true, failOnError: true)
        }
        return null
    }

    RecordCard bootStrap(RecordCard recordCard) {
        return createBase(recordCard, Constants.ROOT)
    }

    RecordCard create(RecordCard recordCard, LoginManager loginManager) {
        return createBase(recordCard, loginManager.getUsername())
    }

    List<RecordCard> list(boolean enabled, int start, int size, Consumer<RecordCard> touch = null) {
        List<RecordCard> recordCardList = (List<RecordCard>) RecordCard.withCriteria {
            firstResult(start)
            maxResults(size)
            order("id", "desc")
            eq "enabled", enabled
        }.findAll()
        TouchableDomain.applyTouch(recordCardList, touch)
        return recordCardList
    }

    List<RecordCard> list(boolean enabled, Consumer<RecordCard> touch = null) {
        List<RecordCard> recordCardList = RecordCard.withCriteria {
            order("id", "desc")
            eq "enabled", enabled
        }.findAll()
        TouchableDomain.applyTouch(recordCardList, touch)
        return recordCardList
    }

    List<RecordCard> list(String description, int size, Consumer<RecordCard> touch = null) {
        List<RecordCard> recordCardList = RecordCard.withCriteria {
            if (size > 0) {
                maxResults(size)
            }
            order("id", "desc")
            like "description", "%" + description + "%"
            eq "enabled", true
        }.findAll()
        TouchableDomain.applyTouch(recordCardList, touch)
        return recordCardList
    }

    List<RecordCard> list(boolean enabled, boolean admin, Consumer<RecordCard> touch = null) {
        List<RecordCard> recordCardList = RecordCard.withCriteria {
            order("id", "desc")
            eq "enabled", enabled
        }.findAll()
        TouchableDomain.applyTouch(recordCardList, touch)
        return recordCardList
    }

    int count(boolean enabled) {
        return RecordCard.countByEnabled(enabled)
    }

    int count(boolean enabled, String recordCardNumber, Cycle cycle, Student student, Teacher teacher, Patient patient) {
        return RecordCard.createCriteria().count() {
            order("id", "desc")
            if (recordCardNumber) {
                eq "recordId", recordCardNumber
            }
            if (cycle) {
                eq "cycle.id", cycle.id
            }
            if (student) {
                eq "student", student
            }
            if (teacher) {
                eq "teacher", teacher
            }
            if (patient) {
                eq "patient", patient
            }
            eq "enabled", enabled
        }
    }

    List<RecordCard> list(boolean enabled, int start, int size, String recordCardNumber, Cycle cycle, Student student, Teacher teacher, Patient patient, Consumer<RecordCard> touch = null) {
        List<RecordCard> recordCardList = RecordCard.withCriteria {
            firstResult(start)
            maxResults(size)
            order("id", "desc")

            if (recordCardNumber) {
                eq "recordId", recordCardNumber
            }
            if (cycle) {
                eq "cycle.id", cycle.id
            }
            if (student) {
                eq "student", student
            }
            if (teacher) {
                eq "teacher", teacher
            }
            if (patient) {
                eq "patient", patient
            }
            eq "enabled", enabled
        }.findAll()
        TouchableDomain.applyTouch(recordCardList, touch)
        return recordCardList
    }

    RecordCard refresh(Long id, Consumer<RecordCard> touch = null) {
        RecordCard recordCard = RecordCard.findById(id)
        TouchableDomain.applyTouch(recordCard, touch)
        return recordCard
    }

    RecordCard byId(Long id, Consumer<RecordCard> touch = null) {
        RecordCard recordCard = RecordCard.findById(id)
        TouchableDomain.applyTouch(recordCard, touch)
        return recordCard
    }

    RecordCard byCode(Long code, Consumer<RecordCard> touch = null) {
        RecordCard recordCard = RecordCard.findByCode(code)
        TouchableDomain.applyTouch(recordCard, touch)
        return recordCard
    }

    RecordCard delete(RecordCard recordCard, LoginManager loginManager) {
        if (!recordCard) {
            return null
        }
        recordCard = RecordCard.findById(recordCard.id)
        recordCard.enabled = !recordCard.enabled
        return create(recordCard, loginManager)
    }

    Long findMaxCode(Consumer<RecordCard> touch = null) {
        RecordCard recordCard = RecordCard.withCriteria {
            order("id", "desc")
        }.find() as RecordCard
        if (!recordCard) {
            return 0
        }
        TouchableDomain.applyTouch(recordCard, touch)
        return recordCard.recordId
    }

    RecordCard byRecordId(String recordId, Consumer<RecordCard> touch = null) {
        RecordCard recordCard = RecordCard.findByRecordIdAndEnabled(description, true)
        TouchableDomain.applyTouch(recordCard, touch)
        return recordCard
    }

    RecordCard byPatient(Patient patient, Consumer<RecordCard> touch = null) {
        RecordCard recordCard = RecordCard.findByPatientAndEnabled(patient, true)
        TouchableDomain.applyTouch(recordCard, touch)
        return recordCard
    }

}
