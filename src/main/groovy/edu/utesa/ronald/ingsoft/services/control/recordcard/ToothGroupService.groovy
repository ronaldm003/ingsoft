package edu.utesa.ronald.ingsoft.services.control.recordcard

import edu.utesa.ronald.ingsoft.domains.control.recordcard.ToothGroup
import edu.utesa.ronald.ingsoft.models.LoginManager
import edu.utesa.ronald.ingsoft.utils.Constants
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional

/**
 *  Created by Ronald on 16/11/17.
 */
@SuppressWarnings("GrMethodMayBeStatic")
@Service("toothGroupService")
@Transactional(rollbackFor = Exception.class)
class ToothGroupService {

    private ToothGroup createBase(ToothGroup toothGroup, String userName) {
        if (toothGroup != null && !toothGroup.hasErrors()) {
            if (!ToothGroup.exists(toothGroup.getId())) {
                toothGroup.dateCreated = new Date()
                toothGroup.createdBy = userName
            }
            toothGroup.lastUpdated = new Date()
            toothGroup.modifyBy = userName
            return toothGroup.save(flush: true, failOnError: true)
        }
        return null
    }

    ToothGroup bootStrap(ToothGroup toothGroup) {
        return createBase(toothGroup, Constants.ROOT)
    }

    ToothGroup create(ToothGroup toothGroup, LoginManager loginManager) {
        return createBase(toothGroup, loginManager.getUsername())
    }

    List<ToothGroup> list(boolean enabled, int start, int size) {
        return ToothGroup.withCriteria {
            firstResult(start)
            maxResults(size)
            order("id", "desc")
            eq "enabled", enabled
        }.findAll()
    }

    List<ToothGroup> list(boolean enabled) {
        return ToothGroup.withCriteria {
            order("id", "desc")
            eq "enabled", enabled
        }.findAll()
    }

    List<ToothGroup> list(String description, int size) {
        return ToothGroup.withCriteria {
            if (size > 0) {
                maxResults(size)
            }
            order("id", "desc")
            like "description", "%" + description + "%"
            eq "enabled", true
        }.findAll()
    }

    List<ToothGroup> list(boolean enabled, boolean admin) {
        return ToothGroup.withCriteria {
            order("id", "desc")
            eq "enabled", enabled
        }.findAll()
    }

    int count(boolean enabled) {
        return ToothGroup.countByEnabled(enabled)
    }

    ToothGroup refresh(Long id) {
        ToothGroup toothGroup = ToothGroup.findById(id)
        return toothGroup
    }

    ToothGroup byId(Long id) {
        return ToothGroup.findById(id)
    }

    ToothGroup delete(ToothGroup toothGroup, LoginManager loginManager) {
        if (!toothGroup) {
            return null
        }
        toothGroup = ToothGroup.findById(toothGroup.id)
        toothGroup.enabled = !toothGroup.enabled
        return create(toothGroup, loginManager)
    }

    ToothGroup byName(String name) {
        return ToothGroup.findByNameAndEnabled(name, true)
    }

}
