package edu.utesa.ronald.ingsoft.services.control.recordcard

import edu.utesa.ronald.ingsoft.domains.control.recordcard.Tooth
import edu.utesa.ronald.ingsoft.models.LoginManager
import edu.utesa.ronald.ingsoft.utils.Constants
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional

/**
 *  Created by Ronald on 16/11/17.
 */
@SuppressWarnings("GrMethodMayBeStatic")
@Service("toothService")
@Transactional(rollbackFor = Exception.class)
class ToothService {

    private Tooth createBase(Tooth tooth, String userName) {
        if (tooth != null && !tooth.hasErrors()) {
            if (!Tooth.exists(tooth.getId())) {
                tooth.dateCreated = new Date()
                tooth.createdBy = userName
            }
            tooth.lastUpdated = new Date()
            tooth.modifyBy = userName
            return tooth.save(flush: true, failOnError: true)
        }
        return null
    }

    Tooth bootStrap(Tooth tooth) {
        return createBase(tooth, Constants.ROOT)
    }

    Tooth create(Tooth tooth, LoginManager loginManager) {
        return createBase(tooth, loginManager.getUsername())
    }

    List<Tooth> list(boolean enabled, int start, int size) {
        return Tooth.withCriteria {
            firstResult(start)
            maxResults(size)
            order("id", "desc")
            eq "enabled", enabled
        }.findAll()
    }

    List<Tooth> list(boolean enabled) {
        return Tooth.withCriteria {
            order("id", "desc")
            eq "enabled", enabled
        }.findAll()
    }

    List<Tooth> list(String description, int size) {
        return Tooth.withCriteria {
            if (size > 0) {
                maxResults(size)
            }
            order("id", "desc")
            like "description", "%" + description + "%"
            eq "enabled", true
        }.findAll()
    }

    List<Tooth> list(boolean enabled, boolean admin) {
        return Tooth.withCriteria {
            order("id", "desc")
            eq "enabled", enabled
        }.findAll()
    }

    int count(boolean enabled) {
        return Tooth.countByEnabled(enabled)
    }

    Tooth refresh(Long id) {
        Tooth tooth = Tooth.findById(id)
        return tooth
    }

    Tooth byId(Long id) {
        return Tooth.findById(id)
    }

    Tooth byCode(Long code) {
        return Tooth.findByCode(code)
    }

    Tooth delete(Tooth tooth, LoginManager loginManager) {
        if (!tooth) {
            return null
        }
        tooth = Tooth.findById(tooth.id)
        tooth.enabled = !tooth.enabled
        return create(tooth, loginManager)
    }

    Long findMaxCode() {
        Tooth tooth = Tooth.withCriteria {
            order("id", "desc")
        }.find() as Tooth
        if (!tooth) {
            return 0
        }
        return tooth.number.toLong()
    }

    Tooth byNumber(String number) {
        return Tooth.findByNumberAndEnabled(number, true)
    }

}
