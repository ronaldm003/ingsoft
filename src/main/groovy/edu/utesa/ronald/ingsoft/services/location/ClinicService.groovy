package edu.utesa.ronald.ingsoft.services.location

import edu.utesa.ronald.ingsoft.domains.location.Clinic
import edu.utesa.ronald.ingsoft.models.LoginManager
import edu.utesa.ronald.ingsoft.utils.Constants
import edu.utesa.ronald.ingsoft.utils.TouchableDomain
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional

import java.util.function.Consumer

/**
 *  Created by Ronald on 16/11/17.
 */
@SuppressWarnings("GrMethodMayBeStatic")
@Service("clinicService")
@Transactional(rollbackFor = Exception.class)
class ClinicService {

    private Clinic createBase(Clinic clinic, String userName) {
        if (clinic != null && !clinic.hasErrors()) {
            if (!Clinic.exists(clinic.getId())) {
                clinic.dateCreated = new Date()
                clinic.createdBy = userName
            }
            clinic.lastUpdated = new Date()
            clinic.modifyBy = userName
            return clinic.save(flush: true, failOnError: true)
        }
        return null
    }

    Clinic bootStrap(Clinic clinic) {
        return createBase(clinic, Constants.ROOT)
    }

    Clinic create(Clinic clinic, LoginManager loginManager) {
        return createBase(clinic, loginManager.getUsername())
    }

    List<Clinic> list(boolean enabled, int start, int size, Consumer<Clinic> touch = null) {
        List<Clinic> clinicList = Clinic.withCriteria {
            firstResult(start)
            maxResults(size)
            order("id", "desc")
            eq "enabled", enabled
        }.findAll()
        TouchableDomain.applyTouch(clinicList, touch)
        return clinicList
    }

    List<Clinic> list(boolean enabled, Consumer<Clinic> touch = null) {
        List<Clinic> clinicList = Clinic.withCriteria {
            order("id", "desc")
            eq "enabled", enabled
        }.findAll()
        TouchableDomain.applyTouch(clinicList, touch)
        return clinicList
    }

    List<Clinic> list(String description, int size, Consumer<Clinic> touch = null) {
        List<Clinic> clinicList = Clinic.withCriteria {
            if (size > 0) {
                maxResults(size)
            }
            order("id", "desc")
            like "description", "%" + description + "%"
            eq "enabled", true
        }.findAll()
        TouchableDomain.applyTouch(clinicList, touch)
        return clinicList
    }

    List<Clinic> list(boolean enabled, boolean admin) {
        return Clinic.withCriteria {
            order("id", "desc")
            eq "enabled", enabled
        }.findAll()
    }

    int count(boolean enabled) {
        return Clinic.countByEnabled(enabled)
    }

    Clinic refresh(Long id, Consumer<Clinic> touch = null) {
        Clinic clinic = Clinic.findById(id)
        TouchableDomain.applyTouch(clinic, touch)
        return clinic
    }

    Clinic byId(Long id) {
        return Clinic.findById(id)
    }

    Clinic byCode(Long code) {
        return Clinic.findByCode(code)
    }

    Clinic delete(Clinic clinic, LoginManager loginManager) {
        if (!clinic) {
            return null
        }
        clinic = Clinic.findById(clinic.id)
        clinic.enabled = !clinic.enabled
        return create(clinic, loginManager)
    }

    Long findMaxCode() {
        Clinic clinic = Clinic.withCriteria {
            order("id", "desc")
        }.find() as Clinic
        if (!clinic) {
            return 0
        }
        return clinic.code
    }

    Clinic byDescription(String description) {
        return Clinic.findByDescriptionAndEnabled(description, true)
    }

}
