package edu.utesa.ronald.ingsoft.services.location

import edu.utesa.ronald.ingsoft.domains.location.Location
import edu.utesa.ronald.ingsoft.models.LoginManager
import edu.utesa.ronald.ingsoft.utils.Constants
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional

/**
 *  Created by Ronald on 16/11/17.
 */
@SuppressWarnings("GrMethodMayBeStatic")
@Service("locationService")
@Transactional(rollbackFor = Exception.class)
class LocationService {

    private Location createBase(Location location, String userName) {
        if (location != null && !location.hasErrors()) {
            if (!Location.exists(location.getId())) {
                location.dateCreated = new Date()
                location.createdBy = userName
            }
            location.lastUpdated = new Date()
            location.modifyBy = userName
            return location.save(flush: true, failOnError: true)
        }
        return null
    }

    Location bootStrap(Location location) {
        return createBase(location, Constants.ROOT)
    }

    Location create(Location location, LoginManager loginManager) {
        return createBase(location, loginManager.getUsername())
    }

    List<Location> list(boolean enabled, int start, int size) {
        return Location.withCriteria {
            firstResult(start)
            maxResults(size)
            order("id", "desc")
            eq "enabled", enabled
        }.findAll()
    }

    List<Location> list(boolean enabled) {
        return Location.withCriteria {
            order("id", "desc")
            eq "enabled", enabled
        }.findAll()
    }

    List<Location> list(String description, int size) {
        return Location.withCriteria {
            if (size > 0) {
                maxResults(size)
            }
            order("id", "desc")
            like "description", "%" + description + "%"
            eq "enabled", true
        }.findAll()
    }

    List<Location> list(boolean enabled, boolean admin) {
        return Location.withCriteria {
            order("id", "desc")
            eq "enabled", enabled
        }.findAll()
    }

    int count(boolean enabled) {
        return Location.countByEnabled(enabled)
    }

    Location refresh(Long id) {
        Location location = Location.findById(id)
        return location
    }

    Location byId(Long id) {
        return Location.findById(id)
    }

    Location byCode(Long code) {
        return Location.findByCode(code)
    }

    Location delete(Location location, LoginManager loginManager) {
        if (!location) {
            return null
        }
        location = Location.findById(location.id)
        location.enabled = !location.enabled
        return create(location, loginManager)
    }

    Long findMaxCode() {
        Location location = Location.withCriteria {
            order("id", "desc")
        }.find() as Location
        if (!location) {
            return 1
        }
        return location.code
    }

    Location byDescription(String description) {
        return Location.findByDescriptionAndEnabled(description, true)
    }

}
