package edu.utesa.ronald.ingsoft.services.mail

import edu.utesa.ronald.ingsoft.domains.mail.Email
import edu.utesa.ronald.ingsoft.models.LoginManager
import edu.utesa.ronald.ingsoft.utils.Constants
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional

/**
 *  Created by ronald on 07/24/17.
 */
@SuppressWarnings("GrMethodMayBeStatic")
@Service("emailService")
@Transactional(rollbackFor = Exception.class)
class EmailService {

    private Email createBase(Email email, String userName) {
        if (email != null && !email.hasErrors()) {
            if (!Email.exists(email.getId())) {
                email.dateCreated = new Date()
                email.createdBy = userName
            }
            email.lastUpdated = new Date()
            email.modifyBy = userName
            return email.save(flush: true, failOnError: true)
        }
        return null
    }

    Email bootStrap(Email email) {
        return createBase(email, Constants.ROOT)
    }

    Email create(Email email, LoginManager loginManager) {
        return createBase(email, loginManager.getUsername())
    }

    List<Email> list(boolean enabled, int start, int size) {
        return Email.withCriteria {
            firstResult(start)
            maxResults(size)
            order("id", "desc")
            eq "enabled", enabled
        }.findAll()
    }

    int count(boolean enabled) {
        return Email.countByEnabled(enabled)
    }

    Email refresh(Long id) {
        return Email.findById(id)
    }
}
