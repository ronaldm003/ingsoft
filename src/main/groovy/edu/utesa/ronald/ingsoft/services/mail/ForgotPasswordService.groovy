package edu.utesa.ronald.ingsoft.services.mail

import edu.utesa.ronald.ingsoft.domains.mail.ForgotPassword
import edu.utesa.ronald.ingsoft.models.LoginManager
import edu.utesa.ronald.ingsoft.utils.Constants
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional

/**
 *  Created by ronald on 07/24/17.
 */
@SuppressWarnings("GrMethodMayBeStatic")
@Service("forgotPasswordService")
@Transactional(rollbackFor = Exception.class)
class ForgotPasswordService {

    private ForgotPassword createBase(ForgotPassword forgotPassword, String userName) {
        if (forgotPassword != null && !forgotPassword.hasErrors()) {
            if (!ForgotPassword.exists(forgotPassword.getId())) {
                forgotPassword.dateCreated = new Date()
                forgotPassword.createdBy = userName
            }
            forgotPassword.lastUpdated = new Date()
            forgotPassword.modifyBy = userName
            return forgotPassword.save(flush: true, failOnError: true)
        }
        return null
    }

    ForgotPassword bootStrap(ForgotPassword forgotPassword) {
        return createBase(forgotPassword, Constants.ROOT)
    }

    ForgotPassword create(ForgotPassword forgotPassword, LoginManager loginManager) {
        return createBase(forgotPassword, loginManager.getUsername())
    }

    List<ForgotPassword> list(boolean enabled, int start, int size) {
        return ForgotPassword.withCriteria {
            firstResult(start)
            maxResults(size)
            order("id", "desc")
            eq "enabled", enabled
        }.findAll()
    }

    int count(boolean enabled) {
        return ForgotPassword.countByEnabled(enabled)
    }

    void delete(ForgotPassword forgotPassword) {
        forgotPassword = ForgotPassword.findById(forgotPassword.id)
        forgotPassword.enabled = false
        bootStrap(forgotPassword)
    }

    void delete(List<ForgotPassword> forgotPasswords) {
        for (ForgotPassword forgotPassword : forgotPasswords) {
            delete(forgotPassword)
        }
    }

    ForgotPassword byKeyLost(String keyLost) {
        return ForgotPassword.findByKeyLostAndEnabled(keyLost, true)
    }

    List<ForgotPassword> list() {
        return ForgotPassword.withCriteria {
            order("id", "desc")
            eq "enabled", true
        }.findAll()
    }

    ForgotPassword refresh(Long id) {
        return ForgotPassword.findById(id)
    }
}
