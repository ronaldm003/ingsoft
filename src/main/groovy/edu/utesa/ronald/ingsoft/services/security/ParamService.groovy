package edu.utesa.ronald.ingsoft.services.security

import edu.utesa.ronald.ingsoft.domains.security.Param
import edu.utesa.ronald.ingsoft.models.LoginManager
import edu.utesa.ronald.ingsoft.utils.Constants
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional

/**
 *  Created by Ronald on 16/11/17.
 */
@SuppressWarnings("GrMethodMayBeStatic")
@Service("paramService")
@Transactional(rollbackFor = Exception.class)
class ParamService {

    private Param createBase(Param param, String userName) {
        if (param != null && !param.hasErrors()) {
            if (!Param.exists(param.getId())) {
                param.dateCreated = new Date()
                param.createdBy = userName
            }
            param.lastUpdated = new Date()
            param.modifyBy = userName
            return param.save(flush: true, failOnError: true)
        }
        return null
    }

    Param bootStrap(Param param) {
        return createBase(param, Constants.ROOT)
    }

    Param create(Param param, LoginManager loginManager) {
        return createBase(param, loginManager.getUsername())
    }

    int count(boolean enabled) {
        return Param.countByChangeRootAndEnabled(false, enabled)
    }

    List<Param> list(boolean enabled, int start, int size) {
        return Param.withCriteria {
            firstResult(start)
            maxResults(size)
            order("id", "desc")
            eq "changeRoot", false
            eq "enabled", enabled
        }.findAll()
    }

    Param byCode(Integer code) {
        return Param.findByCodeAndEnabled(code, true)
    }

    boolean delete(Param param, LoginManager loginManager) {
        if (!param) {
            return false
        }
        param = Param.findById(param.id)
        param.setEnabled(!param.getEnabled())
        return create(param, loginManager)
    }

    Param refresh(Long id) {
        return Param.findById(id)
    }
}
