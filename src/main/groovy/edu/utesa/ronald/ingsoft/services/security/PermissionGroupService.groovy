package edu.utesa.ronald.ingsoft.services.security

import edu.utesa.ronald.ingsoft.domains.security.PermissionGroup
import edu.utesa.ronald.ingsoft.models.LoginManager
import edu.utesa.ronald.ingsoft.utils.Constants
import edu.utesa.ronald.ingsoft.utils.TouchableDomain
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional

import java.util.function.Consumer

/**
 *  Created by ronald on 9/08/16.
 */
@SuppressWarnings("GrMethodMayBeStatic")
@Service("permissionGroupService")
@Transactional(rollbackFor = Exception.class)
class PermissionGroupService {

    private PermissionGroup createBase(PermissionGroup permissionGroup, String userName) {
        if (permissionGroup != null && !permissionGroup.hasErrors()) {
            if (!PermissionGroup.exists(permissionGroup.getId())) {
                permissionGroup.dateCreated = new Date()
                permissionGroup.createdBy = userName
            }
            permissionGroup.lastUpdated = new Date()
            permissionGroup.modifyBy = userName
            return permissionGroup.save(flush: true, failOnError: true)
        }
        return null
    }

    PermissionGroup bootStrap(PermissionGroup permissionGroup) {
        return createBase(permissionGroup, Constants.ROOT)
    }

    PermissionGroup create(PermissionGroup permissionGroup, LoginManager loginManager) {
        return createBase(permissionGroup, loginManager.getUsername())
    }

    List<PermissionGroup> list(boolean enabled, int start, int size) {
        return PermissionGroup.withCriteria {
            firstResult(start)
            maxResults(size)
            order("id", "desc")
            eq "enabled", enabled
        }.findAll()
    }

    List<PermissionGroup> list(String name, int size, Consumer<PermissionGroup> touch = null) {
        List<PermissionGroup> groupList = PermissionGroup.withCriteria {
            if (size > 0) {
                maxResults(size)
            }
            order("id", "desc")
            like "name", "%" + name + "%"
        }.findAll()
        TouchableDomain.applyTouch(groupList, touch)
        return groupList
    }

    int count(boolean enabled) {
        return PermissionGroup.countByEnabled(enabled)
    }

    PermissionGroup byName(String name) {
        return PermissionGroup.findByName(name)
    }

    PermissionGroup refresh(Long id, Consumer<PermissionGroup> touch = null) {
        PermissionGroup permissionGroup = PermissionGroup.findById(id)
        TouchableDomain.applyTouch(permissionGroup, touch)
        return permissionGroup
    }

    boolean delete(PermissionGroup permissionGroup, LoginManager loginManager) {
        if (!permissionGroup) {
            return false
        }
        permissionGroup = PermissionGroup.findById(permissionGroup.id)
        permissionGroup.setEnabled(!permissionGroup.getEnabled())
        return create(permissionGroup, loginManager)
    }

}
