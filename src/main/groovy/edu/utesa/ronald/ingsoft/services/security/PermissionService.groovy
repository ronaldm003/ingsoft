package edu.utesa.ronald.ingsoft.services.security

import edu.utesa.ronald.ingsoft.domains.security.Permission
import edu.utesa.ronald.ingsoft.models.LoginManager
import edu.utesa.ronald.ingsoft.utils.Constants
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional

/**
 *  Created by ronald on 9/08/16.
 */
@SuppressWarnings("GrMethodMayBeStatic")
@Service("permissionService")
@Transactional(rollbackFor = Exception.class)
class PermissionService {

    private Permission createBase(Permission permission, String userName) {
        if (permission != null && !permission.hasErrors()) {
            if (!Permission.exists(permission.getId())) {
                permission.dateCreated = new Date()
                permission.createdBy = userName
            }
            permission.lastUpdated = new Date()
            permission.modifyBy = userName
            return permission.save(flush: true, failOnError: true)
        }
        return null
    }

    Permission bootStrap(Permission permission) {
        return createBase(permission, Constants.ROOT)
    }

    Permission create(Permission permission, LoginManager loginManager) {
        return createBase(permission, loginManager.getUsername())
    }

    List<Permission> list(boolean enabled, int start, int size) {
        return Permission.withCriteria {
            firstResult(start)
            maxResults(size)
            order("id", "desc")
            eq "enabled", enabled
        }.findAll()
    }

    int count(boolean enabled) {
        return Permission.countByEnabled(enabled)
    }

    Permission refresh(Long id) {
        Permission permission = Permission.findById(id)
        return permission
    }

    boolean delete(Permission permission, LoginManager loginManager) {
        if (!permission) {
            return false
        }
        permission = Permission.findById(permission.id)
        permission.setEnabled(!permission.getEnabled())
        return create(permission, loginManager)
    }

    List<Permission> list(String name, int size) {
        return Permission.withCriteria {
            if (size > 0) {
                maxResults(size)
            }
            order("id", "desc")
            like "name", "%" + name + "%"
        }.findAll()
    }

    List<Permission> list(int start, int size) {
        return list(true, start, size)
    }

    Permission byName(String name) {
        return Permission.findByName(name)
    }

    Permission byCode(Integer code) {
        return Permission.findByCodeAndEnabled(code, true)
    }
}
