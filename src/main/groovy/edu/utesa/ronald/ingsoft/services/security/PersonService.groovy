package edu.utesa.ronald.ingsoft.services.security

import edu.utesa.ronald.ingsoft.domains.location.Location
import edu.utesa.ronald.ingsoft.domains.security.Person
import edu.utesa.ronald.ingsoft.models.LoginManager
import edu.utesa.ronald.ingsoft.models.enums.Gender
import edu.utesa.ronald.ingsoft.utils.Constants
import edu.utesa.ronald.ingsoft.utils.TouchableDomain
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional

import java.util.function.Consumer

/**
 *  Created by Ronald on 16/11/17.
 */
@SuppressWarnings("GrMethodMayBeStatic")
@Service("personService")
@Transactional(rollbackFor = Exception.class)
class PersonService {

    private Person createBase(Person person, String userName) {
        if (person != null && !person.hasErrors()) {
            if (!Person.exists(person.getId())) {
                person.dateCreated = new Date()
                person.createdBy = userName
            }
            person.lastUpdated = new Date()
            person.modifyBy = userName
            return person.save(flush: true, failOnError: true)
        }
        return null
    }

    Person bootStrap(Person person) {
        return createBase(person, Constants.ROOT)
    }

    Person create(Person person, LoginManager loginManager) {
        return createBase(person, loginManager.getUsername())
    }

    List<Person> list(boolean enabled, int start, int size, Consumer<List<Person>> touch = null) {
        List<Person> personList = Person.withCriteria {
            firstResult(start)
            maxResults(size)
            order("id", "desc")
            eq "enabled", enabled
        }.findAll()
        TouchableDomain.applyTouch(personList, touch)
        return personList
    }

    List<Person> list(Long id, boolean enabled, int start, int size) {
        return Person.withCriteria {
            firstResult(start)
            maxResults(size)
            order("id", "desc")
            eq "id", id
            eq "enabled", enabled
        }.findAll()
    }

    List<Person> list(String name, int size) {
        return Person.withCriteria {
            if (size > 0) {
                maxResults(size)
            }
            order("id", "desc")
            like "name", "%" + name + "%"
            eq "enabled", true
        }.findAll()
    }

    List<Person> list(boolean enabled) {
        return Person.withCriteria {
            order("id", "desc")
            eq "enabled", enabled
        }.findAll()
    }

    List<Person> list(boolean enabled, boolean admin) {
        return Person.withCriteria {
            order("id", "desc")
            eq "admin", admin
            eq "enabled", enabled
        }.findAll()
    }

    int countByAdmin(boolean enabled, boolean admin) {
        return Person.withCriteria {
            order("id", "desc")
            eq "admin", admin
            eq "enabled", enabled
        }.findAll().size()
    }

    List<Person> listByUserAndLocation(Person person, Location location) {
        return Person.withCriteria {
            order("id", "desc")
            eq "location", location
            eq "enabled", true
        }.findAll()
    }

    List<Person> listByLocation(Location location) {
        return Person.withCriteria {
            order("id", "desc")
            eq "location", location
            eq "enabled", true
        }.findAll()
    }

    List<Person> listByUserAndUserRelated(Person person) {
        return Person.withCriteria {
            order("id", "desc")
            ge "listUserRelation", 2
            eq "enabled", true
        }.findAll()
    }

    List<Person> listByUserRelated() {
        return Person.withCriteria {
            order("id", "desc")
            ge "listUserRelation", 2
            eq "enabled", true
        }.findAll()
    }

    Person refresh(Long id, Consumer<Person> touch = null) {
        Person person = Person.findById(id)
        if (person.location) {
            person.location.enabled
        }
        if (person.occupation) {
            person.occupation.enabled
        }
        TouchableDomain.applyTouch(person, touch)
        return person
    }

    int count(boolean enabled) {
        return Person.countByEnabled(enabled)
    }

    int count(Long id, boolean enabled) {
        return Person.countByIdAndEnabled(id, enabled)
    }

    int countByGenre(Gender genderEnum) {
        return Person.countByGenderAndEnabled(genderEnum, true)
    }

    Person delete(Person person, LoginManager loginManager) {
        if (!person) {
            return null
        }
        person = Person.findById(person.id)
        person.enabled = !person.enabled
        return create(person, loginManager)
    }

    Boolean compareByUsername(String username) {
        Person person = Person.findByUsernameAndEnabled(username, true)
        if (person) {
            return true
        } else {
            return false
        }
    }

    Person byName(String name, Consumer<Person> touch = null) {
        Person person = Person.findByNameAndEnabled(name, true)
        TouchableDomain.applyTouch(person, touch)
        return person
    }

    Person byEmail(String email) {
        return Person.findByEmailAndEnabled(email, true)
    }

    Person byId(Long id, Consumer<Person> touch = null) {
        Person person = Person.findByIdAndEnabled(id, true)
        TouchableDomain.applyTouch(person, touch)
        return person
    }

    Long findMaxCode() {
        Person person = Person.withCriteria {
            order("code", "desc")
        }.find() as Person
        if (!person) {
            return 0
        }
        return person.code
    }


}
