package edu.utesa.ronald.ingsoft.services.security

import edu.utesa.ronald.ingsoft.domains.security.User
import edu.utesa.ronald.ingsoft.domains.security.UserKeys
import edu.utesa.ronald.ingsoft.models.LoginManager
import edu.utesa.ronald.ingsoft.utils.Constants
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional

/**
 *  Created by Ronald on 16/11/17.
 */
@SuppressWarnings("GrMethodMayBeStatic")
@Service("userKeysService")
@Transactional(rollbackFor = Exception.class)
class UserKeysService {

    private UserKeys createBase(UserKeys userKeys, String userName) {
        if (userKeys != null && !userKeys.hasErrors()) {
            if (!UserKeys.exists(userKeys.getId())) {
                userKeys.dateCreated = new Date()
                userKeys.createdBy = userName
            }
            userKeys.lastUpdated = new Date()
            userKeys.modifyBy = userName
            return userKeys.save(flush: true, failOnError: true)
        }
        return null
    }

    UserKeys bootStrap(UserKeys userKeys) {
        return createBase(userKeys, Constants.ROOT)
    }

    UserKeys create(UserKeys userKeys, LoginManager loginManager) {
        return createBase(userKeys, loginManager.getUsername())
    }

    List<UserKeys> list(boolean enabled, int start, int size) {
        return UserKeys.withCriteria {
            firstResult(start)
            maxResults(size)
            order("id", "desc")
            eq "enabled", enabled
        }.findAll()
    }

    List<UserKeys> list(boolean enabled) {
        return UserKeys.withCriteria {
            order("id", "desc")
            eq "enabled", enabled
        }.findAll()
    }

    int count(boolean enabled) {
        return UserKeys.countByEnabled(enabled)
    }

    int countByUser(User user) {
        return UserKeys.countByUser(user)
    }

    UserKeys refresh(Long id) {
        UserKeys userKeys = UserKeys.findById(id)
        if (userKeys.role) {
            userKeys.role.enabled
        }
        if (userKeys.user) {
            userKeys.user.enabled
        }
        if (userKeys.secUser) {
            userKeys.secUser.enabled
        }
        return userKeys
    }

    UserKeys delete(UserKeys userKeys, LoginManager loginManager) {
        if (!userKeys) {
            return null
        }
        userKeys = UserKeys.findById(userKeys.id)
        userKeys.enabled = !userKeys.enabled
        return create(userKeys, loginManager)
    }

    UserKeys byId(Long id) {
        return UserKeys.findByIdAndEnabled(id, true)
    }

    UserKeys byAuthorization(String key) {
        return UserKeys.findByAuthorizationKeyAndEnabled(key, true)
    }

    Long findMaxCode() {
        UserKeys userKeys = UserKeys.withCriteria {
            order("id", "desc")
        }.find() as UserKeys
        if (!userKeys) {
            return 0
        }
        return userKeys.code
    }

}
