package edu.utesa.ronald.ingsoft.services.security

import edu.utesa.ronald.ingsoft.domains.location.Location
import edu.utesa.ronald.ingsoft.domains.security.User
import edu.utesa.ronald.ingsoft.models.LoginManager
import edu.utesa.ronald.ingsoft.models.enums.Gender
import edu.utesa.ronald.ingsoft.utils.Constants
import edu.utesa.ronald.ingsoft.utils.TouchableDomain
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional

import java.util.function.Consumer

/**
 *  Created by Ronald on 16/11/17.
 */
@SuppressWarnings("GrMethodMayBeStatic")
@Service("userService")
@Transactional(rollbackFor = Exception.class)
class UserService {

    private User createBase(User user, String userName) {
        if (user != null && !user.hasErrors()) {
            if (!User.exists(user.getId())) {
                user.dateCreated = new Date()
                user.createdBy = userName
            }
            user.lastUpdated = new Date()
            user.modifyBy = userName
            return user.save(flush: true, failOnError: true)
        }
        return null
    }

    User bootStrap(User user) {
        return createBase(user, Constants.ROOT)
    }

    User create(User user, LoginManager loginManager) {
        return createBase(user, loginManager.getUsername())
    }

    List<User> list(boolean enabled, int start, int size, Consumer<User> touch = null) {
        List<User> userList = User.withCriteria {
            firstResult(start)
            maxResults(size)
            order("id", "desc")
            eq "enabled", enabled
        }.findAll()
        TouchableDomain.applyTouch(userList, touch)
        return userList
    }

    List<User> list(boolean enabled, boolean admin, int start, int size) {
        return User.withCriteria {
            firstResult(start)
            maxResults(size)
            order("id", "desc")
            eq "enabled", enabled
            eq "admin", admin
        }.findAll()
    }

    List<User> list(Long id, boolean enabled, int start, int size, Consumer<User> touch = null) {
        List<User> userList = User.withCriteria {
            firstResult(start)
            maxResults(size)
            order("id", "desc")
            eq "id", id
            eq "enabled", enabled
        }.findAll()
        TouchableDomain.applyTouch(userList, touch)
        return userList
    }

    List<User> list(String name, int size) {
        return User.withCriteria {
            if (size > 0) {
                maxResults(size)
            }
            order("id", "desc")
            like "name", "%" + name + "%"
            eq "enabled", true
        }.findAll()
    }

    List<User> list(boolean enabled, Consumer<User> touch = null) {
        List<User> userList = User.withCriteria {
            order("id", "desc")
            eq "enabled", enabled
        }.findAll()
        TouchableDomain.applyTouch(userList, touch)
        return userList
    }

    List<User> list(boolean enabled, boolean admin) {
        return User.withCriteria {
            order("id", "desc")
            eq "admin", admin
            eq "enabled", enabled
        }.findAll()
    }

    int countByAdmin(boolean enabled, boolean admin) {
        return User.withCriteria {
            order("id", "desc")
            eq "admin", admin
            eq "enabled", enabled
        }.findAll().size()
    }

    List<User> listByUserAndLocation(User user, Location location, Consumer<User> touch = null) {
        List<User> userList = User.withCriteria {
            order("id", "desc")
            if (user.person) {
                person {
                    eq "location", location
                }
            }
            eq "enabled", true
        }.findAll()
        TouchableDomain.applyTouch(userList, touch)
        return userList
    }

    List<User> listByLocation(Location location, Consumer<User> touch = null) {
        List<User> userList = User.withCriteria {
            order("id", "desc")
            person {
                eq "location", location
            }
            eq "enabled", true
        }.findAll()
        TouchableDomain.applyTouch(userList, touch)
        return userList
    }

    List<User> listByUserAndUserRelated(User user) {
        return User.withCriteria {
            order("id", "desc")
            ge "listUserRelation", 2
            eq "enabled", true
        }.findAll()
    }

    List<User> listByUserRelated() {
        return User.withCriteria {
            order("id", "desc")
            ge "listUserRelation", 2
            eq "enabled", true
        }.findAll()
    }

    User refresh(Long id) {
        return refreshIt(id, {
            it.person.enabled; it.listPermission.size(); it.person.location?.enabled; it.person.educationLevel?.enabled;
            it.person.occupation?.enabled
        })
    }

    User refreshIt(Long id, Consumer<User> touch = null) {
        User user = User.findById(id)
        TouchableDomain.applyTouch(user, touch)
        return user
    }

    int count(boolean enabled) {
        return User.countByEnabled(enabled)
    }

    int count(Long id, boolean enabled) {
        return User.countByIdAndEnabled(id, enabled)
    }

    int countByGenre(Gender genderEnum) {
        return User.withCriteria {
            order("id", "desc")
            person {
                eq "gender", genderEnum
            }
            eq "enabled", true
        }.findAll().size()
    }

    int count(boolean enabled, boolean admin) {
        return User.countByEnabledAndAdmin(enabled, admin)
    }

    User delete(User user, LoginManager loginManager) {
        if (!user) {
            return null
        }
        user = User.findById(user.id)
        user.enabled = !user.enabled
        return create(user, loginManager)
    }

    User getRootUser() {
        return User.findByUsernameAndEnabled(Constants.ROOT_USERNAME, true)
    }

    User byUsername(String username) {
        return User.findByUsernameAndEnabled(username, true)
    }

    String validateUsername(String username) {
        return byUsername(username) ? byUsername(username).getUsername() : ""
    }

    Boolean compareByUsername(String username) {
        User user = User.findByUsernameAndEnabled(username, true)
        if (user) {
            return true
        } else {
            return false
        }
    }

    User byName(String name) {
        return User.findByNameAndEnabled(name, true)
    }

    User byEmail(String email) {
        return User.findByEmailsAndEnabled(email, true)
    }

    User byId(Long id) {
        return User.findByIdAndEnabled(id, true)
    }

    User changePassword(User user, LoginManager loginManager) {
        return create(user, loginManager)
    }

    Long findMaxCode() {
        User user = User.withCriteria {
            order("id", "desc")
        }.find() as User
        if (!user) {
            return 0
        }
        return user.code
    }


}
