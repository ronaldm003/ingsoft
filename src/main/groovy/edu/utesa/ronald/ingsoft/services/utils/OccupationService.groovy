package edu.utesa.ronald.ingsoft.services.utils

import edu.utesa.ronald.ingsoft.domains.utils.Occupation
import edu.utesa.ronald.ingsoft.models.LoginManager
import edu.utesa.ronald.ingsoft.utils.Constants
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional

/**
 *  Created by Ronald on 16/11/17.
 */
@SuppressWarnings("GrMethodMayBeStatic")
@Service("occupationService")
@Transactional(rollbackFor = Exception.class)
class OccupationService {

    private Occupation createBase(Occupation occupation, String userName) {
        if (occupation != null && !occupation.hasErrors()) {
            if (!Occupation.exists(occupation.getId())) {
                occupation.dateCreated = new Date()
                occupation.createdBy = userName
            }
            occupation.lastUpdated = new Date()
            occupation.modifyBy = userName
            return occupation.save(flush: true, failOnError: true)
        }
        return null
    }

    Occupation bootStrap(Occupation occupation) {
        return createBase(occupation, Constants.ROOT)
    }

    Occupation create(Occupation occupation, LoginManager loginManager) {
        return createBase(occupation, loginManager.getUsername())
    }

    List<Occupation> list(boolean enabled, int start, int size) {
        return Occupation.withCriteria {
            firstResult(start)
            maxResults(size)
            order("id", "desc")
            eq "enabled", enabled
        }.findAll()
    }

    List<Occupation> list(boolean enabled, boolean admin, int start, int size) {
        return Occupation.withCriteria {
            firstResult(start)
            maxResults(size)
            order("id", "desc")
            eq "enabled", enabled
            eq "admin", admin
        }.findAll()
    }

    List<Occupation> list(String name, int size) {
        return Occupation.withCriteria {
            if (size > 0) {
                maxResults(size)
            }
            order("id", "desc")
            like "name", "%" + name + "%"
            eq "enabled", true
        }.findAll()
    }

    List<Occupation> list(boolean enabled) {
        return Occupation.withCriteria {
            order("id", "desc")
            eq "enabled", enabled
        }.findAll()
    }

    List<Occupation> list(boolean enabled, boolean admin) {
        return Occupation.withCriteria {
            order("id", "desc")
            eq "admin", admin
            eq "enabled", enabled
        }.findAll()
    }

    Occupation refresh(Long id) {
        Occupation occupation = Occupation.findById(id)
        return occupation
    }

    int count(boolean enabled) {
        return Occupation.countByEnabled(enabled)
    }

    Occupation delete(Occupation occupation, LoginManager loginManager) {
        if (!occupation) {
            return null
        }
        occupation = Occupation.findById(occupation.id)
        occupation.enabled = !occupation.enabled
        return create(occupation, loginManager)
    }

    Occupation byId(Long id) {
        return Occupation.findByIdAndEnabled(id, true)
    }

    Occupation byDescription(String description) {
        return Occupation.findByDescription(description)
    }

    Occupation changePassword(Occupation occupation, LoginManager loginManager) {
        return create(occupation, loginManager)
    }

    Long findMaxCode() {
        Occupation occupation = Occupation.withCriteria {
            order("id", "desc")
        }.find() as Occupation
        if (!occupation) {
            return 0
        }
        return occupation.code
    }

}
