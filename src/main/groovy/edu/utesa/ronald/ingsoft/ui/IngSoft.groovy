package edu.utesa.ronald.ingsoft.ui

import com.kbdunn.vaadin.addons.fontawesome.FontAwesome
import com.vaadin.addon.contextmenu.ContextMenu
import com.vaadin.addon.contextmenu.Menu
import com.vaadin.addon.contextmenu.MenuItem
import com.vaadin.ui.Button
import com.vaadin.ui.themes.ValoTheme
import edu.utesa.ronald.ingsoft.components.abstracts.APanel
import edu.utesa.ronald.ingsoft.models.constants.Styles
import edu.utesa.ronald.ingsoft.ui.components.ContentPanel
import edu.utesa.ronald.ingsoft.ui.components.MenuHeaderPanel
import edu.utesa.ronald.ingsoft.ui.components.MenuPanel
import edu.utesa.ronald.ingsoft.ui.components.TopBar
import edu.utesa.ronald.ingsoft.ui.utils.dialogs.ChangeLanguage
import edu.utesa.ronald.ingsoft.utils.Languages

/**
 * pantalla principal
 *
 *  Created by ronald on 2/26/17.
 */
class IngSoft extends APanel {

    private IngSoftDesign ingSoftDesign = new IngSoftDesign()

    private ContextMenu contextMenu
    private ContentPanel contentPanel
    private MenuHeaderPanel menuHeaderPanel
    private MenuPanel menuPanel
    private TopBar topBar

    IngSoft() {
        setSizeFull()
        confLayout(ingSoftDesign)
    }

    @Override
    protected void confLanguage() {
        ingSoftDesign.pTopBar.addStyleName(ValoTheme.PANEL_BORDERLESS)
        ingSoftDesign.pMenu.addStyleName(ValoTheme.PANEL_BORDERLESS)
        ingSoftDesign.pMenu.addStyleName(ValoTheme.PANEL_SCROLL_INDICATOR)
        ingSoftDesign.pContent.addStyleName(ValoTheme.PANEL_BORDERLESS)
        ingSoftDesign.addStyleName(Styles.APP_CONTROL)
    }

    @Override
    protected void buildLayout() {
        contentPanel = new ContentPanel()
        menuPanel = new MenuPanel(contentPanel)
        menuHeaderPanel = new MenuHeaderPanel(menuPanel, contentPanel)
        topBar = new TopBar(contentPanel)
        ingSoftDesign.pTopBar.setContent(topBar)
        ingSoftDesign.pMenuHeader.setContent(menuHeaderPanel)
        ingSoftDesign.pMenu.setContent(menuPanel)
        ingSoftDesign.pContent.setContent(contentPanel)
        topBar.btnMenu().addClickListener(new Button.ClickListener() {
            @Override
            void buttonClick(Button.ClickEvent event) {
                ingSoftDesign.vlMenu.setVisible(!ingSoftDesign.vlMenu.isVisible())
            }
        })
        contextMenu = new ContextMenu(this, true)
        contextMenu.addItem(Languages.instance.getText("menu.profile.button.language"), FontAwesome.LANGUAGE, new Menu.Command() {
            @Override
            void menuSelected(MenuItem selectedItem) {
                ChangeLanguage changeLanguage = new ChangeLanguage()
                getUI().addWindow(changeLanguage)
            }
        })

    }

    @Override
    protected void security() {
    }
}
