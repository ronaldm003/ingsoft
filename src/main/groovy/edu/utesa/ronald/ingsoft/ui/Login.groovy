package edu.utesa.ronald.ingsoft.ui

import com.kbdunn.vaadin.addons.fontawesome.FontAwesome
import com.vaadin.event.ShortcutAction
import com.vaadin.server.VaadinSession
import com.vaadin.ui.Button
import com.vaadin.ui.themes.ValoTheme
import edu.utesa.ronald.ingsoft.components.abstracts.AWindow
import edu.utesa.ronald.ingsoft.models.LoginManager
import edu.utesa.ronald.ingsoft.models.constants.Styles
import edu.utesa.ronald.ingsoft.models.interfaces.CallbackUser
import edu.utesa.ronald.ingsoft.services.SecurityService
import edu.utesa.ronald.ingsoft.services.security.ParamService
import edu.utesa.ronald.ingsoft.utils.Constants
import edu.utesa.ronald.ingsoft.utils.Languages
import edu.utesa.ronald.ingsoft.utils.ShowNotification
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value

/**
 * pantalla de login
 *  Created by ronald on 2/25/17.
 */
class Login extends AWindow {

    @Value('${info.app.name}')
    private String APP_NAME

    @Value('${info.app.version}')
    private String APP_VERSION

    @Autowired
    private SecurityService securityService

    @Autowired
    private ParamService paramService

    private LoginDesign loginDesign = new LoginDesign()

    private CallbackUser callbackUser

    Login(CallbackUser callbackUser) {
        this.callbackUser = callbackUser
        Constants.instance.autoWiredClass(this)
        confLayout(loginDesign)
    }

    @Override
    protected void prepare() {
        center()
        setClosable(false)
        setResizable(false)
    }

    @Override
    protected void confLanguage() {
        loginDesign.btnForgotPassword.setCaption(Languages.instance.getText("window.login.forgotpassword"))
        loginDesign.btnLogin.setCaption(Languages.instance.getText("window.login.login"))
        loginDesign.tfUser.setIcon(FontAwesome.USER)
        loginDesign.tfUser.addStyleName(ValoTheme.TEXTFIELD_INLINE_ICON)
        loginDesign.pfPassword.setIcon(FontAwesome.CHAIN)
        loginDesign.pfPassword.addStyleName(ValoTheme.TEXTFIELD_INLINE_ICON)
        loginDesign.lblAppName.addStyleName(Styles.LABEL_LOGIN_APP_NAME)
        loginDesign.lblAppName.addStyleName(ValoTheme.LABEL_BOLD)
        loginDesign.btnLogin.addStyleName(Styles.LOGIN_BUTTON)
        loginDesign.addStyleName(Styles.LOGIN_WINDOW)
    }

    @Override
    protected void buildLayout() {
        loginDesign.lblAppName.setValue(APP_NAME)
        loginDesign.tfUser.focus()
        loginDesign.btnLogin.setClickShortcut(ShortcutAction.KeyCode.ENTER)
        loginDesign.btnLogin.addClickListener(new Button.ClickListener() {
            @Override
            void buttonClick(Button.ClickEvent event) {
                if (loginDesign.pfPassword.getValue() == "") {
                    loginDesign.pfPassword.focus()
                    return
                }
                loginDesign.btnLogin.setEnabled(false)
                if (loginDesign.tfUser.getValue() != "" && loginDesign.pfPassword.getValue() != "") {
                    LoginManager loginManager = securityService.authenticate(loginDesign.tfUser.getValue(), loginDesign.pfPassword.getValue())
                    if (loginManager) {
                        VaadinSession.current.session.setAttribute(Constants.USER_LOGIN, loginManager)
                        callbackUser.onDialogResult()
                    } else {
                        ShowNotification.error()
                        loginDesign.tfUser.setValue("")
                        loginDesign.pfPassword.setValue("")
                    }
                }
                loginDesign.btnLogin.setEnabled(true)
            }
        })
    }

    @Override
    protected void security() {
    }
}
