package edu.utesa.ronald.ingsoft.ui

import com.vaadin.data.Property
import com.vaadin.event.ShortcutAction
import com.vaadin.server.Page
import com.vaadin.server.Sizeable
import com.vaadin.ui.Button
import edu.utesa.ronald.ingsoft.components.abstracts.AWindow
import edu.utesa.ronald.ingsoft.domains.security.User
import edu.utesa.ronald.ingsoft.services.SecurityService
import edu.utesa.ronald.ingsoft.services.security.UserService
import edu.utesa.ronald.ingsoft.utils.Constants
import edu.utesa.ronald.ingsoft.utils.Languages
import edu.utesa.ronald.ingsoft.utils.ShowNotification
import edu.utesa.ronald.ingsoft.utils.configurations.VaadinConfiguration
import edu.utesa.ronald.ingsoft.utils.security.HashUtils
import org.springframework.beans.factory.annotation.Autowired

/**
 * pantalla de sing up
 *  Created by ronald on 2/25/17.
 */
class SignUp extends AWindow {

    @Autowired
    private SecurityService securityService
    @Autowired
    private UserService userService

    private SignUpDesign signUpDesign = new SignUpDesign()

    private User userSave

    SignUp() {
        Constants.instance.autoWiredClass(this)
        userSave = new User()
        confLayout(signUpDesign)
    }

    @Override
    protected void prepare() {
        center()
        setWidth(400, Sizeable.Unit.PIXELS)
        setHeightUndefined()
        setClosable(false)
        setResizable(false)
    }

    @Override
    protected void confLanguage() {
        setCaption(Languages.instance.getText("window.signup.message"))
        signUpDesign.tfName.setCaption(Languages.instance.getText("window.name"))
        signUpDesign.tfUsername.setCaption(Languages.instance.getText("window.username"))
        signUpDesign.pfPassword.setCaption(Languages.instance.getText("window.password"))
        signUpDesign.cbUserType.setCaption(Languages.instance.getText("window.user.type"))
        signUpDesign.btnOK.setCaption(Languages.instance.getText("window.signup"))
    }

    @Override
    protected void buildLayout() {
        signUpDesign.tfName.focus()
        signUpDesign.btnOK.setClickShortcut(ShortcutAction.KeyCode.ENTER)
        signUpDesign.cbUserType.setRequired(true)
        signUpDesign.btnOK.addClickListener(new Button.ClickListener() {
            @Override
            void buttonClick(Button.ClickEvent event) {
                if (signUpDesign.pfPassword.getValue() == "") {
                    signUpDesign.pfPassword.focus()
                    return
                }
                if (onSave()) {
                    ShowNotification.success()
                    Page.getCurrent().setLocation(VaadinConfiguration.LOGIN)
                } else {
                    ShowNotification.error("notification.error.user.created")
                }
            }

        })
        fillUserType()
        signUpDesign.cbUserType.addValueChangeListener(new Property.ValueChangeListener() {
            @Override
            void valueChange(Property.ValueChangeEvent event) {
                if (signUpDesign.cbUserType.getValue().toString()) {
                    categorysSelect = categoryService.byId(signUpDesign.cbUserType.getValue() as Long)
                } else {
                    categorysSelect = null
                }
            }
        })
    }

    protected boolean getElementsValid() {
        if (!signUpDesign.tfName.isValid()) {
            signUpDesign.tfCode.focus()
            return false
        } else if (!signUpDesign.tfUsername.isValid()) {
            signUpDesign.tfUsername.focus()
            return false
        } else if (!signUpDesign.pfPassword.isValid()) {
            signUpDesign.pfPassword.focus()
            return false
        } else if (!signUpDesign.cbUserType.isValid()) {
            signUpDesign.cbUserType.focus()
            return false
        }
        return true
    }

    boolean onSave() {
        if (getElementsValid()) {
            userSave.code = userService.findMaxCode() + 1
            userSave.name = signUpDesign.tfName.getValue()
            userSave.username = signUpDesign.tfUsername.getValue()
            userSave.password = HashUtils.encodeSHA256(signUpDesign.pfPassword.getValue())
            if (categorysSelect) {
                userSave.categorys = categorysSelect
            } else {
                userSave.categorys = categoryService.byDescription("Generico")
            }
            if (userService.compareByUsername(signUpDesign.tfUsername.getValue())) {
                signUpDesign.tfUsername.focus()
                ShowNotification.error()
                return false
            } else {
                userService.bootStrap(userSave)
                return true
            }
        }
        return false
    }

    @Override
    protected void security() {
    }

}
