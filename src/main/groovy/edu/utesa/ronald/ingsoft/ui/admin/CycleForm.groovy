package edu.utesa.ronald.ingsoft.ui.admin

import com.vaadin.server.Sizeable
import edu.utesa.ronald.ingsoft.domains.admin.Cycle
import edu.utesa.ronald.ingsoft.services.admin.CycleService
import edu.utesa.ronald.ingsoft.ui.components.WindowForm
import edu.utesa.ronald.ingsoft.utils.Constants
import edu.utesa.ronald.ingsoft.utils.Languages
import edu.utesa.ronald.ingsoft.utils.WindowsCaption
import groovy.transform.CompileStatic
import org.springframework.beans.factory.annotation.Autowired

/**
 * Aqui se realizan todos los casos de los botones de accion.
 *  Created by ronald on 3/07/17.
 */
@CompileStatic
class CycleForm extends WindowForm<Cycle> {

    @Autowired
    private CycleService cycleService

    private CycleFormCycle formCycle
//    private CycleFormTabEquipments formTabEquipments

    CycleForm() {
        this(null, null)
    }

    CycleForm(Cycle location, Boolean edit) {
        super(location, edit)
        confLayout(WindowsCaption.CYCLE_CAPTION)
        setWidth(600, Sizeable.Unit.PIXELS)
    }

    @Override
    protected void prepare() {
        super.prepare()
        Constants.instance.autoWiredClass(this)
    }

    @Override
    protected void onCreate() {
        formCycle = new CycleFormCycle()
//        formTabEquipments = new CycleFormTabEquipments()
    }

    @Override
    protected void onEditView() {
        itemSave = cycleService.refresh(itemSave.id, { it.listSubjects.size() })
        formCycle = new CycleFormCycle(itemSave, edit)
//        formTabEquipments = new CycleFormTabEquipments(itemSave.listEquipments.toList(), edit)
    }

    @Override
    Cycle onSave() {
        itemSave = formCycle.getValid()
//        itemSave.listEquipments = formTabEquipments.getValid().toSet()
        return cycleService.create(itemSave, loginManager)
    }

    @Override
    protected void onViewTabs() {

    }

    @Override
    protected void buildView() {

    }

    @Override
    protected boolean getValidForm() {
        if (!formCycle.getValid()) {
            return tabSheetManager.selectTab(0)
        }
//        else if (!formTabEquipments.getValid()) {
//            tabSheetManager.selectTab(1)
//            return false
//        }
        return true
    }

    @Override
    protected void buildLayout() {
        setWidth(800, Sizeable.Unit.PIXELS)
        tabSheetManager.addTabPanel(Languages.instance.getText(WindowsCaption.CYCLE_CAPTION + FORM + "cycle"), formCycle)
//        tabSheetManager.addTabPanel(Languages.instance.getText(WindowsCaption.CLINIC_CAPTION + FORM + "equipment"), formTabEquipments)

    }

    @Override
    protected void security() {

    }
}
