package edu.utesa.ronald.ingsoft.ui.admin

import com.vaadin.event.FieldEvents
import edu.utesa.ronald.ingsoft.components.abstracts.AWindowTab
import edu.utesa.ronald.ingsoft.domains.admin.Cycle
import edu.utesa.ronald.ingsoft.services.admin.CycleService
import edu.utesa.ronald.ingsoft.utils.Constants
import edu.utesa.ronald.ingsoft.utils.Languages
import groovy.transform.CompileStatic
import org.springframework.beans.factory.annotation.Autowired

/**
 *  Created by ronald on 3/07/17.
 */
@CompileStatic
class CycleFormCycle extends AWindowTab<Cycle> {

    @Autowired
    private CycleService cycleService

    private CycleFormCycleDesign cycleFormCycleDesign = new CycleFormCycleDesign()

    private Calendar calendar = Calendar.getInstance()

    CycleFormCycle() {
        this(new Cycle(), null)
    }

    CycleFormCycle(Cycle clinic, Boolean edit) {
        super(clinic, edit)
        confLayout(cycleFormCycleDesign)
    }

    @Override
    protected void buildLayout() {
        calendar.add(GregorianCalendar.MONTH, 1)
        cycleFormCycleDesign.dfStart.setValue(new Date())
        cycleFormCycleDesign.dfEnd.setValue(calendar.getTime())
        cycleFormCycleDesign.tfDescription.setRequired(true)
        cycleFormCycleDesign.tfDescription.setRequiredError(Languages.instance.getText("validations.empty.field"))
        cycleFormCycleDesign.tfDescription.setValidationVisible(true)
        cycleFormCycleDesign.tfDescription.setNullRepresentation("")
        cycleFormCycleDesign.tfDescription.addTextChangeListener(new FieldEvents.TextChangeListener() {
            @Override
            void textChange(FieldEvents.TextChangeEvent event) {
                cycleFormCycleDesign.tfDescription.setValue(event.getText())
                cycleFormCycleDesign.tfDescription.setCursorPosition(event.getCursorPosition())
                cycleFormCycleDesign.tfDescription.validate()
            }
        })
    }

    @Override
    protected void restore() {
        cycleFormCycleDesign.tfCode.setReadOnly(false)
        cycleFormCycleDesign.tfCode.setValue(itemSave.code.toString())
        cycleFormCycleDesign.tfCode.setReadOnly(true)
        cycleFormCycleDesign.dfStart.setValue(itemSave.startDate ?: new Date())
        cycleFormCycleDesign.dfEnd.setValue(itemSave.endDate ?: calendar.getTime())
        cycleFormCycleDesign.tfDescription.setValue(itemSave.description ?: "")
    }

    @Override
    protected void visualize() {
        cycleFormCycleDesign.tfCode.setReadOnly(true)
        cycleFormCycleDesign.dfStart.setReadOnly(true)
        cycleFormCycleDesign.dfEnd.setReadOnly(true)
        cycleFormCycleDesign.tfDescription.setReadOnly(true)
    }

    @Override
    protected boolean getElementsValid() {
        if (!cycleFormCycleDesign.tfCode.isValid()) {
            cycleFormCycleDesign.tfCode.focus()
            return false
        } else if (!cycleFormCycleDesign.tfDescription.isValid()) {
            cycleFormCycleDesign.tfDescription.focus()
            return false
        } else if (!cycleFormCycleDesign.dfStart.isValid()) {
            cycleFormCycleDesign.dfStart.focus()
            return false
        } else if (!cycleFormCycleDesign.dfEnd.isValid()) {
            cycleFormCycleDesign.dfEnd.focus()
            return false
        }
        return true
    }

    @Override
    protected void collectData() {
        itemSave.code = cycleFormCycleDesign.tfCode.getValue()
        itemSave.startDate = cycleFormCycleDesign.dfStart.getValue()
        itemSave.endDate = cycleFormCycleDesign.dfEnd.getValue()
        itemSave.description = cycleFormCycleDesign.tfDescription.getValue()
    }

    @Override
    protected void confLanguage() {
        Constants.instance.autoWiredClass(this)
        cycleFormCycleDesign.tfCode.setCaption(Languages.instance.getText("window.id"))
        cycleFormCycleDesign.dfStart.setCaption(Languages.instance.getText("window.start.date"))
        cycleFormCycleDesign.dfEnd.setCaption(Languages.instance.getText("window.end.date"))
        cycleFormCycleDesign.tfDescription.setCaption(Languages.instance.getText("window.description"))
    }

    @Override
    protected Cycle getValid() {
        if (getElementsValid()) {
            collectData()
            return itemSave
        }
        return null
    }

    @Override
    void startFocus() {
        cycleFormCycleDesign.tfDescription.focus()
    }
}
