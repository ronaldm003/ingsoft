package edu.utesa.ronald.ingsoft.ui.admin

import edu.utesa.ronald.ingsoft.domains.admin.Cycle
import edu.utesa.ronald.ingsoft.models.containers.admin.CycleContainer
import edu.utesa.ronald.ingsoft.services.admin.CycleService
import edu.utesa.ronald.ingsoft.ui.admin.CycleForm
import edu.utesa.ronald.ingsoft.ui.components.ATabBase
import edu.utesa.ronald.ingsoft.utils.Constants
import edu.utesa.ronald.ingsoft.utils.container.LazyQuery
import groovy.transform.CompileStatic
import org.springframework.beans.factory.annotation.Autowired

/**
 * Aqui se lista todos los tipos de usuario
 *  Created by ronald on 3/07/17.
 */
@CompileStatic
class CycleTab extends ATabBase<CycleForm, Cycle> {

    @Autowired
    private CycleService cycleService

    CycleTab() {
        Constants.instance.autoWiredClass(this)
    }

    @Override
    protected void confLanguage() {

    }

    @Override
    protected void buildLayout() {
        manageGrid(new CycleContainer(new LazyQuery<Cycle>() {
            @Override
            int getSize() {
                return cycleService.count(showDelete())
            }

            @Override
            List<Cycle> getItemsIds(int startIndex, int numberOfIds) {
                return cycleService.list(showDelete(), startIndex, numberOfIds)
            }

            @Override
            int getFilteredSize() {
                return 0
            }

            @Override
            List<Cycle> getFilteredItemsIds(int startIndex, int numberOfIds) {
                return null
            }
        }))
    }

    @Override
    void refreshItem() {

    }

    @Override
    CycleForm onNew() {
        return new CycleForm()
    }

    @Override
    CycleForm onEdit() {
        return new CycleForm(itemSelect, true)
    }

    @Override
    CycleForm onView() {
        return new CycleForm(itemSelect, false)
    }

    @Override
    Class topClass() {
        return null
    }

    @Override
    boolean deleteDomain() {
        return cycleService.delete(itemSelect, loginManager)
    }

    @Override
    protected void security() {

    }
}
