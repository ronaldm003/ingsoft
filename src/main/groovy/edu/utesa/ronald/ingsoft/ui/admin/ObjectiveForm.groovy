package edu.utesa.ronald.ingsoft.ui.admin

import com.vaadin.server.Sizeable
import edu.utesa.ronald.ingsoft.domains.admin.Objective
import edu.utesa.ronald.ingsoft.services.admin.ObjectiveService
import edu.utesa.ronald.ingsoft.ui.components.WindowForm
import edu.utesa.ronald.ingsoft.utils.Constants
import edu.utesa.ronald.ingsoft.utils.Languages
import edu.utesa.ronald.ingsoft.utils.WindowsCaption
import groovy.transform.CompileStatic
import org.springframework.beans.factory.annotation.Autowired

/**
 * Aqui se realizan todos los casos de los botones de accion.
 *  Created by ronald on 3/07/17.
 */
@CompileStatic
class ObjectiveForm extends WindowForm<Objective> {

    @Autowired
    private ObjectiveService objectiveService

    private ObjectiveFormObjective formObjective
    private ObjectiveFormTabArticles formTabArticles

    ObjectiveForm() {
        this(null, null)
    }

    ObjectiveForm(Objective location, Boolean edit) {
        super(location, edit)
        confLayout(WindowsCaption.OBJECTIVE_CAPTION)
        setWidth(600, Sizeable.Unit.PIXELS)
    }

    @Override
    protected void prepare() {
        super.prepare()
        Constants.instance.autoWiredClass(this)
    }

    @Override
    protected void onCreate() {
        formObjective = new ObjectiveFormObjective()
        formTabArticles = new ObjectiveFormTabArticles()
    }

    @Override
    protected void onEditView() {
        itemSave = objectiveService.refresh(itemSave.id)
        formObjective = new ObjectiveFormObjective(itemSave, edit)
        formTabArticles = new ObjectiveFormTabArticles(itemSave.listArticle.toList(), edit)
    }

    @Override
    Objective onSave() {
        itemSave = formObjective.getValid()
        itemSave.listArticle = formTabArticles.getValid().toSet()
        return objectiveService.create(itemSave, loginManager)
    }

    @Override
    protected void onViewTabs() {

    }

    @Override
    protected void buildView() {

    }

    @Override
    protected boolean getValidForm() {
        if (!formObjective.getValid()) {
            return tabSheetManager.selectTab(0)
        }
//        else if (!formTabArticles.getValid()) {
//            tabSheetManager.selectTab(1)
//            return false
//        }
        return true
    }

    @Override
    protected void buildLayout() {
        setWidth(800, Sizeable.Unit.PIXELS)
        tabSheetManager.addTabPanel(Languages.instance.getText(WindowsCaption.OBJECTIVE_CAPTION + FORM + "objective"), formObjective)
        tabSheetManager.addTabPanel(Languages.instance.getText(WindowsCaption.ARTICLE_CAPTION + FORM + "article"), formTabArticles)

    }

    @Override
    protected void security() {

    }
}
