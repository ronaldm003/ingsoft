package edu.utesa.ronald.ingsoft.ui.admin

import com.vaadin.event.FieldEvents
import edu.utesa.ronald.ingsoft.components.abstracts.AWindowTab
import edu.utesa.ronald.ingsoft.domains.admin.Objective
import edu.utesa.ronald.ingsoft.services.admin.ObjectiveService
import edu.utesa.ronald.ingsoft.utils.Constants
import edu.utesa.ronald.ingsoft.utils.Languages
import groovy.transform.CompileStatic
import org.springframework.beans.factory.annotation.Autowired

/**
 *  Created by ronald on 3/07/17.
 */
@CompileStatic
class ObjectiveFormObjective extends AWindowTab<Objective> {

    @Autowired
    private ObjectiveService objectiveService

    private ObjectiveFormObjectiveDesign objectiveFormObjectiveDesign = new ObjectiveFormObjectiveDesign()

    ObjectiveFormObjective() {
        this(new Objective(), null)
    }

    ObjectiveFormObjective(Objective clinic, Boolean edit) {
        super(clinic, edit)
        confLayout(objectiveFormObjectiveDesign)
    }

    @Override
    protected void buildLayout() {
        objectiveFormObjectiveDesign.tfDescription.setRequired(true)
        objectiveFormObjectiveDesign.tfDescription.setRequiredError(Languages.instance.getText("validations.empty.field"))
        objectiveFormObjectiveDesign.tfDescription.setValidationVisible(true)
        objectiveFormObjectiveDesign.tfDescription.setNullRepresentation("")
        objectiveFormObjectiveDesign.tfDescription.addTextChangeListener(new FieldEvents.TextChangeListener() {
            @Override
            void textChange(FieldEvents.TextChangeEvent event) {
                objectiveFormObjectiveDesign.tfDescription.setValue(event.getText())
                objectiveFormObjectiveDesign.tfDescription.setCursorPosition(event.getCursorPosition())
                objectiveFormObjectiveDesign.tfDescription.validate()
            }
        })
    }

    @Override
    protected void restore() {
        objectiveFormObjectiveDesign.tfDescription.setValue(itemSave.description ?: "")
        objectiveFormObjectiveDesign.tfProcedureQuantity.setValue(itemSave.procedureQuantity.toString() ?: "")
        objectiveFormObjectiveDesign.dfLimitDate.setValue(itemSave.limitDate ?: new Date())
    }

    @Override
    protected void visualize() {
        objectiveFormObjectiveDesign.tfDescription.setReadOnly(true)
        objectiveFormObjectiveDesign.tfProcedureQuantity.setReadOnly(true)
        objectiveFormObjectiveDesign.dfLimitDate.setReadOnly(true)
    }

    @Override
    protected boolean getElementsValid() {
        if (!objectiveFormObjectiveDesign.tfDescription.isValid()) {
            objectiveFormObjectiveDesign.tfDescription.focus()
            return false
        } else if (!objectiveFormObjectiveDesign.tfProcedureQuantity.isValid()) {
            objectiveFormObjectiveDesign.tfProcedureQuantity.focus()
            return false
        } else if (!objectiveFormObjectiveDesign.dfLimitDate.isValid()) {
            objectiveFormObjectiveDesign.dfLimitDate.focus()
            return false
        }
        return true
    }

    @Override
    protected void collectData() {
        itemSave.description = objectiveFormObjectiveDesign.tfDescription.getValue()
        itemSave.procedureQuantity = objectiveFormObjectiveDesign.tfProcedureQuantity.getValue().toLong()
        itemSave.limitDate = objectiveFormObjectiveDesign.dfLimitDate.getValue()
    }

    @Override
    protected void confLanguage() {
        Constants.instance.autoWiredClass(this)
        objectiveFormObjectiveDesign.tfDescription.setCaption(Languages.instance.getText("window.description"))
        objectiveFormObjectiveDesign.tfProcedureQuantity.setCaption(Languages.instance.getText("window.procedure.quantity"))
        objectiveFormObjectiveDesign.dfLimitDate.setCaption(Languages.instance.getText("window.limit.date"))
    }

    @Override
    protected Objective getValid() {
        if (getElementsValid()) {
            collectData()
            return itemSave
        }
        return null
    }

    @Override
    void startFocus() {
        objectiveFormObjectiveDesign.tfDescription.focus()
    }
}
