package edu.utesa.ronald.ingsoft.ui.admin

import edu.utesa.ronald.ingsoft.domains.admin.Objective
import edu.utesa.ronald.ingsoft.models.containers.admin.ObjectiveContainer
import edu.utesa.ronald.ingsoft.services.admin.ObjectiveService
import edu.utesa.ronald.ingsoft.ui.components.ATabBase
import edu.utesa.ronald.ingsoft.utils.Constants
import edu.utesa.ronald.ingsoft.utils.container.LazyQuery
import groovy.transform.CompileStatic
import org.springframework.beans.factory.annotation.Autowired

/**
 * Aqui se lista todos los tipos de usuario
 *  Created by ronald on 3/07/17.
 */
@CompileStatic
class ObjectiveTab extends ATabBase<ObjectiveForm, Objective> {

    @Autowired
    private ObjectiveService objectiveService

    ObjectiveTab() {
        Constants.instance.autoWiredClass(this)
    }

    @Override
    protected void confLanguage() {

    }

    @Override
    protected void buildLayout() {
        manageGrid(new ObjectiveContainer(new LazyQuery<Objective>() {
            @Override
            int getSize() {
                return objectiveService.count(showDelete())
            }

            @Override
            List<Objective> getItemsIds(int startIndex, int numberOfIds) {
                return objectiveService.list(showDelete(), startIndex, numberOfIds)
            }

            @Override
            int getFilteredSize() {
                return 0
            }

            @Override
            List<Objective> getFilteredItemsIds(int startIndex, int numberOfIds) {
                return null
            }
        }))
    }

    @Override
    void refreshItem() {

    }

    @Override
    ObjectiveForm onNew() {
        return new ObjectiveForm()
    }

    @Override
    ObjectiveForm onEdit() {
        return new ObjectiveForm(itemSelect, true)
    }

    @Override
    ObjectiveForm onView() {
        return new ObjectiveForm(itemSelect, false)
    }

    @Override
    Class topClass() {
        return null
    }

    @Override
    boolean deleteDomain() {
        return objectiveService.delete(itemSelect, loginManager)
    }

    @Override
    protected void security() {

    }
}
