package edu.utesa.ronald.ingsoft.ui.admin

import com.vaadin.server.Sizeable
import edu.utesa.ronald.ingsoft.domains.admin.Student
import edu.utesa.ronald.ingsoft.models.interfaces.OnNew
import edu.utesa.ronald.ingsoft.services.admin.StudentService
import edu.utesa.ronald.ingsoft.ui.components.WindowForm
import edu.utesa.ronald.ingsoft.utils.Constants
import edu.utesa.ronald.ingsoft.utils.Languages
import edu.utesa.ronald.ingsoft.utils.WindowsCaption
import groovy.transform.CompileStatic
import org.springframework.beans.factory.annotation.Autowired

/**
 * Aqui se realizan todos los casos de los botones de accion.
 *  Created by ronald on 3/07/17.
 */
@CompileStatic
class StudentForm extends WindowForm<Student> {

    @Autowired
    private StudentService studentService

    private StudentFormStudent formStudent
    private StudentFormTabSubjects formTabSubjects
//    private StudentFormTabEquipments formTabEquipments

    StudentForm() {
        this(null, null)
    }

    StudentForm(OnNew<Student> onNew) {
        this()
        this.onNew = onNew
    }

    StudentForm(Student student, Boolean edit) {
        super(student, edit)
        confLayout(WindowsCaption.STUDENT_CAPTION)
        setWidth(600, Sizeable.Unit.PIXELS)
    }

    @Override
    protected void prepare() {
        super.prepare()
        Constants.instance.autoWiredClass(this)
    }

    @Override
    protected void onCreate() {
        formStudent = new StudentFormStudent()
        formTabSubjects = new StudentFormTabSubjects()
//        formTabEquipments = new StudentFormTabEquipments()
    }

    @Override
    protected void onEditView() {
        itemSave = studentService.refresh(itemSave.id, { it.person.enabled; it.listSubjects.size() })
        formStudent = new StudentFormStudent(itemSave, edit)
        formTabSubjects = new StudentFormTabSubjects(itemSave.listSubjects.toList(), edit)
//        formTabEquipments = new StudentFormTabEquipments(itemSave.listEquipments.toList(), edit)
    }

    @Override
    Student onSave() {
        itemSave = formStudent.getValid()
        itemSave.listSubjects = formTabSubjects.getValid().toSet()
//        itemSave.listEquipments = formTabEquipments.getValid().toSet()
        return studentService.create(itemSave, loginManager)
    }

    @Override
    protected void onViewTabs() {

    }

    @Override
    protected void buildView() {

    }

    @Override
    protected boolean getValidForm() {
        if (!formStudent.getValid()) {
            return tabSheetManager.selectTab(0)
        }
//        else if (!formTabEquipments.getValid()) {
//            tabSheetManager.selectTab(1)
//            return false
//        }
        return true
    }

    @Override
    protected void buildLayout() {
        setWidth(800, Sizeable.Unit.PIXELS)
        tabSheetManager.addTabPanel(Languages.instance.getText(WindowsCaption.STUDENT_CAPTION + FORM + "student"), formStudent)
        tabSheetManager.addTabPanel(Languages.instance.getText(WindowsCaption.STUDENT_CAPTION + FORM + "subject"), formTabSubjects)
//        tabSheetManager.addTabPanel(Languages.instance.getText(WindowsCaption.CLINIC_CAPTION + FORM + "equipment"), formTabEquipments)

    }

    @Override
    protected void security() {

    }
}
