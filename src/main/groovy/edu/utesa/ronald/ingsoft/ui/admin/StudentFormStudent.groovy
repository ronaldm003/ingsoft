package edu.utesa.ronald.ingsoft.ui.admin

import com.vaadin.data.Property
import com.vaadin.event.FieldEvents
import com.vaadin.ui.Button
import edu.utesa.ronald.ingsoft.components.abstracts.AWindowTab
import edu.utesa.ronald.ingsoft.domains.admin.Student
import edu.utesa.ronald.ingsoft.domains.security.Person
import edu.utesa.ronald.ingsoft.models.interfaces.OnNew
import edu.utesa.ronald.ingsoft.models.interfaces.PickElement
import edu.utesa.ronald.ingsoft.models.suggestions.PersonSuggestions
import edu.utesa.ronald.ingsoft.services.admin.StudentService
import edu.utesa.ronald.ingsoft.services.security.PersonService
import edu.utesa.ronald.ingsoft.ui.components.forms.OneToOneAction
import edu.utesa.ronald.ingsoft.ui.security.PersonForm
import edu.utesa.ronald.ingsoft.ui.utils.pickers.PersonPicker
import edu.utesa.ronald.ingsoft.utils.Constants
import edu.utesa.ronald.ingsoft.utils.Languages
import groovy.transform.CompileStatic
import org.springframework.beans.factory.annotation.Autowired

/**
 *  Created by ronald on 3/07/17.
 */
@CompileStatic
class StudentFormStudent extends AWindowTab<Student> {

    @Autowired
    private StudentService studentService
    @Autowired
    private PersonService personService

    private StudentFormStudentDesign studentFormStudentDesign = new StudentFormStudentDesign()
    private OneToOneAction oneToOneActionPerson = new OneToOneAction(false)

    private Person personSelect

    StudentFormStudent() {
        this(new Student(), null)
    }

    StudentFormStudent(Student student, Boolean edit) {
        super(student, edit)
        if (student.person) {
            personSelect = student.person
        } else {
            personSelect = new Person()
        }
        confLayout(studentFormStudentDesign)
    }

    @Override
    protected void buildLayout() {
        studentFormStudentDesign.tfEnrollment.setRequired(true)
        studentFormStudentDesign.tfEnrollment.setRequiredError(Languages.instance.getText("validations.empty.field"))
        studentFormStudentDesign.tfEnrollment.setValidationVisible(true)
        studentFormStudentDesign.tfEnrollment.setNullRepresentation("")
        studentFormStudentDesign.tfEnrollment.addTextChangeListener(new FieldEvents.TextChangeListener() {
            @Override
            void textChange(FieldEvents.TextChangeEvent event) {
                studentFormStudentDesign.tfEnrollment.setValue(event.getText())
                studentFormStudentDesign.tfEnrollment.setCursorPosition(event.getCursorPosition())
                studentFormStudentDesign.tfEnrollment.validate()
            }
        })
        initPerson()
    }

    private void initPerson() {
        studentFormStudentDesign.pPerson.setContent(oneToOneActionPerson)
        oneToOneActionPerson.tfACSearch().setSuggestionProvider(new PersonSuggestions())
        oneToOneActionPerson.tfACSearch().addValueChangeListener(new Property.ValueChangeListener() {
            @Override
            void valueChange(Property.ValueChangeEvent event) {
                if (oneToOneActionPerson.tfACSearch().getValue() && !oneToOneActionPerson.tfACSearch().getValue().isEmpty()) {
                    try {
                        personSelect = personService.byName(oneToOneActionPerson.tfACSearch().getValue().split("-")[0])
                        setPersonFieldValues(personSelect)
                    } catch (Exception ignored) {
                    }
                } else {
                    personSelect = null
                }
            }
        })
        oneToOneActionPerson.btnNew().addClickListener(new Button.ClickListener() {
            @Override
            void buttonClick(Button.ClickEvent event) {
                PersonForm occupationForm = new PersonForm(new OnNew<Person>() {
                    @Override
                    void result(Person person) {
                        personSelect = person
                        setPersonFieldValues(personSelect)
                    }
                })
                getUI().addWindow(occupationForm)
            }
        })
        oneToOneActionPerson.btnSearch().addClickListener(new Button.ClickListener() {
            @Override
            void buttonClick(Button.ClickEvent event) {
                PersonPicker locationPicker = new PersonPicker(new PickElement<Person>() {
                    @Override
                    void onResult(List<Person> elements) {
                        if (elements.size() > 0) {
                            personSelect = elements.get(0)
                            setPersonFieldValues(personSelect)
                        }
                    }
                }, false)
                getUI().addWindow(locationPicker)
            }
        })
    }

    void selectPerson(Person person) {
        if (person) {
            oneToOneActionPerson.tfACSearch().setReadOnly(false)
            oneToOneActionPerson.select(person ? person.toString() : "")
            oneToOneActionPerson.tfACSearch().setReadOnly(true)
        }
    }

    @Override
    protected void restore() {
        personSelect = itemSave.person
        selectPerson(personSelect)
        studentFormStudentDesign.tfEnrollment.setValue(itemSave.enrollment ?: "")
    }

    private void setPersonFieldValues(Person person) {
        selectPerson(person)
//        studentFormStudentDesign.tfName.setValue(person.name ?: "")
//        studentFormStudentDesign.tfLastName.setValue(person.name ?: "")
//        studentFormStudentDesign.tfCellPhone.setValue(person.cellphone ?: "")
//        studentFormStudentDesign.tfEmail.setValue(person.email ?: "")
//        studentFormStudentDesign.cbGender.setValue(person.gender)
//        studentFormStudentDesign.cbNationality.setValue(person.nationality)
//        studentFormStudentDesign.cbWarrantType.setValue(person.warrantType)
//        studentFormStudentDesign.tfWarrant.setValue(person.warrant ?: "")
//        studentFormStudentDesign.cbGender.setValue(person.gender)
//        if (person.location) {
//            studentFormStudentDesign.tfLocation.setValue(person.location.toString() ?: "")
//        }
//        if (person.occupation) {
//            studentFormStudentDesign.tfOccupation.setValue(person.occupation.toString() ?: "")
//        }
//        studentFormStudentDesign.dfBirthday.setValue(person.birthDate ?: new Date())
//        studentFormStudentDesign.dfIngressDate.setValue(person.ingressDate ?: new Date())
    }

    private void setPersonFieldReadOnly() {
//        studentFormStudentDesign.tfName.setReadOnly(true)
//        studentFormStudentDesign.tfLastName.setReadOnly(true)
//        studentFormStudentDesign.tfCellPhone.setReadOnly(true)
//        studentFormStudentDesign.tfEmail.setReadOnly(true)
//        studentFormStudentDesign.cbGender.setReadOnly(true)
//        studentFormStudentDesign.cbNationality.setReadOnly(true)
//        studentFormStudentDesign.cbWarrantType.setReadOnly(true)
//        studentFormStudentDesign.tfWarrant.setReadOnly(true)
//        studentFormStudentDesign.cbGender.setReadOnly(true)
//        studentFormStudentDesign.tfLocation.setReadOnly(true)
//        studentFormStudentDesign.tfOccupation.setReadOnly(true)
//        studentFormStudentDesign.dfBirthday.setReadOnly(true)
//        studentFormStudentDesign.dfIngressDate.setReadOnly(true)
    }

    @Override
    protected void visualize() {
        oneToOneActionPerson.visualize()
        studentFormStudentDesign.tfEnrollment.setReadOnly(true)

    }

    @Override
    protected boolean getElementsValid() {
        if (!oneToOneActionPerson.isValid()) {
            oneToOneActionPerson.focusValid()
            return false
        } else if (!studentFormStudentDesign.tfEnrollment.isValid()) {
            studentFormStudentDesign.tfEnrollment.focus()
            return false
        }
        return true
    }

    @Override
    protected void collectData() {
        itemSave.person = personSelect
        itemSave.enrollment = studentFormStudentDesign.tfEnrollment.getValue()
    }

    @Override
    protected void confLanguage() {
        Constants.instance.autoWiredClass(this)
        oneToOneActionPerson.setCaption(Languages.instance.getText("window.person"))
        studentFormStudentDesign.tfEnrollment.setCaption(Languages.instance.getText("window.enrollment"))
    }

    @Override
    protected Student getValid() {
        if (getElementsValid()) {
            collectData()
            return itemSave
        }
        return null
    }

    @Override
    void startFocus() {
        oneToOneActionPerson.focus()
    }
}
