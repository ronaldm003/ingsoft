package edu.utesa.ronald.ingsoft.ui.admin

import edu.utesa.ronald.ingsoft.domains.admin.Student
import edu.utesa.ronald.ingsoft.models.containers.admin.StudentContainer
import edu.utesa.ronald.ingsoft.services.admin.StudentService
import edu.utesa.ronald.ingsoft.ui.admin.StudentForm
import edu.utesa.ronald.ingsoft.ui.components.ATabBase
import edu.utesa.ronald.ingsoft.utils.Constants
import edu.utesa.ronald.ingsoft.utils.container.LazyQuery
import groovy.transform.CompileStatic
import org.springframework.beans.factory.annotation.Autowired

/**
 * Aqui se lista todos los tipos de usuario
 *  Created by ronald on 3/07/17.
 */
@CompileStatic
class StudentTab extends ATabBase<StudentForm, Student> {

    @Autowired
    private StudentService studentService

    StudentTab() {
        Constants.instance.autoWiredClass(this)
    }

    @Override
    protected void confLanguage() {

    }

    @Override
    protected void buildLayout() {
        manageGrid(new StudentContainer(new LazyQuery<Student>() {
            @Override
            int getSize() {
                return studentService.count(showDelete())
            }

            @Override
            List<Student> getItemsIds(int startIndex, int numberOfIds) {
                return studentService.list(showDelete(), startIndex, numberOfIds, { it.person.enabled })
            }

            @Override
            int getFilteredSize() {
                return 0
            }

            @Override
            List<Student> getFilteredItemsIds(int startIndex, int numberOfIds) {
                return null
            }
        }))
    }

    @Override
    void refreshItem() {

    }

    @Override
    StudentForm onNew() {
        return new StudentForm()
    }

    @Override
    StudentForm onEdit() {
        return new StudentForm(itemSelect, true)
    }

    @Override
    StudentForm onView() {
        return new StudentForm(itemSelect, false)
    }

    @Override
    Class topClass() {
        return null
    }

    @Override
    boolean deleteDomain() {
        return studentService.delete(itemSelect, loginManager)
    }

    @Override
    protected void security() {

    }
}
