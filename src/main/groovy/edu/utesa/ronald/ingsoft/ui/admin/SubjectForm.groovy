package edu.utesa.ronald.ingsoft.ui.admin

import com.vaadin.server.Sizeable
import edu.utesa.ronald.ingsoft.domains.admin.Subject
import edu.utesa.ronald.ingsoft.services.admin.SubjectService
import edu.utesa.ronald.ingsoft.ui.components.WindowForm
import edu.utesa.ronald.ingsoft.utils.Constants
import edu.utesa.ronald.ingsoft.utils.Languages
import edu.utesa.ronald.ingsoft.utils.WindowsCaption
import groovy.transform.CompileStatic
import org.springframework.beans.factory.annotation.Autowired

/**
 * Aqui se realizan todos los casos de los botones de accion.
 *  Created by ronald on 3/07/17.
 */
@CompileStatic
class SubjectForm extends WindowForm<Subject> {

    @Autowired
    private SubjectService subjectService

    private SubjectFormSubject formSubject
    private SubjectFormTabObjectives formTabObjectives
    private SubjectFormTabSubjectSchedule formTabSubjectSchedule

    SubjectForm() {
        this(null, null)
    }

    SubjectForm(Subject location, Boolean edit) {
        super(location, edit)
        confLayout(WindowsCaption.SUBJECT_CAPTION)
        setWidth(600, Sizeable.Unit.PIXELS)
    }

    @Override
    protected void prepare() {
        super.prepare()
        Constants.instance.autoWiredClass(this)
    }

    @Override
    protected void onCreate() {
        formSubject = new SubjectFormSubject()
        formTabObjectives = new SubjectFormTabObjectives()
        formTabSubjectSchedule = new SubjectFormTabSubjectSchedule()
    }

    @Override
    protected void onEditView() {
        itemSave = subjectService.refresh(itemSave.id, { it.listSubjectSchedule.size(); it.listObjectives.size() })
        formSubject = new SubjectFormSubject(itemSave, edit)
        formTabObjectives = new SubjectFormTabObjectives(itemSave.listObjectives.toList(), edit)
        formTabSubjectSchedule = new SubjectFormTabSubjectSchedule(itemSave.listSubjectSchedule.toList(), edit)
    }

    @Override
    Subject onSave() {
        itemSave = formSubject.getValid()
        itemSave.listObjectives = formTabObjectives.getValid().toSet()
        itemSave.listSubjectSchedule = formTabSubjectSchedule.getValid().toSet()
        return subjectService.create(itemSave, loginManager)
    }

    @Override
    protected void onViewTabs() {

    }

    @Override
    protected void buildView() {

    }

    @Override
    protected boolean getValidForm() {
        if (!formSubject.getValid()) {
            return tabSheetManager.selectTab(0)
        }
//        else if (!formTabObjectives.getValid()) {
//            tabSheetManager.selectTab(1)
//            return false
//        }
        return true
    }

    @Override
    protected void buildLayout() {
        setWidth(800, Sizeable.Unit.PIXELS)
        tabSheetManager.addTabPanel(Languages.instance.getText(WindowsCaption.SUBJECT_CAPTION + FORM + "subject"), formSubject)
        tabSheetManager.addTabPanel(Languages.instance.getText(WindowsCaption.SUBJECT_CAPTION + FORM + "objective"), formTabObjectives)
        tabSheetManager.addTabPanel(Languages.instance.getText(WindowsCaption.SUBJECT_CAPTION + FORM + "schedule"), formTabSubjectSchedule)
    }

    @Override
    protected void security() {

    }
}
