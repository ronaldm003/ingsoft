package edu.utesa.ronald.ingsoft.ui.admin

import com.vaadin.event.FieldEvents
import edu.utesa.ronald.ingsoft.components.abstracts.AWindowTab
import edu.utesa.ronald.ingsoft.domains.admin.Subject
import edu.utesa.ronald.ingsoft.services.admin.SubjectService
import edu.utesa.ronald.ingsoft.utils.Constants
import edu.utesa.ronald.ingsoft.utils.Languages
import groovy.transform.CompileStatic
import org.springframework.beans.factory.annotation.Autowired

/**
 *  Created by ronald on 3/07/17.
 */
@CompileStatic
class SubjectFormSubject extends AWindowTab<Subject> {

    @Autowired
    private SubjectService subjectService

    private SubjectFormSubjectDesign subjectFormSubjectDesign = new SubjectFormSubjectDesign()

    SubjectFormSubject() {
        this(new Subject(), null)
    }

    SubjectFormSubject(Subject subject, Boolean edit) {
        super(subject, edit)
        confLayout(subjectFormSubjectDesign)
    }

    @Override
    protected void buildLayout() {
        subjectFormSubjectDesign.tfDescription.setRequired(true)
        subjectFormSubjectDesign.tfDescription.setRequiredError(Languages.instance.getText("validations.empty.field"))
        subjectFormSubjectDesign.tfDescription.setValidationVisible(true)
        subjectFormSubjectDesign.tfDescription.setNullRepresentation("")
        subjectFormSubjectDesign.tfDescription.addTextChangeListener(new FieldEvents.TextChangeListener() {
            @Override
            void textChange(FieldEvents.TextChangeEvent event) {
                subjectFormSubjectDesign.tfDescription.setValue(event.getText())
                subjectFormSubjectDesign.tfDescription.setCursorPosition(event.getCursorPosition())
                subjectFormSubjectDesign.tfDescription.validate()
            }
        })
    }

    @Override
    protected void restore() {
        subjectFormSubjectDesign.tfCode.setValue(itemSave.code ?: "")
        subjectFormSubjectDesign.tfDescription.setValue(itemSave.description ?: "")
        subjectFormSubjectDesign.tfCredits.setValue(itemSave.credits.toString() ?: "")
        subjectFormSubjectDesign.tfTeoricHours.setValue(itemSave.teoricHours.toString() ?: "")
        subjectFormSubjectDesign.tfPracticalHours.setValue(itemSave.practicalHours.toString() ?: "")
    }

    @Override
    protected void visualize() {
        subjectFormSubjectDesign.tfCode.setReadOnly(true)
        subjectFormSubjectDesign.tfDescription.setReadOnly(true)
        subjectFormSubjectDesign.tfCredits.setReadOnly(true)
        subjectFormSubjectDesign.tfTeoricHours.setReadOnly(true)
        subjectFormSubjectDesign.tfPracticalHours.setReadOnly(true)
    }

    @Override
    protected boolean getElementsValid() {
        if (!subjectFormSubjectDesign.tfCode.isValid()) {
            subjectFormSubjectDesign.tfCode.focus()
            return false
        } else if (!subjectFormSubjectDesign.tfDescription.isValid()) {
            subjectFormSubjectDesign.tfDescription.focus()
            return false
        } else if (!subjectFormSubjectDesign.tfCredits.isValid()) {
            subjectFormSubjectDesign.tfCredits.focus()
            return false
        } else if (!subjectFormSubjectDesign.tfTeoricHours.isValid()) {
            subjectFormSubjectDesign.tfTeoricHours.focus()
            return false
        } else if (!subjectFormSubjectDesign.tfPracticalHours.isValid()) {
            subjectFormSubjectDesign.tfPracticalHours.focus()
            return false
        }
        return true
    }

    @Override
    protected void collectData() {
        itemSave.code = subjectFormSubjectDesign.tfCode.getValue()
        itemSave.description = subjectFormSubjectDesign.tfDescription.getValue()
        itemSave.credits = subjectFormSubjectDesign.tfCredits.getValue().toLong()
        itemSave.teoricHours = subjectFormSubjectDesign.tfTeoricHours.getValue().toLong()
        itemSave.practicalHours = subjectFormSubjectDesign.tfPracticalHours.getValue().toLong()
    }

    @Override
    protected void confLanguage() {
        Constants.instance.autoWiredClass(this)
        subjectFormSubjectDesign.tfCode.setCaption(Languages.instance.getText("window.id"))
        subjectFormSubjectDesign.tfDescription.setCaption(Languages.instance.getText("window.description"))
        subjectFormSubjectDesign.tfCredits.setCaption(Languages.instance.getText("window.credit"))
        subjectFormSubjectDesign.tfTeoricHours.setCaption(Languages.instance.getText("window.teoric.hour"))
        subjectFormSubjectDesign.tfPracticalHours.setCaption(Languages.instance.getText("window.practical.hour"))
    }

    @Override
    protected Subject getValid() {
        if (getElementsValid()) {
            collectData()
            return itemSave
        }
        return null
    }

    @Override
    void startFocus() {
        subjectFormSubjectDesign.tfDescription.focus()
    }
}
