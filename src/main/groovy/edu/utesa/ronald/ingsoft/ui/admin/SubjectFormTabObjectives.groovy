package edu.utesa.ronald.ingsoft.ui.admin

import com.vaadin.data.Property
import com.vaadin.event.SelectionEvent
import com.vaadin.ui.Button
import edu.utesa.ronald.ingsoft.components.abstracts.AWindowTab
import edu.utesa.ronald.ingsoft.domains.admin.Objective
import edu.utesa.ronald.ingsoft.models.containers.admin.ObjectiveContainer
import edu.utesa.ronald.ingsoft.models.interfaces.PickElement
import edu.utesa.ronald.ingsoft.models.suggestions.ObjectiveSuggestions
import edu.utesa.ronald.ingsoft.services.admin.ObjectiveService
import edu.utesa.ronald.ingsoft.ui.components.TabWindowBase
import edu.utesa.ronald.ingsoft.ui.utils.pickers.ObjectivePicker
import edu.utesa.ronald.ingsoft.utils.Constants
import edu.utesa.ronald.ingsoft.utils.container.LazyQuery
import groovy.transform.CompileStatic
import org.springframework.beans.factory.annotation.Autowired

/**
 *  Created by ronald on 04/12/18.
 */
@CompileStatic
class SubjectFormTabObjectives extends AWindowTab<List<Objective>> {

    @Autowired
    private ObjectiveService objectiveService

    private TabWindowBase tabWindowBase = new TabWindowBase()

    private Objective objectiveSelect

    SubjectFormTabObjectives() {
        this(new ArrayList<Objective>(), null)
    }

    SubjectFormTabObjectives(List<Objective> itemSave, Boolean edit) {
        super(itemSave, edit)
        Constants.instance.autoWiredClass(this)
        confLayout(tabWindowBase)
    }

    @Override
    protected void confLanguage() {
    }

    @Override
    protected void buildLayout() {
        manageGrid(tabWindowBase.grid(), new ObjectiveContainer(new LazyQuery<Objective>() {
            @Override
            int getSize() {
                return ((List<Objective>) itemSave).size()
            }

            @Override
            List<Objective> getItemsIds(int startIndex, int numberOfIds) {
                return ((List<Objective>) itemSave).subList(startIndex, (startIndex + numberOfIds))
            }

            @Override
            int getFilteredSize() {
                return 0
            }

            @Override
            List<Objective> getFilteredItemsIds(int startIndex, int numberOfIds) {
                return new ArrayList<Objective>()
            }
        }))
        tabWindowBase.grid().addSelectionListener(new SelectionEvent.SelectionListener() {
            @Override
            void select(SelectionEvent event) {
                if (gridManager.getGrid().getSelectedRow()) {
                    objectiveSelect = (Objective) gridManager.getGrid().getSelectedRow()
                    tabWindowBase.enableMenuAction()
                } else {
                    objectiveSelect = null
                    tabWindowBase.disableMenuAction()
                }
            }
        })
        tabWindowBase.oneToManyAction.tfACSearch().setSuggestionProvider(new ObjectiveSuggestions())
        tabWindowBase.oneToManyAction.tfACSearch().addValueChangeListener(new Property.ValueChangeListener() {
            @Override
            void valueChange(Property.ValueChangeEvent event) {
                if (tabWindowBase.oneToManyAction.tfACSearch().getValue() && !tabWindowBase.oneToManyAction.tfACSearch().getValue().isEmpty()) {
                    try {
                        objectiveSelect = objectiveService.byDescription(tabWindowBase.oneToManyAction.tfACSearch().getValue())
                    } catch (Exception ignored) {
                    }
                } else {
                    objectiveSelect = null
                }
            }
        })
        tabWindowBase.oneToManyAction.btnAdd().addClickListener(new Button.ClickListener() {
            @Override
            void buttonClick(Button.ClickEvent clickEvent) {
                if (objectiveSelect) {
                    Objective objective = objectiveService.byDescription(tabWindowBase.oneToManyAction.tfACSearch().getValue())
                    ((List<Objective>) itemSave).add(objective)
                    itemSave = ((List<Objective>) itemSave).toSet().toList()
                    tabWindowBase.oneToManyAction.tfACSearch().setValue("")
                    updateTable()
                }
            }
        })
        tabWindowBase.oneToManyAction.btnDelete().addClickListener(new Button.ClickListener() {
            @Override
            void buttonClick(Button.ClickEvent clickEvent) {
                if (objectiveSelect) {
                    ((List<Objective>) itemSave).remove(objectiveSelect)
                    updateTable()
                }
            }
        })
        tabWindowBase.oneToManyAction.btnNew().setVisible(false)
        tabWindowBase.oneToManyAction.btnSearch().addClickListener(new Button.ClickListener() {
            @Override
            void buttonClick(Button.ClickEvent event) {
                ObjectivePicker objectivePicker = new ObjectivePicker(new PickElement<Objective>() {
                    @Override
                    void onResult(List<Objective> elements) {
                        ((List<Objective>) itemSave).addAll(elements)
                        itemSave = ((List<Objective>) itemSave).toSet().toList()
                        updateTable()
                    }
                }, true)
                getUI().addWindow(objectivePicker)
            }
        })
    }

    @Override
    protected void restore() {
    }

    @Override
    protected void visualize() {
        tabWindowBase.viewMenuAction()
    }

    @Override
    protected void security() {
    }

    @Override
    protected boolean getElementsValid() {
        return true
    }

    @Override
    protected void collectData() {
    }

    @Override
    protected List<Objective> getValid() {
        if (getElementsValid()) {
            return itemSave
        }
        return null
    }

    @Override
    void startFocus() {
        tabWindowBase.oneToManyAction.tfACSearch().focus()
    }
}
