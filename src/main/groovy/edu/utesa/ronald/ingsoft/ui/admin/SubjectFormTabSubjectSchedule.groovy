package edu.utesa.ronald.ingsoft.ui.admin

import com.vaadin.event.SelectionEvent
import com.vaadin.ui.Button
import edu.utesa.ronald.ingsoft.components.abstracts.AWindowTab
import edu.utesa.ronald.ingsoft.domains.admin.SubjectSchedule
import edu.utesa.ronald.ingsoft.models.containers.admin.SubjectScheduleContainer
import edu.utesa.ronald.ingsoft.models.interfaces.PickElement
import edu.utesa.ronald.ingsoft.services.admin.SubjectScheduleService
import edu.utesa.ronald.ingsoft.ui.components.TabWindowBase
import edu.utesa.ronald.ingsoft.ui.utils.pickers.SubjectSchedulePicker
import edu.utesa.ronald.ingsoft.utils.Constants
import edu.utesa.ronald.ingsoft.utils.ShowNotification
import edu.utesa.ronald.ingsoft.utils.container.LazyQuery
import groovy.transform.CompileStatic
import org.springframework.beans.factory.annotation.Autowired

/**
 *  Created by ronald on 04/12/18.
 */
@CompileStatic
class SubjectFormTabSubjectSchedule extends AWindowTab<List<SubjectSchedule>> {

    @Autowired
    private SubjectScheduleService subjectScheduleService

    private TabWindowBase tabWindowBase = new TabWindowBase()

    private SubjectSchedule subjectScheduleSelect

    SubjectFormTabSubjectSchedule() {
        this(new ArrayList<SubjectSchedule>(), null)
    }

    SubjectFormTabSubjectSchedule(List<SubjectSchedule> itemSave, Boolean edit) {
        super(itemSave, edit)
        Constants.instance.autoWiredClass(this)
        confLayout(tabWindowBase)
    }

    @Override
    protected void confLanguage() {
    }

    @Override
    protected void buildLayout() {
        manageGrid(tabWindowBase.grid(), new SubjectScheduleContainer(new LazyQuery<SubjectSchedule>() {
            @Override
            int getSize() {
                return ((List<SubjectSchedule>) itemSave).size()
            }

            @Override
            List<SubjectSchedule> getItemsIds(int startIndex, int numberOfIds) {
                return ((List<SubjectSchedule>) itemSave).subList(startIndex, (startIndex + numberOfIds))
            }

            @Override
            int getFilteredSize() {
                return 0
            }

            @Override
            List<SubjectSchedule> getFilteredItemsIds(int startIndex, int numberOfIds) {
                return new ArrayList<SubjectSchedule>()
            }
        }))
        tabWindowBase.grid().addSelectionListener(new SelectionEvent.SelectionListener() {
            @Override
            void select(SelectionEvent event) {
                if (gridManager.getGrid().getSelectedRow()) {
                    subjectScheduleSelect = (SubjectSchedule) gridManager.getGrid().getSelectedRow()
                    tabWindowBase.enableMenuAction()
                } else {
                    subjectScheduleSelect = null
                    tabWindowBase.disableMenuAction()
                }
            }
        })
        tabWindowBase.oneToManyAction.btnAdd().setVisible(false)
        tabWindowBase.oneToManyAction.btnDelete().addClickListener(new Button.ClickListener() {
            @Override
            void buttonClick(Button.ClickEvent clickEvent) {
                if (subjectScheduleSelect) {
                    ((List<SubjectSchedule>) itemSave).remove(subjectScheduleSelect)
                    updateTable()
                }
            }
        })
        tabWindowBase.oneToManyAction.btnNew().addClickListener(new Button.ClickListener() {
            @Override
            void buttonClick(Button.ClickEvent event) {
                ShowNotification.warning("Implementar la ventana de esto")
            }
        })
        tabWindowBase.oneToManyAction.btnSearch().addClickListener(new Button.ClickListener() {
            @Override
            void buttonClick(Button.ClickEvent event) {
                SubjectSchedulePicker objectivePicker = new SubjectSchedulePicker(new PickElement<SubjectSchedule>() {
                    @Override
                    void onResult(List<SubjectSchedule> elements) {
                        ((List<SubjectSchedule>) itemSave).addAll(elements)
                        itemSave = ((List<SubjectSchedule>) itemSave).toSet().toList()
                        updateTable()
                    }
                }, true)
                getUI().addWindow(objectivePicker)
            }
        })
    }

    @Override
    protected void restore() {
    }

    @Override
    protected void visualize() {
        tabWindowBase.viewMenuAction()
    }

    @Override
    protected void security() {
    }

    @Override
    protected boolean getElementsValid() {
        return true
    }

    @Override
    protected void collectData() {
    }

    @Override
    protected List<SubjectSchedule> getValid() {
        if (getElementsValid()) {
            return itemSave
        }
        return null
    }

    @Override
    void startFocus() {
        tabWindowBase.oneToManyAction.tfACSearch().focus()
    }
}
