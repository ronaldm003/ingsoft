package edu.utesa.ronald.ingsoft.ui.admin

import edu.utesa.ronald.ingsoft.domains.admin.Subject
import edu.utesa.ronald.ingsoft.models.containers.admin.SubjectContainer
import edu.utesa.ronald.ingsoft.services.admin.SubjectService
import edu.utesa.ronald.ingsoft.ui.components.ATabBase
import edu.utesa.ronald.ingsoft.utils.Constants
import edu.utesa.ronald.ingsoft.utils.container.LazyQuery
import groovy.transform.CompileStatic
import org.springframework.beans.factory.annotation.Autowired

/**
 * Aqui se lista todos los tipos de usuario
 *  Created by ronald on 3/07/17.
 */
@CompileStatic
class SubjectTab extends ATabBase<SubjectForm, Subject> {

    @Autowired
    private SubjectService subjectService

    SubjectTab() {
        Constants.instance.autoWiredClass(this)
    }

    @Override
    protected void confLanguage() {

    }

    @Override
    protected void buildLayout() {
        manageGrid(new SubjectContainer(new LazyQuery<Subject>() {
            @Override
            int getSize() {
                return subjectService.count(showDelete())
            }

            @Override
            List<Subject> getItemsIds(int startIndex, int numberOfIds) {
                return subjectService.list(showDelete(), startIndex, numberOfIds)
            }

            @Override
            int getFilteredSize() {
                return 0
            }

            @Override
            List<Subject> getFilteredItemsIds(int startIndex, int numberOfIds) {
                return null
            }
        }))
    }

    @Override
    void refreshItem() {

    }

    @Override
    SubjectForm onNew() {
        return new SubjectForm()
    }

    @Override
    SubjectForm onEdit() {
        return new SubjectForm(itemSelect, true)
    }

    @Override
    SubjectForm onView() {
        return new SubjectForm(itemSelect, false)
    }

    @Override
    Class topClass() {
        return null
    }

    @Override
    boolean deleteDomain() {
        return subjectService.delete(itemSelect, loginManager)
    }

    @Override
    protected void security() {

    }
}
