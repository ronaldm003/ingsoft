package edu.utesa.ronald.ingsoft.ui.admin

import com.vaadin.server.Sizeable
import edu.utesa.ronald.ingsoft.domains.admin.Teacher
import edu.utesa.ronald.ingsoft.models.interfaces.OnNew
import edu.utesa.ronald.ingsoft.services.admin.TeacherService
import edu.utesa.ronald.ingsoft.ui.components.WindowForm
import edu.utesa.ronald.ingsoft.utils.Constants
import edu.utesa.ronald.ingsoft.utils.Languages
import edu.utesa.ronald.ingsoft.utils.WindowsCaption
import groovy.transform.CompileStatic
import org.springframework.beans.factory.annotation.Autowired

/**
 * Aqui se realizan todos los casos de los botones de accion.
 *  Created by ronald on 3/07/17.
 */
@CompileStatic
class TeacherForm extends WindowForm<Teacher> {

    @Autowired
    private TeacherService teacherService

    private TeacherFormTeacher formTeacher
    private TeacherFormTabSubjects formTabSubjects

    TeacherForm() {
        this(null, null)
    }

    TeacherForm(OnNew<Teacher> onNew) {
        this()
        this.onNew = onNew
    }

    TeacherForm(Teacher teacher, Boolean edit) {
        super(teacher, edit)
        confLayout(WindowsCaption.TEACHER_CAPTION)
        setWidth(600, Sizeable.Unit.PIXELS)
    }

    @Override
    protected void prepare() {
        super.prepare()
        Constants.instance.autoWiredClass(this)
    }

    @Override
    protected void onCreate() {
        formTeacher = new TeacherFormTeacher()
        formTabSubjects = new TeacherFormTabSubjects()
    }

    @Override
    protected void onEditView() {
        itemSave = teacherService.refresh(itemSave.id, { it.person.enabled; it.listSubjects.size() })
        formTeacher = new TeacherFormTeacher(itemSave, edit)
        formTabSubjects = new TeacherFormTabSubjects(itemSave.listSubjects.toList(), edit)
    }

    @Override
    Teacher onSave() {
        itemSave = formTeacher.getValid()
        itemSave.listSubjects = formTabSubjects.getValid().toSet()
        return teacherService.create(itemSave, loginManager)
    }

    @Override
    protected void onViewTabs() {

    }

    @Override
    protected void buildView() {

    }

    @Override
    protected boolean getValidForm() {
        if (!formTeacher.getValid()) {
            return tabSheetManager.selectTab(0)
        }
//        else if (!formTabSubjects.getValid()) {
//            tabSheetManager.selectTab(1)
//            return false
//        }
        return true
    }

    @Override
    protected void buildLayout() {
        setWidth(800, Sizeable.Unit.PIXELS)
        tabSheetManager.addTabPanel(Languages.instance.getText(WindowsCaption.TEACHER_CAPTION + FORM + "teacher"), formTeacher)
        tabSheetManager.addTabPanel(Languages.instance.getText(WindowsCaption.SUBJECT_CAPTION + FORM + "subject"), formTabSubjects)

    }

    @Override
    protected void security() {

    }
}
