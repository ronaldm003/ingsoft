package edu.utesa.ronald.ingsoft.ui.admin

import com.vaadin.data.Property
import com.vaadin.event.SelectionEvent
import com.vaadin.ui.Button
import edu.utesa.ronald.ingsoft.components.abstracts.AWindowTab
import edu.utesa.ronald.ingsoft.domains.admin.Subject
import edu.utesa.ronald.ingsoft.models.containers.admin.SubjectContainer
import edu.utesa.ronald.ingsoft.models.interfaces.PickElement
import edu.utesa.ronald.ingsoft.models.suggestions.SubjectSuggestions
import edu.utesa.ronald.ingsoft.services.admin.SubjectService
import edu.utesa.ronald.ingsoft.ui.components.TabWindowBase
import edu.utesa.ronald.ingsoft.ui.utils.pickers.SubjectPicker
import edu.utesa.ronald.ingsoft.utils.Constants
import edu.utesa.ronald.ingsoft.utils.container.LazyQuery
import groovy.transform.CompileStatic
import org.springframework.beans.factory.annotation.Autowired

/**
 *  Created by ronald on 04/12/18.
 */
@CompileStatic
class TeacherFormTabSubjects extends AWindowTab<List<Subject>> {

    @Autowired
    private SubjectService subjectService

    private TabWindowBase tabWindowBase = new TabWindowBase()

    private Subject subjectSelect

    TeacherFormTabSubjects() {
        this(new ArrayList<Subject>(), null)
    }

    TeacherFormTabSubjects(List<Subject> itemSave, Boolean edit) {
        super(itemSave, edit)
        Constants.instance.autoWiredClass(this)
        confLayout(tabWindowBase)
    }

    @Override
    protected void confLanguage() {
    }

    @Override
    protected void buildLayout() {
        manageGrid(tabWindowBase.grid(), new SubjectContainer(new LazyQuery<Subject>() {
            @Override
            int getSize() {
                return ((List<Subject>) itemSave).size()
            }

            @Override
            List<Subject> getItemsIds(int startIndex, int numberOfIds) {
                return ((List<Subject>) itemSave).subList(startIndex, (startIndex + numberOfIds))
            }

            @Override
            int getFilteredSize() {
                return 0
            }

            @Override
            List<Subject> getFilteredItemsIds(int startIndex, int numberOfIds) {
                return new ArrayList<Subject>()
            }
        }))
        tabWindowBase.grid().addSelectionListener(new SelectionEvent.SelectionListener() {
            @Override
            void select(SelectionEvent event) {
                if (gridManager.getGrid().getSelectedRow()) {
                    subjectSelect = (Subject) gridManager.getGrid().getSelectedRow()
                    tabWindowBase.enableMenuAction()
                } else {
                    subjectSelect = null
                    tabWindowBase.disableMenuAction()
                }
            }
        })
        tabWindowBase.oneToManyAction.tfACSearch().setSuggestionProvider(new SubjectSuggestions())
        tabWindowBase.oneToManyAction.tfACSearch().addValueChangeListener(new Property.ValueChangeListener() {
            @Override
            void valueChange(Property.ValueChangeEvent event) {
                if (tabWindowBase.oneToManyAction.tfACSearch().getValue() && !tabWindowBase.oneToManyAction.tfACSearch().getValue().isEmpty()) {
                    try {
                        subjectSelect = subjectService.byDescription(tabWindowBase.oneToManyAction.tfACSearch().getValue())
                    } catch (Exception ignored) {
                    }
                } else {
                    subjectSelect = null
                }
            }
        })
        tabWindowBase.oneToManyAction.btnAdd().addClickListener(new Button.ClickListener() {
            @Override
            void buttonClick(Button.ClickEvent clickEvent) {
                if (subjectSelect) {
                    Subject subject = subjectService.byDescription(tabWindowBase.oneToManyAction.tfACSearch().getValue())
                    ((List<Subject>) itemSave).add(subject)
                    itemSave = ((List<Subject>) itemSave).toSet().toList()
                    tabWindowBase.oneToManyAction.tfACSearch().setValue("")
                    updateTable()
                }
            }
        })
        tabWindowBase.oneToManyAction.btnDelete().addClickListener(new Button.ClickListener() {
            @Override
            void buttonClick(Button.ClickEvent clickEvent) {
                if (subjectSelect) {
                    ((List<Subject>) itemSave).remove(subjectSelect)
                    updateTable()
                }
            }
        })
        tabWindowBase.oneToManyAction.btnNew().setVisible(false)
        tabWindowBase.oneToManyAction.btnSearch().addClickListener(new Button.ClickListener() {
            @Override
            void buttonClick(Button.ClickEvent event) {
                SubjectPicker subjectPicker = new SubjectPicker(new PickElement<Subject>() {
                    @Override
                    void onResult(List<Subject> elements) {
                        ((List<Subject>) itemSave).addAll(elements)
                        itemSave = ((List<Subject>) itemSave).toSet().toList()
                        updateTable()
                    }
                }, true)
                getUI().addWindow(subjectPicker)
            }
        })
    }

    @Override
    protected void restore() {
    }

    @Override
    protected void visualize() {
        tabWindowBase.viewMenuAction()
    }

    @Override
    protected void security() {
    }

    @Override
    protected boolean getElementsValid() {
        return true
    }

    @Override
    protected void collectData() {
    }

    @Override
    protected List<Subject> getValid() {
        if (getElementsValid()) {
            return itemSave
        }
        return null
    }

    @Override
    void startFocus() {
        tabWindowBase.oneToManyAction.tfACSearch().focus()
    }
}
