package edu.utesa.ronald.ingsoft.ui.admin

import com.vaadin.data.Property
import com.vaadin.event.FieldEvents
import com.vaadin.ui.Button
import edu.utesa.ronald.ingsoft.components.abstracts.AWindowTab
import edu.utesa.ronald.ingsoft.domains.admin.Teacher
import edu.utesa.ronald.ingsoft.domains.security.Person
import edu.utesa.ronald.ingsoft.models.interfaces.OnNew
import edu.utesa.ronald.ingsoft.models.interfaces.PickElement
import edu.utesa.ronald.ingsoft.models.suggestions.PersonSuggestions
import edu.utesa.ronald.ingsoft.services.admin.TeacherService
import edu.utesa.ronald.ingsoft.services.security.PersonService
import edu.utesa.ronald.ingsoft.ui.components.forms.OneToOneAction
import edu.utesa.ronald.ingsoft.ui.security.PersonForm
import edu.utesa.ronald.ingsoft.ui.utils.pickers.PersonPicker
import edu.utesa.ronald.ingsoft.utils.Constants
import edu.utesa.ronald.ingsoft.utils.Languages
import groovy.transform.CompileStatic
import org.springframework.beans.factory.annotation.Autowired

/**
 *  Created by ronald on 3/07/17.
 */
@CompileStatic
class TeacherFormTeacher extends AWindowTab<Teacher> {

    @Autowired
    private TeacherService teacherService
    @Autowired
    private PersonService personService

    private TeacherFormTeacherDesign teacherFormTeacherDesign = new TeacherFormTeacherDesign()
    private OneToOneAction oneToOneActionPerson = new OneToOneAction(false)

    private Person personSelect

    TeacherFormTeacher() {
        this(new Teacher(), null)
    }

    TeacherFormTeacher(Teacher teacher, Boolean edit) {
        super(teacher, edit)
        if (teacher.person) {
            personSelect = teacher.person
        } else {
            personSelect = new Person()
        }
        confLayout(teacherFormTeacherDesign)
    }

    @Override
    protected void buildLayout() {
        teacherFormTeacherDesign.tfCode.setRequired(true)
        teacherFormTeacherDesign.tfCode.setRequiredError(Languages.instance.getText("validations.empty.field"))
        teacherFormTeacherDesign.tfCode.setValidationVisible(true)
        teacherFormTeacherDesign.tfCode.setNullRepresentation("")
        teacherFormTeacherDesign.tfCode.addTextChangeListener(new FieldEvents.TextChangeListener() {
            @Override
            void textChange(FieldEvents.TextChangeEvent event) {
                teacherFormTeacherDesign.tfCode.setValue(event.getText())
                teacherFormTeacherDesign.tfCode.setCursorPosition(event.getCursorPosition())
                teacherFormTeacherDesign.tfCode.validate()
            }
        })
        initPerson()
    }

    private void initPerson() {
        teacherFormTeacherDesign.pPerson.setContent(oneToOneActionPerson)
        oneToOneActionPerson.tfACSearch().setSuggestionProvider(new PersonSuggestions())
        oneToOneActionPerson.tfACSearch().addValueChangeListener(new Property.ValueChangeListener() {
            @Override
            void valueChange(Property.ValueChangeEvent event) {
                if (oneToOneActionPerson.tfACSearch().getValue() && !oneToOneActionPerson.tfACSearch().getValue().isEmpty()) {
                    try {
                        personSelect = personService.byName(oneToOneActionPerson.tfACSearch().getValue().split("-")[0])
                        setPersonFieldValues(personSelect)
                    } catch (Exception ignored) {
                    }
                } else {
                    personSelect = null
                }
            }
        })
        oneToOneActionPerson.btnNew().addClickListener(new Button.ClickListener() {
            @Override
            void buttonClick(Button.ClickEvent event) {
                PersonForm occupationForm = new PersonForm(new OnNew<Person>() {
                    @Override
                    void result(Person person) {
                        personSelect = person
                        setPersonFieldValues(personSelect)
                    }
                })
                getUI().addWindow(occupationForm)
            }
        })
        oneToOneActionPerson.btnSearch().addClickListener(new Button.ClickListener() {
            @Override
            void buttonClick(Button.ClickEvent event) {
                PersonPicker locationPicker = new PersonPicker(new PickElement<Person>() {
                    @Override
                    void onResult(List<Person> elements) {
                        if (elements.size() > 0) {
                            personSelect = elements.get(0)
                            setPersonFieldValues(personSelect)
                        }
                    }
                }, false)
                getUI().addWindow(locationPicker)
            }
        })
    }

    void selectPerson(Person person) {
        if (person) {
            oneToOneActionPerson.tfACSearch().setReadOnly(false)
            oneToOneActionPerson.select(person ? person.toString() : "")
            oneToOneActionPerson.tfACSearch().setReadOnly(true)
        }
    }

    @Override
    protected void restore() {
        personSelect = itemSave.person
        teacherFormTeacherDesign.tfCode.setValue(itemSave.code ?: "")
        selectPerson(personSelect)
    }

    private void setPersonFieldValues(Person person) {
        selectPerson(person)
//        teacherFormTeacherDesign.tfName.setValue(person.name ?: "")
//        teacherFormTeacherDesign.tfLastName.setValue(person.name ?: "")
//        teacherFormTeacherDesign.tfCellPhone.setValue(person.cellphone ?: "")
//        teacherFormTeacherDesign.tfEmail.setValue(person.email ?: "")
//        teacherFormTeacherDesign.cbGender.setValue(person.gender)
//        teacherFormTeacherDesign.cbNationality.setValue(person.nationality)
//        teacherFormTeacherDesign.cbWarrantType.setValue(person.warrantType)
//        teacherFormTeacherDesign.tfWarrant.setValue(person.warrant ?: "")
//        teacherFormTeacherDesign.cbGender.setValue(person.gender)
//        if (person.location) {
//            teacherFormTeacherDesign.tfLocation.setValue(person.location.toString() ?: "")
//        }
//        if (person.occupation) {
//            teacherFormTeacherDesign.tfOccupation.setValue(person.occupation.toString() ?: "")
//        }
//        teacherFormTeacherDesign.dfBirthday.setValue(person.birthDate ?: new Date())
//        teacherFormTeacherDesign.dfIngressDate.setValue(person.ingressDate ?: new Date())
    }

    private void setPersonFieldReadOnly() {
//        teacherFormTeacherDesign.tfName.setReadOnly(true)
//        teacherFormTeacherDesign.tfLastName.setReadOnly(true)
//        teacherFormTeacherDesign.tfCellPhone.setReadOnly(true)
//        teacherFormTeacherDesign.tfEmail.setReadOnly(true)
//        teacherFormTeacherDesign.cbGender.setReadOnly(true)
//        teacherFormTeacherDesign.cbNationality.setReadOnly(true)
//        teacherFormTeacherDesign.cbWarrantType.setReadOnly(true)
//        teacherFormTeacherDesign.tfWarrant.setReadOnly(true)
//        teacherFormTeacherDesign.cbGender.setReadOnly(true)
//        teacherFormTeacherDesign.tfLocation.setReadOnly(true)
//        teacherFormTeacherDesign.tfOccupation.setReadOnly(true)
//        teacherFormTeacherDesign.dfBirthday.setReadOnly(true)
//        teacherFormTeacherDesign.dfIngressDate.setReadOnly(true)
    }

    @Override
    protected void visualize() {
        oneToOneActionPerson.visualize()
    }

    @Override
    protected boolean getElementsValid() {
        if (!oneToOneActionPerson.isValid()) {
            oneToOneActionPerson.focusValid()
            return false
        } else if (!teacherFormTeacherDesign.tfCode) {
            teacherFormTeacherDesign.tfCode.focus()
            return false
        }
        return true
    }

    @Override
    protected void collectData() {
        itemSave.person = personSelect
        itemSave.code = teacherFormTeacherDesign.tfCode.getValue()
    }

    @Override
    protected void confLanguage() {
        Constants.instance.autoWiredClass(this)
        teacherFormTeacherDesign.tfCode.setCaption(Languages.instance.getText("window.id"))
        oneToOneActionPerson.setCaption(Languages.instance.getText("window.person"))
    }

    @Override
    protected Teacher getValid() {
        if (getElementsValid()) {
            collectData()
            return itemSave
        }
        return null
    }

    @Override
    void startFocus() {
        oneToOneActionPerson.focus()
    }
}
