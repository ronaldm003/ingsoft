package edu.utesa.ronald.ingsoft.ui.admin

import edu.utesa.ronald.ingsoft.domains.admin.Teacher
import edu.utesa.ronald.ingsoft.models.containers.admin.TeacherContainer
import edu.utesa.ronald.ingsoft.services.admin.TeacherService
import edu.utesa.ronald.ingsoft.ui.admin.TeacherForm
import edu.utesa.ronald.ingsoft.ui.components.ATabBase
import edu.utesa.ronald.ingsoft.utils.Constants
import edu.utesa.ronald.ingsoft.utils.container.LazyQuery
import groovy.transform.CompileStatic
import org.springframework.beans.factory.annotation.Autowired

/**
 * Aqui se lista todos los tipos de usuario
 *  Created by ronald on 3/07/17.
 */
@CompileStatic
class TeacherTab extends ATabBase<TeacherForm, Teacher> {

    @Autowired
    private TeacherService teacherService

    TeacherTab() {
        Constants.instance.autoWiredClass(this)
    }

    @Override
    protected void confLanguage() {

    }

    @Override
    protected void buildLayout() {
        manageGrid(new TeacherContainer(new LazyQuery<Teacher>() {
            @Override
            int getSize() {
                return teacherService.count(showDelete())
            }

            @Override
            List<Teacher> getItemsIds(int startIndex, int numberOfIds) {
                return teacherService.list(showDelete(), startIndex, numberOfIds, { it.person.enabled })
            }

            @Override
            int getFilteredSize() {
                return 0
            }

            @Override
            List<Teacher> getFilteredItemsIds(int startIndex, int numberOfIds) {
                return null
            }
        }))
    }

    @Override
    void refreshItem() {

    }

    @Override
    TeacherForm onNew() {
        return new TeacherForm()
    }

    @Override
    TeacherForm onEdit() {
        return new TeacherForm(itemSelect, true)
    }

    @Override
    TeacherForm onView() {
        return new TeacherForm(itemSelect, false)
    }

    @Override
    Class topClass() {
        return null
    }

    @Override
    boolean deleteDomain() {
        return teacherService.delete(itemSelect, loginManager)
    }

    @Override
    protected void security() {

    }
}
