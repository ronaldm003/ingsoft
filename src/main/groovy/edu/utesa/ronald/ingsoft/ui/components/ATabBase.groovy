package edu.utesa.ronald.ingsoft.ui.components

import com.kbdunn.vaadin.addons.fontawesome.FontAwesome
import com.vaadin.addon.contextmenu.GridContextMenu
import com.vaadin.addon.contextmenu.Menu
import com.vaadin.addon.contextmenu.MenuItem
import com.vaadin.data.Property
import com.vaadin.event.ItemClickEvent
import com.vaadin.ui.Button
import com.vaadin.ui.Window
import edu.utesa.ronald.ingsoft.components.abstracts.ATab
import edu.utesa.ronald.ingsoft.utils.Languages
import edu.utesa.ronald.ingsoft.utils.ShowNotification
import edu.utesa.ronald.ingsoft.utils.container.BaseContainer
import groovy.transform.CompileStatic

/**
 *  Created by ronald on 7/29/17.
 */
@CompileStatic
abstract class ATabBase<T extends Window, P> extends ATab<T, P> {

    TabBase tabBase = new TabBase()
    protected GridContextMenu gridContextMenu

    ATabBase() {
        gridContextMenu = new GridContextMenu(tabBase.getGrid())
        confLayout(tabBase)
        manageForms()
        configureGridMenu()
        tabBase.disableMenuAction()
    }

    private void manageForms() {
        tabBase.grid.addItemClickListener(new ItemClickEvent.ItemClickListener() {
            @Override
            void itemClick(ItemClickEvent event) {
                if (event.isDoubleClick()) {
                    tabBase.grid.select(event.itemId)
                    tabBase.formTabAction.btnView().click()
                }
            }
        })
        tabBase.formTabAction.btnNew().addClickListener(new Button.ClickListener() {
            @Override
            void buttonClick(Button.ClickEvent event) {
                windowAdding(onNew(), true)
            }
        })
        tabBase.formTabAction.btnEdit().addClickListener(new Button.ClickListener() {
            @Override
            void buttonClick(Button.ClickEvent event) {
                refreshItem()
                windowAdding(onEdit(), true)
            }
        })
        tabBase.formTabAction.btnView().addClickListener(new Button.ClickListener() {
            @Override
            void buttonClick(Button.ClickEvent event) {
                refreshItem()
                windowAdding(onView(), false)
            }
        })
        tabBase.formTabAction.btnDelete().addClickListener(new Button.ClickListener() {
            @Override
            void buttonClick(Button.ClickEvent event) {
                if (deleteDomain()) {
                    ShowNotification.success()
                    updateTable()
                    unselect()
                } else {
                    ShowNotification.error("notification.error.operation")
                }
            }
        })
        tabBase.formTabAction.chkShowDelete().addValueChangeListener(new Property.ValueChangeListener() {
            @Override
            void valueChange(Property.ValueChangeEvent event) {
                updateTable()
            }
        })
    }

    private void configureGridMenu() {
        gridContextMenu.addGridHeaderContextMenuListener(new GridContextMenu.GridContextMenuOpenListener() {
            @Override
            void onContextMenuOpen(GridContextMenu.GridContextMenuOpenListener.GridContextMenuOpenEvent event) {
                gridContextMenu.removeItems()
                addGridHeaderMenu()
            }
        })
        gridContextMenu.addGridBodyContextMenuListener(new GridContextMenu.GridContextMenuOpenListener() {
            @Override
            void onContextMenuOpen(GridContextMenu.GridContextMenuOpenListener.GridContextMenuOpenEvent event) {
                gridContextMenu.removeItems()
                if (event.itemId) {
                    gridManager.select(event.itemId)
                }
                addGridBodyMenu()
            }
        })
        gridContextMenu.addGridFooterContextMenuListener(new GridContextMenu.GridContextMenuOpenListener() {
            @Override
            void onContextMenuOpen(GridContextMenu.GridContextMenuOpenListener.GridContextMenuOpenEvent event) {
                gridContextMenu.removeItems()
                addGridFooterMenu()
            }
        })
    }

    protected final void baseNewMenu() {
        if (tabBase.formTabAction.btnNew().isVisible() && tabBase.formTabAction.btnNew().isEnabled()) {
            gridContextMenu.addItem(Languages.instance.getText("window.action.new"), FontAwesome.PENCIL,
                    new Menu.Command() {
                        @Override
                        void menuSelected(MenuItem selectedItem) {
                            tabBase.formTabAction.btnNew().click()
                        }
                    })
        }
    }

    protected final void baseEditMenu() {
        if (tabBase.formTabAction.btnEdit().isVisible() && tabBase.formTabAction.btnEdit().isEnabled()) {
            gridContextMenu.addItem(Languages.instance.getText("window.action.edit"), FontAwesome.EDIT,
                    new Menu.Command() {
                        @Override
                        void menuSelected(MenuItem selectedItem) {
                            tabBase.formTabAction.btnEdit().click()
                        }
                    })
        }
    }

    protected final void baseViewMenu() {
        if (tabBase.formTabAction.btnView().isVisible() && tabBase.formTabAction.btnView().isEnabled()) {
            gridContextMenu.addItem(Languages.instance.getText("window.action.view"), FontAwesome.BELL,
                    new Menu.Command() {
                        @Override
                        void menuSelected(MenuItem selectedItem) {
                            tabBase.formTabAction.btnView().click()
                        }
                    })
        }
    }

    protected final void baseDeleteMenu() {
        if (tabBase.formTabAction.btnDelete().isVisible() && tabBase.formTabAction.btnDelete().isEnabled()) {
            gridContextMenu.addItem(Languages.instance.getText("window.action.delete"), FontAwesome.TRASH_O,
                    new Menu.Command() {
                        @Override
                        void menuSelected(MenuItem selectedItem) {
                            tabBase.formTabAction.btnDelete().click()
                        }
                    })
        }
    }

    protected final void baseRefreshGrid() {
        gridContextMenu.addSeparator()
        gridContextMenu.addItem(Languages.instance.getText("window.action.refresh"), FontAwesome.BELL,
                new Menu.Command() {
                    @Override
                    void menuSelected(MenuItem selectedItem) {
                        updateTable()
                    }
                })
    }

    protected final void baseMenu() {
        baseNewMenu()
        if (gridManager.haveSelection()) {
            baseEditMenu()
            baseViewMenu()
            baseDeleteMenu()
        }
        baseRefreshGrid()
    }

    protected void addGridHeaderMenu() {
        baseNewMenu()
    }

    protected void addGridBodyMenu() {
        baseMenu()
    }

    protected void addGridFooterMenu() {
        baseNewMenu()
    }

    protected void updateTable() {
        tabBase.disableMenuAction()
        gridManager.updateTable()
    }

    protected void windowAdding(T window, boolean closeListener) {
        if (window) {
            if (closeListener) {
                window.addCloseListener(new Window.CloseListener() {
                    @Override
                    void windowClose(Window.CloseEvent e) {
                        updateTable()
                        unselect()
                    }
                })
            }
            getUI().addWindow(window)
        }
    }

    void applyPermissionBtnNew(boolean canUse) {
        tabBase.formTabAction.applyPermissionBtnNew(canUse)
    }

    void applyPermissionBtnEdit(boolean canUse) {
        tabBase.formTabAction.applyPermissionBtnEdit(canUse)
    }

    void applyPermissionBtnView(boolean canUse) {
        tabBase.formTabAction.applyPermissionBtnView(canUse)
    }

    void applyPermissionBtnDelete(boolean canUse) {
        tabBase.formTabAction.applyPermissionBtnDelete(canUse)
    }

    void applyPermissionchkShowDelete(boolean canUse) {
        tabBase.formTabAction.applyPermissionchkShowDelete(canUse)
    }

    boolean showDelete() {
        tabBase.showDelete()
    }

    @Override
    protected void manageGrid(BaseContainer container) {
        manageGrid(tabBase.getGrid(), container)
    }

    @Override
    protected final void enableMenu() {
        tabBase.enableMenuAction()
    }

    @Override
    protected void disableMenu() {
        tabBase.disableMenuAction()
    }

    void offMenu() {
        tabBase.offMenu()
    }
}
