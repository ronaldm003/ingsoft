package edu.utesa.ronald.ingsoft.ui.components

import com.vaadin.addon.contextmenu.ContextMenu
import com.vaadin.addon.contextmenu.Menu
import com.vaadin.addon.contextmenu.MenuItem
import com.vaadin.server.Resource
import com.vaadin.ui.Component
import com.vaadin.ui.TabSheet
import com.vaadin.ui.themes.ValoTheme
import edu.utesa.ronald.ingsoft.components.TabSheetManager
import edu.utesa.ronald.ingsoft.components.abstracts.APanel
import edu.utesa.ronald.ingsoft.ui.dashboard.Dashboard
import edu.utesa.ronald.ingsoft.utils.Languages
import groovy.transform.CompileStatic

/**
 *  Created by ronald on 7/11/17.
 */
@SuppressWarnings(["ChangeToOperator", "ChangeToOperator"])
@CompileStatic
class ContentPanel extends APanel {

    private ContentPanelDesign contentPanelDesign = new ContentPanelDesign()

    private ContextMenu contextMenu
    private TabSheetManager tabSheetManager
    private String tabSelect

    ContentPanel() {
        confLayout(contentPanelDesign)
    }

    @Override
    protected void confLanguage() {
        removeStyleName(ValoTheme.PANEL_BORDERLESS)
    }

    @Override
    protected void buildLayout() {
        tabSheetManager = new TabSheetManager(contentPanelDesign.tsContent)
        tabSheetManager.tabSheet().addStyleName(ValoTheme.TABSHEET_COMPACT_TABBAR)
        tabSheetManager.tabSheet().addSelectedTabChangeListener(new TabSheet.SelectedTabChangeListener() {
            @Override
            void selectedTabChange(TabSheet.SelectedTabChangeEvent event) {
                tabSelect = tabSheetManager.selectedTabName()
            }
        })
        tabSheetManager.addTabPanel(Languages.instance.getText("dashboard.principal"), new Dashboard())
        contextMenu = new ContextMenu(tabSheetManager.tabSheet(), true)
        contextMenu.addItem(Languages.instance.getText("window.action.close.all"), new Menu.Command() {
            @Override
            void menuSelected(MenuItem selectedItem) {
                List<TabSheet.Tab> tabs = new ArrayList<>()
                for (int i = 0; i < tabSheetManager.countTabs(); i++) {
                    if (tabSheetManager.getTab(i).isClosable()) {
                        tabs.add(tabSheetManager.getTab(i))
                    }
                }
                tabSheetManager.removeTabs(tabs)
            }
        })
        contextMenu.addItem(Languages.instance.getText("window.action.close.all.without.selected"), new Menu.Command() {
            @Override
            void menuSelected(MenuItem selectedItem) {
                List<TabSheet.Tab> tabs = new ArrayList<>()
                for (int i = 0; i < tabSheetManager.countTabs(); i++) {
                    if (tabSheetManager.getTab(i).isClosable() &&
                            !tabSheetManager.getTab(i).caption.equals(tabSelect)) {
                        tabs.add(tabSheetManager.getTab(i))
                    }
                }
                tabSheetManager.removeTabs(tabs)
            }
        })
        contextMenu.addItem(Languages.instance.getText("window.action.close.active"), new Menu.Command() {
            @Override
            void menuSelected(MenuItem selectedItem) {
                List<TabSheet.Tab> tabs = new ArrayList<>()
                for (int i = 0; i < tabSheetManager.countTabs(); i++) {
                    if (tabSheetManager.getTab(i).isClosable() &&
                            tabSheetManager.getTab(i).caption.equals(tabSelect)) {
                        tabs.add(tabSheetManager.getTab(i))
                    }
                }
                tabSheetManager.removeTabs(tabs)
            }
        })
    }

    @Override
    protected void security() {
    }

    void addTabPanel(String name, Component component, Resource icon) {
        tabSelect = name
        tabSheetManager.addTabPanel(name, component, icon, true)
    }

}
