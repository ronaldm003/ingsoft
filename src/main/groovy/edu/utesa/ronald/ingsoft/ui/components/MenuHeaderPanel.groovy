package edu.utesa.ronald.ingsoft.ui.components

import com.kbdunn.vaadin.addons.fontawesome.FontAwesome
import com.vaadin.addon.contextmenu.ContextMenu
import com.vaadin.event.FieldEvents
import com.vaadin.event.ShortcutAction
import com.vaadin.event.ShortcutListener
import com.vaadin.server.StreamResource
import com.vaadin.ui.Component
import com.vaadin.ui.TextField
import com.vaadin.ui.themes.ValoTheme
import edu.utesa.ronald.ingsoft.components.abstracts.APanel
import edu.utesa.ronald.ingsoft.models.constants.Styles
import edu.utesa.ronald.ingsoft.models.interfaces.SearchMenu
import edu.utesa.ronald.ingsoft.utils.*
import groovy.transform.CompileStatic

/**
 *  Created by ronald on03/07/17.
 */
@SuppressWarnings("ChangeToOperator")
@CompileStatic
class MenuHeaderPanel extends APanel {

    private MenuHeaderPanelDesign menuHeaderPanelDesign = new MenuHeaderPanelDesign()

    private ContentPanel contentPanel
    private ContextMenu contextMenu
    private MenuPanel menuPanel

    MenuHeaderPanel(MenuPanel menuPanel, ContentPanel contentPanel) {
        this.menuPanel = menuPanel
        this.contentPanel = contentPanel
        confLayout(menuHeaderPanelDesign)
    }

    @Override
    protected void confLanguage() {
        menuHeaderPanelDesign.addStyleName(ValoTheme.PANEL_BORDERLESS)
        menuHeaderPanelDesign.imgLogo.setSource(new StreamResource(new StreamResource.StreamSource() {
            @Override
            InputStream getStream() {
                return ImageLoader.instance.getImageStream(ImageModel.INGSOFT)
            }
        }, ImageModel.INGSOFT.toString() + ".png"))
        menuHeaderPanelDesign.addStyleName(Styles.LEFT_PANEL_MENU)
        menuHeaderPanelDesign.tfSearch.setInputPrompt(Languages.instance.getText(MenuCaption.SEARCH_MENU))
        menuHeaderPanelDesign.tfSearch.setIcon(FontAwesome.SEARCH)
        menuHeaderPanelDesign.tfSearch.addStyleName(Styles.SEARCH_MENU)
        menuHeaderPanelDesign.tfSearch.addStyleName(ValoTheme.TEXTFIELD_INLINE_ICON)
        menuHeaderPanelDesign.tfSearch.addShortcutListener(new ShortcutListener("clean", ShortcutAction.KeyCode.ESCAPE, null) {
            @Override
            void handleAction(Object sender, Object target) {
                menuHeaderPanelDesign.tfSearch.clear()
                search("")
            }
        })
    }

    @Override
    protected void buildLayout() {
        contextMenu = new ContextMenu(menuHeaderPanelDesign.imgLogo, true)
        menuHeaderPanelDesign.addStyleName(Styles.LEFT_PANEL_MENU)
        menuHeaderPanelDesign.tfSearch.setImmediate(true)
        menuHeaderPanelDesign.tfSearch.addTextChangeListener(new FieldEvents.TextChangeListener() {
            @Override
            void textChange(FieldEvents.TextChangeEvent event) {
                search(event.text)
            }
        })
    }

    private void search(String text) {
        text = StringTool.onlyAscii(text.trim()).toLowerCase()
        boolean stateSearch = !(!text || text.equals(""))
        Iterator<Component> itr = menuPanel.design().iterator()
        while (itr.hasNext()) {
            Component component = itr.next()
            if (component instanceof SearchMenu) {
                SearchMenu searchMenu = (SearchMenu) component
                if (stateSearch) {
                    searchMenu.onSearch(text)
                } else {
                    searchMenu.clearSearch()
                }
            }
        }
    }

    TextField tfSearch() {
        return menuHeaderPanelDesign.tfSearch
    }

    @Override
    protected void security() {
    }
}
