package edu.utesa.ronald.ingsoft.ui.components

import com.vaadin.ui.VerticalLayout
import edu.utesa.ronald.ingsoft.components.abstracts.APanel
import edu.utesa.ronald.ingsoft.models.constants.Styles
import edu.utesa.ronald.ingsoft.ui.components.menus.*
import edu.utesa.ronald.ingsoft.utils.Constants
import groovy.transform.CompileStatic

/**
 *  Created by ronald on 7/11/17.
 */
@SuppressWarnings("ChangeToOperator")
@CompileStatic
class MenuPanel extends APanel {

    private MenuPanelDesign menuPanelDesign = new MenuPanelDesign()

    private ContentPanel contentPanel

    MenuPanel(ContentPanel contentPanel) {
        this.contentPanel = contentPanel
        Constants.instance.autoWiredClass(this)
        confLayout(menuPanelDesign)
    }

    @Override
    protected void confLanguage() {
    }

    @Override
    protected void buildLayout() {
        menuPanelDesign.addStyleName(Styles.LEFT_PANEL_MENU)
    }

    @Override
    protected void security() {
        addMenus(loginManager.user.admin)
//        menuPanelDesign.addComponent(new Label("<hr/><strong><center>ADM</center></strong><hr/>", ContentMode.HTML))
    }

    private void addMenus(boolean root) {
        menuPanelDesign.addComponent(new MenuDental(contentPanel))
        menuPanelDesign.addComponent(new MenuAdmin(contentPanel))
        menuPanelDesign.addComponent(new MenuControl(contentPanel))
        menuPanelDesign.addComponent(new MenuLocation(contentPanel))
        menuPanelDesign.addComponent(new MenuSecurity(contentPanel))
        menuPanelDesign.addComponent(new MenuUtils(contentPanel))

    }

    VerticalLayout design() {
        return menuPanelDesign
    }
}
