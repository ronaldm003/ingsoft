package edu.utesa.ronald.ingsoft.ui.components

import com.vaadin.ui.Grid
import edu.utesa.ronald.ingsoft.components.abstracts.APanel
import edu.utesa.ronald.ingsoft.ui.components.forms.FormTabAction
import groovy.transform.CompileStatic

/**
 *
 *  Created by ronald on 7/11/17.
 */
@CompileStatic
class TabBase extends APanel {

    private TabBaseDesign tabBaseDesign = new TabBaseDesign()
    final FormTabAction formTabAction = new FormTabAction()

    TabBase() {
        confLayout(tabBaseDesign)
    }

    @Override
    protected void confLanguage() {
    }

    @Override
    protected void buildLayout() {
        tabBaseDesign.pMenu.setContent(formTabAction)
        disableMenuAction()
    }

    @Override
    protected void security() {
    }

    final Grid getGrid() {
        return tabBaseDesign.gridData
    }

    boolean showDelete() {
        if (formTabAction.chkShowDelete().isVisible()) {
            return !formTabAction.chkShowDelete().getValue()
        }
        return true
    }

    void enableMenuAction() {
        formTabAction.btnEdit().setEnabled(true)
        formTabAction.btnView().setEnabled(true)
        formTabAction.btnDelete().setEnabled(true)
    }

    void disableMenuAction() {
        formTabAction.btnEdit().setEnabled(false)
        formTabAction.btnView().setEnabled(false)
        formTabAction.btnDelete().setEnabled(false)
    }

    void offMenu() {
        tabBaseDesign.pMenu.setEnabled(false)
        tabBaseDesign.pMenu.setVisible(false)

        hideButtons()
    }

    void hideButtons() {
        formTabAction.applyPermissionBtnEdit(false)
        formTabAction.applyPermissionBtnNew(false)
        formTabAction.applyPermissionBtnView(false)
        formTabAction.applyPermissionBtnDelete(false)
        formTabAction.applyPermissionchkShowDelete(false)
    }
}
