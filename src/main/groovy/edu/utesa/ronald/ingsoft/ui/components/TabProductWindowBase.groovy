package edu.utesa.ronald.ingsoft.ui.components

import com.vaadin.ui.Grid
import edu.utesa.ronald.ingsoft.components.abstracts.APanel
import edu.utesa.ronald.ingsoft.ui.components.forms.ProductToGridAction
import groovy.transform.CompileStatic

/**
 *  Created by ronald on03/25/17.
 */
@CompileStatic
class TabProductWindowBase extends APanel {

    private TabWindowBaseDesign tabWindowBaseDesign = new TabWindowBaseDesign()

    final ProductToGridAction productToGridAction = new ProductToGridAction()

    private boolean view = false

    TabProductWindowBase() {
        confLayout(tabWindowBaseDesign)
    }

    @Override
    protected void confLanguage() {
    }

    @Override
    protected void buildLayout() {
        tabWindowBaseDesign.pMenu.setContent(productToGridAction)
        disableMenuAction()
    }

    @Override
    protected void security() {
    }

    final Grid grid() {
        return tabWindowBaseDesign.gridData
    }

    void enableMenuAction() {
        if (!view) {
            productToGridAction.tfACSearch().setEnabled(true)
            productToGridAction.tfQuantity().setEnabled(true)
            productToGridAction.btnAdd().setEnabled(true)
            productToGridAction.btnDelete().setEnabled(true)
            productToGridAction.btnNew().setEnabled(true)
            productToGridAction.btnSearch().setEnabled(true)
        }
    }

    void disableMenuAction() {
        if (!view) {
            productToGridAction.tfACSearch().setEnabled(true)
            productToGridAction.tfQuantity().setEnabled(true)
            productToGridAction.btnAdd().setEnabled(true)
            productToGridAction.btnDelete().setEnabled(false)
            productToGridAction.btnNew().setEnabled(true)
            productToGridAction.btnSearch().setEnabled(true)
        }
    }

    void viewMenuAction() {
        productToGridAction.tfACSearch().setEnabled(false)
        productToGridAction.tfACSearch().setReadOnly(true)
        productToGridAction.tfQuantity().setEnabled(false)
        productToGridAction.tfQuantity().setReadOnly(true)
        productToGridAction.cbMonths().setEnabled(false)
        productToGridAction.cbMonths().setReadOnly(true)
        productToGridAction.btnAdd().setEnabled(false)
        productToGridAction.btnAdd().setReadOnly(true)
        productToGridAction.btnDelete().setEnabled(false)
        productToGridAction.btnDelete().setReadOnly(true)
        productToGridAction.btnNew().setEnabled(false)
        productToGridAction.btnNew().setReadOnly(true)
        productToGridAction.btnSearch().setEnabled(false)
        productToGridAction.btnSearch().setReadOnly(true)
        view = true
    }
}
