package edu.utesa.ronald.ingsoft.ui.components

import com.vaadin.ui.Grid
import com.vaadin.ui.themes.ValoTheme
import edu.utesa.ronald.ingsoft.components.abstracts.APanel
import edu.utesa.ronald.ingsoft.ui.components.forms.OneToManyAction
import groovy.transform.CompileStatic

/**
 *  Created by ronald on 8/1/17.
 */
@CompileStatic
class TabWindowBase extends APanel {

    private TabWindowBaseDesign tabWindowBaseDesign = new TabWindowBaseDesign()

    final OneToManyAction oneToManyAction = new OneToManyAction()

    private boolean view = false

    TabWindowBase() {
        confLayout(tabWindowBaseDesign)
    }

    @Override
    protected void confLanguage() {
        tabWindowBaseDesign.gridData.addStyleName(ValoTheme.TABLE_COMPACT)
        tabWindowBaseDesign.gridData.addStyleName(ValoTheme.TABLE_SMALL)
    }

    @Override
    protected void buildLayout() {
        tabWindowBaseDesign.pMenu.setContent(oneToManyAction)
        disableMenuAction()
    }

    @Override
    protected void security() {
    }

    final Grid grid() {
        return tabWindowBaseDesign.gridData
    }

    void enableMenuAction() {
        if (!view) {
            oneToManyAction.tfACSearch().setEnabled(true)
            oneToManyAction.btnAdd().setEnabled(true)
            oneToManyAction.btnDelete().setEnabled(true)
            oneToManyAction.btnNew().setEnabled(true)
            oneToManyAction.btnSearch().setEnabled(true)
        }
    }

    void disableMenuAction() {
        if (!view) {
            oneToManyAction.tfACSearch().setEnabled(true)
            oneToManyAction.btnAdd().setEnabled(true)
            oneToManyAction.btnDelete().setEnabled(false)
            oneToManyAction.btnNew().setEnabled(true)
            oneToManyAction.btnSearch().setEnabled(true)
        }
    }

    void viewMenuAction() {
        oneToManyAction.tfACSearch().setEnabled(false)
        oneToManyAction.tfACSearch().setReadOnly(true)
        oneToManyAction.btnAdd().setEnabled(false)
        oneToManyAction.btnAdd().setReadOnly(true)
        oneToManyAction.btnDelete().setEnabled(false)
        oneToManyAction.btnDelete().setReadOnly(true)
        oneToManyAction.btnNew().setEnabled(false)
        oneToManyAction.btnNew().setReadOnly(true)
        oneToManyAction.btnSearch().setEnabled(false)
        oneToManyAction.btnSearch().setReadOnly(true)
        view = true
    }
}
