package edu.utesa.ronald.ingsoft.ui.components

import com.kbdunn.vaadin.addons.fontawesome.FontAwesome
import com.vaadin.server.Page
import com.vaadin.server.StreamResource
import com.vaadin.server.VaadinServlet
import com.vaadin.server.VaadinSession
import com.vaadin.shared.ui.label.ContentMode
import com.vaadin.ui.Alignment
import com.vaadin.ui.Button
import com.vaadin.ui.MenuBar
import com.vaadin.ui.themes.ValoTheme
import edu.utesa.ronald.ingsoft.components.abstracts.APanel
import edu.utesa.ronald.ingsoft.models.constants.Styles
import edu.utesa.ronald.ingsoft.ui.utils.dialogs.ChangeLanguage
import edu.utesa.ronald.ingsoft.ui.utils.dialogs.utils.ChangePasswordWindow
import edu.utesa.ronald.ingsoft.utils.Constants
import edu.utesa.ronald.ingsoft.utils.ImageLoader
import edu.utesa.ronald.ingsoft.utils.ImageModel
import edu.utesa.ronald.ingsoft.utils.Languages
import groovy.transform.CompileStatic
import org.springframework.beans.factory.annotation.Value

/**
 *  Created by ronald on03/9/17.
 */
@CompileStatic
class TopBar extends APanel {

    @Value('${info.app.name}')
    private String APP_NAME

    private TopBarDesign topBarDesign = new TopBarDesign()

    private MenuBar.MenuItem mConfiguration
    private Button btnExit = new Button()
    private ContentPanel contentPanel

    TopBar(ContentPanel contentPanel) {
        this.contentPanel = contentPanel
        Constants.instance.autoWiredClass(this)
        confLayout(topBarDesign)
    }

    @Override
    protected void confLanguage() {
        topBarDesign.addStyleName(Styles.TOP_BAR)

        topBarDesign.menuUser.addStyleName(Styles.MENU_USER_SETTINGS)
        topBarDesign.menuUser.addStyleName(ValoTheme.MENUBAR_BORDERLESS)
        topBarDesign.btnMenu.addStyleName(ValoTheme.BUTTON_BORDERLESS)
        topBarDesign.btnMenu.setIcon(FontAwesome.NAVICON)

//        topBarDesign.lInformations.setCaption(Languages.instance.getText("info.welcome") + " " + loginManager.user.name)
        topBarDesign.lInformations.addStyleName(Styles.LABEL_INFORMATIONS)
        topBarDesign.lVersion.addStyleName(Styles.LABEL_SOFTWARE)
        topBarDesign.lVersion.setContentMode(ContentMode.HTML)
        topBarDesign.setComponentAlignment(topBarDesign.lVersion, Alignment.BOTTOM_CENTER)
        topBarDesign.lVersion.setValue(APP_NAME)
    }

    @Override
    protected void buildLayout() {
        topBarDesign.imgLogo.setSource(new StreamResource(new StreamResource.StreamSource() {
            @Override
            InputStream getStream() {
                return ImageLoader.instance.getImageStream(ImageModel.INGSOFT)
            }
        }, ImageModel.INGSOFT.toString() + ".png"))
        topBarDesign.imgLogo.setVisible(false)
        btnExit.setIcon(FontAwesome.SIGN_OUT)
        btnExit.addStyleName(Styles.TOP_BAR)
        btnExit.addClickListener(new Button.ClickListener() {
            @Override
            void buttonClick(Button.ClickEvent event) {
                try {
                    Page.getCurrent().setLocation(VaadinServlet.getCurrent().getServletContext().getContextPath())
                    VaadinSession.getCurrent().getSession().invalidate()
                } catch (Exception ignore) {
                }
            }
        })
        topBarDesign.pNotification.setVisible(false)
        topBarDesign.pNotification.setEnabled(false)
        profileConfigurationMenu()
        topBarDesign.addComponent(btnExit)
    }

    @Override
    protected void security() {
    }

    private void applyPermissionMenuItem(MenuBar.MenuItem menuItem, boolean canUse) {
        menuItem.setEnabled(canUse)
        menuItem.setVisible(canUse)
    }

    private void profileConfigurationMenu() {
        mConfiguration = topBarDesign.menuUser.addItem(Languages.instance.getText("menu.profile.welcome"), FontAwesome.GEAR, null)
        mConfiguration.addItem(Languages.instance.getText("menu.profile.button.language"), FontAwesome.LANGUAGE, new MenuBar.Command() {
            @Override
            void menuSelected(MenuBar.MenuItem menuItem) {
                ChangeLanguage changeLanguage = new ChangeLanguage()
                getUI().addWindow(changeLanguage)
            }
        })
        mConfiguration.addItem(Languages.instance.getText("menu.profile.button.change.password"), FontAwesome.EXCHANGE, new MenuBar.Command() {
            @Override
            void menuSelected(MenuBar.MenuItem menuItem) {
                ChangePasswordWindow changePasswordWindow = new ChangePasswordWindow()
                getUI().addWindow(changePasswordWindow)
            }
        })
        mConfiguration.addItem(Languages.instance.getText("menu.profile.button.logout"), FontAwesome.SIGN_OUT, new MenuBar.Command() {
            @Override
            void menuSelected(MenuBar.MenuItem menuItem) {
                try {
                    Page.getCurrent().setLocation(VaadinServlet.getCurrent().getServletContext().getContextPath())
                    VaadinSession.getCurrent().getSession().invalidate()
                } catch (Exception ignore) {
                }
            }
        })
    }

    Button btnMenu() {
        return topBarDesign.btnMenu
    }
}
