package edu.utesa.ronald.ingsoft.ui.components

import com.vaadin.ui.Button
import com.vaadin.ui.TabSheet
import com.vaadin.ui.themes.ValoTheme
import edu.utesa.ronald.ingsoft.components.TabSheetManager
import edu.utesa.ronald.ingsoft.components.abstracts.AWindowForm
import edu.utesa.ronald.ingsoft.models.constants.Styles
import edu.utesa.ronald.ingsoft.models.interfaces.OnNew
import edu.utesa.ronald.ingsoft.models.interfaces.WindowStartFocus
import edu.utesa.ronald.ingsoft.ui.components.forms.FormAction
import edu.utesa.ronald.ingsoft.utils.Languages
import edu.utesa.ronald.ingsoft.utils.ShowNotification
import groovy.transform.CompileStatic

/**
 *  Created by ronald on 7/08/17.
 */
@CompileStatic
abstract class WindowForm<T> extends AWindowForm<T> {

    private WindowFormDesign windowFormDesign = new WindowFormDesign()

    FormAction formAction = new FormAction()
    T itemSave

    final TabSheetManager tabSheetManager = new TabSheetManager(windowFormDesign.tsTabs)
    protected Boolean edit = null
    protected OnNew<T> onNew

    /**
     * Este caso cubre los casos simple de asignación.
     */
    protected WindowForm() {
        this(null, null)
    }

    protected WindowForm(T t, Boolean edit) {
        itemSave = t
        this.edit = edit
        addStyleName(Styles.APP_CONTROL_TABS)
        setModal(true)
        tabSheetManager.tabSheet().addStyleName(ValoTheme.TABSHEET_COMPACT_TABBAR)
        setContent(windowFormDesign)
        prepare()
    }

    @Override
    protected void prepare() {
        center()
        windowFormDesign.pMenu.setContent(formAction)
        formAction.btnSave().addClickListener(new Button.ClickListener() {
            @Override
            void buttonClick(Button.ClickEvent event) {
                if (getValidForm()) {
                    if (onSave()) {
                        ShowNotification.success()
                        if (onNew) {
                            onNew.result(itemSave)
                        }
                        close()
                    } else {
                        ShowNotification.error()
                    }
                } else {
                    ShowNotification.incomplete(Styles.NOTIFICATION_INCOMPLETE)
                }
            }
        })
        formAction.btnCancel().addClickListener(new Button.ClickListener() {
            @Override
            void buttonClick(Button.ClickEvent event) {
                close()
            }
        })
    }

    @Override
    protected void confLanguage() {
    }

    @Override
    protected void prepareTab(String captionLanguage) {
        if (edit == null) {
            onCreate()
            setCaption(Languages.instance.getText(captionLanguage + NEW_CAPTION))
        } else if (!edit) {
            onEditView()
            onViewTabs()
            formAction.btnSave().setEnabled(false)
            setCaption(Languages.instance.getText(captionLanguage + VIEW_CAPTION))
        } else {
            onEditView()
            setCaption(Languages.instance.getText(captionLanguage + EDIT_CAPTION))
        }
    }

    @Override
    protected void prepareTabStart() {
        if (isView()) {
            buildView()
        }
        windowFormDesign.tsTabs.addSelectedTabChangeListener(new TabSheet.SelectedTabChangeListener() {
            @Override
            void selectedTabChange(TabSheet.SelectedTabChangeEvent selectedTabChangeEvent) {
                initFocus()
                center()
            }
        })
        windowFormDesign.tsTabs.setSelectedTab(0)
        initFocus()
    }

    private void initFocus() {
        if (windowFormDesign.tsTabs.getSelectedTab() instanceof WindowStartFocus) {
            (windowFormDesign.tsTabs.getSelectedTab() as WindowStartFocus).startFocus()
        }
    }

    protected abstract boolean getValidForm()

    private boolean isView() {
        return edit != null & !edit
    }
}
