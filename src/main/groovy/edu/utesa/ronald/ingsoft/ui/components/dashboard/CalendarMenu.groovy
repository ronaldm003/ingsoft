package edu.utesa.ronald.ingsoft.ui.components.dashboard

import com.kbdunn.vaadin.addons.fontawesome.FontAwesome
import com.vaadin.ui.Button
import edu.utesa.ronald.ingsoft.components.abstracts.APanel
import edu.utesa.ronald.ingsoft.models.constants.Styles
import edu.utesa.ronald.ingsoft.utils.Languages
import groovy.transform.CompileStatic

/**
 *  Created by ronald on 7/26/17.
 */
@CompileStatic
class CalendarMenu extends APanel {

    private CalendarMenuDesign calendarMenuDesign = new CalendarMenuDesign()

    // Ahora debo separar el mennu informativo de arriba de este.
    // Pasa a un menu lateral y individual.

    CalendarMenu() {
        confLayout(calendarMenuDesign)
    }

    @Override
    protected void confLanguage() {
        calendarMenuDesign.btnPrev.setCaption(Languages.instance.getText("Prev"))
        calendarMenuDesign.btnPrev.setIcon(FontAwesome.BACKWARD)
        calendarMenuDesign.btnMonth.setCaption(Languages.instance.getText("Month"))
        calendarMenuDesign.btnMonth.addStyleName(Styles.CALENDAR_BUTTON)
        calendarMenuDesign.btnWeek.setCaption(Languages.instance.getText("Week"))
        calendarMenuDesign.btnWeek.addStyleName(Styles.CALENDAR_BUTTON)
        calendarMenuDesign.btnDay.setCaption(Languages.instance.getText("Day"))
        calendarMenuDesign.btnDay.addStyleName(Styles.CALENDAR_BUTTON)
        calendarMenuDesign.btnNext.setCaption(Languages.instance.getText("Next"))
        calendarMenuDesign.btnNext.setIcon(FontAwesome.FORWARD)
    }

    @Override
    protected void buildLayout() {
    }

    @Override
    protected void security() {
    }

    Button btnPrev() {
        return calendarMenuDesign.btnPrev
    }

    Button btnMonth() {
        return calendarMenuDesign.btnMonth
    }

    Button btnWeek() {
        return calendarMenuDesign.btnWeek
    }

    Button btnDay() {
        return calendarMenuDesign.btnDay
    }

    Button btnNext() {
        return calendarMenuDesign.btnNext
    }
}
