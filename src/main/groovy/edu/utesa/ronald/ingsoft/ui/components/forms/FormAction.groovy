package edu.utesa.ronald.ingsoft.ui.components.forms

import com.kbdunn.vaadin.addons.fontawesome.FontAwesome
import com.vaadin.event.ShortcutAction
import com.vaadin.ui.Button
import edu.utesa.ronald.ingsoft.components.abstracts.APanel
import edu.utesa.ronald.ingsoft.utils.Languages
import groovy.transform.CompileStatic

/**
 *  Created by ronald on 7/08/17.
 */
@CompileStatic
class FormAction extends APanel {

    private FormActionDesign formActionDesign = new FormActionDesign()

    FormAction() {
        confLayout(formActionDesign)
    }

    @Override
    protected void confLanguage() {
        formActionDesign.btnCancel.setCaption(Languages.instance.getText("window.action.cancel"))
        formActionDesign.btnCancel.setIcon(FontAwesome.UNDO)
        formActionDesign.btnSave.setCaption(Languages.instance.getText("window.action.save"))
        formActionDesign.btnSave.setIcon(FontAwesome.SAVE)
    }

    @Override
    protected void buildLayout() {
        formActionDesign.btnSave.setClickShortcut(ShortcutAction.KeyCode.ENTER)
    }

    @Override
    protected void security() {
    }

    final Button btnCancel() {
        return formActionDesign.btnCancel
    }

    final Button btnSave() {
        return formActionDesign.btnSave
    }
}
