package edu.utesa.ronald.ingsoft.ui.components.forms

import com.vaadin.event.ShortcutAction
import com.vaadin.server.FontAwesome
import com.vaadin.ui.Alignment
import com.vaadin.ui.Button
import com.vaadin.ui.HorizontalLayout
import com.vaadin.ui.Panel
import com.vaadin.ui.themes.ValoTheme
import edu.utesa.ronald.ingsoft.models.constants.Styles
import edu.utesa.ronald.ingsoft.utils.Languages
import groovy.transform.CompileStatic

/**
 *  Created by ronald on 7/25/2016.
 */
@CompileStatic
class FormEventAction extends Panel {

    private HorizontalLayout mainLayout

    private Button btnApplyEvent, btnCancelEvent, btnDeleteEvent

    FormEventAction() {
        setSizeFull()
        setHeight("80px")
        addStyleName(Styles.FORM_ACTION)

        mainLayout = new HorizontalLayout()
        mainLayout.setSizeFull()
        mainLayout.setSpacing(true)
        mainLayout.addStyleName(ValoTheme.WINDOW_BOTTOM_TOOLBAR)
        buildLayout()
        setContent(mainLayout)
    }

    private void buildLayout() {
        btnApplyEvent = new Button(Languages.instance.getText("window.action.save"))
        btnApplyEvent.addStyleName(ValoTheme.BUTTON_PRIMARY)
        btnApplyEvent.addStyleName(ValoTheme.BUTTON_TINY)
        btnApplyEvent.setClickShortcut(ShortcutAction.KeyCode.ENTER)

        btnCancelEvent = new Button(Languages.instance.getText("window.action.cancel"))
        btnCancelEvent.addStyleName(Styles.BUTTON_CANCEL)
        btnCancelEvent.addStyleName(ValoTheme.BUTTON_TINY)

        btnDeleteEvent = new Button(Languages.instance.getText("window.action.delete"))
        btnDeleteEvent.addStyleName(ValoTheme.BUTTON_BORDERLESS)
        btnDeleteEvent.addStyleName(ValoTheme.BUTTON_TINY)
        btnDeleteEvent.setIcon(FontAwesome.TRASH)

        mainLayout.addComponent(btnDeleteEvent)
        mainLayout.addComponent(btnCancelEvent)
        mainLayout.addComponent(btnApplyEvent)
        mainLayout.setComponentAlignment(btnDeleteEvent, Alignment.MIDDLE_LEFT)
        mainLayout.setComponentAlignment(btnCancelEvent, Alignment.MIDDLE_CENTER)
        mainLayout.setComponentAlignment(btnApplyEvent, Alignment.MIDDLE_RIGHT)
    }

    Button getBtnApplyEvent() {
        return btnApplyEvent
    }

    Button getBtnCancelEvent() {
        return btnCancelEvent
    }

    Button getBtnDeleteEvent() {
        return btnDeleteEvent
    }
}
