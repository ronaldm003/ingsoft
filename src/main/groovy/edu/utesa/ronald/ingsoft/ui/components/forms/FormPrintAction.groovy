package edu.utesa.ronald.ingsoft.ui.components.forms

import com.vaadin.ui.Button
import com.vaadin.ui.ComboBox
import com.vaadin.ui.DateField
import edu.utesa.ronald.ingsoft.components.abstracts.APanel
import edu.utesa.ronald.ingsoft.utils.Languages
import groovy.transform.CompileStatic

/**
 *  Created by ronald on03/4/17.
 */
@CompileStatic
class FormPrintAction extends APanel {

    private FormPrintActionDesign formPrintActionDesign = new FormPrintActionDesign()

    FormPrintAction() {
        confLayout(formPrintActionDesign)
    }

    @Override
    protected void confLanguage() {
        formPrintActionDesign.dfStart.setCaption(Languages.instance.getText("window.action.start.date"))
        formPrintActionDesign.dfEnd.setCaption(Languages.instance.getText("window.action.end.date"))
        formPrintActionDesign.btnDownload.setCaption(Languages.instance.getText("window.action.download"))
        formPrintActionDesign.btnExport.setCaption(Languages.instance.getText("window.action.export"))
        formPrintActionDesign.cbFormats.setCaption(Languages.instance.getText("window.action.format"))
    }

    @Override
    protected void buildLayout() {
    }

    @Override
    protected void security() {
    }

    Button btnExport() {
        return formPrintActionDesign.btnExport
    }

    Button btnDownload() {
        return formPrintActionDesign.btnDownload
    }

    DateField dfEnd() {
        return formPrintActionDesign.dfEnd
    }

    DateField dfStart() {
        return formPrintActionDesign.dfStart
    }

    ComboBox cbFormats() {
        return formPrintActionDesign.cbFormats
    }
}
