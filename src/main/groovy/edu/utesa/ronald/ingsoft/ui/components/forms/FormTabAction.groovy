package edu.utesa.ronald.ingsoft.ui.components.forms

import com.kbdunn.vaadin.addons.fontawesome.FontAwesome
import com.vaadin.data.Property
import com.vaadin.ui.Button
import com.vaadin.ui.CheckBox
import edu.utesa.ronald.ingsoft.components.abstracts.APanel
import edu.utesa.ronald.ingsoft.utils.Languages
import groovy.transform.CompileStatic

/**
 *  Created by ronald on 7/08/17.
 */
@CompileStatic
class FormTabAction extends APanel {

    private FormTabActionDesign formTabActionDesign = new FormTabActionDesign()

    FormTabAction() {
        confLayout(formTabActionDesign)
    }

    @Override
    protected void confLanguage() {
        formTabActionDesign.btnNew.setCaption(Languages.instance.getText("window.action.new"))
        formTabActionDesign.btnNew.setIcon(FontAwesome.PENCIL)
        formTabActionDesign.btnEdit.setCaption(Languages.instance.getText("window.action.edit"))
        formTabActionDesign.btnEdit.setIcon(FontAwesome.EDIT)
        formTabActionDesign.btnView.setCaption(Languages.instance.getText("window.action.view"))
        formTabActionDesign.btnView.setIcon(FontAwesome.BELL)
        formTabActionDesign.btnDelete.setCaption(Languages.instance.getText("window.action.delete"))
        formTabActionDesign.btnDelete.setIcon(FontAwesome.TRASH_O)
        formTabActionDesign.chkShowDelete.setCaption(Languages.instance.getText("window.action.show.delete"))
    }

    @Override
    protected void buildLayout() {
        formTabActionDesign.chkShowDelete.addValueChangeListener(new Property.ValueChangeListener() {
            @Override
            void valueChange(Property.ValueChangeEvent event) {
                if (formTabActionDesign.chkShowDelete.getValue()) {
                    formTabActionDesign.btnDelete.setCaption(Languages.instance.getText("window.action.restore"))
                } else {
                    formTabActionDesign.btnDelete.setCaption(Languages.instance.getText("window.action.delete"))
                }
            }
        })
    }

    @Override
    protected void security() {
    }

    void applyPermissionBtnNew(boolean canUse) {
        applyPermission(formTabActionDesign.btnNew, canUse)
    }

    void applyPermissionBtnEdit(boolean canUse) {
        applyPermission(formTabActionDesign.btnEdit, canUse)
    }

    void applyPermissionBtnView(boolean canUse) {
        applyPermission(formTabActionDesign.btnView, canUse)
    }

    void applyPermissionBtnDelete(boolean canUse) {
        applyPermission(formTabActionDesign.btnDelete, canUse)
    }

    void applyPermissionchkShowDelete(boolean canUse) {
        applyPermission(formTabActionDesign.chkShowDelete, canUse)
    }

    final Button btnNew() {
        return formTabActionDesign.btnNew
    }

    final Button btnEdit() {
        return formTabActionDesign.btnEdit
    }

    final Button btnView() {
        return formTabActionDesign.btnView
    }

    final Button btnDelete() {
        return formTabActionDesign.btnDelete
    }

    final CheckBox chkShowDelete() {
        return formTabActionDesign.chkShowDelete
    }
}
