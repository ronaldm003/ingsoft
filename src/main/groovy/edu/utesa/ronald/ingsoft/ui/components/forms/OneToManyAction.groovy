package edu.utesa.ronald.ingsoft.ui.components.forms

import com.kbdunn.vaadin.addons.fontawesome.FontAwesome
import com.vaadin.event.ShortcutAction
import com.vaadin.ui.Button
import com.vaadin.ui.themes.ValoTheme
import edu.utesa.ronald.ingsoft.components.abstracts.APanel
import eu.maxschuster.vaadin.autocompletetextfield.AutocompleteTextField
import groovy.transform.CompileStatic

/**
 *  Created by ronald on 7/08/17.
 */
@CompileStatic
class OneToManyAction extends APanel {

    private OneToManyActionDesign oneToManyActionDesign = new OneToManyActionDesign()

    private boolean required

    private AutocompleteTextField tfACSearch = new AutocompleteTextField()

    OneToManyAction() {
        this(false)
    }

    OneToManyAction(boolean required) {
        this.required = required
        confLayout(oneToManyActionDesign)
    }

    @Override
    protected void confLanguage() {
    }

    @Override
    protected void buildLayout() {
        oneToManyActionDesign.removeComponent(oneToManyActionDesign.getComponent(0))
        tfACSearch.withWidthFull()
        oneToManyActionDesign.addComponent(tfACSearch, 0)
        oneToManyActionDesign.setExpandRatio(tfACSearch, 1f)

        oneToManyActionDesign.btnAdd.setIcon(FontAwesome.ARROW_DOWN)
        oneToManyActionDesign.btnAdd.addStyleName(ValoTheme.BUTTON_TINY)
        oneToManyActionDesign.btnNew.addStyleName(ValoTheme.BUTTON_FRIENDLY)
        oneToManyActionDesign.btnNew.addStyleName(ValoTheme.BUTTON_TINY)
        oneToManyActionDesign.btnNew.setIcon(FontAwesome.PLUS_CIRCLE)
        oneToManyActionDesign.btnDelete.addStyleName(ValoTheme.BUTTON_DANGER)
        oneToManyActionDesign.btnDelete.setIcon(FontAwesome.TRASH)
        oneToManyActionDesign.btnDelete.addStyleName(ValoTheme.BUTTON_TINY)
        oneToManyActionDesign.btnSearch.setIcon(FontAwesome.SEARCH)
        oneToManyActionDesign.btnSearch.addStyleName(ValoTheme.BUTTON_TINY)
        oneToManyActionDesign.btnAdd.setClickShortcut(ShortcutAction.KeyCode.TAB)

        tfACSearch.addStyleName(ValoTheme.TEXTFIELD_TINY)
        tfACSearch.setRequired(required)
    }

    @Override
    protected void security() {
    }

    final AutocompleteTextField tfACSearch() {
        return tfACSearch
    }

    final Button btnAdd() {
        return oneToManyActionDesign.btnAdd
    }

    final Button btnDelete() {
        return oneToManyActionDesign.btnDelete
    }

    final Button btnNew() {
        return oneToManyActionDesign.btnNew
    }

    final Button btnSearch() {
        return oneToManyActionDesign.btnSearch
    }

    void select(String dataSelect) {
        tfACSearch.setValue(dataSelect)
    }

    void visualize() {
        tfACSearch.setReadOnly(true)
        oneToManyActionDesign.btnAdd.setReadOnly(true)
        oneToManyActionDesign.btnDelete.setReadOnly(true)
        oneToManyActionDesign.btnNew.setReadOnly(true)
        oneToManyActionDesign.btnSearch.setReadOnly(true)
    }

    boolean isValid() {
        if (required) {
            return tfACSearch.isValid()
        }
        return true
    }

    void focusValid() {
        tfACSearch.focus()
    }
}
