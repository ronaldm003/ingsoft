package edu.utesa.ronald.ingsoft.ui.components.forms

import com.kbdunn.vaadin.addons.fontawesome.FontAwesome
import com.vaadin.ui.Button
import com.vaadin.ui.themes.ValoTheme
import edu.utesa.ronald.ingsoft.components.abstracts.APanel
import eu.maxschuster.vaadin.autocompletetextfield.AutocompleteTextField
import groovy.transform.CompileStatic

/**
 *  Created by ronald on 7/08/17.
 */
@CompileStatic
class OneToOneAction extends APanel {

    private OneToOneActionDesign oneToOneActionDesign = new OneToOneActionDesign()

    private AutocompleteTextField tfACSearch = new AutocompleteTextField()

    private boolean required

    OneToOneAction(boolean required) {
        this.required = required
        confLayout(oneToOneActionDesign)
    }

    @Override
    protected void confLanguage() {
        tfACSearch.addStyleName(ValoTheme.TEXTFIELD_TINY)
        oneToOneActionDesign.btnNew.addStyleName(ValoTheme.BUTTON_FRIENDLY)
        oneToOneActionDesign.btnNew.addStyleName(ValoTheme.BUTTON_TINY)
        oneToOneActionDesign.btnNew.setIcon(FontAwesome.PLUS_CIRCLE)
        oneToOneActionDesign.btnNew.setIcon(FontAwesome.PLUS_CIRCLE)
        oneToOneActionDesign.btnSearch.setIcon(FontAwesome.SEARCH)
        oneToOneActionDesign.btnSearch.addStyleName(ValoTheme.BUTTON_TINY)
    }

    @Override
    protected void buildLayout() {
        oneToOneActionDesign.removeComponent(oneToOneActionDesign.getComponent(0))
        tfACSearch.withWidthFull()
        tfACSearch.setNullRepresentation("")
        oneToOneActionDesign.addComponent(tfACSearch, 0)
        oneToOneActionDesign.setExpandRatio(tfACSearch, 1f)

        tfACSearch.setRequired(required)
        tfACSearch.setMinChars(3)
    }

    @Override
    protected void security() {
    }

    final AutocompleteTextField tfACSearch() {
        return tfACSearch
    }

    final Button btnNew() {
        return oneToOneActionDesign.btnNew
    }

    final Button btnSearch() {
        return oneToOneActionDesign.btnSearch
    }

    void select(String dataSelect) {
        tfACSearch.setValue(dataSelect)
    }

    void visualize() {
        tfACSearch.setReadOnly(true)
        oneToOneActionDesign.btnNew.setReadOnly(true)
        oneToOneActionDesign.btnNew.setEnabled(false)
        oneToOneActionDesign.btnSearch.setReadOnly(true)
        oneToOneActionDesign.btnSearch.setEnabled(false)
    }

    boolean isValid() {
        if (required) {
            return tfACSearch.isValid()
        }
        return true
    }

    void focusValid() {
        tfACSearch.focus()
    }
}
