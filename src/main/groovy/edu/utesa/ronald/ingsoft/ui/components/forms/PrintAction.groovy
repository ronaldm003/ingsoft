package edu.utesa.ronald.ingsoft.ui.components.forms

import com.kbdunn.vaadin.addons.fontawesome.FontAwesome
import com.vaadin.event.ShortcutAction
import com.vaadin.ui.Button
import edu.utesa.ronald.ingsoft.components.abstracts.APanel
import edu.utesa.ronald.ingsoft.models.interfaces.PrintElement
import edu.utesa.ronald.ingsoft.utils.Languages
import groovy.transform.CompileStatic

/**
 * https://vaadin.com/docs/-/part/framework/advanced/advanced-printing.html
 * https://dev.vaadin.com/svn/doc/book-examples/branches/vaadin-7/src/com/vaadin/book/examples/advanced/PrintingExample.java
 *
 *  Created by ronald on 9/19/17.
 */
@CompileStatic
class PrintAction extends APanel {

    private PrintActionDesign printActionDesign = new PrintActionDesign()

    private PrintElement printElement

    PrintAction(PrintElement printElement) {
        this.printElement = printElement
        confLayout(printActionDesign)
    }

    @Override
    protected void confLanguage() {
        printActionDesign.btnPrint.setCaption(Languages.instance.getText("window.action.print"))
        printActionDesign.btnPrint.setIcon(FontAwesome.PRINT)
        printActionDesign.btnDownload.setCaption(Languages.instance.getText("window.action.download"))
        printActionDesign.btnDownload.setIcon(FontAwesome.DOWNLOAD)
        printActionDesign.btnSend.setCaption(Languages.instance.getText("window.action.send"))
        printActionDesign.btnSend.setIcon(FontAwesome.SEND)
        printActionDesign.btnClose.setCaption(Languages.instance.getText("window.action.close"))
        printActionDesign.btnClose.setIcon(FontAwesome.CLOSE)
    }

    @Override
    protected void buildLayout() {
        printActionDesign.btnClose.setClickShortcut(ShortcutAction.KeyCode.ENTER)
        // Me faltan los shortcuts
        printActionDesign.btnPrint.addClickListener(new Button.ClickListener() {
            @Override
            void buttonClick(Button.ClickEvent event) {

            }
        })
        printActionDesign.btnDownload.addClickListener(new Button.ClickListener() {
            @Override
            void buttonClick(Button.ClickEvent event) {
                // Como rayos hago esto? creo que se elimina por el caso
            }
        })
        printActionDesign.btnSend.addClickListener(new Button.ClickListener() {
            @Override
            void buttonClick(Button.ClickEvent event) {
                // Envio la foto.
                // Esto se supone que es un anexo en el correo.
                // Como lo guardo?
            }
        })
    }

    @Override
    protected void security() {
    }

    final Button btnPrint() {
        return printActionDesign.btnPrint
    }

    final Button btnDownload() {
        return printActionDesign.btnDownload
    }

    final Button btnSend() {
        return printActionDesign.btnSend
    }

    final Button btnClose() {
        return printActionDesign.btnClose
    }
}
