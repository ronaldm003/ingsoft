package edu.utesa.ronald.ingsoft.ui.components.forms

import com.kbdunn.vaadin.addons.fontawesome.FontAwesome
import com.vaadin.event.ShortcutAction
import com.vaadin.ui.Button
import com.vaadin.ui.ComboBox
import com.vaadin.ui.TextField
import com.vaadin.ui.themes.ValoTheme
import edu.utesa.ronald.ingsoft.components.abstracts.APanel
import edu.utesa.ronald.ingsoft.utils.Languages
import eu.maxschuster.vaadin.autocompletetextfield.AutocompleteTextField
import groovy.transform.CompileStatic

/**
 *  Created by ronald on03/20/17.
 */
@CompileStatic
class ProductToGridAction extends APanel {

    private ProductToGridActionDesign productToGridActionDesign = new ProductToGridActionDesign()

    private AutocompleteTextField tfACSearch = new AutocompleteTextField()

    ProductToGridAction() {
        confLayout(productToGridActionDesign)
    }

    @Override
    protected void confLanguage() {
        tfACSearch().setCaption(Languages.instance.getText("window.item"))
        productToGridActionDesign.tfQuantity.setCaption(Languages.instance.getText("window.quantity"))
        productToGridActionDesign.cbMonths.setCaption(Languages.instance.getText("window.months"))
    }

    @Override
    protected void buildLayout() {
        productToGridActionDesign.removeComponent(productToGridActionDesign.getComponent(0))
        tfACSearch.withWidthFull()
        productToGridActionDesign.addComponent(tfACSearch, 0)
        productToGridActionDesign.setExpandRatio(tfACSearch, 1f)
        productToGridActionDesign.btnAdd.setIcon(FontAwesome.ARROW_DOWN)
        productToGridActionDesign.btnNew.setVisible(false)
        productToGridActionDesign.btnNew.setEnabled(false)
        productToGridActionDesign.btnNew.addStyleName(ValoTheme.BUTTON_FRIENDLY)
        productToGridActionDesign.btnNew.setIcon(FontAwesome.PLUS_CIRCLE)
        productToGridActionDesign.btnDelete.addStyleName(ValoTheme.BUTTON_DANGER)
        productToGridActionDesign.btnDelete.setIcon(FontAwesome.TRASH)
        productToGridActionDesign.btnSearch.setIcon(FontAwesome.SEARCH)
        productToGridActionDesign.btnAdd.setClickShortcut(ShortcutAction.KeyCode.TAB)
        fillMonths()
    }

    @Override
    protected void security() {
    }

    final AutocompleteTextField tfACSearch() {
        return tfACSearch
    }

    final Button btnSearch() {
        return productToGridActionDesign.btnSearch
    }

    final TextField tfQuantity() {
        return productToGridActionDesign.tfQuantity
    }

    final ComboBox cbMonths() {
        return productToGridActionDesign.cbMonths
    }

    final Button btnAdd() {
        return productToGridActionDesign.btnAdd
    }

    final Button btnNew() {
        return productToGridActionDesign.btnNew
    }

    final Button btnDelete() {
        return productToGridActionDesign.btnDelete
    }

    void select(String dataSelect) {
        tfACSearch().setValue(dataSelect)
    }

    void clearPanel() {
        tfACSearch().setValue("")
        productToGridActionDesign.tfQuantity.setValue("")
    }

    void visualize() {
        tfACSearch().setReadOnly(true)
        productToGridActionDesign.btnSearch.setReadOnly(true)
        productToGridActionDesign.tfQuantity.setReadOnly(true)
        productToGridActionDesign.cbMonths.setReadOnly(true)
        productToGridActionDesign.btnAdd.setReadOnly(true)
        productToGridActionDesign.btnNew.setReadOnly(true)
        productToGridActionDesign.btnDelete.setReadOnly(true)
    }

    void fillMonths() {
        productToGridActionDesign.cbMonths.removeAllItems()
        for (int i = 1; i <= 12; i++) {
            productToGridActionDesign.cbMonths.addItem(i)
        }
    }

    void focusValid() {
        tfACSearch().focus()
    }
}
