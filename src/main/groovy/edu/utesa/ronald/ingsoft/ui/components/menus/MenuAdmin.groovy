package edu.utesa.ronald.ingsoft.ui.components.menus

import com.kbdunn.vaadin.addons.fontawesome.FontAwesome
import com.vaadin.ui.Button
import com.vaadin.ui.Component
import edu.utesa.ronald.ingsoft.components.abstracts.AMenu
import edu.utesa.ronald.ingsoft.models.interfaces.OnClick
import edu.utesa.ronald.ingsoft.ui.admin.*
import edu.utesa.ronald.ingsoft.ui.components.ContentPanel
import edu.utesa.ronald.ingsoft.utils.Languages
import edu.utesa.ronald.ingsoft.utils.MenuCaption
import edu.utesa.ronald.ingsoft.utils.WindowsCaption
import groovy.transform.CompileStatic

/**
 *  Created by ronald on 7/11/17.
 */
@CompileStatic
class MenuAdmin extends AMenu {

    private Button btnCycle
    private Button btnObjective
    private Button btnStudent
    private Button btnSubject
    private Button btnTeacher

    MenuAdmin(ContentPanel contentPanel) {
        super(contentPanel)
        setCaption(Languages.instance.getText(MenuCaption.MENU_ADMIN))
        setIcon(FontAwesome.ADJUST)
        setSpacing(true)
    }

    @Override
    protected void buildLayout() {
        btnCycle = createButton(Languages.instance.getText(WindowsCaption.CYCLE_CAPTION), FontAwesome.USER_O, new OnClick() {
            @Override
            Component onClick() {
                return new CycleTab()
            }
        })
        btnObjective = createButton(Languages.instance.getText(WindowsCaption.OBJECTIVE_CAPTION), FontAwesome.ADDRESS_BOOK, new OnClick() {
            @Override
            Component onClick() {
                return new ObjectiveTab()
            }
        })
        btnStudent = createButton(Languages.instance.getText(WindowsCaption.STUDENT_CAPTION), FontAwesome.ADDRESS_BOOK, new OnClick() {
            @Override
            Component onClick() {
                return new StudentTab()
            }
        })
        btnSubject = createButton(Languages.instance.getText(WindowsCaption.SUBJECT_CAPTION), FontAwesome.ADDRESS_BOOK, new OnClick() {
            @Override
            Component onClick() {
                return new SubjectTab()
            }
        })
        btnTeacher = createButton(Languages.instance.getText(WindowsCaption.TEACHER_CAPTION), FontAwesome.ADDRESS_BOOK, new OnClick() {
            @Override
            Component onClick() {
                return new TeacherTab()
            }
        })
        addComponent(btnCycle)
        addComponent(btnObjective)
        addComponent(btnStudent)
        addComponent(btnSubject)
        addComponent(btnTeacher)
    }

    @Override
    protected void security() {
    }

    @Override
    protected String captionContants(String caption) {
        switch (caption) {
            case btnCycle.getCaption():
                return WindowsCaption.CYCLE_CAPTION
            case btnObjective.getCaption():
                return WindowsCaption.OBJECTIVE_CAPTION
            case btnStudent.getCaption():
                return WindowsCaption.STUDENT_CAPTION
            case btnSubject.getCaption():
                return WindowsCaption.SUBJECT_CAPTION
            case btnTeacher.getCaption():
                return WindowsCaption.TEACHER_CAPTION
        }
        return ""
    }
}
