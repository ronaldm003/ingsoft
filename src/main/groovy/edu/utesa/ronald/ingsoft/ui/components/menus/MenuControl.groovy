package edu.utesa.ronald.ingsoft.ui.components.menus

import com.kbdunn.vaadin.addons.fontawesome.FontAwesome
import com.vaadin.ui.Button
import com.vaadin.ui.Component
import edu.utesa.ronald.ingsoft.components.abstracts.AMenu
import edu.utesa.ronald.ingsoft.models.interfaces.OnClick
import edu.utesa.ronald.ingsoft.ui.components.ContentPanel
import edu.utesa.ronald.ingsoft.ui.control.ArticleTab
import edu.utesa.ronald.ingsoft.ui.control.EquipmentTab
import edu.utesa.ronald.ingsoft.ui.control.InventoryTab
import edu.utesa.ronald.ingsoft.ui.control.PatientTab
import edu.utesa.ronald.ingsoft.utils.Languages
import edu.utesa.ronald.ingsoft.utils.MenuCaption
import edu.utesa.ronald.ingsoft.utils.WindowsCaption
import groovy.transform.CompileStatic

/**
 *  Created by ronald on 7/11/17.
 */
@CompileStatic
class MenuControl extends AMenu {

    private Button btnPatient
    private Button btnArticle
    private Button btnEquipment
    private Button btnInventory

    MenuControl(ContentPanel contentPanel) {
        super(contentPanel)
        setCaption(Languages.instance.getText(MenuCaption.MENU_CONTROL))
        setIcon(FontAwesome.ASTERISK)
        setSpacing(true)
    }

    @Override
    protected void buildLayout() {
        btnPatient = createButton(Languages.instance.getText(WindowsCaption.PATIENT_CAPTION), FontAwesome.FILE, new OnClick() {
            @Override
            Component onClick() {
                return new PatientTab()
            }
        })
        btnArticle = createButton(Languages.instance.getText(WindowsCaption.ARTICLE_CAPTION), FontAwesome.SERVER, new OnClick() {
            @Override
            Component onClick() {
                return new ArticleTab()
            }
        })
        btnEquipment = createButton(Languages.instance.getText(WindowsCaption.EQUIPMENT_CAPTION), FontAwesome.TABLET, new OnClick() {
            @Override
            Component onClick() {
                return new EquipmentTab()
            }
        })
        btnInventory = createButton(Languages.instance.getText(WindowsCaption.INVENTORY_CAPTION), FontAwesome.EMPIRE, new OnClick() {
            @Override
            Component onClick() {
                return new InventoryTab()
            }
        })
        addComponent(btnPatient)
        addComponent(btnArticle)
        addComponent(btnEquipment)
        addComponent(btnInventory)
    }

    @Override
    protected void security() {
    }

    @Override
    protected String captionContants(String caption) {
        switch (caption) {
            case btnPatient.getCaption():
                return WindowsCaption.PATIENT_CAPTION
            case btnArticle.getCaption():
                return WindowsCaption.ARTICLE_CAPTION
            case btnEquipment.getCaption():
                return WindowsCaption.EQUIPMENT_CAPTION
            case btnInventory.getCaption():
                return WindowsCaption.INVENTORY_CAPTION
        }
        return ""
    }
}
