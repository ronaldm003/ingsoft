package edu.utesa.ronald.ingsoft.ui.components.menus

import com.kbdunn.vaadin.addons.fontawesome.FontAwesome
import com.vaadin.ui.Button
import com.vaadin.ui.Component
import edu.utesa.ronald.ingsoft.components.abstracts.AMenu
import edu.utesa.ronald.ingsoft.models.interfaces.OnClick
import edu.utesa.ronald.ingsoft.ui.components.ContentPanel
import edu.utesa.ronald.ingsoft.ui.security.UserTab
import edu.utesa.ronald.ingsoft.utils.Languages
import edu.utesa.ronald.ingsoft.utils.MenuCaption
import edu.utesa.ronald.ingsoft.utils.WindowsCaption
import groovy.transform.CompileStatic

/**
 *  Created by ronald on 7/11/17.
 */
@CompileStatic
class MenuControlUser extends AMenu {

    private Button btnConsults
    private Button btnProfile
    private Button btnUser

    MenuControlUser(ContentPanel contentPanel) {
        super(contentPanel)
        setCaption(Languages.instance.getText(MenuCaption.MENU_CONTROL))
        setIcon(FontAwesome.ASTERISK)
        setSpacing(true)
    }

    @Override
    protected void buildLayout() {
//        btnProfile = createButton(Languages.instance.getText(WindowsCaption.PROFILE_CAPTION), FontAwesome.USER_SECRET, new ProfileTab())
//        btnConsults = createButton(Languages.instance.getText(WindowsCaption.CONSULTS_CAPTION), FontAwesome.MAP, new ConsultsTab())
        btnUser = createButton(Languages.instance.getText(WindowsCaption.USERS_CAPTION), FontAwesome.USER_O, new OnClick() {
            @Override
            Component onClick() {
                return new UserTab()
            }
        })

//        addComponent(btnProfile)
//        addComponent(btnConsults)
        addComponent(btnUser)
    }

    @Override
    protected void security() {
    }

    @Override
    protected String captionContants(String caption) {
        switch (caption) {
            case btnConsults.getCaption():
                return WindowsCaption.CONSULTS_CAPTION
            case btnProfile.getCaption():
                return WindowsCaption.PROFILE_CAPTION
            case btnUser.getCaption():
                return WindowsCaption.USERS_CAPTION
        }
        return ""
    }
}
