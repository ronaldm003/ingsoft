package edu.utesa.ronald.ingsoft.ui.components.menus

import com.kbdunn.vaadin.addons.fontawesome.FontAwesome
import com.vaadin.icons.VaadinIcons
import com.vaadin.ui.Button
import com.vaadin.ui.Component
import edu.utesa.ronald.ingsoft.components.abstracts.AMenu
import edu.utesa.ronald.ingsoft.models.interfaces.OnClick
import edu.utesa.ronald.ingsoft.ui.components.ContentPanel
import edu.utesa.ronald.ingsoft.ui.control.recordcard.RecordCardTab
import edu.utesa.ronald.ingsoft.ui.dental.ConsultsTab
import edu.utesa.ronald.ingsoft.ui.dental.PatientObjectiveAssingTab
import edu.utesa.ronald.ingsoft.ui.dental.PatientObjectiveAssingWindow
import edu.utesa.ronald.ingsoft.utils.Languages
import edu.utesa.ronald.ingsoft.utils.MenuCaption
import edu.utesa.ronald.ingsoft.utils.WindowsCaption
import groovy.transform.CompileStatic

/**
 *  Created by ronald on 7/11/17.
 */
@CompileStatic
class MenuDental extends AMenu {

    private Button btnRecordCard
    private Button btnAssing
    private Button btnAssingTab
    private Button btnConsults


    MenuDental(ContentPanel contentPanel) {
        super(contentPanel)
        setCaption(Languages.instance.getText(MenuCaption.MENU_DENTAL))
        setIcon(VaadinIcons.DENTAL_CHAIR)
        setSpacing(true)
    }

    @Override
    protected void buildLayout() {
        btnRecordCard = createButton(Languages.instance.getText(WindowsCaption.RECORD_CARD_CAPTION), FontAwesome.PAPERCLIP, new OnClick() {
            @Override
            Component onClick() {
                return new RecordCardTab()
            }
        })
        btnAssing = createButton(Languages.instance.getText(WindowsCaption.ASSIGN_CAPTION), FontAwesome.CODE_FORK, new OnClick() {
            @Override
            Component onClick() {
                return new PatientObjectiveAssingWindow()
            }
        })
        btnAssingTab = createButton(Languages.instance.getText(WindowsCaption.ASSIGN_TAB_CAPTION), FontAwesome.CODE_FORK, new OnClick() {
            @Override
            Component onClick() {
                return new PatientObjectiveAssingTab()
            }
        })
        btnConsults = createButton(Languages.instance.getText(WindowsCaption.CONSULTS_CAPTION), FontAwesome.SEARCH, new OnClick() {
            @Override
            Component onClick() {
                return new ConsultsTab()
            }
        })
        addComponent(btnConsults)
        addComponent(btnAssing)
        addComponent(btnAssingTab)
        addComponent(btnRecordCard)

    }

    @Override
    protected void security() {
    }

    @Override
    protected String captionContants(String caption) {
        switch (caption) {
            case btnConsults.getCaption():
                return WindowsCaption.CONSULTS_CAPTION
            case btnRecordCard.getCaption():
                return WindowsCaption.RECORD_CARD_CAPTION
            case btnAssing.getCaption():
                return WindowsCaption.ASSIGN_CAPTION
            case btnAssingTab.getCaption():
                return WindowsCaption.ASSIGN_TAB_CAPTION
        }
        return ""
    }
}
