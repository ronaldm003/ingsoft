package edu.utesa.ronald.ingsoft.ui.components.menus

import com.kbdunn.vaadin.addons.fontawesome.FontAwesome
import com.vaadin.ui.Button
import com.vaadin.ui.Component
import edu.utesa.ronald.ingsoft.components.abstracts.AMenu
import edu.utesa.ronald.ingsoft.models.interfaces.OnClick
import edu.utesa.ronald.ingsoft.ui.components.ContentPanel
import edu.utesa.ronald.ingsoft.ui.location.ClinicTab
import edu.utesa.ronald.ingsoft.ui.location.LocationTab
import edu.utesa.ronald.ingsoft.utils.Languages
import edu.utesa.ronald.ingsoft.utils.MenuCaption
import edu.utesa.ronald.ingsoft.utils.WindowsCaption
import groovy.transform.CompileStatic

/**
 *  Created by ronald on 7/11/17.
 */
@CompileStatic
class MenuLocation extends AMenu {

    private Button btnLocation
    private Button btnClinic

    MenuLocation(ContentPanel contentPanel) {
        super(contentPanel)
        setCaption(Languages.instance.getText(MenuCaption.MENU_LOCATION))
        setIcon(FontAwesome.SHIELD)
        setSpacing(true)
    }

    @Override
    protected void buildLayout() {
        btnLocation = createButton(Languages.instance.getText(WindowsCaption.LOCATION_CAPTION), FontAwesome.ADDRESS_BOOK, new OnClick() {
            @Override
            Component onClick() {
                return new LocationTab()
            }
        })
        btnClinic = createButton(Languages.instance.getText(WindowsCaption.CLINIC_CAPTION), FontAwesome.BUILDING_O, new OnClick() {
            @Override
            Component onClick() {
                return new ClinicTab()
            }
        })
        addComponent(btnLocation)
        addComponent(btnClinic)
    }

    @Override
    protected void security() {
    }

    @Override
    protected String captionContants(String caption) {
        switch (caption) {
            case btnLocation.getCaption():
                return WindowsCaption.LOCATION_CAPTION
            case btnClinic.getCaption():
                return WindowsCaption.CLINIC_CAPTION
        }
        return ""
    }
}
