package edu.utesa.ronald.ingsoft.ui.components.menus

import com.kbdunn.vaadin.addons.fontawesome.FontAwesome
import com.vaadin.ui.Button
import com.vaadin.ui.Component
import edu.utesa.ronald.ingsoft.components.abstracts.AMenu
import edu.utesa.ronald.ingsoft.models.interfaces.OnClick
import edu.utesa.ronald.ingsoft.ui.components.ContentPanel
import edu.utesa.ronald.ingsoft.utils.Languages
import edu.utesa.ronald.ingsoft.utils.MenuCaption
import edu.utesa.ronald.ingsoft.utils.WindowsCaption
import groovy.transform.CompileStatic

/**
 *  Created by ronald on 7/11/17.
 */
@CompileStatic
class MenuReport extends AMenu {

    private Button btnPrinted

    MenuReport(ContentPanel contentPanel) {
        super(contentPanel)
        setCaption(Languages.instance.getText(MenuCaption.MENU_REPORTS))
        setIcon(FontAwesome.FILE_PDF_O)
    }

    @Override
    protected void buildLayout() {
        btnPrinted = createButton(Languages.instance.getText(WindowsCaption.REPORT_PRINTED_CAPTION), FontAwesome.CREDIT_CARD, new OnClick() {
            @Override
            Component onClick() {
                return null
            }
        })
    }

    @Override
    protected void security() {
        addComponent(btnPrinted)
    }

    @Override
    protected String captionContants(String caption) {
        switch (caption) {
            case btnPrinted.getCaption():
                return WindowsCaption.REPORT_PRINTED_CAPTION
        }
        return ""
    }
}
