package edu.utesa.ronald.ingsoft.ui.components.menus

import com.kbdunn.vaadin.addons.fontawesome.FontAwesome
import com.vaadin.ui.Button
import com.vaadin.ui.Component
import edu.utesa.ronald.ingsoft.components.abstracts.AMenu
import edu.utesa.ronald.ingsoft.models.interfaces.OnClick
import edu.utesa.ronald.ingsoft.ui.components.ContentPanel
import edu.utesa.ronald.ingsoft.ui.security.*
import edu.utesa.ronald.ingsoft.utils.Languages
import edu.utesa.ronald.ingsoft.utils.MenuCaption
import edu.utesa.ronald.ingsoft.utils.WindowsCaption
import groovy.transform.CompileStatic

/**
 *  Created by ronald on 7/11/17.
 */
@CompileStatic
class MenuSecurity extends AMenu {

    private Button btnUser
    private Button btnPerson
    private Button btnParams
    private Button btnPermission
    private Button btnPermissionGroup

    MenuSecurity(ContentPanel contentPanel) {
        super(contentPanel)
        setCaption(Languages.instance.getText(MenuCaption.MENU_SECURITY))
        setIcon(FontAwesome.SHIELD)
        setSpacing(true)
    }

    @Override
    protected void buildLayout() {
        btnUser = createButton(Languages.instance.getText(WindowsCaption.USERS_CAPTION), FontAwesome.USER_O, new OnClick() {
            @Override
            Component onClick() {
                return new UserTab()
            }
        })
        btnPerson = createButton(Languages.instance.getText(WindowsCaption.PERSON_CAPTION), FontAwesome.USER_MD, new OnClick() {
            @Override
            Component onClick() {
                return new PersonTab()
            }
        })
        btnParams = createButton(Languages.instance.getText(WindowsCaption.PARAMS_CAPTION), FontAwesome.ADDRESS_BOOK, new OnClick() {
            @Override
            Component onClick() {
                return new ParamTab()
            }
        })
        btnPermission = createButton(Languages.instance.getText(WindowsCaption.PERMISSIONS_CAPTION), FontAwesome.ADDRESS_BOOK, new OnClick() {
            @Override
            Component onClick() {
                return new PermissionTab()
            }
        })
        btnPermissionGroup = createButton(Languages.instance.getText(WindowsCaption.PERMISSIONS_GROUP_CAPTION), FontAwesome.ADDRESS_BOOK, new OnClick() {
            @Override
            Component onClick() {
                return new PermissionGroupTab()
            }
        })
        addComponent(btnUser)
        addComponent(btnPerson)
        addComponent(btnParams)
        addComponent(btnPermission)
        addComponent(btnPermissionGroup)
    }

    @Override
    protected void security() {
    }

    @Override
    protected String captionContants(String caption) {
        switch (caption) {
            case btnUser.getCaption():
                return WindowsCaption.USERS_CAPTION
            case btnPerson.getCaption():
                return WindowsCaption.PERSON_CAPTION
            case btnParams.getCaption():
                return WindowsCaption.PARAMS_CAPTION
            case btnPermission.getCaption():
                return WindowsCaption.PERMISSIONS_CAPTION
            case btnPermissionGroup.getCaption():
                return WindowsCaption.PERMISSIONS_GROUP_CAPTION
        }
        return ""
    }
}
