package edu.utesa.ronald.ingsoft.ui.components.menus

import com.kbdunn.vaadin.addons.fontawesome.FontAwesome
import com.vaadin.ui.Button
import com.vaadin.ui.Component
import edu.utesa.ronald.ingsoft.components.abstracts.AMenu
import edu.utesa.ronald.ingsoft.models.interfaces.OnClick
import edu.utesa.ronald.ingsoft.ui.components.ContentPanel
import edu.utesa.ronald.ingsoft.ui.utils.OccupationTab
import edu.utesa.ronald.ingsoft.utils.Languages
import edu.utesa.ronald.ingsoft.utils.MenuCaption
import edu.utesa.ronald.ingsoft.utils.WindowsCaption
import groovy.transform.CompileStatic

/**
 *  Created by ronald on 7/11/17.
 */
@CompileStatic
class MenuUtils extends AMenu {

    private Button btnOccupation

    MenuUtils(ContentPanel contentPanel) {
        super(contentPanel)
        setCaption(Languages.instance.getText(MenuCaption.MENU_UTILS))
        setIcon(FontAwesome.SHIELD)
        setSpacing(true)
    }

    @Override
    protected void buildLayout() {
        btnOccupation = createButton(Languages.instance.getText(WindowsCaption.OCCUPATION_CAPTION), FontAwesome.HOURGLASS_END, new OnClick() {
            @Override
            Component onClick() {
                return new OccupationTab()
            }
        })
        addComponent(btnOccupation)
    }

    @Override
    protected void security() {
    }

    @Override
    protected String captionContants(String caption) {
        switch (caption) {
            case btnOccupation.getCaption():
                return WindowsCaption.OCCUPATION_CAPTION
        }
        return ""
    }
}
