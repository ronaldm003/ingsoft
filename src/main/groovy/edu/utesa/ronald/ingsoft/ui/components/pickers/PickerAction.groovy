package edu.utesa.ronald.ingsoft.ui.components.pickers

import com.vaadin.ui.Button
import edu.utesa.ronald.ingsoft.components.abstracts.APanel
import edu.utesa.ronald.ingsoft.utils.Languages
import groovy.transform.CompileStatic

/**
 *  Created by ronald on 7/21/17.
 */
@CompileStatic
class PickerAction extends APanel {

    private PickerActionDesign pickerActionDesign = new PickerActionDesign()

    PickerAction() {
        confLayout(pickerActionDesign)
    }

    @Override
    protected void confLanguage() {
        pickerActionDesign.btnCancel.setCaption(Languages.instance.getText("window.action.cancel"))
        pickerActionDesign.btnNull.setCaption(Languages.instance.getText("window.action.null"))
        pickerActionDesign.btnPick.setCaption(Languages.instance.getText("window.action.pick"))
    }

    @Override
    protected void buildLayout() {
    }

    @Override
    protected void security() {
    }

    final Button btnCancel() {
        return pickerActionDesign.btnCancel
    }

    final Button btnNull() {
        return pickerActionDesign.btnNull
    }

    final Button btnPick() {
        return pickerActionDesign.btnPick
    }
}
