package edu.utesa.ronald.ingsoft.ui.components.pickers

import com.vaadin.event.ItemClickEvent
import com.vaadin.server.Sizeable
import com.vaadin.ui.Button
import com.vaadin.ui.Grid
import edu.utesa.ronald.ingsoft.components.abstracts.APick
import edu.utesa.ronald.ingsoft.models.interfaces.PickElement
import groovy.transform.CompileStatic

/**
 *  Created by ronald on 7/21/17.
 */
@CompileStatic
abstract class PickerBase<T> extends APick {

    private PickerBaseDesign pickerBaseDesign = new PickerBaseDesign()
    final PickerAction pickerAction = new PickerAction()

    protected PickElement<T> pickElement

    PickerBase(PickElement<T> pickElement, boolean multiSelect) {
        super(multiSelect)
        this.pickElement = pickElement
        setWidth(400, Sizeable.Unit.PIXELS)
        confLayout(pickerBaseDesign)
    }

    @Override
    protected void pickerLanguage() {
    }

    @Override
    protected void preparePicker() {
        setResizable(true)
        setClosable(true)
        setModal(true)
        center()
        if (multiSelect) {
            grid().setSelectionMode(Grid.SelectionMode.MULTI)
        }
    }

    @Override
    protected void buildPicker() {
        pickerBaseDesign.pActions.setContent(pickerAction)
        pickerBaseDesign.gridElements.addItemClickListener(new ItemClickEvent.ItemClickListener() {
            @Override
            void itemClick(ItemClickEvent event) {
                if (event.isDoubleClick()) {
                    grid().select(event.itemId)
                    pickerAction.btnPick().click()
                }
            }
        })

        pickerAction.btnCancel().addClickListener(new Button.ClickListener() {
            @Override
            void buttonClick(Button.ClickEvent event) {
                close()
            }
        })

        pickerAction.btnNull().addClickListener(new Button.ClickListener() {
            @Override
            void buttonClick(Button.ClickEvent event) {
                pickElement.onResult(new ArrayList<T>())
                close()
            }
        })

        pickerAction.btnPick().addClickListener(new Button.ClickListener() {
            @Override
            void buttonClick(Button.ClickEvent event) {
                if (grid().getSelectedRows().size() > 0) {
                    pickElement.onResult(grid().getSelectedRows() as List<T>)
                    close()
                }
            }
        })
    }

    final Grid grid() {
        return pickerBaseDesign.gridElements
    }
}
