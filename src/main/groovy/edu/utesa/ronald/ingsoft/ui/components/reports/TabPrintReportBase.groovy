package edu.utesa.ronald.ingsoft.ui.components.reports

import com.vaadin.ui.Grid
import com.vaadin.ui.Panel
import edu.utesa.ronald.ingsoft.components.abstracts.APanel
import groovy.transform.CompileStatic

/**
 *  Created by ronald on03/14/17.
 */
@CompileStatic
class TabPrintReportBase extends APanel {

    private TabPrintReportBaseDesign tabPrintReportBaseDesign = new TabPrintReportBaseDesign()

    TabPrintReportBase() {
        confLayout(tabPrintReportBaseDesign)
    }

    @Override
    protected void confLanguage() {
    }

    @Override
    protected void buildLayout() {
    }

    @Override
    protected void security() {
    }

    Panel pMenu() {
        return tabPrintReportBaseDesign.pMenu
    }

    Grid gridData() {
        return tabPrintReportBaseDesign.gridData
    }
}
