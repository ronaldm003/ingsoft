package edu.utesa.ronald.ingsoft.ui.components.reports

import edu.utesa.ronald.ingsoft.components.abstracts.APanel
import groovy.transform.CompileStatic

/**
 *  Created by ronald on03/14/17.
 */
@CompileStatic
class TabReportBase extends APanel {

    private TabReportBase tabReportBase = new TabReportBase()

    TabReportBase() {
        confLayout(tabReportBase)
    }

    @Override
    protected void confLanguage() {
    }

    @Override
    protected void buildLayout() {
    }

    @Override
    protected void security() {
    }
}
