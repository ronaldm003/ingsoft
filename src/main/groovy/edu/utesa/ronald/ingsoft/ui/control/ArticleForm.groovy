package edu.utesa.ronald.ingsoft.ui.control

import com.vaadin.server.Sizeable
import edu.utesa.ronald.ingsoft.domains.control.Article
import edu.utesa.ronald.ingsoft.services.control.ArticleService
import edu.utesa.ronald.ingsoft.ui.components.WindowForm
import edu.utesa.ronald.ingsoft.utils.Constants
import edu.utesa.ronald.ingsoft.utils.Languages
import edu.utesa.ronald.ingsoft.utils.WindowsCaption
import groovy.transform.CompileStatic
import org.springframework.beans.factory.annotation.Autowired

/**
 * Aqui se realizan todos los casos de los botones de accion.
 *  Created by ronald on 3/07/17.
 */
@CompileStatic
class ArticleForm extends WindowForm<Article> {

    @Autowired
    private ArticleService articleService

    private ArticleFormArticle formArticle

    ArticleForm() {
        this(null, null)
    }

    ArticleForm(Article control, Boolean edit) {
        super(control, edit)
        confLayout(WindowsCaption.ARTICLE_CAPTION)
        setWidth(600, Sizeable.Unit.PIXELS)
    }

    @Override
    protected void prepare() {
        super.prepare()
        Constants.instance.autoWiredClass(this)
    }

    @Override
    protected void onCreate() {
        formArticle = new ArticleFormArticle()
    }

    @Override
    protected void onEditView() {
        itemSave = articleService.refresh(itemSave.id)
        formArticle = new ArticleFormArticle(itemSave, edit)
    }

    @Override
    Article onSave() {
        itemSave = formArticle.getValid()
        return articleService.create(itemSave, loginManager)
    }

    @Override
    protected void onViewTabs() {

    }

    @Override
    protected void buildView() {

    }

    @Override
    protected boolean getValidForm() {
        if (!formArticle.getValid()) {
            return tabSheetManager.selectTab(0)
        }
        return true
    }

    @Override
    protected void buildLayout() {
        setWidth(800, Sizeable.Unit.PIXELS)
        tabSheetManager.addTabPanel(Languages.instance.getText(WindowsCaption.ARTICLE_CAPTION + FORM + "article"), formArticle)

    }

    @Override
    protected void security() {

    }
}
