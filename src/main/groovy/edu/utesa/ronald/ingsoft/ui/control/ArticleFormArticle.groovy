package edu.utesa.ronald.ingsoft.ui.control

import com.vaadin.event.FieldEvents
import edu.utesa.ronald.ingsoft.components.abstracts.AWindowTab
import edu.utesa.ronald.ingsoft.domains.control.Article
import edu.utesa.ronald.ingsoft.services.control.ArticleService
import edu.utesa.ronald.ingsoft.utils.Constants
import edu.utesa.ronald.ingsoft.utils.Languages
import groovy.transform.CompileStatic
import org.springframework.beans.factory.annotation.Autowired

/**
 *  Created by ronald on 3/07/17.
 */
@CompileStatic
class ArticleFormArticle extends AWindowTab<Article> {

    @Autowired
    private ArticleService articleService

    private ArticleFormArticleDesign articleFormArticleDesign = new ArticleFormArticleDesign()

    ArticleFormArticle() {
        this(new Article(), null)
    }

    ArticleFormArticle(Article article, Boolean edit) {
        super(article, edit)
        confLayout(articleFormArticleDesign)
    }

    @Override
    protected void buildLayout() {
        getNextCode()
        articleFormArticleDesign.tfCode.setReadOnly(true)
        articleFormArticleDesign.tfDescription.setRequired(true)
        articleFormArticleDesign.tfDescription.setRequiredError(Languages.instance.getText("validations.empty.field"))
        articleFormArticleDesign.tfDescription.setValidationVisible(true)
        articleFormArticleDesign.tfDescription.setNullRepresentation("")
        articleFormArticleDesign.tfDescription.addTextChangeListener(new FieldEvents.TextChangeListener() {
            @Override
            void textChange(FieldEvents.TextChangeEvent event) {
                articleFormArticleDesign.tfDescription.setValue(event.getText())
                articleFormArticleDesign.tfDescription.setCursorPosition(event.getCursorPosition())
                articleFormArticleDesign.tfDescription.validate()
            }
        })
    }

    private void getNextCode() {
        Long code = articleService.findMaxCode()
        if (code) {
            articleFormArticleDesign.tfCode.setValue((code + 1).toString())
        } else {
            articleFormArticleDesign.tfCode.setValue("1")
        }
    }

    @Override
    protected void restore() {
        articleFormArticleDesign.tfCode.setReadOnly(false)
        articleFormArticleDesign.tfCode.setValue(itemSave.code.toString())
        articleFormArticleDesign.tfCode.setReadOnly(true)
        articleFormArticleDesign.tfDescription.setValue(itemSave.description ?: "")
        articleFormArticleDesign.tfQuantity.setValue(itemSave.quantity.toString() ?: "0")
        articleFormArticleDesign.tfPunceo.setValue(itemSave.punceo.toString() ?: "0")
        articleFormArticleDesign.tfCost.setValue(itemSave.cost.toString() ?: "0")
    }

    @Override
    protected void visualize() {
        articleFormArticleDesign.tfCode.setReadOnly(true)
        articleFormArticleDesign.tfDescription.setReadOnly(true)
        articleFormArticleDesign.tfQuantity.setReadOnly(true)
        articleFormArticleDesign.tfPunceo.setReadOnly(true)
        articleFormArticleDesign.tfCost.setReadOnly(true)
    }

    @Override
    protected boolean getElementsValid() {
        if (!articleFormArticleDesign.tfCode.isValid()) {
            articleFormArticleDesign.tfCode.focus()
            return false
        } else if (!articleFormArticleDesign.tfDescription.isValid()) {
            articleFormArticleDesign.tfDescription.focus()
            return false
        } else if (!articleFormArticleDesign.tfQuantity.isValid()) {
            articleFormArticleDesign.tfQuantity.focus()
            return false
        } else if (!articleFormArticleDesign.tfPunceo.isValid()) {
            articleFormArticleDesign.tfPunceo.focus()
            return false
        } else if (!articleFormArticleDesign.tfCost.isValid()) {
            articleFormArticleDesign.tfCost.focus()
            return false
        }
        return true
    }

    @Override
    protected void collectData() {
        itemSave.code = articleFormArticleDesign.tfCode.getValue().toLong()
        itemSave.description = articleFormArticleDesign.tfDescription.getValue()
        itemSave.quantity = articleFormArticleDesign.tfQuantity.getValue().toLong()
        itemSave.cost = articleFormArticleDesign.tfCost.getValue().toBigDecimal()
        itemSave.punceo = articleFormArticleDesign.tfPunceo.getValue().toLong()
    }

    @Override
    protected void confLanguage() {
        Constants.instance.autoWiredClass(this)
        articleFormArticleDesign.tfCode.setCaption(Languages.instance.getText("window.id"))
        articleFormArticleDesign.tfDescription.setCaption(Languages.instance.getText("window.description"))
        articleFormArticleDesign.tfQuantity.setCaption(Languages.instance.getText("window.quantity"))
        articleFormArticleDesign.tfCost.setCaption(Languages.instance.getText("window.cost"))
        articleFormArticleDesign.tfPunceo.setCaption(Languages.instance.getText("window.punceo"))
    }

    @Override
    protected Article getValid() {
        if (getElementsValid()) {
            collectData()
            return itemSave
        }
        return null
    }

    @Override
    void startFocus() {
        articleFormArticleDesign.tfDescription.focus()
    }
}
