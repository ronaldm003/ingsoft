package edu.utesa.ronald.ingsoft.ui.control

import edu.utesa.ronald.ingsoft.domains.control.Article
import edu.utesa.ronald.ingsoft.models.containers.control.ArticleContainer
import edu.utesa.ronald.ingsoft.services.control.ArticleService
import edu.utesa.ronald.ingsoft.ui.components.ATabBase
import edu.utesa.ronald.ingsoft.utils.Constants
import edu.utesa.ronald.ingsoft.utils.container.LazyQuery
import groovy.transform.CompileStatic
import org.springframework.beans.factory.annotation.Autowired

/**
 * Aqui se lista todos los tipos de usuario
 *  Created by ronald on 3/07/17.
 */
@CompileStatic
class ArticleTab extends ATabBase<ArticleForm, Article> {

    @Autowired
    private ArticleService articleService

    ArticleTab() {
        Constants.instance.autoWiredClass(this)
    }

    @Override
    protected void confLanguage() {

    }

    @Override
    protected void buildLayout() {
        manageGrid(new ArticleContainer(new LazyQuery<Article>() {
            @Override
            int getSize() {
                return articleService.count(showDelete())
            }

            @Override
            List<Article> getItemsIds(int startIndex, int numberOfIds) {
                return articleService.list(showDelete(), startIndex, numberOfIds)
            }

            @Override
            int getFilteredSize() {
                return 0
            }

            @Override
            List<Article> getFilteredItemsIds(int startIndex, int numberOfIds) {
                return null
            }
        }))
    }

    @Override
    void refreshItem() {

    }

    @Override
    ArticleForm onNew() {
        return new ArticleForm()
    }

    @Override
    ArticleForm onEdit() {
        return new ArticleForm(itemSelect, true)
    }

    @Override
    ArticleForm onView() {
        return new ArticleForm(itemSelect, false)
    }

    @Override
    Class topClass() {
        return null
    }

    @Override
    boolean deleteDomain() {
        return articleService.delete(itemSelect, loginManager)
    }

    @Override
    protected void security() {

    }
}
