package edu.utesa.ronald.ingsoft.ui.control

import com.vaadin.server.Sizeable
import edu.utesa.ronald.ingsoft.domains.control.Equipment
import edu.utesa.ronald.ingsoft.services.control.EquipmentService
import edu.utesa.ronald.ingsoft.ui.components.WindowForm
import edu.utesa.ronald.ingsoft.ui.control.EquipmentFormEquipment
import edu.utesa.ronald.ingsoft.utils.Constants
import edu.utesa.ronald.ingsoft.utils.Languages
import edu.utesa.ronald.ingsoft.utils.WindowsCaption
import groovy.transform.CompileStatic
import org.springframework.beans.factory.annotation.Autowired

/**
 * Aqui se realizan todos los casos de los botones de accion.
 *  Created by ronald on 3/07/17.
 */
@CompileStatic
class EquipmentForm extends WindowForm<Equipment> {

    @Autowired
    private EquipmentService equipmentService

    private EquipmentFormEquipment formEquipment

    EquipmentForm() {
        this(null, null)
    }

    EquipmentForm(Equipment control, Boolean edit) {
        super(control, edit)
        confLayout(WindowsCaption.EQUIPMENT_CAPTION)
        setWidth(600, Sizeable.Unit.PIXELS)
    }

    @Override
    protected void prepare() {
        super.prepare()
        Constants.instance.autoWiredClass(this)
    }

    @Override
    protected void onCreate() {
        formEquipment = new EquipmentFormEquipment()
    }

    @Override
    protected void onEditView() {
        itemSave = equipmentService.refresh(itemSave.id)
        formEquipment = new EquipmentFormEquipment(itemSave, edit)
    }

    @Override
    Equipment onSave() {
        itemSave = formEquipment.getValid()
        return equipmentService.create(itemSave, loginManager)
    }

    @Override
    protected void onViewTabs() {

    }

    @Override
    protected void buildView() {

    }

    @Override
    protected boolean getValidForm() {
        if (!formEquipment.getValid()) {
            return tabSheetManager.selectTab(0)
        }
        return true
    }

    @Override
    protected void buildLayout() {
        setWidth(800, Sizeable.Unit.PIXELS)
        tabSheetManager.addTabPanel(Languages.instance.getText(WindowsCaption.EQUIPMENT_CAPTION + FORM + "equipment"), formEquipment)

    }

    @Override
    protected void security() {

    }
}
