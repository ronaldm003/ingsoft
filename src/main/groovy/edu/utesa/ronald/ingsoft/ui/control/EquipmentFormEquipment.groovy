package edu.utesa.ronald.ingsoft.ui.control

import com.vaadin.event.FieldEvents
import edu.utesa.ronald.ingsoft.components.abstracts.AWindowTab
import edu.utesa.ronald.ingsoft.domains.control.Equipment
import edu.utesa.ronald.ingsoft.services.control.EquipmentService
import edu.utesa.ronald.ingsoft.utils.Constants
import edu.utesa.ronald.ingsoft.utils.Languages
import groovy.transform.CompileStatic
import org.springframework.beans.factory.annotation.Autowired

/**
 *  Created by ronald on 3/07/17.
 */
@CompileStatic
class EquipmentFormEquipment extends AWindowTab<Equipment> {

    @Autowired
    private EquipmentService equipmentService

    private EquipmentFormEquipmentDesign locationFormEquipmentDesign = new EquipmentFormEquipmentDesign()

    EquipmentFormEquipment() {
        this(new Equipment(), null)
    }

    EquipmentFormEquipment(Equipment location, Boolean edit) {
        super(location, edit)
        confLayout(locationFormEquipmentDesign)
    }

    @Override
    protected void buildLayout() {
        locationFormEquipmentDesign.tfName.setRequired(true)
        locationFormEquipmentDesign.tfName.setRequiredError(Languages.instance.getText("validations.empty.field"))
        locationFormEquipmentDesign.tfName.setValidationVisible(true)
        locationFormEquipmentDesign.tfName.setNullRepresentation("")
        locationFormEquipmentDesign.tfName.addTextChangeListener(new FieldEvents.TextChangeListener() {
            @Override
            void textChange(FieldEvents.TextChangeEvent event) {
                locationFormEquipmentDesign.tfName.setValue(event.getText())
                locationFormEquipmentDesign.tfName.setCursorPosition(event.getCursorPosition())
                locationFormEquipmentDesign.tfName.validate()
            }
        })
    }

    @Override
    protected void restore() {
        locationFormEquipmentDesign.tfName.setValue(itemSave.name ?: "")
        locationFormEquipmentDesign.taDescription.setValue(itemSave.description ?: "")
    }

    @Override
    protected void visualize() {
        locationFormEquipmentDesign.tfName.setReadOnly(true)
        locationFormEquipmentDesign.taDescription.setReadOnly(true)
    }

    @Override
    protected boolean getElementsValid() {
        if (!locationFormEquipmentDesign.tfName.isValid()) {
            locationFormEquipmentDesign.tfName.focus()
            return false
        } else if (!locationFormEquipmentDesign.taDescription.isValid()) {
            locationFormEquipmentDesign.taDescription.focus()
            return false
        }
        return true
    }

    @Override
    protected void collectData() {
        itemSave.name = locationFormEquipmentDesign.tfName.getValue()
        itemSave.description = locationFormEquipmentDesign.taDescription.getValue()
    }

    @Override
    protected void confLanguage() {
        Constants.instance.autoWiredClass(this)
        locationFormEquipmentDesign.tfName.setCaption(Languages.instance.getText("window.name"))
        locationFormEquipmentDesign.taDescription.setCaption(Languages.instance.getText("window.description"))
    }

    @Override
    protected Equipment getValid() {
        if (getElementsValid()) {
            collectData()
            return itemSave
        }
        return null
    }

    @Override
    void startFocus() {
        locationFormEquipmentDesign.tfName.focus()
    }
}
