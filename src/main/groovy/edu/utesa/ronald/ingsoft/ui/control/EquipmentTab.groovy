package edu.utesa.ronald.ingsoft.ui.control

import edu.utesa.ronald.ingsoft.domains.control.Equipment
import edu.utesa.ronald.ingsoft.models.containers.control.EquipmentContainer
import edu.utesa.ronald.ingsoft.services.control.EquipmentService
import edu.utesa.ronald.ingsoft.ui.components.ATabBase
import edu.utesa.ronald.ingsoft.ui.control.EquipmentForm
import edu.utesa.ronald.ingsoft.utils.Constants
import edu.utesa.ronald.ingsoft.utils.container.LazyQuery
import groovy.transform.CompileStatic
import org.springframework.beans.factory.annotation.Autowired

/**
 * Aqui se lista todos los tipos de usuario
 *  Created by ronald on 3/07/17.
 */
@CompileStatic
class EquipmentTab extends ATabBase<EquipmentForm, Equipment> {

    @Autowired
    private EquipmentService controlService

    EquipmentTab() {
        Constants.instance.autoWiredClass(this)
    }

    @Override
    protected void confLanguage() {

    }

    @Override
    protected void buildLayout() {
        manageGrid(new EquipmentContainer(new LazyQuery<Equipment>() {
            @Override
            int getSize() {
                return controlService.count(showDelete())
            }

            @Override
            List<Equipment> getItemsIds(int startIndex, int numberOfIds) {
                return controlService.list(showDelete(), startIndex, numberOfIds)
            }

            @Override
            int getFilteredSize() {
                return 0
            }

            @Override
            List<Equipment> getFilteredItemsIds(int startIndex, int numberOfIds) {
                return null
            }
        }))
    }

    @Override
    void refreshItem() {

    }

    @Override
    EquipmentForm onNew() {
        return new EquipmentForm()
    }

    @Override
    EquipmentForm onEdit() {
        return new EquipmentForm(itemSelect, true)
    }

    @Override
    EquipmentForm onView() {
        return new EquipmentForm(itemSelect, false)
    }

    @Override
    Class topClass() {
        return null
    }

    @Override
    boolean deleteDomain() {
        return controlService.delete(itemSelect, loginManager)
    }

    @Override
    protected void security() {

    }
}
