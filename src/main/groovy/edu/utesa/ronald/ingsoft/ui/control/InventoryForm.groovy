package edu.utesa.ronald.ingsoft.ui.control

import com.vaadin.server.Sizeable
import edu.utesa.ronald.ingsoft.domains.control.Inventory
import edu.utesa.ronald.ingsoft.services.control.InventoryService
import edu.utesa.ronald.ingsoft.ui.components.WindowForm
import edu.utesa.ronald.ingsoft.ui.control.InventoryFormInventory
import edu.utesa.ronald.ingsoft.utils.Constants
import edu.utesa.ronald.ingsoft.utils.Languages
import edu.utesa.ronald.ingsoft.utils.WindowsCaption
import groovy.transform.CompileStatic
import org.springframework.beans.factory.annotation.Autowired

/**
 * Aqui se realizan todos los casos de los botones de accion.
 *  Created by ronald on 3/07/17.
 */
@CompileStatic
class InventoryForm extends WindowForm<Inventory> {

    @Autowired
    private InventoryService inventoryService

    private InventoryFormInventory formInventory
    private InventoryFormTabArticles formTabArticles

    InventoryForm() {
        this(null, null)
    }

    InventoryForm(Inventory control, Boolean edit) {
        super(control, edit)
        confLayout(WindowsCaption.INVENTORY_CAPTION)
        setWidth(600, Sizeable.Unit.PIXELS)
    }

    @Override
    protected void prepare() {
        super.prepare()
        Constants.instance.autoWiredClass(this)
    }

    @Override
    protected void onCreate() {
        formInventory = new InventoryFormInventory()
        formTabArticles = new InventoryFormTabArticles()
    }

    @Override
    protected void onEditView() {
        itemSave = inventoryService.refresh(itemSave.id)
        formInventory = new InventoryFormInventory(itemSave, edit)
        formTabArticles = new InventoryFormTabArticles(itemSave.listArticles.toList(), edit)
    }

    @Override
    Inventory onSave() {
        itemSave = formInventory.getValid()
        itemSave.listArticles = formTabArticles.getValid().toSet()
        return inventoryService.create(itemSave, loginManager)
    }

    @Override
    protected void onViewTabs() {

    }

    @Override
    protected void buildView() {

    }

    @Override
    protected boolean getValidForm() {
        if (!formInventory.getValid()) {
            return tabSheetManager.selectTab(0)
        }
        return true
    }

    @Override
    protected void buildLayout() {
        setWidth(800, Sizeable.Unit.PIXELS)
        tabSheetManager.addTabPanel(Languages.instance.getText(WindowsCaption.INVENTORY_CAPTION + FORM + "inventory"), formInventory)
        tabSheetManager.addTabPanel(Languages.instance.getText(WindowsCaption.INVENTORY_CAPTION + FORM + "article"), formTabArticles)

    }

    @Override
    protected void security() {

    }
}
