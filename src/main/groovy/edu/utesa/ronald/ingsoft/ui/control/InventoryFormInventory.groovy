package edu.utesa.ronald.ingsoft.ui.control

import com.vaadin.event.FieldEvents
import edu.utesa.ronald.ingsoft.components.abstracts.AWindowTab
import edu.utesa.ronald.ingsoft.domains.control.Inventory
import edu.utesa.ronald.ingsoft.services.control.InventoryService
import edu.utesa.ronald.ingsoft.utils.Constants
import edu.utesa.ronald.ingsoft.utils.Languages
import groovy.transform.CompileStatic
import org.springframework.beans.factory.annotation.Autowired

/**
 *  Created by ronald on 3/07/17.
 */
@CompileStatic
class InventoryFormInventory extends AWindowTab<Inventory> {

    @Autowired
    private InventoryService inventoryService

    private InventoryFormInventoryDesign locationFormInventoryDesign = new InventoryFormInventoryDesign()

    InventoryFormInventory() {
        this(new Inventory(), null)
    }

    InventoryFormInventory(Inventory location, Boolean edit) {
        super(location, edit)
        confLayout(locationFormInventoryDesign)
    }

    @Override
    protected void buildLayout() {
        locationFormInventoryDesign.tfDescription.setRequired(true)
        locationFormInventoryDesign.tfDescription.setRequiredError(Languages.instance.getText("validations.empty.field"))
        locationFormInventoryDesign.tfDescription.setValidationVisible(true)
        locationFormInventoryDesign.tfDescription.setNullRepresentation("")
        locationFormInventoryDesign.tfDescription.addTextChangeListener(new FieldEvents.TextChangeListener() {
            @Override
            void textChange(FieldEvents.TextChangeEvent event) {
                locationFormInventoryDesign.tfDescription.setValue(event.getText())
                locationFormInventoryDesign.tfDescription.setCursorPosition(event.getCursorPosition())
                locationFormInventoryDesign.tfDescription.validate()
            }
        })
    }

    @Override
    protected void restore() {
        locationFormInventoryDesign.tfDescription.setValue(itemSave.description ?: "")
    }

    @Override
    protected void visualize() {
        locationFormInventoryDesign.tfDescription.setReadOnly(true)
    }

    @Override
    protected boolean getElementsValid() {
        if (!locationFormInventoryDesign.tfDescription.isValid()) {
            locationFormInventoryDesign.tfDescription.focus()
            return false
        }
        return true
    }

    @Override
    protected void collectData() {
        itemSave.description = locationFormInventoryDesign.tfDescription.getValue()
    }

    @Override
    protected void confLanguage() {
        Constants.instance.autoWiredClass(this)
        locationFormInventoryDesign.tfDescription.setCaption(Languages.instance.getText("window.description"))
    }

    @Override
    protected Inventory getValid() {
        if (getElementsValid()) {
            collectData()
            return itemSave
        }
        return null
    }

    @Override
    void startFocus() {
        locationFormInventoryDesign.tfDescription.focus()
    }
}
