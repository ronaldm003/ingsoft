package edu.utesa.ronald.ingsoft.ui.control

import com.vaadin.data.Property
import com.vaadin.event.SelectionEvent
import com.vaadin.ui.Button
import edu.utesa.ronald.ingsoft.components.abstracts.AWindowTab
import edu.utesa.ronald.ingsoft.domains.control.Article
import edu.utesa.ronald.ingsoft.models.containers.control.ArticleContainer
import edu.utesa.ronald.ingsoft.models.interfaces.PickElement
import edu.utesa.ronald.ingsoft.models.suggestions.ArticleSuggestions
import edu.utesa.ronald.ingsoft.services.control.ArticleService
import edu.utesa.ronald.ingsoft.ui.components.TabWindowBase
import edu.utesa.ronald.ingsoft.ui.utils.pickers.ArticlePicker
import edu.utesa.ronald.ingsoft.utils.Constants
import edu.utesa.ronald.ingsoft.utils.container.LazyQuery
import groovy.transform.CompileStatic
import org.springframework.beans.factory.annotation.Autowired

/**
 *  Created by ronald on 04/12/18.
 */
@CompileStatic
class InventoryFormTabArticles extends AWindowTab<List<Article>> {

    @Autowired
    private ArticleService articleService

    private TabWindowBase tabWindowBase = new TabWindowBase()

    private Article articleSelect

    InventoryFormTabArticles() {
        this(new ArrayList<Article>(), null)
    }

    InventoryFormTabArticles(List<Article> itemSave, Boolean edit) {
        super(itemSave, edit)
        Constants.instance.autoWiredClass(this)
        confLayout(tabWindowBase)
    }

    @Override
    protected void confLanguage() {
    }

    @Override
    protected void buildLayout() {
        manageGrid(tabWindowBase.grid(), new ArticleContainer(new LazyQuery<Article>() {
            @Override
            int getSize() {
                return ((List<Article>) itemSave).size()
            }

            @Override
            List<Article> getItemsIds(int startIndex, int numberOfIds) {
                return ((List<Article>) itemSave).subList(startIndex, (startIndex + numberOfIds))
            }

            @Override
            int getFilteredSize() {
                return 0
            }

            @Override
            List<Article> getFilteredItemsIds(int startIndex, int numberOfIds) {
                return new ArrayList<Article>()
            }
        }))
        tabWindowBase.grid().addSelectionListener(new SelectionEvent.SelectionListener() {
            @Override
            void select(SelectionEvent event) {
                if (gridManager.getGrid().getSelectedRow()) {
                    articleSelect = (Article) gridManager.getGrid().getSelectedRow()
                    tabWindowBase.enableMenuAction()
                } else {
                    articleSelect = null
                    tabWindowBase.disableMenuAction()
                }
            }
        })
        tabWindowBase.oneToManyAction.tfACSearch().setSuggestionProvider(new ArticleSuggestions())
        tabWindowBase.oneToManyAction.tfACSearch().addValueChangeListener(new Property.ValueChangeListener() {
            @Override
            void valueChange(Property.ValueChangeEvent event) {
                if (tabWindowBase.oneToManyAction.tfACSearch().getValue() && !tabWindowBase.oneToManyAction.tfACSearch().getValue().isEmpty()) {
                    try {
                        articleSelect = articleService.byDescription(tabWindowBase.oneToManyAction.tfACSearch().getValue())
                    } catch (Exception ignored) {
                    }
                } else {
                    articleSelect = null
                }
            }
        })
        tabWindowBase.oneToManyAction.btnAdd().addClickListener(new Button.ClickListener() {
            @Override
            void buttonClick(Button.ClickEvent clickEvent) {
                if (articleSelect) {
                    Article article = articleService.byDescription(tabWindowBase.oneToManyAction.tfACSearch().getValue())
                    ((List<Article>) itemSave).add(article)
                    itemSave = ((List<Article>) itemSave).toSet().toList()
                    tabWindowBase.oneToManyAction.tfACSearch().setValue("")
                    updateTable()
                }
            }
        })
        tabWindowBase.oneToManyAction.btnDelete().addClickListener(new Button.ClickListener() {
            @Override
            void buttonClick(Button.ClickEvent clickEvent) {
                if (articleSelect) {
                    ((List<Article>) itemSave).remove(articleSelect)
                    updateTable()
                }
            }
        })
        tabWindowBase.oneToManyAction.btnNew().setVisible(false)
        tabWindowBase.oneToManyAction.btnSearch().addClickListener(new Button.ClickListener() {
            @Override
            void buttonClick(Button.ClickEvent event) {
                ArticlePicker articlePicker = new ArticlePicker(new PickElement<Article>() {
                    @Override
                    void onResult(List<Article> elements) {
                        ((List<Article>) itemSave).addAll(elements)
                        itemSave = ((List<Article>) itemSave).toSet().toList()
                        updateTable()
                    }
                }, true)
                getUI().addWindow(articlePicker)
            }
        })
    }

    @Override
    protected void restore() {
    }

    @Override
    protected void visualize() {
        tabWindowBase.viewMenuAction()
    }

    @Override
    protected void security() {
    }

    @Override
    protected boolean getElementsValid() {
        return true
    }

    @Override
    protected void collectData() {
    }

    @Override
    protected List<Article> getValid() {
        if (getElementsValid()) {
            return itemSave
        }
        return null
    }

    @Override
    void startFocus() {
        tabWindowBase.oneToManyAction.tfACSearch().focus()
    }
}
