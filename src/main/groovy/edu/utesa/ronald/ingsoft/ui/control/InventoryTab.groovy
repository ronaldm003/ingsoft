package edu.utesa.ronald.ingsoft.ui.control

import edu.utesa.ronald.ingsoft.domains.control.Inventory
import edu.utesa.ronald.ingsoft.models.containers.control.InventoryContainer
import edu.utesa.ronald.ingsoft.services.control.InventoryService
import edu.utesa.ronald.ingsoft.ui.components.ATabBase
import edu.utesa.ronald.ingsoft.utils.Constants
import edu.utesa.ronald.ingsoft.utils.container.LazyQuery
import groovy.transform.CompileStatic
import org.springframework.beans.factory.annotation.Autowired

/**
 * Aqui se lista todos los tipos de usuario
 *  Created by ronald on 3/07/17.
 */
@CompileStatic
class InventoryTab extends ATabBase<InventoryForm, Inventory> {

    @Autowired
    private InventoryService inventoryService

    InventoryTab() {
        Constants.instance.autoWiredClass(this)
    }

    @Override
    protected void confLanguage() {

    }

    @Override
    protected void buildLayout() {
        manageGrid(new InventoryContainer(new LazyQuery<Inventory>() {
            @Override
            int getSize() {
                return inventoryService.count(showDelete())
            }

            @Override
            List<Inventory> getItemsIds(int startIndex, int numberOfIds) {
                return inventoryService.list(showDelete(), startIndex, numberOfIds)
            }

            @Override
            int getFilteredSize() {
                return 0
            }

            @Override
            List<Inventory> getFilteredItemsIds(int startIndex, int numberOfIds) {
                return null
            }
        }))
    }

    @Override
    void refreshItem() {

    }

    @Override
    InventoryForm onNew() {
        return new InventoryForm()
    }

    @Override
    InventoryForm onEdit() {
        return new InventoryForm(itemSelect, true)
    }

    @Override
    InventoryForm onView() {
        return new InventoryForm(itemSelect, false)
    }

    @Override
    Class topClass() {
        return null
    }

    @Override
    boolean deleteDomain() {
        return inventoryService.delete(itemSelect, loginManager)
    }

    @Override
    protected void security() {

    }
}
