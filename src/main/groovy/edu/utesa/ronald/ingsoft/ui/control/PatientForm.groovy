package edu.utesa.ronald.ingsoft.ui.control

import com.vaadin.server.Sizeable
import edu.utesa.ronald.ingsoft.domains.control.Patient
import edu.utesa.ronald.ingsoft.models.interfaces.OnNew
import edu.utesa.ronald.ingsoft.services.control.PatientService
import edu.utesa.ronald.ingsoft.ui.components.WindowForm
import edu.utesa.ronald.ingsoft.utils.Constants
import edu.utesa.ronald.ingsoft.utils.Languages
import edu.utesa.ronald.ingsoft.utils.WindowsCaption
import groovy.transform.CompileStatic
import org.springframework.beans.factory.annotation.Autowired

/**
 * Aqui se realizan todos los casos de los botones de accion.
 *  Created by ronald on 3/07/17.
 */
@CompileStatic
class PatientForm extends WindowForm<Patient> {

    @Autowired
    private PatientService patientService

    private PatientFormPatient formPatient

    PatientForm() {
        this(null, null)
    }

    PatientForm(OnNew<Patient> onNew) {
        this()
        this.onNew = onNew
    }

    PatientForm(Patient patient, Boolean edit) {
        super(patient, edit)
        confLayout(WindowsCaption.PATIENT_CAPTION)
        setWidth(600, Sizeable.Unit.PIXELS)
    }

    @Override
    protected void prepare() {
        super.prepare()
        Constants.instance.autoWiredClass(this)
    }

    @Override
    protected void onCreate() {
        formPatient = new PatientFormPatient()
    }

    @Override
    protected void onEditView() {
        itemSave = patientService.refresh(itemSave.id)
        formPatient = new PatientFormPatient(itemSave, edit)
    }

    @Override
    Patient onSave() {
        itemSave = formPatient.getValid()
        return patientService.create(itemSave, loginManager)
    }

    @Override
    protected void onViewTabs() {

    }

    @Override
    protected void buildView() {

    }

    @Override
    protected boolean getValidForm() {
        if (!formPatient.getValid()) {
            return tabSheetManager.selectTab(0)
        }
        return true
    }

    @Override
    protected void buildLayout() {
        setWidth(800, Sizeable.Unit.PIXELS)
        tabSheetManager.addTabPanel(Languages.instance.getText(WindowsCaption.PATIENT_CAPTION + FORM + "patient"), formPatient)

    }

    @Override
    protected void security() {

    }
}
