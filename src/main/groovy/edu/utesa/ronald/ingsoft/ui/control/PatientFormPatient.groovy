package edu.utesa.ronald.ingsoft.ui.control

import com.vaadin.event.FieldEvents
import edu.utesa.ronald.ingsoft.components.abstracts.AWindowTab
import edu.utesa.ronald.ingsoft.domains.control.Patient
import edu.utesa.ronald.ingsoft.services.control.PatientService
import edu.utesa.ronald.ingsoft.ui.control.PatientFormPatientDesign
import edu.utesa.ronald.ingsoft.utils.Constants
import edu.utesa.ronald.ingsoft.utils.Languages
import groovy.transform.CompileStatic
import org.springframework.beans.factory.annotation.Autowired

/**
 *  Created by ronald on 3/07/17.
 */
@CompileStatic
class PatientFormPatient extends AWindowTab<Patient> {

    @Autowired
    private PatientService patientService

    private PatientFormPatientDesign controlFormPatientDesign = new PatientFormPatientDesign()

    PatientFormPatient() {
        this(new Patient(), null)
    }

    PatientFormPatient(Patient control, Boolean edit) {
        super(control, edit)
        confLayout(controlFormPatientDesign)
    }

    @Override
    protected void buildLayout() {
        controlFormPatientDesign.tfCode.setReadOnly(true)
        controlFormPatientDesign.tfDescription.setRequired(true)
        controlFormPatientDesign.tfDescription.setRequiredError(Languages.instance.getText("validations.empty.field"))
        controlFormPatientDesign.tfDescription.setValidationVisible(true)
        controlFormPatientDesign.tfDescription.setNullRepresentation("")
        controlFormPatientDesign.tfDescription.addTextChangeListener(new FieldEvents.TextChangeListener() {
            @Override
            void textChange(FieldEvents.TextChangeEvent event) {
                controlFormPatientDesign.tfDescription.setValue(event.getText())
                controlFormPatientDesign.tfDescription.setCursorPosition(event.getCursorPosition())
                controlFormPatientDesign.tfDescription.validate()
            }
        })
    }


    @Override
    protected void restore() {

//        controlFormPatientDesign.tfDescription.setValue(itemSave.description ?: "")
//        controlFormPatientDesign.tfLongitude.setValue(itemSave.longitude.toString() ?: "")
//        controlFormPatientDesign.tfLatitude.setValue(itemSave.latitude.toString() ?: "")
    }

    @Override
    protected void visualize() {
        controlFormPatientDesign.tfCode.setReadOnly(true)
        controlFormPatientDesign.tfDescription.setReadOnly(true)
        controlFormPatientDesign.tfLongitude.setReadOnly(true)
        controlFormPatientDesign.tfLatitude.setReadOnly(true)
    }

    @Override
    protected boolean getElementsValid() {
        if (!controlFormPatientDesign.tfCode.isValid()) {
            controlFormPatientDesign.tfCode.focus()
            return false
        } else if (!controlFormPatientDesign.tfDescription.isValid()) {
            controlFormPatientDesign.tfDescription.focus()
            return false
        } else if (!controlFormPatientDesign.tfLongitude.isValid()) {
            controlFormPatientDesign.tfLongitude.focus()
            return false
        } else if (!controlFormPatientDesign.tfLatitude.isValid()) {
            controlFormPatientDesign.tfLatitude.focus()
            return false
        }
        return true
    }

    @Override
    protected void collectData() {
//        itemSave.code = controlFormPatientDesign.tfCode.getValue().toLong()
//        itemSave.description = controlFormPatientDesign.tfDescription.getValue()
//        itemSave.longitude = locationFormPatientDesign.tfLongitude.getValue().toLong()
//        itemSave.latitude = locationFormPatientDesign.tfLatitude.getValue().toLong()
    }

    @Override
    protected void confLanguage() {
        Constants.instance.autoWiredClass(this)
//        locationFormPatientDesign.tfCode.setCaption(Languages.instance.getText("window.id"))
//        locationFormPatientDesign.tfDescription.setCaption(Languages.instance.getText("window.description"))
//        locationFormPatientDesign.tfLongitude.setCaption(Languages.instance.getText("window.longitude"))
//        locationFormPatientDesign.tfLatitude.setCaption(Languages.instance.getText("window.latitude"))
    }

    @Override
    protected Patient getValid() {
        if (getElementsValid()) {
            collectData()
            return itemSave
        }
        return null
    }

    @Override
    void startFocus() {
//        locationFormPatientDesign.tfDescription.focus()
    }
}
