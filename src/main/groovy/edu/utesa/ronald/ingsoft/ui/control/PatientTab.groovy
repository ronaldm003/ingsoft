package edu.utesa.ronald.ingsoft.ui.control

import edu.utesa.ronald.ingsoft.domains.control.Patient
import edu.utesa.ronald.ingsoft.models.containers.control.PatientContainer
import edu.utesa.ronald.ingsoft.services.control.PatientService
import edu.utesa.ronald.ingsoft.ui.components.ATabBase
import edu.utesa.ronald.ingsoft.ui.control.PatientForm
import edu.utesa.ronald.ingsoft.utils.Constants
import edu.utesa.ronald.ingsoft.utils.container.LazyQuery
import groovy.transform.CompileStatic
import org.springframework.beans.factory.annotation.Autowired

/**
 * Aqui se lista todos los tipos de usuario
 *  Created by ronald on 3/07/17.
 */
@CompileStatic
class PatientTab extends ATabBase<PatientForm, Patient> {

    @Autowired
    private PatientService patientService

    PatientTab() {
        Constants.instance.autoWiredClass(this)
    }

    @Override
    protected void confLanguage() {

    }

    @Override
    protected void buildLayout() {
        manageGrid(new PatientContainer(new LazyQuery<Patient>() {
            @Override
            int getSize() {
                return patientService.count(showDelete())
            }

            @Override
            List<Patient> getItemsIds(int startIndex, int numberOfIds) {
                return patientService.list(showDelete(), startIndex, numberOfIds)
            }

            @Override
            int getFilteredSize() {
                return 0
            }

            @Override
            List<Patient> getFilteredItemsIds(int startIndex, int numberOfIds) {
                return null
            }
        }))
    }

    @Override
    void refreshItem() {

    }

    @Override
    PatientForm onNew() {
        return new PatientForm()
    }

    @Override
    PatientForm onEdit() {
        return new PatientForm(itemSelect, true)
    }

    @Override
    PatientForm onView() {
        return new PatientForm(itemSelect, false)
    }

    @Override
    Class topClass() {
        return null
    }

    @Override
    boolean deleteDomain() {
        return patientService.delete(itemSelect, loginManager)
    }

    @Override
    protected void security() {

    }
}
