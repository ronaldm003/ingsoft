package edu.utesa.ronald.ingsoft.ui.control.recordcard

import com.vaadin.server.Sizeable
import edu.utesa.ronald.ingsoft.domains.control.recordcard.PatientEvaluationAnswer
import edu.utesa.ronald.ingsoft.domains.control.recordcard.RecordCard
import edu.utesa.ronald.ingsoft.services.control.recordcard.PatientEvaluationAnswerService
import edu.utesa.ronald.ingsoft.services.control.recordcard.RecordCardService
import edu.utesa.ronald.ingsoft.ui.components.WindowForm
import edu.utesa.ronald.ingsoft.utils.Constants
import edu.utesa.ronald.ingsoft.utils.Languages
import edu.utesa.ronald.ingsoft.utils.WindowsCaption
import groovy.transform.CompileStatic
import org.springframework.beans.factory.annotation.Autowired

/**
 * Aqui se realizan todos los casos de los botones de accion.
 *  Created by ronald on 3/07/17.
 */
@CompileStatic
class RecordCardForm extends WindowForm<RecordCard> {

    @Autowired
    private RecordCardService recordCardService

    @Autowired
    private PatientEvaluationAnswerService patientEvaluationAnswerService

    private RecordCardFormRecordCard formRecordCard
    private RecordCardFormEvaluation formEvaluation
    private RecordCardFormExamns formExamns
    private RecordCardFormIllnessAndHabits formIllness
    private RecordCardFormOcclusion formOcclusion

    RecordCardForm() {
        this(null, null)
    }

    RecordCardForm(RecordCard control, Boolean edit) {
        super(control, edit)
        setWidth(1024, Sizeable.Unit.PIXELS)
        confLayout(WindowsCaption.RECORD_CARD_CAPTION)
    }

    @Override
    protected void prepare() {
        super.prepare()
        Constants.instance.autoWiredClass(this)
    }

    @Override
    protected void onCreate() {
        formRecordCard = new RecordCardFormRecordCard()
        formEvaluation = new RecordCardFormEvaluation()
        formExamns = new RecordCardFormExamns()
        formIllness = new RecordCardFormIllnessAndHabits()
        formOcclusion = new RecordCardFormOcclusion()
    }

    @Override
    protected void onEditView() {
        itemSave = recordCardService.refresh(itemSave.id, {
            it.patient?.enabled;
            it.patient?.person?.enabled;
            it.cycle?.enabled;
            it.student?.enabled;
            it.teacher?.enabled;
            it.patient?.person?.location?.enabled;
            it.patient?.person?.occupation?.enabled;
            it.patient?.person?.educationLevel?.enabled;
            it.student?.person?.enabled;
            it.teacher?.person?.enabled;
            it.occlusion?.enabled;
            it.listPatientEvaluationAnswer.size()
            it.listPatientEvaluationAnswer.each { it.evaluationQuestion.enabled }
            it.listCariesIndex.size()
            it.listPatientExtraBuccalAnswer.size()
            it.listPatientExtraBuccalAnswer.each { it.extraBuccal.enabled }
            it.listPatientIntraBuccalAnswer.size()
            it.listPatientIntraBuccalAnswer.each { it.intraBuccal.enabled }
            it.listMedObservations.size()
            it.listOralHygieneIndex.size()
            it.listPatientHabits.size()
            it.listPatientHabits.each { it.habits.enabled }
            it.listPatientIllness.size()
            it.listPatientIllness.each { it.illness.enabled }
            it.listToothGroup.size()
            it.oralHygiene?.enabled;
            it.motherOccupation?.enabled;
            it.fatherOccupation?.enabled
        })
        formRecordCard = new RecordCardFormRecordCard(itemSave, edit)
        List<PatientEvaluationAnswer> patientEvaluationAnswers1 = new ArrayList<>()

        if (itemSave.listPatientEvaluationAnswer.size() > 0) {
            itemSave.listPatientEvaluationAnswer.stream().forEach({ esto ->
                patientEvaluationAnswers1.add(patientEvaluationAnswerService.refresh(esto.id, {
                    it.evaluationQuestion.enabled
                }))
            })
        }
        itemSave.listPatientEvaluationAnswer = patientEvaluationAnswers1.toSet()
        formEvaluation = new RecordCardFormEvaluation(itemSave.listPatientEvaluationAnswer.toList(), edit)
        formIllness = new RecordCardFormIllnessAndHabits(true, itemSave.listPatientIllness.toList(), itemSave.listPatientHabits.toList(), edit)
        formExamns = new RecordCardFormExamns(true, edit, itemSave.listPatientExtraBuccalAnswer.toList(), itemSave.listPatientIntraBuccalAnswer.toList())
        formOcclusion = new RecordCardFormOcclusion(itemSave.occlusion, edit)
    }

    @Override
    RecordCard onSave() {
        itemSave = formRecordCard.getValid()
        itemSave.listPatientEvaluationAnswer = formEvaluation.getValid().toSet()
        itemSave.listPatientIllness = formIllness.validPatientIllnessList().toSet()
        itemSave.listPatientHabits = formIllness.validPatientHabitsList().toSet()
        itemSave.listPatientExtraBuccalAnswer = formExamns.validPatientExtraBuccalAnswer().toSet()
        itemSave.listPatientIntraBuccalAnswer = formExamns.validPatientIntraBuccalAnswer().toSet()
        itemSave.occlusion = formOcclusion.getValid()
        return recordCardService.create(itemSave, loginManager)
    }

    @Override
    protected void onViewTabs() {
    }

    @Override
    protected void buildView() {

    }

    @Override
    protected boolean getValidForm() {
        if (!formRecordCard.getValid()) {
            return tabSheetManager.selectTab(0)
        }
        return true
    }

    @Override
    protected void buildLayout() {
        setWidth(800, Sizeable.Unit.PIXELS)
        tabSheetManager.addTabPanel(Languages.instance.getText(WindowsCaption.RECORD_CARD_CAPTION + FORM + "recordCard"), formRecordCard)
        tabSheetManager.addTabPanel(Languages.instance.getText(WindowsCaption.RECORD_CARD_CAPTION + FORM + "evaluation"), formEvaluation)
        tabSheetManager.addTabPanel(Languages.instance.getText(WindowsCaption.RECORD_CARD_CAPTION + FORM + "examn"), formExamns)
        tabSheetManager.addTabPanel(Languages.instance.getText(WindowsCaption.RECORD_CARD_CAPTION + FORM + "illness.habits"), formIllness)
        tabSheetManager.addTabPanel(Languages.instance.getText(WindowsCaption.RECORD_CARD_CAPTION + FORM + "occlusion"), formOcclusion)

    }

    @Override
    protected void security() {

    }
}
