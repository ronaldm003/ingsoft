package edu.utesa.ronald.ingsoft.ui.control.recordcard

import com.vaadin.server.Sizeable
import com.vaadin.ui.themes.ValoTheme
import edu.utesa.ronald.ingsoft.components.TableManager
import edu.utesa.ronald.ingsoft.components.abstracts.AWindowTab
import edu.utesa.ronald.ingsoft.domains.control.recordcard.EvaluationQuestion
import edu.utesa.ronald.ingsoft.domains.control.recordcard.PatientEvaluationAnswer
import edu.utesa.ronald.ingsoft.models.containers.control.recordcard.PatientEvaluationAnswerContainer
import edu.utesa.ronald.ingsoft.services.control.recordcard.EvaluationQuestionService
import edu.utesa.ronald.ingsoft.services.control.recordcard.PatientEvaluationAnswerService
import edu.utesa.ronald.ingsoft.services.control.recordcard.RecordCardService
import edu.utesa.ronald.ingsoft.utils.Constants
import edu.utesa.ronald.ingsoft.utils.Languages
import edu.utesa.ronald.ingsoft.utils.container.LazyQuery
import groovy.transform.CompileStatic
import org.springframework.beans.factory.annotation.Autowired

/**
 *  Created by ronald on 3/07/17.
 */
@CompileStatic
class RecordCardFormEvaluation extends AWindowTab<List<PatientEvaluationAnswer>> {

    @Autowired
    private RecordCardService recordCardService
    @Autowired
    private PatientEvaluationAnswerService patientEvaluationAnswerService
    @Autowired
    private EvaluationQuestionService evaluationQuestionService

    private RecordCardFormEvaluationDesign recordCardFormEvaluationDesign = new RecordCardFormEvaluationDesign()

    private List<PatientEvaluationAnswer> patientEvaluationAnswerList

    private TableManager tableActionPatientEvaluationAnswer

    RecordCardFormEvaluation() {
        this(new ArrayList<PatientEvaluationAnswer>(), null)
    }

    RecordCardFormEvaluation(List<PatientEvaluationAnswer> patientEvaluationAnswers, Boolean edit) {
        super(patientEvaluationAnswers, edit)
        if (patientEvaluationAnswers.size() > 0) {
            patientEvaluationAnswerList = patientEvaluationAnswers.sort({ a, b -> a.evaluationQuestion?.code <=> b.evaluationQuestion?.code })
        } else {
            patientEvaluationAnswerList = new ArrayList<>()
        }
        confLayout(recordCardFormEvaluationDesign)
    }

    @Override
    protected void buildLayout() {
        addPatientEvaluationAnswerGrid()
    }

    private void addPatientEvaluationAnswerGrid() {
        createTableEvaluationQuestions()
    }

    private void createTableEvaluationQuestions() {
        addEvaluationQuestion()
        tableActionPatientEvaluationAnswer = new TableManager(recordCardFormEvaluationDesign.tableEvaluation, new PatientEvaluationAnswerContainer(new LazyQuery<PatientEvaluationAnswer>() {
            @Override
            int getSize() {
                return patientEvaluationAnswerList.size()
            }

            @Override
            List<PatientEvaluationAnswer> getItemsIds(int startIndex, int numberOfIds) {
                return patientEvaluationAnswerList.subList(startIndex, (startIndex + numberOfIds))
            }

            @Override
            int getFilteredSize() {
                return 0
            }

            @Override
            List<PatientEvaluationAnswer> getFilteredItemsIds(int startIndex, int numberOfIds) {
                return new ArrayList<PatientEvaluationAnswer>()
            }
        }, (edit != null && !edit)))
        tableActionPatientEvaluationAnswer.getTable().setHeight(450, Sizeable.Unit.PIXELS)
        tableActionPatientEvaluationAnswer.getTable().setStyleName(ValoTheme.TABLE_SMALL)
        tableActionPatientEvaluationAnswer.getTable().setColumnHeader("evaluationQuestion", Languages.instance.getText("grid.column.evaluationQuestion"))
        tableActionPatientEvaluationAnswer.getTable().setColumnHeader("answer", Languages.instance.getText("grid.column.answer"))
        tableActionPatientEvaluationAnswer.getTable().setColumnExpandRatio("evaluationQuestion", 0.50f)
        tableActionPatientEvaluationAnswer.getTable().setColumnExpandRatio("answer", 0.50f)
        tableActionPatientEvaluationAnswer.removeColumnsDefault()
        tableActionPatientEvaluationAnswer.updateTable()
    }

    void addEvaluationQuestion() {
        List<EvaluationQuestion> evaluationQuestions = evaluationQuestionService.list(true)
        for (EvaluationQuestion evaluationQuestion : evaluationQuestions) {
            if (!haveEvaluationQuestion(evaluationQuestion)) {
                patientEvaluationAnswerList.add(new PatientEvaluationAnswer(evaluationQuestion: evaluationQuestion))
            }
        }
    }

    private boolean haveEvaluationQuestion(EvaluationQuestion evaluationQuestion) {
        for (PatientEvaluationAnswer patientEvaluationAnswer : patientEvaluationAnswerList) {
            if (patientEvaluationAnswer.evaluationQuestion.id == evaluationQuestion.id) {
                return true
            }
        }
        return false
    }


    @Override
    protected void restore() {

    }

    @Override
    protected void visualize() {

    }

    @Override
    protected boolean getElementsValid() {

        return true
    }

    @Override
    protected void collectData() {
        itemSave = patientEvaluationAnswerList
    }

    @Override
    protected void confLanguage() {
        Constants.instance.autoWiredClass(this)
        recordCardFormEvaluationDesign.tableEvaluation.setCaption(Languages.instance.getText("window.patient.evaluation.questions"))
    }

    @Override
    protected List<PatientEvaluationAnswer> getValid() {
        if (getElementsValid()) {
            collectData()
            return itemSave
        }
        return null
    }

    @Override
    void startFocus() {
    }
}
