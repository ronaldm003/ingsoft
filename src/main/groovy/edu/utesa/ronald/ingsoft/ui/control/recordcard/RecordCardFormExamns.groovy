package edu.utesa.ronald.ingsoft.ui.control.recordcard

import com.vaadin.server.Sizeable
import com.vaadin.ui.themes.ValoTheme
import edu.utesa.ronald.ingsoft.components.TableManager
import edu.utesa.ronald.ingsoft.components.abstracts.AWindowTab
import edu.utesa.ronald.ingsoft.domains.control.recordcard.ExtraBuccal
import edu.utesa.ronald.ingsoft.domains.control.recordcard.IntraBuccal
import edu.utesa.ronald.ingsoft.domains.control.recordcard.PatientExtraBuccalAnswer
import edu.utesa.ronald.ingsoft.domains.control.recordcard.PatientIntraBuccalAnswer
import edu.utesa.ronald.ingsoft.models.containers.control.recordcard.PatientExtraBuccalAnswerContainer
import edu.utesa.ronald.ingsoft.models.containers.control.recordcard.PatientIntraBuccalAnswerContainer
import edu.utesa.ronald.ingsoft.services.control.recordcard.*
import edu.utesa.ronald.ingsoft.utils.Constants
import edu.utesa.ronald.ingsoft.utils.Languages
import edu.utesa.ronald.ingsoft.utils.container.LazyQuery
import groovy.transform.CompileStatic
import org.springframework.beans.factory.annotation.Autowired

/**
 *  Created by ronald on 3/07/17.
 */
@CompileStatic
class RecordCardFormExamns extends AWindowTab<Boolean> {

    @Autowired
    private RecordCardService recordCardService
    @Autowired
    private PatientExtraBuccalAnswerService extraBuccalAnswerService
    @Autowired
    private PatientIntraBuccalAnswerService intraBuccalAnswerService
    @Autowired
    private IntraBuccalService intraBuccalService
    @Autowired
    private ExtraBuccalService extraBuccalService

    private RecordCardFormExamnsDesign recordCardFormExamnsDesign = new RecordCardFormExamnsDesign()

    private List<PatientExtraBuccalAnswer> extraBuccalAnswerList
    private List<PatientIntraBuccalAnswer> intraBuccalAnswerList

    private TableManager tableActionPatientExtraBuccalAnswer
    private TableManager tableActionPatientIntraBuccalAnswer

    RecordCardFormExamns() {
        this(new Boolean(true), null, new ArrayList<PatientExtraBuccalAnswer>(), new ArrayList<PatientIntraBuccalAnswer>())
    }

    RecordCardFormExamns(Boolean yes, Boolean edit, List<PatientExtraBuccalAnswer> extraBuccalAnswers, List<PatientIntraBuccalAnswer> intraBuccalAnswers) {
        super(yes, edit)
        if (extraBuccalAnswers) {
            extraBuccalAnswerList = extraBuccalAnswers.sort({ a, b -> a.extraBuccal.code <=> b.extraBuccal.code })
        } else {
            extraBuccalAnswerList = new ArrayList<>()
        }
        if (intraBuccalAnswers) {
            intraBuccalAnswerList = intraBuccalAnswers.sort({ a, b -> a.intraBuccal.code <=> b.intraBuccal.code })
        } else {
            intraBuccalAnswerList = new ArrayList<>()
        }
        confLayout(recordCardFormExamnsDesign)
    }

    @Override
    protected void buildLayout() {
        createTableIntraBuccal()
        createTableExtraBuccal()
    }

    private void createTableIntraBuccal() {
        addIntraBuccal()
        tableActionPatientIntraBuccalAnswer = new TableManager(recordCardFormExamnsDesign.tableIntraBuccal, new PatientIntraBuccalAnswerContainer(new LazyQuery<PatientIntraBuccalAnswer>() {
            @Override
            int getSize() {
                return intraBuccalAnswerList.size()
            }

            @Override
            List<PatientIntraBuccalAnswer> getItemsIds(int startIndex, int numberOfIds) {
                return intraBuccalAnswerList.subList(startIndex, (startIndex + numberOfIds))
            }

            @Override
            int getFilteredSize() {
                return 0
            }

            @Override
            List<PatientIntraBuccalAnswer> getFilteredItemsIds(int startIndex, int numberOfIds) {
                return new ArrayList<PatientIntraBuccalAnswer>()
            }
        }, (edit != null && !edit)))
        tableActionPatientIntraBuccalAnswer.getTable().setHeight(450, Sizeable.Unit.PIXELS)
        tableActionPatientIntraBuccalAnswer.getTable().setStyleName(ValoTheme.TABLE_SMALL)
        tableActionPatientIntraBuccalAnswer.getTable().setColumnHeader("evaluationQuestion", Languages.instance.getText("grid.column.intraBuccal"))
        tableActionPatientIntraBuccalAnswer.getTable().setColumnHeader("answer", Languages.instance.getText("grid.column.answer"))
        tableActionPatientIntraBuccalAnswer.getTable().setColumnExpandRatio("evaluationQuestion", 0.50f)
        tableActionPatientIntraBuccalAnswer.getTable().setColumnExpandRatio("answer", 0.50f)
        tableActionPatientIntraBuccalAnswer.removeColumnsDefault()
        tableActionPatientIntraBuccalAnswer.updateTable()
    }

    private void addIntraBuccal() {
        List<IntraBuccal> intraBuccals = intraBuccalService.list(true)
        for (IntraBuccal intraBuccal : intraBuccals) {
            if (!haveIntraBuccal(intraBuccal)) {
                intraBuccalAnswerList.add(new PatientIntraBuccalAnswer(intraBuccal: intraBuccal))
            }
        }
    }

    private boolean haveIntraBuccal(IntraBuccal intraBuccal) {
        for (PatientIntraBuccalAnswer patientIntraBuccalAnswer : intraBuccalAnswerList) {
            if (patientIntraBuccalAnswer.intraBuccal.id == intraBuccal.id) {
                return true
            }
        }
        return false
    }

    private void createTableExtraBuccal() {
        addExtraBuccal()
        tableActionPatientExtraBuccalAnswer = new TableManager(recordCardFormExamnsDesign.tableExtraBuccal, new PatientExtraBuccalAnswerContainer(new LazyQuery<PatientExtraBuccalAnswer>() {
            @Override
            int getSize() {
                return extraBuccalAnswerList.size()
            }

            @Override
            List<PatientExtraBuccalAnswer> getItemsIds(int startIndex, int numberOfIds) {
                return extraBuccalAnswerList.subList(startIndex, (startIndex + numberOfIds))
            }

            @Override
            int getFilteredSize() {
                return 0
            }

            @Override
            List<PatientExtraBuccalAnswer> getFilteredItemsIds(int startIndex, int numberOfIds) {
                return new ArrayList<PatientExtraBuccalAnswer>()
            }
        }, (edit != null && !edit)))
        tableActionPatientExtraBuccalAnswer.getTable().setHeight(450, Sizeable.Unit.PIXELS)
        tableActionPatientExtraBuccalAnswer.getTable().setStyleName(ValoTheme.TABLE_SMALL)
        tableActionPatientExtraBuccalAnswer.getTable().setColumnHeader("evaluationQuestion", Languages.instance.getText("grid.column.extraBuccal"))
        tableActionPatientExtraBuccalAnswer.getTable().setColumnHeader("answer", Languages.instance.getText("grid.column.answer"))
        tableActionPatientExtraBuccalAnswer.getTable().setColumnExpandRatio("evaluationQuestion", 0.50f)
        tableActionPatientExtraBuccalAnswer.getTable().setColumnExpandRatio("answer", 0.50f)
        tableActionPatientExtraBuccalAnswer.removeColumnsDefault()
        tableActionPatientExtraBuccalAnswer.updateTable()
    }

    void addExtraBuccal() {
        List<ExtraBuccal> extraBuccals = extraBuccalService.list(true)
        for (ExtraBuccal extraBuccal : extraBuccals) {
            if (!haveExtraBuccal(extraBuccal)) {
                extraBuccalAnswerList.add(new PatientExtraBuccalAnswer(extraBuccal: extraBuccal))
            }
        }
    }

    private boolean haveExtraBuccal(ExtraBuccal extraBuccal) {
        for (PatientExtraBuccalAnswer patientExtraBuccalAnswer : extraBuccalAnswerList) {
            if (patientExtraBuccalAnswer.extraBuccal.id == extraBuccal.id) {
                return true
            }
        }
        return false
    }


    @Override
    protected void restore() {

    }

    @Override
    protected void visualize() {

    }

    @Override
    protected boolean getElementsValid() {
        return true
    }

    @Override
    protected void collectData() {

    }

    @Override
    protected void confLanguage() {
        Constants.instance.autoWiredClass(this)
        recordCardFormExamnsDesign.tableExtraBuccal.setCaption(Languages.instance.getText("window.patient.examn.extraBuccal"))
        recordCardFormExamnsDesign.tableIntraBuccal.setCaption(Languages.instance.getText("window.patient.examn.intraBuccal"))
    }

    @Override
    protected Boolean getValid() {
        if (getElementsValid()) {
            collectData()
            return itemSave
        }
        return null
    }

    List<PatientExtraBuccalAnswer> validPatientExtraBuccalAnswer() {
        return extraBuccalAnswerList
    }

    List<PatientIntraBuccalAnswer> validPatientIntraBuccalAnswer() {
        return intraBuccalAnswerList
    }

    @Override
    void startFocus() {
    }
}
