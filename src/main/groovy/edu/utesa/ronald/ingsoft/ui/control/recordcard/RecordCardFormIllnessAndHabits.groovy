package edu.utesa.ronald.ingsoft.ui.control.recordcard

import com.vaadin.server.Sizeable
import com.vaadin.ui.themes.ValoTheme
import edu.utesa.ronald.ingsoft.components.TableManager
import edu.utesa.ronald.ingsoft.components.abstracts.AWindowTab
import edu.utesa.ronald.ingsoft.domains.control.recordcard.Habits
import edu.utesa.ronald.ingsoft.domains.control.recordcard.Illness
import edu.utesa.ronald.ingsoft.domains.control.recordcard.PatientHabits
import edu.utesa.ronald.ingsoft.domains.control.recordcard.PatientIllness
import edu.utesa.ronald.ingsoft.models.containers.control.recordcard.PatientHabitsContainer
import edu.utesa.ronald.ingsoft.models.containers.control.recordcard.PatientIllnessContainer
import edu.utesa.ronald.ingsoft.services.control.recordcard.HabitsService
import edu.utesa.ronald.ingsoft.services.control.recordcard.IllnessService
import edu.utesa.ronald.ingsoft.services.control.recordcard.PatientHabitsService
import edu.utesa.ronald.ingsoft.services.control.recordcard.PatientIllnessService
import edu.utesa.ronald.ingsoft.utils.Constants
import edu.utesa.ronald.ingsoft.utils.Languages
import edu.utesa.ronald.ingsoft.utils.container.LazyQuery
import groovy.transform.CompileStatic
import org.springframework.beans.factory.annotation.Autowired

/**
 *  Created by ronald on 3/07/17.
 */
@CompileStatic
class RecordCardFormIllnessAndHabits extends AWindowTab<Boolean> {

    @Autowired
    private HabitsService habitsService
    @Autowired
    private IllnessService illnessService
    @Autowired
    private PatientIllnessService patientIllnessService
    @Autowired
    private PatientHabitsService patientHabitsService

    private RecordCardFormIllnessAndHabitsDesign recordCardFormIllnessDesign = new RecordCardFormIllnessAndHabitsDesign()

    private List<PatientIllness> patientIllnessList
    private List<PatientHabits> patientHabitsList

    private TableManager tableActionPatientIllness
    private TableManager tableActionPatientHabits

    RecordCardFormIllnessAndHabits() {
        this(new Boolean(true), new ArrayList<PatientIllness>(), new ArrayList<PatientHabits>(), null)
    }

    RecordCardFormIllnessAndHabits(Boolean aBoolean, List<PatientIllness> patientIllnessList, List<PatientHabits> patientHabitsList, Boolean edit) {
        super(aBoolean, edit)
        if (patientIllnessList) {
            this.patientIllnessList = patientIllnessList.sort({ a, b -> a.illness.code <=> b.illness.code })
        } else {
            this.patientIllnessList = new ArrayList<>()
        }
        if (patientHabitsList) {
            this.patientHabitsList = patientHabitsList.sort({ a, b -> a.habits.code <=> b.habits.code })
        } else {
            this.patientHabitsList = new ArrayList<>()
        }
        confLayout(recordCardFormIllnessDesign)
    }

    @Override
    protected void buildLayout() {
        createTableIllness()
        createTableHabits()
    }

    private void createTableIllness() {
        addIllness()
        tableActionPatientIllness = new TableManager(recordCardFormIllnessDesign.tableIllness, new PatientIllnessContainer(new LazyQuery<PatientIllness>() {
            @Override
            int getSize() {
                return patientIllnessList.size()
            }

            @Override
            List<PatientIllness> getItemsIds(int startIndex, int numberOfIds) {
                return patientIllnessList.subList(startIndex, (startIndex + numberOfIds))
            }

            @Override
            int getFilteredSize() {
                return 0
            }

            @Override
            List<PatientIllness> getFilteredItemsIds(int startIndex, int numberOfIds) {
                return new ArrayList<PatientIllness>()
            }
        }, (edit != null && !edit)))
        tableActionPatientIllness.getTable().setHeight(450, Sizeable.Unit.PIXELS)
        tableActionPatientIllness.getTable().setStyleName(ValoTheme.TABLE_SMALL)
        tableActionPatientIllness.getTable().setColumnHeader("illness", Languages.instance.getText("grid.column.illness"))
        tableActionPatientIllness.getTable().setColumnHeader("answer", Languages.instance.getText("grid.column.state"))
        tableActionPatientIllness.getTable().setColumnExpandRatio("illness", 0.50f)
        tableActionPatientIllness.getTable().setColumnExpandRatio("state", 0.50f)
        tableActionPatientIllness.removeColumnsDefault()
        tableActionPatientIllness.updateTable()
    }

    void addIllness() {
        List<Illness> listOfIllnesses = illnessService.list(true)
        for (Illness illness : listOfIllnesses) {
            if (!haveIllness(illness)) {
                patientIllnessList.add(new PatientIllness(illness: illness))
            }
        }
    }

    private boolean haveIllness(Illness illness) {
        for (PatientIllness patientIllness : patientIllnessList) {
            if (patientIllness.illness.id == illness.id) {
                return true
            }
        }
        return false
    }


    private void createTableHabits() {
        addHabit()
        tableActionPatientHabits = new TableManager(recordCardFormIllnessDesign.tableHabits, new PatientHabitsContainer(new LazyQuery<PatientHabits>() {
            @Override
            int getSize() {
                return patientHabitsList.size()
            }

            @Override
            List<PatientHabits> getItemsIds(int startIndex, int numberOfIds) {
                return patientHabitsList.subList(startIndex, (startIndex + numberOfIds))
            }

            @Override
            int getFilteredSize() {
                return 0
            }

            @Override
            List<PatientHabits> getFilteredItemsIds(int startIndex, int numberOfIds) {
                return new ArrayList<PatientHabits>()
            }
        }, (edit != null && !edit)))
        tableActionPatientHabits.getTable().setHeight(450, Sizeable.Unit.PIXELS)
        tableActionPatientHabits.getTable().setStyleName(ValoTheme.TABLE_SMALL)
        tableActionPatientHabits.getTable().setColumnHeader("habits", Languages.instance.getText("grid.column.habits"))
        tableActionPatientHabits.getTable().setColumnHeader("answer", Languages.instance.getText("grid.column.state"))
        tableActionPatientHabits.getTable().setColumnExpandRatio("habits", 0.50f)
        tableActionPatientHabits.getTable().setColumnExpandRatio("state", 0.50f)
        tableActionPatientHabits.removeColumnsDefault()
        tableActionPatientHabits.updateTable()
    }

    void addHabit() {
        List<Habits> listOfHabits = habitsService.list(true)
        for (Habits habits : listOfHabits) {
            if (!haveHabit(habits)) {
                patientHabitsList.add(new PatientHabits(habits: habits))
            }
        }
    }

    private boolean haveHabit(Habits habits) {
        for (PatientHabits patientHabits : patientHabitsList) {
            if (patientHabits.habits.id == habits.id) {
                return true
            }
        }
        return false
    }

    @Override
    protected void restore() {

    }

    @Override
    protected void visualize() {

    }

    @Override
    protected boolean getElementsValid() {

        return true
    }

    @Override
    protected void collectData() {
        itemSave = patientIllnessList
    }

    @Override
    protected void confLanguage() {
        Constants.instance.autoWiredClass(this)
        recordCardFormIllnessDesign.tableIllness.setCaption(Languages.instance.getText("window.patient.illness"))
        recordCardFormIllnessDesign.tableHabits.setCaption(Languages.instance.getText("window.patient.habits"))
    }

    @Override
    protected Boolean getValid() {
        return true
    }

    List<PatientIllness> validPatientIllnessList() {
        return patientIllnessList
    }

    List<PatientHabits> validPatientHabitsList() {
        return patientHabitsList
    }

    @Override
    void startFocus() {
    }
}
