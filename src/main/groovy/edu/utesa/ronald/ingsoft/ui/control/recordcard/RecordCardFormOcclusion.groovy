package edu.utesa.ronald.ingsoft.ui.control.recordcard

import com.vaadin.data.Property
import edu.utesa.ronald.ingsoft.components.abstracts.AWindowTab
import edu.utesa.ronald.ingsoft.domains.control.recordcard.Occlusion
import edu.utesa.ronald.ingsoft.models.enums.recordcard.ArcType
import edu.utesa.ronald.ingsoft.models.enums.recordcard.FacialProfile
import edu.utesa.ronald.ingsoft.services.control.recordcard.OcclusionService
import edu.utesa.ronald.ingsoft.utils.CBFiller
import edu.utesa.ronald.ingsoft.utils.Constants
import edu.utesa.ronald.ingsoft.utils.Languages
import groovy.transform.CompileStatic
import org.springframework.beans.factory.annotation.Autowired

/**
 *  Created by ronald on 3/07/17.
 */
@CompileStatic
class RecordCardFormOcclusion extends AWindowTab<Occlusion> {

    @Autowired
    private OcclusionService occlusionService

    private RecordCardFormOcclusionDesign recordCardFormOcclusionDesign = new RecordCardFormOcclusionDesign()

    private List<String> archLenght = Arrays.asList("Adecuada", "Inadecuada",)

    private ArcType arcType
    private FacialProfile facialProfile
    private String maxilarSup
    private String maxilarInf

    RecordCardFormOcclusion() {
        this(new Occlusion(), null)
    }

    RecordCardFormOcclusion(Occlusion occlusion, Boolean edit) {
        super(occlusion, edit)
        if (occlusion) {
            itemSave = occlusion
        } else {
            itemSave = new Occlusion()
        }
        confLayout(recordCardFormOcclusionDesign)
    }

    @Override
    protected void buildLayout() {
        CBFiller.fillFacialProfile(recordCardFormOcclusionDesign.ogFacialProfile)
        CBFiller.fillArcType(recordCardFormOcclusionDesign.ogArcType)
        CBFiller.fillArcLenght(recordCardFormOcclusionDesign.ogMaxilarInf, archLenght)
        CBFiller.fillArcLenght(recordCardFormOcclusionDesign.ogMaxilarSup, archLenght)

        recordCardFormOcclusionDesign.ogArcType.addValueChangeListener(new Property.ValueChangeListener() {
            @Override
            void valueChange(Property.ValueChangeEvent event) {
                if (recordCardFormOcclusionDesign.ogArcType.getValue()) {
                    arcType = recordCardFormOcclusionDesign.ogArcType.getValue() as ArcType
                } else {
                    arcType = null
                }
            }
        })

        recordCardFormOcclusionDesign.ogFacialProfile.addValueChangeListener(new Property.ValueChangeListener() {
            @Override
            void valueChange(Property.ValueChangeEvent event) {
                if (recordCardFormOcclusionDesign.ogFacialProfile.getValue()) {
                    facialProfile = recordCardFormOcclusionDesign.ogFacialProfile.getValue() as FacialProfile
                } else {
                    facialProfile = null
                }
            }
        })
        recordCardFormOcclusionDesign.ogMaxilarSup.addValueChangeListener(new Property.ValueChangeListener() {
            @Override
            void valueChange(Property.ValueChangeEvent event) {
                if (recordCardFormOcclusionDesign.ogMaxilarSup.getValue()) {
                    maxilarSup = recordCardFormOcclusionDesign.ogMaxilarSup.getValue()
                } else {
                    maxilarSup = null
                }
            }
        })
        recordCardFormOcclusionDesign.ogMaxilarInf.addValueChangeListener(new Property.ValueChangeListener() {
            @Override
            void valueChange(Property.ValueChangeEvent event) {
                if (recordCardFormOcclusionDesign.ogMaxilarInf.getValue()) {
                    maxilarInf = recordCardFormOcclusionDesign.ogMaxilarInf.getValue()
                } else {
                    maxilarInf = null
                }
            }
        })
    }

    @Override
    protected void restore() {
        if (itemSave) {
            recordCardFormOcclusionDesign.ogFacialProfile.setValue(itemSave.facialProfile)
            recordCardFormOcclusionDesign.tfMolarRelation.setValue(itemSave.molarRelation ?: "")
            recordCardFormOcclusionDesign.tfCanineRelation.setValue(itemSave.canineRelation ?: "")
            recordCardFormOcclusionDesign.tfEruptionSecuence.setValue(itemSave.eruptionSecuence ?: "")
            recordCardFormOcclusionDesign.tfCrossBite.setValue(itemSave.crossBite ?: "")
            recordCardFormOcclusionDesign.tfSupernumeraries.setValue(itemSave.supernumeraries ?: "")
            recordCardFormOcclusionDesign.tfCongenitalAbsences.setValue(itemSave.congenitalAbsences ?: "")
            recordCardFormOcclusionDesign.tfPrimateSpaces.setValue(itemSave.primateSpaces ?: "")
            recordCardFormOcclusionDesign.tfMiddleLine.setValue(itemSave.middleLine ?: "")
            recordCardFormOcclusionDesign.tfMandibularDeviation.setValue(itemSave.mandibularDeviation ?: "")
            recordCardFormOcclusionDesign.tfOpenBite.setValue(itemSave.openBite ?: "")
            recordCardFormOcclusionDesign.tfOverBite.setValue(itemSave.overBite ?: "")
            recordCardFormOcclusionDesign.tfOpenJet.setValue(itemSave.openJet ?: "")
            recordCardFormOcclusionDesign.ogArcType.setValue(itemSave.arcType)
            recordCardFormOcclusionDesign.ogMaxilarSup.setValue(itemSave.maxilarSup)
            recordCardFormOcclusionDesign.ogMaxilarInf.setValue(itemSave.maxilarInf)
        }

    }

    @Override
    protected void visualize() {
        recordCardFormOcclusionDesign.ogFacialProfile.setReadOnly(true)
        recordCardFormOcclusionDesign.tfMolarRelation.setReadOnly(true)
        recordCardFormOcclusionDesign.tfCanineRelation.setReadOnly(true)
        recordCardFormOcclusionDesign.tfEruptionSecuence.setReadOnly(true)
        recordCardFormOcclusionDesign.tfCrossBite.setReadOnly(true)
        recordCardFormOcclusionDesign.tfSupernumeraries.setReadOnly(true)
        recordCardFormOcclusionDesign.tfCongenitalAbsences.setReadOnly(true)
        recordCardFormOcclusionDesign.tfPrimateSpaces.setReadOnly(true)
        recordCardFormOcclusionDesign.tfMiddleLine.setReadOnly(true)
        recordCardFormOcclusionDesign.tfMandibularDeviation.setReadOnly(true)
        recordCardFormOcclusionDesign.tfOpenBite.setReadOnly(true)
        recordCardFormOcclusionDesign.tfOverBite.setReadOnly(true)
        recordCardFormOcclusionDesign.tfOpenJet.setReadOnly(true)
        recordCardFormOcclusionDesign.ogArcType.setReadOnly(true)
        recordCardFormOcclusionDesign.ogMaxilarSup.setReadOnly(true)
        recordCardFormOcclusionDesign.ogMaxilarInf.setReadOnly(true)
        recordCardFormOcclusionDesign.hlArcLenght.setReadOnly(true)

    }

    @Override
    protected boolean getElementsValid() {
        return true
    }

    @Override
    protected void collectData() {
        if (facialProfile) {
            itemSave.facialProfile = facialProfile as FacialProfile
        }
        itemSave.molarRelation = recordCardFormOcclusionDesign.tfMolarRelation.getValue()
        itemSave.canineRelation = recordCardFormOcclusionDesign.tfCanineRelation.getValue()
        itemSave.eruptionSecuence = recordCardFormOcclusionDesign.tfEruptionSecuence.getValue()
        itemSave.crossBite = recordCardFormOcclusionDesign.tfCrossBite.getValue()
        itemSave.supernumeraries = recordCardFormOcclusionDesign.tfSupernumeraries.getValue()
        itemSave.congenitalAbsences = recordCardFormOcclusionDesign.tfCongenitalAbsences.getValue()
        itemSave.primateSpaces = recordCardFormOcclusionDesign.tfPrimateSpaces.getValue()
        itemSave.middleLine = recordCardFormOcclusionDesign.tfMiddleLine.getValue()
        itemSave.mandibularDeviation = recordCardFormOcclusionDesign.tfMandibularDeviation.getValue()
        itemSave.openBite = recordCardFormOcclusionDesign.tfOpenBite.getValue()
        itemSave.overBite = recordCardFormOcclusionDesign.tfOverBite.getValue()
        itemSave.openJet = recordCardFormOcclusionDesign.tfOpenJet.getValue()
        if (arcType) {
            itemSave.arcType = arcType as ArcType
        }
        if (maxilarSup) {
            itemSave.maxilarSup = maxilarSup
        }
        if (maxilarInf) {
            itemSave.maxilarInf = maxilarInf
        }
    }

    @Override
    protected void confLanguage() {
        Constants.instance.autoWiredClass(this)

        recordCardFormOcclusionDesign.ogFacialProfile.setCaption(Languages.instance.getText("window.facialProfile"))
        recordCardFormOcclusionDesign.tfMolarRelation.setCaption(Languages.instance.getText("window.molarRelation"))
        recordCardFormOcclusionDesign.tfCanineRelation.setCaption(Languages.instance.getText("window.canineRelation"))
        recordCardFormOcclusionDesign.tfEruptionSecuence.setCaption(Languages.instance.getText("window.eruptionSecuence"))
        recordCardFormOcclusionDesign.tfCrossBite.setCaption(Languages.instance.getText("window.crossBite"))
        recordCardFormOcclusionDesign.tfSupernumeraries.setCaption(Languages.instance.getText("window.supernumeraries"))
        recordCardFormOcclusionDesign.tfCongenitalAbsences.setCaption(Languages.instance.getText("window.congenitalAbsences"))
        recordCardFormOcclusionDesign.tfPrimateSpaces.setCaption(Languages.instance.getText("window.primateSpaces"))
        recordCardFormOcclusionDesign.tfMiddleLine.setCaption(Languages.instance.getText("window.middleLine"))
        recordCardFormOcclusionDesign.tfMandibularDeviation.setCaption(Languages.instance.getText("window.mandibularDeviation"))
        recordCardFormOcclusionDesign.tfOpenBite.setCaption(Languages.instance.getText("window.openBite"))
        recordCardFormOcclusionDesign.tfOverBite.setCaption(Languages.instance.getText("window.overBite"))
        recordCardFormOcclusionDesign.tfOpenJet.setCaption(Languages.instance.getText("window.openJet"))
        recordCardFormOcclusionDesign.ogArcType.setCaption(Languages.instance.getText("window.arcType"))
        recordCardFormOcclusionDesign.ogMaxilarSup.setCaption(Languages.instance.getText("window.maxilarSup"))
        recordCardFormOcclusionDesign.ogMaxilarInf.setCaption(Languages.instance.getText("window.maxilarInf"))
        recordCardFormOcclusionDesign.hlArcLenght.setCaption(Languages.instance.getText("window.arc.lenght"))

    }

    @Override
    protected Occlusion getValid() {
        if (getElementsValid()) {
            collectData()
            return itemSave
        }
        return null
    }

    @Override
    void startFocus() {
        recordCardFormOcclusionDesign.ogFacialProfile.focus()
    }
}
