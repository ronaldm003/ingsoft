package edu.utesa.ronald.ingsoft.ui.control.recordcard

import com.vaadin.data.Property
import com.vaadin.event.ContextClickEvent
import com.vaadin.ui.Button
import com.vaadin.ui.themes.ValoTheme
import edu.utesa.ronald.ingsoft.components.abstracts.AWindowTab
import edu.utesa.ronald.ingsoft.domains.admin.Cycle
import edu.utesa.ronald.ingsoft.domains.admin.Student
import edu.utesa.ronald.ingsoft.domains.control.Patient
import edu.utesa.ronald.ingsoft.domains.control.recordcard.RecordCard
import edu.utesa.ronald.ingsoft.domains.security.Person
import edu.utesa.ronald.ingsoft.domains.utils.Occupation
import edu.utesa.ronald.ingsoft.models.enums.Behavior
import edu.utesa.ronald.ingsoft.models.enums.Complexion
import edu.utesa.ronald.ingsoft.models.interfaces.OnNew
import edu.utesa.ronald.ingsoft.models.interfaces.PickElement
import edu.utesa.ronald.ingsoft.models.suggestions.PersonSuggestions
import edu.utesa.ronald.ingsoft.models.suggestions.StudentSuggestions
import edu.utesa.ronald.ingsoft.services.admin.CycleService
import edu.utesa.ronald.ingsoft.services.admin.StudentService
import edu.utesa.ronald.ingsoft.services.control.PatientService
import edu.utesa.ronald.ingsoft.services.control.recordcard.RecordCardService
import edu.utesa.ronald.ingsoft.services.security.PersonService
import edu.utesa.ronald.ingsoft.services.utils.OccupationService
import edu.utesa.ronald.ingsoft.ui.admin.StudentForm
import edu.utesa.ronald.ingsoft.ui.components.forms.OneToOneAction
import edu.utesa.ronald.ingsoft.ui.security.PersonForm
import edu.utesa.ronald.ingsoft.ui.utils.pickers.CyclePicker
import edu.utesa.ronald.ingsoft.ui.utils.pickers.PersonPicker
import edu.utesa.ronald.ingsoft.ui.utils.pickers.StudentPicker
import edu.utesa.ronald.ingsoft.utils.CBFiller
import edu.utesa.ronald.ingsoft.utils.Constants
import edu.utesa.ronald.ingsoft.utils.Languages
import edu.utesa.ronald.ingsoft.utils.TimeTool
import groovy.transform.CompileStatic
import org.springframework.beans.factory.annotation.Autowired

/**
 *  Created by ronald on 3/07/17.
 */
@CompileStatic
class RecordCardFormRecordCard extends AWindowTab<RecordCard> {

    @Autowired
    private RecordCardService recordCardService
    @Autowired
    private PersonService personService
    @Autowired
    private PatientService patientService
    @Autowired
    private StudentService studentService
    @Autowired
    private CycleService cycleService
    @Autowired
    private OccupationService occupationService

    private RecordCardFormRecordCardDesign recordCardFormRecordCardDesign = new RecordCardFormRecordCardDesign()

    private OneToOneAction oneToOneActionPerson = new OneToOneAction(false)
    private OneToOneAction oneToOneActionStudent = new OneToOneAction(false)

    private Patient patientSelect
    private Cycle cycleSelect
    private Person personSelect
    private Student studentSelect

    private Occupation motherOccupationSelect
    private Occupation fatherOccupationSelect
    private List<String> brothers = Arrays.asList("1", "2", "3", "Mas")

    RecordCardFormRecordCard() {
        this(new RecordCard(), null)
    }

    RecordCardFormRecordCard(RecordCard recordCard, Boolean edit) {
        super(recordCard, edit)
        confLayout(recordCardFormRecordCardDesign)
    }

    @Override
    protected void buildLayout() {
        getNextCode()
        initCycle()
        initStudent()
        initPerson()
        CBFiller.fillWarrantType(recordCardFormRecordCardDesign.cbWarrantType)
        CBFiller.fillGender(recordCardFormRecordCardDesign.cbGender)
        List<Occupation> occupationList = occupationService.list(true)
        CBFiller.fillBrothers(recordCardFormRecordCardDesign.cbBrothers, brothers)
        CBFiller.fillOccupation(recordCardFormRecordCardDesign.cbFatherOccupation, occupationList)
        CBFiller.fillOccupation(recordCardFormRecordCardDesign.cbMotherOccupation, occupationList)
        recordCardFormRecordCardDesign.cbFatherOccupation.addValueChangeListener(new Property.ValueChangeListener() {
            @Override
            void valueChange(Property.ValueChangeEvent event) {
                if (recordCardFormRecordCardDesign.cbFatherOccupation.getValue()) {
                    fatherOccupationSelect = occupationService.byDescription(recordCardFormRecordCardDesign.cbFatherOccupation.getValue().toString())
                } else {
                    fatherOccupationSelect = null
                }
            }
        })
        recordCardFormRecordCardDesign.cbMotherOccupation.addValueChangeListener(new Property.ValueChangeListener() {
            @Override
            void valueChange(Property.ValueChangeEvent event) {
                if (recordCardFormRecordCardDesign.cbMotherOccupation.getValue()) {
                    motherOccupationSelect = occupationService.byDescription(recordCardFormRecordCardDesign.cbMotherOccupation.getValue().toString())
                } else {
                    motherOccupationSelect = null
                }
            }
        })

        enableMedAlertsField(false)
        CBFiller.fillNationality(recordCardFormRecordCardDesign.cbNationality)
        CBFiller.fillBehavior(recordCardFormRecordCardDesign.cbBehavior)
        CBFiller.fillComplexion(recordCardFormRecordCardDesign.cbComplexion)
        recordCardFormRecordCardDesign.chkBrother.addValueChangeListener(new Property.ValueChangeListener() {
            @Override
            void valueChange(Property.ValueChangeEvent event) {
                Boolean enabled = (Boolean) event.property.getValue()
                enableBrotherField(enabled)
            }
        })
        recordCardFormRecordCardDesign.chkMedAlert.addValueChangeListener(new Property.ValueChangeListener() {
            @Override
            void valueChange(Property.ValueChangeEvent event) {
                Boolean enabled = (Boolean) event.property.getValue()
                enableMedAlertsField(enabled)
            }
        })
        recordCardFormRecordCardDesign.chkPediatritian.addValueChangeListener(new Property.ValueChangeListener() {
            @Override
            void valueChange(Property.ValueChangeEvent event) {
                Boolean enabled = (Boolean) event.property.getValue()
                enablePediatritianField(enabled)
            }
        })
        recordCardFormRecordCardDesign.ckParentsLiveTogether.addValueChangeListener(new Property.ValueChangeListener() {
            @Override
            void valueChange(Property.ValueChangeEvent event) {
                Boolean enabled = (Boolean) event.property.getValue()
                enableParentsLiveTogetherField(enabled)
            }
        })
        setChildrenFields(false)
    }

    private void enableBrotherField(boolean yes) {
        recordCardFormRecordCardDesign.cbBrothers.setVisible(yes)
    }

    private void enableMedAlertsField(boolean yes) {
        recordCardFormRecordCardDesign.cbMedAlert.setVisible(yes)
        recordCardFormRecordCardDesign.tfMedAlert.setVisible(yes)
    }

    private void enablePediatritianField(boolean yes) {
        recordCardFormRecordCardDesign.tfPediatritian.setVisible(yes)
    }

    private void enableParentsLiveTogetherField(boolean yes) {
        recordCardFormRecordCardDesign.tfParentsLiveTogether.setVisible(yes)
    }

    private void getNextCode() {
        Long code = recordCardService.findMaxCode()
        if (code) {
            recordCardFormRecordCardDesign.tfRecordCardNumber.setValue((code + 1).toString())
            recordCardFormRecordCardDesign.tfRecordCardNumber.setReadOnly(true)
        } else {
            recordCardFormRecordCardDesign.tfRecordCardNumber.setValue("1")
            recordCardFormRecordCardDesign.tfRecordCardNumber.setReadOnly(true)
        }
    }

    private void initPerson() {
        recordCardFormRecordCardDesign.pPatient.setContent(oneToOneActionPerson)
        oneToOneActionPerson.tfACSearch().setSuggestionProvider(new PersonSuggestions())
        oneToOneActionPerson.tfACSearch().addValueChangeListener(new Property.ValueChangeListener() {
            @Override
            void valueChange(Property.ValueChangeEvent event) {
                if (oneToOneActionPerson.tfACSearch().getValue() && !oneToOneActionPerson.tfACSearch().getValue().isEmpty()) {
                    try {
                        personSelect = personService.byName(oneToOneActionPerson.tfACSearch().getValue().split("-")[0], {
                            it.educationLevel.enabled; it.occupation.enabled; it.location.enabled
                        })
                        setPersonFieldValues(personSelect)
                        selectPatient(personSelect)
                    } catch (Exception ignored) {
                    }
                } else {
                    personSelect = null
                }
            }
        })
        oneToOneActionPerson.btnNew().addClickListener(new Button.ClickListener() {
            @Override
            void buttonClick(Button.ClickEvent event) {
                PersonForm occupationForm = new PersonForm(new OnNew<Person>() {
                    @Override
                    void result(Person person) {
                        personSelect = person
                        selectPatient(personSelect)
                        setPersonFieldValues(personSelect)
                    }
                })
                getUI().addWindow(occupationForm)
            }
        })
        oneToOneActionPerson.btnSearch().addClickListener(new Button.ClickListener() {
            @Override
            void buttonClick(Button.ClickEvent event) {
                PersonPicker locationPicker = new PersonPicker(new PickElement<Person>() {
                    @Override
                    void onResult(List<Person> elements) {
                        if (elements.size() > 0) {
                            personSelect = elements.get(0)
                            selectPatient(personSelect)
                            setPersonFieldValues(personSelect)
                        }
                    }
                }, false)
                getUI().addWindow(locationPicker)
            }
        })
    }

    private void selectPatient(Person person) {
        if (person) {
            patientSelect = patientService.byPerson(person)
            if (!patientSelect) {
                Patient patient = new Patient()
                patient.code = patientService.findMaxCode()
                patient.person = person
                patientSelect = patientService.create(patient, loginManager)
            }
        }
    }

    void selectPerson(Person person) {
        if (person) {
            oneToOneActionPerson.tfACSearch().setReadOnly(false)
            oneToOneActionPerson.select(person ? person.toString() : "")
            oneToOneActionPerson.tfACSearch().setReadOnly(true)
        }
    }

    private void setPersonFieldValues(Person person) {
        selectPerson(person)
        recordCardFormRecordCardDesign.tfName.setValue(person.name ?: "")
        recordCardFormRecordCardDesign.tfLastName.setValue(person.name ?: "")
        recordCardFormRecordCardDesign.tfCellPhone.setValue(person.cellphone ?: "")
        recordCardFormRecordCardDesign.tfEmail.setValue(person.email ?: "")
        recordCardFormRecordCardDesign.cbGender.setValue(person.gender)
        recordCardFormRecordCardDesign.cbNationality.setValue(person.nationality)
        recordCardFormRecordCardDesign.cbWarrantType.setValue(person.warrantType)
        recordCardFormRecordCardDesign.tfWarrant.setValue(person.warrant ?: "")
        recordCardFormRecordCardDesign.cbGender.setValue(person.gender)
        if (person.location) {
            recordCardFormRecordCardDesign.tfLocation.setValue(person.location.toString() ?: "")
        }
        if (person.occupation) {
            recordCardFormRecordCardDesign.tfOccupation.setValue(person.occupation.toString() ?: "")
        }
        recordCardFormRecordCardDesign.dfBirthday.setValue(person.birthDate ?: new Date())
        if (TimeTool.getAge(person.birthDate) < 18) {
            setChildrenFields(true)
        }
        recordCardFormRecordCardDesign.dfIngressDate.setValue(person.ingressDate ?: new Date())
    }

    private void setChildrenFields(boolean enable) {
        enableBrotherField(false)
        enableMedAlertsField(false)
        enableParentsLiveTogetherField(false)
        enablePediatritianField(false)
        recordCardFormRecordCardDesign.hlChildren.setVisible(enable)
        recordCardFormRecordCardDesign.hlMother.setVisible(enable)
        recordCardFormRecordCardDesign.hlFather.setVisible(enable)
        recordCardFormRecordCardDesign.hlBrothers.setVisible(enable)
        recordCardFormRecordCardDesign.hlPediatritian.setVisible(enable)
        recordCardFormRecordCardDesign.ckParentsLiveTogether.setVisible(enable)
        recordCardFormRecordCardDesign.tfSchool.setVisible(enable)
        recordCardFormRecordCardDesign.cbComplexion.setVisible(enable)
        recordCardFormRecordCardDesign.cbBehavior.setVisible(enable)
        recordCardFormRecordCardDesign.tfHeight.setValue(itemSave.height ?: "")
        recordCardFormRecordCardDesign.tfWeight.setValue(itemSave.weight ?: "")
        recordCardFormRecordCardDesign.tfICM.setValue(itemSave.icm ?: "")
        if (itemSave.medicalAlerts && !itemSave.medicalAlerts.isEmpty()) {
            recordCardFormRecordCardDesign.chkMedAlert.setValue(true)
            recordCardFormRecordCardDesign.tfMedAlert.setValue(itemSave.medicalAlerts)
            recordCardFormRecordCardDesign.cbMedAlert.setValue(itemSave.medicalAlerts)
        } else {
            recordCardFormRecordCardDesign.chkMedAlert.setValue(false)
        }
        recordCardFormRecordCardDesign.tfMotherName.setValue(itemSave.motherName ?: "")
        recordCardFormRecordCardDesign.tfFatherName.setValue(itemSave.fatherName ?: "")
        recordCardFormRecordCardDesign.cbBehavior.setValue(itemSave.behavior)
        recordCardFormRecordCardDesign.cbComplexion.setValue(itemSave.complexion)
        recordCardFormRecordCardDesign.tfSchool.setValue(itemSave.school ?: "")
        recordCardFormRecordCardDesign.cbMotherOccupation.setValue(itemSave.motherOccupation)
        recordCardFormRecordCardDesign.cbFatherOccupation.setValue(itemSave.fatherOccupation)
        if (itemSave.parentsLiveTogether && !itemSave.parentsLiveTogether.isEmpty()) {
            recordCardFormRecordCardDesign.ckParentsLiveTogether.setValue(true)
            recordCardFormRecordCardDesign.tfParentsLiveTogether.setValue(itemSave.parentsLiveTogether)
        } else {
            recordCardFormRecordCardDesign.ckParentsLiveTogether.setValue(false)
        }
        if (itemSave.brothers && !itemSave.brothers.isEmpty()) {
            recordCardFormRecordCardDesign.chkBrother.setValue(true)
            recordCardFormRecordCardDesign.cbBrothers.setValue(itemSave.brothers)
        } else {
            recordCardFormRecordCardDesign.chkBrother.setValue(false)
        }
        if (itemSave.pediatrician && !itemSave.pediatrician.isEmpty()) {
            recordCardFormRecordCardDesign.chkPediatritian.setValue(true)
            recordCardFormRecordCardDesign.tfPediatritian.setValue(itemSave.pediatrician)
        } else {
            recordCardFormRecordCardDesign.chkPediatritian.setValue(false)
        }
    }

    private void setPersonFieldReadOnly() {
        recordCardFormRecordCardDesign.tfName.setReadOnly(true)
        recordCardFormRecordCardDesign.tfLastName.setReadOnly(true)
        recordCardFormRecordCardDesign.tfCellPhone.setReadOnly(true)
        recordCardFormRecordCardDesign.tfEmail.setReadOnly(true)
        recordCardFormRecordCardDesign.cbGender.setReadOnly(true)
        recordCardFormRecordCardDesign.cbNationality.setReadOnly(true)
        recordCardFormRecordCardDesign.cbWarrantType.setReadOnly(true)
        recordCardFormRecordCardDesign.tfWarrant.setReadOnly(true)
        recordCardFormRecordCardDesign.cbGender.setReadOnly(true)
        recordCardFormRecordCardDesign.tfLocation.setReadOnly(true)
        recordCardFormRecordCardDesign.tfOccupation.setReadOnly(true)
        recordCardFormRecordCardDesign.dfBirthday.setReadOnly(true)
        recordCardFormRecordCardDesign.dfIngressDate.setReadOnly(true)
    }

    private void initStudent() {
        recordCardFormRecordCardDesign.pStudent.setContent(oneToOneActionStudent)
        oneToOneActionStudent.tfACSearch().setSuggestionProvider(new StudentSuggestions())
        oneToOneActionStudent.tfACSearch().addValueChangeListener(new Property.ValueChangeListener() {
            @Override
            void valueChange(Property.ValueChangeEvent event) {
                if (oneToOneActionStudent.tfACSearch().getValue() && !oneToOneActionStudent.tfACSearch().getValue().isEmpty()) {
                    try {
                        studentSelect = studentService.byEnrollment(oneToOneActionStudent.tfACSearch().getValue().split(":")[0], {
                            it.person.enabled; it.listSubjects.size()
                        })
                        selectStudent(studentSelect)
                    } catch (Exception ignored) {
                    }
                } else {
                    studentSelect = null
                }
            }
        })
        oneToOneActionStudent.btnNew().addClickListener(new Button.ClickListener() {
            @Override
            void buttonClick(Button.ClickEvent event) {
                StudentForm occupationForm = new StudentForm(new OnNew<Student>() {
                    @Override
                    void result(Student student) {
                        studentSelect = student
                        selectStudent(studentSelect)
                    }
                })
                getUI().addWindow(occupationForm)
            }
        })
        oneToOneActionStudent.btnSearch().addClickListener(new Button.ClickListener() {
            @Override
            void buttonClick(Button.ClickEvent event) {
                StudentPicker studentPicker = new StudentPicker(new PickElement<Student>() {
                    @Override
                    void onResult(List<Student> elements) {
                        if (elements.size() > 0) {
                            studentSelect = elements.get(0)
                            selectStudent(studentSelect)
                        }
                    }
                }, false)
                getUI().addWindow(studentPicker)
            }
        })
    }

    void selectStudent(Student student) {
        if (student) {
            oneToOneActionStudent.tfACSearch().setReadOnly(false)
            oneToOneActionStudent.select(student ? student.toString() : "")
            oneToOneActionStudent.tfACSearch().setReadOnly(true)
        }
    }

    private void initCycle() {
        recordCardFormRecordCardDesign.tfCycle.setImmediate(true)
        recordCardFormRecordCardDesign.tfCycle.addContextClickListener(new ContextClickEvent.ContextClickListener() {
            @Override
            void contextClick(ContextClickEvent event) {
                CyclePicker cyclePicker = new CyclePicker(new PickElement<Cycle>() {
                    @Override
                    void onResult(List<Cycle> elements) {
                        if (elements.size() > 0) {
                            cycleSelect = elements.get(0)
                            selectCycle(cycleSelect)
                        }
                    }
                }, false)
                getUI().addWindow(cyclePicker)
            }
        })
    }

    void selectCycle(Cycle cycle) {
        if (cycle) {
            recordCardFormRecordCardDesign.tfCycle.setReadOnly(false)
            recordCardFormRecordCardDesign.tfCycle.setValue(cycle ? cycle.toString() : "")
            recordCardFormRecordCardDesign.tfCycle.setReadOnly(true)
        }
    }

    @Override
    protected void restore() {
        personSelect = itemSave.patient.person
        if (personSelect) {
            setPersonFieldValues(personSelect)
            setPersonFieldReadOnly()
        }
        studentSelect = itemSave.student
        if (studentSelect) {
            selectStudent(studentSelect)
        }
        recordCardFormRecordCardDesign.tfRecordCardNumber.setReadOnly(false)
        recordCardFormRecordCardDesign.tfRecordCardNumber.setValue(itemSave.recordId.toString())
        recordCardFormRecordCardDesign.tfRecordCardNumber.setReadOnly(true)
        recordCardFormRecordCardDesign.tfCycle.setReadOnly(false)
        recordCardFormRecordCardDesign.tfCycle.setValue(itemSave.cycle.toString())
        recordCardFormRecordCardDesign.tfCycle.setReadOnly(true)
        recordCardFormRecordCardDesign.dfIngressDate.setValue(itemSave.ingressDate ?: new Date())

    }

    @Override
    protected void visualize() {
        recordCardFormRecordCardDesign.tfRecordCardNumber.setReadOnly(false)
        recordCardFormRecordCardDesign.tfCycle.setReadOnly(true)
        recordCardFormRecordCardDesign.dfIngressDate.setReadOnly(true)
        oneToOneActionStudent.visualize()
        oneToOneActionPerson.visualize()
    }

    @Override
    protected boolean getElementsValid() {
        if (!recordCardFormRecordCardDesign.dfIngressDate.isValid()) {
            recordCardFormRecordCardDesign.dfIngressDate.focus()
            return false
        } else if (!oneToOneActionStudent.isValid()) {
            oneToOneActionStudent.focusValid()
            return false
        } else if (!oneToOneActionPerson.isValid()) {
            oneToOneActionPerson.focusValid()
            return false
        }
        return true
    }

    @Override
    protected void collectData() {
        itemSave.cycle = cycleSelect
        itemSave.recordId = recordCardFormRecordCardDesign.tfRecordCardNumber.getValue().toLong()
        itemSave.ingressDate = recordCardFormRecordCardDesign.dfIngressDate.getValue()
        itemSave.student = studentSelect
        itemSave.patient = patientSelect
        itemSave.fatherName = recordCardFormRecordCardDesign.tfFatherName.getValue()
        itemSave.fatherOccupation = fatherOccupationSelect
        itemSave.motherName = recordCardFormRecordCardDesign.tfMotherName.getValue()
        itemSave.motherOccupation = motherOccupationSelect
        itemSave.medicalAlerts = recordCardFormRecordCardDesign.tfMedAlert.getValue()
        itemSave.parentsLiveTogether = recordCardFormRecordCardDesign.tfParentsLiveTogether.getValue()
        itemSave.brothers = recordCardFormRecordCardDesign.cbBrothers.getValue()
        itemSave.pediatrician = recordCardFormRecordCardDesign.tfPediatritian.getValue()
        itemSave.school = recordCardFormRecordCardDesign.tfSchool.getValue()
        itemSave.behavior = recordCardFormRecordCardDesign.cbBehavior.getValue() as Behavior
        itemSave.complexion = recordCardFormRecordCardDesign.cbComplexion.getValue() as Complexion
        itemSave.height = recordCardFormRecordCardDesign.tfHeight.getValue()
        itemSave.weight = recordCardFormRecordCardDesign.tfWeight.getValue()
        itemSave.icm = recordCardFormRecordCardDesign.tfICM.getValue()
    }

    @Override
    protected void confLanguage() {
        Constants.instance.autoWiredClass(this)
        cycleSelect = cycleService.findLatestCycle()

        recordCardFormRecordCardDesign.tfCycle.addStyleName(ValoTheme.TEXTFIELD_TINY)
        recordCardFormRecordCardDesign.tfRecordCardNumber.addStyleName(ValoTheme.TEXTFIELD_TINY)
        recordCardFormRecordCardDesign.tfName.addStyleName(ValoTheme.TEXTFIELD_TINY)
        recordCardFormRecordCardDesign.tfLastName.addStyleName(ValoTheme.TEXTFIELD_TINY)
        recordCardFormRecordCardDesign.tfPhone.addStyleName(ValoTheme.TEXTFIELD_TINY)
        recordCardFormRecordCardDesign.tfCellPhone.addStyleName(ValoTheme.TEXTFIELD_TINY)
        recordCardFormRecordCardDesign.tfLocation.addStyleName(ValoTheme.TEXTFIELD_TINY)
        recordCardFormRecordCardDesign.tfEmail.addStyleName(ValoTheme.TEXTFIELD_TINY)
        recordCardFormRecordCardDesign.tfWarrant.addStyleName(ValoTheme.TEXTFIELD_TINY)
        recordCardFormRecordCardDesign.tfOccupation.addStyleName(ValoTheme.TEXTFIELD_TINY)

        recordCardFormRecordCardDesign.cbNationality.addStyleName(ValoTheme.COMBOBOX_TINY)
        recordCardFormRecordCardDesign.cbWarrantType.addStyleName(ValoTheme.COMBOBOX_TINY)
        recordCardFormRecordCardDesign.cbGender.addStyleName(ValoTheme.COMBOBOX_TINY)

        recordCardFormRecordCardDesign.dfIngressDate.addStyleName(ValoTheme.DATEFIELD_TINY)
        recordCardFormRecordCardDesign.dfBirthday.addStyleName(ValoTheme.DATEFIELD_TINY)

        recordCardFormRecordCardDesign.tfCycle.setCaption(Languages.instance.getText("window.cycle"))
        recordCardFormRecordCardDesign.tfCycle.setValue(cycleSelect.toString() ?: "")
        recordCardFormRecordCardDesign.tfCycle.setReadOnly(true)
        recordCardFormRecordCardDesign.tfRecordCardNumber.setCaption(Languages.instance.getText("window.recodId"))
        oneToOneActionPerson.setCaption(Languages.instance.getText("window.patient"))
        oneToOneActionStudent.setCaption(Languages.instance.getText("window.student"))

        recordCardFormRecordCardDesign.tfName.setCaption(Languages.instance.getText("window.name"))
        recordCardFormRecordCardDesign.tfLastName.setCaption(Languages.instance.getText("window.lastname"))
        recordCardFormRecordCardDesign.tfCellPhone.setCaption(Languages.instance.getText("window.cellphone"))
        recordCardFormRecordCardDesign.tfEmail.setCaption(Languages.instance.getText("window.email"))
        recordCardFormRecordCardDesign.cbGender.setCaption(Languages.instance.getText("window.gender"))
        recordCardFormRecordCardDesign.cbWarrantType.setCaption(Languages.instance.getText("window.warrant.type"))
        recordCardFormRecordCardDesign.cbNationality.setCaption(Languages.instance.getText("window.nationality"))
        recordCardFormRecordCardDesign.tfLocation.setCaption(Languages.instance.getText("window.location"))
        recordCardFormRecordCardDesign.tfOccupation.setCaption(Languages.instance.getText("window.occupation"))
        recordCardFormRecordCardDesign.tfWarrant.setCaption(Languages.instance.getText("window.warrant"))
        recordCardFormRecordCardDesign.tfPhone.setCaption(Languages.instance.getText("window.phone"))
        recordCardFormRecordCardDesign.dfBirthday.setCaption(Languages.instance.getText("window.birthday"))
        recordCardFormRecordCardDesign.dfIngressDate.setCaption(Languages.instance.getText("window.ingress.date"))

        recordCardFormRecordCardDesign.tfHeight.setCaption(Languages.instance.getText("window.height"))
        recordCardFormRecordCardDesign.tfWeight.setCaption(Languages.instance.getText("window.weight"))
        recordCardFormRecordCardDesign.tfICM.setCaption(Languages.instance.getText("window.icm"))
        recordCardFormRecordCardDesign.tfMedAlert.setCaption(Languages.instance.getText("window.medical.alert"))
        recordCardFormRecordCardDesign.tfMotherName.setCaption(Languages.instance.getText("window.mother"))
        recordCardFormRecordCardDesign.tfFatherName.setCaption(Languages.instance.getText("window.father"))
        recordCardFormRecordCardDesign.tfSchool.setCaption(Languages.instance.getText("window.school"))
        recordCardFormRecordCardDesign.tfPediatritian.setCaption(Languages.instance.getText("window.pediatritian"))
        recordCardFormRecordCardDesign.cbBehavior.setCaption(Languages.instance.getText("window.behavior"))
        recordCardFormRecordCardDesign.cbComplexion.setCaption(Languages.instance.getText("window.complexion"))
        recordCardFormRecordCardDesign.cbMedAlert.setCaption(Languages.instance.getText("window.medical.alert"))
        recordCardFormRecordCardDesign.cbMotherOccupation.setCaption(Languages.instance.getText("window.occupation"))
        recordCardFormRecordCardDesign.cbFatherOccupation.setCaption(Languages.instance.getText("window.occupation"))
        recordCardFormRecordCardDesign.cbBrothers.setCaption(Languages.instance.getText("window.brothers"))
        recordCardFormRecordCardDesign.chkMedAlert.setCaption(Languages.instance.getText("window.medical.alert.chk"))
        recordCardFormRecordCardDesign.ckParentsLiveTogether.setCaption(Languages.instance.getText("window.parent.live.together.chk"))
        recordCardFormRecordCardDesign.tfParentsLiveTogether.setCaption(Languages.instance.getText("window.parent.live.together"))
        recordCardFormRecordCardDesign.chkBrother.setCaption(Languages.instance.getText("window.brothers.chk"))
        recordCardFormRecordCardDesign.chkPediatritian.setCaption(Languages.instance.getText("window.pediatritian.chk"))
    }

    @Override
    protected RecordCard getValid() {
        if (getElementsValid()) {
            collectData()
            return itemSave
        }
        return null
    }

    @Override
    void startFocus() {
        oneToOneActionStudent.focusValid()
    }
}
