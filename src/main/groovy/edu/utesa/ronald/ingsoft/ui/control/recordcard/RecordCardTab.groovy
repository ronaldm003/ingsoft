package edu.utesa.ronald.ingsoft.ui.control.recordcard

import edu.utesa.ronald.ingsoft.domains.control.recordcard.RecordCard
import edu.utesa.ronald.ingsoft.models.containers.control.recordcard.RecordCardContainer
import edu.utesa.ronald.ingsoft.services.control.recordcard.RecordCardService
import edu.utesa.ronald.ingsoft.ui.components.ATabBase
import edu.utesa.ronald.ingsoft.utils.Constants
import edu.utesa.ronald.ingsoft.utils.container.LazyQuery
import groovy.transform.CompileStatic
import org.springframework.beans.factory.annotation.Autowired

/**
 * Aqui se lista todos los tipos de usuario
 *  Created by ronald on 3/07/17.
 */
@CompileStatic
class RecordCardTab extends ATabBase<RecordCardForm, RecordCard> {

    @Autowired
    private RecordCardService recordCardService

    RecordCardTab() {
        Constants.instance.autoWiredClass(this)
    }

    @Override
    protected void confLanguage() {

    }

    @Override
    protected void buildLayout() {
        manageGrid(new RecordCardContainer(new LazyQuery<RecordCard>() {
            @Override
            int getSize() {
                return recordCardService.count(showDelete())
            }

            @Override
            List<RecordCard> getItemsIds(int startIndex, int numberOfIds) {
                return recordCardService.list(showDelete(), startIndex, numberOfIds, {
                    it.patient?.enabled;
                    it.patient?.person?.enabled;
                    it.cycle?.enabled;
                    it.student?.enabled;
                    it.teacher?.enabled;
                    it.patient?.person?.location?.enabled;
                    it.patient?.person?.occupation?.enabled;
                    it.patient?.person?.educationLevel?.enabled;
                    it.student?.person?.enabled;
                    it.teacher?.person?.enabled;
                    it.occlusion?.enabled;
                    it.listPatientEvaluationAnswer.size()
                    it.listCariesIndex.size()
                    it.listPatientExtraBuccalAnswer.size()
                    it.listPatientIntraBuccalAnswer.size()
                    it.listMedObservations.size()
                    it.listOralHygieneIndex.size()
                    it.listPatientHabits.size()
                    it.listPatientIllness.size()
                    it.listToothGroup.size()
                    it.oralHygiene?.enabled;
                    it.motherOccupation?.enabled;
                    it.fatherOccupation?.enabled
                })
            }

            @Override
            int getFilteredSize() {
                return 0
            }

            @Override
            List<RecordCard> getFilteredItemsIds(int startIndex, int numberOfIds) {
                return null
            }
        }))
    }

    @Override
    void refreshItem() {

    }

    @Override
    RecordCardForm onNew() {
        return new RecordCardForm()
    }

    @Override
    RecordCardForm onEdit() {
        return new RecordCardForm(itemSelect, true)
    }

    @Override
    RecordCardForm onView() {
        return new RecordCardForm(itemSelect, false)
    }

    @Override
    Class topClass() {
        return null
    }

    @Override
    boolean deleteDomain() {
        return recordCardService.delete(itemSelect, loginManager)
    }

    @Override
    protected void security() {

    }
}
