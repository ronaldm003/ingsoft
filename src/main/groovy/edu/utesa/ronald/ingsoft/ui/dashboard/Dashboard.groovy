package edu.utesa.ronald.ingsoft.ui.dashboard

import edu.utesa.ronald.ingsoft.components.TabSheetManager
import edu.utesa.ronald.ingsoft.components.abstracts.APanel
import edu.utesa.ronald.ingsoft.utils.Languages
import groovy.transform.CompileStatic

/**
 *  Created by ronald on 7/11/17.
 */
@CompileStatic
class Dashboard extends APanel {

    private DashboardDesign dashboardDesign = new DashboardDesign()

    private TabSheetManager tabSheetManager = new TabSheetManager(dashboardDesign)

    Dashboard() {
        confLayout(dashboardDesign)
    }

    @Override
    protected void confLanguage() {
    }

    @Override
    protected void buildLayout() {
        tabSheetManager.addTabPanel(Languages.instance.getText("dashboard.news"), new DashboardNews())
        tabSheetManager.addTabPanel(Languages.instance.getText("dashboard.appointments"), new DashboardTabCalendar())
//        tabSheetManager.addTabPanel(Languages.instance.getText("dashboard.user.consults"), new UserConsultsTab())
        dashboardDesign.setSelectedTab(0)
    }

    @Override
    protected void security() {
    }
}
