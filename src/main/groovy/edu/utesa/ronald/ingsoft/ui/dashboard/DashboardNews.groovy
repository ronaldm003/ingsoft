package edu.utesa.ronald.ingsoft.ui.dashboard

import com.vaadin.addon.contextmenu.ContextMenu
import com.vaadin.addon.contextmenu.Menu
import com.vaadin.addon.contextmenu.MenuItem
import com.vaadin.server.Responsive
import com.vaadin.server.Sizeable
import com.vaadin.ui.*
import com.vaadin.ui.themes.ValoTheme
import edu.utesa.ronald.ingsoft.components.abstracts.APanel
import edu.utesa.ronald.ingsoft.domains.location.Location
import edu.utesa.ronald.ingsoft.models.constants.Styles
import edu.utesa.ronald.ingsoft.models.containers.DocumentsLocationContainer
import edu.utesa.ronald.ingsoft.models.containers.UserLocationContainer
import edu.utesa.ronald.ingsoft.services.location.LocationService
import edu.utesa.ronald.ingsoft.services.security.UserKeysService
import edu.utesa.ronald.ingsoft.services.security.UserService
import edu.utesa.ronald.ingsoft.ui.dashboard.components.BusinessStatusBar
import edu.utesa.ronald.ingsoft.ui.dashboard.components.charts.*
import edu.utesa.ronald.ingsoft.ui.dashboard.components.charts.models.DocumentsLocation
import edu.utesa.ronald.ingsoft.ui.dashboard.components.charts.models.UserLocation
import edu.utesa.ronald.ingsoft.utils.Constants
import edu.utesa.ronald.ingsoft.utils.Languages
import edu.utesa.ronald.ingsoft.utils.container.LazyQuery
import edu.utesa.ronald.ingsoft.utils.patterns.ObserverMaster
import groovy.transform.CompileStatic
import org.springframework.beans.factory.annotation.Autowired

/**
 *  Created by ronald on 4/01/17.
 */
@SuppressWarnings(["ChangeToOperator", "ChangeToOperator"])
@CompileStatic
class DashboardNews extends APanel implements ObserverMaster.ObservableListener {

    @Autowired
    private UserService userService
    @Autowired
    private UserKeysService userKeysService
    @Autowired
    private LocationService locationService

    private List<Location> locationList

    private DashboardNewsDesign dashboardNewsDesign = new DashboardNewsDesign()

    private ContextMenu contextMenu
    private CssLayout dashboardPanels

    private Button btnAddDocument
    private Button btnDocumentRequest
    private Button btnDocumentValidation

    DashboardNews() {
        confLayout(dashboardNewsDesign)
    }

    @Override
    protected void confLanguage() {
        ObserverMaster.get().add(this)
        Constants.instance.autoWiredClass(this)
        locationList = locationService.list(true)
    }

    @Override
    protected void buildLayout() {
        dashboardNewsDesign.addStyleName("dashboard")
        Responsive.makeResponsive(dashboardNewsDesign)
        dashboardNewsDesign.addComponent(buildHeader())
        dashboardNewsDesign.addComponent(buildSparklines())
        Component content = buildContent()
        dashboardNewsDesign.addComponent(content)
        contextMenu = new ContextMenu(this, true)
        contextMenu.addItem(Languages.instance.getText("window.action.refresh"), new Menu.Command() {
            @Override
            void menuSelected(MenuItem selectedItem) {
                update()
            }
        })
    }

    private static Component buildHeader() {
        HorizontalLayout header = new HorizontalLayout()
        header.addStyleName("viewheader")
        header.setSpacing(false)
//        btnAddDocument = new Button(Languages.instance.getText("dashboard.news.toolbar.add.document"))
//        btnAddDocument.addStyleName(ValoTheme.BUTTON_BORDERLESS_COLORED)
//        btnAddDocument.addClickListener(new Button.ClickListener() {
//            @Override
//            void buttonClick(Button.ClickEvent event) {
//                DocumentsForm documentsForm = new DocumentsForm()
//                getUI().addWindow(documentsForm)
//                ObserverMaster.get().fire()
//            }
//        })
//        btnDocumentRequest = new Button(Languages.instance.getText("dashboard.news.toolbar.request.document"))
//        btnDocumentRequest.addStyleName(ValoTheme.BUTTON_BORDERLESS_COLORED)
//        btnDocumentRequest.addClickListener(new Button.ClickListener() {
//            @Override
//            void buttonClick(Button.ClickEvent event) {
//                UserKeysWindow userKeysWindow = new UserKeysWindow(true)
//                getUI().addWindow(userKeysWindow)
//            }
//        })
//        btnDocumentValidation = new Button(Languages.instance.getText("dashboard.news.toolbar.validate.document"))
//        btnDocumentValidation.addStyleName(ValoTheme.BUTTON_BORDERLESS_COLORED)
//        btnDocumentValidation.addClickListener(new Button.ClickListener() {
//            @Override
//            void buttonClick(Button.ClickEvent event) {
//                UserKeysWindow userKeysWindow = new UserKeysWindow(false)
//                getUI().addWindow(userKeysWindow)
//            }
//        })

        HorizontalLayout tools = new HorizontalLayout()
//        tools.addComponent(btnAddDocument)
//        tools.addComponent(btnDocumentRequest)
//        tools.addComponent(btnDocumentValidation)
        tools.setSpacing(false)
        tools.addStyleName("toolbar")
        header.addComponent(tools)

        return header
    }

    private Component buildSparklines() {
        CssLayout sparks = new CssLayout()
        sparks.addStyleName("sparks")
        sparks.setWidth(100, Sizeable.Unit.PERCENTAGE)
        Responsive.makeResponsive(sparks)

//        int documentCount = documentsService.count(true, loginManager.user)
        int consultsCount = userKeysService.countByUser(loginManager.user)
        int userRelatedCount = 0

//        BusinessStatusBar documentQuantity = new BusinessStatusBar(documentCount,
//                Languages.instance.getText("chart.title.document.quantity"), "", "", Styles.ACTIVE_LOANS_LABEL)
//        sparks.addComponent(documentQuantity)

//        BusinessStatusBar relatedUserQuantity = new BusinessStatusBar(userRelatedCount,
//                Languages.instance.getText("chart.title.user.related.quantity"), "", "", Styles.BORROWED_MONEY_LABEL)
//        sparks.addComponent(relatedUserQuantity)

        BusinessStatusBar userConsultsQuantity = new BusinessStatusBar(consultsCount,
                Languages.instance.getText("chart.title.user.consults.quantity"), "", "", Styles.DAY_PAYMENTS_LABEL)
        sparks.addComponent(userConsultsQuantity)

//        BusinessStatusBar lateLoans = new BusinessStatusBar(0 as BigDecimal,
//                Languages.instance.getText("chart.title.late.loans"), "", "", Styles.LATE_PAYMENTS_LABEL)
//        sparks.addComponent(lateLoans)
        return sparks
    }

    private Component buildContent() {
        dashboardPanels = new CssLayout()
        dashboardPanels.addStyleName("dashboard-panels")
        Responsive.makeResponsive(dashboardPanels)
//        dashboardPanels.addComponent(documentPerRoleChart())
//        dashboardPanels.addComponent(documentTypeChart())

        dashboardPanels.addComponent(usersPerAgeChart())
//            dashboardPanels.addComponent(usersPerCategorysChart())
        dashboardPanels.addComponent(genderChart())
        dashboardPanels.addComponent(userPerLocationGrid())

//        dashboardPanels.addComponent(documentsPerLocationGrid())
//        dashboardPanels.addComponent(userPerUserRelatedGrid())
        return dashboardPanels
    }

    private static Component createContentWrapper(final Component content) {
        final CssLayout slot = new CssLayout()
        slot.setWidth(100, Sizeable.Unit.PERCENTAGE)
        slot.addStyleName("dashboard-panel-slot")

        CssLayout card = new CssLayout()
        card.setWidth(100, Sizeable.Unit.PERCENTAGE)
        card.addStyleName(ValoTheme.LAYOUT_CARD)

        HorizontalLayout toolbar = new HorizontalLayout()
        toolbar.addStyleName("dashboard-panel-toolbar")
        toolbar.setWidth(100, Sizeable.Unit.PERCENTAGE)

        Label caption = new Label(content.getCaption())
        caption.addStyleName(ValoTheme.LABEL_H4)
        caption.addStyleName(ValoTheme.LABEL_COLORED)
        caption.addStyleName(ValoTheme.LABEL_NO_MARGIN)
        content.setCaption(null)

        toolbar.addComponents(caption)
//        toolbar.setExpandRatio(caption, 1)
//        toolbar.setComponentAlignment(caption, Alignment.MIDDLE_LEFT)

        slot.setCaption(content.getCaption())
        card.addComponents(toolbar, content)
        slot.addComponent(card)
        return slot
    }

    private Component userPerLocationGrid() {
        Component contentWrapper = createContentWrapper(userLocationGrid())
        contentWrapper.addStyleName("v-caption-ronald v-caption")
        contentWrapper.setCaption(Languages.instance.getText("chart.title.user.per.location"))
        return contentWrapper
    }

    private Component documentsPerLocationGrid() {
        Component contentWrapper = createContentWrapper(documentsLocationGrid())
        contentWrapper.addStyleName("v-caption-ronald v-caption")
        contentWrapper.setCaption(Languages.instance.getText("chart.title.documents.per.location"))
        return contentWrapper
    }

//    private Component userPerUserRelatedGrid() {
//        Component contentWrapper = createContentWrapper(userUserRelatedGrid())
//        contentWrapper.addStyleName("v-caption-ronald v-caption")
//        contentWrapper.setCaption(Languages.instance.getText("chart.title.user.per.user.related"))
//        return contentWrapper
//    }

    private Component userLocationGrid() {
        Grid grid = new Grid()
        grid.setCaption(Languages.instance.getText("chart.title.user.per.location"))
        grid.setWidth(100, Sizeable.Unit.PERCENTAGE)
        grid.addStyleName("v-caption-ronald v-caption")
        grid.addStyleName(ValoTheme.TABLE_COMPACT)
        grid.addStyleName(ValoTheme.TABLE_SMALL)
        List<UserLocation> userLocationList = new ArrayList<>()
        for (Location location : locationList) {
            int size
            if (loginManager.admin) {
                size = userService.listByLocation(location).size()
            } else {
                size = userService.listByUserAndLocation(loginManager.user, location).size()
            }
            userLocationList.add(new UserLocation(location, size))
        }
        manageGrid(grid, new UserLocationContainer(new LazyQuery<UserLocation>() {
            @Override
            int getSize() {
                return userLocationList.size()
            }

            @Override
            List<UserLocation> getItemsIds(int startIndex, int numberOfIds) {
                return userLocationList.subList(startIndex, (startIndex + numberOfIds))
            }

            @Override
            int getFilteredSize() {
                return 0
            }

            @Override
            List<UserLocation> getFilteredItemsIds(int startIndex, int numberOfIds) {
                return null
            }
        }))
        return grid
    }

    private Component documentsLocationGrid() {
        Grid grid = new Grid()
        grid.setCaption(Languages.instance.getText("chart.title.documents.per.location"))
        grid.setWidth(100, Sizeable.Unit.PERCENTAGE)
        grid.addStyleName("v-caption-ronald v-caption")
        grid.addStyleName(ValoTheme.TABLE_COMPACT)
        grid.addStyleName(ValoTheme.TABLE_SMALL)
        List<DocumentsLocation> documentsLocations = new ArrayList<>()
        for (Location location : locationList) {
            int size = 0
//            if (loginManager.admin) {
//                size = documentsService.listByLocation(location).size()
//            } else {
//                size = documentsService.listByUserAndLocation(loginManager.user, location).size()
//            }
            documentsLocations.add(new DocumentsLocation(location, size))
        }
        manageGrid(grid, new DocumentsLocationContainer(new LazyQuery<DocumentsLocation>() {
            @Override
            int getSize() {
                return documentsLocations.size()
            }

            @Override
            List<DocumentsLocation> getItemsIds(int startIndex, int numberOfIds) {
                return documentsLocations.subList(startIndex, (startIndex + numberOfIds))
            }

            @Override
            int getFilteredSize() {
                return 0
            }

            @Override
            List<DocumentsLocation> getFilteredItemsIds(int startIndex, int numberOfIds) {
                return null
            }
        }))
        return grid
    }

//    private Component userUserRelatedGrid() {
//        Grid grid = new Grid()
//        grid.setCaption(Languages.instance.getText("chart.title.user.per.user.related"))
//        grid.setWidth(100, Sizeable.Unit.PERCENTAGE)
//        grid.addStyleName("v-caption-ronald v-caption")
//        grid.addStyleName(ValoTheme.TABLE_COMPACT)
//        grid.addStyleName(ValoTheme.TABLE_SMALL)
//        List<UserUserRelated> userRelateds = new ArrayList<>()
//        for (User user : userService.list(true)) {
//            int size
//            if (loginManager.admin) {
//                size = userService.listByUserRelated().size()
//            } else {
//                size = userService.listByUserAndUserRelated(loginManager.user).size()
//            }
//            userRelateds.add(new UserUserRelated(user, size))
//        }
//        manageGrid(grid, new UserUserRelatedContainer(new LazyQuery<UserUserRelated>() {
//            @Override
//            int getSize() {
//                return userRelateds.size()
//            }
//
//            @Override
//            List<UserUserRelated> getItemsIds(int startIndex, int numberOfIds) {
//                return userRelateds.subList(startIndex, (startIndex + numberOfIds))
//            }
//
//            @Override
//            int getFilteredSize() {
//                return 0
//            }
//
//            @Override
//            List<UserUserRelated> getFilteredItemsIds(int startIndex, int numberOfIds) {
//                return null
//            }
//        }))
//        return grid
//    }

    private Component usersPerAgeChart() {
        UserPerAgeChart userPerAgeChart = new UserPerAgeChart(loginManager)
        userPerAgeChart.setSizeFull()
        return createContentWrapper(userPerAgeChart)
    }

    private Component usersPerCategorysChart() {
        UsersPerCategorysChart userPerAgeChart = new UsersPerCategorysChart(loginManager)
        userPerAgeChart.setSizeFull()
        return createContentWrapper(userPerAgeChart)
    }

    private Component documentTypeChart() {
        DocumentTypeChart documentTypeChart = new DocumentTypeChart(loginManager)
        documentTypeChart.setSizeFull()
        return createContentWrapper(documentTypeChart)
    }

    private Component documentPerRoleChart() {
        DocumentPerRoleChart documentPerRoleChart = new DocumentPerRoleChart(loginManager)
        documentPerRoleChart.setSizeFull()
        return createContentWrapper(documentPerRoleChart)
    }

    private Component genderChart() {
        GenderChart genderChart = new GenderChart(loginManager)
        genderChart.setSizeFull()
        return createContentWrapper(genderChart)
    }

    @Override
    protected void security() {
    }

    void update() {
        updateTable()
    }

    @Override
    void actionObserver() {
        //TODO:Aqui debo de implementar que cuando se haga un cambio se refresque la pantalla
    }
}
