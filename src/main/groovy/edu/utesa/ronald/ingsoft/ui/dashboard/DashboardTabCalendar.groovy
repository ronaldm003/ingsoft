package edu.utesa.ronald.ingsoft.ui.dashboard

import com.vaadin.data.Property
import com.vaadin.server.Sizeable
import com.vaadin.ui.*
import com.vaadin.ui.components.calendar.event.BasicEventProvider
import com.vaadin.ui.themes.ValoTheme
import edu.utesa.ronald.ingsoft.components.CalendarManager
import edu.utesa.ronald.ingsoft.components.abstracts.APanel
import edu.utesa.ronald.ingsoft.domains.admin.Cycle
import edu.utesa.ronald.ingsoft.domains.admin.Student
import edu.utesa.ronald.ingsoft.domains.appointment.Appointment
import edu.utesa.ronald.ingsoft.models.LoginManager
import edu.utesa.ronald.ingsoft.models.constants.Styles
import edu.utesa.ronald.ingsoft.models.enums.CalendarMode
import edu.utesa.ronald.ingsoft.models.interfaces.OnNew
import edu.utesa.ronald.ingsoft.models.interfaces.PickElement
import edu.utesa.ronald.ingsoft.models.suggestions.StudentSuggestions
import edu.utesa.ronald.ingsoft.services.admin.CycleService
import edu.utesa.ronald.ingsoft.services.admin.StudentService
import edu.utesa.ronald.ingsoft.services.appointment.AppointmentService
import edu.utesa.ronald.ingsoft.services.security.ParamService
import edu.utesa.ronald.ingsoft.ui.admin.StudentForm
import edu.utesa.ronald.ingsoft.ui.components.forms.OneToOneAction
import edu.utesa.ronald.ingsoft.ui.events.AppointmentForm
import edu.utesa.ronald.ingsoft.ui.utils.pickers.StudentPicker
import edu.utesa.ronald.ingsoft.utils.Constants
import edu.utesa.ronald.ingsoft.utils.Languages
import edu.utesa.ronald.ingsoft.utils.calendar.AppointmentEvent
import edu.utesa.ronald.ingsoft.utils.calendar.CallCalendar
import edu.utesa.ronald.ingsoft.utils.params.ParamType
import org.springframework.beans.factory.annotation.Autowired

import static com.vaadin.ui.components.calendar.CalendarComponentEvents.*

/**
 *  Created by ronald on 7/8/16.
 */
@SuppressWarnings(["ChangeToOperator", "ChangeToOperator"])
class DashboardTabCalendar extends APanel {

    @Autowired
    private AppointmentService appointmentService
    @Autowired
    private StudentService studentService
    @Autowired
    private ParamService paramService
    @Autowired
    private CycleService cycleService

    private HorizontalLayout hlButtons, hlButtonsLeft, hlButtonsRight, hlOptions

    private OneToOneAction oneToOneActionStudent = new OneToOneAction(false)

    private LoginManager loginManager
    private Student studentSelect
    private Cycle cycleSelect

    private GridLayout mainLayout

    private final Label captionLabel = new Label("")
    private ComboBox cbCycle

    private Button btnSearch
    private Button btnPrevious
    private Button btMonthView
    private Button btnWeekView
    private Button btnDayView
    private Button btnNewEvent
    private Button btnNext
    private CheckBox ckHideWeekend
    private CssLayout calendarViewGroup

    private Calendar calendarComponent
    private CalendarManager calendarManager
    private BasicEventProvider dataSource


    private AppointmentForm calendarEventForm

    DashboardTabCalendar() {
        mainLayout = new GridLayout(1, 3)
        mainLayout.setSizeFull()
        mainLayout.setSpacing(true)
        confLayout(mainLayout)
    }

    @Override
    protected void confLanguage() {
        Constants.instance.autoWiredClass(this)
    }

    @Override
    protected void buildLayout() {

        dataSource = new BasicEventProvider()

        Integer fHour = paramService.byCode(ParamType.CALENDAR_FIRST_HOUR).getValue().toInteger() ?: 0
        Integer lHour = paramService.byCode(ParamType.CALENDAR_LAST_HOUR).getValue().toInteger() ?: 0

        calendarComponent = new Calendar(dataSource)
        calendarComponent.setWidth(100, Sizeable.Unit.PERCENTAGE)
        calendarComponent.setHeight(93, Sizeable.Unit.PERCENTAGE)
        calendarComponent.setFirstVisibleHourOfDay(fHour)
        calendarComponent.setLastVisibleDayOfWeek(lHour)
        calendarManager = new CalendarManager(calendarComponent, new CallCalendar() {
            @Override
            void findEvents(Date start, Date end) {
                for (AppointmentEvent appointmentEvent1 : this.appointmentEvents) {
                    dataSource.removeEvent(appointmentEvent1)
                }
                appointmentEvents.clear()
                for (Appointment appointment : appointmentService.listByCycleAndStudent(cycleSelect, studentSelect)) {
                    println "Appoint " + appointment.toString() + " " + appointment.start + " " + appointment.end
                    AppointmentEvent appointmentEvent = new AppointmentEvent(appointmentService.refresh(appointment.id))
                    appointmentEvents.add(appointmentEvent)
                    dataSource.addEvent(appointmentEvent)
                }
            }
        })
        btnSearch = new Button(Languages.instance.getText("window.search"))
        btnSearch.addStyleName(ValoTheme.BUTTON_BORDERLESS_COLORED)
        btnSearch.addClickListener(new Button.ClickListener() {
            @Override
            void buttonClick(Button.ClickEvent event) {
                calendarManager.refresh()
                refreshLabel()
            }
        })
        refreshLabel()
        initStudent()
        initCycle()
        initButtonCalendar()
        fillCycle()
        calendarManager.switchToMonthView()
    }

    private void initCycle() {
        cbCycle = new ComboBox()
        cbCycle.setWidth(100, Unit.PERCENTAGE)
        cbCycle.addStyleName(ValoTheme.COMBOBOX_TINY)
        cbCycle.setImmediate(true)
        cbCycle.addValueChangeListener(new Property.ValueChangeListener() {
            @Override
            void valueChange(Property.ValueChangeEvent event) {
                if (cbCycle.getValue().toString()) {
                    cycleSelect = cycleService.byId(cbCycle.getValue() as Long)
                } else {
                    cycleSelect = null
                }
            }
        })
    }

    void fillCycle() {
        cbCycle.removeAllItems()
        for (Cycle cycle : cycleService.list(true, { it.listSubjects.size() })) {
            cbCycle.addItem(cycle.id)
            cbCycle.setItemCaption(cycle.id, cycle.toString())
        }
        cbCycle.setValue(0)
    }

    private void initButtonCalendar() {
        btnPrevious = new Button(Languages.instance.getText("tab.dashboard.date.calendar.previous"))
        btnPrevious.addStyleName(ValoTheme.BUTTON_BORDERLESS_COLORED)
        btnPrevious.addClickListener(new Button.ClickListener() {
            @Override
            void buttonClick(Button.ClickEvent event) {
                calendarManager.previous()
                refreshLabel()
            }
        })
        ckHideWeekend = new CheckBox(Languages.instance.getText("tab.dashboard.date.calendar.hideweekend"))
        ckHideWeekend.setImmediate(true)
        ckHideWeekend.setValue(true)
        if (ckHideWeekend.getValue()) {
            calendarManager.hideWeekends(true)
        }
        ckHideWeekend.addStyleName(ValoTheme.CHECKBOX_SMALL)
        ckHideWeekend.addValueChangeListener(new Property.ValueChangeListener() {
            @Override
            void valueChange(Property.ValueChangeEvent event) {
                calendarManager.hideWeekends(ckHideWeekend.getValue())
            }
        })
        btMonthView = new Button(Languages.instance.getText("tab.dashboard.date.calendar.month"))
        btMonthView.addStyleName(Styles.BUTTON_MONTH)
        btMonthView.addClickListener(new Button.ClickListener() {
            @Override
            void buttonClick(Button.ClickEvent event) {
                calendarManager.switchToMonthView()
            }
        })
        btnWeekView = new Button(Languages.instance.getText("tab.dashboard.date.calendar.week"))
        btnWeekView.addStyleName(Styles.BUTTON_WEEK)
        btnWeekView.addClickListener(new Button.ClickListener() {
            @Override
            void buttonClick(Button.ClickEvent event) {
                calendarManager.switchToWeekView()
            }
        })

        btnDayView = new Button(Languages.instance.getText("tab.dashboard.date.calendar.day"))
        btnDayView.addStyleName(Styles.BUTTON_DAY)
        btnDayView.addClickListener(new Button.ClickListener() {
            @Override
            void buttonClick(Button.ClickEvent event) {
                calendarManager.switchToDayView()
            }
        })
        btnNewEvent = new Button(Languages.instance.getText("tab.dashboard.date.calendar.newevent"))
        btnNewEvent.addStyleName(Styles.BUTTON_NEW_EVENT)
        btnNewEvent.addClickListener(new Button.ClickListener() {
            @Override
            void buttonClick(Button.ClickEvent clickEvent) {
                calendarEventForm = new AppointmentForm(new Date(), new Date())
                calendarEventForm.addCloseListener(new Window.CloseListener() {
                    @Override
                    void windowClose(Window.CloseEvent e) {
                        calendarManager.refresh()
                    }
                })
                getUI().addWindow(calendarEventForm)
            }
        })
        btnNext = new Button(Languages.instance.getText("tab.dashboard.date.calendar.next"))
        btnNext.addStyleName(ValoTheme.BUTTON_BORDERLESS_COLORED)
        btnNext.addClickListener(new Button.ClickListener() {
            @Override
            void buttonClick(Button.ClickEvent event) {
                calendarManager.next()
                refreshLabel()
            }
        })
        calendarComponent.setHandler(new EventClickHandler() {
            @Override
            void eventClick(EventClick event) {
                AppointmentEvent appointmentEvent = event.getCalendarEvent() as AppointmentEvent
                AppointmentForm calendarEventForm = new AppointmentForm(appointmentEvent.getAppointment(), true)
                calendarEventForm.addCloseListener(new Window.CloseListener() {
                    @Override
                    void windowClose(Window.CloseEvent e) {
                        calendarManager.refresh()
                    }
                })
                getUI().addWindow(calendarEventForm)
            }
        })
        calendarComponent.setHandler(new RangeSelectHandler() {
            @Override
            void rangeSelect(RangeSelectEvent event) {
                Date end = event.getEnd()
                if (calendarManager.getCalendarMode() == CalendarMode.MONTH) {
                    end = calendarManager.getEndOfDay(end)
                }
                AppointmentForm calendarEventForm = new AppointmentForm(event.getStart(), end)
                calendarEventForm.addCloseListener(new Window.CloseListener() {
                    @Override
                    void windowClose(Window.CloseEvent e) {
                        calendarManager.refresh()
                    }
                })
                getUI().addWindow(calendarEventForm)
            }
        })

        hlButtons = new HorizontalLayout()
        hlButtons.setWidth(100, Sizeable.Unit.PERCENTAGE)
        hlButtons.setSpacing(true)
        captionLabel.addStyleName(ValoTheme.LABEL_COLORED)

        hlButtonsLeft = new HorizontalLayout()
        hlButtonsLeft.setWidth(100, Sizeable.Unit.PERCENTAGE)
        hlButtonsLeft.addStyleName(ValoTheme.LAYOUT_COMPONENT_GROUP)
        hlButtonsLeft.addComponent(btnPrevious)
        hlButtonsLeft.addComponent(ckHideWeekend)
        hlButtonsLeft.setComponentAlignment(btnPrevious, Alignment.MIDDLE_LEFT)
        hlButtonsLeft.setComponentAlignment(ckHideWeekend, Alignment.MIDDLE_CENTER)

        hlButtonsRight = new HorizontalLayout()
        hlButtonsRight.setWidth(100, Sizeable.Unit.PERCENTAGE)
        hlButtonsRight.addStyleName(ValoTheme.LAYOUT_COMPONENT_GROUP)
        hlButtonsRight.addComponent(captionLabel)
        hlButtonsRight.addComponent(btnNewEvent)
        hlButtonsRight.addComponent(btnNext)
        hlButtonsRight.setComponentAlignment(captionLabel, Alignment.MIDDLE_RIGHT)
        hlButtonsRight.setComponentAlignment(btnNewEvent, Alignment.MIDDLE_RIGHT)
        hlButtonsRight.setComponentAlignment(btnNext, Alignment.MIDDLE_RIGHT)

        hlOptions = new HorizontalLayout()
        hlOptions.setWidth(100, Sizeable.Unit.PERCENTAGE)
        hlOptions.addStyleName(ValoTheme.LAYOUT_COMPONENT_GROUP)
        hlOptions.addComponent(cbCycle)
        hlOptions.addComponent(oneToOneActionStudent)
        hlOptions.addComponent(btnSearch)

        calendarViewGroup = new CssLayout()
        calendarViewGroup.addStyleName(ValoTheme.LAYOUT_COMPONENT_GROUP)
        calendarViewGroup.addComponent(btMonthView)
        calendarViewGroup.addComponent(btnWeekView)
        calendarViewGroup.addComponent(btnDayView)

        hlButtons.addComponent(hlButtonsLeft)
        hlButtons.addComponent(calendarViewGroup)
        hlButtons.addComponent(hlButtonsRight)
        hlButtons.setComponentAlignment(hlButtonsLeft, Alignment.MIDDLE_LEFT)
        hlButtons.setComponentAlignment(calendarViewGroup, Alignment.MIDDLE_CENTER)
        hlButtons.setComponentAlignment(hlButtonsRight, Alignment.MIDDLE_RIGHT)

        mainLayout.addComponent(hlOptions, 0, 0)
        mainLayout.addComponent(hlButtons, 0, 1)
        mainLayout.addComponent(calendarComponent, 0, 2)
        mainLayout.setRowExpandRatio(mainLayout.getRows() - 1, 1.0f)
    }

    private void initStudent() {
        oneToOneActionStudent.tfACSearch().setSuggestionProvider(new StudentSuggestions())
        oneToOneActionStudent.tfACSearch().addValueChangeListener(new Property.ValueChangeListener() {
            @Override
            void valueChange(Property.ValueChangeEvent event) {
                if (oneToOneActionStudent.tfACSearch().getValue() && !oneToOneActionStudent.tfACSearch().getValue().isEmpty()) {
                    try {
                        studentSelect = studentService.byEnrollment(oneToOneActionStudent.tfACSearch().getValue().split(":")[0])
                        selectStudent(studentSelect)
                    } catch (Exception ignored) {
                    }
                } else {
                    studentSelect = null
                }
            }
        })
        oneToOneActionStudent.btnNew().addClickListener(new Button.ClickListener() {
            @Override
            void buttonClick(Button.ClickEvent event) {
                StudentForm studentForm = new StudentForm(new OnNew<Student>() {
                    @Override
                    void result(Student student) {
                        studentSelect = student
                        selectStudent(studentSelect)
                    }
                })
                getUI().addWindow(studentForm)
            }
        })
        oneToOneActionStudent.btnSearch().addClickListener(new Button.ClickListener() {
            @Override
            void buttonClick(Button.ClickEvent event) {
                StudentPicker locationPicker = new StudentPicker(new PickElement<Student>() {
                    @Override
                    void onResult(List<Student> elements) {
                        if (elements.size() > 0) {
                            studentSelect = elements.get(0)
                            selectStudent(studentSelect)
                        }
                    }
                }, false)
                getUI().addWindow(locationPicker)
            }
        })
    }

    void selectStudent(Student student) {
        if (student) {
            oneToOneActionStudent.tfACSearch().setReadOnly(false)
            oneToOneActionStudent.select(student ? student.toString() : "")
            oneToOneActionStudent.tfACSearch().setReadOnly(true)
        }
    }


    private void refreshLabel() {
        captionLabel.setValue(calendarManager.getMothAndYear())
    }

    @Override
    protected void security() {
    }
}
