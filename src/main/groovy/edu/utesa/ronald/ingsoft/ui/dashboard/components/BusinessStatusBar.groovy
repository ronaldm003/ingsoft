package edu.utesa.ronald.ingsoft.ui.dashboard.components

import com.vaadin.server.Responsive
import com.vaadin.ui.Alignment
import com.vaadin.ui.Label
import com.vaadin.ui.VerticalLayout
import com.vaadin.ui.themes.ValoTheme

/**
 * ui de grafica
 * Created by ronald on 3/16/17.
 */
class BusinessStatusBar extends VerticalLayout {

    private String name
    private String unit
    private String prefix
    private BigDecimal amount
    private String style
    private Integer quantity

    BusinessStatusBar(BigDecimal amount, String name, String unit, String prefix, String style) {
        this(amount, null, name, unit, prefix, style)
    }

    BusinessStatusBar(Integer quantity, String name, String unit, String prefix, String style) {
        this(null, quantity, name, unit, prefix, style)
    }

    BusinessStatusBar(BigDecimal amount, Integer quantity, String name, String unit, String prefix, String style) {
        this.amount = amount
        this.quantity = quantity
        this.name = name
        this.unit = unit
        this.prefix = prefix
        this.style = style
        buildChart()
    }

    private void buildChart() {
        setSizeUndefined()
        addStyleName("spark")
        setDefaultComponentAlignment(Alignment.MIDDLE_CENTER)

        Label lblAmount = new Label(prefix + amount + unit)
        Responsive.makeResponsive(lblAmount)
        lblAmount.setSizeUndefined()
        lblAmount.addStyleName(ValoTheme.LABEL_BOLD)
        lblAmount.addStyleName(ValoTheme.LABEL_HUGE)
        lblAmount.addStyleName(style)

        Label lblQuantity = new Label(quantity + unit)
        Responsive.makeResponsive(lblQuantity)
        lblQuantity.setSizeUndefined()
        lblQuantity.addStyleName(ValoTheme.LABEL_LARGE)
        lblQuantity.addStyleName(style)

        Label title = new Label(name)
        title.setSizeUndefined()
        title.addStyleName(ValoTheme.LABEL_SMALL)
        title.addStyleName(ValoTheme.LABEL_LIGHT)
        title.addStyleName(style)

        addComponent(title)
        if (amount) {
            addComponent(lblAmount)
        }
        addComponent(lblQuantity)
    }
}
