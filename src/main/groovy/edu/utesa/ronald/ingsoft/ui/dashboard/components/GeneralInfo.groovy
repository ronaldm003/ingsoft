package edu.utesa.ronald.ingsoft.ui.dashboard.components

import com.vaadin.server.Sizeable
import com.vaadin.ui.themes.ValoTheme
import edu.utesa.ronald.ingsoft.components.abstracts.APanel
import edu.utesa.ronald.ingsoft.utils.Constants
import groovy.transform.CompileStatic

/**
 *  Created by ronald on 2/14/17.
 */
@CompileStatic
class GeneralInfo extends APanel {

    private GeneralInfoDesign generalInfoDesign = new GeneralInfoDesign()

    GeneralInfo() {
        confLayout(generalInfoDesign)
    }

    @Override
    protected void confLanguage() {
        Constants.instance.autoWiredClass(this)
        removeStyleName(ValoTheme.PANEL_BORDERLESS)
        setWidth(200, Sizeable.Unit.PIXELS)
    }

    @Override
    protected void buildLayout() {
    }

    @Override
    protected void security() {
    }
}
