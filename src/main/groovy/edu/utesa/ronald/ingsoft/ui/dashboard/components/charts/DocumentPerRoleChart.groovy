package edu.utesa.ronald.ingsoft.ui.dashboard.components.charts

import com.vaadin.addon.charts.Chart
import com.vaadin.addon.charts.model.*
import com.vaadin.addon.charts.model.style.SolidColor
import edu.utesa.ronald.ingsoft.models.LoginManager
import edu.utesa.ronald.ingsoft.utils.Constants
import edu.utesa.ronald.ingsoft.utils.Languages
import groovy.transform.CompileStatic

/**
 * Loans Charts.
 *
 *  Created by ronald on 3/16/17.
 */
@CompileStatic
class DocumentPerRoleChart extends Chart {

    LoginManager loginManager

    private DataSeries dataSeriesDocumentPerCategory

    DocumentPerRoleChart(LoginManager loginManager) {
        this.loginManager = loginManager
        Constants.instance.autoWiredClass(this)
        int totalDocumentPerRole = 0
        getConfiguration().getChart().setType(ChartType.COLUMN)
        getConfiguration().setTitle(Languages.instance.getText("chart.title.document.per.role"))
        getConfiguration().setExporting(true)

        XAxis xAxis = new XAxis()

        Tooltip tooltip = new Tooltip()
        tooltip.setValueDecimals(1)
        tooltip.setHeaderFormat("<span style=\"font-size:13px\">{series.name}</span><br>")
        tooltip.setPointFormat("<span style=\"color:{point.color};font-weight: bolder;\">{point.name}</span>: <b>{point.y}</b><br/>")
        getConfiguration().setTooltip(tooltip)

        PlotOptionsColumn plotOptionsColumn = new PlotOptionsColumn()
        plotOptionsColumn.setCursor(Cursor.POINTER)
        plotOptionsColumn.setColorByPoint(true)
        plotOptionsColumn.setPointPadding(0.2)
        plotOptionsColumn.setBorderWidth(0)


        DataLabels dataLabels = plotOptionsColumn.getDataLabels()
        dataLabels.setEnabled(true)
        dataLabels.setFormat("{point.description} {point.y:.1f}")
        getConfiguration().setPlotOptions(plotOptionsColumn)

        dataSeriesDocumentPerCategory = new DataSeries()
        dataSeriesDocumentPerCategory.setName(Languages.instance.getText("window.role"))
//        List<Role> roleList = roleService.list(true)
//        int Totalsize
//        for (Role role : roleList) {
//            if (loginManager.admin) {
//                Totalsize = documentsService.listByRoles(role).size()
//            } else {
//                Totalsize = documentsService.listByUserAndRoles(loginManager.user, role).size()
//            }
//            dataSeriesDocumentPerCategory.add(new DataSeriesItem(role.toString(), Totalsize))
//            totalDocumentPerRole += Totalsize
//        }
//        for (int i = 0; i < roleList.size(); i++) {
//            xAxis.setCategories(roleList.get(0).toString())
//        }

        Legend legend = new Legend()
        legend.setLayout(LayoutDirection.VERTICAL)
        legend.setBackgroundColor(new SolidColor("#FFFFFF"))
        legend.setAlign(HorizontalAlign.LEFT)
        legend.setVerticalAlign(VerticalAlign.TOP)
        legend.setX(100)
        legend.setY(70)
        legend.setFloating(true)
        legend.setShadow(true)

        getConfiguration().setSubTitle(Languages.instance.getText("chart.title.document.per.role.amount") + totalDocumentPerRole)
        getConfiguration().setLegend(legend)
        getConfiguration().addxAxis(xAxis)
        getConfiguration().setSeries(dataSeriesDocumentPerCategory)
        drawChart()
    }
}
