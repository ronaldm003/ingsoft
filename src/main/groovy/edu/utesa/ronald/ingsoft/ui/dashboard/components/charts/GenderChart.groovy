package edu.utesa.ronald.ingsoft.ui.dashboard.components.charts

import com.vaadin.addon.charts.Chart
import com.vaadin.addon.charts.LegendItemClickEvent
import com.vaadin.addon.charts.LegendItemClickListener
import com.vaadin.addon.charts.model.*
import edu.utesa.ronald.ingsoft.models.LoginManager
import edu.utesa.ronald.ingsoft.models.enums.Gender
import edu.utesa.ronald.ingsoft.services.security.UserService
import edu.utesa.ronald.ingsoft.utils.Constants
import edu.utesa.ronald.ingsoft.utils.Languages
import groovy.transform.CompileStatic
import org.springframework.beans.factory.annotation.Autowired

/**
 * Loans Charts.
 *
 *  Created by ronald on 3/16/17.
 */
@CompileStatic
class GenderChart extends Chart {

    @Autowired
    private UserService userService

    LoginManager loginManager

    private DataSeries dataSeriesGender
    private Credits creditsGender

    GenderChart(LoginManager loginManager) {
        this.loginManager = loginManager
        Constants.instance.autoWiredClass(this)

        int mans = userService.countByGenre(Gender.MAN)
        int womans = userService.countByGenre(Gender.WOMAN)
        int other = userService.countByGenre(Gender.OTHERS)
        int totalGenre = mans + womans + other

        getConfiguration().getChart().setType(ChartType.PIE)
        getConfiguration().setTitle(Languages.instance.getText("chart.title.gender"))
        getConfiguration().setExporting(true)


        Tooltip tooltip = new Tooltip()
        tooltip.setValueDecimals(1)
        tooltip.setPointFormat("{series.description}: <b>{point.percentage}%</b>")
        getConfiguration().setTooltip(tooltip)

        PlotOptionsPie plotOptions = new PlotOptionsPie()
        plotOptions.setAllowPointSelect(true)
        plotOptions.setCursor(Cursor.POINTER)
        plotOptions.setShowInLegend(true)
        DataLabels dataLabels = plotOptions.getDataLabels();
        dataLabels.setEnabled(true);
        dataLabels.setFormat("{point.description}: {point.y:.1f}");
        getConfiguration().setPlotOptions(plotOptions)

        creditsGender = new Credits("Custom Credit")
        creditsGender.setPosition(new Position())
        creditsGender.getPosition().setHorizontalAlign(HorizontalAlign.CENTER)
        creditsGender.setText(Languages.instance.getText("chart.total") + totalGenre)
        addLegendItemClickListener(new LegendItemClickListener() {
            @Override
            void onClick(LegendItemClickEvent event) {
                println "Legend item click" + " : " + event.getSeriesItemIndex() + " : " +
                        ((DataSeries) event.getSeries()).get(event.getSeriesItemIndex()).getName()
            }
        })
        dataSeriesGender = new DataSeries()
        dataSeriesGender.add(new DataSeriesItem(Gender.MAN.toString(), mans))
        dataSeriesGender.add(new DataSeriesItem(Gender.WOMAN.toString(), womans))
        dataSeriesGender.add(new DataSeriesItem(Gender.OTHERS.toString(), other))
        getConfiguration().setSubTitle(Languages.instance.getText("chart.title.gender.amount") + totalGenre)
        getConfiguration().setSeries(dataSeriesGender)
        drawChart()
    }
}
