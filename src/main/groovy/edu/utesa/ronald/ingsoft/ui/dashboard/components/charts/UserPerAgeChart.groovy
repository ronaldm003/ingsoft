package edu.utesa.ronald.ingsoft.ui.dashboard.components.charts

import com.vaadin.addon.charts.Chart
import com.vaadin.addon.charts.model.*
import com.vaadin.addon.charts.model.style.SolidColor
import edu.utesa.ronald.ingsoft.domains.security.User
import edu.utesa.ronald.ingsoft.models.LoginManager
import edu.utesa.ronald.ingsoft.services.security.UserService
import edu.utesa.ronald.ingsoft.utils.Constants
import edu.utesa.ronald.ingsoft.utils.Languages
import groovy.transform.CompileStatic
import org.springframework.beans.factory.annotation.Autowired

/**
 * Loans Charts.
 *
 *  Created by ronald on 3/16/17.
 */
@CompileStatic
class UserPerAgeChart extends Chart {
    @Autowired
    private UserService userService

    LoginManager loginManager

    private DataSeries dataSeriesUsersPerAge

    UserPerAgeChart(LoginManager loginManager) {
        this.loginManager = loginManager
        Constants.instance.autoWiredClass(this)
        int totalUsersPerAge = 0
        getConfiguration().getChart().setType(ChartType.COLUMN)
        getConfiguration().setTitle(Languages.instance.getText("chart.title.user.per.age"))
        getConfiguration().setExporting(true)

        XAxis xAxis = new XAxis()

        Tooltip tooltip = new Tooltip()
        tooltip.setValueDecimals(1)
        tooltip.setHeaderFormat("<span style=\"font-size:13px\">{series.name}</span><br>")
        tooltip.setPointFormat("<span style=\"color:{point.color};font-weight: bolder;\">{point.name}</span>: <b>{point.y}</b><br/>")
        getConfiguration().setTooltip(tooltip)

        PlotOptionsColumn plotOptionsColumn = new PlotOptionsColumn()
        plotOptionsColumn.setCursor(Cursor.POINTER)
        plotOptionsColumn.setColorByPoint(true)
        plotOptionsColumn.setPointPadding(0.2)
        plotOptionsColumn.setBorderWidth(0)

        DataLabels dataLabels = plotOptionsColumn.getDataLabels()
        dataLabels.setEnabled(true)
        dataLabels.setFormat("{point.description} {point.y:.1f}")
        getConfiguration().setPlotOptions(plotOptionsColumn)

        dataSeriesUsersPerAge = new DataSeries()
        dataSeriesUsersPerAge.setName(Languages.instance.getText("window.user"))
        List<User> userList = userService.list(true)
        int oneToTen = 0
        int tenToTwenty = 0
        int twentyToThirty = 0
        int thirtyToFourty = 0
        int fourtyToFifty = 0
        int fiftyToSixty = 0
        int sixtyToSeventy = 0
        int seventyToEighty = 0
        int eightyToNinety = 0
        int ninetyToHundred = 0
        int greaterThanHundred = 0
        for (User user : userList) {
            if (loginManager.admin) {
                switch (Constants.instance.getAge(user.person.birthDate)) {
                    case 1..10:
                        oneToTen++
                        break
                    case 11..20:
                        tenToTwenty++
                        break
                    case 21..30:
                        twentyToThirty++
                        break
                    case 31..40:
                        thirtyToFourty++
                        break
                    case 41..50:
                        fourtyToFifty++
                        break
                    case 51..60:
                        fiftyToSixty++
                        break
                    case 61..70:
                        sixtyToSeventy++
                        break
                    case 71..80:
                        seventyToEighty++
                        break
                    case 81..90:
                        eightyToNinety++
                        break
                    case 91..100:
                        ninetyToHundred++
                        break
                    case 101 > 1000:
                        greaterThanHundred++
                        break
                }
            }
        }
        addDaraseriesItem("1..10", oneToTen)
        addDaraseriesItem("11..20", tenToTwenty)
        addDaraseriesItem("21..30", twentyToThirty)
        addDaraseriesItem("31..40", thirtyToFourty)
        addDaraseriesItem("41..50", fourtyToFifty)
        addDaraseriesItem("51..60", fiftyToSixty)
        addDaraseriesItem("61..70", sixtyToSeventy)
        addDaraseriesItem("71..80", seventyToEighty)
        addDaraseriesItem("81..90", eightyToNinety)
        addDaraseriesItem("91..100", ninetyToHundred)
        addDaraseriesItem(">100", greaterThanHundred)
        totalUsersPerAge += oneToTen + tenToTwenty + twentyToThirty + thirtyToFourty + fourtyToFifty + fiftyToSixty + sixtyToSeventy + seventyToEighty + eightyToNinety + ninetyToHundred + greaterThanHundred
        String[] categoriesList = ["1..10", "11..20", "21..30", "31..40", "41..50", "51..60", "61..70", "71..80", "81..90", "91..100", ">100"]
        for (int i = 0; i < categoriesList.size(); i++) {
            xAxis.setCategories(categoriesList[i])
        }

        Legend legend = new Legend()
        legend.setLayout(LayoutDirection.VERTICAL)
        legend.setBackgroundColor(new SolidColor("#FFFFFF"))
        legend.setAlign(HorizontalAlign.LEFT)
        legend.setVerticalAlign(VerticalAlign.TOP)
        legend.setX(100)
        legend.setY(70)
        legend.setFloating(true)
        legend.setShadow(true)

        getConfiguration().setSubTitle(Languages.instance.getText("chart.title.user.per.age.amount") + totalUsersPerAge)
        getConfiguration().setLegend(legend)
        getConfiguration().addxAxis(xAxis)
        getConfiguration().setSeries(dataSeriesUsersPerAge)
        drawChart()
    }

    private void addDaraseriesItem(String range, int size) {
        dataSeriesUsersPerAge.add(new DataSeriesItem(range, size))
    }
}
