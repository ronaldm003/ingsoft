package edu.utesa.ronald.ingsoft.ui.dashboard.components.charts

import com.vaadin.addon.charts.Chart
import com.vaadin.addon.charts.LegendItemClickEvent
import com.vaadin.addon.charts.LegendItemClickListener
import com.vaadin.addon.charts.model.*
import edu.utesa.ronald.ingsoft.models.LoginManager
import edu.utesa.ronald.ingsoft.utils.Constants
import edu.utesa.ronald.ingsoft.utils.Languages
import groovy.transform.CompileStatic

/**
 * Loans Charts.
 *
 *  Created by ronald on 3/16/17.
 */
@CompileStatic
class UserTypeChart extends Chart {

    LoginManager loginManager

    UserTypeChart(LoginManager loginManager) {
        this.loginManager = loginManager
        Constants.instance.autoWiredClass(this)
        getConfiguration().getChart().setType(ChartType.PIE)
        getConfiguration().setTitle(Languages.instance.getText("chart.title.userType"))

        Tooltip tooltip = new Tooltip()
        tooltip.setValueDecimals(1)
        tooltip.setPointFormat("{series.description}: <b>{point.percentage}%</b>")
        getConfiguration().setTooltip(tooltip)

        PlotOptionsPie plotOptions = new PlotOptionsPie()
        plotOptions.setAllowPointSelect(true)
        plotOptions.setCursor(Cursor.POINTER)
        plotOptions.setShowInLegend(true)
        getConfiguration().setPlotOptions(plotOptions)

        DataSeries series = new DataSeries()
        series.add(new DataSeriesItem("Firefox", 45.0))
        series.add(new DataSeriesItem("IE", 26.8))
        DataSeriesItem chrome = new DataSeriesItem("Chrome", 12.8)
        chrome.setSliced(true)
        chrome.setSelected(true)
        series.add(chrome)
        series.add(new DataSeriesItem("Safari", 8.5))
        series.add(new DataSeriesItem("Opera", 6.2))
        series.add(new DataSeriesItem("Others", 0.7))
        getConfiguration().setSeries(series)

        addLegendItemClickListener(new LegendItemClickListener() {
            @Override
            void onClick(LegendItemClickEvent event) {
                println "Legend item click" + " : " + event.getSeriesItemIndex() + " : " +
                        ((DataSeries) event.getSeries()).get(event.getSeriesItemIndex()).getName()
            }
        })

        drawChart(getConfiguration())
    }
}
