package edu.utesa.ronald.ingsoft.ui.dashboard.components.charts

import com.vaadin.addon.charts.Chart
import com.vaadin.addon.charts.model.*
import com.vaadin.addon.charts.model.style.SolidColor
import edu.utesa.ronald.ingsoft.models.LoginManager
import edu.utesa.ronald.ingsoft.services.security.UserService
import edu.utesa.ronald.ingsoft.utils.Constants
import edu.utesa.ronald.ingsoft.utils.Languages
import groovy.transform.CompileStatic
import org.springframework.beans.factory.annotation.Autowired

/**
 * Loans Charts.
 *
 *  Created by ronald on 3/16/17.
 */
@CompileStatic
class UsersPerCategorysChart extends Chart {

    @Autowired
    private UserService userService

    LoginManager loginManager

    private DataSeries dataSeriesUserPerCategorys

    UsersPerCategorysChart(LoginManager loginManager) {
        this.loginManager = loginManager
        Constants.instance.autoWiredClass(this)
        int totalDocumentType = 0
        getConfiguration().getChart().setType(ChartType.COLUMN)
        getConfiguration().setTitle(Languages.instance.getText("chart.title.user.per.categorys"))

        XAxis xAxis = new XAxis()

        Tooltip tooltip = new Tooltip()
        tooltip.setValueDecimals(1)
        tooltip.setHeaderFormat("<span style=\"font-size:13px\">{series.name}</span><br>")
        tooltip.setPointFormat("<span style=\"color:{point.color};font-weight: bolder;\">{point.name}</span> <b>{point.y}</b><br/>")
        getConfiguration().setTooltip(tooltip)
        getConfiguration().setExporting(true)

        PlotOptionsColumn plotOptionsColumn = new PlotOptionsColumn()
        plotOptionsColumn.setCursor(Cursor.POINTER)
        plotOptionsColumn.setColorByPoint(true)
        plotOptionsColumn.setPointPadding(0.2)
        plotOptionsColumn.setBorderWidth(0)

        DataLabels dataLabels = plotOptionsColumn.getDataLabels()
        dataLabels.setEnabled(true)
        dataLabels.setFormat("{point.description} {point.y:.1f}")
        getConfiguration().setPlotOptions(plotOptionsColumn)

        dataSeriesUserPerCategorys = new DataSeries()
        dataSeriesUserPerCategorys.setName(Languages.instance.getText("window.category"))
//        List<Categorys> listOfCategorys = categoryService.list(true)
//        for (Categorys categorys : listOfCategorys) {
//            int size
//            if (loginManager.admin) {
//                size = userService.listByCategorys(categorys).size()
//            } else {
//                size = userService.listByUserAndCategorys(loginManager.user, categorys).size()
//            }
//            dataSeriesUserPerCategorys.add(new DataSeriesItem(categorys.toString(), size))
//            totalDocumentType += size
//        }
//        for (int i = 0; i < listOfCategorys.size(); i++) {
//            xAxis.setCategories(listOfCategorys.get(0).toString())
//        }
        Legend legend = new Legend()
        legend.setLayout(LayoutDirection.VERTICAL)
        legend.setBackgroundColor(new SolidColor("#FFFFFF"))
        legend.setAlign(HorizontalAlign.LEFT)
        legend.setVerticalAlign(VerticalAlign.TOP)
        legend.setX(100)
        legend.setY(70)
        legend.setFloating(true)
        legend.setShadow(true)

        getConfiguration().setSubTitle(Languages.instance.getText("chart.title.user.per.categorys.amount") + totalDocumentType)
        getConfiguration().setLegend(legend)
        getConfiguration().addxAxis(xAxis)
        getConfiguration().setSeries(dataSeriesUserPerCategorys)
        drawChart()
    }
}
