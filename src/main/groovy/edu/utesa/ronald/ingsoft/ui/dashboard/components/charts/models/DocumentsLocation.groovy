package edu.utesa.ronald.ingsoft.ui.dashboard.components.charts.models

import edu.utesa.ronald.ingsoft.domains.location.Location
import groovy.transform.CompileStatic

@CompileStatic
class DocumentsLocation {
    Location location
    Integer documentsAmount

    DocumentsLocation() {
    }

    DocumentsLocation(Location location, Integer documentsAmount) {
        this.location = location
        this.documentsAmount = documentsAmount
    }
}
