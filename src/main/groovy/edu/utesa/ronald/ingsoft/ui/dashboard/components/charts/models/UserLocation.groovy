package edu.utesa.ronald.ingsoft.ui.dashboard.components.charts.models

import edu.utesa.ronald.ingsoft.domains.location.Location
import groovy.transform.CompileStatic

@CompileStatic
class UserLocation {
    Location location
    Integer userAmount

    UserLocation() {
    }

    UserLocation(Location location, Integer userAmount) {
        this.location = location
        this.userAmount = userAmount
    }
}
