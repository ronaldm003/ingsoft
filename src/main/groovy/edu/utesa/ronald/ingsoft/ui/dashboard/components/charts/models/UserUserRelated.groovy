package edu.utesa.ronald.ingsoft.ui.dashboard.components.charts.models

import edu.utesa.ronald.ingsoft.domains.security.User
import groovy.transform.CompileStatic

@CompileStatic
class UserUserRelated {
    User user
    Integer userRelatedAmount

    UserUserRelated() {
    }

    UserUserRelated(User user, Integer documentsAmount) {
        this.user = user
        this.userRelatedAmount = userRelatedAmount
    }
}
