package edu.utesa.ronald.ingsoft.ui.dental

import com.vaadin.data.Property
import com.vaadin.event.ItemClickEvent
import com.vaadin.ui.Button
import edu.utesa.ronald.ingsoft.components.abstracts.APanel
import edu.utesa.ronald.ingsoft.domains.admin.Cycle
import edu.utesa.ronald.ingsoft.domains.admin.Student
import edu.utesa.ronald.ingsoft.domains.admin.Teacher
import edu.utesa.ronald.ingsoft.domains.control.Patient
import edu.utesa.ronald.ingsoft.domains.control.recordcard.RecordCard
import edu.utesa.ronald.ingsoft.models.containers.dental.ConsultsContainer
import edu.utesa.ronald.ingsoft.services.admin.CycleService
import edu.utesa.ronald.ingsoft.services.admin.StudentService
import edu.utesa.ronald.ingsoft.services.admin.TeacherService
import edu.utesa.ronald.ingsoft.services.control.PatientService
import edu.utesa.ronald.ingsoft.services.control.recordcard.RecordCardService
import edu.utesa.ronald.ingsoft.ui.control.recordcard.RecordCardForm
import edu.utesa.ronald.ingsoft.utils.Constants
import edu.utesa.ronald.ingsoft.utils.Languages
import edu.utesa.ronald.ingsoft.utils.container.LazyQuery
import groovy.transform.CompileStatic
import org.springframework.beans.factory.annotation.Autowired

/**
 *  Created by ronald on 3/29/17.
 */
@SuppressWarnings("ChangeToOperator")
@CompileStatic
class ConsultsTab extends APanel {
    @Autowired
    private PatientService patientService
    @Autowired
    private StudentService studentService
    @Autowired
    private CycleService cycleService
    @Autowired
    private TeacherService teacherService
    @Autowired
    private RecordCardService recordCardService

    private ConsultsFormConsultsDesign consultsDesign = new ConsultsFormConsultsDesign()

    private Patient patientSelect
    private Cycle cycleSelect
    private Student studentSelect
    private Teacher teacherSelect

    ConsultsTab() {
        confLayout(consultsDesign)
    }

    @Override
    protected void confLanguage() {
        Constants.instance.autoWiredClass(this)
        consultsDesign.cbCycle.setCaption(Languages.instance.getText("window.cycle"))
        consultsDesign.cbTeacher.setCaption(Languages.instance.getText("window.teacher"))
        consultsDesign.cbStudent.setCaption(Languages.instance.getText("window.student"))
        consultsDesign.cbPatient.setCaption(Languages.instance.getText("window.patient"))
        consultsDesign.tfRecordNumber.setCaption(Languages.instance.getText("window.code"))
        consultsDesign.btnSearch.setCaption(Languages.instance.getText("window.search"))
    }

    @Override
    protected void buildLayout() {
        fillCycle()
        fillPatient()
        fillStudent()
        fillTeacher()
        consultsDesign.cbCycle.addValueChangeListener(new Property.ValueChangeListener() {
            @Override
            void valueChange(Property.ValueChangeEvent event) {
                if (consultsDesign.cbCycle.getValue().toString()) {
                    cycleSelect = cycleService.byId(consultsDesign.cbCycle.getValue() as Long)
                } else {
                    cycleSelect = null
                }
            }
        })
        consultsDesign.cbPatient.addValueChangeListener(new Property.ValueChangeListener() {
            @Override
            void valueChange(Property.ValueChangeEvent event) {
                if (consultsDesign.cbPatient.getValue().toString()) {
                    patientSelect = patientService.byId(consultsDesign.cbPatient.getValue() as Long)
                } else {
                    patientSelect = null
                }
            }
        })
        consultsDesign.cbStudent.addValueChangeListener(new Property.ValueChangeListener() {
            @Override
            void valueChange(Property.ValueChangeEvent event) {
                if (consultsDesign.cbStudent.getValue().toString()) {
                    studentSelect = studentService.byId(consultsDesign.cbStudent.getValue() as Long)
                } else {
                    studentSelect = null
                }
            }
        })
        consultsDesign.cbTeacher.addValueChangeListener(new Property.ValueChangeListener() {
            @Override
            void valueChange(Property.ValueChangeEvent event) {
                if (consultsDesign.cbTeacher.getValue().toString()) {
                    teacherSelect = teacherService.byId(consultsDesign.cbTeacher.getValue() as Long)
                } else {
                    teacherSelect = null
                }
            }
        })
        consultsDesign.btnSearch.addClickListener(new Button.ClickListener() {
            @Override
            void buttonClick(Button.ClickEvent event) {
                updateTable()
            }
        })
        initGrid()
        gridDetails()
    }

    private void initGrid() {
        manageGrid(consultsDesign.grid, new ConsultsContainer(new LazyQuery<RecordCard>() {
            @Override
            int getSize() {
                return recordCardService.count(true, consultsDesign.tfRecordNumber.getValue(), cycleSelect, studentSelect, teacherSelect, patientSelect)
            }

            @Override
            List<RecordCard> getItemsIds(int startIndex, int numberOfIds) {
                return recordCardService.list(true, startIndex, numberOfIds, consultsDesign.tfRecordNumber.getValue(), cycleSelect, studentSelect, teacherSelect, patientSelect, {
                    it.patient?.enabled;
                    it.patient?.person?.enabled;
                    it.cycle?.enabled;
                    it.student?.enabled;
                    it.teacher?.enabled;
                    it.patient?.person?.location?.enabled;
                    it.patient?.person?.occupation?.enabled;
                    it.patient?.person?.educationLevel?.enabled;
                    it.student?.person?.enabled;
                    it.teacher?.person?.enabled;
                    it.occlusion?.enabled;
                    it.listPatientEvaluationAnswer.size()
                    it.listCariesIndex.size()
                    it.listPatientExtraBuccalAnswer.size()
                    it.listPatientIntraBuccalAnswer.size()
                    it.listMedObservations.size()
                    it.listOralHygieneIndex.size()
                    it.listPatientHabits.size()
                    it.listPatientIllness.size()
                    it.listToothGroup.size()
                    it.oralHygiene?.enabled;
                    it.motherOccupation?.enabled;
                    it.fatherOccupation?.enabled
                })
            }

            @Override
            int getFilteredSize() {
                return 0
            }

            @Override
            List<RecordCard> getFilteredItemsIds(int startIndex, int numberOfIds) {
                return null
            }
        }))
    }

    private void gridDetails() {
        consultsDesign.grid.addItemClickListener(new ItemClickEvent.ItemClickListener() {
            @Override
            void itemClick(ItemClickEvent event) {
                if (event.isDoubleClick()) {
                    gridManager.grid.select(event.itemId)
                    RecordCard recordCardSelected = gridManager.grid.getSelectedRow() as RecordCard
                    RecordCardForm recordCardForm = new RecordCardForm(recordCardSelected, false)
                    getUI().addWindow(recordCardForm)
                }
            }
        })
    }

    void fillCycle() {
        consultsDesign.cbCycle.removeAllItems()
        for (Cycle cycle : cycleService.list(true, { it.listSubjects.size() })) {
            consultsDesign.cbCycle.addItem(cycle.id)
            consultsDesign.cbCycle.setItemCaption(cycle.id, cycle.toString())
        }
    }

    void fillTeacher() {
        consultsDesign.cbTeacher.removeAllItems()
        for (Teacher teacher : teacherService.list(true, { it.person.enabled })) {
            consultsDesign.cbTeacher.addItem(teacher.id)
            consultsDesign.cbTeacher.setItemCaption(teacher.id, teacher.toString())
        }
    }

    void fillStudent() {
        consultsDesign.cbStudent.removeAllItems()
        for (Student student : studentService.list(true, { it.person.enabled })) {
            consultsDesign.cbStudent.addItem(student.id)
            consultsDesign.cbStudent.setItemCaption(student.id, student.toString())
        }
    }

    void fillPatient() {
        consultsDesign.cbPatient.removeAllItems()
        for (Patient patient : patientService.list(true, { it.person.enabled })) {
            consultsDesign.cbPatient.addItem(patient.id)
            consultsDesign.cbPatient.setItemCaption(patient.id, patient.toString())
        }
    }

    @Override
    protected void security() {
    }
}
