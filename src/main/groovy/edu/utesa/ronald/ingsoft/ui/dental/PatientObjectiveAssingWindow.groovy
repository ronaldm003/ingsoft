package edu.utesa.ronald.ingsoft.ui.dental

import com.vaadin.data.Property
import com.vaadin.event.SelectionEvent
import com.vaadin.server.FontAwesome
import com.vaadin.server.Sizeable
import com.vaadin.ui.Button
import com.vaadin.ui.themes.ValoTheme
import edu.utesa.ronald.ingsoft.components.abstracts.AWindow
import edu.utesa.ronald.ingsoft.domains.admin.Cycle
import edu.utesa.ronald.ingsoft.domains.admin.Objective
import edu.utesa.ronald.ingsoft.domains.admin.Student
import edu.utesa.ronald.ingsoft.domains.admin.Teacher
import edu.utesa.ronald.ingsoft.domains.control.Patient
import edu.utesa.ronald.ingsoft.models.containers.admin.ObjectiveContainer
import edu.utesa.ronald.ingsoft.models.interfaces.OnNew
import edu.utesa.ronald.ingsoft.models.interfaces.PickElement
import edu.utesa.ronald.ingsoft.models.interfaces.WindowStartFocus
import edu.utesa.ronald.ingsoft.models.suggestions.ObjectiveSuggestions
import edu.utesa.ronald.ingsoft.models.suggestions.PatientSuggestions
import edu.utesa.ronald.ingsoft.models.suggestions.StudentSuggestions
import edu.utesa.ronald.ingsoft.models.suggestions.TeacherSuggestions
import edu.utesa.ronald.ingsoft.services.admin.CycleService
import edu.utesa.ronald.ingsoft.services.admin.ObjectiveService
import edu.utesa.ronald.ingsoft.services.admin.StudentService
import edu.utesa.ronald.ingsoft.services.admin.TeacherService
import edu.utesa.ronald.ingsoft.services.control.PatientService
import edu.utesa.ronald.ingsoft.ui.admin.StudentForm
import edu.utesa.ronald.ingsoft.ui.admin.TeacherForm
import edu.utesa.ronald.ingsoft.ui.components.TabWindowBase
import edu.utesa.ronald.ingsoft.ui.components.forms.OneToOneAction
import edu.utesa.ronald.ingsoft.ui.control.PatientForm
import edu.utesa.ronald.ingsoft.ui.utils.pickers.ObjectivePicker
import edu.utesa.ronald.ingsoft.ui.utils.pickers.PatientPicker
import edu.utesa.ronald.ingsoft.ui.utils.pickers.StudentPicker
import edu.utesa.ronald.ingsoft.ui.utils.pickers.TeacherPicker
import edu.utesa.ronald.ingsoft.utils.Constants
import edu.utesa.ronald.ingsoft.utils.Languages
import edu.utesa.ronald.ingsoft.utils.ProcessAppointments
import edu.utesa.ronald.ingsoft.utils.WindowsCaption
import edu.utesa.ronald.ingsoft.utils.container.LazyQuery
import groovy.transform.CompileStatic
import org.springframework.beans.factory.annotation.Autowired

/**
 *
 *  Created by ronald on 4/18/2018.
 */
@CompileStatic
class PatientObjectiveAssingWindow extends AWindow implements WindowStartFocus {

    @Autowired
    private PatientService patientService
    @Autowired
    private StudentService studentService
    @Autowired
    private TeacherService teacherService
    @Autowired
    private ObjectiveService objectiveService
    @Autowired
    private CycleService cycleService

    private PatientObjectiveAssignDesign patientObjectiveAssignDesign = new PatientObjectiveAssignDesign()

    private TabWindowBase tabWindowBase = new TabWindowBase()

    private OneToOneAction oneToOneActionPatient = new OneToOneAction(false)
    private OneToOneAction oneToOneActionStudent = new OneToOneAction(false)
    private OneToOneAction oneToOneActionTeacher = new OneToOneAction(false)

    private Patient patientSelect
    private Student studentSelect
    private Teacher teacherSelect
    private Objective objectiveSelect
    private Cycle cycleSelect

    private List<Objective> objectiveList = new ArrayList<>()

    PatientObjectiveAssingWindow() {
        Constants.instance.autoWiredClass(this)
        confLayout(patientObjectiveAssignDesign)
    }

    @Override
    protected void prepare() {
        setWidth(1024, Sizeable.Unit.PIXELS)
        setResizable(false)
        center()
        setModal(true)
        setCaption(Languages.instance.getText(WindowsCaption.ASSIGN_CAPTION))
    }

    @Override
    protected void confLanguage() {
        oneToOneActionStudent.tfACSearch().setCaption(Languages.instance.getText("window.student"))
        oneToOneActionPatient.tfACSearch().setCaption(Languages.instance.getText("window.patient"))
        oneToOneActionTeacher.tfACSearch().setCaption(Languages.instance.getText("window.teacher"))
        patientObjectiveAssignDesign.btnCancel.setCaption(Languages.instance.getText("window.action.cancel"))
        patientObjectiveAssignDesign.btnCancel.setIcon(FontAwesome.UNDO)
        patientObjectiveAssignDesign.btnSave.setCaption(Languages.instance.getText("window.action.save"))
        patientObjectiveAssignDesign.btnSave.setIcon(FontAwesome.SAVE)
        patientObjectiveAssignDesign.cbCycle.addStyleName(ValoTheme.COMBOBOX_TINY)
    }

    @Override
    protected void buildLayout() {
        initStudent()
        initPatient()
        initTeacher()
        initObjectives()
        fillCycle()
        patientObjectiveAssignDesign.btnCancel.addClickListener(new Button.ClickListener() {
            @Override
            void buttonClick(Button.ClickEvent event) {
                close()
            }
        })
        patientObjectiveAssignDesign.btnSave.addClickListener(new Button.ClickListener() {
            @Override
            void buttonClick(Button.ClickEvent event) {
                ProcessAppointments.instance.validateAppointments(loginManager, cycleSelect, studentSelect, patientSelect, teacherSelect, objectiveList)
            }
        })
        patientObjectiveAssignDesign.cbCycle.addValueChangeListener(new Property.ValueChangeListener() {
            @Override
            void valueChange(Property.ValueChangeEvent event) {
                if (patientObjectiveAssignDesign.cbCycle.getValue().toString()) {
                    cycleSelect = cycleService.byId(patientObjectiveAssignDesign.cbCycle.getValue() as Long, {
                        it.listSubjects.size()
                    })
                } else {
                    cycleSelect = null
                }
            }
        })

    }

    void fillCycle() {
        patientObjectiveAssignDesign.cbCycle.removeAllItems()
        for (Cycle cycle : cycleService.list(true, { it.listSubjects.size() })) {
            patientObjectiveAssignDesign.cbCycle.addItem(cycle.id)
            patientObjectiveAssignDesign.cbCycle.setItemCaption(cycle.id, cycle.toString())
        }
    }

    private void initObjectives() {
        patientObjectiveAssignDesign.pContent.setContent(tabWindowBase)
        manageGrid(tabWindowBase.grid(), new ObjectiveContainer(new LazyQuery<Objective>() {
            @Override
            int getSize() {
                return objectiveList.size()
            }

            @Override
            List<Objective> getItemsIds(int startIndex, int numberOfIds) {
                return objectiveList.subList(startIndex, (startIndex + numberOfIds))
            }

            @Override
            int getFilteredSize() {
                return 0
            }

            @Override
            List<Objective> getFilteredItemsIds(int startIndex, int numberOfIds) {
                return new ArrayList<Objective>()
            }
        }))
        tabWindowBase.grid().addSelectionListener(new SelectionEvent.SelectionListener() {
            @Override
            void select(SelectionEvent event) {
                if (gridManager.getGrid().getSelectedRow()) {
                    objectiveSelect = (Objective) gridManager.getGrid().getSelectedRow()
                    tabWindowBase.enableMenuAction()
                } else {
                    objectiveSelect = null
                    tabWindowBase.disableMenuAction()
                }
            }
        })
        tabWindowBase.oneToManyAction.tfACSearch().setSuggestionProvider(new ObjectiveSuggestions())
        tabWindowBase.oneToManyAction.tfACSearch().addValueChangeListener(new Property.ValueChangeListener() {
            @Override
            void valueChange(Property.ValueChangeEvent event) {
                if (tabWindowBase.oneToManyAction.tfACSearch().getValue() && !tabWindowBase.oneToManyAction.tfACSearch().getValue().isEmpty()) {
                    try {
                        objectiveSelect = objectiveService.byDescription(tabWindowBase.oneToManyAction.tfACSearch().getValue())
                    } catch (Exception ignored) {
                    }
                } else {
                    objectiveSelect = null
                }
            }
        })
        tabWindowBase.oneToManyAction.btnAdd().addClickListener(new Button.ClickListener() {
            @Override
            void buttonClick(Button.ClickEvent clickEvent) {
                if (objectiveSelect) {
                    Objective objective = objectiveService.byDescription(tabWindowBase.oneToManyAction.tfACSearch().getValue())
                    objectiveList.add(objective)
                    objectiveList = objectiveList.toSet().toList()
                    tabWindowBase.oneToManyAction.tfACSearch().setValue("")
                    updateTable()
                }
            }
        })
        tabWindowBase.oneToManyAction.btnDelete().addClickListener(new Button.ClickListener() {
            @Override
            void buttonClick(Button.ClickEvent clickEvent) {
                if (objectiveSelect) {
                    objectiveList.remove(objectiveSelect)
                    updateTable()
                }
            }
        })
        tabWindowBase.oneToManyAction.btnNew().setVisible(false)
        tabWindowBase.oneToManyAction.btnSearch().addClickListener(new Button.ClickListener() {
            @Override
            void buttonClick(Button.ClickEvent event) {
                ObjectivePicker objectivePicker = new ObjectivePicker(new PickElement<Objective>() {
                    @Override
                    void onResult(List<Objective> elements) {
                        objectiveList.addAll(elements)
                        objectiveList = objectiveList.toSet().toList()
                        updateTable()
                    }
                }, true)
                getUI().addWindow(objectivePicker)
            }
        })
    }

    private void initStudent() {
        patientObjectiveAssignDesign.pStudent.setContent(oneToOneActionStudent)
        oneToOneActionStudent.tfACSearch().setSuggestionProvider(new StudentSuggestions())
        oneToOneActionStudent.tfACSearch().addValueChangeListener(new Property.ValueChangeListener() {
            @Override
            void valueChange(Property.ValueChangeEvent event) {
                if (oneToOneActionStudent.tfACSearch().getValue() && !oneToOneActionStudent.tfACSearch().getValue().isEmpty()) {
                    try {
                        studentSelect = studentService.byEnrollment(oneToOneActionStudent.tfACSearch().getValue().split(":")[0], {
                            it.person.enabled; it.listSubjects?.size();
                        })
                        selectStudent(studentSelect)
                    } catch (Exception ignored) {
                    }
                } else {
                    studentSelect = null
                }
            }
        })
        oneToOneActionStudent.btnNew().addClickListener(new Button.ClickListener() {
            @Override
            void buttonClick(Button.ClickEvent event) {
                StudentForm studentForm = new StudentForm(new OnNew<Student>() {
                    @Override
                    void result(Student student) {
                        studentSelect = student
                        selectStudent(studentSelect)
                    }
                })
                getUI().addWindow(studentForm)
            }
        })
        oneToOneActionStudent.btnSearch().addClickListener(new Button.ClickListener() {
            @Override
            void buttonClick(Button.ClickEvent event) {
                StudentPicker locationPicker = new StudentPicker(new PickElement<Student>() {
                    @Override
                    void onResult(List<Student> elements) {
                        if (elements.size() > 0) {
                            studentSelect = elements.get(0)
                            selectStudent(studentSelect)
                        }
                    }
                }, false)
                getUI().addWindow(locationPicker)
            }
        })
    }

    void selectStudent(Student student) {
        if (student) {
            oneToOneActionStudent.tfACSearch().setReadOnly(false)
            oneToOneActionStudent.select(student ? student.toString() : "")
            oneToOneActionStudent.tfACSearch().setReadOnly(true)
        }
    }

    private void initPatient() {
        patientObjectiveAssignDesign.pPatient.setContent(oneToOneActionPatient)
        oneToOneActionPatient.tfACSearch().setSuggestionProvider(new PatientSuggestions())
        oneToOneActionPatient.tfACSearch().addValueChangeListener(new Property.ValueChangeListener() {
            @Override
            void valueChange(Property.ValueChangeEvent event) {
                if (oneToOneActionPatient.tfACSearch().getValue() && !oneToOneActionPatient.tfACSearch().getValue().isEmpty()) {
                    try {
                        patientSelect = patientService.byWarrant(oneToOneActionPatient.tfACSearch().getValue().split("-")[1])
                        selectPatient(patientSelect)
                    } catch (Exception ignored) {
                    }
                } else {
                    patientSelect = null
                }
            }
        })
        oneToOneActionPatient.btnNew().addClickListener(new Button.ClickListener() {
            @Override
            void buttonClick(Button.ClickEvent event) {
                PatientForm patientForm = new PatientForm(new OnNew<Patient>() {
                    @Override
                    void result(Patient patient) {
                        patientSelect = patient
                        selectPatient(patientSelect)
                    }
                })
                getUI().addWindow(patientForm)
            }
        })
        oneToOneActionPatient.btnSearch().addClickListener(new Button.ClickListener() {
            @Override
            void buttonClick(Button.ClickEvent event) {
                PatientPicker patientPicker = new PatientPicker(new PickElement<Patient>() {
                    @Override
                    void onResult(List<Patient> elements) {
                        if (elements.size() > 0) {
                            patientSelect = elements.get(0)
                            selectPatient(patientSelect)
                        }
                    }
                }, false)
                getUI().addWindow(patientPicker)
            }
        })
    }

    void selectPatient(Patient patient) {
        if (patient) {
            oneToOneActionPatient.tfACSearch().setReadOnly(false)
            oneToOneActionPatient.select(patient ? patient.toString() : "")
            oneToOneActionPatient.tfACSearch().setReadOnly(true)
        }
    }

    private void initTeacher() {
        patientObjectiveAssignDesign.pTeacher.setContent(oneToOneActionTeacher)
        oneToOneActionTeacher.tfACSearch().setSuggestionProvider(new TeacherSuggestions())
        oneToOneActionTeacher.tfACSearch().addValueChangeListener(new Property.ValueChangeListener() {
            @Override
            void valueChange(Property.ValueChangeEvent event) {
                if (oneToOneActionTeacher.tfACSearch().getValue() && !oneToOneActionTeacher.tfACSearch().getValue().isEmpty()) {
                    try {
                        teacherSelect = teacherService.byCode(oneToOneActionTeacher.tfACSearch().getValue().split(":")[0], {
                            it.person.enabled; it.listSubjects.size()
                        })
                        selectTeacher(teacherSelect)
                    } catch (Exception ignored) {
                    }
                } else {
                    teacherSelect = null
                }
            }
        })
        oneToOneActionTeacher.btnNew().addClickListener(new Button.ClickListener() {
            @Override
            void buttonClick(Button.ClickEvent event) {
                TeacherForm teacherForm = new TeacherForm(new OnNew<Teacher>() {
                    @Override
                    void result(Teacher teacher) {
                        teacherSelect = teacher
                        selectTeacher(teacherSelect)
                    }
                })
                getUI().addWindow(teacherForm)
            }
        })
        oneToOneActionTeacher.btnSearch().addClickListener(new Button.ClickListener() {
            @Override
            void buttonClick(Button.ClickEvent event) {
                TeacherPicker teacherPicker = new TeacherPicker(new PickElement<Teacher>() {
                    @Override
                    void onResult(List<Teacher> elements) {
                        if (elements.size() > 0) {
                            teacherSelect = elements.get(0)
                            selectTeacher(teacherSelect)
                        }
                    }
                }, false)
                getUI().addWindow(teacherPicker)
            }
        })
    }

    void selectTeacher(Teacher teacher) {
        if (teacher) {
            oneToOneActionTeacher.tfACSearch().setReadOnly(false)
            oneToOneActionTeacher.select(teacher ? teacher.toString() : "")
            oneToOneActionTeacher.tfACSearch().setReadOnly(true)
        }
    }

    @Override
    protected void security() {
    }

    @Override
    void startFocus() {
    }


}
