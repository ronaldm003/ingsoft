package edu.utesa.ronald.ingsoft.ui.events

import com.vaadin.server.Sizeable
import com.vaadin.server.VaadinSession
import com.vaadin.ui.Button
import com.vaadin.ui.TabSheet
import com.vaadin.ui.themes.ValoTheme
import edu.utesa.ronald.ingsoft.components.abstracts.AWindowEventForm
import edu.utesa.ronald.ingsoft.domains.appointment.Appointment
import edu.utesa.ronald.ingsoft.models.LoginManager
import edu.utesa.ronald.ingsoft.models.interfaces.WindowStartFocus
import edu.utesa.ronald.ingsoft.services.appointment.AppointmentService
import edu.utesa.ronald.ingsoft.utils.Constants
import edu.utesa.ronald.ingsoft.utils.Languages
import edu.utesa.ronald.ingsoft.utils.ShowNotification
import edu.utesa.ronald.ingsoft.utils.WindowsCaption
import groovy.transform.CompileStatic
import org.springframework.beans.factory.annotation.Autowired

/**
 *  Created by Ronald on 7/25/2016.
 */
@CompileStatic
class AppointmentForm extends AWindowEventForm {

    @Autowired
    private AppointmentService appointmentService

    private AppointmentFormTabAppointment calendarEventFormTabEvent
    private Appointment appointmentSelect

    private Date start
    private Date end

    AppointmentForm(Date start, Date end) {
        appointmentSelect = new Appointment()
        this.start = start
        this.end = end
        confLayout()
    }

    AppointmentForm(Appointment appointment, boolean edit) {
        appointmentSelect = appointment
        this.edit = edit
        confLayout()
    }

    protected void confLayout() {
        Constants.instance.autoWiredClass(this)
        loginManager = (LoginManager) VaadinSession.current.session.getAttribute(Constants.USER_LOGIN)
        setModal(true)
        center()
        setWidth(500, Sizeable.Unit.PIXELS)
        initLayout(1, 1)
        prepareTab(WindowsCaption.APPOINTMENT_CAPTION)
        showLayout()
    }

    @Override
    protected void prepareTabStart() {
        tabSheet.addSelectedTabChangeListener(new TabSheet.SelectedTabChangeListener() {
            @Override
            void selectedTabChange(TabSheet.SelectedTabChangeEvent selectedTabChangeEvent) {
                initFocus()
                center()
            }
        })
        tabSheet.setSelectedTab(0)
        initFocus()
    }

    private boolean isView() {
        return edit != null & !edit
    }

    private void initFocus() {
        if (tabSheet.getSelectedTab() instanceof WindowStartFocus) {
            (tabSheet.getSelectedTab() as WindowStartFocus).startFocus()
        }
    }

    @Override
    protected void prepareTab(String captionLanguage) {
        if (edit == null) {
            onCreate()
            setCaption(Languages.instance.getText(captionLanguage + NEW_CAPTION))
        } else if (!edit) {
            onEditView()
            formEventAction.getBtnApplyEvent().setEnabled(false)
            setCaption(Languages.instance.getText(captionLanguage + VIEW_CAPTION))
        } else {
            onEditView()
            setCaption(Languages.instance.getText(captionLanguage + EDIT_CAPTION))
        }
    }

    @Override
    protected void onCreate() {

    }

    @Override
    protected void onEditView() {

    }

    @Override
    protected void buildLayout() {
        tabSheet.addStyleName(ValoTheme.TABSHEET_COMPACT_TABBAR)
        tabSheet.setSizeFull()
        formEventAction.getBtnApplyEvent().addClickListener(new Button.ClickListener() {
            @Override
            void buttonClick(Button.ClickEvent event) {
                if (AppointmentForm.this.getElementsValid()) {
                    appointmentSelect = calendarEventFormTabEvent.getAppointment()
                    appointmentSelect = appointmentService.create(appointmentSelect, loginManager)
                    if (appointmentSelect) {
                        ShowNotification.success(ValoTheme.NOTIFICATION_SUCCESS)
                        close()
                    } else {
                        ShowNotification.error()
                    }
                } else {
                    ShowNotification.incomplete()
                }
            }
        })

        formEventAction.getBtnCancelEvent().addClickListener(new Button.ClickListener() {
            @Override
            void buttonClick(Button.ClickEvent event) {
                close()
            }
        })
        formEventAction.getBtnDeleteEvent().addClickListener(new Button.ClickListener() {
            @Override
            void buttonClick(Button.ClickEvent event) {
                if (edit != null && edit) {
                    appointmentService.delete(appointmentSelect, loginManager)
                    close()
                }
            }
        })
        if (edit == null) {
            calendarEventFormTabEvent = new AppointmentFormTabAppointment(start, end)
        } else {
            calendarEventFormTabEvent = new AppointmentFormTabAppointment(appointmentSelect, edit)
        }
        tabSheet.addTab(calendarEventFormTabEvent, Languages.instance.getText("Event"))
        if (edit != null && !edit) {
            //
        }
        tabSheet.setSelectedTab(0)
        tabSheet.addSelectedTabChangeListener(new TabSheet.SelectedTabChangeListener() {
            @Override
            void selectedTabChange(TabSheet.SelectedTabChangeEvent selectedTabChangeEvent) {
                center()
            }
        })

        mainLayout.addComponent(tabSheet)
        mainLayout.addComponent(formEventAction)
    }

    @Override
    protected void restore() {
    }

    @Override
    protected void visualize() {
        formEventAction.getBtnApplyEvent().setEnabled(false)
    }

    @Override
    protected void security() {
    }

    @Override
    protected boolean getElementsValid() {
        return true
    }
}
