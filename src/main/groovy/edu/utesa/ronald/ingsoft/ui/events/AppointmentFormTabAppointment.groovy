package edu.utesa.ronald.ingsoft.ui.events

import com.vaadin.data.Property
import com.vaadin.server.VaadinSession
import com.vaadin.shared.ui.datefield.Resolution
import com.vaadin.ui.*
import edu.utesa.ronald.ingsoft.components.abstracts.AWindowTabForm
import edu.utesa.ronald.ingsoft.domains.appointment.Appointment
import edu.utesa.ronald.ingsoft.domains.appointment.AppointmentType
import edu.utesa.ronald.ingsoft.models.LoginManager
import edu.utesa.ronald.ingsoft.services.appointment.AppointmentTypeService
import edu.utesa.ronald.ingsoft.utils.Constants
import edu.utesa.ronald.ingsoft.utils.Languages
import groovy.transform.CompileStatic
import org.springframework.beans.factory.annotation.Autowired

/**
 *  Created by ronald on 26/7/2016.
 */
@CompileStatic
class AppointmentFormTabAppointment extends AWindowTabForm {

    @Autowired
    private AppointmentTypeService appointmentTypeService

//    @Autowired
//    private PatientService patientService

    private VerticalLayout vlControl1

    private DateField startDateField
    private DateField endDateField

//    private AutocompleteTextField tfPatient
    private TextField tfName
    private TextArea taDescription
    private ComboBox cbAppointmentType

    private boolean useSecondResolution

    private Appointment appointmentSave
    private AppointmentType appointmentTypeSelect
//    private Patient patientSelect

    private Date start
    private Date end

    AppointmentFormTabAppointment(Date start, Date end) {
        appointmentSave = new Appointment()
        appointmentTypeSelect = new AppointmentType()
        this.start = start
        this.end = end
        confLayout()
    }

    AppointmentFormTabAppointment(Appointment appointment, Boolean edit) {
        appointmentSave = appointment
        appointmentTypeSelect = appointmentSave.appointmentType
//        patientSelect = appointmentSave.patient
        this.edit = edit
        confLayout()
    }

    @Override
    protected void confLayout() {
        Constants.instance.autoWiredClass(this)
        loginManager = (LoginManager) VaadinSession.current.session.getAttribute(Constants.USER_LOGIN)
        initLayout(1, 1)
        if (edit != null) {
            restore()
            if (!edit) {
                visualize()
            }
        }
        showLayout()
    }

    @Override
    protected void buildLayout() {
        startDateField = createDateField(Languages.instance.getText("form.calendar.date.event.star"))
        startDateField.setResolution(Resolution.MINUTE)
//        startDateField.setDateFormat(Constants.systemDateFormat)
        startDateField.setValue(start)
        startDateField.setSizeFull()

        endDateField = createDateField(Languages.instance.getText("form.calendar.date.event.end"))
        endDateField.setResolution(Resolution.MINUTE)
//        endDateField.setDateFormat(Constants.systemDateFormat)
        endDateField.setValue(end)
        endDateField.setSizeFull()

//        tfPatient = new AutocompleteTextField(Languages.instance.getText("form.calendar.date.event.patient"))
//        tfPatient.setSizeFull()
//        tfPatient.setRequired(true)
//        tfPatient.setCache(true)
//        tfPatient.setDelay(150)
//        tfPatient.setItemAsHtml(false)
//        tfPatient.setMinChars(1)
//        tfPatient.setScrollBehavior(ScrollBehavior.NONE)
//        tfPatient.setSuggestionLimit(0)
//        tfPatient.setSuggestionProvider(new PatientSuggestions())
//        tfPatient.addValueChangeListener(new Property.ValueChangeListener() {
//            @Override
//            void valueChange(Property.ValueChangeEvent event) {
//                if (tfPatient.getValue() && !tfPatient.getValue().isEmpty()) {
//                    try {
//                        patientSelect = patientService.findByName(tfPatient.getValue().split("-")[0])
//                    } catch (Exception ignored) {
//                    }
//                } else {
//                    println "ESTO EN BLANCO AQUI"
//                    patientSelect = null
//                }
//            }
//        })

        tfName = new TextField(Languages.instance.getText("form.calendar.date.event.caption"))
        tfName.setNullRepresentation("")
        tfName.setSizeFull()
        tfName.setInputPrompt(Languages.instance.getText("form.calendar.date.event.captionprompt"))
        tfName.setRequired(true)

        taDescription = new TextArea(Languages.instance.getText("form.calendar.date.event.description"))
        taDescription.setNullRepresentation("")
        taDescription.setSizeFull()
        taDescription.setInputPrompt(Languages.instance.getText("form.calendar.date.event.descriptionprompt"))
        taDescription.setRows(3)

        cbAppointmentType = createAppointmentTypeComboBox()
        cbAppointmentType.setInputPrompt(Languages.instance.getText("form.calendar.date.event.stylenameprompt"))
        cbAppointmentType.setSizeFull()
        cbAppointmentType.setTextInputAllowed(false)
        cbAppointmentType.addValueChangeListener(new Property.ValueChangeListener() {
            @Override
            void valueChange(Property.ValueChangeEvent valueChangeEvent) {
                if (cbAppointmentType.getValue().toString()) {
                    appointmentTypeSelect = appointmentTypeService.findById(cbAppointmentType.getValue() as Long)
                } else {
                    appointmentTypeSelect = null
                }
            }
        })

        vlControl1 = new VerticalLayout()
        vlControl1.setSpacing(true)
        vlControl1.setSizeFull()

        vlControl1.addComponent(startDateField)
        vlControl1.addComponent(endDateField)
//        vlControl1.addComponent(tfPatient)
        vlControl1.addComponent(tfName)
        vlControl1.addComponent(taDescription)
        vlControl1.addComponent(cbAppointmentType)

        mainLayout.addComponent(vlControl1, 0, 0)
    }

    @Override
    protected void restore() {
        startDateField.setValue(appointmentSave.getStart())
        endDateField.setValue(appointmentSave.getEnd())
//        patientSelect = appointmentSave.getPatient()
//        tfPatient.setValue(appointmentSave.getPatient() ? appointmentSave.getPatient().name : "")
//        tfName.setValue(appointmentSave.getName())
        taDescription.setValue(appointmentSave.getDescription())
        if (appointmentSave.appointmentType) {
            cbAppointmentType.setValue(appointmentSave.getAppointmentType().id)
        }
    }

    @Override
    protected void visualize() {

    }

    @Override
    protected void security() {
    }

    @Override
    protected boolean getElementsValid() {
        return true
    }

    @Override
    protected void collectData() {
        appointmentSave.setStart(startDateField.getValue())
        appointmentSave.setEnd(endDateField.getValue())
//        if (patientSelect) {
//            appointmentSave.setPatient(patientSelect)
//        }
        appointmentSave.setName(tfName.getValue())
        appointmentSave.setDescription(taDescription.getValue())
        appointmentSave.setAppointmentType(appointmentTypeSelect)
    }

    private DateField createDateField(String caption) {
        DateField dateField = new DateField(caption)
        if (useSecondResolution) {
            dateField.setResolution(Resolution.SECOND)
        } else {
            dateField.setResolution(Resolution.MINUTE)
        }
        return dateField
    }

    private ComboBox createAppointmentTypeComboBox() {
        ComboBox cbAppointmentTypeStyle = new ComboBox(caption)
        cbAppointmentTypeStyle.removeAllItems()
        for (AppointmentType appointmentType : appointmentTypeService.list(true)) {
            cbAppointmentTypeStyle.addItem(appointmentType.getId())
            cbAppointmentTypeStyle.setItemCaption(appointmentType.getId(), appointmentType.getName())
        }
        return cbAppointmentTypeStyle
    }

    Appointment getAppointment() {
        if (getElementsValid()) {
            collectData()
            return appointmentSave
        }
        return null
    }
}
