package edu.utesa.ronald.ingsoft.ui.location

import com.vaadin.server.Sizeable
import edu.utesa.ronald.ingsoft.domains.location.Clinic
import edu.utesa.ronald.ingsoft.services.location.ClinicService
import edu.utesa.ronald.ingsoft.ui.components.WindowForm
import edu.utesa.ronald.ingsoft.utils.Constants
import edu.utesa.ronald.ingsoft.utils.Languages
import edu.utesa.ronald.ingsoft.utils.WindowsCaption
import groovy.transform.CompileStatic
import org.springframework.beans.factory.annotation.Autowired

/**
 * Aqui se realizan todos los casos de los botones de accion.
 *  Created by ronald on 3/07/17.
 */
@CompileStatic
class ClinicForm extends WindowForm<Clinic> {

    @Autowired
    private ClinicService clinicService

    private ClinicFormClinic formClinic
    private ClinicFormTabEquipments formTabEquipments

    ClinicForm() {
        this(null, null)
    }

    ClinicForm(Clinic location, Boolean edit) {
        super(location, edit)
        confLayout(WindowsCaption.CLINIC_CAPTION)
        setWidth(600, Sizeable.Unit.PIXELS)
    }

    @Override
    protected void prepare() {
        super.prepare()
        Constants.instance.autoWiredClass(this)
    }

    @Override
    protected void onCreate() {
        formClinic = new ClinicFormClinic()
        formTabEquipments = new ClinicFormTabEquipments()
    }

    @Override
    protected void onEditView() {
        itemSave = clinicService.refresh(itemSave.id, {
            it.listEquipments.size(); it.location?.enabled; it.inventory?.enabled
        })
        formClinic = new ClinicFormClinic(itemSave, edit)
        formTabEquipments = new ClinicFormTabEquipments(itemSave.listEquipments.toList(), edit)
    }

    @Override
    Clinic onSave() {
        itemSave = formClinic.getValid()
        itemSave.listEquipments = formTabEquipments.getValid().toSet()
        return clinicService.create(itemSave, loginManager)
    }

    @Override
    protected void onViewTabs() {

    }

    @Override
    protected void buildView() {

    }

    @Override
    protected boolean getValidForm() {
        if (!formClinic.getValid()) {
            return tabSheetManager.selectTab(0)
        }
//        else if (!formTabEquipments.getValid()) {
//            tabSheetManager.selectTab(1)
//            return false
//        }
        return true
    }

    @Override
    protected void buildLayout() {
        setWidth(800, Sizeable.Unit.PIXELS)
        tabSheetManager.addTabPanel(Languages.instance.getText(WindowsCaption.CLINIC_CAPTION + FORM + "clinic"), formClinic)
        tabSheetManager.addTabPanel(Languages.instance.getText(WindowsCaption.CLINIC_CAPTION + FORM + "equipment"), formTabEquipments)

    }

    @Override
    protected void security() {

    }
}
