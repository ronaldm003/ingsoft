package edu.utesa.ronald.ingsoft.ui.location

import com.vaadin.data.Property
import com.vaadin.event.FieldEvents
import com.vaadin.ui.Button
import edu.utesa.ronald.ingsoft.components.abstracts.AWindowTab
import edu.utesa.ronald.ingsoft.domains.control.Inventory
import edu.utesa.ronald.ingsoft.domains.location.Clinic
import edu.utesa.ronald.ingsoft.domains.location.Location
import edu.utesa.ronald.ingsoft.models.interfaces.PickElement
import edu.utesa.ronald.ingsoft.models.suggestions.InventorySuggestions
import edu.utesa.ronald.ingsoft.models.suggestions.LocationSuggestions
import edu.utesa.ronald.ingsoft.services.control.InventoryService
import edu.utesa.ronald.ingsoft.services.location.ClinicService
import edu.utesa.ronald.ingsoft.services.location.LocationService
import edu.utesa.ronald.ingsoft.ui.components.forms.OneToOneAction
import edu.utesa.ronald.ingsoft.ui.utils.pickers.InventoryPicker
import edu.utesa.ronald.ingsoft.ui.utils.pickers.LocationPicker
import edu.utesa.ronald.ingsoft.utils.Constants
import edu.utesa.ronald.ingsoft.utils.Languages
import groovy.transform.CompileStatic
import org.springframework.beans.factory.annotation.Autowired

/**
 *  Created by ronald on 3/07/17.
 */
@CompileStatic
class ClinicFormClinic extends AWindowTab<Clinic> {

    @Autowired
    private ClinicService clinicService
    @Autowired
    private LocationService locationService
    @Autowired
    private InventoryService inventoryService

    private ClinicFormClinicDesign clinicFormClinicDesign = new ClinicFormClinicDesign()
    private OneToOneAction oneToOneActionLocation = new OneToOneAction(false)
    private OneToOneAction oneToOneActionInventory = new OneToOneAction(false)

    private Location locationSelect
    private Inventory inventorySelect

    ClinicFormClinic() {
        this(new Clinic(), null)
    }

    ClinicFormClinic(Clinic clinic, Boolean edit) {
        super(clinic, edit)
        confLayout(clinicFormClinicDesign)
    }

    @Override
    protected void buildLayout() {
        getNextCode()
        clinicFormClinicDesign.tfCode.setReadOnly(true)
        clinicFormClinicDesign.tfDescription.setRequired(true)
        clinicFormClinicDesign.tfDescription.setRequiredError(Languages.instance.getText("validations.empty.field"))
        clinicFormClinicDesign.tfDescription.setValidationVisible(true)
        clinicFormClinicDesign.tfDescription.setNullRepresentation("")
        clinicFormClinicDesign.tfDescription.addTextChangeListener(new FieldEvents.TextChangeListener() {
            @Override
            void textChange(FieldEvents.TextChangeEvent event) {
                clinicFormClinicDesign.tfDescription.setValue(event.getText())
                clinicFormClinicDesign.tfDescription.setCursorPosition(event.getCursorPosition())
                clinicFormClinicDesign.tfDescription.validate()
            }
        })
        initLocation()
        initInventory()
    }

    private void initLocation() {
        clinicFormClinicDesign.pLocation.setContent(oneToOneActionLocation)
        oneToOneActionLocation.tfACSearch().setSuggestionProvider(new LocationSuggestions())
        oneToOneActionLocation.tfACSearch().addValueChangeListener(new Property.ValueChangeListener() {
            @Override
            void valueChange(Property.ValueChangeEvent event) {
                if (oneToOneActionLocation.tfACSearch().getValue() && !oneToOneActionLocation.tfACSearch().getValue().isEmpty()) {
                    try {
                        locationSelect = locationService.byDescription(oneToOneActionLocation.tfACSearch().getValue())
                        selectLocation(locationSelect)
                    } catch (Exception ignored) {
                    }
                } else {
                    locationSelect = null
                }
            }
        })
        oneToOneActionLocation.btnNew().setEnabled(false)
        oneToOneActionLocation.btnNew().setVisible(false)
        oneToOneActionLocation.btnSearch().addClickListener(new Button.ClickListener() {
            @Override
            void buttonClick(Button.ClickEvent event) {
                LocationPicker locationPicker = new LocationPicker(new PickElement<Location>() {
                    @Override
                    void onResult(List<Location> elements) {
                        if (elements.size() > 0) {
                            locationSelect = elements.get(0)
                            selectLocation(locationSelect)
                        }
                    }
                }, false)
                getUI().addWindow(locationPicker)
            }
        })
        selectLocation(locationSelect)
    }

    void selectLocation(Location location) {
        if (location) {
            oneToOneActionLocation.tfACSearch().setReadOnly(false)
            oneToOneActionLocation.select(location.toString())
            oneToOneActionLocation.tfACSearch().setReadOnly(true)
        }
    }

    private void initInventory() {
        clinicFormClinicDesign.pInventory.setContent(oneToOneActionInventory)
        oneToOneActionInventory.tfACSearch().setSuggestionProvider(new InventorySuggestions())
        oneToOneActionInventory.tfACSearch().addValueChangeListener(new Property.ValueChangeListener() {
            @Override
            void valueChange(Property.ValueChangeEvent event) {
                if (oneToOneActionInventory.tfACSearch().getValue() && !oneToOneActionInventory.tfACSearch().getValue().isEmpty()) {
                    try {
                        inventorySelect = inventoryService.byName(oneToOneActionInventory.tfACSearch().getValue())
                        selectInventory(inventorySelect)
                    } catch (Exception ignored) {
                    }
                } else {
                    locationSelect = null
                }
            }
        })
        oneToOneActionInventory.btnNew().setEnabled(false)
        oneToOneActionInventory.btnNew().setVisible(false)
        oneToOneActionInventory.btnSearch().addClickListener(new Button.ClickListener() {
            @Override
            void buttonClick(Button.ClickEvent event) {
                InventoryPicker inventoryPicker = new InventoryPicker(new PickElement<Inventory>() {
                    @Override
                    void onResult(List<Inventory> elements) {
                        if (elements.size() > 0) {
                            inventorySelect = elements.get(0)
                            selectInventory(inventorySelect)
                        }
                    }
                }, false)
                getUI().addWindow(inventoryPicker)
            }
        })
        selectInventory(inventorySelect)
    }

    void selectInventory(Inventory inventory) {
        if (inventory) {
            oneToOneActionInventory.tfACSearch().setReadOnly(false)
            oneToOneActionInventory.select(inventory.toString())
            oneToOneActionInventory.tfACSearch().setReadOnly(true)
        }
    }

    private void getNextCode() {
        Long code = clinicService.findMaxCode()
        if (code) {
            clinicFormClinicDesign.tfCode.setValue((code + 1).toString())
        } else {
            clinicFormClinicDesign.tfCode.setValue("1")
        }
    }

    @Override
    protected void restore() {
        locationSelect = itemSave.location
        inventorySelect = itemSave.inventory
        clinicFormClinicDesign.tfCode.setReadOnly(false)
        clinicFormClinicDesign.tfCode.setValue(itemSave.code.toString())
        clinicFormClinicDesign.tfCode.setReadOnly(true)
        clinicFormClinicDesign.tfDescription.setValue(itemSave.description ?: "")
        selectLocation(itemSave.location)
        selectInventory(itemSave.inventory)
    }

    @Override
    protected void visualize() {
        clinicFormClinicDesign.tfCode.setReadOnly(true)
        clinicFormClinicDesign.tfDescription.setReadOnly(true)
        oneToOneActionLocation.visualize()
        oneToOneActionInventory.visualize()
    }

    @Override
    protected boolean getElementsValid() {
        if (!clinicFormClinicDesign.tfCode.isValid()) {
            clinicFormClinicDesign.tfCode.focus()
            return false
        } else if (!clinicFormClinicDesign.tfDescription.isValid()) {
            clinicFormClinicDesign.tfDescription.focus()
            return false
        } else if (!oneToOneActionLocation.isValid()) {
            oneToOneActionLocation.focusValid()
            return false
        } else if (!oneToOneActionInventory.isValid()) {
            oneToOneActionInventory.focusValid()
            return false
        }
        return true
    }

    @Override
    protected void collectData() {
        itemSave.code = clinicFormClinicDesign.tfCode.getValue().toLong()
        itemSave.description = clinicFormClinicDesign.tfDescription.getValue()
        itemSave.location = locationSelect
        itemSave.inventory = inventorySelect
    }

    @Override
    protected void confLanguage() {
        Constants.instance.autoWiredClass(this)
        clinicFormClinicDesign.tfCode.setCaption(Languages.instance.getText("window.id"))
        clinicFormClinicDesign.tfDescription.setCaption(Languages.instance.getText("window.description"))
        oneToOneActionLocation.tfACSearch().setCaption(Languages.instance.getText("window.location"))
        oneToOneActionInventory.tfACSearch().setCaption(Languages.instance.getText("window.inventory"))
    }

    @Override
    protected Clinic getValid() {
        if (getElementsValid()) {
            collectData()
            return itemSave
        }
        return null
    }

    @Override
    void startFocus() {
        clinicFormClinicDesign.tfDescription.focus()
    }
}
