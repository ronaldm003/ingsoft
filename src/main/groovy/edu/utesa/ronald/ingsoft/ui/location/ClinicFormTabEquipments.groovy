package edu.utesa.ronald.ingsoft.ui.location

import com.vaadin.data.Property
import com.vaadin.event.SelectionEvent
import com.vaadin.ui.Button
import edu.utesa.ronald.ingsoft.components.abstracts.AWindowTab
import edu.utesa.ronald.ingsoft.domains.control.Equipment
import edu.utesa.ronald.ingsoft.models.containers.control.EquipmentContainer
import edu.utesa.ronald.ingsoft.models.interfaces.PickElement
import edu.utesa.ronald.ingsoft.models.suggestions.EquipmentSuggestions
import edu.utesa.ronald.ingsoft.services.control.EquipmentService
import edu.utesa.ronald.ingsoft.ui.components.TabWindowBase
import edu.utesa.ronald.ingsoft.ui.utils.pickers.EquipmentPicker
import edu.utesa.ronald.ingsoft.utils.Constants
import edu.utesa.ronald.ingsoft.utils.container.LazyQuery
import groovy.transform.CompileStatic
import org.springframework.beans.factory.annotation.Autowired

/**
 *  Created by ronald on 04/12/18.
 */
@CompileStatic
class ClinicFormTabEquipments extends AWindowTab<List<Equipment>> {

    @Autowired
    private EquipmentService equipmentService

    private TabWindowBase tabWindowBase = new TabWindowBase()

    private Equipment equipmentSelect

    ClinicFormTabEquipments() {
        this(new ArrayList<Equipment>(), null)
    }

    ClinicFormTabEquipments(List<Equipment> itemSave, Boolean edit) {
        super(itemSave, edit)
        Constants.instance.autoWiredClass(this)
        confLayout(tabWindowBase)
    }

    @Override
    protected void confLanguage() {
    }

    @Override
    protected void buildLayout() {
        manageGrid(tabWindowBase.grid(), new EquipmentContainer(new LazyQuery<Equipment>() {
            @Override
            int getSize() {
                return ((List<Equipment>) itemSave).size()
            }

            @Override
            List<Equipment> getItemsIds(int startIndex, int numberOfIds) {
                return ((List<Equipment>) itemSave).subList(startIndex, (startIndex + numberOfIds))
            }

            @Override
            int getFilteredSize() {
                return 0
            }

            @Override
            List<Equipment> getFilteredItemsIds(int startIndex, int numberOfIds) {
                return new ArrayList<Equipment>()
            }
        }))
        tabWindowBase.grid().addSelectionListener(new SelectionEvent.SelectionListener() {
            @Override
            void select(SelectionEvent event) {
                if (gridManager.getGrid().getSelectedRow()) {
                    equipmentSelect = (Equipment) gridManager.getGrid().getSelectedRow()
                    tabWindowBase.enableMenuAction()
                } else {
                    equipmentSelect = null
                    tabWindowBase.disableMenuAction()
                }
            }
        })
        tabWindowBase.oneToManyAction.tfACSearch().setSuggestionProvider(new EquipmentSuggestions())
        tabWindowBase.oneToManyAction.tfACSearch().addValueChangeListener(new Property.ValueChangeListener() {
            @Override
            void valueChange(Property.ValueChangeEvent event) {
                if (tabWindowBase.oneToManyAction.tfACSearch().getValue() && !tabWindowBase.oneToManyAction.tfACSearch().getValue().isEmpty()) {
                    try {
                        equipmentSelect = equipmentService.byName(tabWindowBase.oneToManyAction.tfACSearch().getValue())
                    } catch (Exception ignored) {
                    }
                } else {
                    equipmentSelect = null
                }
            }
        })
        tabWindowBase.oneToManyAction.btnAdd().addClickListener(new Button.ClickListener() {
            @Override
            void buttonClick(Button.ClickEvent clickEvent) {
                if (equipmentSelect) {
                    Equipment equipment = equipmentService.byName(tabWindowBase.oneToManyAction.tfACSearch().getValue())
                    ((List<Equipment>) itemSave).add(equipment)
                    itemSave = ((List<Equipment>) itemSave).toSet().toList()
                    tabWindowBase.oneToManyAction.tfACSearch().setValue("")
                    updateTable()
                }
            }
        })
        tabWindowBase.oneToManyAction.btnDelete().addClickListener(new Button.ClickListener() {
            @Override
            void buttonClick(Button.ClickEvent clickEvent) {
                if (equipmentSelect) {
                    ((List<Equipment>) itemSave).remove(equipmentSelect)
                    updateTable()
                }
            }
        })
        tabWindowBase.oneToManyAction.btnNew().setVisible(false)
        tabWindowBase.oneToManyAction.btnSearch().addClickListener(new Button.ClickListener() {
            @Override
            void buttonClick(Button.ClickEvent event) {
                EquipmentPicker equipmentPicker = new EquipmentPicker(new PickElement<Equipment>() {
                    @Override
                    void onResult(List<Equipment> elements) {
                        ((List<Equipment>) itemSave).addAll(elements)
                        itemSave = ((List<Equipment>) itemSave).toSet().toList()
                        updateTable()
                    }
                }, true)
                getUI().addWindow(equipmentPicker)
            }
        })
    }

    @Override
    protected void restore() {
    }

    @Override
    protected void visualize() {
        tabWindowBase.viewMenuAction()
    }

    @Override
    protected void security() {
    }

    @Override
    protected boolean getElementsValid() {
        return true
    }

    @Override
    protected void collectData() {
    }

    @Override
    protected List<Equipment> getValid() {
        if (getElementsValid()) {
            return itemSave
        }
        return null
    }

    @Override
    void startFocus() {
        tabWindowBase.oneToManyAction.tfACSearch().focus()
    }
}
