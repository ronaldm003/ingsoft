package edu.utesa.ronald.ingsoft.ui.location

import edu.utesa.ronald.ingsoft.domains.location.Clinic
import edu.utesa.ronald.ingsoft.models.containers.location.ClinicContainer
import edu.utesa.ronald.ingsoft.services.location.ClinicService
import edu.utesa.ronald.ingsoft.ui.components.ATabBase
import edu.utesa.ronald.ingsoft.utils.Constants
import edu.utesa.ronald.ingsoft.utils.container.LazyQuery
import groovy.transform.CompileStatic
import org.springframework.beans.factory.annotation.Autowired

/**
 * Aqui se lista todos los tipos de usuario
 *  Created by ronald on 3/07/17.
 */
@CompileStatic
class ClinicTab extends ATabBase<ClinicForm, Clinic> {

    @Autowired
    private ClinicService clinicService

    ClinicTab() {
        Constants.instance.autoWiredClass(this)
    }

    @Override
    protected void confLanguage() {

    }

    @Override
    protected void buildLayout() {
        manageGrid(new ClinicContainer(new LazyQuery<Clinic>() {
            @Override
            int getSize() {
                return clinicService.count(showDelete())
            }

            @Override
            List<Clinic> getItemsIds(int startIndex, int numberOfIds) {
                return clinicService.list(showDelete(), startIndex, numberOfIds, {
                    it.location?.enabled; it.inventory?.enabled
                })
            }

            @Override
            int getFilteredSize() {
                return 0
            }

            @Override
            List<Clinic> getFilteredItemsIds(int startIndex, int numberOfIds) {
                return null
            }
        }))
    }

    @Override
    void refreshItem() {
    }

    @Override
    ClinicForm onNew() {
        return new ClinicForm()
    }

    @Override
    ClinicForm onEdit() {

        return new ClinicForm(itemSelect, true)
    }

    @Override
    ClinicForm onView() {
        return new ClinicForm(itemSelect, false)
    }

    @Override
    Class topClass() {
        return null
    }

    @Override
    boolean deleteDomain() {
        return clinicService.delete(itemSelect, loginManager)
    }

    @Override
    protected void security() {

    }
}
