package edu.utesa.ronald.ingsoft.ui.location

import com.vaadin.server.Sizeable
import edu.utesa.ronald.ingsoft.domains.location.Location
import edu.utesa.ronald.ingsoft.services.location.LocationService
import edu.utesa.ronald.ingsoft.ui.components.WindowForm
import edu.utesa.ronald.ingsoft.utils.Constants
import edu.utesa.ronald.ingsoft.utils.Languages
import edu.utesa.ronald.ingsoft.utils.WindowsCaption
import groovy.transform.CompileStatic
import org.springframework.beans.factory.annotation.Autowired

/**
 * Aqui se realizan todos los casos de los botones de accion.
 *  Created by ronald on 3/07/17.
 */
@CompileStatic
class LocationForm extends WindowForm<Location> {

    @Autowired
    private LocationService locationService

    private LocationFormLocation formLocation

    LocationForm() {
        this(null, null)
    }

    LocationForm(Location location, Boolean edit) {
        super(location, edit)
        confLayout(WindowsCaption.LOCATION_CAPTION)
        setWidth(600, Sizeable.Unit.PIXELS)
    }

    @Override
    protected void prepare() {
        super.prepare()
        Constants.instance.autoWiredClass(this)
    }

    @Override
    protected void onCreate() {
        formLocation = new LocationFormLocation()
    }

    @Override
    protected void onEditView() {
        itemSave = locationService.refresh(itemSave.id)
        formLocation = new LocationFormLocation(itemSave, edit)
    }

    @Override
    Location onSave() {
        itemSave = formLocation.getValid()
        return locationService.create(itemSave, loginManager)
    }

    @Override
    protected void onViewTabs() {

    }

    @Override
    protected void buildView() {

    }

    @Override
    protected boolean getValidForm() {
        if (!formLocation.getValid()) {
            return tabSheetManager.selectTab(0)
        }
        return true
    }

    @Override
    protected void buildLayout() {
        setWidth(800, Sizeable.Unit.PIXELS)
        tabSheetManager.addTabPanel(Languages.instance.getText(WindowsCaption.LOCATION_CAPTION + FORM + "location"), formLocation)

    }

    @Override
    protected void security() {

    }
}
