package edu.utesa.ronald.ingsoft.ui.location

import com.vaadin.event.FieldEvents
import edu.utesa.ronald.ingsoft.components.abstracts.AWindowTab
import edu.utesa.ronald.ingsoft.domains.location.Location
import edu.utesa.ronald.ingsoft.services.location.LocationService
import edu.utesa.ronald.ingsoft.utils.Constants
import edu.utesa.ronald.ingsoft.utils.Languages
import groovy.transform.CompileStatic
import org.springframework.beans.factory.annotation.Autowired

/**
 *  Created by ronald on 3/07/17.
 */
@CompileStatic
class LocationFormLocation extends AWindowTab<Location> {

    @Autowired
    private LocationService locationService

    private LocationFormLocationDesign locationFormLocationDesign = new LocationFormLocationDesign()

    LocationFormLocation() {
        this(new Location(), null)
    }

    LocationFormLocation(Location location, Boolean edit) {
        super(location, edit)
        confLayout(locationFormLocationDesign)
    }

    @Override
    protected void buildLayout() {
        getNextCode()
        locationFormLocationDesign.tfCode.setReadOnly(true)
        locationFormLocationDesign.tfDescription.setRequired(true)
        locationFormLocationDesign.tfDescription.setRequiredError(Languages.instance.getText("validations.empty.field"))
        locationFormLocationDesign.tfDescription.setValidationVisible(true)
        locationFormLocationDesign.tfDescription.setNullRepresentation("")
        locationFormLocationDesign.tfDescription.addTextChangeListener(new FieldEvents.TextChangeListener() {
            @Override
            void textChange(FieldEvents.TextChangeEvent event) {
                locationFormLocationDesign.tfDescription.setValue(event.getText())
                locationFormLocationDesign.tfDescription.setCursorPosition(event.getCursorPosition())
                locationFormLocationDesign.tfDescription.validate()
            }
        })
    }

    private void getNextCode() {
        Long code = locationService.findMaxCode()
        if (code) {
            locationFormLocationDesign.tfCode.setValue((code + 1).toString())
        } else {
            locationFormLocationDesign.tfCode.setValue("1")
        }
    }

    @Override
    protected void restore() {
        locationFormLocationDesign.tfCode.setReadOnly(false)
        locationFormLocationDesign.tfCode.setValue(itemSave.code.toString())
        locationFormLocationDesign.tfCode.setReadOnly(true)
        locationFormLocationDesign.tfDescription.setValue(itemSave.description ?: "")
        locationFormLocationDesign.tfLongitude.setValue(itemSave.longitude?.toString() ?: "")
        locationFormLocationDesign.tfLatitude.setValue(itemSave.latitude?.toString() ?: "")
    }

    @Override
    protected void visualize() {
        locationFormLocationDesign.tfCode.setReadOnly(true)
        locationFormLocationDesign.tfDescription.setReadOnly(true)
        locationFormLocationDesign.tfLongitude.setReadOnly(true)
        locationFormLocationDesign.tfLatitude.setReadOnly(true)
    }

    @Override
    protected boolean getElementsValid() {
        if (!locationFormLocationDesign.tfCode.isValid()) {
            locationFormLocationDesign.tfCode.focus()
            return false
        } else if (!locationFormLocationDesign.tfDescription.isValid()) {
            locationFormLocationDesign.tfDescription.focus()
            return false
        } else if (!locationFormLocationDesign.tfLongitude.isValid()) {
            locationFormLocationDesign.tfLongitude.focus()
            return false
        } else if (!locationFormLocationDesign.tfLatitude.isValid()) {
            locationFormLocationDesign.tfLatitude.focus()
            return false
        }
        return true
    }

    @Override
    protected void collectData() {
        itemSave.code = locationFormLocationDesign.tfCode.getValue().toLong()
        itemSave.description = locationFormLocationDesign.tfDescription.getValue()
        itemSave.longitude = locationFormLocationDesign.tfLongitude.getValue().toLong()
        itemSave.latitude = locationFormLocationDesign.tfLatitude.getValue().toLong()
    }

    @Override
    protected void confLanguage() {
        Constants.instance.autoWiredClass(this)
        locationFormLocationDesign.tfCode.setCaption(Languages.instance.getText("window.id"))
        locationFormLocationDesign.tfDescription.setCaption(Languages.instance.getText("window.description"))
        locationFormLocationDesign.tfLongitude.setCaption(Languages.instance.getText("window.longitude"))
        locationFormLocationDesign.tfLatitude.setCaption(Languages.instance.getText("window.latitude"))
    }

    @Override
    protected Location getValid() {
        if (getElementsValid()) {
            collectData()
            return itemSave
        }
        return null
    }

    @Override
    void startFocus() {
        locationFormLocationDesign.tfDescription.focus()
    }
}
