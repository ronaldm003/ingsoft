package edu.utesa.ronald.ingsoft.ui.location

import edu.utesa.ronald.ingsoft.domains.location.Location
import edu.utesa.ronald.ingsoft.models.containers.location.LocationContainer
import edu.utesa.ronald.ingsoft.services.location.LocationService
import edu.utesa.ronald.ingsoft.ui.components.ATabBase
import edu.utesa.ronald.ingsoft.utils.Constants
import edu.utesa.ronald.ingsoft.utils.container.LazyQuery
import groovy.transform.CompileStatic
import org.springframework.beans.factory.annotation.Autowired

/**
 * Aqui se lista todos los tipos de usuario
 *  Created by ronald on 3/07/17.
 */
@CompileStatic
class LocationTab extends ATabBase<LocationForm, Location> {

    @Autowired
    private LocationService locationService

    LocationTab() {
        Constants.instance.autoWiredClass(this)
    }

    @Override
    protected void confLanguage() {

    }

    @Override
    protected void buildLayout() {
        manageGrid(new LocationContainer(new LazyQuery<Location>() {
            @Override
            int getSize() {
                return locationService.count(showDelete())
            }

            @Override
            List<Location> getItemsIds(int startIndex, int numberOfIds) {
                return locationService.list(showDelete(), startIndex, numberOfIds)
            }

            @Override
            int getFilteredSize() {
                return 0
            }

            @Override
            List<Location> getFilteredItemsIds(int startIndex, int numberOfIds) {
                return null
            }
        }))
    }

    @Override
    void refreshItem() {

    }

    @Override
    LocationForm onNew() {
        return new LocationForm()
    }

    @Override
    LocationForm onEdit() {
        return new LocationForm(itemSelect, true)
    }

    @Override
    LocationForm onView() {
        return new LocationForm(itemSelect, false)
    }

    @Override
    Class topClass() {
        return null
    }

    @Override
    boolean deleteDomain() {
        return locationService.delete(itemSelect, loginManager)
    }

    @Override
    protected void security() {

    }
}
