package edu.utesa.ronald.ingsoft.ui.security

import com.vaadin.server.Sizeable
import edu.utesa.ronald.ingsoft.domains.security.Param
import edu.utesa.ronald.ingsoft.services.security.ParamService
import edu.utesa.ronald.ingsoft.ui.components.WindowForm
import edu.utesa.ronald.ingsoft.utils.Constants
import edu.utesa.ronald.ingsoft.utils.Languages
import edu.utesa.ronald.ingsoft.utils.WindowsCaption
import groovy.transform.CompileStatic
import org.springframework.beans.factory.annotation.Autowired

/**
 *  Created by Ing. Aluis Marte on 2/5/2015.
 */
@CompileStatic
class ParamForm extends WindowForm<Param> {
    @Autowired
    private ParamService paramService

    private ParamFormTabParam formTabParam

    ParamForm() {
        this(null, null)
    }

    ParamForm(Param param, Boolean edit) {
        super(param, edit)
        Constants.instance.autoWiredClass(this)
        confLayout(WindowsCaption.PARAMS_CAPTION)
    }

    @Override
    protected boolean getValidForm() {
        if (!formTabParam.getValid()) {
            return tabSheetManager.selectTab(0)
        }
        return true
    }

    @Override
    protected void onCreate() {
    }

    @Override
    protected void onEditView() {
        formTabParam = new ParamFormTabParam(itemSave, edit)
    }

    @Override
    protected void buildView() {
    }

    @Override
    Param onSave() {
        itemSave = formTabParam.getValid()
        return paramService.create(itemSave, loginManager)
    }

    @Override
    protected void onViewTabs() {
    }

    @Override
    protected void buildLayout() {
        setWidth(800, Sizeable.Unit.PIXELS)
        tabSheetManager.addTabPanel(Languages.instance.getText(WindowsCaption.PARAMS_CAPTION + FORM + "param"), formTabParam)
    }

    @Override
    protected void security() {
    }
}