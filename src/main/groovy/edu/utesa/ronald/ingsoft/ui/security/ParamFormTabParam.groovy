package edu.utesa.ronald.ingsoft.ui.security

import com.vaadin.data.Property
import edu.utesa.ronald.ingsoft.components.abstracts.AWindowTab
import edu.utesa.ronald.ingsoft.domains.security.Param
import edu.utesa.ronald.ingsoft.models.enums.params.BooleanValue
import edu.utesa.ronald.ingsoft.models.params.ParamValueType
import edu.utesa.ronald.ingsoft.utils.CBFiller
import edu.utesa.ronald.ingsoft.utils.Languages
import groovy.transform.CompileStatic

/**
 *  Created by aluis on 4/7/2015.
 */
@SuppressWarnings("ChangeToOperator")
@CompileStatic
class ParamFormTabParam extends AWindowTab<Param> {

    private ParamFormParamDesign paramFormParamDesign = new ParamFormParamDesign()

    ParamFormTabParam(Param param, Boolean edit) {
        super(param, edit)
        confLayout(paramFormParamDesign)
        if (!loginManager.getAdmin() && param.getChangeRoot()) {
            this.edit = false
        }
    }

    @Override
    protected void confLanguage() {
        paramFormParamDesign.tfCode.setCaption(Languages.instance.getText("window.code"))
        paramFormParamDesign.cbParamValueType.setCaption(Languages.instance.getText("window.paramValueType"))
        paramFormParamDesign.tfName.setCaption(Languages.instance.getText("window.name"))
        paramFormParamDesign.tfValue.setCaption(Languages.instance.getText("window.value"))
        paramFormParamDesign.cbValue.setCaption(Languages.instance.getText("window.value"))
        paramFormParamDesign.chkValue.setCaption(Languages.instance.getText("window.value"))
        paramFormParamDesign.chkRoot.setCaption(Languages.instance.getText("window.root"))
        paramFormParamDesign.taDescription.setCaption(Languages.instance.getText("window.description"))
    }

    @Override
    protected void buildLayout() {
        //TODO: Revisar la ui por errores de visualizacion
        CBFiller.fillParamValueType(paramFormParamDesign.cbParamValueType)
        paramFormParamDesign.cbParamValueType.addValueChangeListener(new Property.ValueChangeListener() {
            @Override
            void valueChange(Property.ValueChangeEvent event) {
                ParamValueType select = (ParamValueType) paramFormParamDesign.cbParamValueType.getValue()
                switch (select) {
                    case ParamValueType.BOOLEAN:
                        paramFormParamDesign.tfValue.setVisible(false)
                        paramFormParamDesign.cbValue.setVisible(false)
                        paramFormParamDesign.chkValue.setVisible(true)
                        break
                    case ParamValueType.ENUM_PAYMENT:
                    case ParamValueType.ENUM_TIME_ZONE:
                        paramFormParamDesign.tfValue.setVisible(false)
                        paramFormParamDesign.cbValue.setVisible(true)
                        paramFormParamDesign.chkValue.setVisible(false)
                        break
                    case ParamValueType.NUMERIC:
                    case ParamValueType.TEXT:
                    case ParamValueType.IMG:
                        paramFormParamDesign.tfValue.setVisible(true)
                        paramFormParamDesign.cbValue.setVisible(false)
                        paramFormParamDesign.chkValue.setVisible(false)
                        break
                }
                switch (select) {
                    case ParamValueType.ENUM_TIME_ZONE:
                        CBFiller.fillTimeZone(paramFormParamDesign.cbValue)
                        break
                }
            }
        })
        if (!itemSave.paramValueType) {
            paramFormParamDesign.cbParamValueType.setValue(ParamValueType.BOOLEAN)
        } else {
            paramFormParamDesign.cbParamValueType.setValue(itemSave.paramValueType)
        }
    }

    @Override
    protected void restore() {
        paramFormParamDesign.tfCode.setValue(itemSave.getCode().toString() ?: "-1")
        paramFormParamDesign.tfCode.setReadOnly(true)
        paramFormParamDesign.tfName.setValue(itemSave.getName() ?: "")
        paramFormParamDesign.taDescription.setValue(itemSave.getDescription() ?: "")
        paramFormParamDesign.chkRoot.setValue(itemSave.getChangeRoot())
        paramFormParamDesign.cbParamValueType.setValue(itemSave.getParamValueType() ?: "")
        paramFormParamDesign.cbParamValueType.setReadOnly(true)
        if (paramFormParamDesign.tfValue.isVisible()) {
            paramFormParamDesign.tfValue.setValue(itemSave.value ?: "")
        } else if (paramFormParamDesign.chkValue.isVisible()) {
            paramFormParamDesign.chkValue.setValue(BooleanValue.TRUE.toString().equals(itemSave.value))
        } else {
            switch (itemSave.paramValueType) {
                case ParamValueType.ENUM_TIME_ZONE:
                    paramFormParamDesign.cbValue.setValue(itemSave.value)
                    break
            }
        }
        paramFormParamDesign.tfCode.setReadOnly(true)
        paramFormParamDesign.tfName.setReadOnly(true)
    }

    @Override
    protected void visualize() {
        paramFormParamDesign.tfCode.setReadOnly(true)
        paramFormParamDesign.tfName.setReadOnly(true)
        paramFormParamDesign.taDescription.setReadOnly(true)
        paramFormParamDesign.cbValue.setReadOnly(true)
        paramFormParamDesign.tfValue.setReadOnly(true)
        paramFormParamDesign.chkValue.setReadOnly(true)
        paramFormParamDesign.cbParamValueType.setReadOnly(true)
        paramFormParamDesign.chkRoot.setReadOnly(true)
    }

    @Override
    protected void security() {
    }

    @Override
    protected boolean getElementsValid() {
        if (!paramFormParamDesign.tfCode.isValid()) {
            paramFormParamDesign.tfCode.focus()
            return false
        } else if (!paramFormParamDesign.tfName.isValid()) {
            paramFormParamDesign.tfName.focus()
            return false
        } else if (!paramFormParamDesign.taDescription.isValid()) {
            paramFormParamDesign.taDescription.focus()
            return false
        } else if (paramFormParamDesign.tfValue.isVisible()) {
            if (!paramFormParamDesign.tfValue.isValid()) {
                paramFormParamDesign.tfValue.focus()
                return false
            }
        } else if (paramFormParamDesign.cbValue.isVisible()) {
            if (!paramFormParamDesign.cbValue.isValid()) {
                paramFormParamDesign.cbValue.focus()
                return false
            }
        } else if (paramFormParamDesign.chkRoot.isEnabled() && !paramFormParamDesign.chkRoot.isValid()) {
            paramFormParamDesign.chkRoot.focus()
            return false
        }
        return true
    }

    @Override
    protected void collectData() {
        itemSave.code = paramFormParamDesign.tfCode.getValue().toInteger()
        itemSave.name = paramFormParamDesign.tfName.getValue()
        if (paramFormParamDesign.tfValue.isVisible()) {
            itemSave.value = paramFormParamDesign.tfValue.getValue()
        } else if (paramFormParamDesign.chkValue.isVisible()) {
            itemSave.value = paramFormParamDesign.chkValue.getValue() ? BooleanValue.TRUE.toString() : BooleanValue.FALSE.toString()
        } else {
            itemSave.value = paramFormParamDesign.cbValue.getValue()
        }
        itemSave.changeRoot = paramFormParamDesign.chkRoot.getValue()
        itemSave.description = paramFormParamDesign.taDescription.getValue()
    }

    @Override
    protected Param getValid() {
        if (getElementsValid()) {
            collectData()
            return itemSave
        }
        return null
    }

    @Override
    void startFocus() {
        paramFormParamDesign.tfCode.focus()
    }
}
