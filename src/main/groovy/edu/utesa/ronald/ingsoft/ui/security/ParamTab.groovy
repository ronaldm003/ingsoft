package edu.utesa.ronald.ingsoft.ui.security

import edu.utesa.ronald.ingsoft.domains.security.Param
import edu.utesa.ronald.ingsoft.models.containers.security.ParamContainer
import edu.utesa.ronald.ingsoft.services.security.ParamService
import edu.utesa.ronald.ingsoft.ui.components.ATabBase
import edu.utesa.ronald.ingsoft.utils.Constants
import edu.utesa.ronald.ingsoft.utils.container.LazyQuery
import edu.utesa.ronald.ingsoft.utils.permissions.PermissionType
import groovy.transform.CompileStatic
import org.springframework.beans.factory.annotation.Autowired

/**
 *  Created by Ing. Aluis Marte on 2/5/2015.
 */
@CompileStatic
class ParamTab extends ATabBase<ParamForm, Param> {

    @Autowired
    private ParamService paramService

    ParamTab() {
    }

    @Override
    protected void confLanguage() {
        Constants.instance.autoWiredClass(this)
    }

    @Override
    protected void buildLayout() {
        tabBase.formTabAction.btnNew().setVisible(false)
        tabBase.formTabAction.btnDelete().setVisible(false)

        manageGrid(new ParamContainer(new LazyQuery() {
            @Override
            int getSize() {
                return paramService.count(showDelete())
            }

            @Override
            List getItemsIds(int startIndex, int numberOfIds) {
                return paramService.list(showDelete(), startIndex, numberOfIds)
            }

            @Override
            int getFilteredSize() {
                return 0
            }

            @Override
            List getFilteredItemsIds(int startIndex, int numberOfIds) {
                return null
            }
        }))
    }

    @Override
    void refreshItem() {
        itemSelect = paramService.refresh(itemSelect.id)
    }

    @Override
    ParamForm onNew() {
        return null
    }

    @Override
    ParamForm onEdit() {
        return new ParamForm(itemSelect, true)
    }

    @Override
    ParamForm onView() {
        return new ParamForm(itemSelect, false)
    }

    @Override
    Class topClass() {
        return this.getClass()
    }

    @Override
    boolean deleteDomain() {
        return paramService.delete(itemSelect, loginManager)
    }

    @Override
    protected void security() {
//        applyPermissionBtnNew(loginManager.havePermission(PermissionType.PARAMS_NEW))
//        applyPermissionBtnEdit(loginManager.havePermission(PermissionType.PARAMS_EDIT))
//        applyPermissionBtnView(loginManager.havePermission(PermissionType.PARAMS_VIEW))
//        applyPermissionBtnDelete(loginManager.havePermission(PermissionType.PARAMS_DELETE))
//        applyPermissionchkShowDelete(loginManager.havePermission(PermissionType.PARAMS_SHOW_DELETE))
    }

}