package edu.utesa.ronald.ingsoft.ui.security

import com.vaadin.server.Sizeable
import edu.utesa.ronald.ingsoft.domains.security.Permission
import edu.utesa.ronald.ingsoft.services.security.PermissionService
import edu.utesa.ronald.ingsoft.ui.components.WindowForm
import edu.utesa.ronald.ingsoft.utils.Constants
import edu.utesa.ronald.ingsoft.utils.Languages
import edu.utesa.ronald.ingsoft.utils.WindowsCaption
import groovy.transform.CompileStatic
import org.springframework.beans.factory.annotation.Autowired

/**
 *  Created by aluis on 4/8/2015.
 */
@CompileStatic
class PermissionForm extends WindowForm<Permission> {

    @Autowired
    private PermissionService permissionService

    private PermissionFormTabPermission formTabPermission

    PermissionForm(Permission permission, Boolean edit) {
        super(permission, edit)
        Constants.instance.autoWiredClass(this)
        confLayout(WindowsCaption.PERMISSIONS_CAPTION)
    }

    @Override
    protected boolean getValidForm() {
        if (!formTabPermission.getValid()) {
            tabSheetManager.selectTab(0)
            return false
        }
        return true
    }

    @Override
    protected void onCreate() {
    }

    @Override
    protected void onEditView() {
        formTabPermission = new PermissionFormTabPermission(itemSave, edit)
    }

    @Override
    protected void buildView() {
    }

    @Override
    Permission onSave() {
        itemSave = formTabPermission.getValid()
        return permissionService.create(itemSave, loginManager)
    }

    @Override
    protected void onViewTabs() {
    }

    @Override
    protected void buildLayout() {
        setWidth(500, Sizeable.Unit.PIXELS)
        tabSheetManager.addTabPanel(Languages.instance.getText(WindowsCaption.PERMISSIONS_CAPTION + FORM + "permission"), formTabPermission)
    }

    @Override
    protected void security() {
    }
}