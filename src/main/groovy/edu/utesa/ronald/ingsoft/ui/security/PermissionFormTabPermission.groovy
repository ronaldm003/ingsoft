package edu.utesa.ronald.ingsoft.ui.security

import edu.utesa.ronald.ingsoft.components.abstracts.AWindowTab
import edu.utesa.ronald.ingsoft.domains.security.Permission
import edu.utesa.ronald.ingsoft.utils.Languages
import groovy.transform.CompileStatic

/**
 *  Created by aluis on 4/8/2015.
 */
@CompileStatic
class PermissionFormTabPermission extends AWindowTab<Permission> {

    private PermissionFormPermissionDesign permissionFormPermissionDesign = new PermissionFormPermissionDesign()

    PermissionFormTabPermission(Permission permission, Boolean edit) {
        super(permission, edit)
        confLayout(permissionFormPermissionDesign)
    }

    @Override
    protected void confLanguage() {
        permissionFormPermissionDesign.tfCode.setCaption(Languages.instance.getText("window.code"))
        permissionFormPermissionDesign.tfName.setCaption(Languages.instance.getText("window.name"))
        permissionFormPermissionDesign.taDescription.setCaption(Languages.instance.getText("window.description"))
    }

    @Override
    protected void restore() {
        permissionFormPermissionDesign.tfCode.setValue(itemSave.getCode().toString() ?: "-1")
        permissionFormPermissionDesign.tfName.setValue(itemSave.getName() ?: "")
        permissionFormPermissionDesign.taDescription.setValue(itemSave.getDescription() ?: "")
        permissionFormPermissionDesign.tfCode.setReadOnly(true)
    }

    @Override
    protected void visualize() {
        permissionFormPermissionDesign.tfCode.setReadOnly(true)
        permissionFormPermissionDesign.tfName.setReadOnly(true)
        permissionFormPermissionDesign.taDescription.setReadOnly(true)
    }

    @Override
    protected void security() {
    }

    @Override
    protected boolean getElementsValid() {
        if (!permissionFormPermissionDesign.tfCode.isValid()) {
            permissionFormPermissionDesign.tfCode.focus()
            return false
        } else if (!permissionFormPermissionDesign.tfName.isValid()) {
            permissionFormPermissionDesign.tfName.focus()
            return false
        } else if (!permissionFormPermissionDesign.taDescription.isValid()) {
            permissionFormPermissionDesign.taDescription.focus()
            return false
        }
        return true
    }

    @Override
    protected void collectData() {
        itemSave.code = permissionFormPermissionDesign.tfCode.getValue().toInteger()
        itemSave.name = permissionFormPermissionDesign.tfName.getValue()
        itemSave.description = permissionFormPermissionDesign.taDescription.getValue()
    }

    @Override
    protected Permission getValid() {
        if (getElementsValid()) {
            collectData()
            return itemSave
        }
        return null
    }

    @Override
    void startFocus() {
        permissionFormPermissionDesign.tfCode.focus()
    }
}
