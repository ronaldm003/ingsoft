package edu.utesa.ronald.ingsoft.ui.security

import com.vaadin.server.Sizeable
import edu.utesa.ronald.ingsoft.domains.security.PermissionGroup
import edu.utesa.ronald.ingsoft.services.security.PermissionGroupService
import edu.utesa.ronald.ingsoft.ui.components.WindowForm
import edu.utesa.ronald.ingsoft.utils.Constants
import edu.utesa.ronald.ingsoft.utils.Languages
import edu.utesa.ronald.ingsoft.utils.WindowsCaption
import groovy.transform.CompileStatic
import org.springframework.beans.factory.annotation.Autowired

/**
 *  Created by Ing. Aluis Marte on 2/5/2015.
 */
@CompileStatic
class PermissionGroupForm extends WindowForm<PermissionGroup> {

    @Autowired
    private PermissionGroupService permissionGroupService

    private PermissionGroupFormTabPermissionGroup formTabPermissionGroup
    private PermissionGroupFormTabPermissions formTabPermission

    PermissionGroupForm() {
        this(null, null)
    }

    PermissionGroupForm(PermissionGroup groupPermission, Boolean edit) {
        super(groupPermission, edit)
        Constants.instance.autoWiredClass(this)
        confLayout(WindowsCaption.PERMISSIONS_GROUP_CAPTION)
    }

    @Override
    protected boolean getValidForm() {
        if (!formTabPermissionGroup.getValid()) {
            tabSheetManager.selectTab(0)
            return false
        } else if (!formTabPermission.getValid()) {
            tabSheetManager.selectTab(1)
            return false
        }
        return true
    }

    @Override
    protected void onCreate() {
        formTabPermissionGroup = new PermissionGroupFormTabPermissionGroup()
        formTabPermission = new PermissionGroupFormTabPermissions()
    }

    @Override
    protected void onEditView() {
        itemSave = permissionGroupService.refresh(itemSave.id, { it.listPermissions.size() })
        formTabPermissionGroup = new PermissionGroupFormTabPermissionGroup(itemSave, edit)
        formTabPermission = new PermissionGroupFormTabPermissions(itemSave.listPermissions.toList(), edit)
    }

    @Override
    protected void buildView() {
    }

    @Override
    PermissionGroup onSave() {
        itemSave = formTabPermissionGroup.getValid()
        itemSave.setListPermissions(formTabPermission.getValid().toSet())
        return permissionGroupService.create(itemSave, loginManager)
    }

    @Override
    protected void onViewTabs() {
    }

    @Override
    protected void buildLayout() {
        setWidth(400, Sizeable.Unit.PIXELS)
        tabSheetManager.addTabPanel(Languages.instance.getText(WindowsCaption.PERMISSIONS_GROUP_CAPTION + FORM + "permission.group"), formTabPermissionGroup)
        tabSheetManager.addTabPanel(Languages.instance.getText(WindowsCaption.PERMISSIONS_GROUP_CAPTION + FORM + "permission"), formTabPermission)
    }

    @Override
    protected void security() {
    }
}