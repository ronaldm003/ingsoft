package edu.utesa.ronald.ingsoft.ui.security

import edu.utesa.ronald.ingsoft.components.abstracts.AWindowTab
import edu.utesa.ronald.ingsoft.domains.security.PermissionGroup
import edu.utesa.ronald.ingsoft.utils.Languages
import groovy.transform.CompileStatic

/**
 *  Created by Ing. Aluis Marte on 2/5/2015.
 */
@CompileStatic
class PermissionGroupFormTabPermissionGroup extends AWindowTab<PermissionGroup> {

    private PermissionGroupFormPermissionGroupDesign formPermissionGroup = new PermissionGroupFormPermissionGroupDesign()

    PermissionGroupFormTabPermissionGroup() {
        this(new PermissionGroup(), null)
    }

    PermissionGroupFormTabPermissionGroup(PermissionGroup groupPermission, Boolean edit) {
        super(groupPermission, edit)
        confLayout(formPermissionGroup)
    }

    @Override
    protected void confLanguage() {
        formPermissionGroup.tfName.setCaption(Languages.instance.getText("window.name"))
        formPermissionGroup.taDescription.setCaption(Languages.instance.getText("window.description"))
    }

    @Override
    protected void restore() {
        formPermissionGroup.tfName.setValue(itemSave.getName() ?: "")
        formPermissionGroup.taDescription.setValue(itemSave.getDescription() ?: "")
    }

    @Override
    protected void visualize() {
        formPermissionGroup.tfName.setReadOnly(true)
        formPermissionGroup.taDescription.setReadOnly(true)
    }

    @Override
    protected boolean getElementsValid() {
        if (!formPermissionGroup.tfName.isValid()) {
            formPermissionGroup.tfName.focus()
            return false
        } else if (!formPermissionGroup.taDescription.isValid()) {
            formPermissionGroup.taDescription.focus()
            return false
        }
        return true
    }

    @Override
    protected void collectData() {
        itemSave.name = formPermissionGroup.tfName.getValue()
        itemSave.description = formPermissionGroup.taDescription.getValue()
    }

    @Override
    protected PermissionGroup getValid() {
        if (getElementsValid()) {
            collectData()
            return itemSave
        }
        return null
    }

    @Override
    protected void security() {
    }

    @Override
    void startFocus() {
        formPermissionGroup.tfName.focus()
    }
}
