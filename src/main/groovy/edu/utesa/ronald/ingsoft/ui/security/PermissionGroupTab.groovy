package edu.utesa.ronald.ingsoft.ui.security

import edu.utesa.ronald.ingsoft.domains.security.PermissionGroup
import edu.utesa.ronald.ingsoft.models.containers.security.PermissionGroupContainer
import edu.utesa.ronald.ingsoft.services.security.PermissionGroupService
import edu.utesa.ronald.ingsoft.ui.components.ATabBase
import edu.utesa.ronald.ingsoft.utils.Constants
import edu.utesa.ronald.ingsoft.utils.container.LazyQuery
import groovy.transform.CompileStatic
import org.springframework.beans.factory.annotation.Autowired

/**
 *  Created by Ing. Aluis Marte on 2/5/2015.
 */
@CompileStatic
class PermissionGroupTab extends ATabBase<PermissionGroupForm, PermissionGroup> {

    @Autowired
    private PermissionGroupService permissionGroupService

    PermissionGroupTab() {
        Constants.instance.autoWiredClass(this)
    }

    @Override
    protected void confLanguage() {
    }

    @Override
    protected void buildLayout() {
        manageGrid(new PermissionGroupContainer(new LazyQuery() {
            @Override
            int getSize() {
                return permissionGroupService.count(showDelete())
            }

            @Override
            List getItemsIds(int startIndex, int numberOfIds) {
                return permissionGroupService.list(showDelete(), startIndex, numberOfIds)
            }

            @Override
            int getFilteredSize() {
                return 0
            }

            @Override
            List getFilteredItemsIds(int startIndex, int numberOfIds) {
                return null
            }
        }))
    }

    @Override
    void refreshItem() {
        itemSelect = permissionGroupService.refresh(itemSelect.id, { it.listPermissions.size() })
    }

    @Override
    PermissionGroupForm onNew() {
        return new PermissionGroupForm()
    }

    @Override
    PermissionGroupForm onEdit() {
        return new PermissionGroupForm(itemSelect, true)
    }

    @Override
    PermissionGroupForm onView() {
        return new PermissionGroupForm(itemSelect, false)
    }

    @Override
    Class topClass() {
        return null
    }

    @Override
    boolean deleteDomain() {
        return permissionGroupService.delete(itemSelect, loginManager)
    }


    @Override
    protected void security() {
//        applyPermissionBtnNew(loginManager.havePermission(PermissionType.PERMISSION_GROUP_NEW))
//        applyPermissionBtnEdit(loginManager.havePermission(PermissionType.PERMISSION_GROUP_EDIT))
//        applyPermissionBtnView(loginManager.havePermission(PermissionType.PERMISSION_GROUP_VIEW))
//        applyPermissionBtnDelete(loginManager.havePermission(PermissionType.PERMISSION_GROUP_DELETE))
//        applyPermissionchkShowDelete(loginManager.havePermission(PermissionType.PERMISSION_GROUP_SHOW_DELETE))
    }

}