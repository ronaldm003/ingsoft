package edu.utesa.ronald.ingsoft.ui.security

import edu.utesa.ronald.ingsoft.domains.security.Permission
import edu.utesa.ronald.ingsoft.models.containers.security.PermissionContainer
import edu.utesa.ronald.ingsoft.services.security.PermissionService
import edu.utesa.ronald.ingsoft.ui.components.ATabBase
import edu.utesa.ronald.ingsoft.utils.Constants
import edu.utesa.ronald.ingsoft.utils.container.LazyQuery
import edu.utesa.ronald.ingsoft.utils.permissions.PermissionType
import groovy.transform.CompileStatic
import org.springframework.beans.factory.annotation.Autowired

/**
 *  Created by aluis on 4/8/2015.
 */
@CompileStatic
class PermissionTab extends ATabBase<PermissionForm, Permission> {

    @Autowired
    private PermissionService permissionService

    PermissionTab() {
        Constants.instance.autoWiredClass(this)
    }

    @Override
    protected void confLanguage() {
    }

    @Override
    protected void buildLayout() {
        tabBase.hideButtons()
        manageGrid(new PermissionContainer(new LazyQuery() {
            @Override
            int getSize() {
                return permissionService.count(true)
            }

            @Override
            List getItemsIds(int startIndex, int numberOfIds) {
                return permissionService.list(startIndex, numberOfIds)
            }

            @Override
            int getFilteredSize() {
                return 0
            }

            @Override
            List getFilteredItemsIds(int startIndex, int numberOfIds) {
                return null
            }
        }))
    }

    @Override
    void refreshItem() {

    }

    @Override
    PermissionForm onNew() {
        return null
    }

    @Override
    PermissionForm onEdit() {
        return null
    }

    @Override
    PermissionForm onView() {
        return new PermissionForm(itemSelect, false)
    }

    @Override
    Class topClass() {
        return this.getClass()
    }

    @Override
    boolean deleteDomain() {
        return permissionService.delete(itemSelect, loginManager)
    }

    @Override
    protected void security() {
//        applyPermissionBtnNew(loginManager.havePermission(PermissionType.PERMISSION_NEW))
//        applyPermissionBtnEdit(loginManager.havePermission(PermissionType.PERMISSION_EDIT))
//        applyPermissionBtnView(loginManager.havePermission(PermissionType.PERMISSION_VIEW))
//        applyPermissionBtnDelete(loginManager.havePermission(PermissionType.PERMISSION_DELETE))
//        applyPermissionchkShowDelete(loginManager.havePermission(PermissionType.PERMISSION_SHOW_DELETE))
    }
}