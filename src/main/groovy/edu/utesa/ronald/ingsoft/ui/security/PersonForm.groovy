package edu.utesa.ronald.ingsoft.ui.security

import com.vaadin.server.Sizeable
import edu.utesa.ronald.ingsoft.domains.security.Person
import edu.utesa.ronald.ingsoft.models.interfaces.OnNew
import edu.utesa.ronald.ingsoft.services.security.PersonService
import edu.utesa.ronald.ingsoft.ui.components.WindowForm
import edu.utesa.ronald.ingsoft.utils.Constants
import edu.utesa.ronald.ingsoft.utils.Languages
import edu.utesa.ronald.ingsoft.utils.WindowsCaption
import groovy.transform.CompileStatic
import org.springframework.beans.factory.annotation.Autowired

/**
 * Aqui se realizan todos los casos de los botones de accion.
 *  Created by ronald on 3/07/17.
 */
@CompileStatic
class PersonForm extends WindowForm<Person> {

    @Autowired
    private PersonService personService

    private PersonFormPerson formPerson

    PersonForm() {
        this(null, null)
    }

    PersonForm(OnNew<Person> onNew) {
        this()
        this.onNew = onNew
    }

    PersonForm(Person user, Boolean edit) {
        super(user, edit)
        confLayout(WindowsCaption.PERSON_CAPTION)
    }

    @Override
    protected void prepare() {
        super.prepare()
        Constants.instance.autoWiredClass(this)
    }

    @Override
    protected void onCreate() {
        formPerson = new PersonFormPerson()
    }

    @Override
    protected void onEditView() {
        itemSave = personService.refresh(itemSave.id)
        formPerson = new PersonFormPerson(itemSave, edit)
    }

    @Override
    Person onSave() {
        itemSave = formPerson.getValid()
        return personService.create(itemSave, loginManager)
    }

    @Override
    protected void onViewTabs() {

    }

    @Override
    protected void buildView() {

    }

    @Override
    protected boolean getValidForm() {
        if (!formPerson.getValid()) {
            return tabSheetManager.selectTab(0)
        }
        return true
    }

    @Override
    protected void buildLayout() {
        setWidth(800, Sizeable.Unit.PIXELS)
        tabSheetManager.addTabPanel(Languages.instance.getText(WindowsCaption.PERSON_CAPTION + FORM + "person"), formPerson)
    }

    @Override
    protected void security() {

    }
}
