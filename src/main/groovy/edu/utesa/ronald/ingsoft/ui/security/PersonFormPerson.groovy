package edu.utesa.ronald.ingsoft.ui.security

import com.vaadin.data.Property
import com.vaadin.event.FieldEvents
import com.vaadin.ui.Button
import edu.utesa.ronald.ingsoft.components.abstracts.AWindowTab
import edu.utesa.ronald.ingsoft.domains.location.Location
import edu.utesa.ronald.ingsoft.domains.security.Person
import edu.utesa.ronald.ingsoft.domains.utils.Occupation
import edu.utesa.ronald.ingsoft.models.enums.Gender
import edu.utesa.ronald.ingsoft.models.enums.Nationality
import edu.utesa.ronald.ingsoft.models.enums.WarrantType
import edu.utesa.ronald.ingsoft.models.interfaces.OnNew
import edu.utesa.ronald.ingsoft.models.interfaces.PickElement
import edu.utesa.ronald.ingsoft.models.suggestions.LocationSuggestions
import edu.utesa.ronald.ingsoft.services.location.LocationService
import edu.utesa.ronald.ingsoft.services.security.PersonService
import edu.utesa.ronald.ingsoft.services.utils.OccupationService
import edu.utesa.ronald.ingsoft.ui.components.forms.OneToOneAction
import edu.utesa.ronald.ingsoft.ui.utils.OccupationForm
import edu.utesa.ronald.ingsoft.ui.utils.pickers.LocationPicker
import edu.utesa.ronald.ingsoft.ui.utils.pickers.OccupationPicker
import edu.utesa.ronald.ingsoft.utils.CBFiller
import edu.utesa.ronald.ingsoft.utils.Constants
import edu.utesa.ronald.ingsoft.utils.Languages
import groovy.transform.CompileStatic
import org.springframework.beans.factory.annotation.Autowired

/**
 * Ventana de creacion de los usuarios.
 *
 *  Created by ronald on 3/07/17.
 */
@CompileStatic
class PersonFormPerson extends AWindowTab<Person> {

    @Autowired
    private PersonService personService
    @Autowired
    private LocationService locationService
    @Autowired
    private OccupationService occupationService

    private PersonFormPersonDesign personFormPersonDesign = new PersonFormPersonDesign()
    private OneToOneAction oneToOneActionLocation = new OneToOneAction(false)
    private OneToOneAction oneToOneActionOccupation = new OneToOneAction(false)

    private Location locationSelect
    private Occupation occupationSelect

    PersonFormPerson() {
        this(new Person(), null)
    }

    PersonFormPerson(Person person, Boolean edit) {
        super(person, edit)
        if (person.location) {
            locationSelect = person.location
        }
        if (person.occupation) {
            occupationSelect = person.occupation
        }
        confLayout(personFormPersonDesign)
    }

    @Override
    protected void buildLayout() {
        getNextCode()
        personFormPersonDesign.tfCode.setReadOnly(true)
        CBFiller.fillGender(personFormPersonDesign.cbGender)
        CBFiller.fillNationality(personFormPersonDesign.cbNationality)
        CBFiller.fillWarrantType(personFormPersonDesign.cbWarrantType)
        initLocation()
        initOccupation()
        fieldValidators()
    }

    private void getNextCode() {
        Long code = personService.findMaxCode()
        if (code) {
            personFormPersonDesign.tfCode.setValue((code + 1).toString())
        } else {
            personFormPersonDesign.tfCode.setValue("1")
        }
    }

    private void initLocation() {
        personFormPersonDesign.pLocation.setContent(oneToOneActionLocation)
        oneToOneActionLocation.tfACSearch().setSuggestionProvider(new LocationSuggestions())
        oneToOneActionLocation.tfACSearch().addValueChangeListener(new Property.ValueChangeListener() {
            @Override
            void valueChange(Property.ValueChangeEvent event) {
                if (oneToOneActionLocation.tfACSearch().getValue() && !oneToOneActionLocation.tfACSearch().getValue().isEmpty()) {
                    try {
                        locationSelect = locationService.byDescription(oneToOneActionLocation.tfACSearch().getValue())
                        selectLocation()
                    } catch (Exception ignored) {
                    }
                } else {
                    locationSelect = null
                }
            }
        })
        oneToOneActionLocation.btnNew().setEnabled(false)
        oneToOneActionLocation.btnNew().setVisible(false)
        oneToOneActionLocation.btnSearch().addClickListener(new Button.ClickListener() {
            @Override
            void buttonClick(Button.ClickEvent event) {
                LocationPicker locationPicker = new LocationPicker(new PickElement<Location>() {
                    @Override
                    void onResult(List<Location> elements) {
                        if (elements.size() > 0) {
                            locationSelect = elements.get(0)
                            selectLocation()
                        }
                    }
                }, false)
                getUI().addWindow(locationPicker)
            }
        })
        selectLocation()
    }

    void selectLocation() {
        if (locationSelect) {
            oneToOneActionLocation.tfACSearch().setReadOnly(false)
            oneToOneActionLocation.select(locationSelect.toString())
            oneToOneActionLocation.tfACSearch().setReadOnly(true)
        }
    }

    private void initOccupation() {
        personFormPersonDesign.pOccupation.setContent(oneToOneActionOccupation)
        oneToOneActionOccupation.tfACSearch().setSuggestionProvider(new LocationSuggestions())
        oneToOneActionOccupation.tfACSearch().addValueChangeListener(new Property.ValueChangeListener() {
            @Override
            void valueChange(Property.ValueChangeEvent event) {
                if (oneToOneActionOccupation.tfACSearch().getValue() && !oneToOneActionOccupation.tfACSearch().getValue().isEmpty()) {
                    try {
                        occupationSelect = occupationService.byDescription(oneToOneActionOccupation.tfACSearch().getValue())
                        selectOccupation()
                    } catch (Exception ignored) {
                    }
                } else {
                    occupationSelect = null
                }
            }
        })
        oneToOneActionOccupation.btnNew().addClickListener(new Button.ClickListener() {
            @Override
            void buttonClick(Button.ClickEvent event) {
                OccupationForm occupationForm = new OccupationForm(new OnNew<Occupation>() {
                    @Override
                    void result(Occupation occupation) {
                        occupationSelect = occupation
                        selectOccupation()
                    }
                })
                getUI().addWindow(occupationForm)
            }
        })
        oneToOneActionOccupation.btnSearch().addClickListener(new Button.ClickListener() {
            @Override
            void buttonClick(Button.ClickEvent event) {
                OccupationPicker locationPicker = new OccupationPicker(new PickElement<Occupation>() {
                    @Override
                    void onResult(List<Occupation> elements) {
                        if (elements.size() > 0) {
                            occupationSelect = elements.get(0)
                            selectOccupation()
                        }
                    }
                }, false)
                getUI().addWindow(locationPicker)
            }
        })
        selectLocation()
    }

    void selectOccupation() {
        if (occupationSelect) {
            oneToOneActionOccupation.tfACSearch().setReadOnly(false)
            oneToOneActionOccupation.select(occupationSelect.toString())
            oneToOneActionOccupation.tfACSearch().setReadOnly(true)
        }
    }

    private void fieldValidators() {
        personFormPersonDesign.tfName.setRequired(true)
        personFormPersonDesign.tfName.setRequiredError(Languages.instance.getText("validations.empty.field"))
        personFormPersonDesign.tfName.setValidationVisible(true)
        personFormPersonDesign.tfName.setNullRepresentation("")
        personFormPersonDesign.tfName.addTextChangeListener(new FieldEvents.TextChangeListener() {
            @Override
            void textChange(FieldEvents.TextChangeEvent event) {
                personFormPersonDesign.tfName.setValue(event.getText())
                personFormPersonDesign.tfName.setCursorPosition(event.getCursorPosition())
                personFormPersonDesign.tfName.validate()
            }
        })
    }

    @Override
    protected void restore() {
        personFormPersonDesign.tfCode.setReadOnly(false)
        personFormPersonDesign.tfCode.setValue(itemSave.code.toString())
        personFormPersonDesign.tfCode.setReadOnly(true)
        personFormPersonDesign.tfName.setValue(itemSave.name ?: "")
        personFormPersonDesign.tfLastName.setValue(itemSave.name ?: "")
        personFormPersonDesign.tfCellPhone.setValue(itemSave.cellphone ?: "")
        personFormPersonDesign.tfEmail.setValue(itemSave.email ?: "")
        personFormPersonDesign.cbGender.setValue(itemSave.gender)
        personFormPersonDesign.cbNationality.setValue(itemSave.nationality)
        personFormPersonDesign.cbWarrantType.setValue(itemSave.warrantType)
        personFormPersonDesign.tfWarrant.setValue(itemSave.warrant ?: "")
        personFormPersonDesign.cbGender.setValue(itemSave.gender)
        if (locationSelect) {
            oneToOneActionLocation.select(locationSelect.toString() ?: "")
        }
        if (occupationSelect) {
            oneToOneActionOccupation.select(occupationSelect.toString() ?: "")
        }
        personFormPersonDesign.dfBirthday.setValue(itemSave.birthDate ?: new Date())
        personFormPersonDesign.dfIngressDate.setValue(itemSave.ingressDate ?: new Date())
    }

    @Override
    protected void visualize() {
        personFormPersonDesign.tfCode.setReadOnly(true)
        personFormPersonDesign.tfName.setReadOnly(true)
        personFormPersonDesign.tfLastName.setReadOnly(true)
        personFormPersonDesign.tfCellPhone.setReadOnly(true)
        personFormPersonDesign.tfPhone.setReadOnly(true)
        personFormPersonDesign.tfEmail.setReadOnly(true)
        personFormPersonDesign.tfWarrant.setReadOnly(true)
        oneToOneActionLocation.visualize()
        oneToOneActionOccupation.visualize()
        personFormPersonDesign.cbGender.setReadOnly(true)
        personFormPersonDesign.cbNationality.setReadOnly(true)
        personFormPersonDesign.cbWarrantType.setReadOnly(true)
        personFormPersonDesign.dfBirthday.setReadOnly(true)
        personFormPersonDesign.dfIngressDate.setReadOnly(true)
    }

    @Override
    protected boolean getElementsValid() {
        if (!personFormPersonDesign.tfEmail.isValid()) {
            personFormPersonDesign.tfEmail.focus()
            return false
        } else if (!personFormPersonDesign.tfName.isValid()) {
            personFormPersonDesign.tfName.focus()
            return false
        } else if (!personFormPersonDesign.tfCellPhone.isValid()) {
            personFormPersonDesign.tfCellPhone.focus()
            return false
        } else if (!personFormPersonDesign.cbGender.isValid()) {
            personFormPersonDesign.cbGender.focus()
            return false
        } else if (!oneToOneActionLocation.isValid()) {
            oneToOneActionLocation.focus()
            return false
        } else if (!personFormPersonDesign.tfLastName.isValid()) {
            personFormPersonDesign.tfLastName.focus()
            return false
        } else if (!personFormPersonDesign.tfWarrant.isValid()) {
            personFormPersonDesign.tfWarrant.focus()
            return false
        } else if (!personFormPersonDesign.dfBirthday.isValid()) {
            personFormPersonDesign.dfBirthday.focus()
            return false
        } else if (!personFormPersonDesign.dfIngressDate.isValid()) {
            personFormPersonDesign.dfIngressDate.focus()
            return false
        }
        return true
    }

    @Override
    protected void collectData() {
        itemSave.code = personFormPersonDesign.tfCode.getValue() as Long
        itemSave.name = personFormPersonDesign.tfName.getValue()
        itemSave.lastName = personFormPersonDesign.tfLastName.getValue()
        itemSave.phone = personFormPersonDesign.tfPhone.getValue()
        itemSave.cellphone = personFormPersonDesign.tfCellPhone.getValue()
        itemSave.email = personFormPersonDesign.tfEmail.getValue()
        itemSave.gender = personFormPersonDesign.cbGender.getValue() as Gender
        itemSave.nationality = personFormPersonDesign.cbNationality.getValue() as Nationality
        if (occupationSelect) {
            itemSave.occupation = occupationSelect
        }
        itemSave.warrantType = personFormPersonDesign.cbWarrantType.getValue() as WarrantType
        itemSave.warrant = personFormPersonDesign.tfWarrant.getValue()
        itemSave.birthDate = personFormPersonDesign.dfBirthday.getValue()
        itemSave.ingressDate = personFormPersonDesign.dfIngressDate.getValue()
        itemSave.location = locationSelect
    }

    @Override
    protected void confLanguage() {
        Constants.instance.autoWiredClass(this)
        personFormPersonDesign.tfCode.setCaption(Languages.instance.getText("window.id"))
        personFormPersonDesign.tfName.setCaption(Languages.instance.getText("window.name"))
        personFormPersonDesign.tfLastName.setCaption(Languages.instance.getText("window.lastname"))
        personFormPersonDesign.tfCellPhone.setCaption(Languages.instance.getText("window.cellphone"))
        personFormPersonDesign.tfEmail.setCaption(Languages.instance.getText("window.email"))
        personFormPersonDesign.cbGender.setCaption(Languages.instance.getText("window.gender"))
        personFormPersonDesign.cbWarrantType.setCaption(Languages.instance.getText("window.warrant.type"))
        personFormPersonDesign.cbNationality.setCaption(Languages.instance.getText("window.nationality"))
        oneToOneActionLocation.setCaption(Languages.instance.getText("window.location"))
        oneToOneActionOccupation.setCaption(Languages.instance.getText("window.occupation"))
        personFormPersonDesign.tfWarrant.setCaption(Languages.instance.getText("window.warrant"))
        personFormPersonDesign.tfPhone.setCaption(Languages.instance.getText("window.phone"))
        personFormPersonDesign.dfBirthday.setCaption(Languages.instance.getText("window.birthday"))
        personFormPersonDesign.dfIngressDate.setCaption(Languages.instance.getText("window.ingress.date"))
    }

    @Override
    protected Person getValid() {
        if (getElementsValid()) {
            collectData()
            return itemSave
        }
        return null
    }

    @Override
    void startFocus() {

    }
}
