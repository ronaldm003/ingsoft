package edu.utesa.ronald.ingsoft.ui.security

import edu.utesa.ronald.ingsoft.domains.security.Person
import edu.utesa.ronald.ingsoft.models.containers.security.PersonContainer
import edu.utesa.ronald.ingsoft.services.security.PersonService
import edu.utesa.ronald.ingsoft.ui.components.ATabBase
import edu.utesa.ronald.ingsoft.utils.Constants
import edu.utesa.ronald.ingsoft.utils.container.LazyQuery
import groovy.transform.CompileStatic
import org.springframework.beans.factory.annotation.Autowired

/**
 * Aqui se lista todos los tipos de usuario
 *  Created by ronald on 3/07/18.
 */
@CompileStatic
class PersonTab extends ATabBase<PersonForm, Person> {

    @Autowired
    private PersonService personService

    PersonTab() {
        Constants.instance.autoWiredClass(this)
    }

    @Override
    protected void confLanguage() {
    }

    @Override
    protected void buildLayout() {
        //TODO: verificar porque esta vaina no me presenta bien los usuarios cuando soy admun
        manageGrid(new PersonContainer(new LazyQuery<Person>() {
            @Override
            int getSize() {
                if (loginManager.admin) {
                    return personService.count(showDelete())
                } else {
                    return personService.count(loginManager.user.id, showDelete())
                }
            }

            @Override
            List<Person> getItemsIds(int startIndex, int numberOfIds) {
                if (loginManager.admin) {
                    return personService.list(showDelete(), startIndex, numberOfIds)
                } else {
                    return personService.list(loginManager.user.id, showDelete(), startIndex, numberOfIds)
                }

            }

            @Override
            int getFilteredSize() {
                return 0
            }

            @Override
            List<Person> getFilteredItemsIds(int startIndex, int numberOfIds) {
                return null
            }
        }))
    }

    @Override
    void refreshItem() {

    }

    @Override
    PersonForm onNew() {
        return new PersonForm()
    }

    @Override
    PersonForm onEdit() {
        return new PersonForm(itemSelect, true)
    }

    @Override
    PersonForm onView() {
        return new PersonForm(itemSelect, false)
    }

    @Override
    Class topClass() {
        return null
    }

    @Override
    boolean deleteDomain() {
        return personService.delete(itemSelect, loginManager)
    }

    @Override
    protected void security() {

    }
}
