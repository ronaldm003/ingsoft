package edu.utesa.ronald.ingsoft.ui.security

import com.vaadin.server.Sizeable
import edu.utesa.ronald.ingsoft.domains.security.User
import edu.utesa.ronald.ingsoft.services.security.UserService
import edu.utesa.ronald.ingsoft.ui.components.WindowForm
import edu.utesa.ronald.ingsoft.utils.Constants
import edu.utesa.ronald.ingsoft.utils.Languages
import edu.utesa.ronald.ingsoft.utils.WindowsCaption
import groovy.transform.CompileStatic
import org.springframework.beans.factory.annotation.Autowired

/**
 * Aqui se realizan todos los casos de los botones de accion.
 *  Created by ronald on 3/07/17.
 */
@CompileStatic
class UserForm extends WindowForm<User> {

    @Autowired
    private UserService userService

    private UserFormUser formUser
    private UserFormPermission formTabPermission

    UserForm() {
        this(null, null)
    }

    UserForm(User user, Boolean edit) {
        super(user, edit)
        confLayout(WindowsCaption.USERS_CAPTION)
    }

    @Override
    protected void prepare() {
        super.prepare()
        Constants.instance.autoWiredClass(this)
    }

    @Override
    protected void onCreate() {
        formUser = new UserFormUser()
        formTabPermission = new UserFormPermission()
    }

    @Override
    protected void onEditView() {
        itemSave = userService.refresh(itemSave.id)
        formUser = new UserFormUser(itemSave, edit)
        formTabPermission = new UserFormPermission(itemSave.listPermission.toList(), edit)
    }

    @Override
    User onSave() {
        itemSave = formUser.getValid()
        itemSave.listPermission = formTabPermission.getValid().toSet()
        return userService.create(itemSave, loginManager)
    }

    @Override
    protected void onViewTabs() {

    }

    @Override
    protected void buildView() {

    }

    @Override
    protected boolean getValidForm() {
        if (!formUser.getValid()) {
            return tabSheetManager.selectTab(0)
        }
        return true
    }

    @Override
    protected void buildLayout() {
        setWidth(800, Sizeable.Unit.PIXELS)
        tabSheetManager.addTabPanel(Languages.instance.getText(WindowsCaption.USERS_CAPTION + FORM + "user"), formUser)
        tabSheetManager.addTabPanel(Languages.instance.getText(WindowsCaption.USERS_CAPTION + FORM + "permission"), formTabPermission)
    }

    @Override
    protected void security() {

    }
}
