package edu.utesa.ronald.ingsoft.ui.security

import com.vaadin.data.Property
import com.vaadin.event.SelectionEvent
import com.vaadin.ui.Button
import edu.utesa.ronald.ingsoft.components.abstracts.AWindowTab
import edu.utesa.ronald.ingsoft.domains.security.Permission
import edu.utesa.ronald.ingsoft.domains.security.PermissionGroup
import edu.utesa.ronald.ingsoft.models.containers.security.PermissionsUserContainer
import edu.utesa.ronald.ingsoft.models.interfaces.PickElement
import edu.utesa.ronald.ingsoft.models.suggestions.GroupPermissionSuggestions
import edu.utesa.ronald.ingsoft.services.security.PermissionGroupService
import edu.utesa.ronald.ingsoft.ui.components.TabWindowBase
import edu.utesa.ronald.ingsoft.ui.utils.pickers.PermissionPicker
import edu.utesa.ronald.ingsoft.utils.container.LazyQuery
import groovy.transform.CompileStatic
import org.springframework.beans.factory.annotation.Autowired

/**
 *  Created by aluis on 4/8/2015.
 */
@CompileStatic
class UserFormPermission extends AWindowTab<List<Permission>> {

    private TabWindowBase tabWindowBase = new TabWindowBase()

    private Permission permissionSelect
    private PermissionGroup groupPermissionSelect

    @Autowired
    private PermissionGroupService permissionGroupService


    UserFormPermission() {
        this(new ArrayList<Permission>(), null)
    }

    UserFormPermission(List<Permission> itemSave, Boolean edit) {
        super(itemSave, edit)
        confLayout(tabWindowBase)
    }

    @Override
    protected void confLanguage() {
    }

    @Override
    protected void buildLayout() {
        manageGrid(tabWindowBase.grid(), new PermissionsUserContainer(new LazyQuery<Permission>() {
            @Override
            int getSize() {
                return ((List<Permission>) itemSave).size()
            }

            @Override
            List<Permission> getItemsIds(int startIndex, int numberOfIds) {
                return ((List<Permission>) itemSave).subList(startIndex, (startIndex + numberOfIds))
            }

            @Override
            int getFilteredSize() {
                return 0
            }

            @Override
            List<Permission> getFilteredItemsIds(int startIndex, int numberOfIds) {
                return new ArrayList<Permission>()
            }
        }))
        tabWindowBase.grid().addSelectionListener(new SelectionEvent.SelectionListener() {
            @Override
            void select(SelectionEvent event) {
                if (gridManager.getGrid().getSelectedRow()) {
                    permissionSelect = (Permission) gridManager.getGrid().getSelectedRow()
                    tabWindowBase.enableMenuAction()
                } else {
                    permissionSelect = null
                    tabWindowBase.disableMenuAction()
                }
            }
        })
        tabWindowBase.oneToManyAction.tfACSearch().setSuggestionProvider(new GroupPermissionSuggestions())
        tabWindowBase.oneToManyAction.tfACSearch().addValueChangeListener(new Property.ValueChangeListener() {
            @Override
            void valueChange(Property.ValueChangeEvent event) {
                if (tabWindowBase.oneToManyAction.tfACSearch().getValue()) {
                    try {
                        groupPermissionSelect = permissionGroupService.refresh(permissionGroupService.byName(tabWindowBase.oneToManyAction.tfACSearch().getValue()).id, {
                            it.listPermissions.size()
                        })
                    } catch (Exception ignored) {
                    }
                } else {
                    groupPermissionSelect = null
                }
            }
        })
        tabWindowBase.oneToManyAction.btnAdd().addClickListener(new Button.ClickListener() {
            @Override
            void buttonClick(Button.ClickEvent clickEvent) {
                if (groupPermissionSelect) {
                    ((List<Permission>) itemSave).addAll(groupPermissionSelect.listPermissions)
                    itemSave = ((List<Permission>) itemSave).toSet().toList()
                    tabWindowBase.oneToManyAction.tfACSearch().setValue("")
                    updateTable()
                }
            }
        })
        tabWindowBase.oneToManyAction.btnDelete().addClickListener(new Button.ClickListener() {
            @Override
            void buttonClick(Button.ClickEvent clickEvent) {
                if (permissionSelect) {
                    ((List<Permission>) itemSave).remove(permissionSelect)
                    updateTable()
                }
            }
        })
        tabWindowBase.oneToManyAction.btnNew().setVisible(false)
        tabWindowBase.oneToManyAction.btnSearch().addClickListener(new Button.ClickListener() {
            @Override
            void buttonClick(Button.ClickEvent event) {
                PermissionPicker permissionPicker = new PermissionPicker(new PickElement<Permission>() {
                    @Override
                    void onResult(List<Permission> elements) {
                        ((List<Permission>) itemSave).addAll(elements)
                        itemSave = ((List<Permission>) itemSave).toSet().toList()
                        updateTable()
                    }
                }, true)
                getUI().addWindow(permissionPicker)
            }
        })
    }

    @Override
    protected void restore() {
    }

    @Override
    protected void visualize() {
        tabWindowBase.viewMenuAction()
    }

    @Override
    protected void security() {
    }

    @Override
    protected boolean getElementsValid() {
        return true
    }

    @Override
    protected void collectData() {
    }

    @Override
    protected List<Permission> getValid() {
        if (getElementsValid()) {
            return itemSave
        }
        return null
    }

    @Override
    void startFocus() {
        tabWindowBase.oneToManyAction.tfACSearch().focus()
    }
}
