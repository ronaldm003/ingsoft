package edu.utesa.ronald.ingsoft.ui.security

import com.vaadin.data.Property
import com.vaadin.event.FieldEvents
import com.vaadin.ui.Button
import edu.utesa.ronald.ingsoft.components.abstracts.AWindowTab
import edu.utesa.ronald.ingsoft.domains.security.Person
import edu.utesa.ronald.ingsoft.domains.security.User
import edu.utesa.ronald.ingsoft.models.enums.UserType
import edu.utesa.ronald.ingsoft.models.interfaces.OnNew
import edu.utesa.ronald.ingsoft.models.interfaces.PickElement
import edu.utesa.ronald.ingsoft.models.suggestions.PersonSuggestions
import edu.utesa.ronald.ingsoft.services.security.PersonService
import edu.utesa.ronald.ingsoft.services.security.UserService
import edu.utesa.ronald.ingsoft.ui.components.forms.OneToOneAction
import edu.utesa.ronald.ingsoft.ui.utils.pickers.PersonPicker
import edu.utesa.ronald.ingsoft.utils.CBFiller
import edu.utesa.ronald.ingsoft.utils.Constants
import edu.utesa.ronald.ingsoft.utils.Languages
import edu.utesa.ronald.ingsoft.utils.ShowNotification
import edu.utesa.ronald.ingsoft.utils.security.HashUtils
import groovy.transform.CompileStatic
import org.springframework.beans.factory.annotation.Autowired

/**
 * Ventana de creacion de los usuarios.
 *
 *  Created by ronald on 3/07/17.
 */
@CompileStatic
class UserFormUser extends AWindowTab<User> {

    @Autowired
    private UserService userService
    @Autowired
    private PersonService personService

    private UserFormUserDesign userFormUserDesign = new UserFormUserDesign()
    private OneToOneAction oneToOneActionPerson = new OneToOneAction(false)

    private Person personSelect

    UserFormUser() {
        this(new User(), null)
    }

    UserFormUser(User user, Boolean edit) {
        super(user, edit)
        if (user.person) {
            personSelect = user.person
        } else {
            personSelect = new Person()
        }
        confLayout(userFormUserDesign)
    }

    @Override
    protected void buildLayout() {
        if (!loginManager.admin) {
            userFormUserDesign.chkAdmin.setVisible(false)
            userFormUserDesign.chkAdmin.setEnabled(false)
        }

        userFormUserDesign.chkAdmin.setRequired(false)
        CBFiller.fillUserType(userFormUserDesign.cbUserType)
        CBFiller.fillWarrantType(userFormUserDesign.cbWarrantType)
        CBFiller.fillGender(userFormUserDesign.cbGender)
        CBFiller.fillNationality(userFormUserDesign.cbNationality)
        initPerson()
        fieldValidators()
    }

    private void initPerson() {
        userFormUserDesign.pPerson.setContent(oneToOneActionPerson)
        oneToOneActionPerson.tfACSearch().setSuggestionProvider(new PersonSuggestions())
        oneToOneActionPerson.tfACSearch().addValueChangeListener(new Property.ValueChangeListener() {
            @Override
            void valueChange(Property.ValueChangeEvent event) {
                if (oneToOneActionPerson.tfACSearch().getValue() && !oneToOneActionPerson.tfACSearch().getValue().isEmpty()) {
                    try {
                        personSelect = personService.byName(oneToOneActionPerson.tfACSearch().getValue().split("-")[0], {
                            it.location?.enabled; it.occupation?.enabled
                        })
                        setPersonFieldValues(personSelect)
                    } catch (Exception ignored) {
                    }
                } else {
                    personSelect = null
                }
            }
        })
        oneToOneActionPerson.btnNew().addClickListener(new Button.ClickListener() {
            @Override
            void buttonClick(Button.ClickEvent event) {
                PersonForm occupationForm = new PersonForm(new OnNew<Person>() {
                    @Override
                    void result(Person person) {
                        personSelect = person
                        setPersonFieldValues(personSelect)
                    }
                })
                getUI().addWindow(occupationForm)
            }
        })
        oneToOneActionPerson.btnSearch().addClickListener(new Button.ClickListener() {
            @Override
            void buttonClick(Button.ClickEvent event) {
                PersonPicker locationPicker = new PersonPicker(new PickElement<Person>() {
                    @Override
                    void onResult(List<Person> elements) {
                        if (elements.size() > 0) {
                            personSelect = elements.get(0)
                            setPersonFieldValues(personSelect)
                        }
                    }
                }, false)
                getUI().addWindow(locationPicker)
            }
        })
    }

    void selectPerson(Person person) {
        if (person) {
            oneToOneActionPerson.tfACSearch().setReadOnly(false)
            oneToOneActionPerson.select(person ? person.toString() : "")
            oneToOneActionPerson.tfACSearch().setReadOnly(true)
        }
    }

    private void fieldValidators() {
        userFormUserDesign.tfName.setRequired(true)
        userFormUserDesign.tfName.setRequiredError(Languages.instance.getText("validations.empty.field"))
        userFormUserDesign.tfName.setValidationVisible(true)
        userFormUserDesign.tfName.setNullRepresentation("")
        userFormUserDesign.tfName.addTextChangeListener(new FieldEvents.TextChangeListener() {
            @Override
            void textChange(FieldEvents.TextChangeEvent event) {
                userFormUserDesign.tfName.setValue(event.getText())
                userFormUserDesign.tfName.setCursorPosition(event.getCursorPosition())
                userFormUserDesign.tfName.validate()
            }
        })
        userFormUserDesign.tfUsername.setRequired(true)
        userFormUserDesign.tfUsername.setRequiredError(Languages.instance.getText("validations.empty.field"))
        userFormUserDesign.tfUsername.setValidationVisible(true)
        userFormUserDesign.tfUsername.setNullRepresentation("")
        userFormUserDesign.tfUsername.addTextChangeListener(new FieldEvents.TextChangeListener() {
            @Override
            void textChange(FieldEvents.TextChangeEvent event) {
                userFormUserDesign.tfUsername.setValue(event.getText())
                userFormUserDesign.tfUsername.setCursorPosition(event.getCursorPosition())
                userFormUserDesign.tfUsername.validate()
            }
        })

        userFormUserDesign.pfPassword.setRequired(true)
        userFormUserDesign.pfPassword.setRequiredError(Languages.instance.getText("validations.empty.field"))
        userFormUserDesign.pfPassword.setValidationVisible(true)
        userFormUserDesign.pfPassword.setNullRepresentation("")
        userFormUserDesign.pfPassword.addTextChangeListener(new FieldEvents.TextChangeListener() {
            @Override
            void textChange(FieldEvents.TextChangeEvent event) {
                userFormUserDesign.pfPassword.setValue(event.getText())
                userFormUserDesign.pfPassword.setCursorPosition(event.getCursorPosition())
                userFormUserDesign.pfPassword.validate()
            }
        })
        userFormUserDesign.cbUserType.setRequired(true)
        userFormUserDesign.cbUserType.setRequiredError(Languages.instance.getText("validations.empty.field"))
        userFormUserDesign.cbUserType.setValidationVisible(true)
        userFormUserDesign.cbUserType.setNullSelectionItemId("")
    }

    @Override
    protected void restore() {
        personSelect = itemSave.person
        if (personSelect) {
            setPersonFieldValues(personSelect)
            setPersonFieldReadOnly()
        }
        userFormUserDesign.tfUsername.setValue(itemSave.username ?: "")
        userFormUserDesign.pfPassword.setValue(itemSave.password ?: "")
        userFormUserDesign.cbUserType.setValue(itemSave.userType)
        userFormUserDesign.chkAdmin.setValue(itemSave.admin)
    }

    private void setPersonFieldValues(Person person) {
        selectPerson(person)
        userFormUserDesign.tfName.setValue(person.name ?: "")
        userFormUserDesign.tfLastName.setValue(person.name ?: "")
        userFormUserDesign.tfCellPhone.setValue(person.cellphone ?: "")
        userFormUserDesign.tfEmail.setValue(person.email ?: "")
        userFormUserDesign.cbGender.setValue(person.gender)
        userFormUserDesign.cbNationality.setValue(person.nationality)
        userFormUserDesign.cbWarrantType.setValue(person.warrantType)
        userFormUserDesign.tfWarrant.setValue(person.warrant ?: "")
        userFormUserDesign.cbGender.setValue(person.gender)
        if (person.location) {
            userFormUserDesign.tfLocation.setValue(person.location?.toString() ?: "")
        }
        if (person.occupation) {
            userFormUserDesign.tfOccupation.setValue(person.occupation.toString() ?: "")
        }
        userFormUserDesign.dfBirthday.setValue(person.birthDate ?: new Date())
        userFormUserDesign.dfIngressDate.setValue(person.ingressDate ?: new Date())
    }

    private void setPersonFieldReadOnly() {
        userFormUserDesign.tfName.setReadOnly(true)
        userFormUserDesign.tfLastName.setReadOnly(true)
        userFormUserDesign.tfCellPhone.setReadOnly(true)
        userFormUserDesign.tfEmail.setReadOnly(true)
        userFormUserDesign.cbGender.setReadOnly(true)
        userFormUserDesign.cbNationality.setReadOnly(true)
        userFormUserDesign.cbWarrantType.setReadOnly(true)
        userFormUserDesign.tfWarrant.setReadOnly(true)
        userFormUserDesign.cbGender.setReadOnly(true)
        userFormUserDesign.tfLocation.setReadOnly(true)
        userFormUserDesign.tfOccupation.setReadOnly(true)
        userFormUserDesign.dfBirthday.setReadOnly(true)
        userFormUserDesign.dfIngressDate.setReadOnly(true)
    }

    @Override
    protected void visualize() {
        oneToOneActionPerson.visualize()
        userFormUserDesign.tfUsername.setReadOnly(true)
        userFormUserDesign.pfPassword.setReadOnly(true)
        userFormUserDesign.cbUserType.setReadOnly(true)
        userFormUserDesign.chkAdmin.setReadOnly(true)
        userFormUserDesign.tfName.setReadOnly(true)
        userFormUserDesign.tfLastName.setReadOnly(true)
        userFormUserDesign.tfCellPhone.setReadOnly(true)
        userFormUserDesign.tfPhone.setReadOnly(true)
        userFormUserDesign.tfEmail.setReadOnly(true)
        userFormUserDesign.tfWarrant.setReadOnly(true)
        userFormUserDesign.tfOccupation.setReadOnly(true)
        userFormUserDesign.tfLocation.setReadOnly(true)
        userFormUserDesign.cbGender.setReadOnly(true)
        userFormUserDesign.cbNationality.setReadOnly(true)
        userFormUserDesign.cbWarrantType.setReadOnly(true)
        userFormUserDesign.dfBirthday.setReadOnly(true)
        userFormUserDesign.dfIngressDate.setReadOnly(true)
    }

    @Override
    protected boolean getElementsValid() {
        if (!oneToOneActionPerson.isValid()) {
            oneToOneActionPerson.focus()
            return false
        } else if (!userFormUserDesign.tfUsername.isValid() || userFormUserDesign.tfUsername.isEmpty()) {
            userFormUserDesign.tfUsername.focus()
            return false
        } else if (userFormUserDesign.tfUsername.getValue().equals(userService.validateUsername(userFormUserDesign.tfUsername.getValue()))) {
            ShowNotification.warning("notification.warning.username.created")
            return false
        } else if (!userFormUserDesign.pfPassword.isValid() || userFormUserDesign.pfPassword.isEmpty()) {
            userFormUserDesign.pfPassword.focus()
            return false
        } else if (!userFormUserDesign.cbUserType.isValid() || userFormUserDesign.cbUserType.isEmpty()) {
            userFormUserDesign.cbUserType.focus()
            return false
        }
        return true
    }

    @Override
    protected void collectData() {
        itemSave.person = personService.refresh(personSelect.id)
        itemSave.username = userFormUserDesign.tfUsername.getValue()
        itemSave.password = HashUtils.encodeSHA256(userFormUserDesign.pfPassword.getValue())
        itemSave.userType = userFormUserDesign.cbUserType.getValue() as UserType
        itemSave.admin = userFormUserDesign.chkAdmin.getValue()
    }

    @Override
    protected void confLanguage() {
        Constants.instance.autoWiredClass(this)

        oneToOneActionPerson.setCaption(Languages.instance.getText("window.person"))
        userFormUserDesign.tfUsername.setCaption(Languages.instance.getText("window.username"))
        userFormUserDesign.pfPassword.setCaption(Languages.instance.getText("window.password"))
        userFormUserDesign.chkAdmin.setCaption(Languages.instance.getText("window.admin"))
        userFormUserDesign.cbUserType.setCaption(Languages.instance.getText("window.user.type"))

        userFormUserDesign.tfName.setCaption(Languages.instance.getText("window.name"))
        userFormUserDesign.tfLastName.setCaption(Languages.instance.getText("window.lastname"))
        userFormUserDesign.tfCellPhone.setCaption(Languages.instance.getText("window.cellphone"))
        userFormUserDesign.tfEmail.setCaption(Languages.instance.getText("window.email"))
        userFormUserDesign.cbGender.setCaption(Languages.instance.getText("window.gender"))
        userFormUserDesign.cbWarrantType.setCaption(Languages.instance.getText("window.warrant.type"))
        userFormUserDesign.cbNationality.setCaption(Languages.instance.getText("window.nationality"))
        userFormUserDesign.tfLocation.setCaption(Languages.instance.getText("window.location"))
        userFormUserDesign.tfOccupation.setCaption(Languages.instance.getText("window.occupation"))
        userFormUserDesign.tfWarrant.setCaption(Languages.instance.getText("window.warrant"))
        userFormUserDesign.tfPhone.setCaption(Languages.instance.getText("window.phone"))
        userFormUserDesign.dfBirthday.setCaption(Languages.instance.getText("window.birthday"))
        userFormUserDesign.dfIngressDate.setCaption(Languages.instance.getText("window.ingress.date"))
    }

    @Override
    protected User getValid() {
        if (getElementsValid()) {
            collectData()
            return itemSave
        }
        return null
    }

    @Override
    void startFocus() {

    }
}
