package edu.utesa.ronald.ingsoft.ui.security

import com.vaadin.data.Property
import com.vaadin.server.Sizeable
import com.vaadin.ui.Button
import com.vaadin.ui.ComboBox
import edu.utesa.ronald.ingsoft.components.abstracts.AWindow
import edu.utesa.ronald.ingsoft.domains.security.User
import edu.utesa.ronald.ingsoft.domains.security.UserKeys
import edu.utesa.ronald.ingsoft.models.constants.Styles
import edu.utesa.ronald.ingsoft.services.security.UserKeysService
import edu.utesa.ronald.ingsoft.services.security.UserService
import edu.utesa.ronald.ingsoft.utils.Constants
import edu.utesa.ronald.ingsoft.utils.Languages
import edu.utesa.ronald.ingsoft.utils.ShowNotification
import edu.utesa.ronald.ingsoft.utils.email.templates.ValidationKeyTemplateEmail
import groovy.transform.CompileStatic
import org.springframework.beans.factory.annotation.Autowired

/**
 *  Created by ronald on 7/08/17.
 */
@CompileStatic
class UserKeysWindow extends AWindow {
    @Autowired
    private UserService userService
    @Autowired
    private UserKeysService userKeysService

    private UserKeysFormUserKeysDesign userKeysDesign = new UserKeysFormUserKeysDesign()

    private User userSelect

    private boolean request

    UserKeysWindow(boolean request) {
        this.request = request
        confLayout(userKeysDesign)
    }

    @Override
    protected void prepare() {
        Constants.instance.autoWiredClass(this)
        setWidth(600, Sizeable.Unit.PIXELS)
        setResizable(false)
        center()
    }

    @Override
    protected void confLanguage() {
        userKeysDesign.cbSecUser.setCaption(Languages.instance.getText("window.user"))
        userKeysDesign.cbRole.setCaption(Languages.instance.getText("window.role"))
        userKeysDesign.dfDate.setCaption(Languages.instance.getText("window.date"))
        userKeysDesign.tfAuthorizationKey.setCaption(Languages.instance.getText("window.authorization"))
        userKeysDesign.btnValidate.setCaption(Languages.instance.getText("window.validate"))
        userKeysDesign.btnKeyRequest.setCaption(Languages.instance.getText("window.key.request"))
    }

    @Override
    protected void buildLayout() {
        if (request) {
            setCaption(Languages.instance.getText("window.key.request"))
            userKeysDesign.tfAuthorizationKey.setVisible(false)
            userKeysDesign.tfAuthorizationKey.setEnabled(false)
            userKeysDesign.btnValidate.setVisible(false)
            userKeysDesign.btnValidate.setEnabled(false)
        } else {
            setCaption(Languages.instance.getText("window.key.validation"))
            userKeysDesign.cbSecUser.setVisible(false)
            userKeysDesign.cbSecUser.setEnabled(false)
            userKeysDesign.cbRole.setVisible(false)
            userKeysDesign.cbRole.setEnabled(false)
            userKeysDesign.dfDate.setVisible(false)
            userKeysDesign.dfDate.setEnabled(false)
            userKeysDesign.btnKeyRequest.setVisible(false)
            userKeysDesign.btnKeyRequest.setEnabled(false)
        }
        fillUser(userKeysDesign.cbSecUser)
        userKeysDesign.dfDate.setValue(new Date())
        userKeysDesign.cbSecUser.setNullSelectionAllowed(false)
        userKeysDesign.cbSecUser.addValueChangeListener(new Property.ValueChangeListener() {
            @Override
            void valueChange(Property.ValueChangeEvent event) {
                if (userKeysDesign.cbSecUser.getValue().toString()) {
                    userSelect = userService.byId(userKeysDesign.cbSecUser.getValue() as Long)
                } else {
                    userSelect = null
                }
            }
        })
        userKeysDesign.cbRole.setNullSelectionAllowed(false)

        userKeysDesign.btnKeyRequest.addClickListener(new Button.ClickListener() {
            @Override
            void buttonClick(Button.ClickEvent event) {
                if (getElementsValid()) {
//                    String valdationKey = Constants.instance.validationKeyGenerator(userSelect, userKeysDesign.dfDate.getValue())
//                    println "VALIDATION_KEY : " + valdationKey
//                    if (userSelect.emails && !userSelect.emails.isEmpty()) {
//                        if (userService.byEmail(userSelect.emails)) {
//                            UserKeys userKeys = new UserKeys()
//                            userKeys.user = loginManager.user
//                            userKeys.authorizationKey = valdationKey
//                            userKeys.secUser = userSelect
//                            userKeys.date = userKeysDesign.dfDate.getValue()
//                            userKeys.estatus = true
//                            if (userKeysService.create(userKeys, loginManager)) {
//                                Constants.instance.getMailGunAPI().sendValidationKey(
//                                        new ValidationKeyTemplateEmail(userKeys.secUser.emails, userKeys.user.emails, userKeys.authorizationKey))
//                                close()
//                            }
//                        }
//                    }
                    ShowNotification.success("notification.success.key.request", Styles.NOTIFICATION_SUCCESS)
                }
            }
        })
        userKeysDesign.btnValidate.addClickListener(new Button.ClickListener() {
            @Override
            void buttonClick(Button.ClickEvent event) {
                UserKeys userKeys = userKeysService.byAuthorization(userKeysDesign.tfAuthorizationKey.getValue())
                if (userKeys) {
                    userKeys.user = loginManager.getUser()
                    userKeys.authorizationKey = userKeysDesign.tfAuthorizationKey.getValue()
                    userKeys.secUser = userService.byId(userKeys.secUser.id)
                    userKeys.date = userKeys.date
                    userKeys.estatus = false
                    userKeysService.create(userKeys, loginManager)
                    ShowNotification.success("notification.success.key.request", Styles.NOTIFICATION_SUCCESS)
                } else {
                    ShowNotification.error("notification.error.key.validation")
                }
            }
        })
    }

    protected boolean getElementsValid() {
        if (!userKeysDesign.cbSecUser.isValid()) {
            userKeysDesign.cbSecUser.focus()
            return false
        } else if (!userKeysDesign.cbRole.isValid()) {
            userKeysDesign.cbRole.focus()
            return false
        } else if (!userKeysDesign.dfDate.isValid()) {
            userKeysDesign.dfDate.focus()
            return false
        }
        return true
    }

    private void fillUser(ComboBox comboBox) {
        comboBox.removeAllItems()
        for (User user : userService.list(true)) {
            comboBox.addItem(user.id)
            comboBox.setItemCaption(user.id, user.toString())
        }
    }

    @Override
    protected void security() {
    }
}
