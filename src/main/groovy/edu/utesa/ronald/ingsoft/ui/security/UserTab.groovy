package edu.utesa.ronald.ingsoft.ui.security

import edu.utesa.ronald.ingsoft.domains.security.User
import edu.utesa.ronald.ingsoft.models.containers.security.UserContainer
import edu.utesa.ronald.ingsoft.services.security.UserService
import edu.utesa.ronald.ingsoft.ui.components.ATabBase
import edu.utesa.ronald.ingsoft.utils.Constants
import edu.utesa.ronald.ingsoft.utils.container.LazyQuery
import groovy.transform.CompileStatic
import org.springframework.beans.factory.annotation.Autowired

/**
 * Aqui se lista todos los tipos de usuario
 *  Created by ronald on 3/07/18.
 */
@CompileStatic
class UserTab extends ATabBase<UserForm, User> {

    @Autowired
    private UserService userService

    UserTab() {
        Constants.instance.autoWiredClass(this)
    }

    @Override
    protected void confLanguage() {
    }

    @Override
    protected void buildLayout() {
        //TODO: verificar porque esta vaina no me presenta bien los usuarios cuando soy admun
        manageGrid(new UserContainer(new LazyQuery<User>() {
            @Override
            int getSize() {
                if (loginManager.admin) {
                    return userService.count(showDelete())
                } else {
                    return userService.count(loginManager.user.id, showDelete())
                }
            }

            @Override
            List<User> getItemsIds(int startIndex, int numberOfIds) {
                if (loginManager.admin) {
                    return userService.list(showDelete(), startIndex, numberOfIds, { it.person.enabled })
                } else {
                    return userService.list(loginManager.user.id, showDelete(), startIndex, numberOfIds, {
                        it.person.enabled
                    })
                }

            }

            @Override
            int getFilteredSize() {
                return 0
            }

            @Override
            List<User> getFilteredItemsIds(int startIndex, int numberOfIds) {
                return null
            }
        }))
    }

    @Override
    void refreshItem() {
        itemSelect = userService.refresh(itemSelect.id)
    }

    @Override
    UserForm onNew() {
        return new UserForm()
    }

    @Override
    UserForm onEdit() {
        return new UserForm(itemSelect, true)
    }

    @Override
    UserForm onView() {
        return new UserForm(itemSelect, false)
    }

    @Override
    Class topClass() {
        return null
    }

    @Override
    boolean deleteDomain() {
        return userService.delete(itemSelect, loginManager)
    }

    @Override
    protected void security() {

    }
}
