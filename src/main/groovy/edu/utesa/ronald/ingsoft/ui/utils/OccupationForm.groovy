package edu.utesa.ronald.ingsoft.ui.utils

import com.vaadin.server.Sizeable
import edu.utesa.ronald.ingsoft.domains.utils.Occupation
import edu.utesa.ronald.ingsoft.models.interfaces.OnNew
import edu.utesa.ronald.ingsoft.services.utils.OccupationService
import edu.utesa.ronald.ingsoft.ui.components.WindowForm
import edu.utesa.ronald.ingsoft.utils.Constants
import edu.utesa.ronald.ingsoft.utils.Languages
import edu.utesa.ronald.ingsoft.utils.WindowsCaption
import groovy.transform.CompileStatic
import org.springframework.beans.factory.annotation.Autowired

/**
 * Aqui se realizan todos los casos de los botones de accion.
 *  Created by ronald on 3/07/17.
 */
@CompileStatic
class OccupationForm extends WindowForm<Occupation> {

    @Autowired
    private OccupationService occupationService

    private OccupationFormOccupation formOccupation

    OccupationForm() {
        this(null, null)
    }

    OccupationForm(OnNew<Occupation> onNew) {
        this()
        this.onNew = onNew
    }

    OccupationForm(Occupation location, Boolean edit) {
        super(location, edit)
        confLayout(WindowsCaption.OCCUPATION_CAPTION)
        setWidth(600, Sizeable.Unit.PIXELS)
    }

    @Override
    protected void prepare() {
        super.prepare()
        Constants.instance.autoWiredClass(this)
    }

    @Override
    protected void onCreate() {
        formOccupation = new OccupationFormOccupation()
    }

    @Override
    protected void onEditView() {
        itemSave = occupationService.refresh(itemSave.id)
        formOccupation = new OccupationFormOccupation(itemSave, edit)
    }

    @Override
    Occupation onSave() {
        itemSave = formOccupation.getValid()
        return occupationService.create(itemSave, loginManager)
    }

    @Override
    protected void onViewTabs() {

    }

    @Override
    protected void buildView() {

    }

    @Override
    protected boolean getValidForm() {
        if (!formOccupation.getValid()) {
            return tabSheetManager.selectTab(0)
        }
        return true
    }

    @Override
    protected void buildLayout() {
        setWidth(800, Sizeable.Unit.PIXELS)
        tabSheetManager.addTabPanel(Languages.instance.getText(WindowsCaption.OCCUPATION_CAPTION + FORM + "occupation"), formOccupation)

    }

    @Override
    protected void security() {

    }
}
