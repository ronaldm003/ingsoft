package edu.utesa.ronald.ingsoft.ui.utils

import com.vaadin.event.FieldEvents
import edu.utesa.ronald.ingsoft.components.abstracts.AWindowTab
import edu.utesa.ronald.ingsoft.domains.utils.Occupation
import edu.utesa.ronald.ingsoft.services.utils.OccupationService
import edu.utesa.ronald.ingsoft.utils.Constants
import edu.utesa.ronald.ingsoft.utils.Languages
import groovy.transform.CompileStatic
import org.springframework.beans.factory.annotation.Autowired

/**
 *  Created by ronald on 3/07/17.
 */
@CompileStatic
class OccupationFormOccupation extends AWindowTab<Occupation> {

    @Autowired
    private OccupationService occupationService

    private OccupationFormOccupationDesign occupationFormOccupationDesign = new OccupationFormOccupationDesign()

    OccupationFormOccupation() {
        this(new Occupation(), null)
    }

    OccupationFormOccupation(Occupation occupation, Boolean edit) {
        super(occupation, edit)
        confLayout(occupationFormOccupationDesign)
    }

    @Override
    protected void buildLayout() {
        occupationFormOccupationDesign.tfDescription.setRequired(true)
        occupationFormOccupationDesign.tfDescription.setRequiredError(Languages.instance.getText("validations.empty.field"))
        occupationFormOccupationDesign.tfDescription.setValidationVisible(true)
        occupationFormOccupationDesign.tfDescription.setNullRepresentation("")
        occupationFormOccupationDesign.tfDescription.addTextChangeListener(new FieldEvents.TextChangeListener() {
            @Override
            void textChange(FieldEvents.TextChangeEvent event) {
                occupationFormOccupationDesign.tfDescription.setValue(event.getText())
                occupationFormOccupationDesign.tfDescription.setCursorPosition(event.getCursorPosition())
                occupationFormOccupationDesign.tfDescription.validate()
            }
        })
    }

    @Override
    protected void restore() {
        occupationFormOccupationDesign.tfDescription.setValue(itemSave.description ?: "")
    }

    @Override
    protected void visualize() {
        occupationFormOccupationDesign.tfDescription.setReadOnly(true)
    }

    @Override
    protected boolean getElementsValid() {
        if (!occupationFormOccupationDesign.tfDescription.isValid()) {
            occupationFormOccupationDesign.tfDescription.focus()
            return false
        }
        return true
    }

    @Override
    protected void collectData() {
        itemSave.description = occupationFormOccupationDesign.tfDescription.getValue()
    }

    @Override
    protected void confLanguage() {
        Constants.instance.autoWiredClass(this)
        occupationFormOccupationDesign.tfDescription.setCaption(Languages.instance.getText("window.description"))
    }

    @Override
    protected Occupation getValid() {
        if (getElementsValid()) {
            collectData()
            return itemSave
        }
        return null
    }

    @Override
    void startFocus() {
        occupationFormOccupationDesign.tfDescription.focus()
    }
}
