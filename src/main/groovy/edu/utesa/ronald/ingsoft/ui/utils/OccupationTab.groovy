package edu.utesa.ronald.ingsoft.ui.utils

import edu.utesa.ronald.ingsoft.domains.utils.Occupation
import edu.utesa.ronald.ingsoft.models.containers.utils.OccupationContainer
import edu.utesa.ronald.ingsoft.services.utils.OccupationService
import edu.utesa.ronald.ingsoft.ui.components.ATabBase
import edu.utesa.ronald.ingsoft.utils.Constants
import edu.utesa.ronald.ingsoft.utils.container.LazyQuery
import groovy.transform.CompileStatic
import org.springframework.beans.factory.annotation.Autowired

/**
 * Aqui se lista todos los tipos de usuario
 *  Created by ronald on 3/07/17.
 */
@CompileStatic
class OccupationTab extends ATabBase<OccupationForm, Occupation> {

    @Autowired
    private OccupationService locationService

    OccupationTab() {
        Constants.instance.autoWiredClass(this)
    }

    @Override
    protected void confLanguage() {

    }

    @Override
    protected void buildLayout() {
        manageGrid(new OccupationContainer(new LazyQuery<Occupation>() {
            @Override
            int getSize() {
                return locationService.count(showDelete())
            }

            @Override
            List<Occupation> getItemsIds(int startIndex, int numberOfIds) {
                return locationService.list(showDelete(), startIndex, numberOfIds)
            }

            @Override
            int getFilteredSize() {
                return 0
            }

            @Override
            List<Occupation> getFilteredItemsIds(int startIndex, int numberOfIds) {
                return null
            }
        }))
    }

    @Override
    void refreshItem() {

    }

    @Override
    OccupationForm onNew() {
        return new OccupationForm()
    }

    @Override
    OccupationForm onEdit() {
        return new OccupationForm(itemSelect, true)
    }

    @Override
    OccupationForm onView() {
        return new OccupationForm(itemSelect, false)
    }

    @Override
    Class topClass() {
        return null
    }

    @Override
    boolean deleteDomain() {
        return locationService.delete(itemSelect, loginManager)
    }

    @Override
    protected void security() {

    }
}
