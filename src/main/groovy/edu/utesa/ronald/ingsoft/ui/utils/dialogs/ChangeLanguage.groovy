package edu.utesa.ronald.ingsoft.ui.utils.dialogs

import com.vaadin.server.Sizeable
import com.vaadin.ui.Button
import edu.utesa.ronald.ingsoft.components.abstracts.AWindow
import edu.utesa.ronald.ingsoft.utils.CBFiller
import edu.utesa.ronald.ingsoft.utils.Constants
import edu.utesa.ronald.ingsoft.utils.Languages
import groovy.transform.CompileStatic

/**
 *  Created by ronald on 7/08/17.
 */
@CompileStatic
class ChangeLanguage extends AWindow {

    private ChangeLanguageDesign changeLanguageDesign = new ChangeLanguageDesign()

    ChangeLanguage() {
        confLayout(changeLanguageDesign)
    }

    @Override
    protected void prepare() {
        Constants.instance.autoWiredClass(this)
        setWidth(300, Sizeable.Unit.PIXELS)
        setResizable(false)
        center()
    }

    @Override
    protected void confLanguage() {
        changeLanguageDesign.cbLanguage.setCaption(Languages.instance.getText("language.window.caption"))
        changeLanguageDesign.btnOK.setCaption(Languages.instance.getText("ok"))
    }

    @Override
    protected void buildLayout() {
        CBFiller.fillLanguage(changeLanguageDesign.cbLanguage)
        changeLanguageDesign.cbLanguage.setNullSelectionAllowed(false)
        changeLanguageDesign.cbLanguage.setValue(Languages.instance.getDefault())
        changeLanguageDesign.btnOK.addClickListener(new Button.ClickListener() {
            @Override
            void buttonClick(Button.ClickEvent event) {
                Locale select = changeLanguageDesign.cbLanguage.getValue() as Locale
                if (Languages.instance.getDefault() != select) {
                    Languages.setDefault(select)
                    getUI().getPage().reload()
                    close()
                }
            }
        })
    }

    @Override
    protected void security() {
    }
}
