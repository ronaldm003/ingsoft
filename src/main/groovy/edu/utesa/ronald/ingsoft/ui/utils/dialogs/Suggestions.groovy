package edu.utesa.ronald.ingsoft.ui.utils.dialogs

import com.vaadin.server.Sizeable
import com.vaadin.ui.Button
import edu.utesa.ronald.ingsoft.components.abstracts.AWindow
import edu.utesa.ronald.ingsoft.utils.Constants
import edu.utesa.ronald.ingsoft.utils.Languages
import groovy.transform.CompileStatic

/**
 *  Created by ronald on 7/08/17.
 */
@CompileStatic
class Suggestions extends AWindow {

    private SuggestionsDesign suggestionsDesign = new SuggestionsDesign()

    private String message

    Suggestions(String message) {
        this.message = message
        confLayout(suggestionsDesign)
    }

    @Override
    protected void prepare() {
        Constants.instance.autoWiredClass(this)
        setWidth(500, Sizeable.Unit.PIXELS)
        setResizable(false)
        setModal(true)
        center()
    }

    @Override
    protected void confLanguage() {
        suggestionsDesign.taSuggestion.setCaption(Languages.instance.getText("window.message"))
        suggestionsDesign.btnSend.setCaption(Languages.instance.getText("window.action.close"))
    }

    @Override
    protected void buildLayout() {
        suggestionsDesign.taSuggestion.setValue(message)
        suggestionsDesign.btnSend.addClickListener(new Button.ClickListener() {
            @Override
            void buttonClick(Button.ClickEvent event) {
//                ShowNotification.notification("Enviando Mensaje", "", Notification.Type.HUMANIZED_MESSAGE, ValoTheme.NOTIFICATION_SUCCESS)
                close()
            }
        })
    }

    @Override
    protected void security() {
    }
}
