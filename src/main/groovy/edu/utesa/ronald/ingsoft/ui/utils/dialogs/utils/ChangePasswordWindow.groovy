package edu.utesa.ronald.ingsoft.ui.utils.dialogs.utils

import com.vaadin.server.Sizeable
import com.vaadin.server.VaadinSession
import com.vaadin.ui.Button
import com.vaadin.ui.themes.ValoTheme
import edu.utesa.ronald.ingsoft.components.abstracts.AWindow
import edu.utesa.ronald.ingsoft.domains.security.User
import edu.utesa.ronald.ingsoft.services.security.UserService
import edu.utesa.ronald.ingsoft.utils.Constants
import edu.utesa.ronald.ingsoft.utils.Languages
import edu.utesa.ronald.ingsoft.utils.ShowNotification
import edu.utesa.ronald.ingsoft.utils.security.HashUtils
import groovy.transform.CompileStatic
import org.springframework.beans.factory.annotation.Autowired

/**
 *  Created by ronald on 07/26/17.
 */
@SuppressWarnings(["ChangeToOperator", "ChangeToOperator"])
@CompileStatic
class ChangePasswordWindow extends AWindow {

    @Autowired
    private UserService userService

    private ChangePasswordDesign changePasswordDesign = new ChangePasswordDesign()

    ChangePasswordWindow() {
        confLayout(changePasswordDesign)
    }

    @Override
    protected void prepare() {
        setWidth(400, Sizeable.Unit.PIXELS)
        center()
        setResizable(false)
        setModal(true)
    }

    @Override
    protected void confLanguage() {
        Constants.instance.autoWiredClass(this)
        changePasswordDesign.pfOldPassword.setCaption(Languages.instance.getText("window.oldpassword"))
        changePasswordDesign.pfNewPassword.setCaption(Languages.instance.getText("window.newpassword"))
        changePasswordDesign.pfConfirmPassword.setCaption(Languages.instance.getText("window.confirmpassword"))
        changePasswordDesign.btnOK.setCaption(Languages.instance.getText("window.ok"))
    }

    @Override
    protected void buildLayout() {
        changePasswordDesign.btnOK.addClickListener(new Button.ClickListener() {
            @Override
            void buttonClick(Button.ClickEvent event) {
                if (checkOldPasswords()) {
                    if (checkPasswords()) {
                        User user = userService.refresh(loginManager.user.id)
                        user.password = HashUtils.encodeSHA256(changePasswordDesign.pfConfirmPassword.getValue())
                        loginManager.refresh(userService.changePassword(user, loginManager))
                        VaadinSession.current.session.setAttribute(Constants.USER_LOGIN, loginManager)
                        ShowNotification.success(ValoTheme.NOTIFICATION_SUCCESS)
                        close()
                    } else {
                        ShowNotification.passwordMismatchError()
                    }
                } else {
                    ShowNotification.oldPasswordError()
                }
            }
        })
    }

    private boolean checkOldPasswords() {
        boolean check = loginManager.password.equals(HashUtils.encodeSHA256(changePasswordDesign.pfOldPassword.getValue()))
        return check
    }

    private boolean checkPasswords() {
        return changePasswordDesign.pfNewPassword.getValue().equals(changePasswordDesign.pfConfirmPassword.getValue())
    }

    @Override
    protected void security() {
    }

}
