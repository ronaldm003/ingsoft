package edu.utesa.ronald.ingsoft.ui.utils.pickers

import com.vaadin.server.Sizeable
import edu.utesa.ronald.ingsoft.domains.control.Article
import edu.utesa.ronald.ingsoft.models.containers.control.ArticleContainer
import edu.utesa.ronald.ingsoft.models.interfaces.PickElement
import edu.utesa.ronald.ingsoft.services.control.ArticleService
import edu.utesa.ronald.ingsoft.ui.components.pickers.PickerBase
import edu.utesa.ronald.ingsoft.utils.Constants
import edu.utesa.ronald.ingsoft.utils.Languages
import edu.utesa.ronald.ingsoft.utils.WindowsCaption
import edu.utesa.ronald.ingsoft.utils.container.LazyQuery
import groovy.transform.CompileStatic
import org.springframework.beans.factory.annotation.Autowired

/**
 *  Created by ronald on 9/22/16.
 */
@CompileStatic
class ArticlePicker extends PickerBase<Article> {

    @Autowired
    private ArticleService articleService

    ArticlePicker(PickElement<Article> pickElement, boolean multiSelect) {
        super(pickElement, multiSelect)
    }

    @Override
    protected void prepare() {
        Constants.instance.autoWiredClass(this)
        setWidth(600, Sizeable.Unit.PIXELS)
    }

    @Override
    protected void confLanguage() {
        setCaption(Languages.instance.getText(WindowsCaption.ARTICLE_CAPTION + PICK))
    }

    @Override
    protected void buildLayout() {
        manageGrid(grid(), new ArticleContainer(new LazyQuery<Article>() {
            @Override
            int getSize() {
                return articleService.count(true)
            }

            @Override
            List<Article> getItemsIds(int startIndex, int numberOfIds) {
                return articleService.list(true, startIndex, numberOfIds)
            }

            @Override
            int getFilteredSize() {
                return 0
            }

            @Override
            List<Article> getFilteredItemsIds(int startIndex, int numberOfIds) {
                return null
            }
        }))
    }

    @Override
    protected void security() {
    }
}
