package edu.utesa.ronald.ingsoft.ui.utils.pickers

import com.vaadin.server.Sizeable
import edu.utesa.ronald.ingsoft.domains.admin.Cycle
import edu.utesa.ronald.ingsoft.models.containers.admin.CycleContainer
import edu.utesa.ronald.ingsoft.models.interfaces.PickElement
import edu.utesa.ronald.ingsoft.services.admin.CycleService
import edu.utesa.ronald.ingsoft.ui.components.pickers.PickerBase
import edu.utesa.ronald.ingsoft.utils.Constants
import edu.utesa.ronald.ingsoft.utils.Languages
import edu.utesa.ronald.ingsoft.utils.WindowsCaption
import edu.utesa.ronald.ingsoft.utils.container.LazyQuery
import groovy.transform.CompileStatic
import org.springframework.beans.factory.annotation.Autowired

/**
 *  Created by ronald on 9/14/17.
 */
@CompileStatic
class CyclePicker extends PickerBase<Cycle> {

    @Autowired
    private CycleService cycleService

    CyclePicker(PickElement<Cycle> pickElement, boolean multiSelect) {
        super(pickElement, multiSelect)
    }

    @Override
    protected void prepare() {
        Constants.instance.autoWiredClass(this)
        setWidth(800, Sizeable.Unit.PIXELS)
    }

    @Override
    protected void confLanguage() {
        setCaption(Languages.instance.getText(WindowsCaption.CYCLE_CAPTION + PICK))
    }

    @Override
    protected void buildLayout() {
        pickerAction.btnNull().setEnabled(false)
        pickerAction.btnNull().setVisible(false)

        manageGrid(grid(), new CycleContainer(new LazyQuery<Cycle>() {
            @Override
            int getSize() {
                return cycleService.count(true)
            }

            @Override
            List<Cycle> getItemsIds(int startIndex, int numberOfIds) {
                return cycleService.list(true, startIndex, numberOfIds)
            }

            @Override
            int getFilteredSize() {
                return 0
            }

            @Override
            List<Cycle> getFilteredItemsIds(int startIndex, int numberOfIds) {
                return null
            }
        }))
    }

    @Override
    protected void security() {
    }
}
