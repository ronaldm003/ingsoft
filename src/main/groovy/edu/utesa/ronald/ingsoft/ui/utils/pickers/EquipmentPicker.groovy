package edu.utesa.ronald.ingsoft.ui.utils.pickers

import com.vaadin.server.Sizeable
import edu.utesa.ronald.ingsoft.domains.control.Equipment
import edu.utesa.ronald.ingsoft.models.containers.control.EquipmentContainer
import edu.utesa.ronald.ingsoft.models.interfaces.PickElement
import edu.utesa.ronald.ingsoft.services.control.EquipmentService
import edu.utesa.ronald.ingsoft.ui.components.pickers.PickerBase
import edu.utesa.ronald.ingsoft.utils.Constants
import edu.utesa.ronald.ingsoft.utils.Languages
import edu.utesa.ronald.ingsoft.utils.WindowsCaption
import edu.utesa.ronald.ingsoft.utils.container.LazyQuery
import groovy.transform.CompileStatic
import org.springframework.beans.factory.annotation.Autowired

/**
 *  Created by ronald on 9/22/16.
 */
@CompileStatic
class EquipmentPicker extends PickerBase<Equipment> {

    @Autowired
    private EquipmentService equipmentService

    EquipmentPicker(PickElement<Equipment> pickElement, boolean multiSelect) {
        super(pickElement, multiSelect)
    }

    @Override
    protected void prepare() {
        Constants.instance.autoWiredClass(this)
        setWidth(600, Sizeable.Unit.PIXELS)
    }

    @Override
    protected void confLanguage() {
        setCaption(Languages.instance.getText(WindowsCaption.EQUIPMENT_CAPTION + PICK))
    }

    @Override
    protected void buildLayout() {
        manageGrid(grid(), new EquipmentContainer(new LazyQuery<Equipment>() {
            @Override
            int getSize() {
                return equipmentService.count(true)
            }

            @Override
            List<Equipment> getItemsIds(int startIndex, int numberOfIds) {
                return equipmentService.list(true, startIndex, numberOfIds)
            }

            @Override
            int getFilteredSize() {
                return 0
            }

            @Override
            List<Equipment> getFilteredItemsIds(int startIndex, int numberOfIds) {
                return null
            }
        }))
    }

    @Override
    protected void security() {
    }
}
