package edu.utesa.ronald.ingsoft.ui.utils.pickers

import com.vaadin.server.Sizeable
import edu.utesa.ronald.ingsoft.domains.control.Inventory
import edu.utesa.ronald.ingsoft.models.containers.control.InventoryContainer
import edu.utesa.ronald.ingsoft.models.interfaces.PickElement
import edu.utesa.ronald.ingsoft.services.control.InventoryService
import edu.utesa.ronald.ingsoft.ui.components.pickers.PickerBase
import edu.utesa.ronald.ingsoft.utils.Constants
import edu.utesa.ronald.ingsoft.utils.Languages
import edu.utesa.ronald.ingsoft.utils.WindowsCaption
import edu.utesa.ronald.ingsoft.utils.container.LazyQuery
import groovy.transform.CompileStatic
import org.springframework.beans.factory.annotation.Autowired

/**
 *  Created by ronald on 9/22/16.
 */
@CompileStatic
class InventoryPicker extends PickerBase<Inventory> {

    @Autowired
    private InventoryService inventoryService

    InventoryPicker(PickElement<Inventory> pickElement, boolean multiSelect) {
        super(pickElement, multiSelect)
    }

    @Override
    protected void prepare() {
        Constants.instance.autoWiredClass(this)
        setWidth(600, Sizeable.Unit.PIXELS)
    }

    @Override
    protected void confLanguage() {
        setCaption(Languages.instance.getText(WindowsCaption.INVENTORY_CAPTION + PICK))
    }

    @Override
    protected void buildLayout() {
        manageGrid(grid(), new InventoryContainer(new LazyQuery<Inventory>() {
            @Override
            int getSize() {
                return inventoryService.count(true)
            }

            @Override
            List<Inventory> getItemsIds(int startIndex, int numberOfIds) {
                return inventoryService.list(true, startIndex, numberOfIds)
            }

            @Override
            int getFilteredSize() {
                return 0
            }

            @Override
            List<Inventory> getFilteredItemsIds(int startIndex, int numberOfIds) {
                return null
            }
        }))
    }

    @Override
    protected void security() {
    }
}
