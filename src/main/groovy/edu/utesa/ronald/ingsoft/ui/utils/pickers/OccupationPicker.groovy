package edu.utesa.ronald.ingsoft.ui.utils.pickers

import com.vaadin.server.Sizeable
import edu.utesa.ronald.ingsoft.domains.utils.Occupation
import edu.utesa.ronald.ingsoft.models.containers.utils.OccupationContainer
import edu.utesa.ronald.ingsoft.models.interfaces.PickElement
import edu.utesa.ronald.ingsoft.services.utils.OccupationService
import edu.utesa.ronald.ingsoft.ui.components.pickers.PickerBase
import edu.utesa.ronald.ingsoft.utils.Constants
import edu.utesa.ronald.ingsoft.utils.Languages
import edu.utesa.ronald.ingsoft.utils.WindowsCaption
import edu.utesa.ronald.ingsoft.utils.container.LazyQuery
import groovy.transform.CompileStatic
import org.springframework.beans.factory.annotation.Autowired

/**
 *  Created by ronald on 9/14/17.
 */
@CompileStatic
class OccupationPicker extends PickerBase<Occupation> {

    @Autowired
    private OccupationService occupationService

    OccupationPicker(PickElement<Occupation> pickElement, boolean multiSelect) {
        super(pickElement, multiSelect)
    }

    @Override
    protected void prepare() {
        Constants.instance.autoWiredClass(this)
        setWidth(800, Sizeable.Unit.PIXELS)
    }

    @Override
    protected void confLanguage() {
        setCaption(Languages.instance.getText(WindowsCaption.OCCUPATION_CAPTION + PICK))
    }

    @Override
    protected void buildLayout() {
        pickerAction.btnNull().setEnabled(false)
        pickerAction.btnNull().setVisible(false)

        manageGrid(grid(), new OccupationContainer(new LazyQuery<Occupation>() {
            @Override
            int getSize() {
                return occupationService.count(true)
            }

            @Override
            List<Occupation> getItemsIds(int startIndex, int numberOfIds) {
                return occupationService.list(true, startIndex, numberOfIds)
            }

            @Override
            int getFilteredSize() {
                return 0
            }

            @Override
            List<Occupation> getFilteredItemsIds(int startIndex, int numberOfIds) {
                return null
            }
        }))
    }

    @Override
    protected void security() {
    }
}
