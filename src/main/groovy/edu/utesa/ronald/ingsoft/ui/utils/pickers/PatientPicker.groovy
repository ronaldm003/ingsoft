package edu.utesa.ronald.ingsoft.ui.utils.pickers

import com.vaadin.server.Sizeable
import edu.utesa.ronald.ingsoft.domains.control.Patient
import edu.utesa.ronald.ingsoft.models.containers.control.PatientContainer
import edu.utesa.ronald.ingsoft.models.interfaces.PickElement
import edu.utesa.ronald.ingsoft.services.control.PatientService
import edu.utesa.ronald.ingsoft.ui.components.pickers.PickerBase
import edu.utesa.ronald.ingsoft.utils.Constants
import edu.utesa.ronald.ingsoft.utils.Languages
import edu.utesa.ronald.ingsoft.utils.WindowsCaption
import edu.utesa.ronald.ingsoft.utils.container.LazyQuery
import groovy.transform.CompileStatic
import org.springframework.beans.factory.annotation.Autowired

/**
 *  Created by ronald on 9/22/16.
 */
@CompileStatic
class PatientPicker extends PickerBase<Patient> {

    @Autowired
    private PatientService patientService

    PatientPicker(PickElement<Patient> pickElement, boolean multiSelect) {
        super(pickElement, multiSelect)
    }

    @Override
    protected void prepare() {
        Constants.instance.autoWiredClass(this)
        setWidth(600, Sizeable.Unit.PIXELS)
    }

    @Override
    protected void confLanguage() {
        setCaption(Languages.instance.getText(WindowsCaption.PATIENT_CAPTION + PICK))
    }

    @Override
    protected void buildLayout() {
        manageGrid(grid(), new PatientContainer(new LazyQuery<Patient>() {
            @Override
            int getSize() {
                return patientService.count(true)
            }

            @Override
            List<Patient> getItemsIds(int startIndex, int numberOfIds) {
                return patientService.list(true, startIndex, numberOfIds, { it.person.enabled })
            }

            @Override
            int getFilteredSize() {
                return 0
            }

            @Override
            List<Patient> getFilteredItemsIds(int startIndex, int numberOfIds) {
                return null
            }
        }))
    }

    @Override
    protected void security() {
    }
}
