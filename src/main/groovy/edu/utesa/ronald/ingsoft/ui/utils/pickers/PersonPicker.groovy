package edu.utesa.ronald.ingsoft.ui.utils.pickers

import com.vaadin.server.Sizeable
import edu.utesa.ronald.ingsoft.domains.security.Person
import edu.utesa.ronald.ingsoft.models.containers.security.PersonContainer
import edu.utesa.ronald.ingsoft.models.interfaces.PickElement
import edu.utesa.ronald.ingsoft.services.security.PersonService
import edu.utesa.ronald.ingsoft.ui.components.pickers.PickerBase
import edu.utesa.ronald.ingsoft.utils.Constants
import edu.utesa.ronald.ingsoft.utils.Languages
import edu.utesa.ronald.ingsoft.utils.WindowsCaption
import edu.utesa.ronald.ingsoft.utils.container.LazyQuery
import groovy.transform.CompileStatic
import org.springframework.beans.factory.annotation.Autowired

/**
 *  Created by ronald on 9/14/17.
 */
@CompileStatic
class PersonPicker extends PickerBase<Person> {

    @Autowired
    private PersonService personService

    PersonPicker(PickElement<Person> pickElement, boolean multiSelect) {
        super(pickElement, multiSelect)
    }

    @Override
    protected void prepare() {
        Constants.instance.autoWiredClass(this)
        setWidth(800, Sizeable.Unit.PIXELS)
    }

    @Override
    protected void confLanguage() {
        setCaption(Languages.instance.getText(WindowsCaption.PERSON_CAPTION + PICK))
    }

    @Override
    protected void buildLayout() {
        pickerAction.btnNull().setEnabled(false)
        pickerAction.btnNull().setVisible(false)

        manageGrid(grid(), new PersonContainer(new LazyQuery<Person>() {
            @Override
            int getSize() {
                return personService.count(true)
            }

            @Override
            List<Person> getItemsIds(int startIndex, int numberOfIds) {
                return personService.list(true, startIndex, numberOfIds, {
                    it.location?.enabled; it.occupation?.enabled
                })
            }

            @Override
            int getFilteredSize() {
                return 0
            }

            @Override
            List<Person> getFilteredItemsIds(int startIndex, int numberOfIds) {
                return null
            }
        }))
    }

    @Override
    protected void security() {
    }
}
