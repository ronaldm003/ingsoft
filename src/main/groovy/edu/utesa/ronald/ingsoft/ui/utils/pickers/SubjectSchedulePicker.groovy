package edu.utesa.ronald.ingsoft.ui.utils.pickers

import com.vaadin.server.Sizeable
import edu.utesa.ronald.ingsoft.domains.admin.SubjectSchedule
import edu.utesa.ronald.ingsoft.models.containers.admin.SubjectScheduleContainer
import edu.utesa.ronald.ingsoft.models.interfaces.PickElement
import edu.utesa.ronald.ingsoft.services.admin.SubjectScheduleService
import edu.utesa.ronald.ingsoft.ui.components.pickers.PickerBase
import edu.utesa.ronald.ingsoft.utils.Constants
import edu.utesa.ronald.ingsoft.utils.Languages
import edu.utesa.ronald.ingsoft.utils.WindowsCaption
import edu.utesa.ronald.ingsoft.utils.container.LazyQuery
import groovy.transform.CompileStatic
import org.springframework.beans.factory.annotation.Autowired

/**
 *  Created by ronald on 9/22/16.
 */
@CompileStatic
class SubjectSchedulePicker extends PickerBase<SubjectSchedule> {

    @Autowired
    private SubjectScheduleService subjectScheduleService

    SubjectSchedulePicker(PickElement<SubjectSchedule> pickElement, boolean multiSelect) {
        super(pickElement, multiSelect)
    }

    @Override
    protected void prepare() {
        Constants.instance.autoWiredClass(this)
        setWidth(600, Sizeable.Unit.PIXELS)
    }

    @Override
    protected void confLanguage() {
        setCaption(Languages.instance.getText(WindowsCaption.SUBJECT_SCHEDULE_CAPTION + PICK))
    }

    @Override
    protected void buildLayout() {
        manageGrid(grid(), new SubjectScheduleContainer(new LazyQuery<SubjectSchedule>() {
            @Override
            int getSize() {
                return subjectScheduleService.count(true)
            }

            @Override
            List<SubjectSchedule> getItemsIds(int startIndex, int numberOfIds) {
                return subjectScheduleService.list(true, startIndex, numberOfIds)
            }

            @Override
            int getFilteredSize() {
                return 0
            }

            @Override
            List<SubjectSchedule> getFilteredItemsIds(int startIndex, int numberOfIds) {
                return null
            }
        }))
    }

    @Override
    protected void security() {
    }
}
