package edu.utesa.ronald.ingsoft.ui.utils.pickers

import com.vaadin.server.Sizeable
import edu.utesa.ronald.ingsoft.domains.admin.Teacher
import edu.utesa.ronald.ingsoft.models.containers.admin.TeacherContainer
import edu.utesa.ronald.ingsoft.models.interfaces.PickElement
import edu.utesa.ronald.ingsoft.services.admin.TeacherService
import edu.utesa.ronald.ingsoft.ui.components.pickers.PickerBase
import edu.utesa.ronald.ingsoft.utils.Constants
import edu.utesa.ronald.ingsoft.utils.Languages
import edu.utesa.ronald.ingsoft.utils.WindowsCaption
import edu.utesa.ronald.ingsoft.utils.container.LazyQuery
import groovy.transform.CompileStatic
import org.springframework.beans.factory.annotation.Autowired

/**
 *  Created by ronald on 9/14/17.
 */
@CompileStatic
class TeacherPicker extends PickerBase<Teacher> {

    @Autowired
    private TeacherService teacherService

    TeacherPicker(PickElement<Teacher> pickElement, boolean multiSelect) {
        super(pickElement, multiSelect)
    }

    @Override
    protected void prepare() {
        Constants.instance.autoWiredClass(this)
        setWidth(800, Sizeable.Unit.PIXELS)
    }

    @Override
    protected void confLanguage() {
        setCaption(Languages.instance.getText(WindowsCaption.TEACHER_CAPTION + PICK))
    }

    @Override
    protected void buildLayout() {
        pickerAction.btnNull().setEnabled(false)
        pickerAction.btnNull().setVisible(false)
        manageGrid(grid(), new TeacherContainer(new LazyQuery<Teacher>() {
            @Override
            int getSize() {
                return teacherService.count(true)
            }

            @Override
            List<Teacher> getItemsIds(int startIndex, int numberOfIds) {
                return teacherService.list(true, startIndex, numberOfIds, { it.person.enabled })
            }

            @Override
            int getFilteredSize() {
                return 0
            }

            @Override
            List<Teacher> getFilteredItemsIds(int startIndex, int numberOfIds) {
                return null
            }
        }))
    }

    @Override
    protected void security() {
    }
}
