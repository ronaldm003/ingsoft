package edu.utesa.ronald.ingsoft.utils

import groovy.transform.CompileStatic

@CompileStatic
class BaseDB {

    boolean enabled = true
    String createdBy = Constants.ROOT
    String modifyBy = Constants.ROOT
    Date dateCreated
    Date lastUpdated
}
