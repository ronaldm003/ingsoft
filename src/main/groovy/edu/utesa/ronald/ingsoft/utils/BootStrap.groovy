package edu.utesa.ronald.ingsoft.utils

import edu.utesa.ronald.ingsoft.utils.bootstrap.*
import edu.utesa.ronald.ingsoft.utils.bootstrap.recordcard.*

/**
 *  Created by ronald on 7/4/17.
 */
class BootStrap {

    BasicParams basicParams = new BasicParams()
    BasicLocation basicLocation = new BasicLocation()
    BasicOccupation basicOccupation = new BasicOccupation()
    BasicInventory basicInventory = new BasicInventory()
    BasicEquipment basicEquipment = new BasicEquipment()
    BasicCycle basicCycle = new BasicCycle()
    BasicSubject basicSubject = new BasicSubject()
    BasicObjective basicObjective = new BasicObjective()
    BasicArticle basicArticle = new BasicArticle()
    BasicClinic basicClinic = new BasicClinic()
    BasicPerson basicPerson = new BasicPerson()
    BasicTeacher basicTeacher = new BasicTeacher()
    BasicStudent basicStudent = new BasicStudent()
    BasicPatient basicPatient = new BasicPatient()
    BasicUsers basicUsers = new BasicUsers()
    BasicTooth basicTooth = new BasicTooth()
    BasicIllness basicIllness = new BasicIllness()
    BasicHabits basicHabits = new BasicHabits()
    BasicExtraBuccal basicExtraBuccal = new BasicExtraBuccal()
    BasicIntraBuccal basicIntraBuccal = new BasicIntraBuccal()
    BasicEvaluationQuestions basicEvaluationQuestions = new BasicEvaluationQuestions()

    BootStrap() {
        Constants.instance.autoWiredClass(this)
    }

    void init() {
        basicParams.insert()
        basicOccupation.insert()
        basicLocation.insert()
        basicInventory.insert()
        basicEquipment.insert()
        basicArticle.insert()
        basicObjective.insert()
        basicSubject.insertSubject(basicObjective)
        basicCycle.insert(basicSubject)
        basicClinic.insert(basicInventory)
        basicPerson.insert(basicLocation, basicOccupation)
        basicTeacher.insert(basicPerson)
        basicStudent.insert(basicPerson, basicSubject)
        basicPatient.insert(basicPerson)
        basicUsers.insert(basicPerson)
        basicTooth.insert()
        basicIllness.insert()
        basicHabits.insert()
        basicExtraBuccal.insert()
        basicIntraBuccal.insert()
        basicEvaluationQuestions.insert()
//        Constants.instance.getMailGunAPI()
    }
}
