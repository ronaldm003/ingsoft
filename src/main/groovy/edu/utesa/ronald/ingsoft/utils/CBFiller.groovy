package edu.utesa.ronald.ingsoft.utils

import com.vaadin.ui.ComboBox
import com.vaadin.ui.OptionGroup
import edu.utesa.ronald.ingsoft.domains.utils.Occupation
import edu.utesa.ronald.ingsoft.models.enums.*
import edu.utesa.ronald.ingsoft.models.enums.recordcard.ArcType
import edu.utesa.ronald.ingsoft.models.enums.recordcard.FacialProfile
import edu.utesa.ronald.ingsoft.models.params.ParamValueType
import groovy.transform.CompileStatic

/**
 *  Created by ronald on 9/27/17.
 */
@CompileStatic
class CBFiller {

    private static void configure(ComboBox comboBox) {
        comboBox.removeAllItems()
        comboBox.setNullSelectionAllowed(false)
    }

    private static void configure(OptionGroup optionGroup) {
        optionGroup.removeAllItems()
        optionGroup.setNullSelectionAllowed(false)
    }

    static void fillLanguage(ComboBox comboBox) {
        configure(comboBox)
        for (Locale language : Languages.instance.values()) {
            comboBox.addItem(language)
            comboBox.setItemCaption(language, Languages.instance.getMapLanguage(language))
        }
    }

    static void fillRelationType(ComboBox comboBox) {
        configure(comboBox)
        for (RelationType relationTypeEnum : RelationType.values()) {
            comboBox.addItem(relationTypeEnum)
            comboBox.setItemCaption(relationTypeEnum, relationTypeEnum.toString())
        }
    }

    static void fillParamValueType(ComboBox comboBox) {
        configure(comboBox)
        for (ParamValueType paramValueType : ParamValueType.values()) {
            comboBox.addItem(paramValueType)
            comboBox.setItemCaption(paramValueType, paramValueType.toString())
        }
    }

    static void fillTimeZone(ComboBox comboBox) {
        configure(comboBox)
        for (String timeZone : TimeZone.getAvailableIDs()) {
            comboBox.addItem(timeZone)
        }
    }

    static void fillNationality(ComboBox comboBox) {
        configure(comboBox)
        for (Nationality nationality : Nationality.values()) {
            comboBox.addItem(nationality)
            comboBox.setItemCaption(nationality, nationality.toString())
        }
    }

    static void fillGender(ComboBox comboBox) {
        configure(comboBox)
        for (Gender gender : Gender.values()) {
            comboBox.addItem(gender)
            comboBox.setItemCaption(gender, gender.toString())
        }
    }

    static void fillUserType(ComboBox comboBox) {
        configure(comboBox)
        for (UserType userType : UserType.values()) {
            comboBox.addItem(userType)
            comboBox.setItemCaption(userType, userType.toString())
        }
    }

    static void fillWarrantType(ComboBox comboBox) {
        configure(comboBox)
        for (WarrantType warrantType : WarrantType.values()) {
            comboBox.addItem(warrantType)
            comboBox.setItemCaption(warrantType, warrantType.toString())
        }
    }

    static void fillComplexion(ComboBox comboBox) {
        configure(comboBox)
        for (Complexion complexion : Complexion.values()) {
            comboBox.addItem(complexion)
            comboBox.setItemCaption(complexion, complexion.toString())
        }
    }

    static void fillFacialProfile(OptionGroup optionGroup) {
        configure(optionGroup)
        for (FacialProfile facialProfile : FacialProfile.values()) {
            optionGroup.addItem(facialProfile)
            optionGroup.setItemCaption(facialProfile, facialProfile.toString())
        }
    }

    static void fillArcType(OptionGroup optionGroup) {
        configure(optionGroup)
        for (ArcType arcType : ArcType.values()) {
            optionGroup.addItem(arcType)
            optionGroup.setItemCaption(arcType, arcType.toString())
        }
    }

    static void fillBehavior(ComboBox comboBox) {
        configure(comboBox)
        for (Behavior behavior : Behavior.values()) {
            comboBox.addItem(behavior)
            comboBox.setItemCaption(behavior, behavior.toString())
        }
    }

    static void fillOccupation(ComboBox comboBox, List<Occupation> occupations) {
        fillOccupation(comboBox, occupations.toArray(new Occupation[occupations.size()]))
    }

    static void fillOccupation(ComboBox comboBox, Occupation... occupations) {
        configure(comboBox)
        for (Occupation occupation : occupations) {
            comboBox.addItem(occupation)
            comboBox.setItemCaption(occupation, occupation.toString())
        }
    }

    static void fillBrothers(ComboBox comboBox, List<String> listOfStrings) {
        fillBrothers(comboBox, listOfStrings.toArray(new String[listOfStrings.size()]))
    }

    static void fillBrothers(ComboBox comboBox, String... brothers) {
        configure(comboBox)
        for (String brother : brothers) {
            comboBox.addItem(brother)
        }
    }

    static void fillArcLenght(OptionGroup optionGroup, List<String> listOfStrings) {
        fillArcLenght(optionGroup, listOfStrings.toArray(new String[listOfStrings.size()]))
    }

    static void fillArcLenght(OptionGroup optionGroup, String... brothers) {
        configure(optionGroup)
        for (String brother : brothers) {
            optionGroup.addItem(brother)
        }
    }
}
