package edu.utesa.ronald.ingsoft.utils;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVPrinter;

import java.io.IOException;
import java.io.Writer;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;

/**
 * Created by ronald on 2/26/18.
 */
@SuppressWarnings("WeakerAccess")
public class CVSTool {

    private static final String CVS_PATH = "upload";
    private static final Character DELIMITER = '|';
    private static final String EXTENSION = ".csv";

    private static void writeCVS(String file, String[] header, List<Object> objectList) {
        try {
            Writer writer = Files.newBufferedWriter(Paths.get(CVS_PATH + "/" + file + EXTENSION));
            CSVPrinter csvPrinter = new CSVPrinter(writer, CSVFormat.DEFAULT.withHeader(header).withDelimiter(DELIMITER));
            for (Object o : objectList) {
                csvPrinter.printRecord(Arrays.asList(
                        // Aqui recojo los valores del objeto para escribirlos en el archivo.
                ));
            }
            csvPrinter.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static void readCVS(String file) {

    }
}
