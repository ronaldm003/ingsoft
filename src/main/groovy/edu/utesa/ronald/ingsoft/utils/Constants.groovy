package edu.utesa.ronald.ingsoft.utils

import com.google.gson.Gson
import com.google.gson.GsonBuilder
import edu.utesa.ronald.ingsoft.IngSoftSpringInit
import edu.utesa.ronald.ingsoft.utils.email.MailGunAPI
import groovy.transform.CompileStatic
import org.springframework.beans.factory.annotation.Value
import org.springframework.context.ApplicationListener
import org.springframework.context.ConfigurableApplicationContext
import org.springframework.context.event.ContextClosedEvent

import java.nio.file.Files
import java.nio.file.Paths
import java.sql.Date
import java.sql.Time
import java.text.SimpleDateFormat
import java.time.LocalDate

/**
 *  Created by ronald on 7/4/17.
 */
@CompileStatic
@Singleton
class Constants {

    @Value('${info.app.version}')
    String VERSION

    static final String RAYGUN_API_KEY = "tgpAY53JJi1mdwU0y6HcZQ=="

    static final String USER_LOGIN = "lenunow_user_login"

    public static final String EXTENSION = ".json"

    private static final String HOUR_FORMAT = "HH:mm:ss"

    public static final String DATE_FIND = "yyyy-MM-dd"

    public static final String KEY_DATE_FORMAT = "yyyyMMdd"

    private static final String DATE_HOUR_FIND = "yyyy-MM-dd HH:mm:ss"

    final static String DATE_FORMAT = DATE_FIND + " " + HOUR_FORMAT

    public static final SimpleDateFormat hourFormat = new SimpleDateFormat(HOUR_FORMAT)
    public static final SimpleDateFormat simpleDateFormat = new SimpleDateFormat(DATE_FIND)
    public static final SimpleDateFormat simpleDateFormatToFind

    final String MIN_HOUR = " 00:00:00"

    final String MAX_HOUR = " 23:59:59"

    static final String ROOT = "System"

    static final String ROOT_USERNAME = "root"

    private final Gson gson = new GsonBuilder().setDateFormat(DATE_FORMAT).setPrettyPrinting().create()

    private static final SimpleDateFormat keySimpleDateFormatDate = new SimpleDateFormat(KEY_DATE_FORMAT)
    private static final SimpleDateFormat simpleDateFormatDate = new SimpleDateFormat(DATE_FORMAT)
    static final SimpleDateFormat simpleDateFormatQuery = new SimpleDateFormat(DATE_HOUR_FIND)
    static final SimpleDateFormat simpleDateFormatToFind = new SimpleDateFormat(DATE_FIND)

    public ConfigurableApplicationContext applicationContext

    private MailGunAPI mailGunAPI = null

    void setApplicationContext(ConfigurableApplicationContext applicationContext) {
        this.applicationContext = applicationContext
        this.applicationContext.addApplicationListener(new ApplicationListener<ContextClosedEvent>() {
            @Override
            void onApplicationEvent(ContextClosedEvent event) {
                IngSoftSpringInit.stop()
            }
        })
        autoWiredClass(this)
    }

    void    autoWiredClass(Object objectToWired) {
        applicationContext.getAutowireCapableBeanFactory().autowireBean(objectToWired)
    }

    String stringify(Object object) {
        return gson.toJson(object)
    }

    final Object validRequest(String data, Class validClass) {
        return convert(data, validClass)
    }

    final <T> T convert(String data, Class<T> validClass) {
        if (data) {
            try {
                return gson.fromJson(data, validClass)
            } catch (Exception ignored) {
                println "Error de parseo: "
                println "Clase: " + validClass.toString()
                println "Data: " + data
                return null
            }
        }
        return null
    }

    MailGunAPI getMailGunAPI() {
        if (mailGunAPI == null) {
            mailGunAPI = new MailGunAPI()
        }
        return mailGunAPI
    }

//    static final String validationKeyGenerator(User user, Date date) {
//        return user.code.toString() + keySimpleDateFormatDate.format(date)
//    }


    final String showDate(Date date) {
        return simpleDateFormatDate.format(date)
    }

    final String showDate(java.util.Date date) {
        return simpleDateFormatDate.format(date)
    }

    final java.util.Date parseLessThanEqualDateQuery(java.util.Date date) {
        return simpleDateFormatQuery.parse(simpleDateFormatDate.format(date) + MAX_HOUR)
    }

    final java.util.Date parseGreaterEqualDateQuery(java.util.Date date) {
        return simpleDateFormatQuery.parse(simpleDateFormatDate.format(date) + MIN_HOUR)
    }

    final java.util.Date parseLessThanEqualDateQuery(java.util.Date date, Time starTime) {
        println "ddddd" + starTime
        return new SimpleDateFormat(DATE_FORMAT).parse(simpleDateFormatDate.format(date) + " " + starTime)
    }

    final java.util.Date parseGreaterEqualDateQuery(java.util.Date date, Time endTime) {
        println "rrrr" + endTime
        return new SimpleDateFormat(DATE_FORMAT).parse(simpleDateFormatDate.format(date) + " " + endTime)
    }

    String readJsonFile(String fileName) {
        return new String(Files.readAllBytes(Paths.get(getClass().getResource("/" + fileName + EXTENSION).toURI())))
    }

    static int getAge(java.util.Date birthDate) {
        Calendar birthDateCalendar = Calendar.getInstance()
        birthDateCalendar.setTime(birthDate)

        Calendar today = Calendar.getInstance()

        int diff_year = today.get(Calendar.YEAR) - birthDateCalendar.get(Calendar.YEAR)
        int diff_month = today.get(Calendar.MONTH) - birthDateCalendar.get(Calendar.MONTH)
        int diff_day = today.get(Calendar.DAY_OF_MONTH) - birthDateCalendar.get(Calendar.DAY_OF_MONTH)

        if (diff_month < 0 || (diff_month == 0 && diff_day < 0)) {
            diff_year = diff_year - 1
        }
        return diff_year
    }

    static int randBetween(int start, int end) {
        return start + (int) Math.round(Math.random() * (end - start))
    }

    static LocalDate localDateNow() {
        return LocalDate.now()
    }

    static java.util.Date toDate(LocalDate localDate) {
        if (!localDate) {
            return null
        }
        return Date.valueOf(localDate)
    }

    static LocalDate toLocalDate(java.util.Date date) {
        if (!date) {
            return localDateNow()
        }
        return new Date(date.time).toLocalDate()
    }
}
