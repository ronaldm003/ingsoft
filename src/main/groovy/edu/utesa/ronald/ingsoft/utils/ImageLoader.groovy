package edu.utesa.ronald.ingsoft.utils

import groovy.transform.CompileStatic

/**
 *  Created by ronald on03/18/17.
 */
@CompileStatic
@Singleton
class ImageLoader {

    private final String BASE = "/images/"

    private final String REPORT_LOGO = "Report-logo.png"
    private final String RAVESOFT = "RaveSoft-Logo.png"
    private final String INGSOFT = "utesa-logo.png"
    private final String USER = "user.png"


    URL getImage(ImageModel imageModel) {
        switch (imageModel) {
            case ImageModel.REPORT_LOGO:
                return getClass().getResource(BASE + REPORT_LOGO)
            case ImageModel.RAVESOFT:
                return getClass().getResource(BASE + RAVESOFT)
            case ImageModel.INGSOFT:
                return getClass().getResource(BASE + INGSOFT)
        }
        return null
    }

    InputStream getImageStream(ImageModel imageModel) {
        switch (imageModel) {
            case ImageModel.REPORT_LOGO:
                return getClass().getResourceAsStream(BASE + REPORT_LOGO)
            case ImageModel.RAVESOFT:
                return getClass().getResourceAsStream(BASE + RAVESOFT)
            case ImageModel.INGSOFT:
                return getClass().getResourceAsStream(BASE + INGSOFT)
        }
        return null
    }
}
