package edu.utesa.ronald.ingsoft.utils

import groovy.transform.CompileStatic

/**
 *  Created by ronald on 9/22/17.
 */
@CompileStatic
enum ImageModel {
    REPORT_LOGO, RAVESOFT, INGSOFT

    private static final String sREPORT_LOGO = "REPORT_LOGO"
    private static final String sRAVESOFT = "RAVESOFT"
    private static final String sINGSOFT_LOGO = "INGSOFT"

    @Override
    String toString() {
        switch (this) {
            case REPORT_LOGO:
                return sREPORT_LOGO
            case RAVESOFT:
                return sRAVESOFT
            case INGSOFT:
                return sINGSOFT_LOGO
        }
        return ""
    }
}
