package edu.utesa.ronald.ingsoft.utils

import com.vaadin.server.VaadinSession
import com.vaadin.ui.ComboBox
import groovy.transform.CompileStatic

/**
 *  Created by ronald on 7/4/17.
 */
@CompileStatic
@Singleton
class Languages {

    public final Locale ENGLISH = Locale.ENGLISH
    public final Locale SPANISH = new Locale("es")

    ResourceBundle getI18N() {
        switch (VaadinSession.current.locale) {
            case SPANISH:
                return new PropertyResourceBundle(new InputStreamReader(getClass().getResourceAsStream("/messages_es.properties"), "UTF-8"))
            case ENGLISH:
            default:
                return new PropertyResourceBundle(new InputStreamReader(getClass().getResourceAsStream("/messages.properties"), "UTF-8"))
        }
    }

    ResourceBundle getURLFileI18NReport() {
        switch (Locale.getDefault()) {
            case SPANISH:
                return new PropertyResourceBundle(getClass().getResourceAsStream("/messagesReport_es.properties"))
            case ENGLISH:
            default:
                return new PropertyResourceBundle(getClass().getResourceAsStream("/messagesReport.properties"))
        }
    }

    String getText(String key) {
        try {
            return getI18N().getString(key)
        } catch (Exception ignored) {
            return "[" + key + "]"
        }
    }

    Locale getDefault() {
        switch (VaadinSession.current.locale) {
            case SPANISH:
                return SPANISH
            case ENGLISH:
            default:
                return ENGLISH
        }
    }

    static void setDefault(Locale locale) {
        if (VaadinSession?.current) {
            VaadinSession.current.locale = locale
        }
    }

    String getMapLanguage(Locale language) {
        switch (language) {
            case SPANISH:
                return getText("language.spanish")
            case ENGLISH:
            default:
                return getText("language.english")
        }
    }

    List<Locale> values() {
        List<Locale> locales = new ArrayList<>()
        locales.add(ENGLISH)
        locales.add(SPANISH)
        return locales
    }

    String dayOfWeek(int day) {
        switch (day) {
            case Calendar.SUNDAY:
                return getText("window.week.sunday")
            case Calendar.MONDAY:
                return getText("window.week.monday")
            case Calendar.TUESDAY:
                return getText("window.week.tuesday")
            case Calendar.WEDNESDAY:
                return getText("window.week.wednesday")
            case Calendar.THURSDAY:
                return getText("window.week.thursday")
            case Calendar.FRIDAY:
                return getText("window.week.friday")
            case Calendar.SATURDAY:
                return getText("window.week.saturday")
        }
        return ""
    }

    final void fillWeekDays(ComboBox comboBox) {
        comboBox.removeAllItems()
        for (int i = 1; i < 8; i++) {
            comboBox.addItem(i)
            comboBox.setItemCaption(i, dayOfWeek(i))
        }
    }
}
