package edu.utesa.ronald.ingsoft.utils

import edu.utesa.ronald.ingsoft.domains.admin.*
import edu.utesa.ronald.ingsoft.domains.appointment.Appointment
import edu.utesa.ronald.ingsoft.domains.control.Patient
import edu.utesa.ronald.ingsoft.models.LoginManager
import edu.utesa.ronald.ingsoft.services.admin.CycleService
import edu.utesa.ronald.ingsoft.services.admin.StudentService
import edu.utesa.ronald.ingsoft.services.admin.SubjectService
import edu.utesa.ronald.ingsoft.services.admin.TeacherService
import edu.utesa.ronald.ingsoft.services.appointment.AppointmentService
import edu.utesa.ronald.ingsoft.services.control.PatientService
import groovy.transform.CompileStatic
import org.joda.time.DateTime
import org.springframework.beans.factory.annotation.Autowired

import java.time.LocalDate
import java.time.LocalTime

/**
 *  Created by ronald on 7/4/17.
 */
@CompileStatic
@Singleton
class ProcessAppointments {

    @Autowired
    private CycleService cycleService
    @Autowired
    private SubjectService subjectService
    @Autowired
    private StudentService studentService
    @Autowired
    private PatientService patientService
    @Autowired
    private TeacherService teacherService
    @Autowired
    private AppointmentService appointmentService

    private List<Date> availableDateList = new ArrayList<>()

    private void prepare() {
        Constants.instance.autoWiredClass(this)
    }

    void validateAppointments(LoginManager loginManager, Cycle cycle, Student student, Patient patient, Teacher teacher, List<Objective> objectiveList) {
        prepare()
        List<ScheduleDay> scheduleDayList = new ArrayList<>()
        for (Subject subject : student.listSubjects) {
            for (SubjectSchedule schedule : subjectService.refresh(subject.id, {
                it.listSubjectSchedule.size()
            }).listSubjectSchedule) {
                scheduleDayList.addAll(getDayOfTheWeek(schedule.day, cycle.startDate, cycle.endDate).collect({
                    new ScheduleDay(schedule, it)
                }))
            }
        }
        for (ScheduleDay scheduleDay : scheduleDayList) {
            for (Objective objective : objectiveList) {
                if (objective.procedureQuantity <= 0) {
                    continue
                }
                long timeAvailableTemp = scheduleDay.timeAvailable
                int objectiveMinutes = objective.averageTime.toLocalTime().getMinute()
                if (timeAvailableTemp >= objectiveMinutes) {
                    LocalTime time = scheduleDay.subjectSchedule.startTime.toLocalTime()
                    DateTime startDate = new DateTime(scheduleDay.date).withTime(time.hour, time.minute, time.second, 0);
                    DateTime dateEnd = startDate.plusMinutes(objectiveMinutes)
                    if (appointmentService.create(createAppointment(startDate.toDate(), dateEnd.toDate(), student, patient, teacher, objective), loginManager)) {
                        objective.procedureQuantity--
                        scheduleDay.timeConsumed += objectiveMinutes
                        println "bien"
                        ShowNotification.success()
                    }
                }
            }


        }
    }

    class ScheduleDay {
        SubjectSchedule subjectSchedule
        Date date
        long timeConsumed = 0

        ScheduleDay(SubjectSchedule subjectSchedule, Date date) {
            this.subjectSchedule = subjectSchedule
            this.date = date
        }

        long getTimeAvailable() {
            return TimeTool.getDifferenceMinutes(subjectSchedule.startTime, subjectSchedule.endTime) - timeConsumed
        }

    }

    private static Appointment createAppointment(Date start, Date end, Student student, Patient patient,
                                                 Teacher teacher, Objective objective) {
        Appointment appointment = new Appointment()
        appointment.teacher = teacher
        appointment.student = student
        appointment.patient = patient
        appointment.start = start
        appointment.end = end
        appointment.name = objective.description
        appointment.description = patient.toString()
        appointment.objective = objective
        return appointment
    }

    private static int getCycleBusinessWeeks(Date startDate, Date endDate) {
        return TimeTool.getDifferenceWeeks(startDate, endDate)
    }

    private static List<Date> getDayOfTheWeek(int day, Date startDate, Date endDate) {
        List<Date> dateList = new ArrayList<>()
        Calendar calendar1 = Calendar.getInstance();
        calendar1.setTime(startDate);
        Calendar calendar2 = Calendar.getInstance();
        calendar2.setTime(endDate);
        Calendar calendar3 = Calendar.getInstance();
        LocalDate startDateLocalDate = LocalDate.of(calendar1.get(Calendar.YEAR), calendar1.get(Calendar.MONTH) + 1, calendar1.get(Calendar.DAY_OF_MONTH));
        LocalDate endDateLocalDat = LocalDate.of(calendar2.get(Calendar.YEAR), calendar2.get(Calendar.MONTH) + 1, calendar2.get(Calendar.DAY_OF_MONTH));
        for (LocalDate date = startDateLocalDate; date.isBefore(endDateLocalDat); date = date.plusDays(1)) {
            Date date1 = Constants.instance.toDate(date)
            calendar3.setTime(date1)
            if (day.equals(calendar3.get(Calendar.DAY_OF_WEEK))) {
                dateList.add(calendar3.getTime())
            }
        }
        return dateList
    }

}
