package edu.utesa.ronald.ingsoft.utils

import com.vaadin.server.Page
import com.vaadin.ui.Notification
import groovy.transform.CompileStatic

/**
 *  Created by ronald on 9/8/17.
 */
@CompileStatic
class ShowNotification {

    static void notification(String caption, String description, Notification.Type type, String style) {
        Notification notification = new Notification(Languages.instance.getText(caption), Languages.instance.getText(description), type, true)
        if (style) {
            notification.setStyleName(style)
        }
        notification.show(Page.getCurrent())
    }

    static void notification(String caption, String description, Notification.Type type) {
        notification(caption, description, type, null)
    }

    static void success() {
        success(null)
    }

    static void success(String style) {
        notification("notification.success", "notification.success.operation", Notification.Type.TRAY_NOTIFICATION, style)
    }

    static void success(String description, String style) {
        notification("notification.success", description, Notification.Type.TRAY_NOTIFICATION, style)
    }

    static void error() {
        notification("notification.error", "notification.error.operation", Notification.Type.ERROR_MESSAGE)
    }

    static void error(String description) {
        notification("notification.error", description, Notification.Type.ERROR_MESSAGE)
    }

    static void warning(String description) {
        notification("notification.error", description, Notification.Type.WARNING_MESSAGE)
    }

    static void incomplete() {
        incomplete(null)
    }

    static void incomplete(String style) {
        notification("notification.incomplete", "notification.incomplete.operation", Notification.Type.TRAY_NOTIFICATION, style)
    }

    static void passwordMismatchError() {
        notification("notification.error", "notification.error.recover.password.mismatch", Notification.Type.ERROR_MESSAGE)
    }

    static void oldPasswordError() {
        notification("notification.error", "notification.error.old.password.mismatch", Notification.Type.ERROR_MESSAGE)
    }

    static void keyLostError() {
        notification("notification.error", "notification.error.recover.password.key", Notification.Type.ERROR_MESSAGE)
    }

    static void noInventorySlotAvailable() {
        notification("notification.error", "notification.error.no.inventory.slot.available", Notification.Type.ERROR_MESSAGE)
    }
}
