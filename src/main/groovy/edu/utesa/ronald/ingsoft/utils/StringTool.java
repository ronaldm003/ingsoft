package edu.utesa.ronald.ingsoft.utils;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.text.Normalizer;

/**
 * Created by ronald on 2/26/18.
 */
@SuppressWarnings("WeakerAccess")
public class StringTool {

    public static String onlyAscii(String string) {
        StringBuilder sb = new StringBuilder(string.length());
        string = Normalizer.normalize(string, Normalizer.Form.NFD);
        for (char c : string.toCharArray()) {
            if (c <= '\u007F') {
                sb.append(c);
            }
        }
        return sb.toString();
    }

    public static String reverse(CharSequence self) {
        return new StringBuilder(self).reverse().toString();
    }

    /**
     * Determine if a CharSequence can be parsed as a BigDecimal.
     *
     * @param self a CharSequence
     * @return true if the CharSequence can be parsed
     * @since 1.8.2
     */
    public static boolean isBigDecimal(CharSequence self) {
        return toBigDecimal(self.toString()) != null;
    }

    public static boolean isBigInteger(CharSequence self) {
        return toBigInteger(self.toString()) != null;
    }

    public static boolean isInteger(CharSequence self) {
        return toInteger(self.toString()) != null;
    }

    public static boolean isLong(CharSequence self) {
        return toLong(self.toString()) != null;
    }

    public static boolean isDouble(CharSequence self) {
        return toDouble(self.toString()) != null;
    }

    /**
     * Determine if a CharSequence can be parsed as a Number.
     * Synonym for 'isBigDecimal()'.
     *
     * @param self a CharSequence
     * @return true if the CharSequence can be parsed
     * @see #isBigDecimal(CharSequence)
     * @since 1.8.2
     */
    public static boolean isNumber(CharSequence self) {
        return isBigDecimal(self);
    }

    public static Double toDouble(String value) {
        try {
            return Double.parseDouble(value.trim());
        } catch (NumberFormatException nfe) {
            return null;
        }
    }

    public static Long toLong(String value) {
        try {
            return Long.parseLong(value.trim());
        } catch (NumberFormatException nfe) {
            return null;
        }
    }

    public static Integer toInteger(String value) {
        try {
            return Integer.parseInt(value.trim());
        } catch (NumberFormatException nfe) {
            return null;
        }
    }

    public static BigDecimal toBigDecimal(String value) {
        try {
            return new BigDecimal(value.trim());
        } catch (NumberFormatException nfe) {
            return null;
        }
    }

    public static BigInteger toBigInteger(String value) {
        try {
            return new BigInteger(value.trim());
        } catch (NumberFormatException nfe) {
            return null;
        }
    }

    /**
     * Returns CSS representation of the Color, e.g. #000000.
     */
    public static String toCSS(int rgb) {
        int value = 0xff000000 | rgb;
        int red = (value >> 16) & 0xFF;
        int green = (value >> 8) & 0xFF;
        int blue = (value >> 0) & 0xFF;
        int alpha = (value >> 24) & 0xff;

        String redString = Integer.toHexString(red);
        redString = redString.length() < 2 ? "0" + redString : redString;

        String greenString = Integer.toHexString(green);
        greenString = greenString.length() < 2 ? "0" + greenString
                : greenString;

        String blueString = Integer.toHexString(blue);
        blueString = blueString.length() < 2 ? "0" + blueString : blueString;

        return "#" + redString + greenString + blueString;
    }

}
