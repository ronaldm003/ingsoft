package edu.utesa.ronald.ingsoft.utils;

import edu.utesa.ronald.ingsoft.models.enums.FrequencyType;

import java.sql.Time;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.*;
import java.util.concurrent.TimeUnit;

/**
 * Created by ronald on 10/26/15.
 */
@SuppressWarnings("WeakerAccess")
public class TimeTool {

    private static final SimpleDateFormat simpleDateFormat = new SimpleDateFormat("HH:mm:ss");

    public static Date addTime(Date date, int type, int cant) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.add(type, cant);
        return calendar.getTime();
    }

    public static Date addDay(Date date, int quoteNumber) {
        return addTime(date, Calendar.DAY_OF_MONTH, quoteNumber);
    }

    public static Date addWeek(Date date, int quoteNumber) {
        return addTime(date, Calendar.DAY_OF_MONTH, 7 * quoteNumber);
    }

    public static Date addBiweekly(Date date, int quoteNumber) {
        return addTime(date, Calendar.DAY_OF_MONTH, 15 * quoteNumber);
    }

    public static Date addMonth(Date date, int quoteNumber) {
        return addTime(date, Calendar.MONTH, quoteNumber);
    }

    public static Date addYear(Date date, int quoteNumber) {
        return addTime(date, Calendar.YEAR, quoteNumber);
    }

    public static Date addFrequency(Date date, FrequencyType frequencyType, int quoteNumber) {
        if (quoteNumber <= 0) {
            return date;
        }
        switch (frequencyType) {
            case DAILY:
                return addDay(date, quoteNumber);
            case WEEKLY:
                return addWeek(date, quoteNumber);
            case BIWEEKLY:
                return addBiweekly(date, quoteNumber);
            case MONTHLY:
                return addMonth(date, quoteNumber);
            case ANNUAL:
                return addYear(date, quoteNumber);
        }
        return null;
    }

    public static Date addSecondsTODate(Date date, int seconds) {
        return addTime(date, Calendar.SECOND, seconds);
    }

    /**
     * Find diference 2 date on days
     */
    public static long getDifferenceDays(Date d1, Date d2) {
        long diff = d2.getTime() - d1.getTime();
        return TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS);
    }

    /**
     * Find difference  2 date on weeks
     */
    public static long getDifferenceWeeks(Date d1, Date d2) {
        Calendar calendar1 = Calendar.getInstance();
        calendar1.setTime(d1);
        Calendar calendar2 = Calendar.getInstance();
        calendar2.setTime(d2);
        LocalDate startDate = LocalDate.of(calendar1.get(Calendar.YEAR), calendar1.get(Calendar.MONTH), calendar1.get(Calendar.DAY_OF_WEEK));
        LocalDate endDate = LocalDate.of(calendar2.get(Calendar.YEAR), calendar2.get(Calendar.MONTH), calendar2.get(Calendar.DAY_OF_WEEK));
        return ChronoUnit.WEEKS.between(startDate, endDate);
    }

    public static boolean isDateMinor(Date dateStart, Date dateEnd) {
        SimpleDateFormat formatter = new SimpleDateFormat("MM-dd-yyyy");
        Calendar calendar1 = Calendar.getInstance();
        Calendar calendar2 = Calendar.getInstance();
        try {
            calendar1.setTime(formatter.parse(formatter.format(dateStart)));
            calendar2.setTime(formatter.parse(formatter.format(dateEnd)));
        } catch (ParseException ignored) {
        }
        return calendar1.compareTo(calendar2) < 0;
    }

    /**
     * Find diference 2 date on hours
     */
    public static long getDifferenceHours(Date d1, Date d2) {
        long diff = d2.getTime() - d1.getTime();
        return TimeUnit.HOURS.convert(diff, TimeUnit.MILLISECONDS);
    }

    /**
     * Find diference 2 date on minutes
     */
    public static long getDifferenceMinutes(Date d1, Date d2) {
        long diff = d2.getTime() - d1.getTime();
        return TimeUnit.MINUTES.convert(diff, TimeUnit.MILLISECONDS);
    }

    /**
     * Find diference 2 date on seconds
     */
    public static long getDifferenceSeconds(Date d1, Date d2) {
        long diff = d2.getTime() - d1.getTime();
        return TimeUnit.SECONDS.convert(diff, TimeUnit.MILLISECONDS);
    }

    /**
     * Compare a time with a date.
     * On case Time is null get 0 for normalize
     * <p>
     * Examples 1:
     * time = 11:22:50
     * date = 11:26:36
     * <p>
     * return -1
     * <p>
     * Example 2:
     * time = 11:22:50
     * date = 11:22:50
     * return 0
     * <p>
     * Example 3:
     * time = 14:22:50
     * date = 13:20:50
     * return 1
     *
     * @return -1 if time is less than date, 0 if are equals and 1 if time is more than date
     */
    public static int compareTimeOnDate(Time time, Date date) {
        if (time == null) {
            return 0;
        }
        Calendar calendar1 = Calendar.getInstance();
        Calendar calendar2 = Calendar.getInstance();
        calendar1.setTime(new Date(time.getTime()));
        calendar2.setTime(new Date(Time.valueOf(simpleDateFormat.format(date)).getTime()));
        return compareCalendars(calendar1, calendar2);
    }

    /**
     * Compare dates
     * Only time case
     *
     * @return -1 if time is less than date, 0 if are equals and 1 if time is more than date
     */
    public static int compareDateOnDate(String dateFormat, Date dateStart, Date dateEnd) {
        SimpleDateFormat formatter = new SimpleDateFormat(dateFormat);
        Calendar calendar1 = Calendar.getInstance();
        Calendar calendar2 = Calendar.getInstance();
        calendar1.setTime(new Date(Time.valueOf(formatter.format(dateStart)).getTime()));
        calendar2.setTime(new Date(Time.valueOf(formatter.format(dateEnd)).getTime()));
        return calendar1.compareTo(calendar2);
    }

    public static boolean equalsDate(String dateFormat, Date dateStart, Date dateEnd) {
        SimpleDateFormat formatter = new SimpleDateFormat(dateFormat);
        Calendar calendar1 = Calendar.getInstance();
        Calendar calendar2 = Calendar.getInstance();
        try {
            calendar1.setTime(formatter.parse(formatter.format(dateStart)));
            calendar2.setTime(formatter.parse(formatter.format(dateEnd)));
        } catch (ParseException ignored) {
        }
        return calendar1.compareTo(calendar2) == 0;
    }

    /**
     * Convert date to specific Time Zone
     * <p>
     * applyTimeZone("America/Nassau", "MM-dd-yyyy HH:mm:ss", new Date())
     *
     * @param timeZone   Example "America/Nassau"
     * @param dateFormat Example "MM-dd-yyyy HH:mm:ss"
     * @param date       Example new Date()
     * @return Date with TimeZone
     */
    public static Date applyTimeZone(String timeZone, String dateFormat, Date date) {
        SimpleDateFormat timeConverter = new SimpleDateFormat(dateFormat);
        timeConverter.setTimeZone(TimeZone.getTimeZone(timeZone));
        try {
            return new SimpleDateFormat(dateFormat).parse(timeConverter.format(date));
        } catch (ParseException ignored) {
            return null;
        }
    }

    /**
     * Get all Time Zone Available
     *
     * @return List with Time zone
     */
    public static List<String> getAllTimeZone() {
        return Arrays.asList(TimeZone.getAvailableIDs());
    }

    /**
     * Get day on week for a date.
     *
     * @return Int value on 1..7
     */
    public static int getWeekDayOnDate(Date date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        return calendar.get(Calendar.DAY_OF_WEEK);
    }

    public static boolean isEqualDayOnDates(Date date1, Date date2) {
        Calendar calendar1 = Calendar.getInstance();
        Calendar calendar2 = Calendar.getInstance();
        calendar1.setTime(date1);
        calendar2.setTime(date2);
        return calendar1.get(Calendar.DAY_OF_YEAR) == calendar2.get(Calendar.DAY_OF_YEAR);
    }

    public static boolean isDifferentDayOnDates(Date date1, Date date2) {
        Calendar calendar1 = Calendar.getInstance();
        Calendar calendar2 = Calendar.getInstance();
        calendar1.setTime(date1);
        calendar2.setTime(date2);
        return calendar1.get(Calendar.DAY_OF_YEAR) != calendar2.get(Calendar.DAY_OF_YEAR);
    }

    private static int compareCalendars(Calendar calendar1, Calendar calendar2) {
        return calendar1.compareTo(calendar2);
    }

    public static Time dateToTime(Date date) {
        return new Time(date.getTime());
    }

    public static int getAge(java.util.Date birthDate) {
        Calendar birthDateCalendar = Calendar.getInstance();
        birthDateCalendar.setTime(birthDate);

        Calendar today = Calendar.getInstance();

        int diff_year = today.get(Calendar.YEAR) - birthDateCalendar.get(Calendar.YEAR);
        int diff_month = today.get(Calendar.MONTH) - birthDateCalendar.get(Calendar.MONTH);
        int diff_day = today.get(Calendar.DAY_OF_MONTH) - birthDateCalendar.get(Calendar.DAY_OF_MONTH);

        if (diff_month < 0 || (diff_month == 0 && diff_day < 0)) {
            diff_year = diff_year - 1;
        }
        return diff_year;
    }
}
