package edu.utesa.ronald.ingsoft.utils;


import java.util.List;
import java.util.function.Consumer;

/**
 * For the touching of domain classes. with consent ;)
 */
public class TouchableDomain {

    /**
     * @param items list of items where touch will be called
     * @param touch meant to touch elements in the list so that they are fetched by gorm/hibernate
     */
    public static <T> void applyTouch(List<T> items, Consumer<T> touch) {
        if (touch != null && !items.isEmpty()) {
            for (T item : items) {
                touch.accept(item);
            }
        }
    }

    /**
     * @param item  item to touch
     * @param touch touch meant to touch elements in the list so that they are fetched by gorm/hibernate
     */
    public static <T> void applyTouch(T item, Consumer<T> touch) {
        if (item != null && touch != null) {
            touch.accept(item);
        }
    }
}
