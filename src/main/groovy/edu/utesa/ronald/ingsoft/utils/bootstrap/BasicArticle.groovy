package edu.utesa.ronald.ingsoft.utils.bootstrap

import edu.utesa.ronald.ingsoft.domains.control.Article
import edu.utesa.ronald.ingsoft.services.control.ArticleService
import edu.utesa.ronald.ingsoft.utils.Constants
import groovy.transform.CompileStatic
import org.springframework.beans.factory.annotation.Autowired

import java.security.SecureRandom

/**
 *  Created by ronald on 7/8/17.
 */
@CompileStatic
class BasicArticle {

    @Autowired
    private ArticleService articleService

    BasicArticle() {
        Constants.instance.autoWiredClass(this)
    }

    void insert() {
        for (String articles : getAllArticles()) {
            create(articles)
        }
    }

    private Article create(String description) {
        Article article = articleService.byDescription(description)
        if (!article) {
            article = new Article()
            article.code = articleService.findMaxCode()
            article.description = description
            article.quantity = new SecureRandom().nextInt(500)
            article.punceo = new SecureRandom().nextInt(50)
            article.cost = new SecureRandom().nextInt(1000).toBigDecimal()
            return articleService.bootStrap(article)
        }
        return article
    }

    private static HashSet<String> getAllArticles() {
        HashSet<String> articleList = new HashSet<>()
        articleList.add("Metapex")
        articleList.add("limas niti")
        articleList.add("EDTA")
        articleList.add("explorador")
        articleList.add("espejo")
        articleList.add("pinza")
        articleList.add("sonda periodontal")
        articleList.add("fresas de operatoria")
        articleList.add("fresas de pulido")
        articleList.add("espátulas de resina #3")
        articleList.add("espátulas de resina #5")
        articleList.add("espátulas de resina #7")
        articleList.add("puntas de siliconas para pulido")
        articleList.add("banda matriz")
        articleList.add("banda celuloide")
        articleList.add("grabado acido hortofosforico 37%")
        articleList.add("adhesivo")
        articleList.add("clorhexidina")
        articleList.add("pasta de pulido")
        articleList.add("dicalero")
        articleList.add("papel articular")
        articleList.add("resina")
        articleList.add("micro brush")
        articleList.add("dique de goma")
        articleList.add("grapas")
        articleList.add("perforadora")
        articleList.add("porta grapas")
        articleList.add("arco de young")
        articleList.add("protector gingival")
        articleList.add("curetas 1-2")
        articleList.add("curetas 3-4")
        articleList.add("curetas 5-6")
        articleList.add("curetas 7-8")
        articleList.add("curetas 9-10")
        articleList.add("curetas 11-12")
        articleList.add("curetas 13-14")
        articleList.add("jakets")
        articleList.add("cavitron")
        articleList.add("sonda periodontal carolina del norte")
        articleList.add("sonda nabers (para furca)")
        articleList.add("tijera")
        articleList.add("porta bisturí")
        articleList.add("bisturí")
        articleList.add("aguja")
        articleList.add("hilo")
        articleList.add("sindesmotomo")
        articleList.add("legra")
        articleList.add("elevadores")
        articleList.add("forceps")
        articleList.add("pinza mosquito")
        articleList.add("porta agujas")
        articleList.add("separadores de mimesota")
        articleList.add("paños quirúrgico")
        articleList.add("porta carpule")
        articleList.add("hipoclorito")
        articleList.add("suero")
        articleList.add("Cureta alveolar")
        articleList.add("lima de hueso")
        articleList.add("turbina")
        articleList.add("micromotor")
        articleList.add("pieza de mano")
        articleList.add("Gasas")
        articleList.add("algodón")
        articleList.add("anestesia (carpule)")
        articleList.add("anestesia tópica")
        articleList.add("Jeringas hipodérmica")
        articleList.add("agujas de irrigacion lateral")
        return articleList
    }
}
