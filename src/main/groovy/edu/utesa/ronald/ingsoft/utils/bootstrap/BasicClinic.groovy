package edu.utesa.ronald.ingsoft.utils.bootstrap

import edu.utesa.ronald.ingsoft.domains.control.Inventory
import edu.utesa.ronald.ingsoft.domains.location.Clinic
import edu.utesa.ronald.ingsoft.domains.location.Location
import edu.utesa.ronald.ingsoft.services.control.EquipmentService
import edu.utesa.ronald.ingsoft.services.location.ClinicService
import edu.utesa.ronald.ingsoft.utils.Constants
import groovy.transform.CompileStatic
import org.springframework.beans.factory.annotation.Autowired

/**
 *  Created by ronald on 7/8/17.
 */
@CompileStatic
class BasicClinic {

    @Autowired
    private ClinicService clinicService
    @Autowired
    private EquipmentService equipmentService

    BasicInventory basicInventory

    Clinic clinicCentroMedico
    Clinic clinicPlazaAlejo

    BasicClinic() {
        Constants.instance.autoWiredClass(this)
    }

    void insert(BasicInventory basicInventory) {
        this.basicInventory = basicInventory
        clinicCentroMedico = create(new Long(1), "Centro Medico Cibao", null, basicInventory.inventoryCentroMedico)
        clinicPlazaAlejo = create(new Long(2), "Plaza Alejo", null, basicInventory.inventoryPlazaAlejo)
    }

    private Clinic create(Long code, String description, Location location, Inventory inventory) {
        Clinic clinic = clinicService.byDescription(description)
        if (!clinic) {
            clinic = new Clinic()
            clinic.code = code
            clinic.description = description
            clinic.location = location
            clinic.inventory = inventory
            clinic.listEquipments = equipmentService.list(true).toSet()
            return clinicService.bootStrap(clinic)
        }
        return clinic
    }
}
