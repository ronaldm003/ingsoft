package edu.utesa.ronald.ingsoft.utils.bootstrap

import edu.utesa.ronald.ingsoft.domains.admin.Cycle
import edu.utesa.ronald.ingsoft.domains.admin.Subject
import edu.utesa.ronald.ingsoft.services.admin.CycleService
import edu.utesa.ronald.ingsoft.utils.Constants
import groovy.transform.CompileStatic
import org.springframework.beans.factory.annotation.Autowired

/**
 *  Created by ronald on 7/8/17.
 */
@CompileStatic
class BasicCycle {

    @Autowired
    private CycleService cycleService

    private Calendar calendarInit = new GregorianCalendar()
    private Calendar calendarEnd = new GregorianCalendar()

    Cycle cycle2_2018
    Cycle cycle3_2018

    BasicCycle() {
        Constants.instance.autoWiredClass(this)
    }

    void insert(BasicSubject basicSubject) {
        calendarInit.set(2018, Calendar.MAY, 16)
        calendarEnd.set(2018, Calendar.AUGUST, 23)
        cycle2_2018 = create("2-2018", "Ciclo Mayo-Agosto", calendarInit.getTime(), calendarEnd.getTime(), basicSubject.getSubjectList())
        calendarInit.set(2018, Calendar.SEPTEMBER, 16)
        calendarEnd.set(2018, Calendar.DECEMBER, 23)
        cycle3_2018 = create("3-2018", "Ciclo Sept-Diciembre", calendarInit.getTime(), calendarEnd.getTime(), basicSubject.getSubjectList())
    }

    private Cycle create(String code, String description, Date startDate, Date endDate, List<Subject> subjectList) {
        Cycle cycle = cycleService.byDescription(description)
        if (!cycle) {
            cycle = new Cycle()
            cycle.code = code
            cycle.description = description
            cycle.startDate = startDate
            cycle.endDate = endDate
            cycle.listSubjects = subjectList.toSet()
            return cycleService.bootStrap(cycle)
        }
        return cycle
    }
}
