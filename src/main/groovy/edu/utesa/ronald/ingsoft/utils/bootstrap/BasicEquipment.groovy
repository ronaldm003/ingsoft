package edu.utesa.ronald.ingsoft.utils.bootstrap

import edu.utesa.ronald.ingsoft.domains.control.Equipment
import edu.utesa.ronald.ingsoft.services.control.EquipmentService
import edu.utesa.ronald.ingsoft.utils.Constants
import groovy.transform.CompileStatic
import org.springframework.beans.factory.annotation.Autowired

/**
 *  Created by ronald on 7/8/17.
 */
@CompileStatic
class BasicEquipment {

    @Autowired
    private EquipmentService equipmentService

    Equipment equipmentLocalizadorApical
    Equipment equipmentCavitron

    BasicEquipment() {
        Constants.instance.autoWiredClass(this)
    }

    void insert() {
        equipmentLocalizadorApical = create("Localizador Apical", "")
        equipmentCavitron = create("Cavitron", "")
    }

    private Equipment create(String name, String description) {
        Equipment equipment = equipmentService.byName(description)
        if (!equipment) {
            equipment = new Equipment()
            equipment.name = name
            equipment.description = description
            return equipmentService.bootStrap(equipment)
        }
        return equipment
    }
}
