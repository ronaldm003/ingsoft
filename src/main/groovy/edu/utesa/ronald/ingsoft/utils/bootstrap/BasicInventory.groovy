package edu.utesa.ronald.ingsoft.utils.bootstrap

import edu.utesa.ronald.ingsoft.domains.control.Inventory
import edu.utesa.ronald.ingsoft.services.control.ArticleService
import edu.utesa.ronald.ingsoft.services.control.InventoryService
import edu.utesa.ronald.ingsoft.utils.Constants
import groovy.transform.CompileStatic
import org.springframework.beans.factory.annotation.Autowired

/**
 *  Created by ronald on 7/8/17.
 */
@CompileStatic
class BasicInventory {

    @Autowired
    private InventoryService inventoryService
    @Autowired
    private ArticleService articleService

    Inventory inventoryCentroMedico
    Inventory inventoryPlazaAlejo

    BasicInventory() {
        Constants.instance.autoWiredClass(this)
    }

    void insert() {
        inventoryCentroMedico = create("Centro Medico Cibao")
        inventoryPlazaAlejo = create("Plaza Alejo")
    }

    private Inventory create(String description) {
        Inventory inventory = inventoryService.byName(description)
        if (!inventory) {
            inventory = new Inventory()
            inventory.description = description
            inventory.listArticles = articleService.list(true).toSet()
            return inventoryService.bootStrap(inventory)
        }
        return inventory
    }
}
