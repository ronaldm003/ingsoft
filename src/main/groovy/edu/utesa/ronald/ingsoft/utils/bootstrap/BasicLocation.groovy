package edu.utesa.ronald.ingsoft.utils.bootstrap

import edu.utesa.ronald.ingsoft.domains.location.Location
import edu.utesa.ronald.ingsoft.services.location.LocationService
import edu.utesa.ronald.ingsoft.utils.Constants
import groovy.transform.CompileStatic
import org.springframework.beans.factory.annotation.Autowired

/**
 *  Created by ronald on 7/8/17.
 */
@CompileStatic
class BasicLocation {

    @Autowired
    private LocationService locationService

    Location locationGeneric

    BasicLocation() {
        Constants.instance.autoWiredClass(this)
    }

    void insert() {
        locationGeneric = create(1l, "Generica", 1l)
        create(2l, "24 de Abril", 39l)
        create(3l, "30 de Mayo", 39l)
        create(4l, "Altos de Arroyo Hondo", 39l)
        create(5l, "Arroyo Manzano", 39l)
        create(6l, "Atala", 39l)
        create(7l, "Bella Vista", 39l)
        create(8l, "Buenos Aires", 39l)
        create(9l, "Cacique", 39l)
        create(223l, "Villa Magante", 15l)
        create(224l, "Veragua", 15l)
    }

    private Location create(Long code, String description, Long city) {
        Location location = locationService.byDescription(description)
        if (!location) {
            location = new Location()
            location.code = locationService.findMaxCode()
            location.description = description
            return locationService.bootStrap(location)
        }
        return location
    }
}
