package edu.utesa.ronald.ingsoft.utils.bootstrap

import edu.utesa.ronald.ingsoft.domains.admin.Objective
import edu.utesa.ronald.ingsoft.services.admin.ObjectiveService
import edu.utesa.ronald.ingsoft.utils.Constants
import groovy.transform.CompileStatic
import org.springframework.beans.factory.annotation.Autowired

import java.security.SecureRandom
import java.sql.Time

/**
 *  Created by ronald on 7/8/17.
 */
@CompileStatic
class BasicObjective {

    @Autowired
    private ObjectiveService objectiveService

    private Calendar calendar = new GregorianCalendar()

    Objective practicaYReporteEscrito
    Objective tomaDeImpresionYModelo
    Objective entregaDeFicha
    Objective diagnosticoPaciente
    Objective profixalisPaciente
    Objective detartaje
    Objective practicaAnestesia
    Objective quizNervioTrigéminodeAnestesia
    Objective diagnosticoPacienteNuevo
    Objective profixalisPaciente2
    Objective detartaje2
    Objective resinasCLI
    Objective resinasClII
    Objective resinasClIII
    Objective resinasClIV
    Objective resinasClV

    BasicObjective() {
        Constants.instance.autoWiredClass(this)
    }

    void insert() {
        calendar.set(new SecureRandom().nextInt(9999),
                new SecureRandom().nextInt(12),
                new SecureRandom().nextInt(28))
        practicaYReporteEscrito = create("Practica y reporte escrito de radiografía.", calendar.getTime(), 5, null)
        tomaDeImpresionYModelo = create("Toma de impresión, y modelo de estudio.", calendar.getTime(), 2, null)
        entregaDeFicha = create("Entrega de ficha completa. ", calendar.getTime(), 1, null)
        diagnosticoPaciente = create("Diagnostico  en paciente ", calendar.getTime(), 1, null)
        profixalisPaciente = create("Profilaxis en paciente", calendar.getTime(), 1, null)
        detartaje = create("Detartraje", calendar.getTime(), 1, null)
        practicaAnestesia = create("Practica de anestesia", calendar.getTime(), 5, null)
        quizNervioTrigéminodeAnestesia = create("Quiz Nervio Trigémino - de Anestesia", calendar.getTime(), 1, null)
        diagnosticoPacienteNuevo = create("Diagnostico en pacientes nuevos ", calendar.getTime(), 2, null)
        profixalisPaciente2 = create("Profilaxis", calendar.getTime(), 2, null)
        detartaje2 = create("Detartraje", calendar.getTime(), 1, null)
        resinasCLI = create("Resinas CL  I  ", calendar.getTime(), 8, null)
        resinasClII = create("resinasClII ", calendar.getTime(), 2, null)
        resinasClIII = create("resinasClIII ", calendar.getTime(), 2, null)
        resinasClIV = create("resinasClIV ", calendar.getTime(), 2, null)
        resinasClV = create("resinasClV ", calendar.getTime(), 2, null)
        for (String objective : getAllObjectives()) {
            create(objective)
        }
    }

    protected List<Objective> getObjectiveList() {
        List<Objective> objectiveList = new ArrayList<>()
        objectiveList.add(practicaYReporteEscrito)
        objectiveList.add(tomaDeImpresionYModelo)
        objectiveList.add(entregaDeFicha)
        objectiveList.add(diagnosticoPaciente)
        objectiveList.add(profixalisPaciente)
        objectiveList.add(detartaje)
        objectiveList.add(practicaAnestesia)
        objectiveList.add(quizNervioTrigéminodeAnestesia)
        objectiveList.add(diagnosticoPacienteNuevo)
        objectiveList.add(profixalisPaciente2)
        objectiveList.add(detartaje2)
        objectiveList.add(resinasCLI)
        objectiveList.add(resinasClII)
        objectiveList.add(resinasClIII)
        objectiveList.add(resinasClIV)
        objectiveList.add(resinasClV)
        return objectiveList
    }

    private Objective create(String description) {
        Objective objective = objectiveService.byDescription(description)
        if (!objective) {
            calendar.set(new SecureRandom().nextInt(9999),
                    new SecureRandom().nextInt(12),
                    new SecureRandom().nextInt(28))
            objective = new Objective()
            objective.description = description
            objective.limitDate = calendar.getTime()
            objective.procedureQuantity = new SecureRandom().nextInt(5)
            return objectiveService.bootStrap(objective)
        }
        return objective
    }

    private Objective create(String description, Date limitDate, Long procedureQuantity, Time averageTime) {
        Objective objective = objectiveService.byDescription(description)
        if (!objective) {
            objective = new Objective()
            objective.description = description
            objective.limitDate = limitDate
            objective.procedureQuantity = procedureQuantity
            objective.averageTime = averageTime
            return objectiveService.bootStrap(objective)
        }
        return objective
    }

    private static HashSet<String> getAllObjectives() {
        HashSet<String> objectivesHashSet = new HashSet<String>()
        objectivesHashSet.add("Operatoria")
        objectivesHashSet.add("Hexodoncia")
        objectivesHashSet.add("Endodóncia")
        objectivesHashSet.add("Prótesis")
        objectivesHashSet.add("Profiláxis")
        objectivesHashSet.add("Diagnostico")
        objectivesHashSet.add("Tearapia de Fluor")
        objectivesHashSet.add("Instrumentos de Exodoncia")
        objectivesHashSet.add("Detartaje")
        objectivesHashSet.add("Modelo de estudio")
        objectivesHashSet.add("Blanqueamiento")
        return objectivesHashSet
    }
}
