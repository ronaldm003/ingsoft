package edu.utesa.ronald.ingsoft.utils.bootstrap

import edu.utesa.ronald.ingsoft.domains.utils.Occupation
import edu.utesa.ronald.ingsoft.services.utils.OccupationService
import edu.utesa.ronald.ingsoft.utils.Constants
import groovy.transform.CompileStatic
import org.springframework.beans.factory.annotation.Autowired

/**
 *  Created by ronald on 7/8/17.
 */
@CompileStatic
class BasicOccupation {

    @Autowired
    private OccupationService occupationService

    Occupation occupationGeneric

    BasicOccupation() {
        Constants.instance.autoWiredClass(this)
    }

    void insert() {
        occupationGeneric = create("Generica")
        create("Doctor")
        create("Ingeniero")
        create("Abogado")
        create("estudiante")
    }

    private Occupation create(String description) {
        Occupation occupation = occupationService.byDescription(description)
        if (!occupation) {
            occupation = new Occupation()
            occupation.description = description
            return occupationService.bootStrap(occupation)
        }
        return occupation
    }
}
