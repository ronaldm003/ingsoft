package edu.utesa.ronald.ingsoft.utils.bootstrap

import edu.utesa.ronald.ingsoft.domains.security.Param
import edu.utesa.ronald.ingsoft.models.params.ParamValueType
import edu.utesa.ronald.ingsoft.services.security.ParamService
import edu.utesa.ronald.ingsoft.utils.Constants
import edu.utesa.ronald.ingsoft.utils.params.ParamType
import groovy.transform.CompileStatic
import org.springframework.beans.factory.annotation.Autowired

/**
 *  Created by ronald on03/18/17.
 */
@CompileStatic
class BasicParams {

    @Autowired
    private ParamService paramService

    BasicParams() {
        Constants.instance.autoWiredClass(this)
    }

    void insert() {
        for (Integer paramType : ParamType.values()) {
            create(paramType, ParamType.getParamValueType(paramType), ParamType.getCaption(paramType),
                    ParamType.getDescription(paramType), ParamType.getValue(paramType), ParamType.getChangeRoot(paramType))
        }
    }

    private Param create(Integer code, ParamValueType paramValueType, String name, String description, String value, Boolean changeRoot) {
        Param param = paramService.byCode(code)
        if (!param) {
            param = new Param()
            param.setCode(code)
            param.setParamValueType(paramValueType)
            param.setName(name)
            param.setDescription(description)
            param.setValue(value)
            param.setChangeRoot(changeRoot)
            return paramService.bootStrap(param)
        }
        return param
    }

}
