package edu.utesa.ronald.ingsoft.utils.bootstrap

import edu.utesa.ronald.ingsoft.domains.control.Patient
import edu.utesa.ronald.ingsoft.domains.security.Person
import edu.utesa.ronald.ingsoft.services.control.PatientService
import edu.utesa.ronald.ingsoft.utils.Constants
import groovy.transform.CompileStatic
import org.springframework.beans.factory.annotation.Autowired

/**
 *  Created by ronald on 7/8/17.
 */
@CompileStatic
class BasicPatient {

    @Autowired
    private PatientService patientService

    private BasicPerson basicPerson

    Patient patient1

    BasicPatient() {
        Constants.instance.autoWiredClass(this)
    }

    void insert(BasicPerson basicPerson) {
        this.basicPerson = basicPerson
        patient1 = create(new Long(1), basicPerson.personPatient)
    }

    private Patient create(Long code, Person person) {
        Patient patient = patientService.byCode(code)
        if (!patient) {
            patient = new Patient()
            patient.code = code
            patient.person = person
            return patientService.bootStrap(patient)
        }
        return patient
    }
}
