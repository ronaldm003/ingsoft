package edu.utesa.ronald.ingsoft.utils.bootstrap

import edu.utesa.ronald.ingsoft.domains.location.Location
import edu.utesa.ronald.ingsoft.domains.security.Person
import edu.utesa.ronald.ingsoft.domains.utils.Occupation
import edu.utesa.ronald.ingsoft.models.enums.Gender
import edu.utesa.ronald.ingsoft.models.enums.Nationality
import edu.utesa.ronald.ingsoft.models.enums.WarrantType
import edu.utesa.ronald.ingsoft.services.location.LocationService
import edu.utesa.ronald.ingsoft.services.security.PersonService
import edu.utesa.ronald.ingsoft.utils.Constants
import groovy.transform.CompileStatic
import org.springframework.beans.factory.annotation.Autowired

/**
 *  Created by ronald on 7/8/17.
 */
@CompileStatic
class BasicPerson {

    @Autowired
    private PersonService personService
    @Autowired
    private LocationService locationService

    private BasicLocation basicLocation
    private BasicOccupation basicOccupation

    Person personRoot
    Person personPatient
    Person personStudent
    Person personRonald
    Person personTeacher

    Calendar calendar = new GregorianCalendar()

    BasicPerson() {
        Constants.instance.autoWiredClass(this)
        calendar.set(1991, 1, 3)
    }

    void insert(BasicLocation basicLocation, BasicOccupation basicOccupation) {
        this.basicLocation = basicLocation
        this.basicOccupation = basicOccupation
        personRoot = createPerson(new Long(1), "Root", "Root",
                "000-000-0000", "000-000-0000", "", "", calendar.getTime(), new Date(), WarrantType.ID, Nationality.DOMINICAN,
                Gender.MAN, basicLocation.locationGeneric, basicOccupation.occupationGeneric)
        personPatient = createPerson(new Long(2), "Patient", "Patient",
                "000-000-0000", "000-000-0000", "", "", calendar.getTime(), new Date(), WarrantType.ID, Nationality.DOMINICAN,
                Gender.MAN, basicLocation.locationGeneric, basicOccupation.occupationGeneric)
        personStudent = createPerson(new Long(3), "Student", "Student",
                "000-000-0000", "000-000-0000", "", "", calendar.getTime(), new Date(), WarrantType.ID, Nationality.DOMINICAN,
                Gender.MAN, basicLocation.locationGeneric, basicOccupation.occupationGeneric)
        personRonald = createPerson(new Long(5), "Ronald", "Marmol",
                "000-000-0000", "000-000-0000", "", "402-3434234-8", calendar.getTime(), new Date(), WarrantType.ID, Nationality.DOMINICAN,
                Gender.MAN, basicLocation.locationGeneric, basicOccupation.occupationGeneric)
        personTeacher = createPerson(new Long(4), "Teacher", "Teacher",
                "000-000-0000", "000-000-0000", "", "", calendar.getTime(), new Date(), WarrantType.ID, Nationality.DOMINICAN,
                Gender.MAN, basicLocation.locationGeneric, basicOccupation.occupationGeneric)
    }

    private Person createPerson(Long idUser, String name, String lastName, String phone, String cellphone, String email,
                                String identification, Date birthDate, Date ingressDate, WarrantType warrantType,
                                Nationality nationality, Gender gender, Location location, Occupation occupation) {
        Person person = personService.byName(name)
        if (!person) {
            person = new Person()
            person.code = idUser
            person.name = name
            person.lastName = lastName
            person.birthDate = birthDate
            person.ingressDate = ingressDate
            person.email = email
            person.phone = phone
            person.cellphone = cellphone
            person.warrant = identification
            person.warrantType = warrantType
            person.nationality = nationality
            person.gender = gender
            person.location = location
            person.occupation = occupation
            return personService.bootStrap(person)
        }
        return person
    }

}
