package edu.utesa.ronald.ingsoft.utils.bootstrap

import edu.utesa.ronald.ingsoft.domains.admin.Student
import edu.utesa.ronald.ingsoft.domains.admin.Subject
import edu.utesa.ronald.ingsoft.domains.security.Person
import edu.utesa.ronald.ingsoft.services.admin.StudentService
import edu.utesa.ronald.ingsoft.utils.Constants
import groovy.transform.CompileStatic
import org.springframework.beans.factory.annotation.Autowired

/**
 *  Created by ronald on 7/8/17.
 */
@CompileStatic
class BasicStudent {

    @Autowired
    private StudentService studentService

    private BasicPerson basicPerson

    Student student1

    BasicStudent() {
        Constants.instance.autoWiredClass(this)
    }

    void insert(BasicPerson basicPerson, BasicSubject basicSubject) {
        this.basicPerson = basicPerson
        student1 = create("2-09-3423", basicPerson.personStudent, basicSubject.getStudentSubjectList())
    }

    private Student create(String enrollment, Person person, List<Subject> subjectList) {
        Student student = studentService.byEnrollment(enrollment)
        if (!student) {
            student = new Student()
            student.enrollment = enrollment
            student.person = person
            student.listSubjects = subjectList.toSet()
            return studentService.bootStrap(student)
        }
        return student
    }
}
