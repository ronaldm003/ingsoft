package edu.utesa.ronald.ingsoft.utils.bootstrap

import edu.utesa.ronald.ingsoft.domains.admin.Objective
import edu.utesa.ronald.ingsoft.domains.admin.Subject
import edu.utesa.ronald.ingsoft.domains.admin.SubjectSchedule
import edu.utesa.ronald.ingsoft.services.admin.SubjectScheduleService
import edu.utesa.ronald.ingsoft.services.admin.SubjectService
import edu.utesa.ronald.ingsoft.utils.Constants
import groovy.transform.CompileStatic
import org.springframework.beans.factory.annotation.Autowired

import java.sql.Time

/**
 *  Created by ronald on 7/8/17.
 */
@CompileStatic
class BasicSubject {

    @Autowired
    private SubjectService subjectService
    @Autowired
    private SubjectScheduleService subjectScheduleService

    Subject ODO020001
    Subject ODO020002
    Subject ODO020004
    Subject ODO020005
    Subject ODO020010
    Subject ODO020011

    SubjectSchedule L700TO1045
    SubjectSchedule MI700TO1045
    SubjectSchedule MA700TO1045
    SubjectSchedule J700TO1045
    SubjectSchedule J100TO1645
    SubjectSchedule V100TO1645

    List<SubjectSchedule> LMI700TO1045_List = new ArrayList<>()
    List<SubjectSchedule> MAJI700TO1045_List = new ArrayList<>()
    List<SubjectSchedule> JV100TO1645_List = new ArrayList<>()

    BasicSubject() {
        Constants.instance.autoWiredClass(this)
    }

    void insertSubjectSchedule() {
        L700TO1045 = createSchedule(Calendar.MONDAY, new Time(7, 00, 00), new Time(10, 45, 00))
        MI700TO1045 = createSchedule(Calendar.WEDNESDAY, new Time(7, 00, 00), new Time(10, 45, 00))
        LMI700TO1045_List.add(L700TO1045)
        LMI700TO1045_List.add(MI700TO1045)
        MA700TO1045 = createSchedule(Calendar.TUESDAY, new Time(7, 00, 00), new Time(10, 45, 00))
        J700TO1045 = createSchedule(Calendar.THURSDAY, new Time(7, 00, 00), new Time(10, 45, 00))
        MAJI700TO1045_List.add(MA700TO1045)
        MAJI700TO1045_List.add(J700TO1045)
        J100TO1645 = createSchedule(Calendar.THURSDAY, new Time(13, 00, 00), new Time(16, 45, 00))
        V100TO1645 = createSchedule(Calendar.FRIDAY, new Time(13, 00, 00), new Time(16, 45, 00))
        JV100TO1645_List.add(J100TO1645)
        JV100TO1645_List.add(V100TO1645)
    }

    void insertSubject(BasicObjective basicObjective) {
        insertSubjectSchedule()
        ODO020001 = create("ODO020001", "CLINICA ODONTOLOGICA BASICA II", new Long(0), new Long(10), new Long(3), LMI700TO1045_List, basicObjective.getObjectiveList())
        ODO020002 = create("ODO020002", "CLINICA ODONTOLOGICA BASICA II", new Long(0), new Long(10), new Long(1), LMI700TO1045_List, basicObjective.getObjectiveList())
        ODO020004 = create("ODO020004", "CLINICA ODONTOLOGICA BASICA II", new Long(0), new Long(10), new Long(1), MAJI700TO1045_List, basicObjective.getObjectiveList())
        ODO020005 = create("ODO020005", "CLINICA ODONTOLOGICA BASICA II", new Long(0), new Long(10), new Long(1), MAJI700TO1045_List, basicObjective.getObjectiveList())
        ODO020010 = create("ODO020010", "CLINICA ODONTOLOGICA BASICA II", new Long(0), new Long(10), new Long(1), JV100TO1645_List, basicObjective.getObjectiveList())
        ODO020011 = create("ODO020011", "CLINICA ODONTOLOGICA BASICA II", new Long(0), new Long(10), new Long(1), JV100TO1645_List, basicObjective.getObjectiveList())
    }

    private Subject create(String code, String description, Long credits, Long teoricHours, Long practicalHours, List<SubjectSchedule> subjectScheduleList, List<Objective> objectiveList) {
        Subject subject = subjectService.byCode(code)
        if (!subject) {
            subject = new Subject()
            subject.code = code
            subject.description = description
            subject.credits = credits
            subject.teoricHours = teoricHours
            subject.practicalHours = practicalHours
            subject.listSubjectSchedule = subjectScheduleList.toSet()
            subject.listObjectives = objectiveList.toSet()
            return subjectService.bootStrap(subject)
        }
        return subject
    }

    private SubjectSchedule createSchedule(int day, Time startTime, Time endTime) {
        SubjectSchedule subjectSchedule = subjectScheduleService.byDayAndStartTimeAndEndTime(day, startTime, endTime)
        if (!subjectSchedule) {
            subjectSchedule = new SubjectSchedule()
            subjectSchedule.day = day
            subjectSchedule.startTime = startTime
            subjectSchedule.endTime = endTime
            return subjectScheduleService.bootStrap(subjectSchedule)
        }
        return subjectSchedule
    }

    protected List<Subject> getSubjectList() {
        List<Subject> subjectList = new ArrayList<>()
        subjectList.add(ODO020001)
        subjectList.add(ODO020002)
        subjectList.add(ODO020004)
        subjectList.add(ODO020005)
        subjectList.add(ODO020010)
        subjectList.add(ODO020011)
        return subjectList
    }

    protected List<Subject> getStudentSubjectList() {
        List<Subject> subjectList = new ArrayList<>()
        subjectList.add(ODO020001)
        return subjectList
    }
}
