package edu.utesa.ronald.ingsoft.utils.bootstrap

import edu.utesa.ronald.ingsoft.domains.admin.Teacher
import edu.utesa.ronald.ingsoft.domains.security.Person
import edu.utesa.ronald.ingsoft.services.admin.TeacherService
import edu.utesa.ronald.ingsoft.utils.Constants
import groovy.transform.CompileStatic
import org.springframework.beans.factory.annotation.Autowired

/**
 *  Created by ronald on 7/8/17.
 */
@CompileStatic
class BasicTeacher {

    @Autowired
    private TeacherService teacherService

    private BasicPerson basicPerson

    Teacher teacher1

    BasicTeacher() {
        Constants.instance.autoWiredClass(this)
    }

    void insert(BasicPerson basicPerson) {
        this.basicPerson = basicPerson
        teacher1 = create("DF-045", basicPerson.personTeacher)
    }

    private Teacher create(String code, Person person) {
        Teacher teacher = teacherService.byCode(code)
        if (!teacher) {
            teacher = new Teacher()
            teacher.code = code
            teacher.person = person
            return teacherService.bootStrap(teacher)
        }
        return teacher
    }
}
