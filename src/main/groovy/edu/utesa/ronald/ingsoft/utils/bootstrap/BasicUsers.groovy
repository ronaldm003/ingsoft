package edu.utesa.ronald.ingsoft.utils.bootstrap

import edu.utesa.ronald.ingsoft.domains.security.Person
import edu.utesa.ronald.ingsoft.domains.security.User
import edu.utesa.ronald.ingsoft.models.enums.UserType
import edu.utesa.ronald.ingsoft.services.security.UserService
import edu.utesa.ronald.ingsoft.utils.Constants
import edu.utesa.ronald.ingsoft.utils.json.UserJson
import edu.utesa.ronald.ingsoft.utils.security.HashUtils
import groovy.transform.CompileStatic
import org.springframework.beans.factory.annotation.Autowired

/**
 *  Created by ronald on 7/8/17.
 */
@CompileStatic
class BasicUsers {

    @Autowired
    private UserService userService

    private BasicPerson basicPerson

    User userRoot
    User userPatient
    User userStudent
    User userTeacher

    Calendar calendar = new GregorianCalendar()

    Data data

    BasicUsers() {
        Constants.instance.autoWiredClass(this)
        calendar.set(1991, 1, 3)
        data = Constants.instance.convert(Constants.instance.readJsonFile("UserToDbTest"), Data.class)
    }

    void insert(BasicPerson basicPerson) {
        this.basicPerson = basicPerson
        userRoot = create(true, "root", "ronald03", basicPerson.personRoot, UserType.ROOT)
        userPatient = create(false, "patient", "patient", basicPerson.personPatient, UserType.PATIENT)
        userStudent = create(true, "student", "student", basicPerson.personStudent, UserType.STUDENT)
        userTeacher = create(true, "teacher", "teacher", basicPerson.personTeacher, UserType.TEACHER)
//        userService.bootStrap(userWeb)
//        for (UserJson userJson : data.data) {
//            createUserFromExcel(userJson)
//        }
    }

    private User create(boolean admin, String username, String password, Person person, UserType userType) {
        User user = userService.byUsername(username)
        if (!user) {
            user = new User()
            user.admin = admin
            user.person = person
            user.userType = userType
            user.username = username
            user.password = HashUtils.encodeSHA256(password)
            return userService.bootStrap(user)
        }
        return user
    }

//    private User createUserFromExcel(UserJson userJson) {
//        User user = userService.byUsername(userJson.username)
//        if (!user) {
//            user = new User()
//            user.admin = false
//            user.username = userJson.username ?: ""
//            user.password = HashUtils.encodeSHA256(userJson.password)
//            SecureRandom secureRandom0 = new SecureRandom()
//            SecureRandom secureRandom = new SecureRandom()
//            user.estatus = true
//            return userService.bootStrap(user)
//        }
//        return user
//    }

    private class Data {
        List<UserJson> data
    }
}
