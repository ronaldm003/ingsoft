package edu.utesa.ronald.ingsoft.utils.bootstrap.recordcard

import edu.utesa.ronald.ingsoft.domains.control.recordcard.EvaluationQuestion
import edu.utesa.ronald.ingsoft.services.control.recordcard.EvaluationQuestionService
import edu.utesa.ronald.ingsoft.utils.Constants
import groovy.transform.CompileStatic
import org.springframework.beans.factory.annotation.Autowired

/**
 *  Created by ronald on 7/8/17.
 */
@CompileStatic
class BasicEvaluationQuestions {

    @Autowired
    private EvaluationQuestionService evaluationQuestionService

    BasicEvaluationQuestions() {
        Constants.instance.autoWiredClass(this)
    }

    void insert() {
        for (String articles : getAllEvaluationQuestion()) {
            create(articles)
        }
    }

    private EvaluationQuestion create(String question) {
        EvaluationQuestion evaluationQuestion = evaluationQuestionService.byQuestion(question)
        if (!evaluationQuestion) {
            evaluationQuestion = new EvaluationQuestion()
            evaluationQuestion.code = evaluationQuestionService.findMaxCode()
            evaluationQuestion.question = question
            return evaluationQuestionService.bootStrap(evaluationQuestion)
        }
        return evaluationQuestion
    }

    private HashSet<String> getAllEvaluationQuestion() {
        HashSet<String> articleList = new HashSet<>()
        articleList.add("Motivo de la consulta")
        articleList.add("Alerta de la consulta?")
        articleList.add("Antecedentes patologicos hereditarios y personales")
        articleList.add("Ha tenido alguna cirugia o enfermedad no mencionada")
        articleList.add("Observaciones")
        return articleList
    }

}
