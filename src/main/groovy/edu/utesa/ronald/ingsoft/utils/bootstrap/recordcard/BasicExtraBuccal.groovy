package edu.utesa.ronald.ingsoft.utils.bootstrap.recordcard

import edu.utesa.ronald.ingsoft.domains.control.recordcard.ExtraBuccal
import edu.utesa.ronald.ingsoft.services.control.recordcard.ExtraBuccalService
import edu.utesa.ronald.ingsoft.utils.Constants
import groovy.transform.CompileStatic
import org.springframework.beans.factory.annotation.Autowired

/**
 *  Created by ronald on 7/8/17.
 */
@CompileStatic
class BasicExtraBuccal {

    @Autowired
    private ExtraBuccalService extraBuccalService

    BasicExtraBuccal() {
        Constants.instance.autoWiredClass(this)
    }

    void insert() {
        for (String articles : getAllExtraBuccal()) {
            create(articles)
        }
    }

    private ExtraBuccal create(String question) {
        ExtraBuccal extraBuccal = extraBuccalService.byQuestion(question)
        if (!extraBuccal) {
            extraBuccal = new ExtraBuccal()
            extraBuccal.code = extraBuccalService.findMaxCode()
            extraBuccal.question = question
            return extraBuccalService.bootStrap(extraBuccal)
        }
        return extraBuccal
    }

    private HashSet<String> getAllExtraBuccal() {
        HashSet<String> articleList = new HashSet<>()
        articleList.add("Cabeza")
        articleList.add("cuello")
        articleList.add("cara")
        articleList.add("labios")
        articleList.add("manos")
        return articleList
    }

}
