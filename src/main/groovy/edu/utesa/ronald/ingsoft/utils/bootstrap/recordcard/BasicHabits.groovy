package edu.utesa.ronald.ingsoft.utils.bootstrap.recordcard

import edu.utesa.ronald.ingsoft.domains.control.recordcard.Habits
import edu.utesa.ronald.ingsoft.services.control.recordcard.HabitsService
import edu.utesa.ronald.ingsoft.utils.Constants
import groovy.transform.CompileStatic
import org.springframework.beans.factory.annotation.Autowired

/**
 *  Created by ronald on 7/8/17.
 */
@CompileStatic
class BasicHabits {

    @Autowired
    private HabitsService habitsService

    BasicHabits() {
        Constants.instance.autoWiredClass(this)
    }

    void insert() {
        for (String articles : getAllHabits()) {
            create(articles)
        }
    }

    private Habits create(String description) {
        Habits habits = habitsService.byDescription(description)
        if (!habits) {
            habits = new Habits()
            habits.code = (habitsService.findMaxCode() + 1).toString()
            habits.description = description
            return habitsService.bootStrap(habits)
        }
        return habits
    }

    private List<String> getAllHabits() {
        List<String> habitsList = new ArrayList<>()
        habitsList.add("succion de dedos")
        habitsList.add("duerme o durmio con biberon")
        habitsList.add("respiracion bucal")
        habitsList.add("aprieta los dientes")
        habitsList.add("lleva objetos a la boca")
        habitsList.add("onicofagia")
        habitsList.add("chupete(bobo)")
        habitsList.add("deglucion atipica")
        habitsList.add("succion labial")
        habitsList.add("fuma")
        habitsList.add("consume alcohol")
        return habitsList
    }

}
