package edu.utesa.ronald.ingsoft.utils.bootstrap.recordcard

import edu.utesa.ronald.ingsoft.domains.control.recordcard.Illness
import edu.utesa.ronald.ingsoft.services.control.recordcard.IllnessService
import edu.utesa.ronald.ingsoft.utils.Constants
import groovy.transform.CompileStatic
import org.springframework.beans.factory.annotation.Autowired

/**
 *  Created by ronald on 7/8/17.
 */
@CompileStatic
class BasicIllness {

    @Autowired
    private IllnessService illnessService

    BasicIllness() {
        Constants.instance.autoWiredClass(this)
    }

    void insert() {
        for (String articles : getAllIllness()) {
            create(articles)
        }
    }

    private Illness create(String description) {
        Illness illness = illnessService.byDescription(description)
        if (!illness) {
            illness = new Illness()
            illness.code = (illnessService.findMaxCode() + 1).toString()
            illness.description = description
            return illnessService.bootStrap(illness)
        }
        return illness
    }

    private List<String> getAllIllness() {
        List<String> illnessList = new ArrayList<>()
        illnessList.add("Garganta")
        illnessList.add("Riñones")
        illnessList.add("Oidos")
        illnessList.add("Fracturas")
        illnessList.add("Estomago")
        illnessList.add("Neumonia")
        illnessList.add("Anemia")
        illnessList.add("Piel")
        illnessList.add("Alergias")
        illnessList.add("Asma")
        illnessList.add("Corazon")
        illnessList.add("Nervios")
        illnessList.add("Bronquitis")
        illnessList.add("Fiebre Reumatica")
        illnessList.add("Higado")
        illnessList.add("Convulsiones")
        illnessList.add("Diabetes")
        illnessList.add("Sangre")
        illnessList.add("Mareos")
        illnessList.add("Vista")
        return illnessList
    }

}
