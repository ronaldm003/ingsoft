package edu.utesa.ronald.ingsoft.utils.bootstrap.recordcard

import edu.utesa.ronald.ingsoft.domains.control.recordcard.IntraBuccal
import edu.utesa.ronald.ingsoft.services.control.recordcard.IntraBuccalService
import edu.utesa.ronald.ingsoft.utils.Constants
import groovy.transform.CompileStatic
import org.springframework.beans.factory.annotation.Autowired

/**
 *  Created by ronald on 7/8/17.
 */
@CompileStatic
class BasicIntraBuccal {

    @Autowired
    private IntraBuccalService intraBuccalService

    BasicIntraBuccal() {
        Constants.instance.autoWiredClass(this)
    }

    void insert() {
        for (String articles : getAllIntraBuccal()) {
            create(articles)
        }
    }

    private IntraBuccal create(String question) {
        IntraBuccal intraBuccal = intraBuccalService.byQuestion(question)
        if (!intraBuccal) {
            intraBuccal = new IntraBuccal()
            intraBuccal.code = intraBuccalService.findMaxCode()
            intraBuccal.question = question
            return intraBuccalService.bootStrap(intraBuccal)
        }
        return intraBuccal
    }

    private HashSet<String> getAllIntraBuccal() {
        HashSet<String> articleList = new HashSet<>()
        articleList.add("Paladar")
        articleList.add("Frenillos")
        articleList.add("Encía")
        articleList.add("Piso de boca")
        articleList.add("Mucosa bucal")
        articleList.add("Lengua")
        articleList.add("Orofaringe")
        return articleList
    }

}
