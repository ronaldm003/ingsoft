package edu.utesa.ronald.ingsoft.utils.bootstrap.recordcard

import edu.utesa.ronald.ingsoft.domains.control.recordcard.Tooth
import edu.utesa.ronald.ingsoft.services.control.recordcard.ToothService
import edu.utesa.ronald.ingsoft.utils.Constants
import groovy.transform.CompileStatic
import org.springframework.beans.factory.annotation.Autowired

/**
 *  Created by ronald on 7/8/17.
 */
@CompileStatic
class BasicTooth {

    @Autowired
    private ToothService toothService

    BasicTooth() {
        Constants.instance.autoWiredClass(this)
    }

    void insert() {
        for (String articles : getAdultsTooths()) {
            create(articles, false)
        }
        for (String articles : getChildTooths()) {
            create(articles, true)
        }
    }

    private Tooth create(String number, boolean child) {
        Tooth tooth = toothService.byNumber(number)
        if (!tooth) {
            tooth = new Tooth()
            tooth.number = number
            tooth.child = child
            return toothService.bootStrap(tooth)
        }
        return tooth
    }

    private static HashSet<String> getAdultsTooths() {
        HashSet<String> articleList = new HashSet<>()
        articleList.add("18")
        articleList.add("17")
        articleList.add("16")
        articleList.add("15")
        articleList.add("14")
        articleList.add("13")
        articleList.add("12")
        articleList.add("11")

        articleList.add("21")
        articleList.add("22")
        articleList.add("23")
        articleList.add("24")
        articleList.add("25")
        articleList.add("26")
        articleList.add("27")
        articleList.add("28")

        articleList.add("48")
        articleList.add("47")
        articleList.add("46")
        articleList.add("45")
        articleList.add("44")
        articleList.add("43")
        articleList.add("42")
        articleList.add("41")

        articleList.add("31")
        articleList.add("32")
        articleList.add("33")
        articleList.add("34")
        articleList.add("35")
        articleList.add("36")
        articleList.add("37")
        articleList.add("38")
        return articleList
    }

    private static HashSet<String> getChildTooths() {
        HashSet<String> articleList = new HashSet<>()
        articleList.add("55")
        articleList.add("54")
        articleList.add("53")
        articleList.add("52")
        articleList.add("51")

        articleList.add("61")
        articleList.add("62")
        articleList.add("63")
        articleList.add("64")
        articleList.add("65")

        articleList.add("85")
        articleList.add("84")
        articleList.add("83")
        articleList.add("82")
        articleList.add("81")

        articleList.add("71")
        articleList.add("72")
        articleList.add("73")
        articleList.add("74")
        articleList.add("75")

        return articleList
    }
}
