package edu.utesa.ronald.ingsoft.utils.calendar

import com.vaadin.server.Page
import com.vaadin.shared.ui.colorpicker.Color
import com.vaadin.ui.components.calendar.event.BasicEvent
import edu.utesa.ronald.ingsoft.domains.appointment.Appointment
import groovy.transform.CompileStatic

/**
 *  Created by ronald on 7/26/18.
 */
@CompileStatic
class AppointmentEvent extends BasicEvent {

    Appointment appointment

    AppointmentEvent(Appointment appointment) {
        this.appointment = appointment
        addCSS()
        prepare()
    }

    private void addCSS() {
        if (appointment.appointmentType) {
            String cssResult = ".v-calendar-event-" + appointment.appointmentType.name + ", .v-calendar-event-" + appointment.appointmentType.name + "-all-day { background-color: " + new Color(appointment.appointmentType.color).getCSS() + " !important;}"
            Page.getCurrent().getStyles().add(cssResult)
        }
    }

    private void prepare() {
        if (appointment) {
            this.caption = appointment.toString()
        }
        this.description = appointment.description
        this.start = appointment.start
        this.end = appointment.end
        if (appointment.appointmentType) {
            this.styleName = appointment.appointmentType.name
        }
    }
}
