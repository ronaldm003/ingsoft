package edu.utesa.ronald.ingsoft.utils.calendar

import edu.utesa.ronald.ingsoft.models.interfaces.CallbackCalendar
import groovy.transform.CompileStatic

/**
 * Para manejar memoria en el callback
 *
 * Created by ronald on 7/28/16.
 */
@CompileStatic
abstract class CallCalendar implements CallbackCalendar {

    protected List<AppointmentEvent> appointmentEvents = new ArrayList<>()
}
