package edu.utesa.ronald.ingsoft.utils.configurations

import com.vaadin.spring.annotation.EnableVaadin
import com.vaadin.spring.server.SpringVaadinServlet
import groovy.transform.CompileStatic
import org.springframework.boot.web.servlet.ServletRegistrationBean
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration

/**
 *  Created by ronald on 7/4/17.
 */
@SuppressWarnings("GroovyUnusedDeclaration")
@Configuration
@EnableVaadin
@CompileStatic
class VaadinConfiguration {

    static final String INDEX = "/"
    static final String LOGIN = "/login"
    static final String SIGN_UP = "/sign_up"
    static final String APP = "/app"

    @Bean
    ServletRegistrationBean servletRegistrationBeanSignUp() {
        SpringVaadinServlet springVaadinServlet = new SpringVaadinServlet()
        springVaadinServlet.serviceUrlPath = SIGN_UP
        return new ServletRegistrationBean(springVaadinServlet, SIGN_UP + "/*", "/VAADIN/*")
    }

    @Bean
    ServletRegistrationBean servletRegistrationBeanApp() {
        SpringVaadinServlet springVaadinServlet = new SpringVaadinServlet()
        springVaadinServlet.serviceUrlPath = APP
        return new ServletRegistrationBean(springVaadinServlet, APP + "/*", "/VAADIN/*")
    }

    @Bean
    ServletRegistrationBean servletRegistrationBeanLogin() {
        SpringVaadinServlet springVaadinServlet = new SpringVaadinServlet()
        springVaadinServlet.serviceUrlPath = LOGIN
        return new ServletRegistrationBean(springVaadinServlet, LOGIN + "/*", "/VAADIN/*")
    }

}
