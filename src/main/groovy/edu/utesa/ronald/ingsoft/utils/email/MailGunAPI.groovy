package edu.utesa.ronald.ingsoft.utils.email

import edu.utesa.ronald.ingsoft.services.mail.EmailService
import edu.utesa.ronald.ingsoft.services.security.ParamService
import edu.utesa.ronald.ingsoft.utils.Constants
import edu.utesa.ronald.ingsoft.utils.email.models.EmailData
import edu.utesa.ronald.ingsoft.utils.email.models.Sent
import edu.utesa.ronald.ingsoft.utils.email.templates.ForgotMyPasswordTemplateEmail
import edu.utesa.ronald.ingsoft.utils.email.templates.PasswordChangeCompleteTemplateEmail
import edu.utesa.ronald.ingsoft.utils.email.templates.ValidationKeyTemplateEmail
import edu.utesa.ronald.ingsoft.utils.params.ParamType
import groovy.transform.CompileStatic
import org.glassfish.jersey.client.authentication.HttpAuthenticationFeature
import org.springframework.beans.factory.annotation.Autowired

import javax.ws.rs.client.Client
import javax.ws.rs.client.ClientBuilder
import javax.ws.rs.client.Entity
import javax.ws.rs.client.WebTarget
import javax.ws.rs.core.MediaType

/**
 *  Created by ronald on 3/27/17.
 */
@CompileStatic
final class MailGunAPI {

    @Autowired
    private ParamService paramService
    @Autowired
    private EmailService emailService

    private static final String API = "api"

    private static final String SEND = "/messages"

    private Client client
    private WebTarget webTarget

    private String apiKey
    private String urlDomain

    MailGunAPI() {
        Constants.instance.autoWiredClass(this)
        init()
    }

    private void init() {
        apiKey = paramService.byCode(ParamType.EMAIL_API).getValue() ?: ""
        urlDomain = paramService.byCode(ParamType.EMAIL_DOMAIN).getValue()
        client = ClientBuilder.newClient()
        client.register(HttpAuthenticationFeature.basicBuilder().nonPreemptive().credentials(API, apiKey).build())
        webTarget = client.target(urlDomain).path(SEND)
    }

    Sent sendEmail(EmailData emailData) {
        if (emailData.isMultiPart()) {
            return Constants.instance.convert(webTarget.request()
                    .post(Entity.entity(emailData.buildMultipartForm(), MediaType.APPLICATION_FORM_URLENCODED_TYPE))
                    .readEntity(String.class), Sent.class) as Sent
        }
        return Constants.instance.convert(webTarget.request().
                post(Entity.entity(emailData.buildSimpleForm(), MediaType.APPLICATION_FORM_URLENCODED_TYPE))
                .readEntity(String.class), Sent.class) as Sent
    }

    void forgotMyPassword(ForgotMyPasswordTemplateEmail forgotMyPasswordTemplateEmail) {
        emailService.bootStrap(sendEmail(forgotMyPasswordTemplateEmail).toDomain())
    }

    void sendValidationKey(ValidationKeyTemplateEmail validationKeyTemplateEmail) {
        emailService.bootStrap(sendEmail(validationKeyTemplateEmail).toDomain())
    }

    void passwordChangeComplete(PasswordChangeCompleteTemplateEmail passwordChangeCompleteTemplateEmail) {
        emailService.bootStrap(sendEmail(passwordChangeCompleteTemplateEmail).toDomain())
    }
}
