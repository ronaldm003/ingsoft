package edu.utesa.ronald.ingsoft.utils.email.models

import groovy.transform.CompileStatic
import org.glassfish.jersey.media.multipart.FormDataMultiPart
import org.glassfish.jersey.media.multipart.file.FileDataBodyPart

import javax.ws.rs.core.Form
import javax.ws.rs.core.MediaType

/**
 *  Created by ronald on 3/27/17.
 */
@CompileStatic
class EmailData {

    private static final String FROM = "from"
    private static final String TO = "to"
    private static final String SUBJECT = "subject"
    private static final String TEXT = "text"
    private static final String HTML = "html"
    private static final String ATTACHMENT = "attachment"

    String from
    List<String> tos = new ArrayList<>()
    String subject
    String data
    boolean asHTML

    List<File> files = new ArrayList<>()

    EmailData() {
    }

    boolean isMultiPart() {
        return files && files.size() > 0
    }

    Form buildSimpleForm() {
        Form form = new Form()
        form.param(FROM, from)
        for (String toSend : tos) {
            form.param(TO, toSend)
        }
        form.param(SUBJECT, subject)
        if (asHTML) {
            form.param(HTML, data)
        } else {
            form.param(TEXT, data)
        }
        return form
    }

    FormDataMultiPart buildMultipartForm() {
        FormDataMultiPart formDataMultiPart = new FormDataMultiPart()
        formDataMultiPart.field(FROM, from)
        for (String toSend : tos) {
            formDataMultiPart.field(TO, toSend)
        }
        formDataMultiPart.field(SUBJECT, subject)
        if (asHTML) {
            formDataMultiPart.field(HTML, data)
        } else {
            formDataMultiPart.field(TEXT, data)
        }
        int i = 0
        for (File file : files) {
            formDataMultiPart.bodyPart(new FileDataBodyPart(ATTACHMENT + "-" + i, file, MediaType.APPLICATION_OCTET_STREAM_TYPE))
            i++
        }
        return formDataMultiPart
    }
}
