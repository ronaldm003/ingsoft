package edu.utesa.ronald.ingsoft.utils.email.models

import edu.utesa.ronald.ingsoft.domains.mail.Email
import edu.utesa.ronald.ingsoft.models.interfaces.DomainConverter
import groovy.transform.CompileStatic

/**
 *  Created by ronald on 3/27/17.
 */
@CompileStatic
class Sent implements DomainConverter<Email> {

    String id
    String message

    @Override
    Email toDomain() {
        Email email = new Email()
        email.emailID = id
        email.message = message
        return email
    }
}
