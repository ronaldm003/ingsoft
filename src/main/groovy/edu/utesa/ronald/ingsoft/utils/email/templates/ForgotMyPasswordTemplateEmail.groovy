package edu.utesa.ronald.ingsoft.utils.email.templates

import edu.utesa.ronald.ingsoft.services.security.ParamService
import edu.utesa.ronald.ingsoft.services.security.UserService
import edu.utesa.ronald.ingsoft.utils.Constants
import edu.utesa.ronald.ingsoft.utils.Languages
import edu.utesa.ronald.ingsoft.utils.email.models.EmailData
import edu.utesa.ronald.ingsoft.utils.params.ParamType
import groovy.transform.CompileStatic
import org.springframework.beans.factory.annotation.Autowired

/**
 *  Created by ronald on 11/1/17.
 */
@CompileStatic
class ForgotMyPasswordTemplateEmail extends EmailData {

    @Autowired
    private ParamService paramService

    @Autowired
    private UserService userService

    ForgotMyPasswordTemplateEmail(String to, String keyLost) {
        Constants.instance.autoWiredClass(this)
        from = paramService.byCode(ParamType.EMAIL_FROM_DEFAULT_NO_REPLY).getValue()
        tos.add(to)
        subject = Languages.instance.getText("email.forgot.password.subject")
        data = String.format(Languages.instance.getText("email.forgot.password.data"), userService.byEmail(to).toString(), keyLost, keyLost)
        asHTML = true
    }
}
