package edu.utesa.ronald.ingsoft.utils.json

import edu.utesa.ronald.ingsoft.models.enums.Gender
import groovy.transform.CompileStatic

/**
 *  Created by ronald on 6/12/17.
 */
@CompileStatic
class UserJson {

    Long code
    String name
    Gender genre
    String username
    String password

    @Override
    String toString() {
        return "UserJson{" +
                "code=" + code +
                ", name='" + name + '\'' +
                ", gender=" + genre +
                ", username='" + username + '\'' +
                ", password='" + password + '\'' +
                '}'
    }
}
