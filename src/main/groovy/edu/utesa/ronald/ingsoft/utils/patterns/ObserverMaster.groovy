package edu.utesa.ronald.ingsoft.utils.patterns

import groovy.transform.CompileStatic

/**
 * Observer master listener.
 *
 *  Created by ronald on 3/26/17.
 */
@CompileStatic
final class ObserverMaster {

    private static ObserverMaster instance = null

    private final List<ObservableListener> observables = new ArrayList<>()

    private ObserverMaster() {
    }

    static ObserverMaster get() {
        if (instance == null) {
            synchronized (ObserverMaster.class) {
                if (instance == null) {
                    instance = new ObserverMaster()
                }
            }
        }
        return instance
    }

    void add(ObservableListener observableListener) {
        synchronized (observables) {
            observables.add(observableListener)
        }
    }

    void remove(ObservableListener observableListener) {
        synchronized (observables) {
            observables.remove(observableListener)
        }
    }

    void remove(List<ObservableListener> observableListeners) {
        if (observableListeners.size() > 0) {
            synchronized (observables) {
                observables.removeAll(observableListeners)
            }
        }
    }

    void fire() {
        fire(null)
    }

    void fire(ObservableListener dispatcher) {
        List<ObservableListener> toRemove = new ArrayList<>()
        for (ObservableListener observableListener : observables) {
            try {
                observableListener.actionObserver()
            } catch (Exception ignored) {
                toRemove.add(observableListener)
            }
        }
        remove(toRemove)
    }

    interface ObservableListener {

        void actionObserver()
    }
}
