package edu.utesa.ronald.ingsoft.models.constants;

/**
 * Maneja todos los estilos de la aplicación.
 * <p>
 * Created by ronald on 6/24/17.
 */
public class Styles {

    public static final String SEARCH_MENU = "search_menu";
    public static final String MENU_USER_SETTINGS = "menu_user_settings";
    public static final String MENU_BUTTON = "menu_button";
    public static final String SUB_MENU = "sub_menu";

    public static final String TOP_BAR = "top_bar";
    public static final String LABEL_INFORMATIONS = "label_informations";
    public static final String LABEL_SOFTWARE = "software-top-bar-label";
    public static final String BUSINESS_PANEL = "business_panel";

    public static final String FORM_ACTION = "form_action";

    public static final String LOGIN = "login";
    public static final String LABEL_LOGIN_APP_NAME = "login-app-label";
    public static final String LOGIN_WINDOW = "login-window-app";
    public static final String CALENDAR = "calendar_control";
    public static final String APP_CONTROL = "app_control";
    public static final String APP_CONTROL_TABS = "app_control_tabs";

    public static final String EASE_BUTTON = "ease-buttons";
    public static final String QUOTE_BUTTON = "quote-button";
    public static final String CALENDAR_BUTTON = "calendar-buttons";
    public static final String FACEBOOK_BUTTON = "fb-button";
    public static final String YOUTUBE_BUTTON = "yt-button";
    public static final String INSTAGRAM_BUTTON = "ig-button";
    public static final String CHANGE_PASSWORD_BUTTON = "changePassword-button";

    public static final String LEFT_PANEL_MENU = "left_panel_menu";
    public static final String PROFILESOFT_LOGIN = "profilesoft_login";
    public static final String LOGIN_BUTTON = "login_button";
    public static final String FORGOT_BUTTON = "forgot_button";

    public static final String TOTAL_LABEL = "total-label";
    public static final String BORROWED_MONEY_LABEL = "borrowed-money-label";
    public static final String ACTIVE_LOANS_LABEL = "active-loans-label";
    public static final String DAY_PAYMENTS_LABEL = "day-payments-label";
    public static final String LATE_PAYMENTS_LABEL = "late-payments-label";

    public static final String NOTIFICATION_SUCCESS = "notification-success";
    public static final String NOTIFICATION_INCOMPLETE = "notification-incomplete";

    public static final String BUTTON_CANCEL = "rave-cancel-buttons";

    public static final String CENTER_CAPTION_TEXT = "center_caption_text";

    public static final String BUTTON_MONTH = "rave-month-buttons";
    public static final String BUTTON_WEEK = "rave-week-buttons";
    public static final String BUTTON_DAY = "rave-day-buttons";
    public static final String BUTTON_NEW_EVENT = "rave-new_event-buttons";
    public static final String BUTTON_PICK = "btn_pick";
    public static final String BUTTON_NULL = "btn_null";
    public static final String BUTTON_DELETE = "rave-cancel-buttons";
    public static final String BUTTON_REFRESH = "btn_refresh";

    public static final String TABLE_HIDE_COLLAPSE = "hidecollapse";

}
