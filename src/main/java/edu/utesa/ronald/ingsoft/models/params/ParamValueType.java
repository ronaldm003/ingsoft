package edu.utesa.ronald.ingsoft.models.params;

/**
 * Tipos de enum
 * Created by ronald on 7/4/17.
 */
public enum ParamValueType {
    BOOLEAN, TEXT, IMG, NUMERIC, ENUM_TIME_ZONE, ENUM_PAYMENT;

    @Override
    public String toString() {
        switch (this) {
            case BOOLEAN:
                return "Boolean";
            case TEXT:
                return "Text";
            case IMG:
                return "Img";
            case NUMERIC:
                return "Numeric";
            case ENUM_TIME_ZONE:
                return "Time Zone";
            case ENUM_PAYMENT:
                return "Payment";
        }
        return "";
    }
}
