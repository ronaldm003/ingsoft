
package edu.utesa.ronald.ingsoft.models.queries;

import edu.utesa.ronald.ingsoft.models.ui.AboutElement;
import edu.utesa.ronald.ingsoft.utils.container.BaseQuery;
import edu.utesa.ronald.ingsoft.utils.container.BaseQueryFactory;

/**
 * Created by ronald on 6/27/17.
 */
public class AboutQuery extends BaseQuery<AboutElement> {

    public AboutQuery(BaseQueryFactory<AboutElement> baseQueryFactory) {
        super(baseQueryFactory);
    }
}
