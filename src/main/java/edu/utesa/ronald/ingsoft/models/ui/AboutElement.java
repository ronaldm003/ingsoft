package edu.utesa.ronald.ingsoft.models.ui;

/**
 * Created by ronald on 7/4/17.
 */
public class AboutElement {

    private String name;
    private String verion;

    public AboutElement() {
    }

    public AboutElement(String name, String verion) {
        this.name = name;
        this.verion = verion;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getVerion() {
        return verion;
    }

    public void setVerion(String verion) {
        this.verion = verion;
    }
}
