package edu.utesa.ronald.ingsoft.utils;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Logger details.
 * <p>
 * Created by ronald on 7/8/17.
 */
public class LoggerUpdater {

    private static LoggerUpdater instance = null;

    private static final SimpleDateFormat DATE_HOUR_FORMAT = new SimpleDateFormat("MM-dd-yyyy HH:mm:ss");
    private static final String FILE_ENCODE = "UTF-8";

    private static File LOGGER;
    private static File DEBUG;
    private PrintWriter writer;
    private PrintWriter writerDebug;

    private LoggerUpdater() throws FileNotFoundException, UnsupportedEncodingException {
        initFiles();
    }

    public synchronized static LoggerUpdater get() throws FileNotFoundException, UnsupportedEncodingException {
        if (instance == null) {
            instance = new LoggerUpdater();
        }
        return instance;
    }

    private synchronized void initFiles() throws FileNotFoundException, UnsupportedEncodingException {
        LOGGER = new File("./IngSoft.elp");
        DEBUG = new File("./ProfileSoftDebug.elp");
        writer = new PrintWriter(new BufferedWriter(new OutputStreamWriter(new FileOutputStream(LOGGER, true), FILE_ENCODE)));
        writerDebug = new PrintWriter(new BufferedWriter(new OutputStreamWriter(new FileOutputStream(DEBUG, true), FILE_ENCODE)));
    }

    public synchronized void clearFiles() throws FileNotFoundException, UnsupportedEncodingException {
        writer.close();
        writerDebug.close();
        clearFile(LOGGER);
        clearFile(DEBUG);
        initFiles();
    }

    private void clearFile(File file) throws FileNotFoundException {
        PrintWriter writer = new PrintWriter(file);
        writer.print("");
        writer.close();
    }

    public void info(String data) {
        log(data);
    }

    public synchronized void exception(Exception ex) {
        ex.printStackTrace(writerDebug);
        writerDebug.flush();
    }

    private synchronized void log(String log) {
        writer.println(DATE_HOUR_FORMAT.format(new Date()) + " ** " + log);
        writer.flush();
    }

    public List<String> readADM() {
        List<String> data = new ArrayList<>();
        try {
            BufferedReader bufferedReader = new BufferedReader(new FileReader(LOGGER));
            String line;
            while ((line = bufferedReader.readLine()) != null) {
                data.add(line);
            }
            return data;
        } catch (Exception e) {
            return data;
        }
    }

    public String readAll() {
        try {
            return new String(Files.readAllBytes(Paths.get(LOGGER.toURI())), FILE_ENCODE);
        } catch (IOException e) {
            return null;
        }
    }

    public String readAllDebug() {
        try {
            return new String(Files.readAllBytes(Paths.get(DEBUG.toURI())), FILE_ENCODE);
        } catch (IOException e) {
            return null;
        }
    }

    @Override
    protected void finalize() throws Throwable {
        super.finalize();
        writer.close();
        writerDebug.close();
    }
}
