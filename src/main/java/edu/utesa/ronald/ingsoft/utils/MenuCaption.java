package edu.utesa.ronald.ingsoft.utils;

/**
 * Menu tools
 * <p>
 * Created by ronald on03/9/17.
 */
public class MenuCaption {

    public static final String SEARCH_MENU = "menu.search.input.prompt";

    public static final String MENU_DENTAL = "menu.panel.dental";
    public static final String MENU_REPORTS = "menu.panel.reports";
    public static final String MENU_ADMIN = "menu.panel.admin";
    public static final String MENU_CONTROL = "menu.panel.control";
    public static final String MENU_LOCATION = "menu.panel.location";
    public static final String MENU_SECURITY = "menu.panel.security";
    public static final String MENU_UTILS = "menu.panel.utils";
}
