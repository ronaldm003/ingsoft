package edu.utesa.ronald.ingsoft.utils;

/**
 *  Created by ronald on 9/20/2016.
 */
public class ReportParameter {

    public static String IMAGE_LOGO = "IMAGE_LOGO";
    public static String APP_NAME = "APP_NAME";
    public static String REPORT_RESOURCE_BUNDLE = "REPORT_RESOURCE_BUNDLE";
    public static String START_DATE = "START_DATE";
    public static String END_DATE = "END_DATE";

}
