package edu.utesa.ronald.ingsoft.utils;

import com.vaadin.ui.ComboBox;

import java.util.ArrayList;
import java.util.List;

/**
 * Define themes can be use
 * <p>
 * Created by ronald on 8/5/17.
 */
public class Themes {

    public static final String ING_SOFT = "IngSoft";
    public static final String TEST = "Test";

    public static List<String> values() {
        List<String> themes = new ArrayList<>();
        themes.add(ING_SOFT);
        themes.add(TEST);
        return themes;
    }

    public static void fillThemes(ComboBox comboBox) {
        comboBox.removeAllItems();
        Themes.values().forEach(comboBox::addItem);
    }
}
