package edu.utesa.ronald.ingsoft.utils;

/**
 * Captions of windows
 * Created by ronald on 8/4/17.
 */
public class WindowsCaption {

    public static final String APPOINTMENT_CAPTION = "calendar.appointment";

    public static final String CATEGORY_CAPTION = "menu.panel.control.category";
    public static final String SUB_CATEGORY_CAPTION = "menu.panel.control.sub.category";
    public static final String DOCUMENTS_CAPTION = "menu.panel.control.documents";
    public static final String DOCUMENT_TYPE_CAPTION = "menu.panel.control.document.type";
    public static final String PROFILE_CAPTION = "menu.panel.control.profile";

    public static final String ASSIGN_CAPTION = "menu.panel.dental.assign";
    public static final String ASSIGN_TAB_CAPTION = "menu.panel.dental.assign.tab";
    public static final String RECORD_CARD_CAPTION = "menu.panel.dental.record.card";
    public static final String CONSULTS_CAPTION = "menu.panel.dental.consults";

    public static final String CYCLE_CAPTION = "menu.panel.admin.cycle";
    public static final String OBJECTIVE_CAPTION = "menu.panel.admin.objective";
    public static final String STUDENT_CAPTION = "menu.panel.admin.student";
    public static final String SUBJECT_CAPTION = "menu.panel.admin.subject";
    public static final String SUBJECT_SCHEDULE_CAPTION = "menu.panel.admin.subject.schedule";
    public static final String TEACHER_CAPTION = "menu.panel.admin.teacher";

    public static final String ARTICLE_CAPTION = "menu.panel.control.article";
    public static final String INVENTORY_CAPTION = "menu.panel.control.inventory";
    public static final String PATIENT_CAPTION = "menu.panel.control.patient";
    public static final String EQUIPMENT_CAPTION = "menu.panel.control.equipment";

    public static final String PERSON_CAPTION = "menu.panel.security.person";
    public static final String USERS_CAPTION = "menu.panel.security.user";
    public static final String USER_TYPE_CAPTION = "menu.panel.security.user.type";
    public static final String USER_KEYS_CAPTION = "menu.panel.security.user.keys";
    public static final String USER_RELATED_CAPTION = "menu.panel.security.user.related";

    public static final String LOCATION_CAPTION = "menu.panel.location.location";
    public static final String CLINIC_CAPTION = "menu.panel.location.clinic";

    public static final String PARAMS_CAPTION = "menu.panel.security.param";
    public static final String PERMISSIONS_CAPTION = "menu.panel.security.permission";
    public static final String PERMISSIONS_GROUP_CAPTION = "menu.panel.security.permission.group";

    public static final String OCCUPATION_CAPTION = "menu.panel.utils.occupation";

    public static final String LOG_IN_ALERT_CAPTION = "menu.panel.audit.login.alert";

    public static final String EMAIL_CAPTION = "menu.panel.mail.email";
    public static final String FORGOT_PASSWORD_CAPTION = "menu.panel.mail.forgot.password";

    public static final String REPORTS_CAPTION = "menu.panel.reports";
    public static final String REPORT_PRINTED_CAPTION = "menu.panel.reports.printed";
    public static final String REPORT_LOANS_CAPTION = "menu.panel.reports.loans";
    public static final String REPORT_LOANS_HISTORY_CAPTION = "menu.panel.reports.loans.history";

    public static final String SYSTEM_NOTIFICATION_CAPTION = "menu.panel.system.notification";

}
