package edu.utesa.ronald.ingsoft.utils.container;

import org.vaadin.addons.lazyquerycontainer.LazyQueryContainer;
import org.vaadin.addons.lazyquerycontainer.LazyQueryDefinition;
import org.vaadin.addons.lazyquerycontainer.LazyQueryView;

import java.math.BigDecimal;
import java.sql.Time;
import java.util.Date;

/**
 * Created by ronald on03/8/17.
 */
public abstract class BaseContainer extends LazyQueryContainer {

    protected BaseQueryFactory baseQueryFactory;

    public BaseContainer(BaseQueryFactory baseQueryFactory) {
        super(new LazyQueryDefinition(true, BaseQueryFactory.BATCH_GRID_SIZE, BaseQueryFactory.OBJ), baseQueryFactory);
        this.baseQueryFactory = baseQueryFactory;
        addContainerProperty(BaseQueryFactory.OBJ, Object.class, null, false, false);
    }

    @SuppressWarnings("unused")
    public final void addDebug() {
        addContainerProperty(LazyQueryView.DEBUG_PROPERTY_ID_QUERY_INDEX, Integer.class, 0, true, false);
        addContainerProperty(LazyQueryView.DEBUG_PROPERTY_ID_BATCH_INDEX, Integer.class, 0, true, false);
        addContainerProperty(LazyQueryView.DEBUG_PROPERTY_ID_BATCH_QUERY_TIME, Long.class, 0, true, false);
        addContainerProperty(LazyQueryView.PROPERTY_ID_ITEM_STATUS, Enum.class, 0, true, false);
    }

    public final void addGenericColumn(Class dataClass, String name) {
        addGenericColumn(dataClass, name, null);
    }

    public final void addGenericColumn(Class dataClass, String name, Object defaultValue) {
        addContainerProperty(name, dataClass, defaultValue, true, false);
    }

    public final void addStringColumn(String name) {
        addStringColumn(name, "");
    }

    public final void addStringColumn(String name, String defaultValue) {
        addContainerProperty(name, String.class, defaultValue, true, false);
    }

    public final void addBooleanColumn(String name) {
        addBooleanColumn(name, true);
    }

    public final void addBooleanColumn(String name, boolean defaultValue) {
        addContainerProperty(name, Boolean.class, defaultValue, true, false);
    }

    public final void addBigDecimalColumn(String name) {
        addBigDecimalColumn(name, null);
    }

    public final void addBigDecimalColumn(String name, BigDecimal defaultValue) {
        addContainerProperty(name, BigDecimal.class, defaultValue, true, false);
    }

    public final void addIntegerColumn(String name) {
        addIntegerColumn(name, 0);
    }

    public final void addIntegerColumn(String name, Integer defaultValue) {
        addContainerProperty(name, Integer.class, defaultValue, true, false);
    }

    public final void addDateColumn(String name) {
        addDateColumn(name, null);
    }

    public final void addDateColumn(String name, Date defaultValue) {
        addContainerProperty(name, Date.class, defaultValue, true, false);
    }

    public final void addTimeColumn(String name) {
        addTimeColumn(name, null);
    }

    public final void addTimeColumn(String name, Time defaultValue) {
        addContainerProperty(name, Date.class, defaultValue, true, false);
    }

    public abstract void addColumns();

    public final int sizeQuery() {
        return baseQueryFactory.sizeQuery();
    }

    public final boolean getFiltered() {
        return baseQueryFactory.getFiltered();
    }

    public final void setFiltered(boolean filtered) {
        baseQueryFactory.setFiltered(filtered);
    }
}
