package edu.utesa.ronald.ingsoft.utils.container;

import com.vaadin.data.Item;
import com.vaadin.data.util.ObjectProperty;
import com.vaadin.data.util.PropertysetItem;
import org.vaadin.addons.lazyquerycontainer.QueryDefinition;
import org.vaadin.addons.lazyquerycontainer.QueryFactory;

/**
 *  Created by ronald on03/8/17.
 */
public abstract class BaseQueryFactory<BEANTYPE> implements QueryFactory {

    public static final String OBJ = "OBJ";
    public static final String ID = "id";
    public static final int BATCH_GRID_SIZE = 20;

    protected QueryDefinition queryDefinition;
    protected LazyQuery<BEANTYPE> lazyQuery;
    private boolean filtered = false;

    protected BaseQueryFactory(LazyQuery<BEANTYPE> lazyQuery) {
        this.lazyQuery = lazyQuery;
    }

    public abstract Item constructItem();

    @SuppressWarnings("unchecked")
    public Item constructItem(BEANTYPE beantype) {
        PropertysetItem item = new PropertysetItem();
        for (Object propertyId : this.queryDefinition.getPropertyIds()) {
            Object value = createProperty(propertyId, beantype);
            item.addItemProperty(propertyId, new ObjectProperty(value,
                    queryDefinition.getPropertyType(propertyId),
                    queryDefinition.isPropertyReadOnly(propertyId)
            ));
        }
        return item;
    }

    public abstract Object createProperty(Object propertyID, BEANTYPE dataObject);

    protected final Object getDefaultProperty(Object propertyID) {
        return queryDefinition.getPropertyDefaultValue(propertyID);
    }

    public final QueryDefinition getQueryDefinition() {
        return queryDefinition;
    }

    public final void setQueryDefinition(QueryDefinition queryDefinition) {
        this.queryDefinition = queryDefinition;
    }

    public final LazyQuery<BEANTYPE> getLazyQuery() {
        return lazyQuery;
    }

    public final void setLazyQuery(LazyQuery<BEANTYPE> lazyQuery) {
        this.lazyQuery = lazyQuery;
    }

    public final int sizeQuery() {
        if (filtered) {
            return lazyQuery.getFilteredSize();
        }
        return lazyQuery.getSize();
    }

    public final boolean getFiltered() {
        return this.filtered;
    }

    public final void setFiltered(boolean filtered) {
        this.filtered = filtered;
    }
}
