package edu.utesa.ronald.ingsoft.utils.container;

import java.util.List;

/**
 *  Created by ronald on03/8/17.
 */
public interface LazyQuery<BEANTYPE> {

    /**
     * Get Size by a Query
     */
    int getSize();

    /**
     * Get items to view on the page using the same model of the container.
     */
    List<BEANTYPE> getItemsIds(int startIndex, int numberOfIds);

    /**
     * Get Size for manual filter.
     */
    int getFilteredSize();

    /**
     * Get items to view on the page for manual filter using the same model of the container.
     */
    List<BEANTYPE> getFilteredItemsIds(int startIndex, int numberOfIds);
}
