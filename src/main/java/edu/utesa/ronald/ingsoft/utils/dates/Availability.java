package edu.utesa.ronald.ingsoft.utils.dates;

import java.sql.Time;

/**
 * El día viene dato el calendario de JAVA
 * <p>
 * Created by ronald on 7/27/17.
 */
public class Availability {

    public int day;

    public Time start;
    public Time end;

    public String description;
}
