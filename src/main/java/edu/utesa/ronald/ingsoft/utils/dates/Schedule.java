package edu.utesa.ronald.ingsoft.utils.dates;

import java.util.ArrayList;
import java.util.List;

/**
 * Dice la disponibilidad de horario.
 * Este objeto es convertido en JSON para la DB.
 * <p>
 * Created by ronald on 7/27/17.
 */
public class Schedule {

    public List<Availability> availabilities = new ArrayList<>();

    public Schedule() {
        // Instancio con los 7 días de la semana.
    }
}
