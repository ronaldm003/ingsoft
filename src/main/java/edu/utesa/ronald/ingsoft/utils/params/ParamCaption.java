package edu.utesa.ronald.ingsoft.utils.params;

/**
 * Name of params
 * <p>
 * Created by ronald on 7/8/17.
 */
public class ParamCaption {

    public static String getCaption(Integer pt) {
        switch (pt) {
            case ParamType.DEFAULT_APPOINTMENT_NAME:
                return "Default Appointment Name";
            case ParamType.URL_PICTURE_DIR_SAVE:
                return "URL Picture Dir Save";
            case ParamType.SUGGEST_SIZE:
                return "Suggest Size";
            case ParamType.SUGGEST_MIN_CHAR_EVENT:
                return "Suggest Min Char Event";
            case ParamType.SUGGEST_DELAY:
                return "Suggest Delay";
            case ParamType.EMAIL_API:
                return "Email API";
            case ParamType.EMAIL_DOMAIN:
                return "Email DOMAIN";
            case ParamType.EMAIL_FROM_DEFAULT_NO_REPLY:
                return "Email No Reply";
            case ParamType.EMAIL_FROM_DEFAULT_INFO:
                return "Email Info";
            case ParamType.EMAIL_FROM_DEFAULT_SUPPORT:
                return "Email Support";
            case ParamType.NOTIFICATION_SHOW:
                return "Notification Show";
            case ParamType.CALENDAR_FIRST_HOUR:
                return "Calendar first hour of the day";
            case ParamType.CALENDAR_LAST_HOUR:
                return "Calendar last hour of the day";
            case ParamType.SLIDER_MIN_VALUE:
                return "Slider Min Value";
            case ParamType.SLIDER_MAX_VALUE:
                return "Slider Max Value";
        }
        return "";
    }
}
