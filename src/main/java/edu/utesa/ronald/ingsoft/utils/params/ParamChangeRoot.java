package edu.utesa.ronald.ingsoft.utils.params;

/**
 * Check if can be modify by user.
 * <p>
 * Created by ronald on 7/8/17.
 */
public class ParamChangeRoot {

    public static boolean getChangeRoot(Integer pt) {
        switch (pt) {
            case ParamType.DEFAULT_APPOINTMENT_NAME:
                return false;
            case ParamType.URL_PICTURE_DIR_SAVE:
                return true;
            case ParamType.SUGGEST_SIZE:
                return true;
            case ParamType.SUGGEST_MIN_CHAR_EVENT:
                return true;
            case ParamType.SUGGEST_DELAY:
                return true;
            case ParamType.EMAIL_API:
                return true;
            case ParamType.EMAIL_DOMAIN:
                return true;
            case ParamType.EMAIL_FROM_DEFAULT_NO_REPLY:
                return true;
            case ParamType.EMAIL_FROM_DEFAULT_INFO:
                return true;
            case ParamType.EMAIL_FROM_DEFAULT_SUPPORT:
                return true;
            case ParamType.NOTIFICATION_SHOW:
                return true;
            case ParamType.CALENDAR_FIRST_HOUR:
                return false;
            case ParamType.CALENDAR_LAST_HOUR:
                return false;
            case ParamType.SLIDER_MIN_VALUE:
                return false;
            case ParamType.SLIDER_MAX_VALUE:
                return false;
        }
        return true;
    }
}
