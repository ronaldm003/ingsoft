package edu.utesa.ronald.ingsoft.utils.params;

/**
 * Text to describe.
 * <p>
 * Created by ronald on 7/8/17.
 */
public class ParamDescription {

    public static String getDescription(int pt) {
        switch (pt) {
            case ParamType.DEFAULT_APPOINTMENT_NAME:
                return "Define a defult description for appointment when is create";
            case ParamType.URL_PICTURE_DIR_SAVE:
                return "Dir to save picture upload to products.";
            case ParamType.SUGGEST_SIZE:
                return "Suggest caption size";
            case ParamType.SUGGEST_MIN_CHAR_EVENT:
                return "Specify min char write to show suggestion";
            case ParamType.SUGGEST_DELAY:
                return "Time to wait on milis";
            case ParamType.EMAIL_API:
                return "Key to send email";
            case ParamType.EMAIL_DOMAIN:
                return "Domain to send";
            case ParamType.EMAIL_FROM_DEFAULT_NO_REPLY:
                return "Email default NO REPLY";
            case ParamType.EMAIL_FROM_DEFAULT_INFO:
                return "Email default INFO";
            case ParamType.EMAIL_FROM_DEFAULT_SUPPORT:
                return "Email default SUPPORT";
            case ParamType.NOTIFICATION_SHOW:
                return "Notification to show on menu";
            case ParamType.CALENDAR_FIRST_HOUR:
                return "7";
            case ParamType.CALENDAR_LAST_HOUR:
                return "22";
            case ParamType.SLIDER_MIN_VALUE:
                return "1";
            case ParamType.SLIDER_MAX_VALUE:
                return "10";
        }
        return "";
    }
}
