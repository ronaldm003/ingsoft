package edu.utesa.ronald.ingsoft.utils.params;


import edu.utesa.ronald.ingsoft.models.params.ParamValueType;

/**
 * Define type of param
 * <p>
 * Created by ronald on 7/8/17.
 */
public class ParamParamValueType {

    public static ParamValueType getParamValueType(int pt) {
        switch (pt) {
            case ParamType.DEFAULT_APPOINTMENT_NAME:
                return ParamValueType.TEXT;
            case ParamType.URL_PICTURE_DIR_SAVE:
                return ParamValueType.TEXT;
            case ParamType.SUGGEST_SIZE:
                return ParamValueType.NUMERIC;
            case ParamType.SUGGEST_MIN_CHAR_EVENT:
                return ParamValueType.NUMERIC;
            case ParamType.SUGGEST_DELAY:
                return ParamValueType.NUMERIC;
            case ParamType.EMAIL_API:
                return ParamValueType.TEXT;
            case ParamType.EMAIL_DOMAIN:
                return ParamValueType.TEXT;
            case ParamType.EMAIL_FROM_DEFAULT_NO_REPLY:
                return ParamValueType.TEXT;
            case ParamType.EMAIL_FROM_DEFAULT_INFO:
                return ParamValueType.TEXT;
            case ParamType.EMAIL_FROM_DEFAULT_SUPPORT:
                return ParamValueType.TEXT;
            case ParamType.CALENDAR_FIRST_HOUR:
                return ParamValueType.ENUM_TIME_ZONE;
            case ParamType.CALENDAR_LAST_HOUR:
                return ParamValueType.ENUM_TIME_ZONE;
            case ParamType.SLIDER_MIN_VALUE:
                return ParamValueType.NUMERIC;
            case ParamType.SLIDER_MAX_VALUE:
                return ParamValueType.NUMERIC;
        }
        return ParamValueType.TEXT;
    }
}
