package edu.utesa.ronald.ingsoft.utils.params;

import edu.utesa.ronald.ingsoft.models.params.ParamValueType;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ronald on 7/8/17.
 */
public class ParamType {

    public static final int DEFAULT_APPOINTMENT_NAME = 0;
    public static final int URL_PICTURE_DIR_SAVE = 1;
    public static final int SUGGEST_SIZE = 2;
    public static final int SUGGEST_MIN_CHAR_EVENT = 3;
    public static final int SUGGEST_DELAY = 4;
    public static final int EMAIL_API = 5;
    public static final int EMAIL_DOMAIN = 6;
    public static final int NOTIFICATION_SHOW = 10;

    public static final int EMAIL_FROM_DEFAULT_NO_REPLY = 1000;
    public static final int EMAIL_FROM_DEFAULT_INFO = 1001;
    public static final int EMAIL_FROM_DEFAULT_SUPPORT = 1002;

    public static final int CALENDAR_FIRST_HOUR = 5000;
    public static final int CALENDAR_LAST_HOUR = 5001;

    public static final int SLIDER_MIN_VALUE = 5002;
    public static final int SLIDER_MAX_VALUE = 5003;

    public static List<Integer> values() {
        List<Integer> paramTypeList = new ArrayList<>();
        paramTypeList.add(DEFAULT_APPOINTMENT_NAME);
        paramTypeList.add(URL_PICTURE_DIR_SAVE);
        paramTypeList.add(SUGGEST_SIZE);
        paramTypeList.add(SUGGEST_MIN_CHAR_EVENT);
        paramTypeList.add(SUGGEST_DELAY);
        paramTypeList.add(EMAIL_API);
        paramTypeList.add(EMAIL_DOMAIN);
        paramTypeList.add(NOTIFICATION_SHOW);
        paramTypeList.add(EMAIL_FROM_DEFAULT_NO_REPLY);
        paramTypeList.add(EMAIL_FROM_DEFAULT_INFO);
        paramTypeList.add(EMAIL_FROM_DEFAULT_SUPPORT);
        paramTypeList.add(CALENDAR_FIRST_HOUR);
        paramTypeList.add(CALENDAR_LAST_HOUR);
        paramTypeList.add(SLIDER_MIN_VALUE);
        paramTypeList.add(SLIDER_MAX_VALUE);
        return paramTypeList;
    }

    public static String getCaption(Integer pt) {
        return ParamCaption.getCaption(pt);
    }

    public static String getDescription(Integer pt) {
        return ParamDescription.getDescription(pt);
    }

    public static String getValue(Integer pt) {
        return ParamValue.getValue(pt);
    }

    public static ParamValueType getParamValueType(Integer pt) {
        return ParamParamValueType.getParamValueType(pt);
    }

    public static boolean getChangeRoot(Integer pt) {
        return ParamChangeRoot.getChangeRoot(pt);
    }
}
