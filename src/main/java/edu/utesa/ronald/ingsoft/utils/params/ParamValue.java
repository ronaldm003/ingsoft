package edu.utesa.ronald.ingsoft.utils.params;

/**
 * Values default.
 * <p>
 * Created by ronald on 7/8/17.
 */
public class ParamValue {

    public static String getValue(int pt) {
        switch (pt) {
            case ParamType.DEFAULT_APPOINTMENT_NAME:
                return "Appointment";
            case ParamType.URL_PICTURE_DIR_SAVE:
                return "/opt/spacoma/";
            case ParamType.SUGGEST_SIZE:
                return "10";
            case ParamType.SUGGEST_MIN_CHAR_EVENT:
                return "2";
            case ParamType.SUGGEST_DELAY:
                return "150";
            case ParamType.EMAIL_API:
                return "key-f3c2722ec89b7eef62b5f60d4b42d242";
            case ParamType.EMAIL_DOMAIN:
                return "https://api.mailgun.net/v3/sandboxc60ba909622545a2a715d26d5a248f9a.mailgun.org";
            case ParamType.EMAIL_FROM_DEFAULT_NO_REPLY:
                return "no-reply@ronaldmarmol003.com";
            case ParamType.EMAIL_FROM_DEFAULT_INFO:
                return "info@ronaldmarmol003.com";
            case ParamType.EMAIL_FROM_DEFAULT_SUPPORT:
                return "support@ronaldmarmol003.com";
            case ParamType.NOTIFICATION_SHOW:
                return "6";
            case ParamType.CALENDAR_FIRST_HOUR:
                return "7";
            case ParamType.CALENDAR_LAST_HOUR:
                return "22";
            case ParamType.SLIDER_MIN_VALUE:
                return "1";
            case ParamType.SLIDER_MAX_VALUE:
                return "10";
        }
        return "";
    }
}
