package edu.utesa.ronald.ingsoft.utils.permissions;

/**
 * Determina el caption del permiso.
 * <p>
 * Created by ronald on 7/8/17.
 */
public class PermissionCaption {

    public static String getCaption(int pt) {
        if (pt < 1000) {
            return roots(pt);
        } else if (pt < 2000) {
            return windows(pt);
        }
        return reports(pt);
    }

    private static String roots(int pt) {
        switch (pt) {
            case PermissionType.ROOT:
                return "ROOT";
            case PermissionType.LOG:
                return "Log";
            case PermissionType.DEBUG_LOG:
                return "Debug Log";
            case PermissionType.CLEAR_LOGS:
                return "Clear Logs";
        }
        return "";
    }

    private static String windows(int pt) {
        switch (pt) {
            case PermissionType.LOG_IN_ALERT:
                return "Log In Alert";
            case PermissionType.LOG_IN_ALERT_NEW:
                return "New Log In Alert";
            case PermissionType.LOG_IN_ALERT_EDIT:
                return "Edit Log In Alert";
            case PermissionType.LOG_IN_ALERT_VIEW:
                return "View Log In Alert";
            case PermissionType.LOG_IN_ALERT_DELETE:
                return "Delete Log In Alert";
            case PermissionType.LOG_IN_ALERT_SHOW_DELETE:
                return "Show Delete Log In Alert";
            case PermissionType.PARAMS:
                return "Param";
            case PermissionType.PARAMS_NEW:
                return "New Param";
            case PermissionType.PARAMS_EDIT:
                return "Edit Param";
            case PermissionType.PARAMS_VIEW:
                return "View Param";
            case PermissionType.PARAMS_DELETE:
                return "Delete Param";
            case PermissionType.PARAMS_SHOW_DELETE:
                return "Show Delete Param";
            case PermissionType.PERMISSION_GROUP:
                return "Permission Group";
            case PermissionType.PERMISSION_GROUP_NEW:
                return "New Permission Group";
            case PermissionType.PERMISSION_GROUP_EDIT:
                return "Edit Permission Group";
            case PermissionType.PERMISSION_GROUP_VIEW:
                return "View Permission Group";
            case PermissionType.PERMISSION_GROUP_DELETE:
                return "Delete Permission Group";
            case PermissionType.PERMISSION_GROUP_SHOW_DELETE:
                return "Show Delete Permission Group";
            case PermissionType.PERMISSION:
                return "Permission";
            case PermissionType.PERMISSION_NEW:
                return "New Permission";
            case PermissionType.PERMISSION_EDIT:
                return "Edit Permission";
            case PermissionType.PERMISSION_VIEW:
                return "View Permission";
            case PermissionType.PERMISSION_DELETE:
                return "Delete Permission";
            case PermissionType.PERMISSION_SHOW_DELETE:
                return "Show Delete Permission";
            case PermissionType.USER:
                return "User";
            case PermissionType.USER_NEW:
                return "New User";
            case PermissionType.USER_EDIT:
                return "Edit User";
            case PermissionType.USER_VIEW:
                return "View User";
            case PermissionType.USER_DELETE:
                return "Delete User";
            case PermissionType.USER_SHOW_DELETE:
                return "Show Delete User";
            case PermissionType.NOTIFICATION:
                return "Notification";
            case PermissionType.NOTIFICATION_NEW:
                return "New Notification";
            case PermissionType.NOTIFICATION_EDIT:
                return "Edit Notification";
            case PermissionType.NOTIFICATION_VIEW:
                return "View Notification";
            case PermissionType.NOTIFICATION_DELETE:
                return "Delete Notification";
            case PermissionType.NOTIFICATION_SHOW_DELETE:
                return "Show Delete Notification";
        }
        return "";
    }

    private static String reports(int pt) {
        switch (pt) {
            case PermissionType.REPORTS:
                return "Reports";
            case PermissionType.REPORTS_PAPER:
                return "Report on Paper";
            case PermissionType.REPORTS_LOANS:
                return "Report Loans";
        }
        return "";
    }
}
