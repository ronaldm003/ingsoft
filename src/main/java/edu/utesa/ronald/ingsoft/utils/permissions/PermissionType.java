package edu.utesa.ronald.ingsoft.utils.permissions;

import java.util.ArrayList;
import java.util.List;

/**
 * Todos los permisos inician en 1,000
 * Created by ronald on 7/8/17.
 */
public class PermissionType {

    public static final int ROOT = 0;
    public static final int LOG = 1;
    public static final int DEBUG_LOG = 2;
    public static final int CLEAR_LOGS = 3;

    public static final int LOG_IN_ALERT = 1000;
    public static final int LOG_IN_ALERT_NEW = 1001;
    public static final int LOG_IN_ALERT_EDIT = 1002;
    public static final int LOG_IN_ALERT_VIEW = 1003;
    public static final int LOG_IN_ALERT_DELETE = 1004;
    public static final int LOG_IN_ALERT_SHOW_DELETE = 1005;

    public static final int PARAMS = 1010;
    public static final int PARAMS_NEW = 1011;
    public static final int PARAMS_EDIT = 1012;
    public static final int PARAMS_VIEW = 1013;
    public static final int PARAMS_DELETE = 1014;
    public static final int PARAMS_SHOW_DELETE = 1015;

    public static final int PERMISSION_GROUP = 1020;
    public static final int PERMISSION_GROUP_NEW = 1021;
    public static final int PERMISSION_GROUP_EDIT = 1022;
    public static final int PERMISSION_GROUP_VIEW = 1023;
    public static final int PERMISSION_GROUP_DELETE = 1024;
    public static final int PERMISSION_GROUP_SHOW_DELETE = 1025;

    public static final int PERMISSION = 1030;
    public static final int PERMISSION_NEW = 1031;
    public static final int PERMISSION_EDIT = 1032;
    public static final int PERMISSION_VIEW = 1033;
    public static final int PERMISSION_DELETE = 1034;
    public static final int PERMISSION_SHOW_DELETE = 1035;

    public static final int USER = 1040;
    public static final int USER_NEW = 1041;
    public static final int USER_EDIT = 1042;
    public static final int USER_VIEW = 1043;
    public static final int USER_DELETE = 1044;
    public static final int USER_SHOW_DELETE = 1045;

    public static final int NOTIFICATION = 1210;
    public static final int NOTIFICATION_NEW = 1211;
    public static final int NOTIFICATION_EDIT = 1212;
    public static final int NOTIFICATION_VIEW = 1213;
    public static final int NOTIFICATION_DELETE = 1214;
    public static final int NOTIFICATION_SHOW_DELETE = 1215;

    public static final int REPORTS = 2000;
    public static final int REPORTS_PAPER = 2001;
    public static final int REPORTS_LOANS = 2002;

    public static List<Integer> values() {
        List<Integer> permissionList = new ArrayList<>();
        permissionList.add(ROOT);
        permissionList.add(LOG);
        permissionList.add(DEBUG_LOG);
        permissionList.add(CLEAR_LOGS);
        permissionList.add(LOG_IN_ALERT);
        permissionList.add(LOG_IN_ALERT_NEW);
        permissionList.add(LOG_IN_ALERT_EDIT);
        permissionList.add(LOG_IN_ALERT_VIEW);
        permissionList.add(LOG_IN_ALERT_DELETE);
        permissionList.add(LOG_IN_ALERT_SHOW_DELETE);
        permissionList.add(PARAMS);
        permissionList.add(PARAMS_NEW);
        permissionList.add(PARAMS_EDIT);
        permissionList.add(PARAMS_VIEW);
        permissionList.add(PARAMS_DELETE);
        permissionList.add(PARAMS_SHOW_DELETE);
        permissionList.add(PERMISSION_GROUP);
        permissionList.add(PERMISSION_GROUP_NEW);
        permissionList.add(PERMISSION_GROUP_EDIT);
        permissionList.add(PERMISSION_GROUP_VIEW);
        permissionList.add(PERMISSION_GROUP_DELETE);
        permissionList.add(PERMISSION_GROUP_SHOW_DELETE);
        permissionList.add(PERMISSION);
        permissionList.add(PERMISSION_NEW);
        permissionList.add(PERMISSION_EDIT);
        permissionList.add(PERMISSION_VIEW);
        permissionList.add(PERMISSION_DELETE);
        permissionList.add(PERMISSION_SHOW_DELETE);
        permissionList.add(USER);
        permissionList.add(USER_NEW);
        permissionList.add(USER_EDIT);
        permissionList.add(USER_VIEW);
        permissionList.add(USER_DELETE);
        permissionList.add(USER_SHOW_DELETE);
        permissionList.add(REPORTS);
        permissionList.add(REPORTS_PAPER);
        permissionList.add(REPORTS_LOANS);
        permissionList.add(NOTIFICATION);
        permissionList.add(NOTIFICATION_NEW);
        permissionList.add(NOTIFICATION_EDIT);
        permissionList.add(NOTIFICATION_VIEW);
        permissionList.add(NOTIFICATION_DELETE);
        permissionList.add(NOTIFICATION_SHOW_DELETE);
        return permissionList;
    }

    public static String getCaption(int pt) {
        return PermissionCaption.getCaption(pt);
    }

    public static String getDescription(int pt) {
        return PermissionDescription.getDescription(pt);
    }
}
