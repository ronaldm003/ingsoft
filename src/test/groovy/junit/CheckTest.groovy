package junit

import groovy.transform.CompileStatic
import org.junit.FixMethodOrder
import org.junit.Test
import org.junit.runners.MethodSorters

import static org.junit.Assert.assertEquals
import static org.junit.Assert.assertNotEquals

/**
 *  Created by ronald on 7/4/17.
 */
@CompileStatic
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
class CheckTest {

    @Test
    void test() {
        org.junit.Assert.assertEquals("Chequeo si 2 + 2 = 4?", 4, 2 + 2)
    }

    @Test
    void testFail() {
        org.junit.Assert.assertNotEquals("Chequeo si 2 + 2 = 5?", 5, 2 + 2)
    }
}
