package junit

import edu.utesa.ronald.ingsoft.utils.Constants
import edu.utesa.ronald.ingsoft.utils.json.UserJson
import groovy.transform.CompileStatic
import org.junit.Assert
import org.junit.FixMethodOrder
import org.junit.Test
import org.junit.runners.MethodSorters

/**
 *  Created by ronald on 7/4/17.
 */
@CompileStatic
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
class JSonTest {

    @Test
    void test() {
        Data data = Constants.instance.convert(Constants.instance.readJsonFile("UserToDbTest"), Data.class)
        Assert.assertNotNull(data)
        for (UserJson userJson : data.data) {
            println userJson.code
        }
    }

    static class Data {
        List<UserJson> data
    }
}
