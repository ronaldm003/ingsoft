package utils

import groovy.transform.CompileStatic

/**
 *  Created by ronald on 8/6/17.
 */
@CompileStatic
@Singleton
class TestUtils {


    final String SERVER = "http://localhost:8083"
    final String APP = SERVER + "/"

    final void configureDriver() {
//        System.setProperty("webdriver.firefox.driver", new File(getClass().getClassLoader().getResource("chromedriver-2.22").toURI()).getAbsolutePath())
        System.setProperty("webdriver.chrome.driver", new File(getClass().getClassLoader().getResource("chromedriver-2.30").toURI()).getAbsolutePath())
    }

    final String USERNAME = "test"
    final String PASSWORD = "test"
}
